/*
	App configuration
*/

define(["libs/core", "helpers/menu", "helpers/locale", "helpers/theme", "libs/rollbar", "views/user"], function (core, menu, locale, theme, tracker, user) {
    // webix.skin.set("mini");
    webix.skin.$active.inputHeight = 32;
    webix.skin.$active.menuHeight = 28;
    webix.skin.$active.listItemHeight = 28;
    webix.skin.$active.buttonHeight = 30;
    webix.skin.$active.rowHeight = 25;
    // webix.skin.mini = {
    //     topLayout:"space",
    //     //bar in accordion
    //     barHeight:44,           //!!!Set the same in skin.less!!!
    //     tabbarHeight: 42,
    //     sidebarTitleHeight: 44,
    //     rowHeight:36,
    //     toolbarHeight:44,
    //     listItemHeight:36,      //list, grouplist, dataview, etc.
    //     inputHeight: 38,
    //     buttonHeight: 38,
    //     inputPadding: 3,
    //     menuHeight: 36,
    //     labelTopHeight: 22,
    //     propertyItemHeight: 28,
    //     unitHeaderHeight:36,

    //     inputSpacing: 4,
    //     borderWidth: 1,

    //     sliderHandleWidth: 14,
    //     sliderPadding: 10,
    //     sliderBorder: 1,
    //     vSliderPadding:15,
    //     vSliderHeight:100,
    //     switchHeight:22,
    //     switchWidth:50,

    //     //margin - distance between cells
    //     layoutMargin:{
    //         space:10, wide:10, clean:0, head:4,
    //         line:-1, toolbar:4, form:8, accordion: 2
    //     },
    //     //padding - distance inside cell between cell border and cell content
    //     layoutPadding:{
    //         space:10, wide:0, clean:0, head:0,
    //         line:0, toolbar:2, form:17, accordion: 0
    //     },
    //     //space between tabs in tabbar
    //     tabMargin: 0,
    //     tabOffset: 0,
    //     tabBottomOffset: 0,
    //     tabTopOffset:0,
    //     tabbarWithBorder: true,

    //     customCheckbox: true,
    //     customRadio: true,
    //     sidebarMarkAll: true,
    //     noPoint: true,
    //     borderlessPopup: true,

    //     popupPadding: 0,

    //     calendar:{ timepickerHeight:24 },
    //     calendarHeight: 70,
    //     padding:0,
    //     accordionType: "accordion",

    //     optionHeight: 32,
    //     organogramLineColor: "#CCD7E6"
    // };
    webix.codebase = document.location.href.split("#")[0].replace("index.html", "") + "libs/webix/";

    webix.i18n.dateFormat = "%d/%m/%Y";
    webix.i18n.setLocale();
    // set CDN not default  using export (excel, pdf) of webix
    webix.require.disabled = true;

    webix.rules.isEmail = function (value) {
        if (value === undefined || value === null) {
            return false;
        }
        var patt = /^([ ]{0,})[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return patt.test(value.toString().trim() || "");
    };

    webix.rules.isSpecialCharacter = function (value) {
        var patt = /^[a-zA-Z0-9\$%\^\&*\)\(+=._-]+$/;
        return patt.test(value);
    };

    webix.rules.isPhoneNumber = function (value) {
        if (value == "") return true;

        var patt = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
        return patt.test(value);
    };

    webix.rules.isTaxCode = function (value) {
        var patt = /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/;

        if (value.length > 10) {
            patt = /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][-][0-9][0-9][0-9]$/;
        }

        if (value == "") return true;

        if (patt.test(value) == false) {
            return false;
        }

        var number =
            Number(value.charAt(0)) * 31 +
            Number(value.charAt(1)) * 29 +
            Number(value.charAt(2)) * 23 +
            Number(value.charAt(3)) * 19 +
            Number(value.charAt(4)) * 17 +
            Number(value.charAt(5)) * 13 +
            Number(value.charAt(6)) * 7 +
            Number(value.charAt(7)) * 5 +
            Number(value.charAt(8)) * 3;

        return 10 - (number % 11) == Number(value.charAt(9));
    };

    if (!webix.env.touch && webix.ui.scrollSize && webix.CustomScroll) webix.CustomScroll.init();

    //configuration
    var app = core.create({
        id: "admin-demo",
        name: "Webix Admin",
        version: "0.1",
        debug: true,
        start: "/app/dashboard",
        host: "http://localhost:18374",
        //host:	"http://apiketoan.vacom.vn"
    });

    var port = "";

    if (document.location.protocol == "file:") {
        app.config.host = "http://localhost:18374";
    } else if (document.location.hostname == "localhost") {
        app.config.host = "http://localhost:18374";
    } else {
        if (document.location.port != 80 && document.location.port != 445) {
            port = ":" + document.location.port.toString();
        }

        app.config.host = document.location.protocol + "//" + document.location.hostname + port + "/api";
    }

    //alert(app.host);

    document.onkeydown = function () {
        var x = event.keyCode;
        if (((x == 70 || x == 78 || x == 79 || x == 80) && event.ctrlKey) || (x > 111 && x < 124)) {
            //alert ("No new window")
            event.cancelBubble = true;
            event.returnValue = false;
            event.keyCode = false;
            return false;
        }
    };

    webix.UIManager.addHotKey("any", function (view, ev) {
        if (typeof view.getTopParentView !== "function") return;

        var topParent = view.getTopParentView();

        var elButtons = topParent.$view.getElementsByClassName("webix_el_button");

        if (elButtons.length > 0) {
            for (var i = 0; i < elButtons.length; i++) {
                var element = elButtons[i];
                var viewId = element.attributes["view_id"].value;
                var button = webix.$$(viewId);

                if (button.config.shortcut != "undefined") {
                    var shortcut = button.config.shortcut;
                    if (shortcut == ev.code) {
                        button.callEvent("onClick");
                        return;
                    }
                }
            }
        }
    });

    webix.UIManager.addHotKey("esc", function (view) {
        if (typeof view.getTopParentView !== "function") return;

        var topParent = view.getTopParentView();

        var elButtons = topParent.$view.getElementsByClassName("webix_el_button");

        if (elButtons.length > 0) {
            for (var i = 0; i < elButtons.length; i++) {
                var element = elButtons[i];
                var viewId = element.attributes["view_id"].value;
                var button = webix.$$(viewId);

                if (button.config.shortcut == "esc") {
                    button.callEvent("onClick");
                    return;
                }
            }
        }
    });

    if (webix.storage.local.get("APP_CACHE") == null) {
        var app_cache = {};

        webix.storage.local.put("APP_CACHE", app_cache);
    }
    app.use(menu);
    app.use(locale);
    app.use(theme);
    app.use(user);

    return app;
});
