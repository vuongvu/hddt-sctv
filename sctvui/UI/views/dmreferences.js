define(["app", "views/forms/dmreferences"], function (app, dmwindowform) {
    "use strict";

    var op = [];
    var url = app.config.host + "/api/System/getAllWindows";
    webix.ajax(url, function (text, xml, xhr) {
        var rp = JSON.parse(text);
        //console.log(Object.keys(rp).length);

        for (var i = 0; i < Object.keys(rp["data"]).length; i++) {
            op.push({ ID: String(rp["data"][i]["ID"]), WINDOW_NAME: String(rp["data"][i]["WINDOW_NAME"]) });
        }
        console.log(op[0]["WINDOW_NAME"]);
    });

    var controls = [
        {
            id: "btnPlus",
            view: "button",
            type: "iconButton",
            icon: "plus",
            label: "Thêm",
            width: 110,
            css: "button_raised",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var win = this.$scope.ui(dmwindowform.$ui);
                    dmwindowform.setEditMode(1);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "iconButton",
            icon: "edit",
            label: "Sửa",
            width: 100,
            css: "button_raised",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("dmwindow-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();
                    console.log(item);
                    form.setValues(item);

                    dmwindowform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "iconButton",
            icon: "trash-o",
            label: "Xóa",
            width: 100,
            css: "button_raised",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.WINDOW_NAME,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix.ajax().post(app.config.host + "/api/System/DeleteReferences?id=" + item.id, function (text, xml, xhr) {
                                    if (text.hasOwnProperty("error")) {
                                        webix.message(text.error);
                                        return;
                                    }

                                    table.remove(item.id);
                                });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                datafetch: 2,
                loadahead: 15,
                resizeColumn: true,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var url = app.config.host + "/api/System/GetAllReferences";
                        webix.ajax(url, callback, view);
                    },
                },
                columns: [
                    { id: "REFERENCES_ID", header: "RF ID", width: 100 },
                    { id: "NAME", header: "Title", width: 250 },
                    { id: "VALUE_FIELD", header: "Value Field", width: 150 },
                    { id: "DISPLAY_FIELD", header: "Display Field", width: 150 },
                    { id: "TYPE", header: "Type", width: 100 },
                    { id: "SQL", header: "SQL", width: 250 },
                    { id: "ORDER_BY", header: "ORDER BY", width: 150 },
                    { id: "LIST_VALUE", header: "LIST VALUE", width: 250 },
                    { id: "GHICHU", header: "GHI CHU", width: 250 },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 45,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
