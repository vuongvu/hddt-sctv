define(["app", "locale", "models/user", "views/forms/68_kyhieu_donvi"], function (app, _, user, kyhieuForm) {
    "use strict";

    webix.$$("title").parse({
        title: "Phân ký kiệu hoá đơn cho đơn vị",
        details: "Phân ký kiệu hoá đơn cho đơn vị",
    });

    var initUI = function () {
        // var windowno = app.path[1].params[0];
        var table = webix.$$("kyhieu_hoadon");
        // table.disable();
        // table.showProgress();
        // table.clearAll();
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var tlbparam_s = [];
                var url = app.config.host + "/Invoice68/GetKyHieuDonVi68";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }
                var tlbParams = $$("paramDonvi");

                if (tlbParams !== undefined) {
                    var childViews = tlbParams.getChildViews();
                    var parameters = [];

                    for (var i = 0; i < childViews.length; i++) {
                        var child = childViews[i];
                        // console.log(child);
                        var id = child.config.id;
                        //var value_field = child.config.value_field;

                        var item = {
                            columnName: id,
                            value: $$(id).getValue(),
                        };
                        parameters.push(item);
                    }
                    tlbparam_s = parameters;
                    url = url + "&tlbparam=" + JSON.stringify(parameters);
                } else {
                    url = url + "&tlbparam=null";
                }
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var submenuChucNang = webix.ui({
        id: "submnchucnang",
        view: "submenu",
        width: 250,
        padding: 0,
        data: [
            {
                id: "UploadTemplate",
                value: "Tải mẫu lên",
            },
            {
                id: "SignTemplate",
                value: "Ký mẫu hóa đơn",
            },
            {
                id: "PreviewTemplate",
                value: "Xem mẫu hóa đơn (PDF)",
            },
            {
                id: "DownloadTemplate",
                value: "Tải mẫu thiết kế (.repx)",
            },
        ],
        on: {
            onItemClick: function (id, e, node) {
                this.hide();
                var selectedItem = webix.$$("congvieckinhdoanh").getSelectedItem();
                if (selectedselectedItem == null) {
                    webix.message("Bạn chưa chọn khách hàng !", "error");
                    return;
                }
            },
        },
    });

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu_hoadon");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(kyhieuForm.$ui);
                    kyhieuForm.setEditMode(1);
                    var params = null;

                    var tlbParams = $$("paramDonvi");

                    if (tlbParams !== undefined) {
                        var childViews = tlbParams.getChildViews();

                        var item = [];

                        for (var i = 0; i < childViews.length; i++) {
                            var child = childViews[i];
                            var id = child.config.id;
                            var value_field = child.config.value_field;

                            item["branch_code"] = $$(id).getValue();
                        }

                        params = item;
                    }
                    kyhieuForm.setValues(params);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu_hoadon");
                    var selectedItem = table.getSelectedItem();
                    if(selectedItem != null) {
                        webix.confirm({
                            text: "Bạn có muốn xóa bản ghi",
                            ok: "Có",
                            cancel: "Không",
                            callback: function (choose) {
                                if (choose) {
                                    webix.extend(table, webix.ProgressBar);
                                    table.disable();
                                    table.showProgress();

                                    webix
                                        .ajax()
                                        .headers({"Content-type": "application/json"})
                                        .post(app.config.host + "/Invoice68/deletekihieudonvi68", selectedItem, {
                                            error: function (text, data, XmlHttpRequest) {
                                                table.enable();
                                                table.hideProgress();
                                                var json = JSON.parse(text);
                                                webix.message(json.error, "error");
                                                //webix.message(XmlHttpRequest.statusText, "error");
                                            },
                                            success: function (text, data, XmlHttpRequest) {
                                                var res = JSON.parse(text);
                                                if (res.hasOwnProperty("error")) {
                                                    webix.message(res.error, "error");
                                                } else {
                                                    table.remove(selectedItem.id);
                                                    table.enable();
                                                    table.hideProgress();
                                                }
                                            },
                                        });
                                }
                            },
                        });
                    }
                    else {
                        webix.message("Chưa chọn dòng cần xóa", "info");
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
        {},
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "kyhieu_hoadon",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function (obj) {
                        // if (obj.ngayLap) {
                        //     obj.ngayLap = new Date(obj.ngayLap);
                        // }
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                },
                columns: [
                    {
                        id: "phanhoadon_78_donvi_id",
                        header: "phanhoadon_78_donvi_id",
                        hidden: true,
                    },
                    {
                        id: "phanhoadon_78_id",
                        header: "phanhoadon_78_id",
                        hidden: true,
                    },
                    {
                        id: "kyhieu_id",
                        header: "kyhieu_id",
                        hidden: true,
                    },
                    {
                        id: "stt",
                        header: "STT",
                        sort: "int",
                        width: 40,
                        columnType: "numeric",
                    },
                    {
                        id: "khhdon",
                        header: [
                            "Kí hiệu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 240,
                    },
                    {
                        id: "tongso",
                        header: [
                            "Tổng số",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                    },
                    {
                        id: "sosudung",
                        header: [
                            "Số sử dụng",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "ngaybatdau",
                        format: function (text) {
                            var test = new Date(text);
                            if (test !== null && test !== "null") {
                                var format = webix.Date.dateToStr("%d-%m-%Y %H:%i:%s");
                                text = removeTimeZoneT(test);
                                return format(text);
                            }
                            return "";
                        },
                        width: 240,
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "branch_code",
                        header: [
                            "Đơn vị tạo",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "donvisudung",
                        header: [
                            "Đơn vị sử dụng",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                        css: {
                            "text-align": "center",
                        },
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                id: "paramDonvi",
                view: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                borderless: true,
                height: 35,
                cols: [
                    {
                        id: "pd.branch_code",
                        view: "combo",
                        width: 500,
                        label: _("Đơn vị"),
                        options: {
                            body: {
                                //comboId: comp.id,
                                // isDefault: item.has_default,
                                url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00195",
                                ready: function () {
                                    // if (this.config.isDefault == "C") {
                                    var count = this.count();

                                    if (count > 0) {
                                        var combo = webix.$$("pd.branch_code");
                                        combo.setValue(this.getFirstId());
                                        var table = $$("kyhieu_hoadon");
                                        table.clearAll();
                                        table.load(table.config.url);
                                    }
                                    // }
                                },
                            },
                        },
                        // value_field: item.value_field,
                        // isDefault: item.has_default,
                        on: {
                            onChange: function (newv, oldv) {
                                var table = $$("kyhieu_hoadon");
                                table.clearAll();
                                table.load(table.config.url);
                            },
                        },
                    },
                ],
            },
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                // height: 40,
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };
    return {
        $ui: layout,
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
