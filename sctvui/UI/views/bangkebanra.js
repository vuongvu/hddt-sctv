define(["app", "locale", "models/user", "views/forms/bangkebanra"], function (app, _, user, bangkebanra) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Bảng kê bán ra", details: "Báo cáo bảng kê bán ra hóa đơn" });

    var controls = [
        {
            id: "btnReload",
            view: "button",
            type: "icon",
            icon: "fas fa-refresh",
            css: "webix_secondary",
            label: _("REFRESH"),
            width: 95,
            on: {
                onClick: function () {
                    var wininfoparam = this.$scope.ui(bangkebanra.$ui);
                    wininfoparam.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnPrint",
            view: "button",
            type: "icon",
            icon: "fas fa-print",
            css: "webix_secondary",
            label: _("PRINT") + " PDF",
            width: 70,
            on: {
                onClick: function () {
                    var table = $$("bangkebanra");

                    var rpParameter = table.config.rpParameter;

                    if (rpParameter == undefined || rpParameter == null) {
                        webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                        return;
                    }
                    rpParameter.type = "PDF";
                    //console.log(rpParameter);
                    if (rpParameter != null) {
                        var box = webix.modalbox({
                            title: "Đang in báo cáo ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .post(
                                app.config.host + "/Bcthsd/PrintPDFBangKeBanRa",
                                rpParameter,
                                {
                                    error: function (text, data, XmlHttpRequest) {
                                        webix.modalbox.hide(box);
                                        webix.message(XmlHttpRequest.statusText, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var file = new Blob([data], { type: "application/pdf" });
                                        var fileURL = URL.createObjectURL(file);
                                        webix.modalbox.hide(box);
                                        window.open(fileURL, "_blank");
                                    },
                                }
                                // function (text, data) {
                                //     var file = new Blob([data], { type: 'application/pdf' });
                                //     var fileURL = URL.createObjectURL(file);
                                //     webix.modalbox.hide(box);
                                //     window.open(fileURL, '_blank');
                                // }
                            );
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnExport",
            view: "button",
            type: "icon",
            css: "webix_secondary",
            icon: "fas fa-file",
            label: _("EXPORT_EXCEL"),
            width: 120,
            on: {
                onClick: function () {
                    var table = $$("bangkebanra");

                    var rpParameter = table.config.rpParameter;
                    if (rpParameter == undefined || rpParameter == null) {
                        webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                        return;
                    }
                    rpParameter.type = "xlsx";

                    if (rpParameter != null) {
                        var box = webix.modalbox({
                            title: "Đang kết xuất Excel ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .post(
                                app.config.host + "/Bcthsd/PrintPDFBangKeBanRa",
                                rpParameter,
                                {
                                    error: function (text, data, XmlHttpRequest) {
                                        webix.modalbox.hide(box);
                                        webix.message(XmlHttpRequest.statusText, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var file = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                                        //var fileURL = URL.createObjectURL(file);
                                        webix.modalbox.hide(box);
                                        //window.location.href = fileURL;
                                        webix.html.download(file, "BangKeBanRa.xlsx");
                                    },
                                }
                                // function (text, data) {

                                //     var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                                //     var fileURL = URL.createObjectURL(file);

                                //     window.location.href = fileURL;
                                // }
                            );
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                container: "testA",
                view: "treetable",
                id: "bangkebanra",
                select: true,
                resizeColumn: true,
                fullScreen: true,
                fixedRowHeight: false,
                rowLineHeight: 25,
                rowHeight: 25,
                // on: {
                //     onResize: function () {
                //         this.adjustRowHeight("ghi_chu", true);
                //         this.render();
                //     }
                // },
                columns: [
                    {
                        id: "stt",
                        header: "<center>STT</center>",
                        sort: "int",
                        width: 40,
                    },
                    {
                        id: "so_hd",
                        header: [{ text: "<center>Hóa đơn chứng từ bán</center>", colspan: 2 }, { text: "<center>Số hóa đơn</center>" }],
                        width: 500,
                        template: function (obj, common) {
                            if (obj.$level == 1) return common.treetable(obj, common) + "<span style='color:blue;'>" + obj.value + " ( " + obj.$count + " )" + "</span>";
                            return obj.so_hd;
                        },
                    },
                    {
                        id: "ngay_hd",
                        header: [null, { text: "<center>Ngày hóa đơn</center>" }],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    { id: "inv_buyerlegalname", header: "<center>Tên người mua</center>", width: 300 },
                    // {
                    //     header: "<center>Trạng thái</center>", width: 150, template: function (obj) {
                    //         if (obj.$group == true) return "";
                    //         return "<span class='hotro_status" + obj.trang_thai + "'>" + obj.trang_thai + "</span>"
                    //     }
                    // },
                    { id: "ms_thue", header: "<center>Mã số thuế</center>", width: 150 },
                    {
                        id: "tien_tt0",
                        header: "<center>Doanh thu chưa có thuế GTGT</center>",
                        width: 200,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                    },
                    {
                        id: "tien",
                        header: "<center>Thuế GTGT</center>",
                        width: 100,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                    },
                    { id: "ghi_chu", header: "<center>Ghi chú</center>", width: 150 },
                    // {
                    //     id: "nhom", header: "Nhóm", width: 500
                    // }
                    // {
                    //     id: "ngay_thuchien", header: "Ngày thực hiện", width: 150, format: function (text) {
                    //         var format = webix.Date.dateToStr("%d/%m/%Y");
                    //         text = removeTimeZoneT(text);
                    //         return format(text);
                    //     }
                    // }
                ],
                pager: "pagerA",
                scheme: {
                    $group: {
                        by: "nhom",
                        //,css:{ "text-align": "right" }
                        // map:{
                        // 	nguoi_lien_he:["nguoi_lien_he", "count"]
                        // }
                    },
                    //$sort: { by: "stt", dir: "asc" }
                },
                ready: function () {
                    //this.open(this.getFirstId());
                    //this.sort({ by: "stt", dir: "asc" });
                    this.openAll();
                    this.adjustRowHeight("ghi_chu", true);
                    this.refresh();
                },
                //url: app.config.host + "/System/ExecuteQuery?sql=" + sql
                //autoheight: true,
                //autowidth:true,
                //data: big_film_set
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                type: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "loadBangkebanra", function (item) {
                var table = $$("bangkebanra");
                // webix.extend(table, webix.OverlayBox);
                webix.extend(table, webix.ProgressBar);
                table.disable();
                table.showProgress();

                table.config.rpParameter = item;

                webix
                    .ajax()
                    .headers({ "Content-type": "application/json" })
                    .post(app.config.host + "/Bcthsd/bangkebanra", item, {
                        error: function (text, data, XmlHttpRequest) {
                            table.enable();
                            table.hideProgress();
                            alert(text);
                        },
                        success: function (text, data, XmlHttpRequest) {
                            var json = JSON.parse(text);
                            if (!json.hasOwnProperty("error")) {
                                //console.log(json);
                                $$("bangkebanra").clearAll();
                                $$("bangkebanra").parse(json);
                                $$("bangkebanra").refresh();
                            } else {
                                webix.message(json.error, "error");
                            }
                            table.enable();
                            table.hideProgress();
                        },
                    });
            });
        },
    };
});
