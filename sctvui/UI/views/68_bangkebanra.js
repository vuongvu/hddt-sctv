define(["app", "locale", "models/user", "views/forms/68_bangkebanraForm", "views/forms/68_hoadon_history"], function (app, _, user, bangkebanraForm, hoadonHistory) {
    "use strict";

    webix.$$("title").parse({
        title: "Tờ khai chứng từ bán ra",
        details: "Tờ khai chứng từ bán ra",
    });

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(bangkebanraForm.$ui);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        //{
        //    view: "button",
        //    type: "htmlbutton",
        //    width: 100,
        //    label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
        //    id: "btnPlus",
        //    shortcut: "F4",
        //    on: {
        //        onClick: function () {
        //            var detailForm = this.$scope.ui(bangkebanraForm.$ui);
        //            bangkebanraForm.setEditMode(1);
        //            detailForm.show();

        //            var url = app.config.host + "/Invoice68/GetInfoCompany";

        //            webix.ajax(url, function (text) {
        //                var json = JSON.parse(text);
        //                webix.$$("tnnt").setValue(json.name);
        //                webix.$$("mst").setValue(json.tax_code);
        //                webix.$$("ddanh").setValue(json.address);
        //            });
        //        },
        //    },
        //    click: function () {
        //        this.callEvent("onClick");
        //    },
        //},
        //{
        //    view: "button",
        //    type: "htmlbutton",
        //    width: 100,
        //    label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
        //    id: "btnEdit",
        //    shortcut: "F3",
        //    on: {
        //        onClick: function () {
        //            var table = webix.$$("bangkebanra");
        //            var selectedItem = table.getSelectedItem();
        //            if (selectedItem == null) {
        //                webix.message("Bạn chưa chọn dòng nào ! ", "error");
        //                return;
        //            }

        //            var detailForm = this.$scope.ui(bangkebanraForm.$ui);
        //            bangkebanraForm.setEditMode(2);
        //            bangkebanraForm.setValues(selectedItem);
        //            if (selectedItem.trang_thai == 1) {
        //                var form = $$("bangkebanra-form").elements;
        //                for (var key in form) {
        //                    var $el = form[key];
        //                    $el.define("readonly", "true");
        //                    $el.define("css", "mcls_readonly_text");
        //                    $el.refresh();
        //                }

        //                webix.$$("btnBTHSave").destructor();
        //                webix.$$("btnTaiDanhSachBanRa").destructor();
        //            }
        //            detailForm.show();
        //        },
        //    },
        //    click: function () {
        //        this.callEvent("onClick");
        //    },
        //},
        //{
        //    view: "button",
        //    type: "htmlbutton",
        //    width: 100,
        //    label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
        //    id: "btnDelete",
        //    shortcut: "F8",
        //    on: {
        //        onClick: function () {
        //            var table = webix.$$("bangkebanra");
        //            var selectedItem = table.getSelectedItem();
        //            if (selectedItem == null) {
        //                webix.message("Bạn chưa chọn khách hàng ! ", "error");
        //                return;
        //            }
        //            if (selectedItem.trang_thai == 1) {
        //                webix.message("Bảng tổng hợp đã gửi CQT không thể xóa ! ", "error");
        //                return;
        //            }

        //            var khData = {};

        //            khData.editmode = 3;
        //            khData.data = [];
        //            khData.data.push(selectedItem);

        //            webix.confirm({
        //                text: "Bạn có muốn xóa bản ghi",
        //                ok: "Có",
        //                cancel: "Không",
        //                callback: function (choose) {
        //                    if (choose) {
        //                        webix
        //                            .ajax()
        //                            .headers({ "Content-type": "application/json" })
        //                            .post(app.config.host + "/Invoice68/SaveBangKeBanRa", khData, function (text) {
        //                                var response = JSON.parse(text);

        //                                if (response.hasOwnProperty("error")) {
        //                                    webix.message({ type: "error", text: response.error });
        //                                    return;
        //                                }

        //                                // table.remove(item.id);
        //                                var btnRefresh = $$("btnRefresh");
        //                                btnRefresh.callEvent("onClick");
        //                            });
        //                    }
        //                },
        //            });
        //        },
        //    },
        //    click: function () {
        //        this.callEvent("onClick");
        //    },
        //},
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fas fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + "In PDF" + "</span>",
            id: "btnPreview",
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = $$("bangkebanra");

                    var rpParameter = table.config.rpParameter;

                    if (rpParameter == undefined || rpParameter == null) {
                        webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                        return;
                    }

                    var datafrmAll = table.serialize();

                    if (datafrmAll.length == 0) {
                        webix.message("Không có dữ liệu để in !", "error");
                        return;
                    }

                    rpParameter.type = "PDF";
                    if (rpParameter != null) {
                        var box = webix.modalbox({
                            title: "Đang in báo cáo ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .post(
                                app.config.host + "/Invoice68/PrintPDFBangKeBanRa",
                                rpParameter,
                                {
                                    error: function (text, data, XmlHttpRequest) {
                                        webix.modalbox.hide(box);
                                        webix.message(XmlHttpRequest.statusText, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var file = new Blob([data], { type: "application/pdf" });
                                        var fileURL = URL.createObjectURL(file);
                                        webix.modalbox.hide(box);
                                        window.open(fileURL, "_blank");
                                    },
                                }
                                // function (text, data) {
                                //     var file = new Blob([data], { type: 'application/pdf' });
                                //     var fileURL = URL.createObjectURL(file);
                                //     webix.modalbox.hide(box);
                                //     window.open(fileURL, '_blank');
                                // }
                            );
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 150,
            label: '<i class="fa fa-file fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("EXPORT_EXCEL") + "</span>",
            id: "btnExportExcel",
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = $$("bangkebanra");

                    var rpParameter = table.config.rpParameter;
                    if (rpParameter == undefined || rpParameter == null) {
                        webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                        return;
                    }
                    rpParameter.type = "xlsx";

                    var datafrmAll = table.serialize();

                    if (datafrmAll.length == 0) {
                        webix.message("Không có dữ liệu để xuất !", "error");
                        return;
                    }

                    if (rpParameter != null) {
                        var box = webix.modalbox({
                            title: "Đang kết xuất Excel ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .post(app.config.host + "/Invoice68/PrintPDFBangKeBanRa", rpParameter, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                                    //var fileURL = URL.createObjectURL(file);
                                    webix.modalbox.hide(box);
                                    //window.location.href = fileURL;
                                    webix.html.download(file, "BangKeBanRa.xlsx");
                                },
                            });
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        // {
        //     view: "button",
        //     type: "htmlbutton",
        //     width: 100,
        //     label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Gửi email") + "</span>",
        //     id: "btnSendEmail",
        //     on: {
        //         onClick: function () {
        //             var table = webix.$$("hoadon");
        //             var selectedItem = table.getSelectedItem();
        //             if (selectedItem == null) {
        //                 webix.message("Bạn chưa chọn khách hàng ! ", "error");
        //                 return;
        //             }
        //         },
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     },
        // },
        //{
        //    view: "button",
        //    type: "htmlbutton",
        //    width: 100,
        //    label: '<i class="fa fa-pencil fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Ký gửi CQT") + "</span>",
        //    id: "btnSign",
        //    on: {
        //        onClick: function () {
        //            var tableDK = webix.$$("bangkebanra");
        //            var selectedItem = tableDK.getSelectedItem();
        //            if (selectedItem == null) {
        //                webix.message("Bạn chưa chọn dòng ! ", "error");
        //                return;
        //            }
        //            if (selectedItem.trang_thai == 1) {
        //                webix.message("Mẫu đã ký và gửi CQT bạn không thể ký lại! ", "error");
        //                return;
        //            }

        //            webix
        //                .ajax()
        //                .headers({ "Content-Type": "application/json" })
        //                .get(app.config.host + "/Invoice68/GetSignType", function (text) {
        //                    var result = JSON.parse(text);
        //                    var kytaptrung = "";
        //                    for (var i = 0; i < result.length; i++) {
        //                        var item = result[i];
        //                        kytaptrung = item.value;
        //                    }

        //                    webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
        //                        var listcerts = JSON.parse(text);
        //                        if (listcerts.length > 0) {
        //                            var box = webix.modalbox({
        //                                title: "Chọn chứng thư số",
        //                                buttons: ["Nhận", "Hủy bỏ"],
        //                                text: "<div id='ctnCerts'></div>",
        //                                top: 100,
        //                                width: 750,
        //                                callback: function (result) {
        //                                    switch (result) {
        //                                        case "1":
        //                                            webix.modalbox.hide();
        //                                            break;
        //                                        case "0":
        //                                            var table = $$("tblCerts");
        //                                            var data = table.serialize();
        //                                            var cert = null;

        //                                            for (var i = 0; i < data.length; i++) {
        //                                                if (data[i].chon == true) {
        //                                                    cert = data[i];
        //                                                    break;
        //                                                }
        //                                            }

        //                                            if (cert == null) {
        //                                                webix.message("Bạn chưa chọn chứng thư số");
        //                                                return;
        //                                            }
        //                                            var postData = {
        //                                                branch_code: user.getDvcsID(),
        //                                                username: user.getCurrentUser(),
        //                                                cer_serial: cert.cer_serial,
        //                                                id: selectedItem.id,
        //                                            };

        //                                            webix.extend(tableDK, webix.ProgressBar);
        //                                            tableDK.disable();
        //                                            tableDK.showProgress();
        //                                            webix
        //                                                .ajax()
        //                                                .headers({ "Content-type": "application/json" })
        //                                                .post(app.config.host + "/Invoice68/SignBangKeBanRa", postData, {
        //                                                    error: function (text, data, XmlHttpRequest) {
        //                                                        tableDK.enable();
        //                                                        tableDK.hideProgress();
        //                                                        // var json = JSON.parse(text);
        //                                                        // webix.message(json.ExceptionMessage, "error");
        //                                                        webix.message(text + XmlHttpRequest, "error");
        //                                                    },
        //                                                    success: function (text, data, XmlHttpRequest) {
        //                                                        var json = JSON.parse(text);
        //                                                        tableDK.enable();
        //                                                        tableDK.hideProgress();
        //                                                        if (!json.hasOwnProperty("error")) {
        //                                                            webix.message("Gửi thành công đến CQT !", "success");
        //                                                            // $$("huyhoadonForms").close();
        //                                                            app.callEvent("dataChangebangkebanra", [1, null]);
        //                                                        } else {
        //                                                            webix.message(json.error, "error");
        //                                                        }
        //                                                    },
        //                                                });
        //                                            return;
        //                                    }
        //                                },
        //                            });
        //                            var tblMatHang = webix.ui({
        //                                container: "ctnCerts",
        //                                id: "tblCerts",
        //                                view: "datatable",
        //                                borderless: true,
        //                                resizeColumn: true,
        //                                height: 350,
        //                                columns: [
        //                                    {
        //                                        id: "chon",
        //                                        header: _("CHON"),
        //                                        width: 65,
        //                                        checkValue: true,
        //                                        uncheckValue: false,
        //                                        template: "{common.checkbox()}",
        //                                    },
        //                                    {
        //                                        id: "cer_serial",
        //                                        header: _("SO_SERIAL"),
        //                                        width: 150,
        //                                    },
        //                                    {
        //                                        id: "subject_name",
        //                                        header: _("SUBJECTNAME"),
        //                                        width: 450,
        //                                    },
        //                                    {
        //                                        id: "begin_date",
        //                                        header: _("NGAY_BAT_DAU"),
        //                                        width: 200,
        //                                    },
        //                                    {
        //                                        id: "end_date",
        //                                        header: _("NGAY_KET_THUC"),
        //                                        width: 200,
        //                                    },
        //                                    {
        //                                        id: "issuer",
        //                                        header: _("DONVI"),
        //                                        width: 450,
        //                                    },
        //                                ],
        //                                url: app.config.host + "/Invoice68/GetListCertificatesFile68",
        //                            });
        //                        } else {
        //                            var table = $$("bangkebanra");
        //                            webix.extend(table, webix.ProgressBar);
        //                            table.disable();
        //                            table.showProgress();

        //                            var a = webix.ajax(app.config.host + "/Invoice68/XmlBangKeBanRa?id=" + selectedItem.id);
        //                            var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

        //                            webix.promise.all([a, b, selectedItem]).then(function (results) {
        //                                // webix.extend(tblInvoice, webix.ProgressBar);
        //                                // tblInvoice.disable();
        //                                // tblInvoice.showProgress();
        //                                var json = results[0].json();

        //                                if (json.hasOwnProperty("error")) {
        //                                    webix.message({ type: "error", text: json.error });
        //                                }

        //                                var listcerts = results[1].json();
        //                                var item = results[2];

        //                                if (listcerts.length == 0) {
        //                                    table.enable();
        //                                    table.hideProgress();
        //                                    webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
        //                                    return;
        //                                }

        //                                require(["views/modules/minvoiceplugin"], function (plugin) {
        //                                    var hub = plugin.hub();
        //                                    var hubconnect = plugin.conn();
        //                                    if (hubconnect.state != 1) {
        //                                        table.enable();
        //                                        table.hideProgress();
        //                                        webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
        //                                        return;
        //                                    }
        //                                    hub.off("resCommand");
        //                                    hub.on("resCommand", function (result) {
        //                                        var json = JSON.parse(result);

        //                                        if (json.hasOwnProperty("error")) {
        //                                            webix.message({ type: "error", text: json.error });
        //                                        } else {
        //                                            var parameters = {
        //                                                xml: json.xml,
        //                                                id: selectedItem.id,
        //                                                shdon: json.shdon,
        //                                            };

        //                                            webix
        //                                                .ajax()
        //                                                .headers({ "Content-Type": "application/json" })
        //                                                .post(app.config.host + "/Invoice68/SaveXmlBangKeBanRa", parameters, function (text) {
        //                                                    var result = JSON.parse(text);
        //                                                    if (result.hasOwnProperty("error")) {
        //                                                        webix.message(result.error, "error");
        //                                                    } else {
        //                                                        webix.message("Gửi thành công đến CQT !", "success");
        //                                                        // $$("huyhoadonForms").close();
        //                                                        app.callEvent("dataChangebangkebanra", [1, null]);
        //                                                    }
        //                                                });
        //                                        }
        //                                        table.enable();
        //                                        table.hideProgress();
        //                                    });

        //                                    var _listcerts = [];

        //                                    for (var i = 0; i < listcerts.length; i++) {
        //                                        var _cert = listcerts[i];

        //                                        _listcerts.push({
        //                                            so_serial: _cert.cer_serial,
        //                                            ngaybatdau: _cert.begin_date,
        //                                            ngayketthuc: _cert.end_date,
        //                                            issuer: _cert.issuer,
        //                                            subjectname: _cert.subject_name,
        //                                        });
        //                                    }

        //                                    var arg = {
        //                                        idData: "#data",
        //                                        xml: json["xml"],
        //                                        shdon: json.shdon,
        //                                        idSigner: "seller",
        //                                        tagSign: "NNT",
        //                                        listcerts: _listcerts,
        //                                    };

        //                                    hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
        //                                        .done(function () {
        //                                            console.log("Success !");
        //                                        })
        //                                        .fail(function (error) {
        //                                            webix.message(error, "error");
        //                                        });
        //                                });
        //                            });
        //                        }
        //                    });
        //                });
        //        },
        //    },
        //    click: function () {
        //        this.callEvent("onClick");
        //    },
        //},
        //{
        //    view: "button",
        //    type: "htmlbutton",
        //    width: 120,
        //    label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + "Lập bổ sung" + "</span>",
        //    id: "btnCreatePlus",
        //    shortcut: "F4",
        //    on: {
        //        onClick: function () {
        //            var detailForm = this.$scope.ui(bangkebanraForm.$ui);
        //            bangkebanraForm.setEditMode(1);
        //            detailForm.show();

        //            webix.$$("ldau").setValue(0);

        //            var url = app.config.host + "/Invoice68/GetInfoCompany";

        //            webix.ajax(url, function (text) {
        //                var json = JSON.parse(text);
        //                webix.$$("tnnt").setValue(json.name);
        //                webix.$$("mst").setValue(json.tax_code);
        //                webix.$$("ddanh").setValue(json.address);
        //            });
        //        },
        //    },
        //    click: function () {
        //        this.callEvent("onClick");
        //    },
        //},
        // {
        //     view: "button",
        //     type: "htmlbutton",
        //     width: 120,
        //     label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Lập bổ sung") + "</span>",
        //     id: "btnUp",
        //     on: {
        //         onClick: function () {
        //             var table = webix.$$("hoadon");
        //             var selectedItem = table.getSelectedItem();
        //             if (selectedItem == null) {
        //                 webix.message("Bạn chưa chọn khách hàng ! ", "error");
        //                 return;
        //             }
        //         },
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     },
        // },
        //{
        //    view: "button",
        //    type: "htmlbutton",
        //    width: 100,
        //    label: '<i class="fa fa-download fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Tải XML") + "</span>",
        //    id: "btnDownloadXml",
        //    on: {
        //        onClick: function () {
        //            var table = webix.$$("bangkebanra");
        //            var selectedItem = table.getSelectedItem();
        //            if (selectedItem == null) {
        //                webix.message("Bạn chưa chọn khách hàng ! ", "error");
        //                return;
        //            }
        //            if (selectedItem.trang_thai != 1) {
        //                webix.message("Bảng tổng hợp chưa gửi CQT không tồn tại xml ! ", "error");
        //                return;
        //            }

        //            webix.extend(table, webix.ProgressBar);
        //            table.disable();
        //            table.showProgress();

        //            var url = app.config.host + "/Invoice68/ExportXMLBangKeBanRa?id=" + selectedItem.id;
        //            webix
        //                .ajax()
        //                .response("arraybuffer")
        //                .get(url, function (text, data) {
        //                    table.enable();
        //                    table.hideProgress();

        //                    var file = new Blob([data], { type: "application/xml" });
        //                    webix.html.download(file, "BangKeBanRa.xml");
        //                    // var fileURL = URL.createObjectURL(file);
        //                    // window.open(fileURL, "_blank");
        //                });
        //        },
        //    },
        //    click: function () {
        //        this.callEvent("onClick");
        //    },
        //},
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                container: "testA",
                view: "treetable",
                id: "bangkebanra",
                select: true,
                resizeColumn: true,
                fullScreen: true,
                fixedRowHeight: false,
                rowLineHeight: 25,
                rowHeight: 25,
                columns: [
                    {
                        id: "stt",
                        header: "<center>STT</center>",
                        sort: "int",
                        width: 40,
                    },
                    {
                        id: "ky_hieu",
                        header:"<center>Ký hiệu</center>",
                        width: 500,
                        template: function (obj, common) {
                            if (obj.$level == 1) return common.treetable(obj, common) + "<span style='color:blue;'>" + obj.value + " ( " + obj.$count + " )" + "</span>";
                            return obj.ky_hieu;
                        },
                    },
                    {
                        id: "so_hd",
                        header: [{ text: "<center>Hóa đơn chứng từ bán</center>", colspan: 2 }, { text: "<center>Số hóa đơn</center>" }],
                        width: 500,
                        template: function (obj, common) {
                            if (obj.$level == 1) return common.treetable(obj, common) + "<span style='color:blue;'>" + obj.value + " ( " + obj.$count + " )" + "</span>";
                            return obj.so_hd;
                        },
                    },
                    {
                        id: "ngay_hd",
                        header: [null, { text: "<center>Ngày hóa đơn</center>" }],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    { id: "ten_kh_hd", header: "<center>Tên người mua</center>", width: 300 },
                    { id: "ms_thue", header: "<center>Mã số thuế</center>", width: 150 },
                    {
                        id: "tien_tt0",
                        header: "<center>Doanh thu chưa có thuế GTGT</center>",
                        width: 200,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                    },
                    {
                        id: "tien",
                        header: "<center>Thuế GTGT</center>",
                        width: 100,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                    },
                    { id: "ghi_chu", header: "<center>Ghi chú</center>", width: 200 },
                ],
                pager: "pagerA",
                scheme: {
                    $group: {
                        by: "nhom",
                        //,css:{ "text-align": "right" }
                        // map:{
                        // 	nguoi_lien_he:["nguoi_lien_he", "count"]
                        // }
                    },
                    //$sort: { by: "stt", dir: "asc" }
                },
                ready: function () {
                    //this.open(this.getFirstId());
                    //this.sort({ by: "stt", dir: "asc" });
                    this.openAll();
                    this.adjustRowHeight("ghi_chu", true);
                    this.refresh();
                },
                //url: app.config.host + "/System/ExecuteQuery?sql=" + sql
                //autoheight: true,
                //autowidth:true,
                //data: big_film_set
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChangebangkebanra", function (item) {
                var table = $$("bangkebanra");
                // webix.extend(table, webix.OverlayBox);
                webix.extend(table, webix.ProgressBar);
                table.disable();
                table.showProgress();

                table.config.rpParameter = item;

                webix
                    .ajax()
                    .headers({ "Content-type": "application/json" })
                    .post(app.config.host + "/Invoice68/GetDSHoaDonBangKeBanRa", item, {
                        error: function (text, data, XmlHttpRequest) {
                            table.enable();
                            table.hideProgress();
                            alert(text);
                        },
                        success: function (text, data, XmlHttpRequest) {
                            var json = JSON.parse(text);
                            if (!json.hasOwnProperty("error")) {
                                //console.log(json);
                                $$("bangkebanra").clearAll();
                                $$("bangkebanra").parse(json);
                                $$("bangkebanra").refresh();
                            } else {
                                webix.message(json.error, "error");
                            }
                            table.enable();
                            table.hideProgress();
                        },
                    });
            });
        },
    };
});
