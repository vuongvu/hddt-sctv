define(["app", "locale", "models/user"], function (app, _, user) {
    "use strict";

    webix.$$("title").parse({
        title: "Quản lý mã thông điệp CQT",
        // details: "Bảng tổng hợp hóa đơn",
    });

    var initUI = function () {
        var table = webix.$$("mau04_68");
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var url = app.config.host + "/Invoice68/GetThongDiepThueTraVe";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var controls = [
        {
            // view: "button",
            // type: "htmlbutton",
            // width: 100,
            // label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            // id: "btnRefresh",
            // on: {
            //     onClick: function () {
            //         var table = webix.$$("mau04_68");
            //         table.clearAll();
            //         table.load(table.config.url);
            //     },
            // },
            // click: function () {
            //     this.callEvent("onClick");
            // },
        },
        {
            // view: "button",
            // type: "htmlbutton",
            // width: 100,
            // label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            // id: "btnPlus",
            // shortcut: "F4",
            // on: {
            //     onClick: function () {
            //         // var detailForm = this.$scope.ui(form_0405.$ui);
            //         // form_0405.setEditMode(0);
            //         // detailForm.show();
            //     },
            // },
            // click: function () {
            //     this.callEvent("onClick");
            // },
        },
        {
            // view: "button",
            // type: "htmlbutton",
            // width: 100,
            // label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            // id: "btnEdit",
            // shortcut: "F3",
            // on: {
            //     onClick: function () {
            //         var table = webix.$$("mau04_68");
            //         var selectedItem = table.getSelectedItem();
            //         if (selectedItem == null) {
            //             webix.message("Bạn chưa chọn hóa đơn ! ", "error");
            //             return;
            //         }
            //         if (selectedItem.trang_thai == 1) {
            //             webix.message("Mẫu đã ký và gửi CQT bạn không thể sửa! ", "error");
            //             return;
            //         }
            //         if (selectedItem.loai_tbao == "5") {
            //             webix.message("Mẫu 05 nhận từ CQT bạn không thể sửa! ", "error");
            //             return;
            //         }
            //         // var detailForm = this.$scope.ui(form_0405.$ui);
            //         // form_0405.setEditMode(0);
            //         // detailForm.show();
            //     },
            // },
            // click: function () {
            //     this.callEvent("onClick");
            // },
        },
        {
            // view: "button",
            // type: "htmlbutton",
            // width: 100,
            // label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            // id: "btnDelete",
            // shortcut: "F8",
            // on: {
            //     onClick: function () {
            //         var table = webix.$$("mau04_68");
            //         var selectedItem = table.getSelectedItem();
            //         if (selectedItem == null) {
            //             webix.message("Bạn chưa chọn khách hàng ! ", "error");
            //             return;
            //         }
            //         if (selectedItem.trang_thai == 1) {
            //             webix.message("Mẫu đã ký và gửi CQT bạn không thể xóa! ", "error");
            //             return;
            //         }
            //         if (selectedItem.loai_tbao == "5") {
            //             webix.message("Mẫu 05 nhận từ CQT bạn không thể xóa! ", "error");
            //             return;
            //         }
            //         webix.confirm({
            //             text: "Bạn có muốn xóa hóa đơn",
            //             ok: "Có",
            //             cancel: "Không",
            //             callback: function (choose) {
            //                 if (choose) {
            //                     webix.extend(table, webix.ProgressBar);
            //                     table.disable();
            //                     table.showProgress();
            //                     webix
            //                         .ajax()
            //                         .headers({ "Content-type": "application/json" })
            //                         .post(app.config.host + "/Invoice68/deleteMau04", selectedItem, {
            //                             error: function (text, data, XmlHttpRequest) {
            //                                 table.enable();
            //                                 table.hideProgress();
            //                                 var json = JSON.parse(text);
            //                                 webix.message(json.error, "error");
            //                                 //webix.message(XmlHttpRequest.statusText, "error");
            //                             },
            //                             success: function (text, data, XmlHttpRequest) {
            //                                 table.remove(selectedItem.id);
            //                                 table.enable();
            //                                 table.hideProgress();
            //                             },
            //                         });
            //                 }
            //             },
            //         });
            //     },
            // },
            // click: function () {
            //     this.callEvent("onClick");
            // },
        },
        // {
        //     view: "button",
        //     type: "htmlbutton",
        //     width: 100,
        //     label: '<i class="fa fa-eye fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Xem in") + " (F7)" + "</span>",
        //     id: "btnPreview",
        //     shortcut: "F7",
        //     on: {
        //         onClick: function () {
        //             var table = webix.$$("mau04_68");
        //             var selectedItem = table.getSelectedItem();
        //             if (selectedItem == null) {
        //                 webix.message("Bạn chưa dòng in ! ", "error");
        //                 return;
        //             }
        //             var box = webix.modalbox({
        //                 title: "Đang in hóa đơn ....",
        //                 text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
        //                 width: 350,
        //                 height: 200,
        //             });

        //             var newWin = window.open();
        //             var html =
        //                 "<html><head><title>Hóa đơn điện tử SCTV</title>" +
        //                 '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
        //                 '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
        //                 '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
        //             newWin.document.write(html);

        //             webix
        //                 .ajax()
        //                 .response("arraybuffer")
        //                 .headers({ "Content-type": "application/json" })
        //                 .get(app.config.host + "/Invoice68/inMau04?id=" + selectedItem.id + "&type=PDF&loai=" + selectedItem.loai_tbao, {
        //                     error: function (text, data, XmlHttpRequest) {
        //                         webix.modalbox.hide(box);
        //                         webix.message(XmlHttpRequest.statusText, "error");
        //                     },
        //                     success: function (text, data, XmlHttpRequest) {
        //                         var file = new Blob([data], { type: "application/pdf" });
        //                         var fileURL = URL.createObjectURL(file);
        //                         //window.open(fileURL, '_blank');
        //                         webix.modalbox.hide(box);
        //                         newWin.location = fileURL;
        //                     },
        //                 });
        //         },
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     },
        // },
        {},
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "mau04_68",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function () {
                        // this.data.each(function (obj, i) {
                        //     obj.index = i + 1;
                        // });
                    },
                },
                onClick: {
                    Dowload0405: function (event, cell, target) {
                        var win = webix.$$("mau04_68");
                        var record = win.getItem(cell.row);

                        debugger;

                        // if (record.loai_tbao != "5") {
                        //     if (record.trang_thai != 1) {
                        //         webix.message("Mẫu chưa được ký không thể tải !", "error");
                        //         return;
                        //     }
                        // }
                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();
                        var filename =
                            record.mltdiep == "202"
                                ? "202.xml"
                                : record.mltdiep == "204"
                                ? "204.xml"
                                : record.mltdiep == "301"
                                ? "301.xml"
                                : record.mltdiep == "302"
                                ? "302.xml"
                                : record.mltdiep == "102"
                                ? "102.xml"
                                : record.mltdiep == "103"
                                ? "103.xml"
                                : "104.xml";
                        var url = app.config.host + "/Invoice68/ExportXMLThongDiepCQT?id=" + record.id;
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .get(url, function (text, data) {
                                win.enable();
                                win.hideProgress();

                                var file = new Blob([data], { type: "application/xml" });
                                webix.html.download(file, filename);
                                // var fileURL = URL.createObjectURL(file);
                                // window.open(fileURL, "_blank");
                            });
                        // webix.message("Delete row: " + cell.row);
                        return false; // here it blocks the default behavior
                    },

                    InMau0405: function (event, cell, target) {
                        var table = webix.$$("mau04_68");
                        var item = table.getItem(cell.row);

                        //webix.message("Chưa có mẫu in ...", "error");
                        // var rpParameter = null;
                        var box = webix.modalbox({
                            title: "Đang in hóa đơn ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        var newWin = window.open();
                        var html =
                            "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                            '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                            '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                            '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                        newWin.document.write(html);

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inMaThongDiepCQT?id=" + item.id + "&mltdiep=" + item.mltdiep, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    },
                },
                on: {
                    // onSubViewCreate: function (view, item) {
                    //     var subView = view;

                    //     if (subView != null) {
                    //         var tableDetail = webix.$$("mau04_68_chitiet");
                    //         var url = app.config.host + "/Invoice68/GetDataMau0405Detail?id=" + item.id;
                    //         tableDetail.clearAll();
                    //         tableDetail.load(url);
                    //     }
                    // },
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onAfterLoad: function () {},
                },
                columns: [
                    // {
                    //     id: "id",
                    //     header: "id",
                    //     hidden: true,
                    // },
                    // {
                    //     template: "{common.subrow()}",
                    //     width: 50,
                    // },
                    {
                        id: "stt",
                        header: "STT",
                        sort: "int",
                        width: 40,
                    },
                    {
                        id: "mltdiep",
                        header: [
                            "Mã loại thông điệp",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 200,
                    },
                    {
                        id: "mltdiep",
                        header: [
                            "Tên loại thông điệp",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 200,
                        template: function (obj) {
                            return obj.mltdiep == "202"
                                ? "Cấp mã hóa đơn"
                                : obj.mltdiep == "204"
                                ? "Kiểm tra dữ liệu hóa đơn"
                                : obj.mltdiep == "301"
                                ? "Xử lý hóa đơn sai sót"
                                : obj.mltdiep == "302"
                                ? "Rà soát hóa đơn"
                                : obj.mltdiep == "102"
                                ? "Tiếp nhận tờ khai ĐK/SD HDDT"
                                : obj.mltdiep == "103"
                                ? "Chấp nhận tờ khai ĐK/SD HDDT"
                                : "Chấp nhận tờ khai ĐK/SD HDDT khi ủy nhiệm/nhận ủy nhiệm";
                        },
                    },
                    {
                        id: "mtdiep_cqt",
                        //header: ["", "Mã thông điệp CQT"],
                        header: [
                            "Mã thông điệp CQT",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 450,
                    },
                    {
                        id: "Dowload0405",
                        header: ["", "Tải về"],
                        css: { "text-align": "center" },
                        width: 120,
                        template: function (obj) {
                            return "<span class='Dowload0405' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            // if (obj.loai_tbao == "5") {
                            //     return "<span class='Dowload0405' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            // } else {
                            //     if (obj.trang_thai == 1) {
                            //         return "<span class='Dowload0405' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            //     } else return "<span class='Dowload0405' style='color: #c8c8cc'><i class='fas fa-download'></i></span> ";
                            // }
                        },
                    },
                    {
                        id: "InMau0405",
                        header: ["", "In"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.mltdiep != "204" && obj.mltdiep != "202") {
                                return "<span class='InMau0405' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                            } else {
                                return "<span style='color: #c8c8cc'><i class='fas fa-print'></i></span> ";
                            }
                        },
                        width: 120,
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                // height: 40,
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var table = webix.$$("mau04_68");

            // $scope.on(app, "dataChangedMau0405", function (mode, item) {
            //     // if (mode == 1) {
            //     //     table.add(item);
            //     // }
            //     // if (mode == 5) {
            //     table.clearAll();
            //     table.load(table.config.url);
            //     // }
            // });

            $scope.on(app, "filterTable", function (report_id, infoWindowId, item) {
                var infoparam = {
                    infowindow_id: infoWindowId,
                    parameter: item,
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
