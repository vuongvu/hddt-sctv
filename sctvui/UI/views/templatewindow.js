define([
    "app",
    "views/forms/templatewindow",
    "views/forms/infowindow",
	"locale"
], function(app,windowform,infowindow,_) {
    'use strict';
	
	var layoutId = "";
	var col = [];
	var sub_col = [];
    var windowconfig = null;
    var buttons = [];
	//var windowno = "":
	
	var initUI = function(){
		var windowno = app.path[1].params[0];
		
		col = [];		
        sub_col = [];
        
        var query = "SELECT TOP 1 * FROM WB_MENU WHERE WINDOW_ID='"+ windowno +"'";

        var url = app.config.host + "/System/ExecuteQuery?sql=" + query;

        webix.ajax(url,function(text){
            var json = JSON.parse(text);

            if (json.length>0){
                webix.$$("title").parse({title: json[0]["name"], details: json[0]["details"]});
            }
        });

        /* query = "SELECT CODE as id,NAME as value FROM VC_MAUIN WHERE WINDOW_ID='"+ windowno +"'";
        url = app.config.host + "/api/System/ExecuteQuery?sql=" + query;

        webix.ajax(url,function(text){
            var json = JSON.parse(text);

            if (json.length>0){
                $$("cbMauIn").define("options",json);
                $$("cbMauIn").setValue(json[0].id);
                $$("cbMauIn").show();
                $$("btnPrint").show();                
            }
            else {
                 $$("cbMauIn").define("options",[]);
                 $$("cbMauIn").hide();
                 $$("btnPrint").hide();
            }

            $$("cbMauIn").refresh();
        }); */

        url = app.config.host + "/System/ExecuteCommand";

        webix.ajax()
            .headers({'Content-type':'application/json'})
            .post(url,
                    { 
                        command : "CM00002", 
                        parameter: {
                            window_id: windowno
                        }
                    },function(text){
            var json = JSON.parse(text);

            if (buttons.length>0){
                for(var i=0;i<buttons.length;i++){
                    var item = buttons[i];
                    $$("tlbMain").removeView(item.id);
                }                
            }

            buttons = [];

            for(var i=0;i<json.length;i++){
                var item = json[i];

                var fn = new Function(item.on_click);

                var button = {
                    id: item.name,
                    label: _(item.caption),
                    view: "button", 
                    type: "icon", 
                    icon: item.icon, 
                    width: item.width,
                    click: fn                  
                };

                buttons.push(button);
                $$("tlbMain").addView(button,6);

            }

            
        });

		
		url = app.config.host + "/System/GetConfigWinByNo?id="+windowno ;
		
		webix.$$("windowData").getParentView().getParentView().getParentView().disable();
        webix.$$("windowData").getParentView().getParentView().getParentView().showProgress();
		
		webix.ajax(url, function (text, xml, xhr) {

			var rp = JSON.parse(text);
			windowconfig = rp;
			//console.log(Object.keys(rp).length);
            var k= Object.keys(rp['data'][0]['Tabs']).length;
            var vc_window = rp["data"][0];
            var tabs = vc_window.Tabs;

            if (vc_window.window_type == "MasterDetail"){
                col.push({
                    id: "subRow",
                    header: "&nbsp;",
                    template:"{common.subrow()}",
                    width:40
                });
            }

            var masterTab = tabs[0];

			for (var j = 0; j < masterTab.Fields.length; j++) {

                var field = masterTab.Fields[j];

				if(field.hide_in_grid==true){
					continue;
				}			
				
				var item = {
					id: field.column_name, 
                    header: _(field.column_name) ,
                    columnType: field.column_type					
                };
                
                if (field.caption!=null){
                    item.header = _(field.caption);
                }
				
				if (field.column_type == "decimal" ){
					item.css = { "text-align" : "right" };
					item.format = webix.Number.numToStr({
                            groupDelimiter:",",
                            groupSize:3,
                            decimalDelimiter:".",
                            decimalSize:0
                        });
                }
                    
                if (field.type_filter!=null){
                    if (field.type_filter!=""){
                        item.header = [_(field.column_name), { content: field.type_filter}]
                    }
                }
				
				if (field.type_filter == "combo"){
					item.editor = field.type_editor;
					item.options = app.config.host + "/api/System/GetDataByReferencesId?id=" + field.ref_id;
                }
                
                if (field.column_type == "date" || field.column_type == "datetime"){
                    item.format = function(text) {
                        var format = webix.Date.dateToStr("%d/%m/%Y");
                        text = removeTimeZoneT(text);                            
                        return format(text);
                    };
                }
				
				if (field.column_width == null){
					item.fillspace = true;
				}
				else {
					item.width = field.column_width;
				}
				
				col.push(item);
			}
			
			var table = $$("windowData");
			table.clearAll();			
            
            if (vc_window.window_type == "MasterDetail"){               

                var cells = [];

                for(var i=0;i<tabs.length;i++){
                    var tab = tabs[i];

                    if (tab.tab_type == "Master"){
                        continue;
                    }

                    var subCols = [];

                    for(var j=0;j<tab.Fields.length;j++){
                        var field = tab.Fields[j];

                        if (field.hide_in_grid == true){
                            continue;
                        }

                        var subCol = {
                            id: field.column_name, 
                            header: field.caption == null ? _(field.column_name) : _(field.caption),
                            hidden: field.hide_in_grid
                        };

                        if (field.column_type == "date" || field.column_type == "datetime"){
                            subCol.format = function(text) {
                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                text = removeTimeZoneT(text);                            
                                return format(text);
                            };
                        }

                        if (field.column_type == "decimal"){
							subCol.css = { "text-align": "right" };
							subCol.format = webix.Number.numToStr({
												groupDelimiter:",",
												groupSize:3,
												decimalDelimiter:".",
												decimalSize:0
											});
						}
                        
                        if (field.column_width == null){
                            subCol.fillspace = true;
                        }
                        else {
                            subCol.width = field.column_width;
                        }
                        
                        subCols.push(subCol);
                    }

                    if (tab.tab_type=="Detail"){
                        var item = {
                            header: tab.tab_name,
                            select: "cell",
                            body: {
                                view: "datatable",
                                tab_table: tab.tab_table,
                                columns: subCols,
                                on:{
                                    onBeforeLoad:function(){
                                        this.showOverlay(_("LOADING")+"...");
                                    },
                                    onAfterLoad:function(){
                                        this.hideOverlay();
                                    }
                                }
                            }
                        };

                        cells.push(item);
                    }
                }

                var subview = {
                    borderless:true,
                    view:"tabview",
                    height:200,
                    scrollX:true,
                    cells:cells,
                    multiview: {
                        fitBiggest:true
                    }
                };

                table.define("subview",subview);                
            }

            table.define("windowno", windowno);
            
            table.config.infoparam = null ;
            table.config.columns = col;
            table.config.ma_ct = vc_window.ma_ct;

            table.refreshColumns();	

            var parentToolBar = $$("tlbMain").getParentView();
            parentToolBar.removeView("tlbParams");

            if (vc_window.WinParams !== undefined){

                var toolBarParams = {
                    id:"tlbParams",
                    view: "toolbar",
                    height: 35,
                    cols:[]
                }
                
                for(var i=0;i<vc_window.WinParams.length;i++){
                    var item=vc_window.WinParams[i];

                    var comp = {
                        view: item.type_editor,
                        width: item.width,
                        label: _(item.caption),
                        value_field: item.value_field,
                        on:{}
                    };

                    if (comp.view == "combo"){
                        comp.options = app.config.host + "/System/GetDataByReferencesId?id=" + item.ref_id;
                        
					    comp.on.onChange = function(newv,oldv){
							var table = $$("windowData");                            
                            table.clearAll();
                            table.load(table.config.url);
						}
				
                    }

                    toolBarParams.cols.push(comp);

                }

                parentToolBar.addView(toolBarParams);
            }
            
            var url = {
                    $proxy:true,
                    load:function(view, callback, params){                        
                        var windowno = app.path[1].params[0];
                        var url = app.config.host + "/System/GetDataByWindowNo?window_id="+windowno;                        

                        if (params==null){
                            url = url + "&start=0&count="+view.config.datafetch + "&continue=null&filter=null" ;                               
                        }
                        else {
                            url = url + "&start="+params.start+"&count="+params.count + "&continue=" + params.continue;
                            
                            if (params.filter!=null){
                                var array=[];

                                for(var property in params.filter){

                                    var column = view.getColumnConfig(property);

                                    if (column !== undefined){
                                         var item = {
                                            columnName : property,
                                            columnType : column.columnType,
                                            value: params.filter[property]
                                        };

                                        array.push(item);
                                    }
                                    
                                   
                                }

                                //console.log(JSON.stringify(array));
                                if (array.length>0){
                                    url = url + "&filter=" +JSON.stringify(array);
                                }
                                else {
                                    url = url + "&filter=null";
                                }
                                
                            }
                            else {
                                 url = url + "&filter=null";
                            }                                
                        }

                        if (view.config.infoparam!=null){
                            url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
                        }
                        else {
                            url = url + "&infoparam=null";
                        }

                        var tlbParams = $$("tlbParams");

                        if (tlbParams!== undefined){
                            var childViews = tlbParams.getChildViews();

                            var parameters = [];

                            for(var i=0;i< childViews.length;i++){
                                var child = childViews[i];

                                var id = child.config.id;
                                var value_field = child.config.value_field;

                                var item = {
                                    columnName: value_field,
                                    value: $$(id).getValue()
                                };
                               
                                parameters.push(item);
                            }

                            url = url + "&tlbparam=" + JSON.stringify(parameters);

                        }
                        else {
                            url = url + "&tlbparam=null" ;
                        }
                        
                        console.log(url);

                        webix.ajax(url, callback, view);
                        
                    }
            };
            
            table.define("url", url );

            if (vc_window.vc_infowindow_id!=null){
                if (vc_window.vc_infowindow_id!=""){
                    $$("btnSearch").show();
                    table.config.infowindow_id = vc_window.vc_infowindow_id;
                }
                else {
                    $$("btnSearch").hide();
                    table.config.infowindow_id = null;
                }
            }
            else {
                $$("btnSearch").hide();
                table.config.infowindow_id = null;
            }

            
            

			webix.$$("windowData").getParentView().getParentView().getParentView().enable();
			webix.$$("windowData").getParentView().getParentView().getParentView().hideProgress();

		});
	};
	

    var controls = [
        { 
            id: "btnPlus",
            view: "button", 
            type: "icon", 
            icon:"plus", 
            label: "Thêm" ,
            width:65,
            shortcut: "F4",
            //css:"button_raised",
            on:{
                onClick: function(){
                    var vc_window = windowconfig["data"][0];
                    windowform.$ui.id = vc_window.window_id;

                    var detailForm = this.$scope.ui(windowform.$ui);            
                    
                    windowform.initUI(windowconfig);
                    windowform.setEditMode(1);

                    var params = null;

                    var tlbParams = $$("tlbParams");

                    if (tlbParams!== undefined){
                        var childViews = tlbParams.getChildViews();

                        var item = [];

                        for(var i=0;i< childViews.length;i++){
                            var child = childViews[i];

                            var id = child.config.id;
                            var value_field = child.config.value_field;

                            item[value_field] = $$(id).getValue();                            
                           
                        }

                        params = item;

                    }
                    

                    windowform.setValues(null,windowconfig,params);
                    
                    detailForm.show();
                    windowform.focusFirstElement();

                    if (vc_window.after_new!=null){
                        var fn = Function("app",vc_window.after_new);
                        fn(app);
                    }
                }
            },
            click: function(){
                this.callEvent("onClick");                
            }
        },
        { 
            id: "btnEdit",
            view: "button", 
            type: "icon", 
            icon:"edit", 
            label: "Sửa" ,
            width:55,
            shortcut: "F3",
            //css:"button_raised",
            on: {
                onClick: function(){
                    var vc_window = windowconfig["data"][0];
                    windowform.$ui.id = vc_window.window_id;

                    var detailForm = this.$scope.ui(windowform.$ui);
                
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();                
                    
                    windowform.initUI(windowconfig);
                    windowform.setEditMode(2);
                    windowform.setValues(item,windowconfig);
                    
                    detailForm.show();
                    windowform.focusFirstElement();
                }
            },
            click: function(){                
                this.callEvent("onClick");
            }
        },
        { 
            id: "btnDelete", 
            view: "button", 
            type: "icon", 
            icon:"trash-o", 
            label: "Xóa" ,
            width:55,
            shortcut: "F8",
            //css:"button_raised",
            on: {
                onClick: function(){
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    var khData={};
                    khData.windowid=windowconfig.data[0].window_id;
                    khData.editmode=3;
                    khData.data=[];
                    khData.data.push(item);
                    
                    webix.confirm({
                        text:"Bạn có muốn xóa ", ok:"Có", cancel:"Không",
                        callback:function(res){
                            if(res){
                                webix.ajax()
                                .headers({'Content-type':'application/json'})
                                .post( app.config.host + "/System/Save", khData , function(text){
                                    if (text.hasOwnProperty("error")){
                                        webix.message(text.error);
                                        return;
                                    }
                                    
                                    table.remove(item.id);

                                });
                                
                            }
                        }
                    });
                }
            },
            click: function(){               
                this.callEvent("onClick");
            }
        },
        { 
            id: "btnCopy", 
            view: "button", 
            type: "icon", 
            icon:"files-o", 
            label: _("COPY") ,
            width:85,
            on: {
                onClick: function(){
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    if (item == null){
                        webix.message("Bạn chưa chọn dòng dữ liệu");
                        return;
                    }

                    var vc_window = windowconfig["data"][0];
                    windowform.$ui.id = vc_window.window_id;

                    var detailForm = this.$scope.ui(windowform.$ui);            
                    
                    windowform.initUI(windowconfig);
                    windowform.setEditMode(1);
                    windowform.copyValues(item,windowconfig);
                    
                    detailForm.show();
                    windowform.focusFirstElement();

                    if (vc_window.after_new!=null){
                        var fn = Function(vc_window.after_new);
                        fn();
                    }

                    
                }
            },
            click: function(){               
                this.callEvent("onClick");
            }
        },
        { 
            id: "btnSearch", 
            view: "button", 
            type: "icon", 
            icon:"search", 
            label: _("SEARCH") ,
            width:85,
            shortcut: "F2",
            on: {
                onClick: function(){
                    var table = webix.$$("windowData");
                    
                    infowindow.$ui.id = table.config.infowindow_id;
                    infowindow.$ui.load_event = "filterTable";                      

                    var wininfoparam = this.$scope.ui(infowindow.$ui);

                    infowindow.initUI(table.config.infowindow_id);              
                    wininfoparam.show();
                }
            },
            click: function(){               
                this.callEvent("onClick");
            }
        },
        { 
            id: "btnPrint", 
            view: "button", 
            type: "icon", 
            icon:"print", 
            label: _("PRINT") ,
            width:40,
            shortcut: "F7",
            on: {
                onClick: function(){
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dữ liệu cần in");
                        return;
                    }

                    var cbMauIn = $$("cbMauIn");

                    var rpParameter = {
                        parameter : item,
                        CODE: cbMauIn.getValue()
                    };

                    var url = app.config.host + "/api/System/InChungTu" ;

                    webix.ajax()
                        .response("arraybuffer")
                        .headers({'Content-type':'application/json'})
                        .post(url, rpParameter, function(text, data){
                        var file = new Blob([data], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL,'_blank');
                    });
                    
                }
            },
            click: function(){               
                this.callEvent("onClick");
            }
        },
        {},
        {
            view:"richselect",
            label:_("MAUIN"),
            width:250,
            labelWidth:50,
            id:"cbMauIn"
        }
    ]    
    
    var grid = {
        rows:[
            {
                id : "windowData",
                view: "datatable", 
                select: true,
                datafetch: 50,
                columns: col ,
                on:{
                    onSubViewCreate:function(view, item){
                        var subView = view;

                        if (subView!=null){
                            var windowno = app.path[1].params[0];

                            var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id="+ windowno
                                    +"&id="+item.id;
                            
                            var detailTables = subView.getChildViews()[1].getChildViews();

                            for(var i=0;i<detailTables.length;i++){
                                var dt = detailTables[i];
                                dt.clearAll();
                                dt.load(url + "&tab_table="+dt.config.tab_table);
                            }                         
                        }

                    },
                    onSubViewOpen: function(id){
                        var table = this;
                        var subView = table.getSubView(id);
                        
                        if (subView!=null){
                            var windowno = app.path[1].params[0];

                            var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id="+ windowno
                                    +"&id="+id.row;
                            
                            var detailTables = subView.getChildViews()[1].getChildViews();

                            for(var i=0;i<detailTables.length;i++){
                                var dt = detailTables[i];
                                dt.clearAll();
                                dt.load(url + "&tab_table="+dt.config.tab_table);
                            }                         
                        }
                    },
                    onItemDblClick: function(id, e, node){
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    }
                },
                subview: {
                    borderless:true,
                    view:"tabview",
                    height:200,
                    scrollX:true,
                    tabbar:{
						width:200
					},
                    cells:[]
                },
                pager:"pagerA"                                        
            },
            {
                id: "pagerA",
                view:"pager",
                size: 50,
                group: 5,
                template:"{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}"
            }
        ]
    }

    var layout = {		
		rows:[
			{
                rows:[
                    {
                        id:"tlbMain",
                        height:35,
                        type: "toolbar",
                        cols:controls 
                    }                    
                ]                                                          
			},
			{
				rows:[
					grid					
				]
			}



		]

    };
    
    return {
        $ui: layout,
        $oninit: function(view,$scope) {
			
			webix.extend(view, webix.ProgressBar);
			
			
            $scope.on(app, "dataChanged", function(mode,item){
				
                var table = webix.$$("windowData");

                if (mode == 1){
                    table.add(item);
                    //table.select(item.id,false);
                }

                if (mode == 2){
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id,item);

                    webix.UIManager.setFocus($$(table.config.id));
                }
                
            });

            $scope.on(app, "filterTable", function(report_id,infoWindowId,item){				
                var table = $$("windowData");
 
                /* if (item.hasOwnProperty("p_MA_CT")){
                    item["p_MA_CT"] = table.config.ma_ct;
                } */

                var infoparam = {
                    infowindow_id : infoWindowId,
                    parameter : item                    
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);                
            });
        },
		$onurlchange:function(ui, name, url, scope){
			initUI();
		}
	};
});