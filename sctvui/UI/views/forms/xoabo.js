define(["app", "locale", "models/user", "views/modules/minvoiceplugin"], function (app, _, user, plugin) {
    var editmode = 0;

    function signOnServer(data, cert, server_cert) {

        var parameters = {
            xml: data["InvoiceXmlData"],
            refUri: "#data",
            certSerial: cert.so_serial,
            tokenSerial: cert.tokenSerial,
            signId: "seller",
            signTag: "Signatures",
            pass: cert.pass,
            inv_InvoiceAuth_id: data["inv_InvoiceAuth_id"]
        };

        data.serialNumber = cert.so_serial;

        webix.ajax()
            .bind(data)
            .headers({ "Content-Type": "application/json" })
            .post(server_cert + "/Token/SignXml", parameters, function (text) {
                var json = JSON.parse(text);

                if (json.hasOwnProperty("error")) {
                    webix.message({ type: "error", text: json["error"] });

                    var form = webix.$$("chukyso-form");
                    form.enable();
                    form.hideProgress();
                }
                else {
                    var parameters = this;
                    parameters.InvoiceXmlData = json.xml;

                    webix.ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(app.config.host + "/Invoice/SaveInvoiceXoaBo", parameters, function (text) {
                            var result = JSON.parse(text);

                            var form = webix.$$("chukyso-form");
                            form.enable();
                            form.hideProgress();

                            if (result.hasOwnProperty("error")) {
                                webix.message({ type: "error", text: result["error"] });
                            }
                            else {
                                var table = webix.$$("windowData");
                                table.clearAll();
                                table.load(table.config.url);

                                $$("chukyso-win").close();
                            }

                        });

                }
            });
    }


    function signOnServerFile(data, cert, server_cert) {

        var parameters = {
            xml: data["InvoiceXmlData"],
            refUri: "#data",
            certSerial: cert.so_serial,
            tokenSerial: cert.tokenSerial,
            signId: "seller",
            signTag: "Signatures",
            pass: cert.pass,
            path: cert.path,
            inv_InvoiceAuth_id: data["inv_InvoiceAuth_id"]
        };

        data.serialNumber = cert.so_serial;

        webix.ajax()
            .bind(data)
            .headers({ "Content-Type": "application/json" })
            .post(server_cert + "/Token/SignXmlByFile", parameters, function (text) {
                var json = JSON.parse(text);

                if (json.hasOwnProperty("error")) {
                    webix.message({ type: "error", text: json["error"] });

                    var form = webix.$$("chukyso-form");
                    form.enable();
                    form.hideProgress();
                }
                else {
                    var parameters = this;
                    parameters.InvoiceXmlData = json.xml;

                    webix.ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(app.config.host + "/Invoice/SaveInvoiceXoaBo", parameters, function (text) {
                            var result = JSON.parse(text);

                            var form = webix.$$("chukyso-form");
                            form.enable();
                            form.hideProgress();

                            if (result.hasOwnProperty("error")) {
                                webix.message({ type: "error", text: result["error"] });
                            }
                            else {
                                var table = webix.$$("windowData");
                                table.clearAll();
                                table.load(table.config.url);

                                $$("chukyso-win").close();
                            }

                        });

                }
            });
    }

    function signOnCertFile(cert){
        
        var form = webix.$$("chukyso-form");

        var item = form.getValues();
        var table = webix.$$("windowData");
        var selectedItem = table.getSelectedItem();

        var postData={
            username:user.getCurrentUser(),
            branch_code:"",
            template_code:selectedItem.template_code,
            invoice_series:selectedItem.invoice_series,
            invoice_number:selectedItem.invoice_number,
            cer_serial:cert.so_serial,
            ngayvb:moment(item.ngayvb).format("YYYY-MM-DD"),
            sovb:item.sovb,
            ghi_chu:item.ghi_chu
        };

        webix.ajax()
            .headers({'Content-Type':'application/json'})
            .post(app.config.host + "/Invoice/DeleteByCertFile",postData,function(text){
                var res=JSON.parse(text);

                var form = webix.$$("chukyso-form");
                form.enable();
                form.hideProgress();

                if (res.hasOwnProperty("error")){
                    webix.message({type:"error",text:res.error});
                }
                else {
                    var table = webix.$$("windowData");
                    table.clearAll();
                    table.load(table.config.url);

                    $$("chukyso-win").close();
                }
            });
    }

    return {
        $ui: {
            view: "window", modal: true, id: "chukyso-win", position: "center",
            head: "Xoá bỏ hóa đơn",
            width: 400,
            body: {
                paddingY: 10,
                paddingX: 10,
                view: "form",
                id: "chukyso-form",
                complexData: true,
                margin: 0,
                elements: [
                    {
                        view: "text",
                        label: _("SOVB"),
                        labelPosition: "top",
                        name: "sovb",
                        required: true,
                        invalidMessage: "Số văn bản không được để trống"
                    },
                    {
                        view: "datepicker",
                        label: _("NGAYVB"),
                        labelPosition: "top",
                        name: "ngayvb",
                        format: "%d/%m/%Y",
                        required: true,
                        invalidMessage: "Ngày văn bản không được để trống"
                    },
                    {
                        view: "textarea",
                        name: "ghi_chu",
                        label: _("GHI_CHU"),
                        labelPosition: "top",
                        height: 150
                    },
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                shortcut: "F10", view: "button", label: "Lưu (F10)", type: "form", align: "center", width: 100,
                                on: {
                                    onClick: function () {
                                        var form = webix.$$("chukyso-form");

                                        if (form.validate() == false) return;

                                        var item = form.getValues();

                                        var table = webix.$$("windowData");
                                        var selectedItem = table.getSelectedItem();

                                        var a = webix.ajax()
                                            .headers({ "Content-Type": "application/json" })
                                            .post(app.config.host + "/Invoice/GetThongTinXoaBo", {
                                                id: selectedItem.id,
                                                sovb: item.sovb,
                                                ngayvb: item.ngayvb,
                                                ghi_chu: item.ghi_chu
                                            });

                                        var b = webix.ajax(app.config.host + "/Invoice/GetListCertificates");

                                        var c = webix.ajax()
                                            .headers({ "Content-Type": "application/json" })
                                            .post(app.config.host + "/System/ExecuteCommand", {
                                                command: "CM00019",
                                                parameter: {
                                                    ma_dvcs: ""
                                                }
                                            });

                                        webix.extend(form, webix.ProgressBar);
                                        form.disable();
                                        form.showProgress();

                                        webix.promise.all([a, b, c]).then(function (results) {
                                            var a_result = results[0].json();
                                            var listcerts = results[1].json();
                                            var c_result = results[2].json();

                                            var form = webix.$$("chukyso-form");

                                            if (listcerts.length == 0) {
                                                webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });

                                                form.enable();
                                                form.hideProgress();

                                                return;
                                            }

                                            if (a_result.hasOwnProperty("error")) {
                                                webix.message({ type: 'error', text: a_result.error });

                                                form.enable();
                                                form.hideProgress();

                                                return;

                                            }

                                            var kytaptrung = "";
                                            var server_cert = "";

                                            for (var i = 0; i < c_result.length; i++) {
                                                var item = c_result[i];

                                                if (item.ma == "KYTAPTRUNG") {
                                                    kytaptrung = item.gia_tri;
                                                }

                                                if (item.ma == "SERVER_CERT") {
                                                    server_cert = item.gia_tri;
                                                }
                                            }

                                            //console.log(listcerts.length);

                                            if (kytaptrung == "C") {
                                                if (listcerts.length == 1) {
                                                    signOnServer(a_result, listcerts[0], server_cert);
                                                }
                                                else {
                                                    var box = webix.modalbox({
                                                        title: "Chọn chứng thư số",
                                                        buttons: ["Nhận", "Hủy bỏ"],
                                                        text: "<div id='ctnCerts'></div>",
                                                        top: 100,
                                                        width: 750,
                                                        callback: function (result) {
                                                            switch (result) {
                                                                case "1":
                                                                    var form = webix.$$("chukyso-form");
                                                                    form.enable();
                                                                    form.hideProgress();

                                                                    webix.modalbox.hide();
                                                                    break;
                                                                case "0":
                                                                    var table = $$("tblCerts");
                                                                    var data = table.serialize();

                                                                    var cert = null;

                                                                    for (var i = 0; i < data.length; i++) {
                                                                        if (data[i].chon == true) {
                                                                            cert = data[i];
                                                                            break;
                                                                        }
                                                                    }

                                                                    if (cert == null) {
                                                                        webix.message("Bạn chưa chọn chứng thư số");
                                                                        return;
                                                                    }

                                                                    webix.modalbox.hide();                                                                    
                                                                    signOnServer(table.config.xmlData, cert, table.config.server_cert);

                                                                    break;
                                                            }
                                                        }
                                                    });

                                                    var popupText = document.getElementsByClassName("webix_popup_text")[0];
                                                    popupText.style.paddingTop = 0;

                                                    var tblMatHang = webix.ui({
                                                        container: "ctnCerts",
                                                        id: "tblCerts",
                                                        view: "datatable",
                                                        borderless: true,
                                                        xmlData: a_result,
                                                        server_cert: server_cert,
                                                        height: 350,
                                                        columns: [
                                                            {
                                                                id: "chon",
                                                                header: _("CHON"),
                                                                width: 65,
                                                                checkValue: true,
                                                                uncheckValue: false,
                                                                template: "{common.checkbox()}"
                                                            },
                                                            {
                                                                id: "cert_type",
                                                                header: _("Loại"),
                                                                width: 100,
                                                            },
                                                            {
                                                                id: "so_serial",
                                                                header: _("SO_SERIAL"),
                                                                width: 150
                                                            },
                                                            {
                                                                id: "subjectname",
                                                                header: _("SUBJECTNAME"),
                                                                width: 450
                                                            },
                                                            {
                                                                id: "ngaybatdau",
                                                                header: _("NGAY_BAT_DAU"),
                                                                width: 200
                                                            },
                                                            {
                                                                id: "ngayketthuc",
                                                                header: _("NGAY_KET_THUC"),
                                                                width: 200
                                                            },
                                                            {
                                                                id: "issuer",
                                                                header: _("DONVI"),
                                                                width: 450
                                                            },
                                                        ],
                                                        url: app.config.host + "/Invoice/GetListCertificates"
                                                    });
                                                }
                                            }
                                            else {
                                                if (kytaptrung == "FILE") {
                                                    var box = webix.modalbox({
                                                        title: "Chọn chứng thư số",
                                                        buttons: ["Nhận", "Hủy bỏ"],
                                                        text: "<div id='ctnCerts'></div>",
                                                        top: 100,
                                                        width: 750,
                                                        callback: function (result) {
                                                            switch (result) {
                                                                case "1":
                                                                    var form = webix.$$("chukyso-form");
                                                                    form.enable();
                                                                    form.hideProgress();

                                                                    webix.modalbox.hide();
                                                                    break;
                                                                case "0":
                                                                    var table = $$("tblCerts");
                                                                    var data = table.serialize();

                                                                    var cert = null;

                                                                    for (var i = 0; i < data.length; i++) {
                                                                        if (data[i].chon == true) {
                                                                            cert = data[i];
                                                                            break;
                                                                        }
                                                                    }

                                                                    if (cert == null) {
                                                                        webix.message("Bạn chưa chọn chứng thư số");
                                                                        return;
                                                                    }

                                                                    webix.modalbox.hide();
                                                                    signOnServerFile(table.config.xmlData, cert, table.config.server_cert);

                                                                    break;
                                                            }
                                                        }
                                                    });

                                                    var popupText = document.getElementsByClassName("webix_popup_text")[0];
                                                    popupText.style.paddingTop = 0;

                                                    var tblMatHang = webix.ui({
                                                        container: "ctnCerts",
                                                        id: "tblCerts",
                                                        view: "datatable",
                                                        borderless: true,
                                                        xmlData: a_result,
                                                        server_cert: server_cert,
                                                        height: 350,
                                                        columns: [
                                                            {
                                                                id: "chon",
                                                                header: _("CHON"),
                                                                width: 65,
                                                                checkValue: true,
                                                                uncheckValue: false,
                                                                template: "{common.checkbox()}"
                                                            },
                                                            {
                                                                id: "cert_type",
                                                                header: _("Loại"),
                                                                width: 100,
                                                            },
                                                            {
                                                                id: "so_serial",
                                                                header: _("SO_SERIAL"),
                                                                width: 150
                                                            },
                                                            {
                                                                id: "subjectname",
                                                                header: _("SUBJECTNAME"),
                                                                width: 450
                                                            },
                                                            {
                                                                id: "ngaybatdau",
                                                                header: _("NGAY_BAT_DAU"),
                                                                width: 200
                                                            },
                                                            {
                                                                id: "ngayketthuc",
                                                                header: _("NGAY_KET_THUC"),
                                                                width: 200
                                                            },
                                                            {
                                                                id: "issuer",
                                                                header: _("DONVI"),
                                                                width: 450
                                                            },
                                                        ],
                                                        url: app.config.host + "/Invoice/GetListCertificatesFile"
                                                    });
                                                }
                                                else {

                                                    if (listcerts.length == 1){
                                                        var cert = listcerts[0];

                                                        if (cert.cert_type == "FILE"){
                                                            signOnCertFile(cert);
                                                            return;
                                                        }
                                                    }
                                                    
                                                    console.log("Signature_Client");
                                                    //console.log(a_result);
                                                    var hub = plugin.hub();

                                                    hub.off('resCommand');
                                                    hub.on('resCommand', function (result) {
                                                        var json = JSON.parse(result);
                                                        console.log(json);
                                                        if (json.hasOwnProperty("error")) {
                                                            webix.message({ type: 'error', text: json.error });

                                                            var form = webix.$$("chukyso-form");
                                                            form.enable();
                                                            form.hideProgress();

                                                        }
                                                        else {

                                                            var callback = json.callback;

                                                            if (callback == "SignCancelInvoice") {
                                                                var parameters = json.data;

                                                                webix.ajax()
                                                                    .headers({ "Content-Type": "application/json" })
                                                                    .post(app.config.host + "/Invoice/SaveInvoiceXoaBo", parameters, function (text) {
                                                                        var result = JSON.parse(text);

                                                                        var form = webix.$$("chukyso-form");
                                                                        form.enable();
                                                                        form.hideProgress();

                                                                        if (result.hasOwnProperty("error")) {
                                                                            webix.message({ type: "error", text: result["error"] });
                                                                        }
                                                                        else {
                                                                            var table = webix.$$("windowData");
                                                                            table.clearAll();
                                                                            table.load(table.config.url);

                                                                            $$("chukyso-win").close();
                                                                        }
                                                                    });
                                                            }
                                                        }
                                                    });

                                                    var arg = {
                                                        callback: "SignCancelInvoice",
                                                        idData: "#data",
                                                        data: a_result,
                                                        idSigner: "seller",
                                                        tagSign: "Signatures",
                                                        listcerts: listcerts
                                                    };


                                                    hub.invoke('execCommand', arg.callback, JSON.stringify(arg))
                                                        .done(function () {
                                                            console.log("Success done !");
                                                            //console.log("2");
                                                        })
                                                        .fail(function (error) {
                                                            webix.message(error, "error");
                                                        });
                                                }
                                            }

                                        }).fail(function (err) {
                                            alert(err);

                                            var form = webix.$$("chukyso-form");
                                            form.enable();
                                            form.hideProgress();
                                        });

                                    }
                                },
                                click: function () {
                                    this.callEvent("onClick");

                                }
                            },
                            {
                                view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 100,
                                click: function () {
                                    webix.$$("chukyso-win").close();
                                }
                            },
                            {}
                        ]
                    }

                ]
            }

        },
        setEditMode: function (mode) {
            editmode = mode;
        }
    };

});