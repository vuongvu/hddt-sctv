define(["app", "locale", "models/user", "views/modules/minvoiceplugin"], function (app, _, user, plugin) {
    "use strict";

    webix.$$("title").parse({ title: "Đăng ký sử dụng", details: "Đăng ký sử dụng" });
    var editmode = 0;

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            css: "headformchil",
            head: {
                //view: "toolbar",
                // css: { "box-shadow": "0 -2px #3498db inset !important", "background": "#ffffff !important" },
                //css: { "background": "#e6f2ff !important" },
                height: 35,
                cols: [
                    {
                        id: "head-icondksd",
                        view: "template",
                        css: "headform",
                        width: 35,
                        template: "",
                    },
                    {
                        id: "head-formdksd",
                        view: "template",
                        css: "headform",
                        template: function (obj) {
                            return "<div>Đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử<div/>";
                        },
                    },
                    {
                        view: "icon",
                        icon: "fas fa-question-circle",
                        id: "help_dksdForms",
                        tooltip: _("HELPER"),
                        click: function () {
                            // var form = webix.$$("window-form");
                            // hint_form(form.config.hint);
                        },
                    },
                ],
            },
            id: "dangkysudungForms",
            resize: true,
            fullscreen: true,
            move: true,
            on: {
                onBeforeShow: function () {},
            },
            body: {
                view: "form",
                id: "dangkysudung",
                // width: 1500,
                position: "center",
                // autowidth:true,
                paddingY: 10,
                paddingX: 15,
                complexData: true,
                scroll: true,
                margin: 0,
                elements: [
                    {
                        cols: [
                            {
                                view: "radio",
                                id: "loai",
                                name: "loai",
                                width: 200,
                                value: 1,
                                options: [
                                    { id: 1, value: "Đăng ký" }, // the initially selected item
                                    { id: 2, value: "Thay đổi" },
                                ],
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == 1) {
                                            $$("header").define("template", "<center><b><span style='font-size:17px;'>TỜ KHAI</span> </br> THAY ĐỔI THÔNG TIN SỬ DỤNG HÓA ĐƠN ĐIỆN TỬ</b></center>");
                                            $$("header").refresh();
                                        } else {
                                            $$("header").define("template", "<center><b><span style='font-size:17px;'>TỜ KHAI</span> </br> ĐĂNG KÝ THÔNG TIN SỬ DỤNG HÓA ĐƠN ĐIỆN TỬ</b></center>");
                                            $$("header").refresh();
                                        }
                                    },
                                },
                            },
                            {
                                id: "header",
                                template: "<center><b><span style='font-size:17px;'>TỜ KHAI</span> </br>ĐĂNG KÝ THÔNG TIN SỬ DỤNG HÓA ĐƠN ĐIỆN TỬ</b></center>",
                                borderless: true,
                                height: 40,
                            },
                        ],
                    },
                    {
                        paddingX: 40,
                        rows: [
                            {
                                view: "text",
                                label: "Tên người nộp thuế: ",
                                labelWidth: 150,
                                name: "tnnt",
                                id: "tnnt",
                                required: true,
                                invalidMessage: "Tên người nộp thuế không được để trống !",
                                css: "mcls_uppercase",
                            },
                            {
                                view: "search",
                                label: "Mã số thuế",
                                id: "mst",
                                name: "mst",
                                labelWidth: 150,
                                required: true,
                                invalidMessage: "Mã số thuế không được để trống !",
                                on: {
                                    onSearchIconClick: function () {
                                        var mst = this.getValue();
                                        window.open("http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst);
                                    },
                                },
                            },
                            {
                                cols: [
                                    {
                                        id: "cqtqly",
                                        view: "combo",
                                        name: "cqtqly",
                                        label: "Cơ quan thuế quản lý: ",
                                        labelWidth: 150,
                                        options: {
                                            view: "gridsuggest",
                                            columnName: "name",
                                            url: {
                                                $proxy: true,
                                                load: function (view, callback, params) {
                                                    var url = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00251";
                                                    webix.ajax(url, function (text) {
                                                        var json = JSON.parse(text);
                                                        var data = listToTree(json);
                                                        view.parse(data, "json");
                                                    });
                                                },
                                            },
                                            body: {
                                                columns: [{ id: "name", width: 350, header: "name" }],
                                                scroll: true,
                                                autoheight: false,
                                                datatype: "json",
                                            },
                                            on: {
                                                onValueSuggest: function (obj) {
                                                    $$("mcqtqly").setValue(obj["code"]);
                                                    $$("mcqtqly").refresh();
                                                },
                                            },
                                        },
                                        required: true,
                                        invalidMessage: "Cơ quan thuế quản lý không được để trống !",
                                    },
                                    {
                                        cols: [
                                            {
                                                view: "text",
                                                label: "Mã CQT quản lý: ",
                                                labelWidth: 150,
                                                name: "mcqtqly",
                                                id: "mcqtqly",
                                                required: true,
                                                invalidMessage: "Mã cơ quan thuế quản lý không được để trống !",
                                                attributes: { maxlength: 5 },
                                                readonly: true,
                                            },
                                            {
                                                view: "text",
                                                label: "Địa danh: ",
                                                labelWidth: 80,
                                                name: "ddanh",
                                                id: "ddanh",
                                                required: true,
                                                invalidMessage: "Địa danh không được để trống !",
                                                attributes: { maxlength: 20 },
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        label: "Người liên hệ: ",
                                        labelWidth: 150,
                                        name: "nlhe",
                                        id: "nlhe",
                                        required: true,
                                        invalidMessage: "Người liên hệ không được để trống !",
                                    },
                                    {
                                        view: "text",
                                        label: "Điện thoại liên hệ: ",
                                        labelWidth: 150,
                                        name: "dtlhe",
                                        id: "dtlhe",
                                        required: true,
                                        invalidMessage: "Địa chỉ không được để trống !",
                                    },
                                ],
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        label: "Địa chỉ liên hệ: ",
                                        labelWidth: 150,
                                        name: "dclhe",
                                        id: "dclhe",
                                        required: true,
                                        invalidMessage: "Địa chỉ không được để trống !",
                                    },
                                    {
                                        view: "text",
                                        label: "Thư điện tử: ",
                                        labelWidth: 150,
                                        name: "dctdtu",
                                        id: "dctdtu",
                                        validate: webix.rules.isEmail,
                                        required: true,
                                        invalidMessage: "Thư điện tử không hợp lệ !",
                                    },
                                ],
                            },

                            { height: 5 },
                        ],
                    },
                    {
                        rows: [
                            {
                                cols: [
                                    {
                                        view: "label",
                                        label: "Theo nghị định số: 123/2020/NĐ-CP ngày 19 tháng 10 năm 2020 của Chính phủ, chúng tôi/tôi thuộc đối tượng sử dụng hóa đơn điện tử. Chúng tôi/tôi đăng ký/thay đổi thông tin đã đăng ký với cơ quan thuế về việc sử dụng hóa đơn điện tử như sau:",
                                    },
                                ],
                            },
                            { view: "label", label: "<b>1. Áp dụng hóa đơn điện tử:</b>" },
                            {
                                view: "checkbox",
                                id: "adhdcma",
                                name: "adhdcma",
                                labelRight: "Có mã của cơ quan thuế",
                                labelWidth: 0,
                                on: {
                                    onChange: function (newv, oldv) {
                                        // if (newv == 1) {
                                        //     $$("adhdcma").setValue(1);
                                        //     $$("adhdkma").setValue(0);
                                        // }
                                    },
                                },
                            },
                            {
                                view: "checkbox",
                                id: "adhdkma",
                                name: "adhdkma",
                                value: 1,
                                labelRight: "Không có mã của cơ quan thuế",
                                labelWidth: 0,
                                on: {
                                    onChange: function (newv, oldv) {
                                        // if (newv == 1) {
                                        //     $$("adhdcma").setValue(0);
                                        //     $$("adhdkma").setValue(1);
                                        // }
                                    },
                                },
                            },
                            { view: "label", label: "<b>2. Hình thức gửi dữ liệu hóa đơn điện tử:</b>" },
                            {
                                view: "checkbox",
                                id: "htgdlhddtcma",
                                name: "htgdlhddtcma",
                                labelRight: "Trường hợp sử dụng hóa đơn điện tử có mã không phải trả tiền dịch vụ theo khoản 1 Điều 14 của nghị định:",
                                label: "a. ",
                                labelWidth: 10,
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == 1) {
                                            $$("htgdlhddtkma").setValue(0);
                                            $$("htgdlhddtcma").setValue(1);
                                            $$("htgdlhddtcmkkhan").setValue(1);
                                            $$("htgdlhddtcmkcncao").setValue(0);
                                            $$("htgdlhddtkmttiep").setValue(0);
                                            $$("htgdlhddtkmtchuc").setValue(0);
                                        }
                                    },
                                },
                            },
                            {
                                view: "checkbox",
                                id: "htgdlhddtcmkkhan",
                                name: "htgdlhddtcmkkhan",
                                labelRight:
                                    "Doanh nghiệp nhỏ và vừa, hợp tác xã, cá nhân kinh doanh tại địa bàn có điều kiện kinh tế xã hội khó khăn, địa bàn có điều kiện kinh tế xã hội đặc biệt khó khăn.",
                                labelWidth: 50,
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == 1) {
                                            $$("htgdlhddtkma").setValue(0);
                                            $$("htgdlhddtcma").setValue(1);
                                            $$("htgdlhddtcmkcncao").setValue(0);
                                            $$("htgdlhddtcmkkhan").setValue(1);
                                        }
                                    },
                                },
                            },
                            {
                                view: "checkbox",
                                id: "htgdlhddtcmkcncao",
                                name: "htgdlhddtcmkcncao",
                                labelRight:
                                    "Doanh nghiệp nhỏ và vừa khác theo đề nghị của Ủy ban nhân dân tỉnh, thành phố trực thuộc trung ương gửi Bộ Tài chính trừ doanh nghiệp hoạt động tại các khu vực kinh tế, khu công nghiệp, khu công nghệ cao.",
                                labelWidth: 50,
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == 1) {
                                            $$("htgdlhddtkma").setValue(0);
                                            $$("htgdlhddtcma").setValue(1);
                                            $$("htgdlhddtcmkcncao").setValue(1);
                                            $$("htgdlhddtcmkkhan").setValue(0);
                                        }
                                    },
                                },
                            },
                            {
                                view: "checkbox",
                                id: "htgdlhddtkma",
                                name: "htgdlhddtkma",
                                labelRight:
                                    "Trường hợp sử dụng hóa đơn điện tử không có mã của cơ quan thuế: <I><B>(Chú ý: Trường hợp sử dụng hóa đơn có mã của cơ quan thuế không cần chọn mục này, hệ thống của cơ quan thuế mặc định hiểu gửi thông tin qua tổ chức cung cấp dịch vụ) </B></i>",
                                label: "b. ",
                                labelWidth: 10,
                                value: 1,
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == 1) {
                                            $$("htgdlhddtkma").setValue(1);
                                            $$("htgdlhddtcma").setValue(0);
                                            $$("htgdlhddtcmkkhan").setValue(0);
                                            $$("htgdlhddtcmkcncao").setValue(0);
                                            $$("htgdlhddtkmttiep").setValue(1);
                                            $$("htgdlhddtkmtchuc").setValue(0);
                                        }
                                    },
                                },
                            },
                            {
                                view: "checkbox",
                                id: "htgdlhddtkmttiep",
                                name: "htgdlhddtkmttiep",
                                labelRight: "Chuyển dữ liệu hóa đơn điện tử trực tiếp đến cơ quan thuế (điểm b1, khoản 3, Điều 22 của Nghị định).",
                                labelWidth: 50,
                                value: 1,
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == 1) {
                                            $$("htgdlhddtkma").setValue(1);
                                            $$("htgdlhddtcma").setValue(0);
                                            $$("htgdlhddtkmtchuc").setValue(0);
                                            $$("htgdlhddtkmttiep").setValue(1);
                                        }
                                    },
                                },
                            },
                            {
                                view: "checkbox",
                                id: "htgdlhddtkmtchuc",
                                name: "htgdlhddtkmtchuc",
                                labelRight: "Thông qua tổ chức cung cấp dịch vụ hóa đơn điện tử (điểm b2, khoản 3, Điều 22 của Nghị định).",
                                labelWidth: 50,
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == 1) {
                                            $$("htgdlhddtkma").setValue(1);
                                            $$("htgdlhddtcma").setValue(0);
                                            $$("htgdlhddtkmtchuc").setValue(1);
                                            $$("htgdlhddtkmttiep").setValue(0);
                                        }
                                    },
                                },
                            },

                            { view: "label", label: "<b>3. Phương thức chuyển dữ liệu hóa đơn điện tử:</b>" },
                            {
                                view: "checkbox",
                                id: "ptcdlhdndthdon",
                                name: "ptcdlhdndthdon",
                                labelRight: "Chuyển đầy đủ nội dung từng hóa đơn",
                                value: 1,
                                labelWidth: 50,
                                // on: {
                                //     onChange: function (newv, oldv) {
                                //         if (newv == 1) {
                                //             $$("ptcdlhdndthdon").setValue(1);
                                //             $$("ptcdlhdbthop").setValue(0);
                                //         }
                                //     },
                                // },
                            },
                            {
                                view: "checkbox",
                                id: "ptcdlhdbthop",
                                name: "ptcdlhdbthop",
                                labelRight: "Chuyển theo bảng tổng hợp dữ liệu hóa đơn điện tử (điểm a1, khoản 3, Điều 22 của Nghị định)",
                                labelWidth: 50,
                                // on: {
                                //     onChange: function (newv, oldv) {
                                //         if (newv == 1) {
                                //             $$("ptcdlhdndthdon").setValue(0);
                                //             $$("ptcdlhdbthop").setValue(1);
                                //         }
                                //     },
                                // },
                            },

                            { view: "label", label: "<b>4. Loại hóa đơn sử dụng:</b>" },
                            {
                                view: "checkbox",
                                id: "hdgtgt",
                                name: "hdgtgt",
                                labelRight: "Hóa đơn GTGT",
                                value: 1,
                                labelWidth: 50,
                            },
                            {
                                view: "checkbox",
                                id: "hdbhang",
                                name: "hdbhang",
                                labelRight: "Hóa đơn bán hàng",
                                labelWidth: 50,
                            },
                            {
                                view: "checkbox",
                                id: "hdbtscong",
                                name: "hdbtscong",
                                labelRight: "Hóa đơn bán tài sản công",
                                labelWidth: 50,
                            },
                            {
                                view: "checkbox",
                                id: "hdbhdtqgia",
                                name: "hdbhdtqgia",
                                labelRight: "Hóa đơn bán hàng dự trữ quốc gia",
                                labelWidth: 50,
                            },
                            {
                                view: "checkbox",
                                id: "hdkhac",
                                name: "hdkhac",
                                labelRight: "Hóa đơn khác (Tem, vé, thẻ, phiếu....)",
                                labelWidth: 50,
                            },
                            {
                                view: "checkbox",
                                id: "qlnhdon",
                                name: "qlnhdon",
                                labelRight: "Các chứng từ được in, phát hành, sử dụng và quản lý như hóa đơn",
                                labelWidth: 50,
                            },
                        ],
                    },
                    {
                        rows: [
                            {
                                view: "label",
                                label: "<b>5. Danh sách chứng thư số sử dụng:</b>",
                            },
                            {
                                cols: [
                                    {
                                        view: "button",
                                        type: "icon",
                                        //view: "icon",
                                        label: " Thêm",
                                        // width: 60,
                                        autowidth: true,
                                        icon: "fas fa-plus",
                                        on: {
                                            onItemClick: function (id, e) {
                                                webix
                                                    .ajax()
                                                    .headers({ "Content-Type": "application/json" })
                                                    .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                                                        var result = JSON.parse(text);
                                                        var kytaptrung = "";
                                                        for (var i = 0; i < result.length; i++) {
                                                            var item = result[i];
                                                            kytaptrung = item.value;
                                                        }
                                                        webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                                            var listcerts = JSON.parse(text);
                                                            if (listcerts.length > 0) {
                                                                var box = webix.modalbox({
                                                                    title: "Chọn chứng thư số",
                                                                    buttons: ["Nhận", "Hủy bỏ"],
                                                                    text: "<div id='ctnCerts'></div>",
                                                                    top: 100,
                                                                    width: 750,
                                                                    callback: function (result) {
                                                                        debugger;
                                                                        switch (result) {
                                                                            case "1":
                                                                                webix.modalbox.hide();
                                                                                break;
                                                                            case "0":
                                                                                var table = $$("tblCerts");
                                                                                var data = table.serialize();

                                                                                var cert = null;

                                                                                for (var i = 0; i < data.length; i++) {
                                                                                    if (data[i].chon == true) {
                                                                                        cert = data[i];
                                                                                        break;
                                                                                    }
                                                                                }

                                                                                if (cert == null) {
                                                                                    webix.message("Bạn chưa chọn chứng thư số");
                                                                                    return;
                                                                                }

                                                                                var item = {
                                                                                    stt: $$("danhsach_chukyso").count() + 1,
                                                                                    ttchuc: cert.subject_name,
                                                                                    seri: cert.cer_serial,
                                                                                    tngay: cert.begin_date,
                                                                                    dngay: cert.end_date,
                                                                                    hthuc: "1",
                                                                                };

                                                                                // $$("danhsach_chukyso").clearAll();
                                                                                $$("danhsach_chukyso").add(item);
                                                                                return;
                                                                        }
                                                                    },
                                                                });

                                                                var tblMatHang = webix.ui({
                                                                    container: "ctnCerts",
                                                                    id: "tblCerts",
                                                                    view: "datatable",
                                                                    borderless: true,
                                                                    resizeColumn: true,

                                                                    height: 350,
                                                                    columns: [
                                                                        {
                                                                            id: "chon",
                                                                            header: _("CHON"),
                                                                            width: 65,
                                                                            checkValue: true,
                                                                            uncheckValue: false,
                                                                            template: "{common.checkbox()}",
                                                                        },
                                                                        {
                                                                            id: "cert_type",
                                                                            header: _("Loại"),
                                                                            width: 100,
                                                                        },
                                                                        {
                                                                            id: "cer_serial",
                                                                            header: _("SO_SERIAL"),
                                                                            width: 150,
                                                                        },
                                                                        {
                                                                            id: "subject_name",
                                                                            header: _("SUBJECTNAME"),
                                                                            width: 450,
                                                                        },
                                                                        {
                                                                            id: "begin_date",
                                                                            header: _("NGAY_BAT_DAU"),
                                                                            width: 200,
                                                                        },
                                                                        {
                                                                            id: "end_date",
                                                                            header: _("NGAY_KET_THUC"),
                                                                            width: 200,
                                                                        },
                                                                        {
                                                                            id: "issuer",
                                                                            header: _("DONVI"),
                                                                            width: 450,
                                                                        },
                                                                    ],
                                                                    url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                                                });
                                                            } else {
                                                                // xem user có tích checkbox ký HSM không?
                                                                var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                                                                webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                                                    if (resultscheckSigHSM[0].json() == "C") {
                                                                        webix.ajax().get(app.config.host + "/Invoice68/GetCTS_HSM_ToKhai", function (result) {
                                                                            var ctsHSM = JSON.parse(result);

                                                                            if (ctsHSM.hasOwnProperty("error")) {
                                                                                webix.message({ type: "error", text: ctsHSM.error });
                                                                            } else {
                                                                                var item = {
                                                                                    stt: $$("danhsach_chukyso").count() + 1,
                                                                                    ttchuc: ctsHSM.subjectname,
                                                                                    seri: ctsHSM.so_serial,
                                                                                    tngay: ctsHSM.ngaybatdau,
                                                                                    dngay: ctsHSM.ngayketthuc,
                                                                                    hthuc: "1",
                                                                                };

                                                                                // $$("danhsach_chukyso").clearAll();
                                                                                $$("danhsach_chukyso").add(item);
                                                                            }
                                                                        });
                                                                    } else {
                                                                        var hub = plugin.hub();

                                                                        hub.off("resCommand");
                                                                        hub.on("resCommand", function (result) {
                                                                            var json = JSON.parse(result);

                                                                            if (json.hasOwnProperty("error")) {
                                                                                webix.message({ type: "error", text: json.error });
                                                                            } else {
                                                                                var callback = json.callback;

                                                                                if (callback == "RegisterCert") {
                                                                                    var item = {
                                                                                        stt: $$("danhsach_chukyso").count() + 1,
                                                                                        ttchuc: json.data.subjectname,
                                                                                        seri: json.data.so_serial,
                                                                                        tngay: json.data.ngaybatdau,
                                                                                        dngay: json.data.ngayketthuc,
                                                                                        hthuc: "1",
                                                                                    };

                                                                                    // $$("danhsach_chukyso").clearAll();
                                                                                    $$("danhsach_chukyso").add(item);
                                                                                }
                                                                            }
                                                                        });

                                                                        var arg = {
                                                                            callback: "RegisterCert",
                                                                        };

                                                                        hub.invoke("execCommand", arg.callback, JSON.stringify(arg));
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                            },
                                        },
                                    },
                                    {
                                        view: "button",
                                        type: "icon",
                                        //view: "icon",
                                        autowidth: true,
                                        label: " Xóa",
                                        // width: 60,
                                        icon: "fas fa-minus",
                                        on: {
                                            onItemClick: function (id, e) {
                                                var table = webix.$$("danhsach_chukyso");
                                                table.editStop();
                                                var item = table.getSelectedItem();
                                                if (!webix.isUndefined(item)) {
                                                    table.remove(item.id);
                                                    table.clearSelection();
                                                    return;
                                                }
                                                var editor = table.getEditState();
                                                if (!webix.isUndefined(editor)) {
                                                    table.remove(editor.row);
                                                    table.clearSelection();
                                                    return;
                                                }
                                            },
                                        },
                                    },
                                    {},
                                ],
                            },
                            {
                                view: "datatable",
                                id: "danhsach_chukyso",
                                height: 250,
                                name: "danhsach_chukyso",
                                select: true,
                                resizeColumn: true,
                                footer: false,
                                tooltip: true,
                                editable: true,
                                scroll: true,
                                fullscreen: true,
                                scheme: {
                                    $init: function (obj) {},
                                },
                                on: {},
                                columns: [
                                    {
                                        id: "stt",
                                        name: "stt",
                                        header: "STT",
                                        sort: "int",
                                        width: 40,
                                    },
                                    {
                                        id: "ttchuc",
                                        name: "ttchuc",
                                        header: "Tên tổ chức cơ quan chứng thực/cấp/công nhận chữ ký số, chữ ký điện tử",
                                        width: 450,
                                    },
                                    {
                                        id: "seri",
                                        name: "seri",
                                        header: "Số sê-ri chứng thư số",
                                        width: 300,
                                    },
                                    {
                                        id: "tngay",
                                        name: "tngay",
                                        header: [
                                            {
                                                text: "Thời hạn sử dụng chứng thư số",
                                                colspan: 2,
                                            },
                                            {
                                                text: "Từ ngày",
                                            },
                                        ],
                                        width: 200,
                                        //format: webix.i18n.dateFormatStr,
                                    },
                                    {
                                        id: "dngay",
                                        name: "dngay",
                                        header: [
                                            null,
                                            {
                                                text: "Đến ngày",
                                            },
                                        ],
                                        width: 200,
                                        //format: webix.i18n.dateFormatStr,
                                    },
                                    {
                                        id: "hthuc",
                                        name: "hthuc",
                                        header: "Hình thức đăng ký ( Thêm mới, Gia hạn, ngừng sử dụng)",
                                        fillspace: true,
                                        editor: "combo",
                                        options: [
                                            { id: "1", value: "Thêm mới" },
                                            { id: "2", value: "Gia hạn" },
                                            { id: "3", value: "Ngừng sử dụng" },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                    { height: 10 },
                    {
                        view: "combo",
                        label: "Loại đăng ký ủy nhiệm",
                        id: "ldkunhiem",
                        name: "ldkunhiem",
                        labelWidth: 150,
                        options: [
                            { id: 1, value: "Ủy nhiệm" },
                            { id: 2, value: "Nhận ủy nhiệm" },
                        ],
                        width: 400,
                    },
                    { height: 10 },
                    {
                        rows: [
                            {
                                view: "label",
                                label: "<b>6. Đăng ký ủy nhiệm lập hóa đơn:</b>",
                            },
                            {
                                cols: [
                                    {
                                        view: "button",
                                        type: "icon",
                                        //view: "icon",
                                        label: " Thêm",
                                        // width: 60,
                                        autowidth: true,
                                        icon: "fas fa-plus",
                                        on: {
                                            onItemClick: function (id, e) {
                                                var item = {
                                                    stt: $$("danhsach_uynhiem").count() + 1,
                                                    tlhdunhiem: "",
                                                    khmhdon: "",
                                                    khhdunhiem: "",
                                                    mstunhiem: "",
                                                    ttcdunhiem: "",
                                                    mdunhiem: "",
                                                    thunhiem: "",
                                                    tngay: new Date(),
                                                    dngay: new Date(),
                                                    pttthdunhiem: 1,
                                                    ket_qua: 0,
                                                };
                                                // $$("danhsach_uynhiem").clearAll();
                                                $$("danhsach_uynhiem").add(item);

                                                $$("ldkunhiem").define("required", true);
                                                $$("ldkunhiem").define("invalidMessage", "Loại đăng ký ủy nhiệm không được để trống !");
                                                $$("ldkunhiem").refresh();

                                                return;
                                            },
                                        },
                                    },
                                    {
                                        view: "button",
                                        type: "icon",
                                        //view: "icon",
                                        autowidth: true,
                                        label: " Xóa",
                                        // width: 60,
                                        icon: "fas fa-minus",
                                        on: {
                                            onItemClick: function (id, e) {
                                                var table = webix.$$("danhsach_uynhiem");
                                                table.editStop();
                                                var item = table.getSelectedItem();
                                                if (!webix.isUndefined(item)) {
                                                    table.remove(item.id);
                                                    table.clearSelection();

                                                    if (table.count() == 0) {
                                                        $$("ldkunhiem").define("required", false);
                                                        $$("ldkunhiem").refresh();
                                                    } else {
                                                        $$("ldkunhiem").define("required", true);
                                                        $$("ldkunhiem").define("invalidMessage", "Loại đăng ký ủy nhiệm không được để trống !");
                                                        $$("ldkunhiem").refresh();
                                                    }

                                                    return;
                                                }
                                                var editor = table.getEditState();
                                                if (!webix.isUndefined(editor)) {
                                                    table.remove(editor.row);
                                                    table.clearSelection();

                                                    if (table.count() == 0) {
                                                        $$("ldkunhiem").define("required", false);
                                                        $$("ldkunhiem").refresh();
                                                    } else {
                                                        $$("ldkunhiem").define("required", true);
                                                        $$("ldkunhiem").define("invalidMessage", "Loại đăng ký ủy nhiệm không được để trống !");
                                                        $$("ldkunhiem").refresh();
                                                    }

                                                    return;
                                                }
                                            },
                                        },
                                    },
                                    {},
                                ],
                            },
                            {
                                view: "datatable",
                                id: "danhsach_uynhiem",
                                height: 250,
                                name: "danhsach_uynhiem",
                                select: true,
                                resizeColumn: true,
                                footer: false,
                                tooltip: true,
                                editable: true,
                                scroll: true,
                                fullscreen: true,
                                scheme: {
                                    $init: function (obj) {
                                        obj.tngay = new Date(obj.tngay);
                                        obj.dngay = new Date(obj.dngay);
                                    },
                                },
                                on: {},
                                columns: [
                                    {
                                        id: "stt",
                                        name: "stt",
                                        header: "STT",
                                        sort: "int",
                                        width: 40,
                                    },
                                    {
                                        id: "tlhdunhiem",
                                        name: "tlhdunhiem",
                                        header: "Tên loại hóa đơn ủy nhiệm",
                                        width: 200,
                                        editor: "text",
                                    },
                                    {
                                        id: "khmhdon",
                                        name: "khmhdon",
                                        header: "Ký hiệu mẫu hóa đơn",
                                        width: 150,
                                        editor: "text",
                                    },
                                    {
                                        id: "khhdunhiem",
                                        name: "khhdunhiem",
                                        header: "Ký hiệu hóa đơn ủy nhiệm",
                                        width: 150,
                                        editor: "text",
                                    },
                                    {
                                        id: "mstunhiem",
                                        name: "mstunhiem",
                                        header: "Mã số thuế",
                                        width: 150,
                                        editor: "text",
                                    },
                                    {
                                        id: "ttcdunhiem",
                                        name: "ttcdunhiem",
                                        header: "Tên tổ chức được ủy nhiệm",
                                        width: 300,
                                        editor: "text",
                                    },
                                    {
                                        id: "mdunhiem",
                                        name: "mdunhiem",
                                        header: "Mục đích ủy nhiệm",
                                        width: 300,
                                        editor: "text",
                                    },
                                    {
                                        id: "tngay",
                                        name: "tngay",
                                        header: [
                                            {
                                                text: "Thời hạn sử dụng chứng thư số",
                                                colspan: 2,
                                            },
                                            {
                                                text: "Từ ngày",
                                            },
                                        ],
                                        width: 150,
                                        editor: "date",
                                        format: function (text) {
                                            var format = webix.Date.dateToStr("%d/%m/%Y");
                                            text = removeTimeZoneT(text);
                                            return format(text);
                                        },
                                    },
                                    {
                                        id: "dngay",
                                        name: "dngay",
                                        header: [
                                            null,
                                            {
                                                text: "Đến ngày",
                                            },
                                        ],
                                        width: 150,
                                        editor: "date",
                                        format: function (text) {
                                            var format = webix.Date.dateToStr("%d/%m/%Y");
                                            text = removeTimeZoneT(text);
                                            return format(text);
                                        },
                                    },
                                    {
                                        id: "pttthdunhiem",
                                        name: "pttthdunhiem",
                                        header: "PTTT hóa đơn ủy nhiệm",
                                        fillspace: true,
                                        editor: "text",
                                        //editor: "combo",
                                        // options: [
                                        //     { id: 1, value: "Tiền mặt" },
                                        //     { id: 2, value: "Chuyển khoản" },
                                        //     { id: 3, value: "Tiền mặt/Chuyển khoản" },
                                        //     { id: 4, value: "Đối trừ công nợ" },
                                        //     { id: 5, value: "Không thu tiền" },
                                        //     { id: 9, value: "Khác" },
                                        // ],
                                        width: 150,
                                    },
                                    {
                                        id: "ket_qua",
                                        name: "ket_qua",
                                        header: "Kết quả",
                                        fillspace: true,
                                        format: function (text) {
                                            var result = "";
                                            if (text == 0) {
                                                result = "Chờ phản hồi";
                                            } else if (text == 1) {
                                                result = "Chấp nhận";
                                            } else if (text == 2) {
                                                result = "Không chấp nhận";
                                            }
                                            return result;
                                        },
                                        width: 150,
                                    },
                                ],
                                rules: {
                                    tlhdunhiem: webix.rules.isNotEmpty,
                                    khmhdon: webix.rules.isNotEmpty,
                                    khhdunhiem: webix.rules.isNotEmpty,
                                    mstunhiem: webix.rules.isNotEmpty,
                                    ttcdunhiem: webix.rules.isNotEmpty,
                                    mdunhiem: webix.rules.isNotEmpty,
                                    tngay: webix.rules.isNotEmpty,
                                    dngay: webix.rules.isNotEmpty,
                                    pttthdunhiem: webix.rules.isNotEmpty,
                                },
                                ready: function () {
                                    var tableUyNhiem = webix.$$("danhsach_uynhiem");
                                    if (tableUyNhiem.count() == 0) {
                                        $$("ldkunhiem").define("required", false);
                                        $$("ldkunhiem").refresh();
                                    } else {
                                        $$("ldkunhiem").define("required", true);
                                        $$("ldkunhiem").define("invalidMessage", "Loại đăng ký ủy nhiệm không được để trống !");
                                        $$("ldkunhiem").refresh();
                                    }
                                },
                            },
                        ],
                    },
                    { height: 5 },
                    {
                        cols: [
                            {},
                            {
                                shortcut: "F10",
                                view: "button",
                                label: "Ghi (F10)",
                                id: "btnDKSDSave1",
                                width: 150,
                                align: "center",
                                type: "form",
                                on: {
                                    onClick: function () {
                                        var self = this;
                                        setTimeout(function () {
                                            var item = $$("dangkysudung").getValues();
                                            var table = $$("dangkysudung");

                                            if (table.validate()) {
                                                var cks = $$("danhsach_chukyso").serialize();
                                                if (cks.length == 0) {
                                                    webix.message("Bạn chưa đăng ký chữ ký số cho thông báo phát hành !", "error");
                                                    return;
                                                }
                                                if (!$$("danhsach_uynhiem").validate()) {
                                                    webix.message("Bạn chưa nhập đầy đủ thông tin ủy nhiệm hóa đơn !", "error");
                                                    return;
                                                }

                                                webix.extend(table, webix.ProgressBar);
                                                table.disable();
                                                table.showProgress();

                                                var details = [];
                                                details.push({ data: $$("danhsach_chukyso").serialize() });
                                                item["details"] = details;

                                                var uynhiems = [];
                                                uynhiems.push({ data: $$("danhsach_uynhiem").serialize() });
                                                item["uynhiems"] = uynhiems;

                                                var item1 = [];
                                                item1.push(item);
                                                var data_post = { editmode: editmode, data: item1 };

                                                webix
                                                    .ajax()
                                                    .headers({ "Content-type": "application/json" })
                                                    .post(app.config.host + "/Invoice68/dangkysudungSave", data_post, {
                                                        error: function (text, data, XmlHttpRequest) {
                                                            table.enable();
                                                            table.hideProgress();
                                                            // var json = JSON.parse(text);
                                                            // webix.message(json.ExceptionMessage, "error");
                                                            webix.message(text + XmlHttpRequest, "error");
                                                        },
                                                        success: function (text, data, XmlHttpRequest) {
                                                            table.enable();
                                                            table.hideProgress();
                                                            var json = JSON.parse(text);
                                                            if (json.hasOwnProperty("error")) {
                                                                webix.message(json.error, "error");
                                                            } else {
                                                                app.callEvent("dataChangeDangkysudung68", [editmode, null]);

                                                                webix.message("Thành công !", "success");
                                                                webix.$$("dangkysudungForms").close();
                                                            }
                                                        },
                                                    });
                                            }
                                        }, 100);
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },

                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy bỏ (ESC)",
                                width: 120,
                                type: "danger",
                                align: "center",
                                click: function () {
                                    webix.$$("dangkysudungForms").close();
                                },
                            },
                            {},
                        ],
                    },
                    { height: 5 },
                ],
            },
        },
        setEditMode: function (mode) {
            editmode = mode;
            var form = $$(this.$ui.id);
            //var topParentView = form.getTopParentView();
            form.config.editmode = mode;
            var icon = "";
            if (editmode == 1) {
                icon = "<span><i class='fa fa-plus' aria-hidden='true'></i></span>";
                var result = webix
                    .ajax()
                    .sync()
                    .get(app.config.host + "/System/GetDmDvcsByUser");
                var json = JSON.parse(result.response);

                $$("dclhe").setValue(json[0].address);
                $$("tnnt").setValue(json[0].name);
                $$("mst").setValue(json[0].tax_code);
                $$("dtlhe").setValue(json[0].tel);
                $$("dctdtu").setValue(json[0].email);
                $$("nlhe").setValue(json[0].manage);
            } else {
                if (editmode == 2) icon = "<i class='fa fa-edit' aria-hidden='true'></i>";
                else icon = "<i class='fa fa-trash fa-lg' aria-hidden='true'></i>";
            }

            webix.$$("head-icondksd").setHTML(icon);
            webix.$$("head-icondksd").refresh();
        },
        setValues: function (item, winconfig, params) {
            var win = $$(this.$ui.id);
            var form = win.getChildViews()[1];

            // item["tdlap"] = removeTimeZoneT(item["tdlap"]);
            form.setValues(item);

            // if (editmode == 2) {

            // }

            var tableDetail = webix.$$("danhsach_chukyso");
            var url = app.config.host + "/Invoice68/GetDataDangkysudungDetail?id=" + item.id;
            tableDetail.clearAll();
            tableDetail.load(url);

            var tableUyNhiem = webix.$$("danhsach_uynhiem");
            var urlUyNhiem = app.config.host + "/Invoice68/GetDataDangkysudungUyNhiem?id=" + item.id;
            tableUyNhiem.clearAll();
            tableUyNhiem.load(urlUyNhiem);
        },
        // $oninit: function (view, $scope) {
        //     var url = app.config.host + "/Invoice68/getDangkysudung";

        //     webix.ajax(url, function (text) {
        //         var json = JSON.parse(text);

        //         $$("dangkysudung").setValues(json);
        //         $$("danhsach_chukyso").parse(json["data"]);
        //     });
        // }
    };
});
