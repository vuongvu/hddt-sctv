define(["app", "locale", "models/user", "views/commonHD"], function (app, _, user, commonHD) {
    var editmode = 0;
    var param;
    var branh_code;

    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Nhận (F10)",
            type: "form",
            align: "center",
            width: 120,
            on: {
                onClick: function () {
                    var self = this;
                    setTimeout(function () {
                        var item = $$("chuyendai-form").getValues();
                        var kyHieuHoaDon = {
                            cctbao_id: $$("kyhieuhoadon_78_id").getValue(),
                            khieu: $$("kyhieuhoadon_78_id").data.text,
                        };

                        switch (item.loaichuyendai_id) {
                            case "1":
                                commonHD.ExcuteReplaceInvoice(kyHieuHoaDon);
                                break;
                            case "2":
                                commonHD.ExcuteIncreaseChangeInvoice(kyHieuHoaDon);
                                break;
                            case "3":
                                commonHD.ExcuteReduceChangeInvoice(kyHieuHoaDon);
                                break;
                            case "4":
                                commonHD.ExcuteInfoChangeInvoice(kyHieuHoaDon);
                                break;
                            default:
                                break;
                        }
                        $$("chuyendai-win").close();
                    }, 100);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            hotkey: "esc",
            label: "Hủy bỏ (ESC)",
            type: "danger",
            align: "center",
            width: 120,
            click: function () {
                var table = $$("invoiceViews").getParentView();
                console.log(table);
                webix.$$("chuyendai-win").close();
            },
        },
        {},
    ];

    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "chuyendai-win",
        position: "center",
        head: "Chuyển dải hoá đơn hóa đơn",
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            width: 500,
            id: "chuyendai-form",
            complexData: true,
            margin: 0,
            elements: [
                // {
                //     view: "text",
                //     label: "Tên mẫu",
                //     name: "ten_mau",
                //     required: true,
                //     invalidMessage: "Tên mẫu không được bỏ trống",
                //     labelWidth: 120
                // },
                {
                    view: "combo",
                    name: "kyhieuhoadon_78_id",
                    label: "Kí hiệu hóa đơn",
                    //labelPosition: "top",
                    id: "kyhieuhoadon_78_id",
                    required: true,
                    labelWidth: 120,
                    invalidMessage: "Kí hiệu hóa đơn không được bỏ trống",
                },
                {
                    view: "combo",
                    name: "loaichuyendai_id",
                    label: "Loại chuyển dải",
                    id: "loaichuyendai_id",
                    labelWidth: 120,
                    value: "1",
                    options: [
                        { id: 1, value: "Thay thế" },
                        { id: 2, value: "Điều chỉnh tăng" },
                        { id: 3, value: "Điều chỉnh giảm" },
                        { id: 4, value: "Điều chỉnh thông tin" },
                    ],
                },
                {
                    margin: 10,
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },
        initUI: function (item) {
            param = item;
            $$("kyhieuhoadon_78_id").define("options", app.config.host + "/System/GetDataByReferencesId?id=" + param["data"][0].WinParams[0].wb_references_id);
            var table = $$("invoiceViews");
            var item = table.getSelectedItem();
            $$("kyhieuhoadon_78_id").setValue(item.cctbao_id);
            $$("kyhieuhoadon_78_id").refresh();
        },
        setValues: function (item) {
            $$("chuyendai-form").setValues(item);
        },
    };
});
