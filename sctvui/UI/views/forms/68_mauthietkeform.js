define(["app", "models/user"], function (app, user) {
    var editmode = 0;

    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Nhận (F10)",
            type: "form",
            align: "center",
            on: {
                onClick: function () {
                    var self = this;
                    setTimeout(function () {
                        var table = $$("mauthietke-form");
                        var item = table.getValues();
                        if (table.validate()) {
                            item.editmode = editmode;
                            $$("uploader68").define("upload", app.config.host + "/Invoice68/quanlymau68");
                            $$("uploader68").define("formData", item);
                            $$("uploader68").send(function (res) {
                                if (res != undefined) {
                                    if (res.hasOwnProperty("error")) {
                                        webix.message({ type: "error", text: res.error });
                                    } else {
                                        webix.message("Tạo mẫu hóa đơn thành công", "success");

                                        var win = $$("uploader68").getTopParentView();
                                        win.close();

                                        var table = $$("mauthietke");
                                        table.clearAll();
                                        table.load(table.config.url);
                                    }
                                } else {
                                    webix.message("Bạn chưa chọn mẫu hóa đơn ", "error");
                                }
                            });
                        }
                    }, 100);

                    // $$("uploader68").refresh();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            hotkey: "esc",
            label: "Hủy bỏ (ESC)",
            type: "danger",
            align: "center",
            click: function () {
                webix.$$("mauthietke-win").close();
            },
        },
        {},
    ];

    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "mauthietke-win",
        position: "center",
        head: "Mẫu hóa đơn",
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            width: 500,
            id: "mauthietke-form",
            complexData: true,
            margin: 0,
            elementsConfig: {
                labelPosition: "top",
            },
            elements: [
                // {
                //     view: "text",
                //     label: "mauthietke_id",
                //     id: "mauthietke_id",
                //     name: "mauthietke_id",
                //     hidden: true
                // },
                {
                    view: "text",
                    label: "Mã mẫu",
                    name: "mmau",
                    id: "mmau",
                    readonly: true,
                    css: "mcls_readonly_text",
                    labelWidth: 120,
                },
                {
                    view: "text",
                    label: "Tên mẫu",
                    name: "tmau",
                    id: "tmau",
                    labelWidth: 120,
                    required: true,
                    invalidMessage: "Tên mẫu không được bỏ trống hoặc không đúng định dạng",
                },

                {
                    view: "uploader",
                    value: "Upload file",
                    label: "Mẫu thiết kế",
                    id: "uploader68",
                    name: "uploader68",
                    link: "mylist",
                    accept: "repx",
                    autosend: false,
                    multiple: false,
                    // upload: app.config.host + "",
                    on: {
                        onUploadComplete: function (response) {},
                        onFileUploadError: function (file, response) {},
                        onAfterDelete: function (id) {},
                    },
                },
                {
                    view: "list",
                    id: "mylist",
                    type: "uploader",
                    autoheight: true,
                    borderless: true,
                },
                {
                    height: 20,
                },
                {
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },
        setValues: function (item) {
            var form = $$("mauthietke-form");
            form.setValues(item);
        },
    };
});
