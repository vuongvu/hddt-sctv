define(["app","locale"], function (app,_) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "dmtab-win", position: "center",
			head: "Người sử dụng",
			width: 500,
			height: 480,
			body: {
				paddingY: 10,
				paddingX: 10,
				elementsConfig: { labelWidth: 60 },
				view: "form",
				id: "dmtab-form",
				complexData: true,
				margin: 0,
				elements: [
					{
						view: "text",
						name: "username",
						labelPosition: "top",
						required: true,
						label: "Tên truy cập",
						id: "dmtab-username",
						invalidMessage: "Tên truy cập không để trống hoặc nhập không đúng (a-z,A-Z,0-9)",
						validate: webix.rules.isSpecialCharacter,
						css: "mcls_uppercase",
						on: {
							onChange: function (newv, oldv) {
								if (newv != undefined) {
									if (oldv != undefined) {
										if (newv.toUpperCase() != oldv.toUpperCase()) {
											webix.$$(this.config.id).setValue(newv.toUpperCase());
										}
									}
									else {
										webix.$$(this.config.id).setValue(newv.toUpperCase());
									}

								}

							}
						}
					},
					{
						view: "combo",
						name: "wb_role_id",
						label: "Nhóm quyền",
						options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00191",
						id: "dmtab-nhomquyen",
						labelPosition: "top",
						required: true,
						invalidMessage: "Bạn chưa chọn nhóm quyền"
					},					
					{
						view: "text",
						name: "email",
						required: true,
						labelPosition: "top",
						label: "Email",
						id: "dmtab-email",
						validate: webix.rules.isEmail,
						invalidMessage: "Email không được để trống hoặc không đúng định dạng"
					},
					{
						view: "text",
						name: "fullname",
						required: true,
						labelPosition: "top",
						label: "Tên người sử dụng",
						id: "dmtab-fullname",
						//validate: webix.rules.isEmail,
						invalidMessage: "Tên người sử dụng không được để trống"
					},
					/* {
								view:"checkbox",
								id:"dmtab-issigninvoice",
								name:"issigninvoice",
								label:"Ký hóa đơn",
								checkValue:"C",
								uncheckValue:"K" ,
								labelWidth:85
					},
					{
						view:"fieldset",
						label:"Quyền truy cập",
						body:{
							cols:[
								{
									view:"checkbox",
									id:"dmtab-isviewuser",
									name:"isviewuser",
									label:"Xem",
									checkValue:"C",
									uncheckValue:"K" 
								},
								{
								view:"checkbox",
								id:"dmtab-isedituser",
								name:"isedituser",
								label:"Sửa",
								checkValue:"C",
								uncheckValue:"K" 
								},
								{
									view:"checkbox",
									id:"dmtab-isdeluser",
									name:"isdeluser",
									label:"Xóa",
									checkValue:"C",
									uncheckValue:"K" 
								}
							]
						}
					}, */
					{ view: "textarea", name: "dien_giai", labelPosition: "top", label: "Diễn giải", id: "dmtab-diengiai" },

					{
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", view: "button", label: "Lưu (F10)", type: "form", align: "center",
								on: {
									onClick: function () {
										var form = webix.$$("dmtab-form");

										if (form.validate() == false) {
											return;
										}

										var item = form.getValues();
										item.window_type = "accountstore";

										var url = app.config.host + "/Account/Update";
										webix.extend(form, webix.ProgressBar);
										form.disable();
										form.showProgress();
										webix.ajax()
											.headers({ 'Content-type': 'application/json' })
											.post(url, item, function (text) {
												var data = JSON.parse(text);
												form.enable();
												form.hideProgress();
												if (!data.hasOwnProperty("error")) {
													item.id = data.id;
													webix.$$("dmtab-win").close();
													app.callEvent("dataChanged", [editmode, item]);
												}
												else {
													webix.message({ type: "error", text: data.error });
												}
											});
									}
								},
								click: function () {
									this.callEvent("onClick");

								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center",
								click: function () {
									webix.$$("dmtab-win").close();
								}
							},
							{}
						]
					}

				]
			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});