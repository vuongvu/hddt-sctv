define(["app"], function (app) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "dmwindow-win", position: "center",
			head: "Thông tin Window",
			fullscreen: true,
			body: {
				view: "scrollview",
				scroll: "native-y",
				body: {
					paddingY: 10,
					paddingX: 10,
					elementsConfig: { labelWidth: 120 },
					view: "form",
					id: "dmwindow-form",
					complexData: true,
					margin: 0,
					elements: [
						{ view: "text", name: "code", label: "WINDOW ID", id: "dmwindow-id" },
						{ view: "text", name: "name", label: "Tittle", id: "dmwindow-name" },
						{
							view: "combo",
							name: "window_type",
							label: 'Window Type',
							value: "Grid",
							options: ["Grid", "Detail", "MasterDetail", "Report"],
							id: "dmwindow-type"
						},
						{
							cols: [
								{ view: "text", name: "width", label: "Width", id: "dmwindow-width" },
								{ view: "text", name: "height", label: "Height", id: "dmwindow-height" },
							]
						},
						{
							cols: [
								{ view: "text", name: "fix_left", label: "fix_left", id: "dmwindow-fix_left", tooltip: "Số dòng cố định bên trái" },
								{ view: "text", name: "fix_right", label: "fix_right", id: "dmwindow-fix_right", tooltip: "Số dòng cố định bên phải" },
							]
						},
						{ view: "text", name: "max_row", label: "Rows max", id: "dmwindow-rowsmax", tooltip: "Số dòng lớn nhất của form" },
						{ view: "text", name: "tab_row", label: "Row tab", id: "dmwindow-rowtab", tooltip: "Dòng bắt đầu của form chi tiết nếu có" },
						{ view: "text", name: "voucher_code", label: "MA_CT", id: "dmwindow-mact" },
						{ view: "text", name: "sql_code", label: "SQL_CODE", id: "dmwindow-sqlcode" },
						{
							view: "combo",
							name: "wb_infowindow_id",
							label: "INFO WINDOW",
							id: "dmwindow-vcinfowindowid",
							options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00028"
						},
						{ view: "textarea", height: 160, name: "after_new", label: "AFTER_NEW", tooltip: "Trước khi UI forms được tạo" },
						{ view: "textarea", height: 160, name: "before_save", label: "BEFORE_SAVE", tooltip: "Trước khi lưu dữ kiệu TO DO CODE" },
						{ view: "textarea", height: 160, name: "after_save", label: "AFTER_SAVE" },
						{ view: "textarea", height: 160, name: "detail_layout", label: "Layout Detail", tooltip: "Nếu triển khai UI ở đây thì không Auto UI" },
						{ view: "textarea", height: 160, name: "after_edit", label: "AFTER_EDIT" },
						{
							margin: 10,
							cols: [
								{},
								{
									shortcut: "F10", view: "button", label: "Nhận (F10)", type: "form", align: "center", width: 140,
									on: {
										onClick: function () {
											var form = webix.$$("dmwindow-form");
											var item = form.getValues();

											webix.ajax()
												.headers({ 'Content-type': 'application/json' })
												.post(app.config.host + "/WbWindow/Update", item, function (text) {
													var data = JSON.parse(text);
													//console.log(data);
													if (!data.hasOwnProperty("error")) {
														item.id = data.id;
														webix.$$("dmwindow-win").close();
														app.callEvent("dataChanged", [editmode, item]);
													}
													else {
														webix.message(data.error);
													}
												});
										}
									},
									click: function () {
										this.callEvent("onClick");
									}
								},
								{
									view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 140,
									click: function () {
										webix.$$("dmwindow-win").close();
									}
								}
							]
						}

					]
				}

			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});