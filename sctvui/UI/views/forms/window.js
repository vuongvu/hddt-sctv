define(["app", "locale", "models/user", "views/forms/editwindow", "views/forms/selectwindow", "models/findjson"], function (app, _, user, editwindow, selectwindow, findJson) {
    var editmode = 0;
    var tables = [];

    var windowno = app.path[1].params[0];
    var col = [];
    var rw = [];
    var el = [];
    var head_name = [];
    var head;
    var url = app.config.host + "/System/GetAllByWindowNo?window_id=" + windowno;

    function calculateColumn(tableId, columnId, type, fieldId) {
        var table = $$(tableId);
        var result = 0;

        table.eachRow(function (row) {
            var value = Number(row[columnId]);

            if (type == "SUM") {
                result = result + value;
            }
        });

        $$(fieldId).setValue(result);
    }

    function showEditWindow(component, editmode) {
        var self = component;
        var suggest = $$(self.config.suggest);
        var refid = suggest.config.body.refid;
        var sql = {
            command: "SYS001",
            parameter: {
                REFID: refid,
            },
        };
        //var sql = "SELECT a.*,b.code as WINDOWNO FROM #SCHEMA_NAME#.wb_references a INNER JOIN #SCHEMA_NAME#.wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='" + refid + "'";
        // var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;
        //var url = app.config.host + "/System/Query";
        var url = app.config.host + "/System/ExecuteCommand";

        //var model = { sql: sql };
        var win = self.getTopParentView();
        var form = win.getChildViews()[1];

        var editorId = self.config.id;
        var formId = form.config.id;

        var ajax = webix.ajax();

        ajax.master = {
            editorId: editorId,
            formId: formId,
            fieldExpression: self.config.fieldExpression,
            editmode: editmode,
            editorvalue: self.getValue(),
            suggest: self.config.suggest,
        };

        ajax.post(url, sql, function (text) {
            var json = JSON.parse(text);

            if (json.length > 0) {
                json[0].editorId = this.editorId;
                json[0].formId = this.formId;
                json[0].fieldExpression = this.fieldExpression;
                json[0].editmode = this.editmode;
                json[0].editorvalue = this.editorvalue;
                json[0].suggest = this.suggest;

                var windowno = json[0].windowno;

                var url = app.config.host + "/System/GetConfigWinByNo?id=" + windowno;
                var a = webix.ajax(url);
                url = app.config.host + "/System/GetRecordByReferencesId?id=" + json[0].wb_references_id + "&value=" + this.editorvalue;
                var b = webix.ajax(url);
                var c = this.editmode;
                var d = json[0];

                webix.promise.all([a, b, c, d]).then(function (results) {
                    var windowconfig = results[0].json();
                    var res_b = results[1].json();
                    var mode = results[2];
                    var master = results[3];

                    var vc_window = windowconfig["data"][0];

                    if (mode == 1) {
                        editwindow.$ui.id = vc_window.code;
                        editwindow.$ui.editorId = master.editorId;
                        editwindow.$ui.valueField = master.value_field;
                        editwindow.$ui.fieldExpression = master.fieldExpression;
                        editwindow.$ui.formId = master.formId;

                        var win = webix.ui(editwindow.$ui);

                        editwindow.initUI(windowconfig);
                        editwindow.setEditMode(mode);
                        editwindow.setValues(null, windowconfig);

                        $$(master.suggest).hide();
                        win.show();
                    } else {
                        if (res_b.length == 0) {
                            webix.alert({
                                title: "Thông báo",
                                text: "Không tìm thấy dữ liệu",
                            });

                            return;
                        }

                        var ajax = webix.ajax();

                        ajax.master = {
                            master: master,
                            vc_window: vc_window,
                        };

                        var url = app.config.host + "/System/GetDataById?window_id=" + vc_window.code + "&id=" + res_b[0].doc_id;

                        ajax.get(url, function (text) {
                            var json = JSON.parse(text);
                            var data = json;

                            var vc_window = this.vc_window;
                            var master = this.master;

                            editwindow.$ui.id = vc_window.code;
                            editwindow.$ui.editorId = master.editorId;
                            editwindow.$ui.valueField = master.value_field;
                            editwindow.$ui.fieldExpression = master.fieldExpression;
                            editwindow.$ui.formId = master.formId;

                            var win = webix.ui(editwindow.$ui);

                            editwindow.initUI(windowconfig);
                            editwindow.setEditMode(2);
                            editwindow.setValues(data, windowconfig);

                            $$(master.suggest).hide();
                            win.show();
                        });
                    }
                });
            }
        });
    }

    function tableShowEditWindow(table, editmode) {
        var editor = table.getEditState();

        if (!webix.isUndefined(editor)) {
            var suggest = $$(editor.config.suggest);
            var refid = suggest.config.body.refid;
            var sql = {
                command: "SYS003",
                parameter: {
                    refid: refid,
                },
            };
            //var sql = "SELECT a.*,b.code as WINDOWNO FROM #SCHEMA_NAME#.wb_references a INNER JOIN #SCHEMA_NAME#.wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='" + refid + "'";
            // var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;
            //var url = app.config.host + "/System/Query";
            var url = app.config.host + "/System/ExecuteCommand";
            var model = sql;
            var tableId = table.config.id;
            //console.log(tableId);
            var columnName = editor.config.id;

            var ajax = webix.ajax();

            ajax.master = {
                editorId: null,
                tableId: tableId,
                fieldExpression: suggest.config.fieldExpression,
                columnName: columnName,
                editmode: editmode,
                editorvalue: editor.getValue(),
                suggest: editor.config.suggest,
            };

            ajax.post(url, model, function (text) {
                var json = JSON.parse(text);

                if (json.length > 0) {
                    json[0].editorId = this.editorId;
                    json[0].tableId = this.tableId;
                    json[0].fieldExpression = this.fieldExpression;
                    json[0].columnName = this.columnName;
                    json[0].editmode = this.editmode;
                    json[0].editorvalue = this.editorvalue;
                    json[0].suggest = this.suggest;

                    var windowno = json[0].windowno;

                    var url = app.config.host + "/System/GetConfigWinByNo?id=" + windowno;
                    var a = webix.ajax(url);
                    url = app.config.host + "/System/GetRecordByReferencesId?id=" + json[0].wb_references_id + "&value=" + this.editorvalue;
                    var b = webix.ajax(url);
                    var c = this.editmode;
                    var d = json[0];

                    webix.promise.all([a, b, c, d]).then(function (results) {
                        var windowconfig = results[0].json();
                        var res_b = results[1].json();
                        var mode = results[2];
                        var master = results[3];

                        var vc_window = windowconfig["data"][0];

                        if (mode == 1) {
                            editwindow.$ui.id = vc_window.code;
                            editwindow.$ui.editorId = master.editorId;
                            editwindow.$ui.valueField = master.value_field;
                            editwindow.$ui.fieldExpression = master.fieldExpression;
                            editwindow.$ui.formId = master.formId;
                            editwindow.$ui.tableId = master.tableId;
                            editwindow.$ui.columnName = master.columnName;

                            var win = webix.ui(editwindow.$ui);

                            editwindow.initUI(windowconfig);
                            editwindow.setValues(null, windowconfig);
                            editwindow.setEditMode(mode);

                            $$(master.suggest).hide();
                            win.show();
                        } else {
                            if (res_b.length == 0) {
                                webix.alert({
                                    title: "Thông báo",
                                    text: "Không tìm thấy dữ liệu",
                                });

                                return;
                            }
                            var ajax = webix.ajax();

                            ajax.master = {
                                master: master,
                                vc_window: vc_window,
                            };

                            var url = app.config.host + "/System/GetDataById?window_id=" + vc_window.code + "&id=" + res_b[0].doc_id;

                            ajax.get(url, function (text) {
                                var json = JSON.parse(text);
                                var data = json;

                                var vc_window = this.vc_window;
                                var master = this.master;

                                editwindow.$ui.id = vc_window.code;
                                editwindow.$ui.editorId = master.editorId;
                                editwindow.$ui.valueField = master.value_field;
                                editwindow.$ui.fieldExpression = master.fieldExpression;
                                editwindow.$ui.formId = master.formId;
                                editwindow.$ui.tableId = master.tableId;
                                editwindow.$ui.columnName = master.columnName;

                                var win = webix.ui(editwindow.$ui);

                                editwindow.initUI(windowconfig);
                                editwindow.setValues(data, windowconfig);
                                editwindow.setEditMode(2);

                                $$(master.suggest).hide();
                                win.show();
                            });
                        }
                    });
                }
            });
        }
    }

    return {
        $ui: {
            view: "window",
            modal: true,
            id: "window-win",
            position: "center",
            //width:800,
            head: "Thông tin khách hàng",
            //fullscreen:true,
            body: {
                paddingY: 10,
                paddingX: 10,
                view: "form",
                id: "window-form",
                complexData: true,
                keepViews: true,
                multiview: { keepViews: true },
                margin: 0,
                elements: el,
            },
        },
        initUI: function (windowconfig) {
            var rp = windowconfig;
            var max_row = rp["data"][0]["max_row"];
            var vc_window = rp["data"][0];
            var tabs = vc_window.Tabs;

            var win = $$(vc_window.code);

            if (typeof win.showProgress === "undefined") webix.extend(win, webix.ProgressBar);

            el = [];
            col = [];

            if (vc_window.width == null && vc_window.height == null) {
                win.config.fullscreen = true;
            }

            if (vc_window.after_save != null) {
                win.config.after_save = vc_window.after_save;
            }
            // nếu có layout_detail thì bindding layout vào el

            if (vc_window.detail_layout != null && vc_window.detail_layout != "") {
                var layoutdetail = JSON.parse(vc_window.detail_layout);

                var layout_element = JSON.stringify(layoutdetail.elements);
                var json_layout = JSON.parse(layout_element);
                //console.log(json_layout);
                for (var i = 0; i < json_layout.length; i++) {
                    el.push(json_layout[i]);
                }

                var jsonElement = findJson.getObjects(JSON.parse(JSON.stringify(el)), "id", "");

                for (var i = 0; i < jsonElement.length; i++) {
                    //var item = jsonElement[i];
                    var fields = rp["data"][0]["Tabs"][0]["Fields"];

                    for (var j = 0; j < fields.length; j++) {
                        var field = fields[j];

                        if (jsonElement[i].name == field.column_name) {
                            // if (field.hidden == true) {
                            //     continue;
                            // }

                            var item = {
                                view: field.type_editor,
                                resizeColumn: true,
                                //keepViews: true,
                                multiview: { keepViews: true },
                                name: field.column_name,
                                label: _(field.column_name),
                                id: field.column_name + "_" + rp["data"][0]["Tabs"][0]["table_name"],
                                width: field.width,
                                labelPosition: field.label_position,
                                labelWidth: field.label_width,
                                format: field.format,
                                value_change: field.value_change,
                                fieldExpression: field.field_expression,
                                data_width: field.data_width,
                                column_type: field.column_type,
                                css: field.css,
                                attributes: {},
                                on: {},
                                hidden: field.hidden,
                            };
                            if (field.type_editor == "password") {
                                item.view = "text";
                                item.type = "password";
                            }

                            if (item.data_width != null) {
                                if (item.data_width != 0) {
                                    item.attributes.maxlength = item.data_width;
                                    //style: "text-transform: uppercase;"
                                }
                            }

                            if (field.styleinput != null && field.styleinput != "") {
                                item.attributes.style = field.styleinput;
                            }

                            if (field.height != null) {
                                if (field.height != 0) {
                                    item.height = field.height;
                                }
                            }

                            if (field.caption != null) {
                                item.label = _(field.caption);
                            }

                            if (field.read_only == "C") {
                                item.readonly = true;
                            }

                            if (item.view == "combo") {
                                item.suggest = {
                                    body: {
                                        url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                                    },
                                };
                                //item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                            }

                            if (item.view == "select") {
                                item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                            }
                            if (item.view == "checkbox") {
                                if (field.label_position == "right") {
                                    item.labelRight = _(field.column_name);
                                    item.label = "";
                                }

                                if (field.column_type == "char") {
                                    item.checkValue = "C";
                                    item.uncheckValue = "K";
                                }

                                if (field.column_type == "tinyint") {
                                    item.checkValue = "1";
                                    item.uncheckValue = "0";
                                }

                                if (field.column_type == "bit") {
                                    item.checkValue = true;
                                    item.uncheckValue = false;
                                }
                            }

                            if (item.view == "autonumeric" || field.column_type == "numeric") {
                                item.inputAlign = "right";
                                item.decimalPlaces = field.data_decimal;
                            }

                            if (field.valid_rule != null) {
                                var rule = field.valid_rule;

                                if (rule == "isNotEmpty") {
                                    item.required = true;
                                    item.invalidMessage = item.label + " không được bỏ trống";
                                    item.validate = webix.rules.isNotEmpty;
                                }

                                if (rule == "isSpecialCharacter") {
                                    item.required = true;
                                    item.invalidMessage = "Không bỏ trống hoặc ký tự không hợp lệ (a-z,A-Z,0-9)";
                                    item.validate = webix.rules.isSpecialCharacter;
                                }

                                if (rule == "isEmail") {
                                    item.invalidMessage = item.label + " không hợp lệ";
                                    item.validate = webix.rules.isEmail;
                                }

                                if (rule == "isTaxCode") {
                                    item.invalidMessage = item.label + " không hợp lệ";
                                    item.validate = webix.rules.isTaxCode;
                                }
                            }

                            if (item.view == "treesuggest") {
                                //item.REF_ID = field.REF_ID.toString();
                                var listColumns = JSON.parse(field.list_column);

                                for (var v = 0; v < listColumns.length; v++) {
                                    listColumns[v].header = _(listColumns[v].id);
                                }

                                item.view = "combo";
                                item.options = {
                                    view: "treesuggest",
                                    textValue: field.display_field,
                                    url: {
                                        $proxy: true,
                                        load: function (view, callback, params) {
                                            var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

                                            webix.ajax(url, function (text) {
                                                var json = JSON.parse(text);
                                                var data = listToTree(json);
                                                view.parse(data, "json");
                                            });
                                        },
                                    },
                                    body: {
                                        columns: listColumns,
                                        refid: field.ref_id,
                                        filterMode: {
                                            level: false,
                                            showSubItems: false,
                                        },
                                    },
                                };

                                item.on.onKeyPress = function (code, e) {
                                    if (e.code == "F4") {
                                        alert("Moi");
                                    }
                                };
                            }

                            if (item.view == "gridcombo") {
                                var listColumns = JSON.parse(field.list_column);

                                for (var v = 0; v < listColumns.length; v++) {
                                    listColumns[v].header = _(listColumns[v].id);
                                }

                                item.view = "combo";
                                item.options = {
                                    view: "gridsuggest",
                                    url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                                    body: {
                                        columns: listColumns,
                                        refid: field.REF_ID,
                                        textValue: field.display_field,
                                    },
                                };

                                item.on.onKeyPress = function (code, e) {
                                    if (e.code == "F4") {
                                        alert("Moi");
                                    }
                                };
                            }
                            // if (item.view == "search") {
                            // 	var express = item.fieldExpression;

                            // 	item.on.onSearchIconClick = function (e) {
                            // 		var form = $$(this.getFormView());

                            // 		//get value của form hiện tại để đổi giá trị
                            // 		var parentItem = form.getValues();

                            // 		var field_express = this.config.fieldExpression;
                            // 		var text = this.getValue();
                            // 		var datapost = { "masothue": text };
                            // 		var form = $$("window-form");
                            // 		webix.extend(form, webix.ProgressBar);
                            // 		form.disable();
                            // 		form.showProgress();
                            // 		webix.ajax()
                            // 			.headers({ 'Content-type': 'application/json' })
                            // 			.post(app.config.host + "/System/GetInfoMST", datapost, function (text) {

                            // 				var json = JSON.parse(text);

                            // 				if (!json.hasOwnProperty("error")) {
                            // 					if (field_express != null) {

                            // 						var fields = field_express.split(';');
                            // 						// tách json thành array
                            // 						for (var i = 0; i < fields.length; i++) {
                            // 							var field = fields[i].replace("{", "").replace("}", "");
                            // 							var array = field.split(":");

                            // 							parentItem[array[1]] = json[array[0]];

                            // 						}
                            // 						form.setValues(parentItem);
                            // 					}
                            // 					else {
                            // 						webix.message("FIELD_EXPRESSION IS NULL IN FIELD !", "error", 2000);
                            // 					}
                            // 				}
                            // 				else {
                            // 					// nếu không tồn tại mã số thuế phải xóa địa chỉ + tên cty ở đây
                            // 					webix.message(json.error, "error", 2000);
                            // 				}
                            // 				form.enable();
                            // 				form.hideProgress();
                            // 			});

                            // 	}
                            // }

                            if (item.view == "search") {
                                item.on.onSearchIconClick = function (e) {
                                    var form = $$(this.getFormView());

                                    //get value của form hiện tại để đổi giá trị
                                    var mst = this.getValue();
                                    // window.location.href = "http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst;
                                    window.open("http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst);
                                };
                            }

                            if (item.view == "gridsuggest") {
                                var listColumns = JSON.parse(field.list_column);

                                for (var v = 0; v < listColumns.length; v++) {
                                    listColumns[v].header = _(listColumns[v].id);
                                }

                                item.view = "search";
                                item.icon = "mdi mdi-plus";
                                item.on.onSearchIconClick = function (e) {
                                    showEditWindow(this, 1);
                                };
                                item.suggest = {
                                    view: "gridsuggest",
                                    autoheight: false,
                                    value_suggest: field.value_suggest,
                                    textValue: field.display_field,
                                    body: {
                                        columns: listColumns,
                                        refid: field.ref_id,
                                        scroll: true,
                                        autoheight: false,
                                        datatype: "json",
                                        textValue: field.display_field,
                                        dataFeed: function (filtervalue, filter) {
                                            if (filtervalue.length < 1) return;

                                            var refid = this.config.refid;
                                            var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
                                            var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                            this.load(pathUrl + "&" + urldata, this.config.datatype);
                                        },
                                    },
                                    on: {
                                        onValueSuggest: function (obj) {
                                            //console.log(obj);
                                            var master = $$(this.config.master);
                                            if (master.config.fieldExpression != null) {
                                                var win = master.getTopParentView();
                                                var form = win.getChildViews()[1];
                                                //console.log(form);
                                                var parentItem = form.getValues();
                                                //console.log(parentItem);
                                                var fields = master.config.fieldExpression.split(";");

                                                for (var i = 0; i < fields.length; i++) {
                                                    var field = fields[i].replace("{", "").replace("}", "");
                                                    var array = field.split(":");
                                                    //console.log(array);
                                                    parentItem[array[1]] = obj[array[0]];
                                                }

                                                form.setValues(parentItem);
                                            }

                                            if (this.config.value_suggest != null) {
                                                var fn = Function("obj", "id", this.config.value_suggest);
                                                fn(obj, this.config.id);
                                            }
                                        },
                                    },
                                };

                                item.on.onKeyPress = function (code, e) {
                                    if (e.code == "F2") {
                                        var suggest = $$(this.config.suggest);
                                        var refid = suggest.config.body.refid;

                                        suggest.hide();
                                        var sql = {
                                            command: "SYS004",
                                            parameter: {
                                                refid: refid,
                                            },
                                        };
                                        //var sql = "SELECT a.*,b.window_id as WINDOWNO FROM #SCHEMA_NAME#.wb_references a INNER JOIN #SCHEMA_NAME#.wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='" + refid + "'";
                                        //var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;
                                        var url = app.config.host + "/System/ExecuteCommand";

                                        var win = this.getTopParentView();
                                        var form = win.getChildViews()[1];

                                        var editorId = this.config.id;
                                        var formId = form.config.id;

                                        var a = webix.ajax().post(url, sql);
                                        var b = this.config.suggest;
                                        var c = this.config.id;
                                        var d = this.config.fieldExpression;
                                        var e = editorId;
                                        var f = formId;

                                        webix.promise.all([a, b, c, d, e, f]).then(function (results) {
                                            var res_a = results[0].json();

                                            var suggest = $$(results[1]);

                                            suggest.hide();

                                            var url = app.config.host + "/System/GetConfigWinByNo?id=" + res_a[0]["windowno"];

                                            var ajax = webix.ajax();
                                            ajax.master = {
                                                searchId: results[2],
                                                fieldExpression: d,
                                                valueField: res_a[0]["value_field"],
                                                formId: f,
                                                editorId: e,
                                            };

                                            ajax.get(url, function (text) {
                                                var windowconfig = JSON.parse(text);
                                                var txtSearch = $$(this.searchId);

                                                var win = txtSearch.$scope.ui(selectwindow.$ui);

                                                win.config.valueField = this.valueField;
                                                win.config.fieldExpression = this.fieldExpression;
                                                win.config.formId = this.formId;
                                                win.config.editorId = this.editorId;

                                                selectwindow.initUI(windowconfig);

                                                win.getHead().setHTML(windowconfig.data[0].name);
                                                win.show();
                                            });
                                        });

                                        var win = this.$scope.ui(selectwindow.$ui);
                                        win.show();
                                    }

                                    if (e.code == "F4" || e.code == "F3") {
                                        var editmode = e.code == "F3" ? 2 : 1;
                                        showEditWindow(this, editmode);
                                    }
                                };
                            }

                            if (item.value_change != null || item.css != null || item.validate != null) {
                                item.on.onChange = function (newv, oldv) {
                                    if (this.config.validate != null && editmode != 0) {
                                        this.validate();
                                    }

                                    if (this.config.css != null) {
                                        if (this.config.css == "mcls_uppercase" && newv !== undefined && oldv !== undefined) {
                                            if (newv.toUpperCase() != oldv.toUpperCase()) {
                                                webix.$$(this.config.id).setValue(newv.toUpperCase());
                                            }
                                        }
                                    }

                                    if (this.config.value_change != null) {
                                        var topParentView = this.getTopParentView();

                                        if (topParentView.config.editmode == 0) return;

                                        var fn = Function("newv", "oldv", "id", this.config.value_change);
                                        fn(newv, oldv, this.config.id);
                                    }
                                };
                            }

                            jsonElement[i] = item;
                            var finish_replace = findJson.findAndReplace(el, jsonElement[i].name, item);
                            // console.log(finish_replace);
                            //webix.extend(el,jsonElement[i],  true);
                        }
                    }
                }
                //console.log(jsonElement);
            } // nếu không có thì push vào el theo field
            else {
                // insert số dòng max của form

                for (var i = 1; i <= max_row; i++) {
                    el.push({ cols: [] });
                }

                var fields = rp["data"][0]["Tabs"][0]["Fields"];
                // field của tab master (đầu phiếu )

                for (var i = 0; i < fields.length; i++) {
                    var field = fields[i];

                    if (field.hidden == true) {
                        continue;
                    }

                    var row = field.row;

                    var item = {
                        view: field.type_editor,
                        resizeColumn: true,
                        multiview: { keepViews: true },
                        keepViews: true,
                        name: field.column_name,
                        label: _(field.column_name),
                        id: field.column_name + "_" + rp["data"][0]["Tabs"][0]["table_name"],
                        width: field.width,
                        labelPosition: field.label_position,
                        labelWidth: field.label_width,
                        format: field.format,
                        value_change: field.value_change,
                        fieldExpression: field.field_expression,
                        data_width: field.data_width,
                        column_type: field.column_type,
                        css: field.css,
                        attributes: {},
                        on: {},
                    };

                    if (field.type_editor == "password") {
                        item.view = "text";
                        item.type = "password";
                    }

                    if (item.data_width != null) {
                        if (item.data_width != 0) {
                            item.attributes.maxlength = item.data_width;
                            //style: "text-transform: uppercase;"
                        }
                    }

                    if (field.styleinput != null && field.styleinput != "") {
                        item.attributes.style = field.styleinput;
                    }

                    if (field.height != null) {
                        if (field.height != 0) {
                            item.height = field.height;
                        }
                    }

                    if (field.caption != null) {
                        item.label = _(field.caption);
                    }

                    if (field.read_only == "C") {
                        item.readonly = true;
                    }

                    if (item.view == "combo") {
                        item.suggest = {
                            body: {
                                url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                            },
                        };
                        //item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                    }
                    if (item.view == "select") {
                        item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                    }
                    if (item.view == "checkbox") {
                        if (field.label_position == "right") {
                            item.labelRight = _(field.column_name);
                            item.label = "";
                        }

                        if (field.column_type == "char") {
                            item.checkValue = "C";
                            item.uncheckValue = "K";
                        }

                        if (field.column_type == "tinyint") {
                            item.checkValue = "1";
                            item.uncheckValue = "0";
                        }

                        if (field.column_type == "bit") {
                            item.checkValue = true;
                            item.uncheckValue = false;
                        }
                    }

                    if (item.view == "autonumeric" || field.column_type == "numeric") {
                        item.inputAlign = "right";
                        item.decimalPlaces = field.data_decimal;
                    }

                    if (field.valid_rule != null) {
                        var rule = field.valid_rule;

                        if (rule == "isNotEmpty") {
                            item.required = true;
                            item.invalidMessage = item.label + " không được bỏ trống";
                            item.validate = webix.rules.isNotEmpty;
                        }

                        if (rule == "isSpecialCharacter") {
                            item.required = true;
                            item.invalidMessage = "Không bỏ trống hoặc ký tự không hợp lệ (a-z,A-Z,0-9)";
                            item.validate = webix.rules.isSpecialCharacter;
                        }

                        if (rule == "isEmail") {
                            item.invalidMessage = item.label + " không hợp lệ";
                            item.validate = webix.rules.isEmail;
                        }

                        if (rule == "isTaxCode") {
                            item.invalidMessage = item.label + " không hợp lệ";
                            item.validate = webix.rules.isTaxCode;
                        }
                    }

                    if (item.view == "treesuggest") {
                        //item.REF_ID = field.REF_ID.toString();
                        var listColumns = JSON.parse(field.list_column);

                        for (var v = 0; v < listColumns.length; v++) {
                            listColumns[v].header = _(listColumns[v].id);
                        }

                        item.view = "combo";
                        item.options = {
                            view: "treesuggest",
                            textValue: field.display_field,
                            url: {
                                $proxy: true,
                                load: function (view, callback, params) {
                                    var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

                                    webix.ajax(url, function (text) {
                                        var json = JSON.parse(text);
                                        var data = listToTree(json);
                                        view.parse(data, "json");
                                    });
                                },
                            },
                            body: {
                                columns: listColumns,
                                refid: field.ref_id,
                                filterMode: {
                                    level: false,
                                    showSubItems: false,
                                },
                            },
                        };

                        item.on.onKeyPress = function (code, e) {
                            if (e.code == "F4") {
                                alert("Moi");
                            }
                        };
                    }

                    if (item.view == "gridcombo") {
                        var listColumns = JSON.parse(field.list_column);

                        for (var v = 0; v < listColumns.length; v++) {
                            listColumns[v].header = _(listColumns[v].id);
                        }

                        item.view = "combo";
                        item.options = {
                            view: "gridsuggest",
                            url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                            body: {
                                columns: listColumns,
                                refid: field.REF_ID,
                                textValue: field.display_field,
                            },
                        };

                        item.on.onKeyPress = function (code, e) {
                            if (e.code == "F4") {
                                alert("Moi");
                            }
                        };
                    }
                    if (item.view == "search") {
                        var express = item.fieldExpression;

                        item.on.onSearchIconClick = function (e) {
                            var form = $$(this.getFormView());

                            //get value của form hiện tại để đổi giá trị
                            var parentItem = form.getValues();

                            var field_express = this.config.fieldExpression;
                            var text = this.getValue();
                            var datapost = { masothue: text };
                            webix
                                .ajax()
                                .headers({ "Content-type": "application/json" })
                                .post(app.config.host + "/System/GetInfoByMST", datapost, function (text) {
                                    var json = JSON.parse(text);

                                    if (!json.hasOwnProperty("error")) {
                                        if (field_express != null) {
                                            var fields = field_express.split(";");
                                            // tách json thành array
                                            for (var i = 0; i < fields.length; i++) {
                                                var field = fields[i].replace("{", "").replace("}", "");
                                                var array = field.split(":");

                                                parentItem[array[1]] = json[array[0]];
                                            }
                                            form.setValues(parentItem);
                                        } else {
                                            webix.message("FIELD_EXPRESSION IS NULL IN FIELD !", "error", 2000);
                                        }
                                    } else {
                                        // nếu không tồn tại mã số thuế phải xóa địa chỉ + tên cty ở đây
                                        webix.message(json.error, "error", 2000);
                                    }
                                });
                        };
                    }

                    if (item.view == "gridsuggest") {
                        var listColumns = JSON.parse(field.list_column);

                        for (var v = 0; v < listColumns.length; v++) {
                            listColumns[v].header = _(listColumns[v].id);
                        }

                        item.view = "search";
                        item.icon = "mdi mdi-plus";
                        item.on.onSearchIconClick = function (e) {
                            showEditWindow(this, 1);
                        };
                        item.suggest = {
                            view: "gridsuggest",
                            autoheight: false,
                            value_suggest: field.value_suggest,
                            textValue: field.display_field,
                            body: {
                                columns: listColumns,
                                refid: field.ref_id,
                                scroll: true,
                                autoheight: false,
                                datatype: "json",
                                textValue: field.display_field,
                                dataFeed: function (filtervalue, filter) {
                                    if (filtervalue.length < 1) return;

                                    var refid = this.config.refid;
                                    var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
                                    var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                    this.load(pathUrl + "&" + urldata, this.config.datatype);
                                },
                            },
                            on: {
                                onValueSuggest: function (obj) {
                                    //console.log(obj);
                                    var master = $$(this.config.master);
                                    if (master.config.fieldExpression != null) {
                                        var win = master.getTopParentView();
                                        var form = win.getChildViews()[1];
                                        //console.log(form);
                                        var parentItem = form.getValues();
                                        //console.log(parentItem);
                                        var fields = master.config.fieldExpression.split(";");

                                        for (var i = 0; i < fields.length; i++) {
                                            var field = fields[i].replace("{", "").replace("}", "");
                                            var array = field.split(":");
                                            //console.log(array);
                                            parentItem[array[1]] = obj[array[0]];
                                        }

                                        form.setValues(parentItem);
                                    }

                                    if (this.config.value_suggest != null) {
                                        var fn = Function("obj", "id", this.config.value_suggest);
                                        fn(obj, this.config.id);
                                    }
                                },
                            },
                        };

                        item.on.onKeyPress = function (code, e) {
                            if (e.code == "F2") {
                                var suggest = $$(this.config.suggest);
                                var refid = suggest.config.body.refid;

                                suggest.hide();
                                var sql = {
                                    command: "SYS004",
                                    parameter: {
                                        refid: refid,
                                    },
                                };
                                //var sql = "SELECT a.*,b.window_id as WINDOWNO FROM #SCHEMA_NAME#.wb_references a INNER JOIN #SCHEMA_NAME#.wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='" + refid + "'";
                                //var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;
                                var url = app.config.host + "/System/ExecuteCommand";

                                var win = this.getTopParentView();
                                var form = win.getChildViews()[1];

                                var editorId = this.config.id;
                                var formId = form.config.id;

                                var a = webix.ajax().post(url, sql);
                                var b = this.config.suggest;
                                var c = this.config.id;
                                var d = this.config.fieldExpression;
                                var e = editorId;
                                var f = formId;

                                webix.promise.all([a, b, c, d, e, f]).then(function (results) {
                                    var res_a = results[0].json();
                                    var suggest = $$(results[1]);

                                    suggest.hide();

                                    var url = app.config.host + "/System/GetConfigWinByNo?id=" + res_a[0]["windowno"];

                                    var ajax = webix.ajax();
                                    ajax.master = {
                                        searchId: results[2],
                                        fieldExpression: d,
                                        valueField: res_a[0]["value_field"],
                                        formId: f,
                                        editorId: e,
                                    };

                                    ajax.get(url, function (text) {
                                        var windowconfig = JSON.parse(text);
                                        var txtSearch = $$(this.searchId);

                                        var win = txtSearch.$scope.ui(selectwindow.$ui);

                                        win.config.valueField = this.valueField;
                                        win.config.fieldExpression = this.fieldExpression;
                                        win.config.formId = this.formId;
                                        win.config.editorId = this.editorId;

                                        selectwindow.initUI(windowconfig);

                                        win.getHead().setHTML(windowconfig.data[0].name);
                                        win.show();
                                    });
                                });

                                var win = this.$scope.ui(selectwindow.$ui);
                                win.show();
                            }

                            if (e.code == "F4" || e.code == "F3") {
                                var editmode = e.code == "F3" ? 2 : 1;
                                showEditWindow(this, editmode);
                            }
                        };
                    }

                    if (item.value_change != null || item.css != null || item.validate != null) {
                        item.on.onChange = function (newv, oldv) {
                            if (this.config.validate != null && editmode != 0) {
                                this.validate();
                            }

                            if (this.config.css != null) {
                                if (this.config.css == "mcls_uppercase" && newv !== undefined && oldv !== undefined) {
                                    if (newv.toUpperCase() != oldv.toUpperCase()) {
                                        webix.$$(this.config.id).setValue(newv.toUpperCase());
                                    }
                                }
                            }

                            if (this.config.value_change != null) {
                                var topParentView = this.getTopParentView();

                                if (topParentView.config.editmode == 0) return;

                                var fn = Function("newv", "oldv", "id", this.config.value_change);
                                fn(newv, oldv, this.config.id);
                            }
                        };
                    }

                    el[row - 1].cols.push(item);
                    //console.log(el);
                }
            }
            if (vc_window.window_type == "MasterDetail") {
                var cells = [];

                for (var i = 0; i < tabs.length; i++) {
                    var tab = tabs[i];

                    if (tab.tab_type == "Master") {
                        continue;
                    }

                    var subCols = [];

                    for (var j = 0; j < tab.Fields.length; j++) {
                        var field = tab.Fields[j];

                        var subCol = {
                            id: field.column_name,
                            header: field.caption != null ? _(field.caption) : _(field.column_name),
                            editor: field.type_editor,
                            hidden: field.hidden,
                            value_change: field.value_change,
                        };

                        if (field.read_only == "C" || field.type_editor == "checkbox") {
                            delete subCol.editor;
                        }

                        if (field.type_editor == "checkbox") {
                            subCol.template = "{common.checkbox()}";
                            subCol.checkValue = true;
                            subCol.uncheckValue = false;
                        }

                        if (field.column_type == "date" || field.column_type == "datetime") {
                            subCol.format = function (text) {
                                if (text != null && text != undefined) {
                                    var format = webix.Date.dateToStr("%d/%m/%Y");
                                    text = removeTimeZoneT(text);
                                    return format(text);
                                }
                            };
                        }

                        if (field.column_type == "numeric") {
                            subCol.css = { "text-align": "right" };
                            subCol.format = webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: field.data_decimal,
                            });

                            if (field.type_editor == "autonumeric") {
                                if (field.data_decimal >= 0) {
                                    var decimal = Array(field.data_decimal + 1).join("9");

                                    subCol.numericConfig = {
                                        maximumValue: Number("999999999999." + decimal),
                                        minValue: Number("-999999999999." + decimal),
                                        decimalPlaces: field.data_decimal,
                                    };
                                }
                            }
                        }

                        if (field.column_width == null) {
                            subCol.fillspace = true;
                        } else {
                            subCol.width = field.column_width;
                        }

                        if (field.type_editor == "combo" || field.type_editor == "multiselect") {
                            subCol.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                            subCol.optionslist = true;
                        }

                        if (field.type_editor == "datepicker") {
                            subCol.editor = "date";
                        }

                        if (field.type_editor == "treesuggest") {
                            var listColumns = JSON.parse(field.list_column);

                            for (var v = 0; v < listColumns.length; v++) {
                                listColumns[v].header = _(listColumns[v].id);
                            }

                            var popup = webix.ui({
                                view: "treesuggest",
                                body: {
                                    columns: listColumns,
                                    refid: field.ref_id,
                                    url: {
                                        $proxy: true,
                                        load: function (view, callback, params) {
                                            var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

                                            webix.ajax(url, function (text) {
                                                var json = JSON.parse(text);
                                                var data = listToTree(json);
                                                view.parse(data, "json");
                                            });
                                        },
                                    },
                                    filterMode: {
                                        level: false,
                                        showSubItems: false,
                                    },
                                },
                            });

                            subCol.popup = popup;
                            subCol.editor = "combo";
                        }

                        if (field.type_editor == "gridcombo") {
                            var listColumns = JSON.parse(field.list_column);

                            for (var v = 0; v < listColumns.length; v++) {
                                listColumns[v].header = _(listColumns[v].id);
                            }

                            var popup = webix.ui({
                                view: "gridsuggest",
                                textValue: field.display_field,
                                body: {
                                    columns: listColumns,
                                    refid: field.ref_id,
                                    textValue: field.display_field,
                                    tableId: "edit_" + tab.table_name,
                                    url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                                },
                            });

                            subCol.popup = popup;
                            subCol.editor = "combo";
                        }
                        if (field.type_editor == "search") {
                            subCol.onSearchIconClick = function (e) {
                                var form = $$(this.getFormView());

                                var field_express = this.config.fieldExpression;
                                var text = this.getValue();
                                var datapost = { masothue: text };
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/System/GetInfoByMST", datapost, function (text) {
                                        var json = JSON.parse(text);

                                        if (!json.hasOwnProperty("error")) {
                                            //console.log(JSON.stringify(json));
                                            form.setValues(json);
                                        } else {
                                            // nếu không tồn tại mã số thuế phải xóa địa chỉ + tên cty ở đây
                                            webix.message(json.error, "error", 2000);
                                        }
                                    });
                            };
                        }
                        if (field.type_editor == "gridsuggest") {
                            var listColumns = JSON.parse(field.list_column);

                            for (var v = 0; v < listColumns.length; v++) {
                                listColumns[v].header = _(listColumns[v].id.toUpperCase());
                            }

                            subCol.editor = "textsearch";
                            subCol.tableId = "edit_" + tab.table_name;
                            subCol.onIconButtonClick = function (e) {
                                var tableId = this.getAttribute("tableId");
                                var table = $$(tableId);
                                tableShowEditWindow(table, 1);
                            };

                            subCol.suggest = {
                                view: "gridsuggest",
                                value_suggest: field.value_suggest,
                                fieldExpression: field.field_expression,
                                displayField: field.display_field,
                                tableId: "edit_" + tab.table_name,
                                columnName: field.column_name,
                                body: {
                                    columns: listColumns,
                                    refid: field.ref_id,
                                    scroll: true,
                                    autoheight: false,
                                    datatype: "json",
                                    dataFeed: function (filtervalue, filter) {
                                        if (filtervalue.length < 1) return;

                                        var refid = this.config.refid;
                                        var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
                                        var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                        this.load(pathUrl + "&" + urldata, this.config.datatype);
                                    },
                                },
                                on: {
                                    onValueSuggest: function (obj) {
                                        if (this.config.fieldExpression != null) {
                                            var table = $$(this.config.tableId);
                                            var selectedItem = table.getSelectedItem();

                                            var fields = this.config.fieldExpression.split(";");

                                            for (var i = 0; i < fields.length; i++) {
                                                var field = fields[i].replace("{", "").replace("}", "");
                                                var array = field.split(":");

                                                selectedItem[array[1]] = obj[array[0]];
                                            }

                                            table.updateItem(selectedItem.id, selectedItem);
                                        }

                                        if (this.config.value_suggest != null) {
                                            var fn = Function("obj", "id", this.config.value_suggest);
                                            fn(obj, this.config.id);
                                        }
                                    },
                                },
                            };
                        }

                        subCols.push(subCol);
                    }

                    if (tab.tab_type == "Detail") {
                        var item = {
                            header: tab.name,
                            select: "cell",
                            body: {
                                rows: [
                                    {
                                        view: "toolbar",
                                        height: 28,
                                        cols: [
                                            {
                                                view: "button",
                                                type: "icon",
                                                //view: "icon",
                                                label: " (F9)",
                                                width: 60,
                                                icon: "fas fa-plus",
                                                css: { "font-size": "15px" },
                                                on: {
                                                    onItemClick: function (id, e) {
                                                        var parentView = this.getParentView().getParentView();
                                                        var childViews = parentView.getChildViews();

                                                        for (var i = 0; i < childViews.length; i++) {
                                                            var childview = childViews[i];

                                                            if (childview.config.view == "datatable") {
                                                                var table = childview;

                                                                var item = {
                                                                    id: webix.uid(),
                                                                };

                                                                var vc_window = windowconfig["data"][0];
                                                                var tabs = vc_window.Tabs;

                                                                for (var i = 0; i < tabs.length; i++) {
                                                                    var tab = tabs[i];
                                                                    if (tab.tab_type == "Detail") {
                                                                        var fields = tab.Fields;

                                                                        for (var j = 0; j < fields.length; j++) {
                                                                            var field = fields[j];

                                                                            if (field.column_name == "branch_code") {
                                                                                item[field.column_name] = user.getDvcsID();
                                                                            }

                                                                            if (field.default_value != null) {
                                                                                var defaultValue = field.default_value.toString();

                                                                                if (defaultValue.startsWith("@Default=")) {
                                                                                    item[field.column_name] = defaultValue.replace("@Default=", "");
                                                                                }

                                                                                if (defaultValue.startsWith("@JS=")) {
                                                                                    var fn = Function(defaultValue.replace("@JS=", ""));
                                                                                    item[field.column_name] = fn();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                //console.log(item);
                                                                table.add(item);

                                                                table.select(item.id, table.config.columns[0].id, false);
                                                                table.edit({ row: item.id, column: table.config.columns[0].id });

                                                                break;
                                                            }
                                                        }
                                                    },
                                                },
                                            },
                                            {
                                                view: "button",
                                                type: "icon",
                                                //view: "icon",
                                                label: " (F8)",
                                                width: 60,
                                                icon: "fas fa-minus",
                                                on: {
                                                    onItemClick: function (id, e) {
                                                        var parentView = this.getParentView().getParentView();
                                                        var childViews = parentView.getChildViews();

                                                        for (var i = 0; i < childViews.length; i++) {
                                                            var childview = childViews[i];

                                                            if (childview.config.view == "datatable") {
                                                                var table = childview;

                                                                table.editStop();

                                                                var item = table.getSelectedItem();

                                                                if (!webix.isUndefined(item)) {
                                                                    table.remove(item.id);
                                                                    table.clearSelection();
                                                                    return;
                                                                }

                                                                var editor = table.getEditState();

                                                                if (!webix.isUndefined(editor)) {
                                                                    table.remove(editor.row);
                                                                    table.clearSelection();
                                                                    return;
                                                                }

                                                                break;
                                                            }
                                                        }
                                                    },
                                                },
                                            },
                                            {
                                                view: "button",
                                                type: "icon",
                                                //view: "icon",
                                                label: " (F7)",
                                                width: 45,
                                                icon: "fas fa-copy",
                                                on: {
                                                    onItemClick: function (id, e) {
                                                        var parentView = this.getParentView().getParentView();
                                                        var childViews = parentView.getChildViews();

                                                        for (var i = 0; i < childViews.length; i++) {
                                                            var childview = childViews[i];

                                                            if (childview.config.view == "datatable") {
                                                                var table = childview;

                                                                table.editStop();

                                                                var item = table.getSelectedItem();
                                                                if (item == null || item == undefined) {
                                                                    webix.message("Bạn chưa chọn dòng cần copy !", "error");
                                                                    return;
                                                                }
                                                                //item.id = webix.uid();
                                                                var copy = webix.copy(item);
                                                                copy.id = webix.uid();
                                                                copy[tab.table_name + "_id"] = null;

                                                                table.add(copy);
                                                                table.unselect(item.id);
                                                                table.select(copy.id, table.config.columns[0].id, false);
                                                                table.edit({ row: copy.id, column: table.config.columns[0].id });

                                                                break;
                                                            }
                                                        }
                                                    },
                                                },
                                            },
                                            {
                                                hidden: vc_window.code !== "WIN00023" ? true : false,
                                                view: "button",
                                                type: "icon",
                                                icon: "fas fa-folder-open",
                                                label: "Import file excel mẫu",
                                                width: 150,
                                                on: {
                                                    onClick: function () {
                                                        var table = $$("edit_" + tab.table_name);
                                                        require(["views/forms/uploadexceltemplate"], function (uploadForm) {
                                                            uploadForm.$ui.tableId = table.config.id;
                                                            var win = table.$scope.ui(uploadForm.$ui);
                                                            var form = webix.$$("window-form");
                                                            var data = form.getValues();
                                                            win.show();
                                                            webix.$$("uploader").define("formData", { wb_excel_id: data.id, excel_id: data.code, filename: data.name });
                                                            webix.$$("uploader").hide();
                                                        });
                                                    },
                                                },
                                                click: function () {
                                                    this.callEvent("onClick");
                                                },
                                            },
                                        ],
                                    },
                                    {
                                        view: "datatable",
                                        resizeColumn: true,
                                        id: "edit_" + tab.table_name,
                                        tab_table: tab.table_name,
                                        tab_id: tab.tab_id,
                                        editable: true,
                                        select: "cell",
                                        select_change: tab.select_change,
                                        cell_editstop: tab.cell_editstop,
                                        after_addrow: tab.after_addrow,
                                        after_delrow: tab.after_delrow,
                                        oncheck: tab.oncheck,
                                        columns: subCols,
                                        on: {
                                            onCheck: function (row, column, state) {
                                                var table = this;

                                                if (this.config.oncheck != null) {
                                                    var fn = Function("row", "column", "state", "tableId", this.config.oncheck);
                                                    fn(row, column, state, this.config.id);
                                                }
                                            },
                                            onAfterAdd: function (id, index) {
                                                if (this.config.after_addrow != null) {
                                                    var fn = Function("id", "index", "tableId", this.config.after_addrow);
                                                    fn(id, index, this.config.id);
                                                }
                                            },

                                            onAfterDelete: function (id) {
                                                if (this.config.after_delrow != null) {
                                                    var fn = Function("id", "tableId", this.config.after_delrow);
                                                    fn(id, this.config.id);
                                                }
                                            },
                                            onAfterEditStop: function (state, editor, ignore) {
                                                if (state.value === state.old) return;

                                                if (editor.config.value_change != null && editor.config.value_change != "") {
                                                    var selectedItem = null;

                                                    if (typeof editor.getPopup === "function") {
                                                        var popup = editor.getPopup();

                                                        if (popup != null) {
                                                            var list = popup.getList();

                                                            if (list != null) {
                                                                selectedItem = list.getSelectedItem();
                                                            }
                                                        }
                                                    }

                                                    var fn = Function("newv", "oldv", "id", "selectedItem", editor.config.value_change);
                                                    fn(state.value, state.old, editor.config.id, selectedItem);
                                                }

                                                if (this.config.cell_editstop != null) {
                                                    var fn = Function("columnId", "tableId", "rowId", this.config.cell_editstop);
                                                    fn(editor.config.id, this.config.id, editor.row);
                                                }
                                            },
                                            // onKeyDown: function(code, e)
                                            // {
                                            // 		if (e.key == 40) {
                                            // 		var toolbar = this.getParentView().getChildViews()[0];
                                            // 		var btnPlus = toolbar.getChildViews()[0];
                                            // 		console.log(btnPlus);
                                            // 		btnPlus.callEvent("onItemClick");
                                            // 	}
                                            // },
                                            onKeyPress: function (code, e) {
                                                var table = this;

                                                if (e.key == "F8") {
                                                    var toolbar = this.getParentView().getChildViews()[0];
                                                    var btnMinus = toolbar.getChildViews()[1];

                                                    btnMinus.callEvent("onItemClick");
                                                }
                                                if (e.key == "F9") {
                                                    var toolbar = this.getParentView().getChildViews()[0];
                                                    var btnPlus = toolbar.getChildViews()[0];

                                                    btnPlus.callEvent("onItemClick");
                                                }
                                                if (e.key == "F7") {
                                                    var toolbar = this.getParentView().getChildViews()[0];
                                                    var btnCopy = toolbar.getChildViews()[2];

                                                    btnCopy.callEvent("onItemClick");
                                                }

                                                if (e.key == "F2") {
                                                    var editor = table.getEditState();

                                                    if (editor === false) return;

                                                    var suggest = $$(editor.config.suggest);
                                                    var refid = suggest.config.body.refid;
                                                    var sql = {
                                                        command: "SYS004",
                                                        parameter: {
                                                            refid: refid,
                                                        },
                                                    };
                                                    //	var sql = "SELECT a.*,b.window_id as WINDOWNO FROM #SCHEMA_NAME#.wb_references a INNER JOIN #SCHEMA_NAME#.wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='" + refid + "'";
                                                    // var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;
                                                    var url = app.config.host + "/System/ExecuteCommand";

                                                    var a = webix.ajax().post(url, sql);
                                                    var b = editor.config.suggest;
                                                    var c = table.config.id;
                                                    var d = suggest.config.fieldExpression;
                                                    var e = editor.config.id;
                                                    var f = suggest.config.displayField;
                                                    //console.log(f);

                                                    webix.promise.all([a, b, c, d, e, f]).then(function (results) {
                                                        var res_a = results[0].json();
                                                        //	console.log(res_a);
                                                        var suggest = $$(results[1]);

                                                        suggest.hide();

                                                        var url = app.config.host + "/System/GetConfigWinByNo?id=" + res_a[0]["windowno"];

                                                        var ajax = webix.ajax();
                                                        ajax.master = {
                                                            tableId: results[2],
                                                            fieldExpression: d,
                                                            valueField: res_a[0]["value_field"],
                                                            editorId: null,
                                                            columnName: e,
                                                            displayField: f,
                                                        };

                                                        ajax.get(url, function (text) {
                                                            var windowconfig = JSON.parse(text);
                                                            var table = $$(this.tableId);
                                                            //console.log(windowconfig);
                                                            var win = table.$scope.ui(selectwindow.$ui);

                                                            win.config.valueField = this.valueField;
                                                            win.config.fieldExpression = this.fieldExpression;
                                                            win.config.tableId = this.tableId;
                                                            win.config.editorId = null;
                                                            win.config.columnName = this.columnName;
                                                            win.config.displayField = this.displayField;

                                                            selectwindow.initUI(windowconfig);

                                                            win.getHead().setHTML(windowconfig.data[0].name);
                                                            win.show();
                                                        });
                                                    });

                                                    var win = this.$scope.ui(selectwindow.$ui);
                                                    win.show();
                                                }

                                                if (e.key == "F4" || e.key == "F3") {
                                                    var editor = table.getEditState();

                                                    if (editor === false) {
                                                        var toolbar = this.getParentView().getChildViews()[0];
                                                        //console.log(toolbar);
                                                        var btnPlus = toolbar.getChildViews()[0];
                                                        //console.log(btnPlus);
                                                        btnPlus.callEvent("onItemClick");
                                                        return;
                                                    }
                                                    var editmode = e.key == "F3" ? 2 : 1;
                                                    tableShowEditWindow(table, editmode);
                                                }
                                            },
                                            onSelectChange: function () {
                                                var selectedItem = this.getSelectedId();

                                                if (this.config.select_change != null) {
                                                    var fn = Function("selectedItem", this.config.select_change);
                                                    fn(selectedItem);
                                                }
                                            },
                                        },
                                    },
                                ],
                            },
                        };

                        cells.push(item);
                    }
                }

                var subview = {
                    borderless: true,
                    view: "tabview",
                    scrollX: true,
                    cells: cells,
                    tabbar: {
                        width: 200,
                    },
                };

                if (vc_window.width != null || vc_window.height != null) {
                    subview.height = 250;
                }

                //el.push(subview);

                var myJsonString = JSON.stringify(el);
                var x = findJson.getObjects(JSON.parse(myJsonString), "id", "tab_replace");
                //replace tab chi tiết
                if (x.length > 0) {
                    var Y = x[0];
                    //webix.extend(Y, subview, true);
                    // cần cập nhật lại x[0] cho el

                    for (var i = 0; i < el.length; i++) {
                        if (el[i].id == "tab_replace") {
                            //console.log(el[i]);
                            el[i] = subview;
                        }
                    }
                } else {
                    if (vc_window.tab_row != null) {
                        el[vc_window.tab_row - 1] = subview;
                    } else {
                        el.push(subview);
                    }
                }
            }

            var cols = [
                // {},
                {
                    // 		      view: "button", type: "htmlbutton", width: 100,
                    // label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' +
                    // '<span class="text">LƯU (F10)</span>',
                    shortcut: "F10",
                    view: "button",
                    label: "LƯU (F10)",
                    type: "form",
                    align: "center",
                    width: 110,
                    before_save: vc_window.before_save,
                    on: {
                        onClick: function () {
                            var self = this;
                            setTimeout(function () {
                                var form = webix.$$("window-form");

                                if (form.validate() == false) {
                                    return;
                                }

                                if (self.config.before_save != null) {
                                    var fn = Function(self.config.before_save);
                                    var result = fn();

                                    if (result == false) return;
                                }

                                var item = form.getValues();

                                var khData = {};

                                khData.windowid = String(rp["data"][0]["code"]);
                                khData.editmode = editmode;
                                khData.data = [];

                                var vc_window = rp["data"][0];

                                if (vc_window.window_type == "MasterDetail") {
                                    var tabs = vc_window.Tabs;
                                    var details = [];

                                    for (var i = 0; i < tabs.length; i++) {
                                        var tab = tabs[i];

                                        if (tab.tab_type == "Detail") {
                                            var table = webix.$$("edit_" + tab.table_name);

                                            details.push({
                                                tab_id: tab.code,
                                                tab_table: tab.table_name,
                                                data: table.serialize(),
                                            });
                                        }
                                    }

                                    item["details"] = details;
                                }

                                khData.data.push(item);

                                self.getTopParentView().disable();
                                self.getTopParentView().showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/System/Save", khData, function (text) {
                                        var json = JSON.parse(text);

                                        $$(json.windowid).enable();
                                        $$(json.windowid).hideProgress();

                                        if (!json.hasOwnProperty("error")) {
                                            var after_save = $$(json.windowid).config.after_save;

                                            if (after_save != null) {
                                                //Function
                                                var fn = Function("editmode", "data", after_save);
                                                fn(editmode, json.data);
                                                //console.log(editmode);
                                            }

                                            $$(json.windowid).close();
                                            app.callEvent("dataChanged", [editmode, json.data]);
                                        } else {
                                            webix.message(json.error, "error", 2000);
                                        }
                                    });
                            }, 100);
                        },
                    },
                    click: function () {
                        this.callEvent("onClick");
                    },
                },
                {
                    view: "button",
                    shortcut: "esc",
                    label: "HỦY (ESC)",
                    type: "danger",
                    align: "center",
                    width: 110,
                    on: {
                        onClick: function () {
                            this.getTopParentView().close();
                        },
                    },
                    click: function () {
                        this.callEvent("onClick");
                    },
                },
                {},
            ];

            el.push({
                // paddingY:10,
                margin: 10,
                height: 40,
                cols: cols,
            });

            webix.ui(el, $$("window-form"));
            //console.log(el);

            win.getHead().setHTML(windowconfig.data[0].name);
        },
        focusFirstElement: function () {
            var form = $$("window-form");
            form.focus(form.elements[0]);
        },
        setEditMode: function (mode) {
            editmode = mode;

            var form = webix.$$("window-form");
            var topParentView = form.getTopParentView();
            topParentView.config.editmode = mode;
        },
        setValues: function (item, winconfig, params) {
            var form = webix.$$("window-form");
            var vc_window = winconfig["data"][0];

            if (item == null) {
                var tabs = vc_window.Tabs;

                item = {};

                if (params != null) {
                    item = params;
                }

                for (var i = 0; i < tabs.length; i++) {
                    var tab = tabs[i];
                    if (tab.tab_type == "Normal" || tab.tab_type == "Master") {
                        var fields = tab.Fields;

                        for (var j = 0; j < fields.length; j++) {
                            var field = fields[j];

                            if (field.column_name == "branch_code") {
                                item[field.column_name] = user.getDvcsID();
                            }

                            if (field.default_value != null) {
                                var defaultValue = field.default_value.toString();

                                if (defaultValue.startsWith("@Default=")) {
                                    item[field.column_name] = defaultValue.replace("@Default=", "");
                                }

                                if (defaultValue.startsWith("@JS=")) {
                                    var fn = Function(defaultValue.replace("@JS=", ""));
                                    item[field.column_name] = fn();
                                }
                            }
                        }
                    }
                }

                form.setValues(item);
            } else {
                form.setValues(item);

                var tabs = vc_window.Tabs;

                for (var i = 0; i < tabs.length; i++) {
                    var tab = tabs[i];

                    if (tab.tab_type != "Detail") {
                        var fields = tab.Fields;

                        for (var j = 0; j < fields.length; j++) {
                            var field = fields[j];
                            //if (field.hidden != true) {
                            if (field.column_type == "date" || field.column_type == "datetime") {
                                if (item[field.column_name] != null && item[field.column_name] != undefined) {
                                    item[field.column_name] = removeTimeZoneT(item[field.column_name]);
                                    form.setValues(item);
                                }
                            }
                            //}
                        }

                        break;
                    }
                }

                if (vc_window.window_type == "MasterDetail") {
                    for (var i = 0; i < tabs.length; i++) {
                        var tab = tabs[i];

                        if (tab.tab_type == "Master") {
                            continue;
                        }

                        var table = webix.$$("edit_" + tab.table_name);

                        var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id=" + vc_window.code + "&id=" + item.id + "&tab_table=" + tab.table_name;
                        table.clearAll();
                        // if(editmode == 4) continue;
                        table.load(url);
                    }
                }
            }
        },
        copyValues: function (item, winconfig) {
            var form = webix.$$("window-form");
            var vc_window = winconfig["data"][0];

            if (vc_window.window_type == "MasterDetail") {
                var tabs = vc_window.Tabs;

                for (var i = 0; i < tabs.length; i++) {
                    var tab = tabs[i];

                    if (tab.tab_type == "Master") {
                        continue;
                    }

                    var table = webix.$$("edit_" + tab.table_name);

                    var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id=" + vc_window.code + "&id=" + item.id + "&tab_table=" + tab.table_name;
                    table.clearAll();

                    if (vc_window.code == "WIN00008") {
                        continue;
                    }

                    table.load(url);
                }
            }

            var tabs = vc_window.Tabs;

            for (var i = 0; i < tabs.length; i++) {
                var tab = tabs[i];

                if (tab.tab_type == "Normal" || tab.tab_type == "Master") {
                    var fields = tab.Fields;

                    for (var j = 0; j < fields.length; j++) {
                        var field = fields[j];

                        if (field.column_name == tab.table_name + "_id") {
                            item[field.column_name] = null;
                        }

                        if (field.column_name == "branch_code") {
                            item[field.column_name] = user.getDvcsID();
                        }

                        if (field.default_value != null) {
                            var defaultValue = field.default_value.toString();

                            if (defaultValue.startsWith("@Default=")) {
                                item[field.column_name] = defaultValue.replace("@Default=", "");
                            }

                            if (defaultValue.startsWith("@JS=")) {
                                var fn = Function(defaultValue.replace("@JS=", ""));
                                item[field.column_name] = fn();
                            }
                        }
                    }
                }
            }

            form.setValues(item);
        },
    };
});
