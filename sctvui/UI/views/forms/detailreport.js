define([
	"app",
	"locale",
	"views/forms/infowindow",
	"views/forms/window"
],function(app,_,infowindow,windowform){
	var editmode = 0;
	var tableId = 0;

	var contextmenu = webix.ui({
            view:"contextmenu",
            id:"ctxMenuDetail",
            on:{
                onMenuItemClick:function(id){
                    var item = this.getItem(id);

                    if (id=="SUACTGOCDETAIL"){
                        var table =this.getContext().obj;
                        var row = table.getSelectedItem();                        

                        var sql = "SELECT TOP 1 a.* FROM VC_WINDOW a WHERE a.ma_ct='"+row.MA_CT+"'";
                        var url = app.config.host + "/api/System/ExecuteQuery?sql=" + sql;

						var ajax = webix.ajax();
						ajax.master = table;

                        ajax.get(url,function(text){
                            var json = JSON.parse(text);
                            var data = json[0];
                            
                            var table = this;
                            var row = table.getSelectedItem();

                            var a = webix.ajax(app.config.host + "/api/System/GetDataById?window_id="+data.WINDOW_ID + "&id="+row.DOC_ID);
                            var b = webix.ajax(app.config.host + "/api/System/GetAllByWindowNo?window_id="+ data.WINDOW_ID);
							var c = table.config.id;

                            webix.promise.all([a,b,c]).then(function(results){
                                var a_result = results[0].json();
                                var windowconfig = results[1].json();

                                var vc_window = windowconfig["data"][0];
                                windowform.$ui.id = vc_window.WINDOW_ID;

                                var table = $$(c);
                                var detailForm = table.$scope.ui(windowform.$ui);
                            
                                var item = a_result.data[0];              
                                
                                windowform.initUI(windowconfig);
                                windowform.setEditMode(2);
                                windowform.setValues(item,windowconfig);
                                
                                detailForm.show();
                            });
                            
                        });
                    }
                    else {
                        var ctx =this.getContext();

                        detailreport.$ui.id = "dtrp_"+ item.MENU_ID;
                        detailreport.$ui.parentParameter = ctx.obj.config.rpParameter.parameters;
                        detailreport.$ui.parentRow = ctx.obj.getSelectedItem();

                        this.hide();

                        var win = ctx.obj.$scope.ui(detailreport.$ui);
                        detailreport.initUI(item);
                        win.show();
                    }
                    

                    
                }
            }
    });


	function initData(windowId,tableId,infoWindowId,report_id){

		var win = $$(windowId);
		var table = win.getChildViews()[1].getChildViews()[1];

		var menuitem = win.config.menuitem;

		var parentParamter = win.config.parentParameter;
		var parentRow = win.config.parentRow;

		infowindow.$ui.id = infoWindowId;
		infowindow.$ui.report_id = report_id;
		infowindow.$ui.editmode = 1;
		infowindow.$ui.load_event = "loadDetailReport";
		infowindow.$ui.menuitem = menuitem;

		var parameters = menuitem.PARAMETERS								
								.split(';');

		var item = {};

		for(var i=0;i<parameters.length;i++){
			var array = parameters[i]
								.replace("{","")
								.replace("}","")
								.split(':');

			if (array[0].toString()=="1"){
				item[array[2]] = parentParamter[array[1]];
			}

			if (array[0].toString()=="2"){
				item[array[2]] = parentRow[array[1]];
			}
		}

		table.config.rpParameter = {
			parameters : item
		};

		infowindow.$ui.formvalues = item;

		var wininfo = win.$scope.ui(infowindow.$ui);
		
		infowindow.initUI(infoWindowId);
		
		app.callEvent("loadDetailReport",[report_id,infoWindowId,item,menuitem]);
	}

	return {		
		$ui:{   
			view:"window", modal:true, 
			position:"center",
			head:"Thông tin Tab",
			fullscreen:true,
			body:{
				rows:[
					{
						view:"toolbar",
						height:35,
						cols:[
							{
								view:"button",
								type:"icon",
								icon:"refresh",
								label: _("REFRESH"),
								width:90,
								on: {
									onClick: function(){ 
										var win = this.getTopParentView();										
										var table = win.getChildViews()[1].getChildViews()[1];

										infowindow.$ui.id = table.config.infowindow_id;
										infowindow.$ui.report_id = table.config.report_id;
										infowindow.$ui.load_event = "loadDetailReport";
										infowindow.$ui.menuitem = win.config.menuitem;

										if (table.config.rpParameter!=null){									
											infowindow.$ui.editmode = 1;
											infowindow.$ui.formvalues = table.config.rpParameter.parameters;
										}
										else {
											infowindow.$ui.editmode = 0;
											infowindow.$ui.formvalues = null;
										}              

										var wininfoparam = this.$scope.ui(infowindow.$ui);

										infowindow.initUI(table.config.infowindow_id);              
										wininfoparam.show();                    
									}
								},
								click: function(){
									this.callEvent("onClick");
								}
							},
							{
								view:"button",
								type:"icon",
								icon:"print",
								label: _("PRINT"),
								width:40,
								click: function(){
									this.callEvent("onClick");
								},
								on: {
									onClick: function(){
										var win = this.getTopParentView();										
										var table = win.getChildViews()[1].getChildViews()[1];

										var rpParameter = table.config.rpParameter;
										
										rpParameter.type = "pdf";

										if (rpParameter!=null){
											webix.ajax()
												.response("arraybuffer")
												.headers({'Content-type':'application/json'})
												.post(app.config.host + "/api/System/CreateReport", rpParameter, function(text, data){
												var file = new Blob([data], {type: 'application/pdf'});
												var fileURL = URL.createObjectURL(file);
												window.open(fileURL,'_blank');
											});
										}
									}
								}
							},
							{
								view:"button",
								type:"icon",
								icon:"file",
								label: _("EXPORT"),
								width:80,
								click: function(){
									this.callEvent("onClick");
								},
								on: {
									onClick: function(){
										var win = this.getTopParentView();										
										var table = win.getChildViews()[1].getChildViews()[1];
										
										var rpParameter = table.config.rpParameter;
										rpParameter.type = "excel";

										if (rpParameter!=null){
											webix.ajax()
												.response("arraybuffer")
												.headers({'Content-type':'application/json'})
												.post(app.config.host + "/api/System/CreateReport", rpParameter, function(text, data){
												
												var file = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
												var fileURL = URL.createObjectURL(file);                        
																			
												window.location.href = fileURL;
											});
										}
									}
								}
							},
							{
								view:"button",
								type:"icon",
								icon:"close",
								label: _("EXIT"),
								width:80,
								click: function(){
									this.getTopParentView().close();
								}
							}
						]
					},
					{
						view:"datatable",
						scheme:{
							$change:function(item){
								if (item.BOLD == "C")
									item.$css = "rowBold";
							}
						},
						select: "row",
						on:{
							onBeforeContextMenu:function(id){
								var item = this.getItem(id);
								var mn = $$("ctxMenuDetail");

								if (item.DOC_ID!=null && item.MA_CT!=null){									
									var menuitem = mn.getItem("SUACTGOCDETAIL");
									if (menuitem==null){
										mn.add({id:"SUACTGOCDETAIL",value:"Sửa chứng từ"});
									}
								}
								else {
									mn.clearAll();
								}

								webix.delay(function(){
									this.clearSelection();
									this.select(id.row, id.column);
								},this);
							} 
						}
					}
				]
                
			}			
			
		},		
		setEditMode: function(mode) {
			editmode = mode;
		},
		setTableId: function(id){
			tableId = id;
		}, 
		initUI: function(item){
			var report_id = item.REPORT_ID;

			webix.$$("dtrp_" + item.MENU_ID).config.menuitem = item;			

			var sql = "SELECT a.*,b.INFOWINDOW_ID,'"+item.MENU_ID+"' as MENU_ID FROM VC_REPORTCOLUMN a "
					+" INNER JOIN VC_REPORT b ON b.id=a.VC_REPORT_id "
					+" WHERE a.VC_REPORT_id='"+report_id+"' ORDER BY a.SEQ_NO";

			var url=app.config.host + "/api/System/ExecuteQuery?sql=" + sql;
			
			webix.ajax(url,function(text){
				var json = JSON.parse(text);

				var win = webix.$$("dtrp_" + json[0]["MENU_ID"]);
				win.getHead().setHTML(win.config.menuitem.CAPTION);

				var table = win.getChildViews()[1].getChildViews()[1];
				table.config.rpParameter = null;
				table.clearAll();

				$$("ctxMenuDetail").attachTo(table);

				var cols=[];

				for(var i=0;i<json.length;i++){
					var col=json[i];

					var item = {
						id: col.REPORTCOLUMN_FIELD,
						header:"<center>" + _(col.REPORTCOLUMN_NAME) +"</center>",
						width: col.REPORTCOLUMN_WIDTH,
						hidden: col.HIDDEN == "C" ? true : false
					};

					if (col.HEADER!=null){
						if (col.HEADER!=""){
							var headers = JSON.parse(col.HEADER);

							for(var j=0;j<headers.length;j++){
								var hd = headers[j];
								if (hd!=null){
									hd.text = "<center>" + _(hd.text) +"</center>";
								}
							}

							item.header = headers;
						}
						
					}

					if (col.COLUMN_TYPE == "VC_DATE"){
						item.format = function(text) {
							var format = webix.Date.dateToStr("%d/%m/%Y");
							text = removeTimeZoneT(text);                            
							return format(text);
						};
					}

					if (col.COLUMN_TYPE == "VC_TYGIA" || col.COLUMN_TYPE == "VC_TIEN"){
						item.css = { "text-align" : "right" };
						item.format = webix.Number.numToStr({
								groupDelimiter:",",
								groupSize:3,
								decimalDelimiter:".",
								decimalSize:0
							});
					}

					cols.push(item);
				}

			
				table.config.columns = cols;
				table.config.report_id = json[0]["VC_REPORT_id"];
				table.config.infowindow_id = json[0]["INFOWINDOW_ID"];

				table.refreshColumns();	
				
				initData(win.config.id,table.config.id,json[0]["INFOWINDOW_ID"],json[0]["VC_REPORT_id"]);

			});
		},
		$oninit: function(view,$scope) {
			
			 $scope.on(app, "loadDetailReport", function(report_id,infoWindowId,item,menuitem){
				
                var win = $$(infoWindowId);
				var table = win.getChildViews()[1].getChildViews()[1];

                if (table.config.report_id == report_id){

                    var rpParameter={
                        parameters: item,
                        infowindow_id: infoWindowId,
                        type: "json",
                        report_id: report_id
                    };

                    table.config.rpParameter = rpParameter;

                    webix.ajax()
                        .headers({'Content-type':'application/json'})
                        .post( app.config.host + "/api/System/CreateReport", rpParameter, function(text){
                            var data = JSON.parse(text);

                            var table = $$("tblReport");
                            table.clearAll();
                            table.parse(data,"json");
                        });
                }

                
            });
        }
	};

});