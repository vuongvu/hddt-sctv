define(["app", "models/user"], function (app, user) {
    var editmode = 0;
    var param;

    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Nhận (F10)",
            type: "form",
            align: "center",
            width: 120,
            on: {
                onClick: function () {
                    var self = this;
                    setTimeout(function () {
                        var item = $$("kyhieu-form").getValues();
                        var table = $$("kyhieu-form");

                        var khhdon = item.khhdon;

                        if (item.lhdon != khhdon[0] || item.hthuc != khhdon[1] || item.khdon != khhdon[4]) {
                            webix.message("Ký hiệu không hợp lệ", "error");
                            return;
                        }

                        if (table.validate()) {
                            webix.extend(table, webix.ProgressBar);
                            table.disable();
                            table.showProgress();

                            item.editmode = editmode;
                            item.code = param.code;
                            webix
                                .ajax()
                                .headers({ "Content-type": "application/json" })
                                .post(app.config.host + "/Invoice68/quanlykyhieu68", item, {
                                    error: function (text, data, XmlHttpRequest) {
                                        var table = $$("kyhieu-form");
                                        table.enable();
                                        table.hideProgress();
                                        // var json = JSON.parse(text);
                                        // webix.message(json.ExceptionMessage, "error");
                                        webix.message(text + XmlHttpRequest, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var json = JSON.parse(text);
                                        var table = $$("kyhieu-form");
                                        table.enable();
                                        table.hideProgress();
                                        if (!json.hasOwnProperty("error")) {
                                            $$("kyhieu-win").close();
                                            var table = $$("kyhieu");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        } else {
                                            webix.message(json.error, "error");
                                        }
                                    },
                                });
                        }
                    }, 100);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            hotkey: "esc",
            label: "Hủy bỏ (ESC)",
            type: "danger",
            align: "center",
            width: 120,
            click: function () {
                webix.$$("kyhieu-win").close();
            },
        },
        {},
    ];

    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "kyhieu-win",
        position: "center",
        head: "Đăng ký ký hiệu hóa đơn",
        on: {
            onBeforeShow: function () {
                if (editmode == 1) {
                    var date = new Date();
                    var year = date.getFullYear();
                    $$("khhdon").setValue("1K" + year.toString().substring(2, 4) + "TYY");
                }
            },
        },
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            width: 500,
            id: "kyhieu-form",
            complexData: true,
            margin: 0,
            elements: [
                // {
                //     view: "text",
                //     label: "Tên mẫu",
                //     name: "ten_mau",
                //     required: true,
                //     invalidMessage: "Tên mẫu không được bỏ trống",
                //     labelWidth: 120
                // },
                {
                    view: "combo",
                    name: "lhdon",
                    label: "Loại hóa đơn",
                    //labelPosition: "top",
                    id: "lhdon",
                    required: true,
                    labelWidth: 120,
                    value: "1",
                    invalidMessage: "Loại hóa đơn không được bỏ trống !",
                    options: [
                        { id: "1", value: "Hóa đơn GTGT" },
                        { id: "2", value: "Hóa đơn bán hàng" },
                        { id: "3", value: "Hóa đơn điện tử bán tài sản công" },
                        { id: "4", value: "Hóa đơn điện tử bán hàng dự trữ quốc gia" },
                        { id: "5", value: "Hóa đơn khác: tem, vé, thẻ, phiếu..." },
                        { id: "6", value: "Phiếu xuất kho kiêm vận chuyển nội bộ/ hàng gửi bán đại lý" },
                    ],
                    on: {
                        onChange: function (newv, oldv) {
                            var kyhieu = $$("khhdon").getValue();
                            var res = kyhieu.replace(kyhieu.substring(0, 1), newv);
                            $$("khhdon").setValue(res);
                            $$("khhdon").refresh();
                        },
                    },
                },
                {
                    view: "combo",
                    label: "Hình thức",
                    name: "hthuc",
                    id: "hthuc",
                    required: true,
                    invalidMessage: "Hình thức không được bỏ trống",
                    labelWidth: 120,
                    value: "K",
                    options: [
                        { id: "K", value: "Hóa đơn không có mã của CQT" },
                        { id: "C", value: "Hóa đơn có mã của CQT" },
                    ],
                    on: {
                        onChange: function (newv, oldv) {
                            var kyhieu = $$("khhdon").getValue();

                            var res = kyhieu.replace(kyhieu.substring(1, 2), newv);
                            $$("khhdon").setValue(res);
                            $$("khhdon").refresh();
                        },
                    },
                },
                {
                    view: "combo",
                    label: "Kiểu hóa đơn",
                    name: "khdon",
                    id: "khdon",
                    labelWidth: 120,
                    required: true,
                    invalidMessage: "Hình thức không được bỏ trống",
                    value: "T",
                    options: [
                        { id: "T", value: "Tự đăng ký sử dụng" },
                        { id: "D", value: "Hóa đơn đặc thù" },
                        { id: "L", value: "Hóa đơn CQT cấp từng lần" },
                        { id: "M", value: "Hóa đơn khởi tạo từ máy tính tiền" },
                        { id: "N", value: "PXKKVCNB" },
                        { id: "B", value: "PXKGBĐL" },
                        { id: "G", value: "Tem, vé, thẻ điện tử là hóa đơn giá trị gia tăng" },
                        { id: "H", value: "Tem, vé, thẻ điện tử là hóa đơn bán hàng" },
                    ],
                    on: {
                        onChange: function (newv, oldv) {
                            var kyhieu = $$("khhdon").getValue();

                            var res = kyhieu.replace(kyhieu.substring(4, 5), newv);
                            $$("khhdon").setValue(res);
                            $$("khhdon").refresh();
                        },
                    },
                },
                {
                    view: "text",
                    label: "Ký hiệu",
                    name: "khhdon",
                    id: "khhdon",
                    labelWidth: 120,
                    required: true,
                    invalidMessage: "Ký hiệu không được bỏ trống",
                    attributes: {
                        maxlength: 7,
                    },
                    // validate: webix.rules.isMauSo
                },
                {
                    id: "sdmau",
                    name: "sdmau",
                    label: "Số dòng in mẫu",
                    labelWidth: 120,
                    view: "text",
                    value: 1,
                    validate: function (val) {
                        return !isNaN(val * 1) && val * 1 >= 0;
                    },
                    invalidMessage: "Phải nhập số dương",
                    attributes: { type: "number" },
                },
                {
                    view: "combo",
                    label: "Mẫu thiết kế",
                    name: "qlmtke_id",
                    id: "qlmtke_id",
                    labelWidth: 120,
                    required: true,
                    invalidMessage: "Mẫu không được bỏ trống",
                    options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00053",
                    on: {
                        onChange: function (newv, oldv) {},
                    },
                },
                {
                    margin: 10,
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },
        initUI: function (item) {
            param = item;
        },
        setValues: function (item) {
            $$("kyhieu-form").setValues(item);
        },
    };
});
