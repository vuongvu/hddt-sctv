define(["app", "locale", "models/user", "views/commonHD"], function (app, _, user, commonHD) {
    var editmode = 0;
    var mau04 = {};
    var formatSoDefault = {
        groupDelimiter: ",",
        groupSize: 3,
        decimalDelimiter: ".",
        decimalSize: 0,
    };
    function f(a) {
        return webix.Number.format(a, formatSoDefault);
    }
    function g(a) {
        return webix.Number.parse(a, formatSoDefault);
    }
    /*function fmNumber(a){
        if(a !== undefined)
            return f(a);
        return a;
    };*/
    var fmNumber = {
        parse: function (a) {
            return g(a);
        },
        edit: function (a) {
            if (a !== undefined) return f(a);
            return a;
        },
    };

    var mess = "Tổng tiền chiết khấu lớn hơn tổng tiền hàng hóa thông thương trên hóa đơn. Vui lòng lập riêng hóa đơn chiết khấu mới.";

    function renderFormat(val) {
        if (val === undefined) val = "VND";
        var url = app.config.host + "/Invoice68/GetDecimalPlace?ma_nt=" + val;
        var table_chitiet = webix.$$("hoadon68_chitiet");
        var table_phi = webix.$$("hoadon68_phi");
        var table = $$("invoiceForms");

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .get(url, function (text) {
                var arr = JSON.parse(text);
                var decimal_place = Array(Number(arr[0]["hoadon123"]) + 1).join("9");
                var decimal_format = Number(arr[0]["hoadon123"]);
                var format_numeric = {
                    maximumValue: Number("999999999999." + decimal_place),
                    minValue: Number("-999999999999." + decimal_place),
                    decimalPlaces: Number(decimal_format),
                };
                formatSoDefault = {
                    groupDelimiter: ",",
                    groupSize: 3,
                    decimalDelimiter: ".",
                    decimalSize: decimal_format,
                };

                var formatSo = webix.Number.numToStr(formatSoDefault);

                var arrayString_chitiet = ["sluong", "dgia", "thtien", "stckhau", "tthue", "tgtien"];
                for (var i = 0; i < arrayString_chitiet.length; i++) {
                    var format_config = table_chitiet.getColumnConfig(arrayString_chitiet[i]);

                    format_config.format = formatSo;

                    format_config.numericConfig = format_numeric;
                }

                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];

                /*var format_config_phi = table_phi.getColumnConfig("tienphi");
                format_config_phi.format = formatSo;
                format_config_phi.numericConfig =format_numeric;*/

                table_chitiet.refreshColumns();
                //table_phi.refreshColumns();

                for (var i = 0; i < arrayString.length; i++) {
                    $$(arrayString[i]).refresh();
                }
            });
    }
    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            css: "headformchil",
            head: {
                height: 35,
                cols: [
                    {
                        id: "head-iconInvoice",
                        view: "template",
                        css: "headform",
                        width: 35,
                        template: "",
                    },
                    {
                        id: "head-formInvoice",
                        view: "template",
                        css: "headform",
                        template: function (obj) {
                            return "<div>Phiếu xuất kho kiêm vận chuyển nội bộ<div/>";
                        },
                    },
                    {
                        view: "icon",
                        icon: "fas fa-question-circle",
                        id: "help_InvoiceForms",
                        tooltip: _("HELPER"),
                        click: function () {},
                    },
                ],
            },
            id: "invoice68",
            resize: true,
            fullscreen: true,
            move: true,
            on: {
                onBeforeShow: function () {},
            },
            body: {
                paddingY: 10,
                paddingX: 10,
                elementsConfig: {
                    labelWidth: 100,
                },
                view: "form",
                id: "invoiceForms",
                width: 800,
                height: 500,
                complexData: true,
                margin: 0,
                on: {},
                elements: [
                    {
                        view: "scrollview",
                        scroll: "y",
                        body: {
                            paddingX: 18,
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: "fieldset",
                                            label: "Thông tin hóa đơn",
                                            body: {
                                                rows: [
                                                    {
                                                        cols: [
                                                            {
                                                                view: "text",
                                                                label: "cctbao_id",
                                                                id: "cctbao_id",
                                                                name: "cctbao_id",
                                                                hidden: true,
                                                            },
                                                            {
                                                                view: "datepicker",
                                                                name: "nlap",
                                                                label: "Ngày hóa đơn",
                                                                //labelPosition: "top",
                                                                id: "nlap",
                                                                required: true,
                                                            },
                                                            {
                                                                view: "text",
                                                                name: "shdon",
                                                                id: "shdon",
                                                                label: "Số HĐ",
                                                                css: "mcls_readonly_text",
                                                                readonly: true,
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        cols: [
                                                            {
                                                                view: "text",
                                                                label: _("Ký hiệu"),
                                                                name: "khieu",
                                                                css: "mcls_readonly_text",
                                                                readonly: true,
                                                                id: "khieu",
                                                                attributes: {
                                                                    maxlength: 7,
                                                                },
                                                            },
                                                            {
                                                                view: "text",
                                                                label: _("Số đơn hàng"),
                                                                name: "sdhang",
                                                                id: "sdhang",
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        hidden: true,
                                                        cols: [
                                                            {
                                                                view: "combo",
                                                                name: "dvtte",
                                                                label: "Mã tiền tệ",
                                                                //labelPosition: "top",
                                                                id: "dvtte",
                                                                required: true,
                                                                value: "VND",
                                                                invalidMessage: "Trạng thái hóa đơn không thể bỏ trống !",
                                                                options: {
                                                                    view: "gridsuggest",
                                                                    value: "VND",
                                                                    template: "#value#",
                                                                    url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00188",
                                                                    body: {
                                                                        columns: [{ id: "value", header: _("Mã tiền tệ"), width: 120 }],
                                                                        dataFeed: function (filtervalue, filter) {
                                                                            if (filtervalue.length < 1) {
                                                                                this.load(this.config.url);
                                                                                return;
                                                                            }

                                                                            var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00188";
                                                                            var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                            this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                                        },
                                                                    },
                                                                },
                                                                on: {
                                                                    onChange: function (newv, oldv) {
                                                                        renderFormat(newv);
                                                                        var type_curency = $$("dvtte").getValue();

                                                                        if (type_curency != "VND") {
                                                                            $$("docngoaitetv").show();
                                                                            $$("docngoaitetv").setValue(1);
                                                                        } else {
                                                                            $$("docngoaitetv").hide();
                                                                        }

                                                                        commonHD.ExeCuteCheckReadCurencyTV($$("docngoaitetv").getValue());
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                view: "checkbox",
                                                                id: "docngoaitetv",
                                                                name: "docngoaitetv",
                                                                value: 0,
                                                                labelRight: _("Đọc ngoại tệ Tiếng Việt"),
                                                                labelWidth: 2,
                                                                width: 180,
                                                                hidden: true,
                                                                on: {
                                                                    onChange: function (newv, oldv) {
                                                                        commonHD.ExeCuteCheckReadCurencyTV(newv);
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                view: "text",
                                                                label: "Tỷ giá",
                                                                type: "number",
                                                                value: 1,
                                                                name: "tgia",
                                                                id: "tgia",
                                                                on: {
                                                                    onChange: function (newv, oldv) {
                                                                        if (newv == undefined) return;
                                                                        if (newv == "") return;
                                                                        if (!webix.rules.isNumber(newv)) {
                                                                            webix.message("Tỷ giá phải nhập số", "error");
                                                                            this.focus();
                                                                            return;
                                                                        }
                                                                    },
                                                                },
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        view: "text",
                                                        label: _("Lệnh nội bộ"),
                                                        name: "lddnbo",
                                                        id: "lddnbo",
                                                    },
                                                    {
                                                        view: "text",
                                                        label: _("Kho xuất hàng"),
                                                        name: "dckhoxuat",
                                                        id: "dckhoxuat",
                                                    },
                                                    {
                                                        view: "text",
                                                        label: _("Hợp đồng VC"),
                                                        name: "hdvc",
                                                        id: "hdvc",
                                                    },
                                                    {
                                                        view: "text",
                                                        label: _("Người xuất hàng"),
                                                        name: "hvtnxhang",
                                                        id: "hvtnxhang",
                                                    },
                                                    {
                                                        cols: [
                                                            {
                                                                view: "text",
                                                                label: _("Người VC"),
                                                                name: "tnvchuyen",
                                                                id: "tnvchuyen",
                                                            },
                                                            {
                                                                view: "text",
                                                                label: _("Phương tiện VC"),
                                                                name: "ptvchuyen",
                                                                id: "ptvchuyen",
                                                                labelWidth: 120,
                                                                required: true,
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        },
                                        {
                                            view: "fieldset",
                                            label: "Thông tin khách hàng",
                                            body: {
                                                rows: [
                                                    {
                                                        cols: [
                                                            {
                                                                view: "text",
                                                                editor: "textsearch",
                                                                label: "Mã khách hàng",
                                                                id: "mnmua",
                                                                name: "mnmua",
                                                                suggest: {
                                                                    view: "gridsuggest",
                                                                    columnName: "code",
                                                                    body: {
                                                                        columns: [
                                                                            { id: "id", hidden: true, header: "id" },
                                                                            { id: "code", width: 120, header: "code" },
                                                                            { id: "name", width: 350, header: "name" },
                                                                        ],
                                                                        scroll: true,
                                                                        autoheight: false,
                                                                        datatype: "json",
                                                                        dataFeed: function (filtervalue, filter) {
                                                                            if (filtervalue.length < 1) return;

                                                                            var refid = this.config.refid;
                                                                            var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00002";
                                                                            var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                            this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                                        },
                                                                    },
                                                                    on: {
                                                                        onValueSuggest: function (obj) {
                                                                            var table = $$("invoiceForms");
                                                                            var selectedItem = $$("invoiceForms").getValues();

                                                                            selectedItem["mnmua"] = obj["code"];
                                                                            selectedItem["tnmua"] = obj["buyer_name"];
                                                                            selectedItem["ten"] = obj["name"];
                                                                            selectedItem["dchi"] = obj["address"];
                                                                            selectedItem["mst"] = obj["tax_code"];
                                                                            selectedItem["email"] = obj["email"];
                                                                            selectedItem["stknmua"] = obj["bank_account"];
                                                                            table.setValues(selectedItem);
                                                                        },
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                id: "mst",
                                                                name: "mst",
                                                                view: "search",
                                                                label: "Mã số thuế",
                                                                //align: "center",
                                                                validate: webix.rules.isTaxCode,
                                                                invalidMessage: "Mã số thuế không hợp lệ",
                                                                placeholder: _("SEARCH") + "...",
                                                                // width: 300,
                                                                on: {
                                                                    onChange: function (newv, oldv) {
                                                                        if (newv == undefined) return;
                                                                        if (newv == "") return;
                                                                        if (!webix.rules.isTaxCode(newv)) {
                                                                            webix.message("Mã số thuế không hợp lệ", "error");
                                                                            this.focus();
                                                                            return;
                                                                        }
                                                                        // if (newv.length != 11) {
                                                                        //     webix.message({ type: "error", text: "Độ dài mẫu số phải là 11 ký tự" });
                                                                        //     this.focus();
                                                                        //     return;
                                                                        // }
                                                                    },
                                                                    onTimedKeyPress: function () {
                                                                        // var table = $$("tblSearch");
                                                                        // table.clearAll();
                                                                        // table.load(table.config.url);
                                                                    },
                                                                    onSearchIconClick: function (e) {
                                                                        //get value của form hiện tại để đổi giá trị
                                                                        var mst = this.getValue();
                                                                        // window.location.href = "http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst;
                                                                        window.open("http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst);
                                                                    },
                                                                },
                                                            },
                                                        ],
                                                    },
                                                    { view: "text", label: _("Tên đơn vị"), name: "ten", id: "ten" },
                                                    {
                                                        cols: [
                                                            { view: "text", label: _("Người nhập hàng"), name: "tnmua", id: "tnmua" },
                                                            {
                                                                view: "text",
                                                                label: _("Email"),
                                                                name: "email",
                                                                id: "email",
                                                                validate: function (value) {
                                                                    if (value) {
                                                                        var re = /\S+@\S+\.\S+/;
                                                                        return re.test(value);
                                                                    }
                                                                    return true;
                                                                },
                                                                invalidMessage: "Bạn nhập không đúng email",
                                                            },
                                                        ],
                                                    },
                                                    { view: "text", label: _("Địa chỉ"), name: "dchi", id: "dchi" },
                                                    {},
                                                    {},
                                                    {},
                                                ],
                                            },
                                        },
                                    ],
                                },
                                {
                                    borderless: true,
                                    view: "tabview",
                                    scrollX: true,
                                    tabbar: {
                                        width: 300,
                                    },
                                    cells: [
                                        {
                                            header: "Chi tiết hóa đơn",
                                            select: "cell",
                                            body: {
                                                rows: [
                                                    {
                                                        id: "mainInvoiceManage",
                                                        css: {
                                                            "padding-bottom": "5px !important",
                                                        },
                                                        cols: [
                                                            {
                                                                view: "button",
                                                                type: "htmlbutton",
                                                                width: 65,
                                                                label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F9)" + "</span>",
                                                                id: "addrow",
                                                                css: "btnForm",
                                                                on: {
                                                                    onItemClick: function (id, e) {
                                                                        var table = webix.$$("hoadon68_chitiet");
                                                                        var item = {
                                                                            id: webix.uid(),
                                                                        };
                                                                        table.add(item);
                                                                        table.select(item.id, "ma", false);
                                                                        table.edit({
                                                                            row: item.id,
                                                                            column: "ma",
                                                                        });
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                view: "button",
                                                                type: "htmlbutton",
                                                                width: 65,
                                                                label: '<i class="fa fa-minus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F8)" + "</span>",
                                                                id: "removerow",
                                                                css: "btnForm",
                                                                on: {
                                                                    onItemClick: function (id, e) {
                                                                        var table = webix.$$("hoadon68_chitiet");

                                                                        table.editStop();

                                                                        var item = table.getSelectedItem();

                                                                        if (!webix.isUndefined(item)) {
                                                                            table.remove(item.id);
                                                                            table.clearSelection();
                                                                            return;
                                                                        }

                                                                        var editor = table.getEditState();

                                                                        if (!webix.isUndefined(editor)) {
                                                                            table.remove(editor.row);
                                                                            table.clearSelection();
                                                                            return;
                                                                        }
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                view: "button",
                                                                type: "htmlbutton",
                                                                width: 65,
                                                                label: '<i class="fa fa-copy fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F7)" + "</span>",
                                                                view: "button",
                                                                id: "copyrow",
                                                                css: "btnForm",
                                                                on: {
                                                                    onItemClick: function (id, e) {
                                                                        var table = webix.$$("hoadon68_chitiet");

                                                                        table.editStop();

                                                                        var item = table.getSelectedItem();
                                                                        if (item == null || item == undefined) {
                                                                            webix.message("Bạn chưa chọn dòng cần copy !", "error");
                                                                            return;
                                                                        }
                                                                        var copy = webix.copy(item);
                                                                        copy.id = webix.uid();

                                                                        table.add(copy);
                                                                        table.unselect(item.id);
                                                                        table.select(copy.id, "ma", false);
                                                                        table.edit({
                                                                            row: copy.id,
                                                                            column: "ma",
                                                                        });
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                view: "checkbox",
                                                                id: "check_chietkhau",
                                                                value: 0,
                                                                labelRight: _("Chiết khấu"),
                                                                labelWidth: 2,
                                                                tooltip: "Tích vào đây nếu hóa đơn có chiết khấu",
                                                                on: {
                                                                    onChange: function (newv, oldv) {
                                                                        if (newv == 1) {
                                                                            $$("hoadon68_chitiet").showColumn("stckhau");
                                                                            $$("hoadon68_chitiet").showColumn("tlckhau");
                                                                        } else {
                                                                            $$("hoadon68_chitiet").hideColumn("stckhau");
                                                                            $$("hoadon68_chitiet").hideColumn("tlckhau");
                                                                        }
                                                                    },
                                                                },
                                                                hidden: true,
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        view: "datatable",
                                                        resizeColumn: true,
                                                        id: "hoadon68_chitiet",
                                                        editable: true,
                                                        select: "cell",
                                                        drag: true,
                                                        scroll: true,
                                                        columns: [
                                                            {
                                                                id: "stt",
                                                                header: "",
                                                                width: 30,
                                                            },
                                                            {
                                                                id: "ma",
                                                                header: _("Mã hàng hóa"),
                                                                editor: "textsearch",
                                                                width: 100,
                                                                editable: true,
                                                                suggest: {
                                                                    view: "gridsuggest",
                                                                    columnName: "ma_hanghoa",
                                                                    body: {
                                                                        columns: [
                                                                            { id: "id", hidden: true, header: "id" },
                                                                            { id: "code", width: 120, header: "code" },
                                                                            { id: "name", width: 350, header: "name" },
                                                                        ],
                                                                        scroll: true,
                                                                        autoheight: false,
                                                                        datatype: "json",
                                                                        dataFeed: function (filtervalue, filter) {
                                                                            if (filtervalue.length < 1) return;

                                                                            var refid = this.config.refid;
                                                                            var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00005";
                                                                            var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                            this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                                        },
                                                                    },
                                                                    on: {
                                                                        onValueSuggest: function (obj) {
                                                                            debugger;
                                                                            var table = $$("hoadon68_chitiet");
                                                                            var selectedItem = table.getSelectedItem();
                                                                            selectedItem["ma"] = obj["code"];
                                                                            selectedItem["ten"] = obj["name"];
                                                                            selectedItem["mdvtinh"] = obj["unit_code"];
                                                                            selectedItem["dgia"] = obj["unit_price"];
                                                                            selectedItem["tsuat"] = obj["vat_type"];
                                                                            table.updateItem(selectedItem.id, selectedItem);
                                                                        },
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                id: "ten",
                                                                header: _("Tên hàng hóa"),
                                                                editor: "text",
                                                                width: 350,
                                                                editable: true,
                                                            },
                                                            {
                                                                id: "mdvtinh",
                                                                header: _("ĐVT"),
                                                                editor: "text",
                                                                width: 80,
                                                                editable: true,
                                                                suggest: {
                                                                    view: "gridsuggest",
                                                                    columnName: "name",
                                                                    body: {
                                                                        columns: [
                                                                            { id: "id", hidden: true, header: "id" },
                                                                            { id: "name", width: 120, header: "name" },
                                                                        ],
                                                                        scroll: true,
                                                                        autoheight: false,
                                                                        datatype: "json",
                                                                        dataFeed: function (filtervalue, filter) {
                                                                            if (filtervalue.length < 1) return;

                                                                            var refid = this.config.refid;
                                                                            var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00035";
                                                                            var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                            this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                                        },
                                                                    },
                                                                    on: {
                                                                        onValueSuggest: function (obj) {
                                                                            var table = $$("hoadon68_chitiet");
                                                                            var selectedItem = table.getSelectedItem();
                                                                            selectedItem["mdvtinh"] = obj["code"];

                                                                            table.updateItem(selectedItem.id, selectedItem);
                                                                        },
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                id: "sluong",
                                                                header: _("Số lượng"),
                                                                editor: "autonumeric",
                                                                width: 100,
                                                                editable: true,
                                                                css: {
                                                                    "text-align": "right",
                                                                },
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                            },
                                                            {
                                                                id: "dgia",
                                                                header: _("Đơn giá"),
                                                                width: 150,
                                                                editor: "autonumeric",
                                                                editable: true,
                                                                css: {
                                                                    "text-align": "right",
                                                                },
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                            },
                                                            {
                                                                id: "thtien",
                                                                header: _("Tiền trước thuế"),
                                                                width: 150,
                                                                editor: "autonumeric",
                                                                editable: true,
                                                                css: {
                                                                    "text-align": "right",
                                                                },
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                                hidden: true,
                                                            },
                                                            {
                                                                id: "tlckhau",
                                                                header: _("% CK"),
                                                                width: 80,
                                                                editor: "autonumeric",
                                                                editable: true,
                                                                hidden: true,
                                                                css: {
                                                                    "text-align": "right",
                                                                },
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                            },
                                                            {
                                                                id: "stckhau",
                                                                header: _("Tiền CK"),
                                                                width: 150,
                                                                editor: "autonumeric",
                                                                editable: true,
                                                                css: {
                                                                    "text-align": "right",
                                                                },
                                                                hidden: true,
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                            },
                                                            {
                                                                id: "tsuat",
                                                                header: _("Thuế suất"),
                                                                width: 100,
                                                                //  editor: "combo",
                                                                editor: "autonumeric",
                                                                editable: true,
                                                                hidden: true,
                                                                css: { "text-align": "right" },
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                                // on: {
                                                                //     onChange: function (newv, oldv) {
                                                                //     }
                                                                // },
                                                                // options: app.config.host + "/System/GetDataByReferencesId?id=D6FAEAFC-4EF2-4ABD-AB45-F3DCB4805740",
                                                                // optionslist: true
                                                            },
                                                            {
                                                                id: "tthue",
                                                                header: _("Tiền thuế"),
                                                                width: 150,
                                                                editor: "autonumeric",
                                                                editable: true,
                                                                hidden: true,
                                                                css: { "text-align": "right" },
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                            },
                                                            {
                                                                id: "tgtien",
                                                                header: _("Thành tiền"),
                                                                width: 150,
                                                                editor: "autonumeric",
                                                                editable: true,
                                                                css: { "text-align": "right" },
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                            },
                                                            {
                                                                id: "kmai",
                                                                name: "kmai",
                                                                header: _("Tính chất"),
                                                                width: 200,
                                                                editor: "combo",
                                                                editable: true,
                                                                value: "1",
                                                                options: [
                                                                    { id: "1", value: "Hàng hóa dịch vụ" },
                                                                    { id: "2", value: "Khuyến mại" },
                                                                    // { id: "3", value: "Chiết khấu thương mại" },
                                                                    { id: "4", value: "Ghi chú" },
                                                                ],
                                                            },
                                                        ],
                                                        on: {
                                                            "data->onStoreUpdated": function () {
                                                                this.data.each(function (obj, i) {
                                                                    obj.stt = i + 1;
                                                                });
                                                            },
                                                            onAfterLoad: function () {
                                                                var table = this;
                                                                var data = table.serialize();
                                                                data.forEach(function (entry) {
                                                                    if (entry["stckhau"] != undefined || entry["tlckhau"] != undefined) {
                                                                        if (entry["stckhau"] != 0 || entry["tlckhau"] != 0) {
                                                                            var ck = $$("check_chietkhau");
                                                                            if (ck != undefined) {
                                                                                ck.setValue(1);
                                                                                return false;
                                                                            }
                                                                        }
                                                                    }
                                                                });
                                                            },
                                                            onCheck: function (row, column, state) {
                                                                var table = this;
                                                                if (column == "khuyen_mai") {
                                                                    if (state == true) {
                                                                    }
                                                                }
                                                            },
                                                            onAfterAdd: function (id, index) {
                                                                var table = $$(this.config.id);
                                                                var data = table.serialize();

                                                                var tong_tien_tt_dp = 0;
                                                                var tong_tien_thue_dp = 0;
                                                                var tong_tien_ck_dp = 0;
                                                                var tong_tien_dp = 0;
                                                                data.forEach(function (entry) {
                                                                    tong_tien_tt_dp += entry["tien_tt"] == null ? 0 : entry["tien_tt"];
                                                                    tong_tien_thue_dp += entry["tien_thue"] == null ? 0 : entry["tien_thue"];
                                                                    tong_tien_ck_dp += entry["tien_ck"] == null ? 0 : entry["tien_ck"];
                                                                    tong_tien_dp += entry["tong_tien"] == null ? 0 : entry["tong_tien"];
                                                                });
                                                            },
                                                            onAfterDelete: function (id) {
                                                                var table = $$(this.config.id);
                                                                var data = table.serialize();
                                                                var tien_tt_all = 0;
                                                                var tien_thue_all = 0;
                                                                var tien_ck_all = 0;
                                                                var tong_tien_all = 0;

                                                                var tien_tt_10 = 0;
                                                                var tien_thue_10 = 0;
                                                                var tien_ck_10 = 0;
                                                                var tong_tien_10 = 0;

                                                                var tien_tt_5 = 0;
                                                                var tien_thue_5 = 0;
                                                                var tien_ck_5 = 0;
                                                                var tong_tien_5 = 0;

                                                                var tien_tt_0 = 0;
                                                                var tien_thue_0 = 0;
                                                                var tien_ck_0 = 0;
                                                                var tong_tien_0 = 0;

                                                                var tien_tt_kct = 0;
                                                                var tien_thue_kct = 0;
                                                                var tien_ck_kct = 0;
                                                                var tong_tien_kct = 0;

                                                                var tien_tt_kkk = 0;
                                                                var tien_thue_kkk = 0;
                                                                var tien_ck_kkk = 0;
                                                                var tong_tien_kkk = 0;
                                                                data.forEach(function (entry) {
                                                                    if (entry["kmai"] == "1") {
                                                                        tien_tt_all += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                        tien_thue_all += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                        tien_ck_all += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                        tong_tien_all += entry["tgtien"] == null ? 0 : entry["tgtien"];

                                                                        if (entry["tsuat"] == 10) {
                                                                            tien_tt_10 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                            tien_thue_10 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                            tien_ck_10 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                            tong_tien_10 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                        }
                                                                        if (entry["tsuat"] == 5) {
                                                                            tien_tt_5 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                            tien_thue_5 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                            tien_ck_5 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                            tong_tien_5 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                        }
                                                                        if (entry["tsuat"] == 0) {
                                                                            tien_tt_0 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                            tien_thue_0 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                            tien_ck_0 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                            tong_tien_0 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                        }
                                                                        if (entry["tsuat"] == -1) {
                                                                            tien_tt_kct += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                            tien_thue_kct += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                            tien_ck_kct += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                            tong_tien_kct += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                        }
                                                                        if (entry["tsuat"] == -2) {
                                                                            tien_tt_kkk += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                            tien_thue_kkk += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                            tien_ck_kkk += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                            tong_tien_kkk += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                        }
                                                                    }
                                                                });

                                                                $$("tgtcthue").setValue(tien_tt_all);
                                                                $$("tgtcthue10").setValue(tien_tt_10);
                                                                $$("tgtcthue5").setValue(tien_tt_5);
                                                                $$("tgtcthue0").setValue(tien_tt_0);
                                                                $$("tgtcthuekct").setValue(tien_tt_kct);
                                                                $$("tgtcthuekkk").setValue(tien_tt_kkk);

                                                                $$("ttcktmai").setValue(tien_ck_all);
                                                                $$("ttcktmai10").setValue(tien_ck_10);
                                                                $$("ttcktmai5").setValue(tien_ck_5);
                                                                $$("ttcktmai0").setValue(tien_ck_0);
                                                                $$("ttcktmaikct").setValue(tien_ck_kct);
                                                                $$("ttcktmaikkk").setValue(tien_ck_kkk);

                                                                $$("tgtthue").setValue(tien_thue_all);
                                                                $$("tgtthue10").setValue(tien_thue_10);
                                                                $$("tgtthue5").setValue(tien_thue_5);
                                                                $$("tgtthue0").setValue(tien_thue_0);
                                                                $$("tgtthuekct").setValue(tien_thue_kct);
                                                                $$("tgtthuekkk").setValue(tien_thue_kkk);

                                                                $$("tgtttbso").setValue(tong_tien_all);
                                                                $$("tgtttbso10").setValue(tong_tien_10);
                                                                $$("tgtttbso5").setValue(tong_tien_5);
                                                                $$("tgtttbso0").setValue(tong_tien_0);
                                                                $$("tgtttbsokct").setValue(tong_tien_kct);
                                                                $$("tgtttbsokkk").setValue(tong_tien_kkk);

                                                                if ($$("tkcktmn").getValue() != null && $$("tkcktmn").getValue() != "") tong_tien_all -= Number($$("tkcktmn").getValue());
                                                                if ($$("tgtphi").getValue() != null && $$("tgtphi").getValue() != "") tong_tien_all += Number($$("tkcktmn").getValue());

                                                                $$("tgtttbso_last").setValue(tong_tien_all);

                                                                var tien_chietkhauthuongmai = 0;
                                                                data.forEach(function (entry) {
                                                                    if (entry["kmai"] && entry["kmai"].toString() == "3") {
                                                                        tien_chietkhauthuongmai += entry["tgtien"] == null ? 0 : Number(entry["tgtien"]);
                                                                    }
                                                                });
                                                                $$("tkcktmn").setValue(tien_chietkhauthuongmai);
                                                            },
                                                            onAfterEditStop: function (state, editor, ignore) {
                                                                var table_inv_views = webix.$$("invoiceViews");
                                                                var item_inv = table_inv_views.getSelectedItem();
                                                                var item_note = "";
                                                                if (item_inv != undefined && item_inv.hasOwnProperty("note") == true) {
                                                                    item_note = item_inv.note ? item_inv.note : "";
                                                                }
                                                                var table = $$(this.config.id);
                                                                1;
                                                                var selectedItem = table.getItem(editor.row);
                                                                var columnId = editor.config.id;
                                                                if (!selectedItem.kmai) selectedItem.kmai = "1";
                                                                var so_luong = webix.rules.isNumber(selectedItem.sluong) ? selectedItem.sluong : 0;
                                                                var don_gia = webix.rules.isNumber(selectedItem.dgia) ? selectedItem.dgia : 0;
                                                                var tien_tt = webix.rules.isNumber(selectedItem.thtien) ? selectedItem.thtien : 0;
                                                                var pt_ck = webix.rules.isNumber(selectedItem.tlckhau) ? selectedItem.tlckhau : 0;
                                                                var tien_ck = webix.rules.isNumber(selectedItem.stckhau) ? selectedItem.stckhau : 0;
                                                                var pt_thue = 0;
                                                                var tien_thue = webix.rules.isNumber(selectedItem.tthue) ? selectedItem.tthue : 0;
                                                                var tong_tien = webix.rules.isNumber(selectedItem.tgtien) ? selectedItem.tgtien : 0;

                                                                tien_tt += tien_ck;
                                                                if (
                                                                    columnId == "tsuat" ||
                                                                    columnId == "sluong" ||
                                                                    columnId == "dgia" ||
                                                                    columnId == "tlckhau" ||
                                                                    columnId == "tsuatstr" ||
                                                                    columnId == "kmai" ||
                                                                    columnId == "ma"
                                                                ) {
                                                                    if (item_note != "reduce_change_invoice") {
                                                                        if (don_gia < 0) {
                                                                            don_gia = 0;
                                                                        }
                                                                    }

                                                                    if (so_luong < 0) {
                                                                        so_luong = 0;
                                                                    }

                                                                    if (!(item_note == "reduce_change_invoice") && don_gia > 0) {
                                                                        tien_tt = so_luong * don_gia;
                                                                    } else tien_tt = so_luong * don_gia;

                                                                    if (pt_ck < 0) {
                                                                        pt_ck = 0;
                                                                    }
                                                                    if (pt_ck > 100) {
                                                                        pt_ck = 0;
                                                                    }
                                                                    tien_ck = (tien_tt * pt_ck) / 100;
                                                                    if (!(item_note == "reduce_change_invoice") && tien_ck - tien_tt > 0) {
                                                                        tien_ck = 0;
                                                                    }

                                                                    tien_thue = ((tien_tt - tien_ck) * (pt_thue < 0 ? 0 : pt_thue)) / 100;
                                                                    tong_tien = tien_tt - tien_ck + parseFloat(tien_thue);

                                                                    if (item_note == "reduce_change_invoice") {
                                                                        if (don_gia > 0) {
                                                                            don_gia = don_gia * -1;
                                                                            tien_tt = so_luong * don_gia;
                                                                            tien_ck = tien_ck * -1;
                                                                            tien_thue = tien_thue * -1;
                                                                            tong_tien = tong_tien * -1;
                                                                        }
                                                                    }
                                                                }
                                                                tien_tt -= tien_ck;
                                                                selectedItem.sluong = so_luong;
                                                                selectedItem.dgia = don_gia;
                                                                //selectedItem.tsuat = pt_thue;
                                                                selectedItem.thtien = tien_tt;
                                                                selectedItem.tlckhau = pt_ck;
                                                                selectedItem.stckhau = tien_ck;
                                                                selectedItem.tthue = tien_thue;
                                                                /*if(selectedItem.kmai.toString() !== "3"){
                                                                }*/
                                                                selectedItem.tgtien = tong_tien;
                                                                table.updateItem(selectedItem.id, selectedItem);
                                                                if (selectedItem.kmai.toString() === "2") {
                                                                    //selectedItem.sluong = null;
                                                                    //selectedItem.dgia = null;
                                                                    //selectedItem.tsuat = null;
                                                                    //selectedItem.tsuatstr = null;
                                                                    selectedItem.thtien = 0;
                                                                    //selectedItem.tlckhau = null;
                                                                    //selectedItem.stckhau = null;
                                                                    selectedItem.tthue = 0;
                                                                    selectedItem.tgtien = 0;
                                                                    if (columnId == "kmai") {
                                                                        selectedItem.sluong = null;
                                                                        selectedItem.dgia = null;
                                                                        selectedItem.tsuat = null;
                                                                        selectedItem.tsuatstr = null;
                                                                        selectedItem.tlckhau = null;
                                                                        selectedItem.stckhau = null;
                                                                    }
                                                                    table.updateItem(selectedItem.id, selectedItem);
                                                                } else if (selectedItem.kmai.toString() === "3") {
                                                                    //selectedItem.tsuat = "0";
                                                                    //selectedItem.tsuatstr = "0%";
                                                                    selectedItem.tlckhau = null;
                                                                    selectedItem.stckhau = null;
                                                                    //selectedItem.tthue = 0;
                                                                    /*if(columnId !== "tgtien"){
                                                                        selectedItem.tgtien = selectedItem.thtien
                                                                    }*/
                                                                } else if (selectedItem.kmai.toString() === "4") {
                                                                    selectedItem.sluong = null;
                                                                    selectedItem.dgia = null;
                                                                    selectedItem.tsuat = null;
                                                                    selectedItem.tsuatstr = null;
                                                                    selectedItem.thtien = null;
                                                                    selectedItem.tlckhau = null;
                                                                    selectedItem.stckhau = null;
                                                                    selectedItem.tthue = null;
                                                                    selectedItem.tgtien = null;
                                                                    table.updateItem(selectedItem.id, selectedItem);
                                                                }
                                                                var data = table.serialize();

                                                                var tien_tt_all = 0;
                                                                var tien_thue_all = 0;
                                                                var tien_ck_all = 0;
                                                                var tong_tien_all = 0;

                                                                var tien_tt_10 = 0;
                                                                var tien_thue_10 = 0;
                                                                var tien_ck_10 = 0;
                                                                var tong_tien_10 = 0;

                                                                var tien_tt_5 = 0;
                                                                var tien_thue_5 = 0;
                                                                var tien_ck_5 = 0;
                                                                var tong_tien_5 = 0;

                                                                var tien_tt_0 = 0;
                                                                var tien_thue_0 = 0;
                                                                var tien_ck_0 = 0;
                                                                var tong_tien_0 = 0;

                                                                var tien_tt_k = 0;
                                                                var tien_thue_k = 0;
                                                                var tien_ck_k = 0;
                                                                var tong_tien_k = 0;

                                                                var tien_tt_kct = 0;
                                                                var tien_thue_kct = 0;
                                                                var tien_ck_kct = 0;
                                                                var tong_tien_kct = 0;

                                                                var tien_tt_kkk = 0;
                                                                var tien_thue_kkk = 0;
                                                                var tien_ck_kkk = 0;
                                                                var tong_tien_kkk = 0;

                                                                data.forEach(function (entry) {
                                                                    if (entry.kmai && (entry.kmai.toString() === "1" || entry.kmai.toString() === "3")) {
                                                                        if (entry.kmai.toString() === "1") {
                                                                            tien_tt_all += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                            tien_thue_all += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                            tien_ck_all += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                            tong_tien_all += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            if (entry["tsuat"] == 10) {
                                                                                tien_tt_10 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_10 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_10 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_10 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == 5) {
                                                                                tien_tt_5 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_5 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_5 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_5 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == 0) {
                                                                                tien_tt_0 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_0 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_0 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_0 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == -1) {
                                                                                tien_tt_kct += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_kct += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_kct += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_kct += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == -2) {
                                                                                tien_tt_kkk += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_kkk += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_kkk += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_kkk += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else {
                                                                                tien_tt_k += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_k += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_k += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_k += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            }
                                                                        } else {
                                                                            tien_tt_all -= entry["thtien"] == null ? 0 : entry["thtien"];
                                                                            tien_thue_all -= entry["tthue"] == null ? 0 : entry["tthue"];
                                                                            tien_ck_all -= entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                            tong_tien_all -= entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            if (entry["tsuat"] == 10) {
                                                                                tien_tt_10 -= entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_10 -= entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_10 -= entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_10 -= entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == 5) {
                                                                                tien_tt_5 -= entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_5 -= entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_5 -= entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_5 -= entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == 0) {
                                                                                tien_tt_0 -= entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_0 -= entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_0 -= entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_0 -= entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == -1) {
                                                                                tien_tt_kct -= entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_kct -= entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_kct -= entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_kct -= entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else if (entry["tsuat"] == -2) {
                                                                                tien_tt_kkk -= entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_kkk -= entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_kkk -= entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_kkk -= entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            } else {
                                                                                tien_tt_k -= entry["thtien"] == null ? 0 : entry["thtien"];
                                                                                tien_thue_k -= entry["tthue"] == null ? 0 : entry["tthue"];
                                                                                tien_ck_k -= entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                                tong_tien_k -= entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                            }
                                                                        }
                                                                    }
                                                                });

                                                                $$("tgtcthue").setValue(tien_tt_all);
                                                                $$("tgtcthue10").setValue(tien_tt_10);
                                                                $$("tgtcthue5").setValue(tien_tt_5);
                                                                $$("tgtcthue0").setValue(tien_tt_0);
                                                                $$("tgtcthuek").setValue(tien_tt_k);
                                                                $$("tgtcthuekct").setValue(tien_tt_kct);
                                                                $$("tgtcthuekkk").setValue(tien_tt_kkk);

                                                                $$("ttcktmai").setValue(tien_ck_all);
                                                                $$("ttcktmai10").setValue(tien_ck_10);
                                                                $$("ttcktmai5").setValue(tien_ck_5);
                                                                $$("ttcktmai0").setValue(tien_ck_0);
                                                                $$("ttcktmaik").setValue(tien_ck_k);
                                                                $$("ttcktmaikct").setValue(tien_ck_kct);
                                                                $$("ttcktmaikkk").setValue(tien_ck_kkk);

                                                                $$("tgtthue").setValue(tien_thue_all);
                                                                $$("tgtthue10").setValue(tien_thue_10);
                                                                $$("tgtthue5").setValue(tien_thue_5);
                                                                $$("tgtthue0").setValue(tien_thue_0);
                                                                $$("tgtthuek").setValue(tien_thue_k);
                                                                $$("tgtthuekct").setValue(tien_thue_kct);
                                                                $$("tgtthuekkk").setValue(tien_thue_kkk);

                                                                $$("tgtttbso").setValue(tong_tien_all);
                                                                $$("tgtttbso10").setValue(tong_tien_10);
                                                                $$("tgtttbso5").setValue(tong_tien_5);
                                                                $$("tgtttbso0").setValue(tong_tien_0);
                                                                $$("tgtttbsok").setValue(tong_tien_k);
                                                                $$("tgtttbsokct").setValue(tong_tien_kct);
                                                                $$("tgtttbsokkk").setValue(tong_tien_kkk);

                                                                //if ($$("tkcktmn").getValue() != null && $$("tkcktmn").getValue() != "") tong_tien_all -= Number($$("tkcktmn").getValue());
                                                                if ($$("tgtphi").getValue() != null && $$("tgtphi").getValue() != "") tong_tien_all += Number($$("tgtphi").getValue());

                                                                $$("tgtttbso_last").setValue(tong_tien_all);
                                                                var tien_chietkhauthuongmai = 0;
                                                                data.forEach(function (entry) {
                                                                    if (entry["kmai"] && entry["kmai"].toString() == "3") {
                                                                        tien_chietkhauthuongmai += entry["tgtien"] == null ? 0 : Number(entry["tgtien"]);
                                                                    }
                                                                });
                                                                $$("tkcktmn").setValue(tien_chietkhauthuongmai);
                                                            },
                                                        },
                                                    },
                                                ],
                                            },
                                        } /*,
                                        {
                                            header: "Tiền phí/lệ phí",
                                            select: "cell",
                                            body: {
                                                rows: [
                                                    {
                                                        id: "mainInvoiceManage",
                                                        css: {
                                                            "padding-bottom": "5px !important",
                                                        },
                                                        cols: [
                                                            {
                                                                view: "button",
                                                                type: "htmlbutton",
                                                                width: 65,
                                                                label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F9)" + "</span>",
                                                                id: "addrow2",
                                                                css: "btnForm",
                                                                on: {
                                                                    onItemClick: function (id, e) {
                                                                        var table = webix.$$("hoadon68_phi");
                                                                        var item = {
                                                                            id: webix.uid(),
                                                                            tienphi:0
                                                                        };
                                                                        table.add(item);
                                                                        table.select(item.id);
                                                                        table.edit({
                                                                            row: item.id,
                                                                            column: "tnphi",
                                                                        });
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                view: "button",
                                                                type: "htmlbutton",
                                                                width: 65,
                                                                label: '<i class="fa fa-minus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F8)" + "</span>",
                                                                id: "removerow2",
                                                                css: "btnForm",
                                                                on: {
                                                                    onItemClick: function (id, e) {
                                                                        var table = webix.$$("hoadon68_phi");
                                                                        table.editStop();
                                                                        var item = table.getSelectedItem();
                                                                        if (!webix.isUndefined(item)) {
                                                                            table.remove(item.id);
                                                                            table.clearSelection();
                                                                            return;
                                                                        }

                                                                        var editor = table.getEditState();

                                                                        if (!webix.isUndefined(editor)) {
                                                                            table.remove(editor.row);
                                                                            table.clearSelection();
                                                                            return;
                                                                        }
                                                                    },
                                                                },
                                                            },
                                                            {
                                                                view: "button",
                                                                type: "htmlbutton",
                                                                width: 65,
                                                                label: '<i class="fa fa-copy fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F7)" + "</span>",
                                                                view: "button",
                                                                id: "copyrow2",
                                                                css: "btnForm",
                                                                on: {
                                                                    onItemClick: function (id, e) {
                                                                        var table = webix.$$("hoadon68_phi");

                                                                        table.editStop();

                                                                        var item = table.getSelectedItem();
                                                                        if (item == null || item == undefined) {
                                                                            webix.message("Bạn chưa chọn dòng cần copy !", "error");
                                                                            return;
                                                                        }
                                                                        var copy = webix.copy(item);
                                                                        copy.id = webix.uid();

                                                                        table.add(copy);
                                                                        table.unselect(item.id);
                                                                        table.select(copy.id);
                                                                        table.edit({
                                                                            row: copy.id,
                                                                            column: "tnphi",
                                                                        });
                                                                    },
                                                                },
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        view: "datatable",
                                                        resizeColumn: true,
                                                        id: "hoadon68_phi",
                                                        editable: true,
                                                        select: "cell",
                                                        drag: true,
                                                        columns: [
                                                            {
                                                                id: "tnphi",
                                                                name: "tnphi",
                                                                header: _("Tên phí"),
                                                                editor: "text",
                                                                width: 350,

                                                                editable: true,
                                                            },
                                                            {
                                                                id: "tienphi",
                                                                name: "tienphi",
                                                                header: _("Tiền phí"),
                                                                editor: "text",
                                                                width: 350,
                                                                editable: true,
                                                                format: webix.Number.numToStr({
                                                                    groupDelimiter: ".",
                                                                    groupSize: 3,
                                                                    decimalDelimiter: ",",
                                                                    decimalSize: 2,
                                                                }),
                                                            },

                                                        ],
                                                        on: {
                                                            onAfterEditStop: function (state, editor, ignore) {
                                                                var table = $$(this.config.id);
                                                                var data = table.serialize();
                                                                var tien_phi_moi = 0;
                                                                data.forEach(function (entry) {
                                                                    tien_phi_moi += entry["tienphi"] == null ? 0 : Number(entry["tienphi"]);
                                                                });
                                                                $$("tgtphi").setValue(tien_phi_moi);
                                                            },
                                                        }
                                                    }
                                                ]
                                            }
                                        }*/,
                                    ],
                                },
                                {
                                    hidden: true,
                                    cols: [
                                        {
                                            rows: [
                                                { view: "label", label: "Loại thuế", css: "lblTongtien", hidden: true },
                                                {
                                                    view: "label",
                                                    label: " + Tổng cộng hàng hóa, dịch vụ",
                                                    height: 22,
                                                    css: { "padding-left": "5px", "font-weight": "bold", "background-color": "#86b2cf" },
                                                    hidden: true,
                                                },
                                                { view: "label", label: "Thuế suất 10%", css: "lblSubTongtien", height: 22, hidden: true },
                                                { view: "label", label: "Thuế suất 5%", css: "lblSubTongtien", height: 22, hidden: true },
                                                { view: "label", label: "Thuế suất 0%", css: "lblSubTongtien", height: 22, hidden: true },
                                                { view: "label", label: "Thuế suất khác", css: "lblSubTongtien", height: 22, hidden: true },
                                                { view: "label", label: "Không chịu thuế GTGT", css: "lblSubTongtien", height: 22, hidden: true },
                                                { view: "label", label: "Không kê khai, tính nộp thuế GTGT", css: "lblSubTongtien", height: 22, hidden: true },
                                                {},
                                                {},
                                                {},
                                                // { view: "label", label: "Tổng cộng phí lệ phí", css: { "font-weight": "bold", "background-color": "#86b2cf" } , height: 22 },
                                                // { view: "label", label: "Tổng cộng thanh toán", css: { "font-weight": "bold", "background-color": "#86b2cf" } , height: 22 },
                                            ],
                                        },
                                        {
                                            rows: [
                                                { view: "label", label: "Thành tiền trước thuế", css: "lblTongtien", hidden: true },
                                                { view: "text", id: "tgtcthue", name: "tgtcthue", css: "txtTongtien", readonly: true, height: 22, format: fmNumber, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtcthue10", name: "tgtcthue10", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtcthue5", name: "tgtcthue5", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtcthue0", name: "tgtcthue0", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtcthuek", name: "tgtcthuek", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtcthuekct", name: "tgtcthuekct", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtcthuekkk", name: "tgtcthuekkk", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                {},
                                                {},
                                                {},
                                                // { view: "text", id: "2", name: "2", css: "txtTongtien", readonly: true, height: 22 }
                                            ],
                                        },

                                        {
                                            rows: [
                                                { view: "label", label: "Tiền chiết khấu", css: "lblTongtien", hidden: true },
                                                { view: "text", format: fmNumber, id: "ttcktmai", name: "ttcktmai", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "ttcktmai10", name: "ttcktmai10", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "ttcktmai5", name: "ttcktmai5", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "ttcktmai0", name: "ttcktmai0", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "ttcktmaik", name: "ttcktmaik", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "ttcktmaikct", name: "ttcktmaikct", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "ttcktmaikkk", name: "ttcktmaikkk", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                {},
                                                {},
                                                {},
                                            ],
                                        },
                                        {
                                            rows: [
                                                { view: "label", label: "Tiền thuế", css: "lblTongtien", hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtthue", name: "tgtthue", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtthue10", name: "tgtthue10", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtthue5", name: "tgtthue5", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtthue0", name: "tgtthue0", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtthuek", name: "tgtthuek", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtthuekct", name: "tgtthuekct", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtthuekkk", name: "tgtthuekkk", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "label", label: "Chiết khấu thương mại", labelAlign: "right", labelPosition: "top" },
                                                { view: "label", label: "Tổng tiền phí/lệ phí", labelAlign: "right", labelPosition: "top" },
                                                { view: "label", label: "Thành tiền", labelAlign: "right", labelPosition: "top" },
                                            ],
                                        },
                                        {
                                            rows: [
                                                { view: "label", label: "Tổng tiền sau thuế", css: "lblTongtien", hidden: true },
                                                {
                                                    view: "text",
                                                    id: "tgtttbso",
                                                    name: "tgtttbso",
                                                    css: "txtTongtien",
                                                    readonly: true,
                                                    height: 22,
                                                    format: fmNumber,
                                                    hidden: true,
                                                },
                                                { view: "text", format: fmNumber, id: "tgtttbso10", name: "tgtttbso10", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtttbso5", name: "tgtttbso5", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtttbso0", name: "tgtttbso0", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtttbsok", name: "tgtttbsok", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtttbsokct", name: "tgtttbsokct", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                { view: "text", format: fmNumber, id: "tgtttbsokkk", name: "tgtttbsokkk", css: "txtTongtien", readonly: true, height: 22, hidden: true },
                                                {
                                                    id: "tkcktmn",
                                                    view: "text",
                                                    editor: "numeric",
                                                    labelWidth: 200,
                                                    width: 600,
                                                    format: fmNumber,
                                                    name: "tkcktmn",
                                                    /*on: {
                                                        onChange: function (newValue, oldValue, config) {
                                                            var tong_tien_cu = 0;
                                                            if ($$("tgtttbso_last").getValue() != null && $$("tgtttbso").getValue() != "") tong_tien_cu = Number($$("tgtttbso_last").getValue());
                                                            if (oldValue != null && oldValue != "") tong_tien_cu += Number(oldValue);
                                                            if (tong_tien_cu > $$("tkcktmn").getValue()) {
                                                                $$("tgtttbso_last").setValue(tong_tien_cu - newValue);
                                                                $$("tkcktmn").setValue(newValue);
                                                            } else {
                                                                $$("tgtttbso_last").setValue(tong_tien_cu);
                                                            }
                                                        },
                                                    },*/
                                                },
                                                {
                                                    view: "text",
                                                    format: fmNumber,
                                                    id: "tgtphi",
                                                    name: "tgtphi",
                                                    readonly: true,
                                                    value: 0,
                                                    on: {
                                                        onChange: function (newValue, oldValue, config) {
                                                            var tong_tien_cu = 0;
                                                            if ($$("tgtttbso_last").getValue() != null && $$("tgtttbso").getValue() != "") tong_tien_cu = Number($$("tgtttbso_last").getValue());
                                                            if (oldValue != null && oldValue != "") tong_tien_cu -= Number(oldValue);
                                                            tong_tien_cu += Number(newValue);
                                                            $$("tgtttbso_last").setValue(tong_tien_cu);
                                                        },
                                                    },
                                                },
                                                {
                                                    view: "text",
                                                    id: "tgtttbso_last",
                                                    name: "tgtttbso_last",
                                                    readonly: true,
                                                    on: {
                                                        onChange: function (newv, oldv) {
                                                            commonHD.ExeCuteCheckReadCurencyTV($$("docngoaitetv").getValue());
                                                        },
                                                    },
                                                    validate: function (value) {
                                                        var table_inv_views = webix.$$("invoiceViews");
                                                        var item_inv = table_inv_views.getSelectedItem();
                                                        var item_note = "";
                                                        if (item_inv != undefined && item_inv.hasOwnProperty("note") == true) {
                                                            item_note = item_inv.note ? item_inv.note : "";
                                                        }
                                                        if (item_note == "reduce_change_invoice") return true;
                                                        if (Number(value) > 0 || Number(value) === 0) {
                                                            return true;
                                                        }
                                                        webix.message(mess, "error");
                                                        return false;
                                                    },
                                                    format: fmNumber,
                                                },
                                            ],
                                        },
                                    ],
                                },
                                {
                                    hidden: true,
                                    cols: [
                                        {
                                            view: "label",
                                            width: 140,
                                            label: "Đọc tiền bằng chữ:",
                                            css: {
                                                "font-weight": "bold",
                                                "padding-left": "5px",
                                            },
                                            height: 22,
                                        },
                                        {
                                            view: "text",
                                            id: "tgtttbchu",
                                            name: "tgtttbchu",
                                            readonly: true,
                                            height: 22,
                                        },
                                    ],
                                },
                                {
                                    margin: 10,
                                    cols: [
                                        {},
                                        {
                                            view: "button",
                                            id: "btnHoadonSaveMau04",
                                            hidden: true,
                                            label: "Ký và gửi CQT",
                                            align: "center",
                                            type: "form",
                                            width: 120,
                                            click: function () {
                                                var item = $$("invoiceForms").getValues();
                                                var tableHoadon = $$("invoiceForms");
                                                if (tableHoadon.validate()) {
                                                    var record = $$("hoadon68_chitiet").serialize();
                                                    if (record.length == 0) {
                                                        webix.message("Bạn chưa nhập chi tiết hóa đơn", "error");
                                                        return;
                                                    } else {
                                                        for (var i = 0; i < record.length; i++) {
                                                            var it = record[i];
                                                            if (it.ten == null || it.ten == "") {
                                                                webix.message("Bạn chưa nhập tên hàng");
                                                                return;
                                                            }
                                                            if (it.kmai.toString() === "1") {
                                                                var bool = Number(it.tgtien) == Number(it.thtien) + Number(it.tthue);
                                                                if (!bool) {
                                                                    webix.message("Tiền trước thuế + tiền thuế != tiền sau thuế");
                                                                    return;
                                                                }
                                                                /*if (it["tsuat"] == null || it["tsuat"] == "") {
                                                                    webix.message("Bạn chưa nhập thuế suất hàng hóa " + it["ten"]);
                                                                    return;
                                                                }*/
                                                            }
                                                        }
                                                        var t = webix.$$("invoiceViews");
                                                        var k = t.getSelectedItem();
                                                        var details = [];
                                                        details.push({ data: $$("hoadon68_chitiet").serialize() });

                                                        item["details"] = details;
                                                        item["tthdon"] = 2;
                                                        item["hdon68_id_lk"] = k.id;
                                                        /*var phi = [];
                                                        phi.push({
                                                            data: $$("hoadon68_phi").serialize(),
                                                        });
                                                        item["hoadon68_phi"] = phi;*/
                                                        // hóa đơn có mã
                                                        item.is_hdcma = 1;
                                                        var item1 = [];
                                                        item1.push(item);
                                                        var data_post = { editmode: editmode, data: item1 };
                                                        data_post.mau04 = mau04;

                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-Type": "application/json" })
                                                            .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                                                                var result = JSON.parse(text);
                                                                var kytaptrung = "";
                                                                for (var i = 0; i < result.length; i++) {
                                                                    var item = result[i];
                                                                    kytaptrung = item.value;
                                                                }

                                                                webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                                                    var listcerts = JSON.parse(text);
                                                                    if (listcerts.length > 0) {
                                                                        var box = webix.modalbox({
                                                                            title: "Chọn chứng thư số",
                                                                            buttons: ["Nhận", "Hủy bỏ"],
                                                                            text: "<div id='ctnCerts'></div>",
                                                                            top: 100,
                                                                            width: 750,
                                                                            callback: function (result) {
                                                                                switch (result) {
                                                                                    case "1":
                                                                                        webix.modalbox.hide();
                                                                                        break;
                                                                                    case "0":
                                                                                        var table = $$("tblCerts");
                                                                                        var data = table.serialize();

                                                                                        var cert = null;

                                                                                        for (var i = 0; i < data.length; i++) {
                                                                                            if (data[i].chon == true) {
                                                                                                cert = data[i];
                                                                                                break;
                                                                                            }
                                                                                        }

                                                                                        if (cert == null) {
                                                                                            webix.message("Bạn chưa chọn chứng thư số");
                                                                                            return;
                                                                                        }
                                                                                        var postData = {
                                                                                            branch_code: user.getDvcsID(),
                                                                                            username: user.getCurrentUser(),
                                                                                            cer_serial: cert.cer_serial,
                                                                                        };
                                                                                        data_post.sign = postData;

                                                                                        // console.log(data_post);
                                                                                        webix.extend(tableHoadon, webix.ProgressBar);
                                                                                        tableHoadon.disable();
                                                                                        tableHoadon.showProgress();
                                                                                        webix
                                                                                            .ajax()
                                                                                            .headers({ "Content-type": "application/json" })
                                                                                            .post(app.config.host + "/Invoice68/huyHoadonTaomoi", data_post, {
                                                                                                error: function (text, data, XmlHttpRequest) {
                                                                                                    tableHoadon.enable();
                                                                                                    tableHoadon.hideProgress();
                                                                                                    // var json = JSON.parse(text);
                                                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                                                },
                                                                                                success: function (text, data, XmlHttpRequest) {
                                                                                                    var json = JSON.parse(text);
                                                                                                    tableHoadon.enable();
                                                                                                    tableHoadon.hideProgress();
                                                                                                    if (!json.hasOwnProperty("error")) {
                                                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                                                        $$("invoice68").close();
                                                                                                        $$("huyhoadonForms").close();

                                                                                                        app.callEvent("dataChangedInvoie68", [1, null]);
                                                                                                    } else {
                                                                                                        webix.message(json.error, "error");
                                                                                                    }
                                                                                                },
                                                                                            });

                                                                                        return;
                                                                                }
                                                                            },
                                                                        });

                                                                        var tblMatHang = webix.ui({
                                                                            container: "ctnCerts",
                                                                            id: "tblCerts",
                                                                            view: "datatable",
                                                                            borderless: true,
                                                                            resizeColumn: true,
                                                                            height: 350,
                                                                            columns: [
                                                                                {
                                                                                    id: "chon",
                                                                                    header: _("CHON"),
                                                                                    width: 65,
                                                                                    checkValue: true,
                                                                                    uncheckValue: false,
                                                                                    template: "{common.checkbox()}",
                                                                                },
                                                                                {
                                                                                    id: "cer_serial",
                                                                                    header: _("SO_SERIAL"),
                                                                                    width: 150,
                                                                                },
                                                                                {
                                                                                    id: "subject_name",
                                                                                    header: _("SUBJECTNAME"),
                                                                                    width: 450,
                                                                                },
                                                                                {
                                                                                    id: "begin_date",
                                                                                    header: _("NGAY_BAT_DAU"),
                                                                                    width: 200,
                                                                                },
                                                                                {
                                                                                    id: "end_date",
                                                                                    header: _("NGAY_KET_THUC"),
                                                                                    width: 200,
                                                                                },
                                                                                {
                                                                                    id: "issuer",
                                                                                    header: _("DONVI"),
                                                                                    width: 450,
                                                                                },
                                                                            ],
                                                                            url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                                                        });
                                                                    } else {
                                                                        webix.extend(tableHoadon, webix.ProgressBar);
                                                                        tableHoadon.disable();
                                                                        tableHoadon.showProgress();
                                                                        webix
                                                                            .ajax()
                                                                            .headers({ "Content-type": "application/json" })
                                                                            .post(app.config.host + "/Invoice68/huyHoadonTaomoiUSB", data_post, {
                                                                                error: function (text, data, XmlHttpRequest) {
                                                                                    tableHoadon.enable();
                                                                                    tableHoadon.hideProgress();
                                                                                    // var json = JSON.parse(text);
                                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                                },
                                                                                success: function (text, data, XmlHttpRequest) {
                                                                                    var json = JSON.parse(text);
                                                                                    tableHoadon.enable();
                                                                                    tableHoadon.hideProgress();
                                                                                    if (!json.hasOwnProperty("error")) {
                                                                                        var a = webix.ajax(app.config.host + "/Invoice68/XmlMau0405?id=" + json.idMau04);
                                                                                        var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                                                                        webix.promise.all([a, b, json]).then(function (results) {
                                                                                            // webix.extend(tblInvoice, webix.ProgressBar);
                                                                                            // tblInvoice.disable();
                                                                                            // tblInvoice.showProgress();
                                                                                            var json = results[0].json();

                                                                                            if (json.hasOwnProperty("error")) {
                                                                                                webix.message({ type: "error", text: json.error });
                                                                                            }

                                                                                            var listcerts = results[1].json();
                                                                                            var item = results[2];

                                                                                            if (listcerts.length == 0) {
                                                                                                webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                                                                return;
                                                                                            }

                                                                                            require(["views/modules/minvoiceplugin"], function (plugin) {
                                                                                                var hub = plugin.hub();
                                                                                                var hubconnect = plugin.conn();
                                                                                                if (hubconnect.state != 1) {
                                                                                                    // tblInvoice.enable();
                                                                                                    // tblInvoice.hideProgress();
                                                                                                    webix.message(
                                                                                                        "Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !",
                                                                                                        "error"
                                                                                                    );
                                                                                                    return;
                                                                                                }
                                                                                                hub.off("resCommand");
                                                                                                hub.on("resCommand", function (result) {
                                                                                                    var json = JSON.parse(result);

                                                                                                    if (json.hasOwnProperty("error")) {
                                                                                                        webix.message({ type: "error", text: json.error });
                                                                                                    } else {
                                                                                                        var parameters = {
                                                                                                            xml: json.xml,
                                                                                                            id: item.idMau04,
                                                                                                            tthdon: 3,
                                                                                                        };

                                                                                                        webix
                                                                                                            .ajax()
                                                                                                            .headers({ "Content-Type": "application/json" })
                                                                                                            .post(app.config.host + "/Invoice68/SaveXmlMau0405", parameters, function (text) {
                                                                                                                var result = JSON.parse(text);
                                                                                                                if (result.hasOwnProperty("error")) {
                                                                                                                    webix.message(result.error, "error");
                                                                                                                } else {
                                                                                                                    webix.message("Gửi thành công đến CQT !", "success");
                                                                                                                    $$("invoice68").close();
                                                                                                                    $$("huyhoadonForms").close();

                                                                                                                    app.callEvent("dataChangedInvoie68", [1, null]);
                                                                                                                }
                                                                                                            });
                                                                                                    }
                                                                                                    tableHoadon.enable();
                                                                                                    tableHoadon.hideProgress();
                                                                                                });

                                                                                                var _listcerts = [];

                                                                                                for (var i = 0; i < listcerts.length; i++) {
                                                                                                    var _cert = listcerts[i];

                                                                                                    _listcerts.push({
                                                                                                        so_serial: _cert.cer_serial,
                                                                                                        ngaybatdau: _cert.begin_date,
                                                                                                        ngayketthuc: _cert.end_date,
                                                                                                        issuer: _cert.issuer,
                                                                                                        subjectname: _cert.subject_name,
                                                                                                    });
                                                                                                }

                                                                                                var arg = {
                                                                                                    idData: "#data",
                                                                                                    xml: json["xml"],
                                                                                                    shdon: "",
                                                                                                    idSigner: "seller",
                                                                                                    tagSign: "NNT",
                                                                                                    dvky: json["dvky"],
                                                                                                    listcerts: _listcerts,
                                                                                                };

                                                                                                hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                                                                    .done(function () {
                                                                                                        console.log("Success !");
                                                                                                    })
                                                                                                    .fail(function (error) {
                                                                                                        webix.message(error, "error");
                                                                                                    });
                                                                                            });
                                                                                        });
                                                                                        // webix.message("Gửi thành công đến CQT !", "success");
                                                                                        // $$("invoice68").close();
                                                                                        // $$("huyhoadonForms").close();

                                                                                        // app.callEvent("dataChangedInvoie68", [1, null]);
                                                                                    } else {
                                                                                        webix.message(json.error, "error");
                                                                                    }
                                                                                },
                                                                            });
                                                                    }
                                                                });
                                                            });
                                                    }
                                                }
                                            },
                                        },
                                        {
                                            shortcut: "F10",
                                            view: "button",
                                            id: "btnHoadonSave",
                                            label: "Lưu tạm (F10)",
                                            type: "form",
                                            align: "center",
                                            width: 120,
                                            on: {
                                                onClick: function () {
                                                    var item = $$("invoiceForms").getValues();
                                                    var table = $$("invoiceForms");
                                                    var record = $$("hoadon68_chitiet").serialize();
                                                    for (var i = 0; i < record.length; i++) {
                                                        var it = record[i];
                                                        if (it.ten == null || it.ten == "") {
                                                            webix.message("Bạn chưa nhập tên hàng");
                                                            return;
                                                        }
                                                        if (it.kmai.toString() === "1") {
                                                            var bool = Number(it.tgtien) == Number(it.thtien) + Number(it.tthue);
                                                            if (!bool) {
                                                                webix.message("Tiền trước thuế + tiền thuế != tiền sau thuế");
                                                                return;
                                                            }
                                                            /*if (it["tsuat"] == null || it["tsuat"] == "") {
                                                                webix.message("Bạn chưa nhập thuế suất hàng hóa " + it["ten"]);
                                                                return;
                                                            }*/
                                                        }
                                                    }
                                                    if (table.validate()) {
                                                        // var fn = Function(check);
                                                        // var result = fn();
                                                        // console.log(result);
                                                        webix.extend(table, webix.ProgressBar);
                                                        table.disable();
                                                        table.showProgress();

                                                        var details = [];
                                                        details.push({ data: $$("hoadon68_chitiet").serialize() });
                                                        item["details"] = details;
                                                        // hóa đơn không mã
                                                        /*var phi = [];
                                                        phi.push({
                                                            data: $$("hoadon68_phi").serialize(),
                                                        });
                                                        item["hoadon68_phi"] = phi;*/
                                                        // hóa đơn có mã
                                                        item.is_hdcma = 1;
                                                        var item1 = [];
                                                        item1.push(item);
                                                        var data_post = { editmode: editmode, data: item1 };

                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-type": "application/json" })
                                                            .post(app.config.host + "/Invoice68/hoadonSaveChange", data_post, {
                                                                error: function (text, data, XmlHttpRequest) {
                                                                    table.enable();
                                                                    table.hideProgress();
                                                                    // var json = JSON.parse(text);
                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                },
                                                                success: function (text, data, XmlHttpRequest) {
                                                                    var json = JSON.parse(text);
                                                                    table.enable();
                                                                    table.hideProgress();
                                                                    if (!json.hasOwnProperty("error")) {
                                                                        webix.$$("invoice68").close();
                                                                        app.callEvent("dataChangedInvoie68", [editmode, json.data]);
                                                                    } else {
                                                                        webix.message(json.error, "error");
                                                                    }
                                                                },
                                                            });
                                                    }
                                                },
                                            },
                                            click: function () {
                                                this.callEvent("onClick");
                                            },
                                        },
                                        {
                                            shortcut: "F11",
                                            view: "button",
                                            id: "btnHoadonCQT",
                                            label: "Ký và gửi CQT (F11)",
                                            type: "form",
                                            align: "center",
                                            width: 150,
                                            on: {
                                                onClick: function () {
                                                    var item = $$("invoiceForms").getValues();
                                                    var table = $$("invoiceForms");
                                                    var record = $$("hoadon68_chitiet").serialize();
                                                    for (var i = 0; i < record.length; i++) {
                                                        var it = record[i];
                                                        if (it.ten == null || it.ten == "") {
                                                            webix.message("Bạn chưa nhập tên hàng");
                                                            return;
                                                        }
                                                        if (it.kmai.toString() === "1") {
                                                            var bool = Number(it.tgtien) == Number(it.thtien) + Number(it.tthue);
                                                            if (!bool) {
                                                                webix.message("Tiền trước thuế + tiền thuế != tiền sau thuế");
                                                                return;
                                                            }
                                                            /*if (it["tsuat"] == null || it["tsuat"] == "") {
                                                                webix.message("Bạn chưa nhập thuế suất hàng hóa " + it["ten"]);
                                                                return;
                                                            }*/
                                                        }
                                                    }
                                                    if (table.validate()) {
                                                        // var fn = Function(check);
                                                        // var result = fn();
                                                        // console.log(result);
                                                        webix.extend(table, webix.ProgressBar);
                                                        table.disable();
                                                        table.showProgress();

                                                        var details = [];
                                                        details.push({ data: $$("hoadon68_chitiet").serialize() });
                                                        item["details"] = details;
                                                        /* var phi = [];
                                                        phi.push({
                                                            data: $$("hoadon68_phi").serialize(),
                                                        });
                                                        item["hoadon68_phi"] = phi;*/
                                                        // hóa đơn có mã
                                                        item.is_hdcma = 1;
                                                        var item1 = [];
                                                        item1.push(item);
                                                        var data_post = { editmode: editmode, data: item1 };

                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-type": "application/json" })
                                                            .post(app.config.host + "/Invoice68/hoadonSaveChange", data_post, {
                                                                error: function (text, data, XmlHttpRequest) {
                                                                    table.enable();
                                                                    table.hideProgress();
                                                                    // var json = JSON.parse(text);
                                                                    // webix.message(json.ExceptionMessage, "error");

                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                },
                                                                success: function (text, data, XmlHttpRequest) {
                                                                    var _res = JSON.parse(text);
                                                                    var json = JSON.parse(text);
                                                                    table.enable();
                                                                    table.hideProgress();
                                                                    if (!_res.hasOwnProperty("error")) {
                                                                        webix.$$("invoice68").close();
                                                                        app.callEvent("dataChangedInvoie68", [editmode, _res.data]);

                                                                        var lsthdon_id = [];
                                                                        lsthdon_id.push(_res.data.hdon_id);
                                                                        commonHD.ExcuteHoaDon(lsthdon_id, true, true);
                                                                    } else {
                                                                        webix.message(json.error, "error");
                                                                    }
                                                                },
                                                            });
                                                    }
                                                },
                                            },
                                            click: function () {
                                                this.callEvent("onClick");
                                            },
                                        },
                                        {
                                            view: "button",
                                            hotkey: "esc",
                                            label: "Hủy bỏ (ESC)",
                                            align: "center",
                                            type: "danger",
                                            width: 120,
                                            click: function () {
                                                this.getTopParentView().close();
                                                app.callEvent("dataChangedInvoie68", [editmode, null]);
                                            },
                                        },
                                        {},
                                    ],
                                },
                            ],
                        },
                    },
                ],
            },
        },
        setValues: function (item, winconfig, params) {
            var win = $$(this.$ui.id);
            var form = win.getChildViews()[1];
            if (item != null) {
                item["tdlap"] = removeTimeZoneT(item["tdlap"]);
                item["cqqdbants"] = removeTimeZoneT(item["cqqdbants"]);
                item["tgvchdtu"] = removeTimeZoneT(item["tgvchdtu"]);
                item["tgvchdden"] = removeTimeZoneT(item["tgvchdden"]);
                item["nqdbants"] = removeTimeZoneT(item["nqdbants"]);
                form.setValues(item);

                var tableDetail = webix.$$("hoadon68_chitiet");
                var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
                tableDetail.clearAll();
                tableDetail.load(url);
                /*var tablePhi = webix.$$("hoadon68_phi");
                var urlPhi = app.config.host + "/Invoice68/GetDataInvoicePhi?id=" + item.id;
                tablePhi.clearAll();
                tablePhi.load(urlPhi);*/
            }

            if (editmode == 2) {
            }
            // nếu có mẫu 04 (hóa đơn hủy)
            if (winconfig != null) {
                mau04 = winconfig;
                $$("btnHoadonSaveMau04").show();
                $$("btnHoadonCQT").destructor();
                $$("btnHoadonSave").destructor();
            }
            this.getFormat();
        },
        setEditMode: function (mode) {
            editmode = mode;
            //var form = webix.$$("invoiceForms");
            var form = $$(this.$ui.id);
            //var topParentView = form.getTopParentView();
            form.config.editmode = mode;
            var icon = "";
            if (editmode == 1) {
                icon = "<span><i class='fa fa-plus' aria-hidden='true'></i></span>";
            } else {
                if (editmode == 2) icon = "<i class='fa fa-edit' aria-hidden='true'></i>";
                else icon = "<i class='fa fa-trash fa-lg' aria-hidden='true'></i>";
            }

            webix.$$("head-iconInvoice").setHTML(icon);
            webix.$$("head-iconInvoice").refresh();
        },
        copyValues: function (item) {
            var form = webix.$$("invoiceForms");

            var tableDetail = webix.$$("hoadon68_chitiet");
            var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
            webix
                .ajax()
                .headers({
                    "Content-type": "application/json",
                })
                .get(url, function (text) {
                    var data = JSON.parse(text);

                    for (var i = 0; i < data.length; i++) {
                        data[i]["cthdon_id"] = null;
                        data[i]["hdon_id"] = null;
                    }

                    tableDetail.clearAll();
                    tableDetail.parse(data);
                });
            // tableDetail.clearAll();
            // tableDetail.load(url);
            item["tthdon"] = 0;
            item["hdon_id"] = null;
            item["nky"] = null;
            item["id"] = webix.uid();
            form.setValues(item);
            this.getFormat();
        },
        initUINew: function (cctbao_id) {
            $$("cctbao_id").setValue(cctbao_id);
            webix
                .ajax()
                .headers({
                    "Content-type": "application/json",
                })
                .get(app.config.host + "/Invoice68/getKyhieu?cctbao_id=" + cctbao_id, function (text) {
                    var data = JSON.parse(text);

                    if (data.length > 0) {
                        var form = $$("invoiceForms");
                        var values = form.getValues();

                        values["khieu"] = data[0]["khhdon"];

                        form.setValues(values);
                    }
                });
            this.getFormat();
        },
        getFormat: function (val) {
            renderFormat(val);
        },
    };
});
