define(["app", "locale", "views/forms/lichsuemail"], function (app, _, his_email) {
    var editmode = 0;
    var itemdoc;
    return {
        $ui: {
            view: "window",
            modal: true,
            id: "dmtab-win",
            position: "center",
            head: "Gửi hóa đơn cho khách hàng qua email",
            fullscreen: true,
            //height: 700,
            //width: 1000,
            keepViews: true,
            move: true,
            body: {
                paddingY: 5,
                paddingX: 5,
                view: "form",
                id: "dmtab-form",
                // on: {
                // 	onItemClick: function (id) {
                // 		if (id == "his_email") {
                // 			var detailForm = $$("dmtab-win").$scope.ui(his_email.$ui);
                // 			detailForm.show();
                // 		}
                // 	}
                // },
                elements: [
                    {
                        view: "fieldset",
                        label: "Thông tin gửi mail",
                        body: {
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: "text",
                                            name: "nguoi_gui",
                                            label: _("NGUOI_GUI"),
                                            labelPosition: "top",
                                            required: true,
                                            validate: webix.rules.isNotEmpty,
                                            invalidMessage: "Người gửi không được để trống !",
                                        },
                                        {
                                            view: "text",
                                            name: "alias",
                                            label: _("Bí Danh"),
                                            labelPosition: "top",
                                            // required: true,
                                            validate: webix.rules.isNotEmpty,
                                            invalidMessage: "Tên hiển thị không được để trống !",
                                        },
                                        {
                                            view: "text",
                                            name: "mat_khau",
                                            label: _("PASSWORD"),
                                            type: "password",
                                            labelPosition: "top",
                                            required: true,
                                            validate: webix.rules.isNotEmpty,
                                            invalidMessage: "Mật khẩu không được để trống !",
                                        },
                                    ],
                                },
                                {
                                    cols: [
                                        // {
                                        // 	view: "text",
                                        // 	name: "nguoinhan",
                                        // 	id: "nguoinhan",
                                        // 	label: _("NGUOI_NHAN"),
                                        // 	labelPosition: "top",
                                        // 	required: true,
                                        // 	validate: webix.rules.isNotEmpty,
                                        // 	invalidMessage: "Người nhận không được để trống !"
                                        // },
                                        {
                                            view: "multicombo",
                                            name: "nguoinhan",
                                            id: "nguoinhan",
                                            label: _("NGUOI_NHAN"),
                                            labelPosition: "top",
                                            required: true,
                                            validate: webix.rules.isNotEmpty,
                                            invalidMessage: "Người nhận không được để trống !",
                                            newValues: true,
                                            separator: ";",
                                            suggest: {
                                                selectAll: true,

                                                body: {
                                                    id: "nguoinhan_suggest",
                                                    //data: [{"id":"hungkq@minvoice.vn","value":"hungkq@minvoice.vn"},{"id":"khieuhung.kt@gmail.com","value":"khieuhung.kt@gmail.com"},{"id":"abc@gmail.com","value":"abc@gmail.com"},{"id":"xyz@gmail.com","value":"xyz@gmail.com"}]
                                                    //url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00050",
                                                    //template: webix.template("#value#")
                                                },
                                            },
                                        },
                                        {
                                            view: "text",
                                            name: "bcc",
                                            label: "BCC",
                                            labelPosition: "top",
                                        },
                                    ],
                                },
                                {
                                    view: "text",
                                    name: "tieude",
                                    label: _("TIEU_DE"),
                                    labelPosition: "top",
                                },
                            ],
                        },
                    },
                    {
                        view: "label",
                        name: "his_email",
                        id: "his_email",
                        //label: "Hóa đơn đã được gửi đi 5 lần",
                        template: function (obj) {
                            if (obj.value == 0) return "<span style='color:blue'>Hóa đơn chưa gửi cho khách hàng !</span>";
                            return (
                                "<span style='color:blue'>Hóa đơn đã được gửi <big>" +
                                obj.value +
                                "</big> lần !<sub style='color:red'> Lịch sử gửi email <i class='fa fa-hand-o-left' aria-hidden='true'></i></sub></span>"
                            );
                        },
                        on: {
                            onItemClick: function () {
                                var inv_invoiceauth_id = "";
                                var table = webix.$$("windowData");
                                if (table === undefined || table === null) {
                                    table = webix.$$("invoiceViews");
                                    item = table.getSelectedItem();
                                    inv_invoiceauth_id = item.hdon_id;
                                } else {
                                    item = table.getSelectedItem();
                                    inv_invoiceauth_id = item.inv_invoiceauth_id;
                                }

                                var detailForm = $$("dmtab-win").$scope.ui(his_email.$ui);
                                his_email.initUI(inv_invoiceauth_id);
                                detailForm.show();
                            },
                        },
                    },
                    {
                        name: "noidung",
                        view: "ckeditor",
                        keepViews: true,
                        label: _("NOI_DUNG"),
                        labelPosition: "top",
                    },
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                shortcut: "F10",
                                view: "button",
                                label: "Gửi (F10)",
                                type: "form",
                                align: "center",
                                width: 120,
                                on: {
                                    onClick: function () {
                                        var form = webix.$$("dmtab-form");
                                        var item = form.getValues();
                                        var url = app.config.host + "/Invoice/SendInvoiceByEmail";
                                        console.log(itemdoc);
                                        if (itemdoc.hdon68 == 1) {
                                            url = app.config.host + "/Invoice68/SendInvoiceByEmail";
                                        }

                                        if ($$("dmtab-form").validate()) {
                                            webix.extend(form, webix.ProgressBar);
                                            form.disable();
                                            form.showProgress();

                                            webix
                                                .ajax()
                                                .headers({ "Content-type": "application/json" })
                                                .post(url, item, function (text) {
                                                    var form = webix.$$("dmtab-form");

                                                    form.enable();
                                                    form.hideProgress();

                                                    var data = JSON.parse(text);

                                                    if (!data.hasOwnProperty("error")) {
                                                        webix.message("Gửi Email thành công !", "debug");

                                                        var item = form.getValues();
                                                        var topParentView = form.getTopParentView();

                                                        if (topParentView.config.tableId != undefined) {
                                                            var table = $$(topParentView.config.tableId);

                                                            var item = table.getSelectedItem();
                                                            item.trang_thai = "Chờ người mua ký";
                                                            table.updateItem(item.id, item);
                                                        }

                                                        webix.$$("dmtab-win").close();
                                                    } else {
                                                        webix.message(data.error);
                                                    }
                                                });
                                        } else {
                                            $$("nguoinhan").focus();
                                        }
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },
                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy bỏ (ESC)",
                                type: "danger",
                                align: "center",
                                width: 120,
                                click: function () {
                                    webix.$$("dmtab-win").close();
                                },
                            },
                            {},
                        ],
                    },
                ],
            },
        },
        setEditMode: function (mode) {
            editmode = mode;
        },
        setValues: function (values) {
            itemdoc = values;
            $$("dmtab-form").setValues(values);
            // var k = [{ "id": "hungkq@minvoice.vn", "value": "hungkq@minvoice.vn" }, { "id": "khieuhung.kt@gmail.com", "value": "khieuhung.kt@gmail.com" }, { "id": "abc@gmail.com", "value": "abc@gmail.com" }, { "id": "xyz@gmail.com", "value": "xyz@gmail.com" }];
            // $$("nguoinhan_suggest").define("data", k);
            if (values.nguoinhan != null && values.nguoinhan != "") {
                var k = [{ id: values.nguoinhan, value: values.nguoinhan }];
                $$("nguoinhan_suggest").define("data", k);
            }
            if (values.hasOwnProperty("lst_email")) {
                $$("nguoinhan_suggest").define("data", values.lst_email);
            }

            // $$("dmtab-form").setValues(values);
        },
    };
});
