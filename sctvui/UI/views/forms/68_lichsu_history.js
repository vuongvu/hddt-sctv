define(["app", "locale", "models/user"], function (app, _, user) {
    "use strict";

    var hdon_id = "";

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            css: "headformchil",
            head: {
                height: 35,
                cols: [
                    {
                        id: "head-icondksd",
                        view: "template",
                        css: "headform",
                        width: 35,
                        template: "",
                    },
                    {
                        id: "head-formdksd",
                        view: "template",
                        css: "headform",
                        template: function (obj) {
                            return "<div>Lịch sử xử lý<div/>";
                        },
                    },
                ],
            },
            id: "responseMessageForms",
            resize: true,
            width: 1000,
            height: 500,
            move: true,
            on: {
                onBeforeShow: function () {},
            },
            body: {
                view: "form",
                id: "message",
                // width: 1500,
                position: "center",
                // autowidth:true,
                paddingY: 10,
                paddingX: 15,
                complexData: true,
                scroll: true,
                margin: 0,
                elements: [
                    {
                        // id: "header",
                        template: "<center><b><span style='font-size:17px;'>LỊCH SỬ XỬ LÝ</b></center>",
                        borderless: true,
                        height: 40,
                    },
                    { height: 3 },
                    {
                        rows: [
                            {
                                view: "datatable",
                                id: "danhsach_history",
                                // height: 250,
                                //name: "danhsach_history",
                                // select: true,
                                resizeColumn: true,
                                footer: false,
                                tooltip: true,
                                editable: true,
                                select: "cell",
                                scroll: true,
                                scheme: {
                                    $init: function (obj) {},
                                },
                                url:{
                                    $proxy: true,
                                    load: function (view, callback, params) {
                                        // var hdon_id = "87de11a4-0b4e-42d4-a86c-45ba10b80337";
                                        if(hdon_id != ""){
                                            var url = app.config.host + "/Invoice68/GetHistoryTruyenNhan_ChiTiet?id=" + hdon_id;
                                            webix.ajax(url, callback, view);
                                        }
                                    }
                                },
                                onClick: {
                                    dowloadXmlDKSD: function (event, cell, target) {
                                        var win = webix.$$("danhsach_history");
                                        var record = win.getItem(cell.row);
                                        var file = new Blob([record.xml_thongdiep], {type: "application/xml"});
                                        webix.html.download(file, "file.xml");
                                    }
                                },
                                columns: [
                                    {
                                        id: "tg_gui",
                                        header: ["<center>Thời gian gửi</center>"],
                                        // template: function (obj, common) {
                                        //     console.log(obj.tthdon);
                                        //     return "<span class='mcls_status " + obj.tthdon + "'>" + obj.ttthdon + "</span>";
                                        // },
                                        width: 250,
                                        format: function (text) {
                                            var test = new Date(text);
                                            if(test !== null && test !== 'null') {
                                                var format = webix.Date.dateToStr("%d-%m-%Y %H:%i:%s");
                                                text = removeTimeZoneT(test);
                                                return format(text);
                                            }
                                            return '';
                                        },
                                        columnType: "datetime",
                                    },
                                    {
                                        id: "mngui_ft",
                                        header: ["<center>Mã nơi gửi</center>"],
                                        width: 120,
                                        columnType: "nvarchar",
                                    },
                                    {
                                        id: "mltdiep_gui",
                                        header: ["<center>Mã loại thông điệp</center>"],
                                        width: 120,
                                        columnType: "nvarchar",
                                    },
                                    {
                                        id: "mtdiep_gui",
                                        header: ["<center>Mã thông điệp</center>"],
                                        width: 300,
                                        columnType: "xml",
                                    },{

                                        id: "mtdiep_thamchieu",
                                        header: ["<center>Mã thông điệp tham chiếu</center>"],
                                        width: 300,
                                        columnType: "xml",
                                    },
                                    {
                                        id: "xml_thongdiep",
                                        header: ["<center>Xml thông điệp</center>"],
                                        css: { "text-align": "center" },
                                        template: function (obj) {
                                            return "<span class='dowloadXmlDKSD' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                                        },
                                        width: 120,
                                    },
                                    {
                                        id: "mnnhan",
                                        header: ["<center>Mã nơi nhận</center>"],
                                        width: 120,
                                        columnType: "nvarchar",
                                    },
                                    {
                                        id: "tg_nhan",
                                        header: ["<center>Thời gian nhận</center>"],
                                        width: 250,
                                        format: function (text) {
                                            var test = new Date(text);
                                            var format = webix.Date.dateToStr("%d-%m-%Y %H:%i:%s");
                                            text = removeTimeZoneT(test);
                                            return format(text);
                                        },
                                        columnType: "nvarchar",
                                    },
                                    {
                                        id: "vendor",
                                        header: ["<center>Tên giải pháp</center>"],
                                        width: 120,
                                        columnType: "nvarchar",
                                    },
                                    {
                                        id: "mngui",
                                        header: ["<center>Mã số thuế</center>"],
                                        width: 120,
                                        columnType: "nvarchar",
                                    }
                                ],
                            },
                        ],
                    },
                    {
                        id: "btnHuyHDDT",
                        cols: [
                            {},
                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy bỏ (ESC)",
                                width: 120,
                                type: "danger",
                                align: "center",
                                click: function () {
                                    hdon_id = "";
                                    webix.$$("responseMessageForms").close();
                                },
                            },
                            {},
                        ],
                    },
                ],
            },
        },
        setEditMode: function (id) {
            hdon_id = id;
            var table = $$("danhsach_history");
            table.load(table.config.url);
        },
    };
});
