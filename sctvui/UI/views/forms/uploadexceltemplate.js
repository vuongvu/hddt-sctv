define(["app", "locale"], function (app, _) {

	return {
		$ui: {
			view: "window", modal: true, id: "uploadfile-win", position: "center",
			head: "Nhận dữ liệu từ file Excel",
			width: 500,
			height: 500,
			body: {
				paddingY: 10,
				paddingX: 10,
				elementsConfig: { labelWidth: 100 },
				view: "form",
				id: "uploadfile-form",
				complexData: true,
				elements: [
					{
						cols: [
							{
								view: "toolbar",
								css: { "background": "#e6f7ff !important" },
								rows: [
									{
										view: "button", type: "icon", icon: "fas fa-folder-open", label: "Chọn file", width: 110,
										on: {
											onClick: function () {
												webix.$$("uploader").fileDialog();
											}
										},
										click: function () {
											this.callEvent("onClick");
										}
									},
									{
										view: "button", type: "icon", icon: "fas fa-download", label: "Tải file mẫu", width: 110,
										on: {
											onClick: function () {
												var uploader = webix.$$("uploader");
												var formData = uploader.config.formData;
												var filename = formData["filename"] == null ? "template.xlsx" : formData["filename"];
												var table = $$("uploadfile-win");
												webix.extend(table, webix.ProgressBar);
												table.disable();
												table.showProgress();
												webix.ajax()
													.bind({filename: filename})
													.response("arraybuffer")
													.headers({ 'Content-type': 'application/json' })
													.get(app.config.host + "/System/DownloadFileExcelTemplate?id=" + formData["excel_id"], function (text, data, XmlHttpRequest) {
														webix.delay(function (filename) {
															var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
															var fileURL = URL.createObjectURL(file);

															var link = document.createElement('a');
															document.body.appendChild(link);
															link.download = filename;
															link.href = fileURL;
															link.click();
															document.body.removeChild(link); // remove the link when done
														}, null, [filename], 500);

														table.enable();
														table.hideProgress();
													});
											}
										},
										click: function () {
											this.callEvent("onClick");
										}
									},
									{
										shortcut: "F10", view: "button", type: "icon", icon: "fas fa-check", label: "Nhận file (F10)", width: 120, on: {
											onClick: function () {
												var table = $$("uploadfile-win");
												webix.extend(table, webix.ProgressBar);
												table.disable();
												table.showProgress();
												var file = $$("uploader").files.getFirstId();
												if (file == undefined || file == null) {
													webix.message("Bạn chưa chọn file !", "error");
													$$("uploadfile-win").enable();
													$$("uploadfile-win").hideProgress();
													return;
												}
												$$("uploader").send(function (response) {
													var message = response.message || "Nhập file Excel thành công !";
													if (response != undefined) {

														if (!response.hasOwnProperty("error")) {
															$$("uploadfile-win").enable();
															$$("uploadfile-win").hideProgress();
															var topParentView = this.getTopParentView();

															if (topParentView.config.callback){
																topParentView.config.callback();
																return;
															}

															if (topParentView.config.tableId !== undefined) {
																var table = $$(topParentView.config.tableId);

																if (table != null && table.config.url!=undefined) {
																	table.clearAll();
																	table.load(table.config.url);
																}
															}

															webix.alert({
																title: "Thành công !",
																text: "<div class='v-message'>"+message+"</div>",
																type: "alert-warning",
																callback: function (result) {
																	webix.$$("uploadfile-win").close();
																}
															});

														}
														else {
															$$("uploadfile-win").enable();
															$$("uploadfile-win").hideProgress();

															webix.alert({
																title: "Lỗi !",
																text: response.error,
																type: "alert-error",
                                                                callback: function (result) {
																	webix.$$("uploadfile-win").close();
																}
															});
														}
													}
													else {
														$$("uploadfile-win").enable();
														$$("uploadfile-win").hideProgress();
													}
												});
											}
										},
										click: function () {
											this.callEvent("onClick");
										}
									},
									{
										hotkey: "esc", view: "button", type: "icon", icon: "fas fa-times", label: "Hủy bỏ (ESC)", width: 110,
										on: {
											onClick: function () {
												$$("mylist").clearAll();
												$$("mylist").unbind();
												var id = $$("uploader").files.getFirstId();
												if (id != undefined) {
													$$("uploader").stopUpload(id);
												}
												webix.$$("uploadfile-win").close();
											}
										},
										click: function () {
											this.callEvent("onClick");
										}
									}

								]
							},
							{
								rows: [
									{
										id: "uploader",
										view: "uploader",
										value: 'Chọn file',
										hide: true,
										name: "uploader",
										accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
										link: "mylist",
										autosend: false,
										multiple: false,
										upload: app.config.host + "/System/UploadExcelTemplate",
										on: {
											onAfterRender: function () {
												this.hide();
											},
											onAfterFileAdd: function (file) {
												var fileName = file.name;
												if (!fileName.endsWith(".xls") && !fileName.endsWith(".xlsx")) {
													webix.alert({
														title: "Lỗi !",
														text: "File không đúng định dạng Excel!",
														type: "alert-error",
														callback: function () {
															$$("uploader").files.data.clearAll();
														}
													});
												}
											},
											onUploadComplete: function (response) {
												if (response === undefined) return;

												if (response.hasOwnProperty("error")){
													webix.message({type:"error",text:response.error});
												}
											},
											onFileUploadError: function (file, response) {
												$$("uploadfile-win").enable();
												$$("uploadfile-win").hideProgress();
											}
										},
										apiOnly: true,
									},
									{
										view: "list",
										id: "mylist",
										type: "uploader",
										height: 100,
										borderless: true
									}
								]
							}

						]
					}
				]
			}

		}

	};

});