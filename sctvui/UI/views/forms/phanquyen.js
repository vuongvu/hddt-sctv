define(["app","locale"],function(app,_){
	
	function initUI(item){
		
		var url = app.config.host + "/System/ExecuteCommand";

		webix.ajax()
			.headers({'Content-type':'application/json'})
			.post(url,
				{
					command:"CM00003",
					parameter:{
						wb_nhomquyen_id: item.id
					}
				}
				,function(text){
            var json = JSON.parse(text);
			var data = listToTree(json, { idKey: "id", parentId: "parent_id" });
			
			var trtChucNang = $$("trtChucNang");

			trtChucNang.parse(data,"json");
			trtChucNang.openAll();

			for(var i=0;i<json.length;i++){
				var item = json[i];

				if (item.wb_role_id!=null){
					trtChucNang.checkItem(item.id);
				}				
			}
			
		});
		

		webix.ajax().headers({"Content-Type":"application/json"})
			.post(url,
				{
					command:"CM00015",
					parameter:{
						wb_nhomquyen_id: item.id
					}
				},function(text){
            var json = JSON.parse(text);
			var data = listToTree(json, { idKey: "id", parentId: "parent_id" });
			
			var trtBaoCao=$$("trtBaoCao");

			trtBaoCao.parse(data,"json");
			trtBaoCao.openAll();

			for(var i=0;i<json.length;i++){
				var item = json[i];

				if (item.wb_role_id!=null){
					trtBaoCao.checkItem(item.id);
				}
			}
			
		});

	}

	return {		
		$ui:{   
			view:"window", modal:true, id:"phanquyen-win", position:"center",
			head:"Thông tin Tab",
			fullscreen:true,
			body:{
				rows:[
					{
						borderless:true,
						view:"tabview",
						scrollX:true,
						multiview: {
							fitBiggest:true
						},
						cells:[
							{
								header: _("CHUC_NANG"),
								body:{
									id:"trtChucNang",
									view:"treetable",
									threeState: true,
									columns:[
										{
											id:"name",
											header:"<input id='chkBox' class='chkBox' type='checkbox' />" + _("TEN_CHUC_NANG"),
											template : "{common.space()} {common.icon()} {common.treecheckbox()} #name#",
											fillspace:true																				
										},
										{
											id:"them",
											header: [ {text:"", content:"masterCheckbox" }, _("THEM")],
											width:55,
											checkValue:'C', 
											uncheckValue:'K', 
											template: function(obj,common,value, config) {
												
												if (obj.$count>0){
													return "";
												}

												return common.checkbox(obj, common,value, config);
											}
										},
										{
											id:"sua",
											header: [ {text:"", content:"masterCheckbox" }, _("SUA")],
											width:55,
											checkValue:'C', 
											uncheckValue:'K', 
											template: function(obj,common,value, config) {
												
												if (obj.$count>0){
													return "";
												}

												return common.checkbox(obj, common,value, config);
											}
										},
										{
											id:"xoa",
											header: [ {text:"", content:"masterCheckbox" }, _("XOA")],
											width:55,
											checkValue:'C', 
											uncheckValue:'K', 
											template: function(obj,common,value, config) {
												
												if (obj.$count>0){
													return "";
												}

												return common.checkbox(obj, common,value, config);
											}
										}
									],
									on:{
										onHeaderClick:function(id, e, node){
											if (e.target.className == "chkBox"){
												var chk=document.getElementById("chkBox").checked;

												var trtChucNang = $$("trtChucNang");			
												trtChucNang.openAll();

												var json = trtChucNang.serialize();

												for(var i=0;i<json.length;i++){
													var item = json[i];

													if (chk == true){
														trtChucNang.checkItem(item.id);
													}
													else {
														trtChucNang.uncheckItem(item.id);
													}
																													
												}
											}											
										}
									} 
								}
							},
							{
								header: _("BAO_CAO"),
								body:{
									id:"trtBaoCao",
									view:"treetable",
									threeState: true,
									columns:[
										{
											id:"name",
											header:"<input id='chkBox' class='chkBox' type='checkbox' />" +_("CHON_BOCHON"),
											template : "{common.space()} {common.icon()} {common.treecheckbox()} #name#",
											fillspace:true																				
										}										
									],
									on:{
										onHeaderClick:function(id, e, node){
											if (e.target.className == "chkBox"){
												var chk=document.getElementById("chkBox").checked;

												var trtBaoCao = $$("trtBaoCao");			
												trtBaoCao.openAll();

												var json = trtBaoCao.serialize();

												for(var i=0;i<json.length;i++){
													var item = json[i];

													if (chk == true){
														trtBaoCao.checkItem(item.id);
													}
													else {
														trtBaoCao.uncheckItem(item.id);
													}
																													
												}
											}											
										}
									}
								}
							} 
						]						
					},
					{
						cols:[
							{},
							{ 
								shortcut:"F10",  
								view:"button", label:"Nhận (F10)", type:"form", align:"center", 
								width:100,
								on: {
									onClick: function() {										
										var tblChucnang = $$("trtChucNang");
										var tblBaoCao = $$("trtBaoCao");

										var win = this.getTopParentView();

										var checkedItems = tblChucnang.getChecked();

										var data = {
											wb_role_id: win.config.wb_role_id,
											wb_ctquyen: [],
											wb_ctquyenrp: []
										};

										for(var i=0;i<checkedItems.length;i++){
											var checked = tblChucnang.getItem(checkedItems[i]);
											
											if (checked.$count==0){
												var item = {
													wb_role_id: win.config.wb_role_id,
													wb_menu_id: checked.id,
													them: checked.them,
													sua: checked.sua,
													xoa: checked.xoa
												};
												
												data.wb_ctquyen.push(item);
											}
										}

										checkedItems = tblBaoCao.getChecked();

										for(var i=0;i<checkedItems.length;i++){
											var checked = tblBaoCao.getItem(checkedItems[i]);
											
											if (checked.$count==0){
												var item = {
													wb_role_id: win.config.wb_role_id,
													wb_report_id: checked.id													
												};
												
												data.wb_ctquyenrp.push(item);
											}
										}

										var url = app.config.host + "/System/Phanquyen";

										webix.ajax()
											.headers({'Content-type':'application/json'})
											.post(url, data, function(text){
											var result = JSON.parse(text);
											
											if (!result.hasOwnProperty("error")){
												webix.$$("phanquyen-win").close();												
											}
											else {
												webix.message(result.error);
											}
										});
									}
								},
								click:function(){								
									this.callEvent("onClick");							
														
								},
							},
							{ 
								view:"button", shortcut:"esc", label:"Hủy bỏ (ESC)",type:"danger",align:"center",
								width:100,
								on:{
									onClick: function(){
										this.getTopParentView().close();
									}
								},
								click:function(){
									this.callEvent("onClick");
								}
							},
							{}

						]
					},
					{
						height:5
					}
				]
			}			
			
		},		
		initUI: initUI
	};

});