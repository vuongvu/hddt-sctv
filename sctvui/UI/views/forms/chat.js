define(["app", "models/user", "views/modules/chatplugin"], function (app, user, proxy) {
    // var hub = proxy.hub();
    // hub.on("addNewMessageToPage", function (name, message) {
    //     addChatMessage(name, message);
    // });

    // function change_weather(i) {
    //     var jj = proxy.hub();
    //     jj.invoke('change_weather', i);
    // }

    function addChatMessage(name, message) {
        var chat = $$("chat");

        chat.add({
            user: name,
            value: message,
        });

        var lastId = chat.getLastId();
        chat.scrollTo(0, lastId);

        $$("message").setValue("");
        $$("message").focus();
    }

    return {
        $ui: {
            view: "window",
            modal: true,
            id: "chat-win",
            position: "center",
            head: "SCTV Chat",
            // head: {
            //     view: "toolbar",
            //     height: 25,
            //     cols: [
            //         { view: "label", label: "SCTV Chat" },
            //         {
            //             view: "icon", icon: "times", hotkey: "esc", click: function () {
            //                 $$("chat-win").close();
            //             }
            //         }
            //     ]
            // },
            width: 700,
            height: 500,
            body: {
                paddingY: 10,
                paddingX: 10,
                //elementsConfig:{labelWidth: 120},
                view: "form",
                id: "chat-form",
                complexData: true,
                elements: [
                    //rows: [
                    //  { template: "Webix Based Chat", type: "header" },
                    // {
                    //     view: "list", id: "chat", gravity: 3,
                    //     // url: "faye->/data", save: "faye->/data",
                    //     // type: { height: "auto" },
                    //     // template: chat_template
                    // },
                    {
                        view: "list",
                        id: "chat",
                        gravity: 3,
                        type: { height: "auto" },
                        template: function (obj) {
                            return "<span " + (obj.user == user.getCurrentUser() ? "class='rowBold'" : "class='rowBoldGreen'") + ">" + obj.user + "</span> " + obj.value;
                        },
                    },
                    {
                        cols: [
                            { view: "textarea", id: "message", placeholder: "Type message here", gravity: 3 },
                            {
                                rows: [
                                    {
                                        view: "button",
                                        value: "Send",
                                        type: "form",
                                        // width: 100,
                                        click: function () {
                                            //change_weather(124124);
                                            var text = $$("message").getValue();
                                            var ds = user.getCurrentUser();
                                            //webix.message("Hệ thống đang bảo trì !", "error");
                                            //send(ds, text);
                                            var hub = proxy.hub();
                                            hub.invoke("Send", user.getCurrentUser(), text)
                                                .done(function () {
                                                    console.log("send ok !");
                                                    //addChatMessage(ds, text);
                                                })
                                                .fail(function (error) {
                                                    console.log(error);
                                                });
                                        },
                                        // click: function () {
                                        //     webix.message("Hệ thống đang bảo trì !", "error");
                                        // }
                                    },
                                    {
                                        view: "button",
                                        value: "Hủy bỏ (ESC)",
                                        type: "danger",
                                        hotkey: "esc",
                                        align: "center",
                                        // width: 100,
                                        click: function () {
                                            webix.$$("chat-win").close();
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
            //], type: "space", margin: 2
        },
    };
});
