define(["app"],function(app){
	var editmode = 0;	

	return {		
		$ui:{   
			view:"window", modal:true, id:"dmwindow-win", position:"center",
			head:"Thông tin Window",
			fullscreen:true,
			body:{
                paddingY:20, 
                paddingX:30, 
                elementsConfig:{labelWidth: 100}, 
                view:"form", 
				id:"dmwindow-form", 
				complexData:true,
				margin:0,
                elements:[
					{ view:"text", name:"REFERENCES_ID", label:"RF ID",  id:"dmwindow-rfid"},
                    { view:"text", name:"NAME", label:"Name",  id:"dmwindow-name"},
					{
						cols:[
							{ view:"text", name:"VALUE_FIELD", label:"Value Field",  id:"dmwindow-valuefield"},
							{ view:"text", name:"DISPLAY_FIELD", label:"Display Field",  id:"dmwindow-displayfield"},
						]
					},
					{ view:"combo", name:"TYPE", label:"TYPE", options:[ "List", "SQL" ], id:"dmwindow-type"},
					{ view:"text", name:"SQL", label:"SQL",  id:"dmwindow-sql"},
					{ view:"text", name:"LIST_VALUE", label:"LIST VALUE",  id:"dmwindow-listvalue"},
					{ view:"text", name:"LIST_COLUMN", label:"LIST COLUMN",  id:"dmwindow-listcolumn"},
					{ view:"text", name:"FILTER_VALUE", label:"FILTER VALUE",  id:"dmwindow-filtervalue"},
					{ view:"text", name:"ORDER_BY", label:"ORDER BY",  id:"dmwindow-orderby"},
					{ view:"text", name:"GHICHU", label:"Ghi chú",  id:"dmwindow-ghichu"},
					{ 
						view:"combo", 
						name:"WINDOW_ID", 
						label:"WINDOW ID",  
						id:"dmwindow-windowid",
						options: app.config.host + "/api/System/GetDataByReferencesId?id=76DCF72E-7631-458A-986F-5B6D9BE9B203"
					},
					{
						margin:10,
						cols:[
							{},
							{ 
								shortcut:"F10",  view:"button", label:"Nhận", type:"form", align:"center", width:120, 
								on: {
									onClick: function() {
										var form = webix.$$("dmwindow-form");
										var item = form.getValues();
										console.log(item);
										if(editmode==1){
											webix.ajax().post( app.config.host + "/api/System/InsertReferences", item, function(text){
												var data = JSON.parse(text);
												//console.log(data);
												if (!data.hasOwnProperty("error")){
													item.id = data.id;
													webix.$$("dmwindow-win").close();
													app.callEvent("dataChanged",[editmode,item]);
												}
												else {
													webix.message(data.error);
												}
											});
										}
										if(editmode==2){
											webix.ajax().post( app.config.host + "/api/System/UpdateReferences", item, function(text){
												var data = JSON.parse(text);
												//console.log(data);
												if (!data.hasOwnProperty("error")){
													item.id = data.id;
													webix.$$("dmwindow-win").close();
													app.callEvent("dataChanged",[editmode,item]);
												}
												else {
													webix.message(data.error);
												}
											});
										}
										
									}
								},
								click:function(){								
									this.callEvent("onClick");							
									
								}
							},
							{ view:"button", hotkey:"esc", label:"Hủy bỏ",align:"center", width:120, 
								click:function(){
									webix.$$("dmwindow-win").close();
								}
							}
						]
					}

				]
			}			
			
		},		
		setEditMode: function(mode) {
			editmode = mode;
		}
	};

});