define(["app", "locale", "models/user"], function (app, _, user) {
    var editmode = 0;
    var param;

    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Nhận (F10)",
            type: "form",
            align: "center",
            width: 120,
            on: {
                onClick: function () {
                    var self = this;
                    setTimeout(function () {
                        var item = $$("kyhieu-form").getValues();
                        var table = $$("kyhieu-form");
                        item.editmode = editmode;
                        if (table.validate()) {
                            webix.extend(table, webix.ProgressBar);
                            table.disable();
                            table.showProgress();
                            webix
                                .ajax()
                                .headers({ "Content-type": "application/json" })
                                .post(app.config.host + "/Invoice68/hoadonkihieu68", item, {
                                    error: function (text, data, XmlHttpRequest) {
                                        var table = $$("kyhieu-form");
                                        table.enable();
                                        table.hideProgress();
                                        // var json = JSON.parse(text);
                                        // webix.message(json.ExceptionMessage, "error");
                                        webix.message(text + XmlHttpRequest, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var json = JSON.parse(text);
                                        var table = $$("kyhieu-form");
                                        table.enable();
                                        table.hideProgress();
                                        if (!json.hasOwnProperty("error")) {
                                            $$("kyhieu-win").close();
                                            var table = $$("kyhieu_hoadon");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        } else {
                                            webix.message(json.error, "error");
                                        }
                                    },
                                });
                        }
                    }, 100);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            hotkey: "esc",
            label: "Hủy bỏ (ESC)",
            type: "danger",
            align: "center",
            width: 120,
            click: function () {
                webix.$$("kyhieu-win").close();
            },
        },
        {},
    ];

    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "kyhieu-win",
        position: "center",
        head: "Đăng ký ký hiệu hóa đơn",
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            width: 500,
            id: "kyhieu-form",
            complexData: true,
            margin: 0,
            elements: [
                // {
                //     view: "text",
                //     label: "Tên mẫu",
                //     name: "ten_mau",
                //     required: true,
                //     invalidMessage: "Tên mẫu không được bỏ trống",
                //     labelWidth: 120
                // },
                {
                    view: "combo",
                    name: "kyhieu_id",
                    label: "Kí hiệu hóa đơn",
                    //labelPosition: "top",
                    id: "kyhieu_id",
                    required: true,
                    labelWidth: 120,
                    invalidMessage: "Kí hiệu hóa đơn không được bỏ trống",
                    options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00120",
                    on: {
                        onChange: function (newv, oldv) {
                            var test = newv;
                        },
                    },
                },
                {
                    view: "text",
                    name: "tongso",
                    label: _("Số lương"),
                    editor: "autonumeric",
                    labelWidth: 120,
                    required: true,
                    css: { "text-align": "right" },
                    validate: function (val) {
                        if (val === undefined || val === null || val === "") return false;
                        return !isNaN(val) && val >= 0 && val < 100000000;
                    },
                    attributes: { type: "number" },
                    invalidMessage: "Phải nhập số dương và nhỏ hơn 100000000",
                },
                {
                    margin: 10,
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },
        initUI: function (item) {
            param = item;
        },
        setValues: function (item) {
            $$("kyhieu-form").setValues(item);
            if (editmode == 2) {
                $$("kyhieu_id").define("readonly", true);
            }
        },
    };
});
