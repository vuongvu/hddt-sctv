define(["app","locale"], function (app,_) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "register_licence-win", position: "center",
			head: "Đăng ký licence",
			height: 250,
			body: {
				paddingY: 10,
				paddingX: 10,
				//elementsConfig:{labelWidth: 120}, 
				view: "form",
				id: "register_licence-form",
				complexData: true,
				margin: 0,
				elements: [
					{
						id:"cbBranchCode",
						view: "combo",
						name: "branch_code",
						label: "Đơn vị",
						width: 600,
						labelWidth: 200,	
						//id: "dmtab-store",
						required: true,
						readonly: true,
						invalidMessage: "Bạn chưa chọn đơn vị",
											
						options: {
							view: "treesuggest",
							textValue: "value",
							url: {
								$proxy: true,
								load: function (view, callback, params) {
									var url = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00195";

									webix.ajax(url, function (text) {
										var json = JSON.parse(text);
										var data = listToTree(json);
										view.parse(data, "json");
									});

								}
							},
							body: {								
								textValue: "value",
								columns: [
									{
										id: "wb_branch_id",
										width: 120,
										header: _("MA"),
										hidden: true
									},
									{
										id: "name",
										width: 650,
										header: _("TEN"),
										template:"{common.space()}{common.icon()}#name#"
									}
								],
								filterMode: {
									level: false,
									showSubItems: false
								}								
							}
						},
					},
					{
						view: "text",
						label: "Số lượng",
						name: "quantity",
						required: true,
						invalidMessage: "Số lượng không được bỏ trống",
						width: 600,
						labelWidth: 200
					},
                   
					{},
					{
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", view: "button", label: "Lưu (F10)", type: "form", align: "center", width: 120,
								on: {
									onClick: function () {

										var form = webix.$$("register_licence-form");
										if (form.validate() == false) return;
										var item = form.getValues();

										var model = {
											// editmode = editmode,
											branch_code: item.branch_code,
											quantity: item.quantity,
											createdondate: new Date().toUTCString()
										};
										model.editmode = editmode;

										if(editmode == "2"){
											model.pl_licence_branch_id = item.pl_licence_branch_id;
										}

										webix.ajax()
											.headers({ 'Content-type': 'application/json' })
											.post(app.config.host + "/System/RegisterLicenceBranch", model, function (text) {
												var json = JSON.parse(text);
												if (!json.hasOwnProperty("error")) {
													app.callEvent("dataChangedLicense", [editmode, json.data]);
													if(json.hasOwnProperty("ok") && editmode == 1 && json.ok == "true")
													{
														webix.message("Thêm mới license thành công!", "success");
													}
													$$("register_licence-win").close();
												}
												else {
													webix.message(json.error, "error");
													app.callEvent("dataChangedLicense", [editmode, json.data]);
												}

											});
									}
								},
								click: function () {
									this.callEvent("onClick");
								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 120,
								click: function () {
									webix.$$("register_licence-win").close();
								}
							},
							{}
						]
					}

				]
			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});