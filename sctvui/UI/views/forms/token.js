define(["app", "locale"], function (app, _) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "chukyso-win", position: "center",
			head: "Thông tin Token",
			//height:420,		
			body: {
				paddingY: 10,
				paddingX: 10,
				view: "form",
				id: "chukyso-form",
				complexData: true,
				margin: 0,
				elements: [
					{
						view: "text",
						name: "serial_number",
						labelPosition: "top",
						label: _("SO_SERIAL"),
						readonly: true,
						width: 350
					},
					{
						view: "text",
						name: "token_label",
						labelPosition: "top",
						label: "TOKEN LABEL",
						readonly: true,
					},
					{
						view: "text",
						name: "pass",
						labelPosition: "top",
						label: _("PASSWORD"),
						type: "password"
					},
					{},
					{
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", view: "button", label: "Lưu (F10)", type: "form", align: "center", width: 120,
								on: {
									onClick: function () {
										var form = webix.$$("chukyso-form");

										var item = form.getValues();

										var khData = {};

										khData.windowid = app.path[1].params[0];
										khData.editmode = editmode;
										khData.data = [];

										khData.data.push(item);

										webix.ajax()
											.headers({ 'Content-type': 'application/json' })
											.post(app.config.host + "/System/Save", khData, function (text) {

												var json = JSON.parse(text);

												if (!json.hasOwnProperty("error")) {
													$$("chukyso-win").close();
													app.callEvent("dataChanged", [editmode, json.data]);
												}
												else {
													webix.message(json.error, "error");
												}


											});
									}
								},
								click: function () {
									this.callEvent("onClick");

								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", align: "center", width: 120, type: "danger",
								click: function () {
									webix.$$("chukyso-win").close();
								}
							},
							{}
						]
					}

				]
			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});