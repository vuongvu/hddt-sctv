define(["app", "locale"],function(app,_){
	var editmode = 0;	

	return {		
		$ui:{   
			view:"window", modal:true, id:"dmtab-win", position:"center",
			head:"Tìm kiếm chi tiết",
			width:500,
			body:{
                paddingY:10, 
                paddingX:10, 
                view:"form", 
				id:"dmtab-form", 
				complexData:true,
				margin: 0,
                elements:[
					{
						view:"combo", 
						name:"inv_invoiceType", 
						label:_("MA_LOAI"), 
						labelPosition:"top",
						options: {
							body:{
								url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00019"
							},									
							width: 550,
							fitMaster:false
						}
					},
					{
						cols:[							
							{ view:"text", name:"mau_hd", label:_("MAU_HDON"), labelPosition:"top" },
							{ view:"text", name:"inv_invoiceSeries", label:_("SO_SERIAL"), labelPosition:"top" }
						]
					},
					{
						cols:[
							{ view:"text", name:"inv_invoiceNumber", label:_("SO_HDON"), labelPosition:"top" },
							{ 
								view:"combo", 
								name:"user_new", 
								label:_("NGUOI_LAP"), 
								labelPosition:"top",
								options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00020" 
							}
						]
					},
					{
						cols:[
							{ view:"datepicker", name:"tu_ngay", label:_("TU_NGAY"), labelPosition:"top" },
							{ view:"datepicker", name:"den_ngay", label:_("DEN_NGAY"), labelPosition:"top" }
						]
					},
					{
						cols:[
							{ view:"combo", name:"trang_thai", options:["Chờ duyệt","Chờ ký", "Đã ký", "Đã kê khai"], label:_("TRANG_THAI"), labelPosition:"top" },
							{ 
								view:"combo", 
								name:"trang_thai_hd", 
								label:_("TRANG_THAI_HDON"), 
								labelPosition:"top",
								options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00017"
							}
						]
					},								
					{
						margin:10,
						cols:[
							{},
							{ 
								shortcut:"F10",  
								view:"button", 
								label:"Nhận (F10)", 
								type:"form", 
								icon:"",
								align:"center", 
								width:120, 
								on: {
									onClick: function() {
										var form = webix.$$("dmtab-form");
										var item = form.getValues();

										webix.$$("dmtab-win").close();
										app.callEvent("dataChanged",[1,item]);										
										
									}
								},
								click:function(){								
									this.callEvent("onClick");							
									
								}
							},
							{ view:"button", hotkey:"esc", label:"Hủy bỏ (ESC)",type:"danger", align:"center", width:120, 
								click:function(){
									webix.$$("dmtab-win").close();
								}
							},
							{}
						]
					}

				]
			}			
			
		},		
		setEditMode: function(mode) {
			editmode = mode;
		},
		setValues: function(values){
			$$("dmtab-form").setValues(values);
		}
	};

});