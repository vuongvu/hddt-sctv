define(["app", "models/user", "views/forms/68_huyhoadon_1/68_mau04form", "views/forms/68_huyhoadon_1/68_huyhoadonchitietform", "views/forms/68_huyhoadon_1/68_hoadonform"], function (
    app,
    user,
    mau04form,
    huyhoadonchitietform,
    hoadonform
) {
    var editmode = 0;

    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "huyhoadon-win",
        position: "center",
        head: {
            view: "toolbar",
            elements: [
                {
                    width: 50,
                },
                {
                    template: function () {
                        return '<h3 style="text-align: center; margin: 0">Hủy và tạo mới hóa đơn</h3>';
                    },
                },
                {
                    view: "icon",
                    icon: "times",
                    on: {
                        onClick: function () {
                            $$("huyhoadon-win").close();
                        },
                    },
                    click: function () {
                        this.callEvent("onClick");
                    },
                },
            ],
        },
        body: {
            rows: [
                {
                    view: "tabbar",
                    id: "tabbar",
                    multiview: true,
                    height: 50,
                    width: 1500,
                    value: "infoView",
                    options: [
                        { value: "Bước 1: Lập mẫu 04", id: "infoView" },
                        { value: "Bước 2: Tạo hóa đơn mới", id: "formView" },
                        { value: "Bước 3: Ký và gửi", id: "previewView" },
                    ],
                    on: {
                        onBeforeTabClick: function (id, ev) {
                            debugger;
                            if (id == "previewView") {
                                var win = webix.$$("previewView");
                                webix.extend(win, webix.ProgressBar);
                                win.disable();
                                win.showProgress();

                                var url = "http://localhost:18374/hopdong/printhopdong?id=b9496ff0-360d-4852-8153-4b3c54fbe167&type=pdf";
                                webix.ajax().get(url, function (response) {
                                    win.enable();
                                    win.hideProgress();
                                    var file = new Blob([response], {
                                        type: "application/pdf",
                                    });
                                    var fileURL = URL.createObjectURL(file);

                                    var builder = '<div><p>Mẫu hóa đơn</p><iframe class="responsive-iframe" src="' + fileURL + '" frameborder="0" height="700px" width="100%"></iframe></div>';

                                    $$("pdfViewInfoOld").setHTML(builder);
                                    $$("pdfViewInfoNew").setHTML(builder);
                                });
                            }

                            if (id == "formView") {
                                // Load data tab formView
                            }

                            if (id == "infoView") {
                                // Load data tab infoView
                            }
                        },
                        onAfterRender: function () {
                            // Load data tab 1
                        },
                    },
                },
                {
                    height: 650,
                    cells: [mau04form.$ui, hoadonform, huyhoadonchitietform],
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },

        setValues: function (item) {},
    };
});
