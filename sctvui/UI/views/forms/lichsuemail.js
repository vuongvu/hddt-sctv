define(["app", "locale"], function (app, _) {
    function initUI(inv_InvoiceAuth_id) {
        // var model = { inv_InvoiceAuth_id: inv_InvoiceAuth_id };
        // webix.ajax()
        //     .headers({ 'Content-type': 'application/json' })
        //     .post(app.config.host + "/Invoice/history_email", model, function (text) {
        //         var data = JSON.parse(text);
        //         if (!data.hasOwnProperty("error")) {
        //             $$('email-form').setValue(data);
        //         }
        //         else {
        //             webix.message(data.error, "error");
        //         }
        //     });
    }

    return {
        $ui: {
            view: "window",
            modal: true,
            id: "email-win",
            position: "center",
            head: "Lịch sử gửi Email",
            width: 700,
            height: 300,
            move: true,
            body: {
                paddingY: 10,
                paddingX: 10,
                view: "form",
                id: "email-form",
                complexData: true,
                margin: 0,
                elements: [
                    {
                        view: "datatable",
                        header: "Lịch sử gửi Email",
                        id: "tbl_email",
                        editable: false,
                        resizeColumn: true,
                        select: true,
                        // fixedRowHeight: true,
                        // rowLineHeight: 25,
                        // rowHeight: 25,
                        //autoheight:true,
                        url: {
                            $proxy: true,
                            load: function (view, callback, params) {
                                var inv_invoiceauth_id = "";
                                var table = webix.$$("windowData");
                                if (table === undefined || table === null) {
                                    table = webix.$$("invoiceViews");
                                    item = table.getSelectedItem();
                                    inv_invoiceauth_id = item.hdon_id;
                                } else {
                                    item = table.getSelectedItem();
                                    inv_invoiceauth_id = item.inv_invoiceauth_id;
                                }
                                var url = app.config.host + "/Invoice/InvoiceHistorySendEmail?inv_invoiceauth_id=" + inv_invoiceauth_id;
                                webix.ajax(url, callback, view);
                            },
                        },
                        on: {
                            onresize: webix.once(function () {
                                this.adjustRowHeight("send_type", true);
                            }),
                            "data->onStoreUpdated": function () {
                                this.data.each(function (obj, i) {
                                    obj.stt = i + 1;
                                });
                            },
                            onItemDblClick: function (id, e, node) {
                                // var btnEdit = $$("btncskh");
                                // btnEdit.callEvent("onClick");
                                //webix.message("Click SCTV", "error");
                            },
                            onBeforeRender: function () {
                                //$$("tbl_email").refresh();
                                //$$("tbl_email").define("pager", kk);
                            },
                        },
                        columns: [
                            { id: "stt", header: "STT", width: 40 },
                            { id: "invoice_number", name: "invoice_number", header: "Số hóa đơn", width: 100, editor: "text" },
                            { id: "template_code", name: "template_code", header: "Mẫu hóa đơn", width: 120, editor: "text" },
                            { id: "invoice_series", name: "invoice_series", header: "Ký hiệu", width: 90, editor: "text" },
                            { id: "user_send", name: "user_send", header: "Người gửi", width: 130, editor: "text" },
                            {
                                id: "send_date",
                                name: "send_date",
                                header: "Ngày gửi",
                                width: 110,
                                editor: "date",
                                format: function (text) {
                                    var format = webix.Date.dateToStr("%d/%m/%Y");
                                    text = removeTimeZoneT(text);
                                    return format(text);
                                },
                                sort: "date",
                            },
                            { id: "send_to", name: "send_to", header: "Gửi đến", width: 150, editor: "text" },
                            { id: "send_type", name: "send_type", header: "Loại gửi", width: 150, editor: "text" },
                        ],
                        pager: "pagerB",
                        ready: function () {
                            //this.open(this.getFirstId());
                            this.sort({ by: "stt", dir: "desc" });
                        },
                    },
                    {
                        id: "pagerB",
                        view: "pager",
                        size: 5,
                        group: 5,
                        template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                    },
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy bỏ (ESC)",
                                align: "center",
                                width: 120,
                                type: "danger",
                                click: function () {
                                    webix.$$("email-win").close();
                                },
                            },
                            {},
                        ],
                    },
                ],
            },
        },
        initUI: initUI,
    };
});
