define(["app"], function (app) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "dmtab-win", position: "center",
			head: "Thông tin ngôn ngữ",
			width: 400,
			height: 250,
			body: {
				paddingY: 20,
				paddingX: 30,
				elementsConfig: { labelWidth: 60 },
				view: "form",
				id: "dmtab-form",
				complexData: true,
				margin: 0,
				elements: [
					{ view: "text", name: "wb_lang_id", label: "wb_lang_id", id: "wb_lang_id", hidden: true },
					{ view: "text", name: "key_lang", label: "Key", id: "dmtab-name" },
					{ view: "textarea", name: "value_lang", label: "Value", id: "dmtab-table" },
					{
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", view: "button", label: "Nhận (F10)", type: "form", align: "center", width: 120,
								on: {
									onClick: function () {
										var form = webix.$$("dmtab-form");
										var item = form.getValues();
										//console.log(item);
										var url = editmode == 1 ? app.config.host + "/System/InsertLanguages" : app.config.host + "/System/UpdateLanguages"

										webix.ajax()
											.headers({ 'Content-type': 'application/json' })
											.post(url, item, function (text) {
												var data = JSON.parse(text);

												if (!data.hasOwnProperty("error")) {
													item.id = data.id;
													$$("dmtab-win").close();
													app.callEvent("dataChanged", [editmode, item]);
												}
												else {
													webix.message(data.error, "error");
												}
											});
									}
								},
								click: function () {
									this.callEvent("onClick");

								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", align: "center", type: "danger", width: 120,
								click: function () {
									webix.$$("dmtab-win").close();
								}
							},
							{}
						]
					}

				]
			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});