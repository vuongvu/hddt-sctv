define([
    "app",
    "locale",
    "models/user"
], function (app, _, user) {
    var editmode = 0;

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            head: "Thêm mới hóa đơn đầu vào",
            id: "main",
            resize: true,
            move: true,
            on: {
                onBeforeShow: function () {
                    $$("uploader").files.attachEvent("onAfterDelete", function () {
                        $$("inv_Invoice_fileName").setValue(null);
                        $$("inv_Invoice_fileData").setValue(null);
                        $$("inv_Invoice_fileType").setValue(null);
                        $$("inv_Invoice_fileSize").setValue(null);
                    });
                }
            },
            body: {
                paddingY: 10,
                paddingX: 10,
                elementsConfig: { labelWidth: 100 },
                view: "form",
                id: "main-form",
                width: 800,
                height: 500,
                complexData: true,
                margin: 0,
                on: {
                    // onViewShow: function () {

                    // }
                },
                elements: [
                    {
                        view: "combo",
                        name: "inv_ncc_id",
                        label: "Nhà cung cấp",
                        //labelPosition: "top",
                        id: "inv_Invoice_inv_ncc_id",
                        required: true,
                        invalidMessage: "Nhà cung cấp không thể bỏ trống !",
                        options: {
                            view: "gridsuggest",
                            template: "#ten_ncc#",
                            url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00300",
                            body: {
                                columns: [
                                    { id: "mst", header: _("MS_THUE"), width: 150 },
                                    { id: "ten_ncc", header: _("Tên nhà cung cấp"), width: 550 }
                                ]
                            }

                        }
                    },
                    {
                        cols: [
                            {
                                view: "button", type: "icon", icon: "fas fa-folder-open", label: "Chọn file", width: 110,css: "webix_secondary",
                                on: {
                                    onClick: function () {
                                        webix.$$("uploader").fileDialog();
                                    }
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                }
                            },
                            {
                                rows: [
                                    // {view:"text",id:"df",label:"errr"},
                                    {
                                        id: "uploader",
                                        view: "uploader",
                                        value: 'Chọn file',
                                        hide: true,
                                        name: "uploader",
                                        accept: "image/png, image/jpeg, application/vnd.ms-excel,text/XML,text/plain,application/octet-stream,application/pdf,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                        //accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                        link: "mylist",
                                        autosend: false,
                                        //borderless: true,
                                        multiple: false,
                                        //upload: app.config.host + "/Invoice/SaveInv",
                                        on: {
                                            onBeforeFileAdd: function (file) {

                                                var size = file.size;
                                                if (size > 1024000) {
                                                    webix.message("Bạn chỉ được đính kèm File <= 1 Mb", "error");
                                                    return false;
                                                }
                                                var typeList = ["txt", "xml", "xls", "docx", "xlsx", "repx", "png", "pdf", "jpg"];
                                                if (typeList.indexOf(file.type) < 0) {
                                                    webix.message('Định dạng: txt, xml, xls, docx, xlsx, repx, png, pdf, jpg !', "error");
                                                    return false;
                                                }
                                            },
                                            onAfterRender: function () {
                                                //console.log('called once after first rendering:');
                                                this.hide();
                                            },
                                            onAfterFileAdd: function (upload) {
                                                var file = new Blob([upload.file]);

                                                var reader = new FileReader();
                                                var type = upload.type;
                                                if (type == "txt") { type = "text/plain"; }
                                                if (type == "xml") { type = "text/XML"; }
                                                if (type == "xls") { type = "application/vnd.ms-excel"; }
                                                if (type == "docx") { type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; }
                                                if (type == "xlsx") { type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
                                                if (type == "repx" || type == "rar") { type = "application/octet-stream"; }
                                                if (type == "jpg") { type = "image/jpeg"; }
                                                if (type == "png") { type = "image/png"; }
                                                if (type == "pdf") { type = "application/pdf"; }

                                                reader.onload = function () {

                                                    var arrayBuffer = this.result,
                                                        array = new Uint8Array(arrayBuffer);

                                                    var array1 = Array.from(array)
                                                    // console.log(array1);
                                                    //var data_btoa = btoa(array1);
                                                    var data = {
                                                        ten_file: upload.name,
                                                        file_data: array1,
                                                        loai_file: type
                                                    }

                                                    $$("inv_Invoice_fileName").setValue(upload.name);
                                                    $$("inv_Invoice_fileData").setValue(array1);
                                                    $$("inv_Invoice_fileType").setValue(type);
                                                    $$("inv_Invoice_fileSize").setValue(upload.size / 1024);
                                                }
                                                reader.readAsArrayBuffer(upload.file);

                                            },
                                            onUploadComplete: function (response) {
                                                // var topParentView = this.getTopParentView();

                                                // if (topParentView.config.tableId !== undefined) {
                                                // 	var table = $$(topParentView.config.tableId);

                                                // 	if (table != null) {
                                                // 		table.clearAll();
                                                // 		table.load(table.config.url);
                                                // 	}
                                                // }
                                                // webix.message("Upload File thành công !", "debug");
                                                // webix.$$("uploadfile-win").close();
                                            },
                                            onFileUploadError: function (file, response) {
                                                $$("mylist").clearAll();
                                                $$("mylist").refresh();
                                                //console.log(file);
                                                //console.log(response);
                                                var table = $$("main-form");
                                                table.enable();
                                                table.hideProgress();
                                                webix.message(response.error, "error");
                                            }
                                        },
                                        apiOnly: true,
                                    },
                                    {
                                        view: "list",
                                        id: "mylist",
                                        type: "uploader",
                                        //css:{"border-bottom": "none !important", "border-color":"white !important"},
                                        //height: 100,
                                        //autoheight: true
                                        borderless: true,
                                        on: {
                                            onAfterDelete: function (id) {
                                                webix.message("aaaaaaaaaaa");
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        view: "combo",
                        name: "dmloaihd_id",
                        label: "Loại hóa đơn",
                        //labelPosition: "top",
                        id: "inv_Invoice_dmloaihd_id",
                        required: true,
                        invalidMessage: "Loại hóa đơn không thể bỏ trống !",
                        options: {
                            view: "gridsuggest",
                            template: "#ten_loai#",
                            url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00301",
                            body: {
                                columns: [
                                    { id: "ma_loai", header: _("Mã loại"), width: 80 },
                                    { id: "ten_loai", header: _("Tên loại hóa đơn"), width: 700 }
                                ]
                            }
                        }
                    },
                    {
                        cols: [
                            { view: "text", label: _("Số hóa đơn"), name: "invoiceNumber", id: "inv_Invoice_invoiceNumber", attributes: { maxlength: 7 } },
                            { view: "datepicker", label: _("Ngày hóa đơn"), name: "invoiceDate", value: new Date(), id: "inv_Invoice_invoiceDate" }
                        ]

                    },
                    {
                        cols: [
                            {
                                view: "text",
                                label: "Mẫu số",
                                name: "mauhd",
                                id: "inv_Invoice_mauhd",
                                //labelWidth: 90,
                                required: true,
                                invalidMessage: "Mẫu số không được bỏ trống",
                                attributes: {
                                    maxlength: 11
                                },
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == undefined) return;
                                        if (newv == "") return;

                                        if (newv.length != 11) {
                                            webix.message({ type: "error", text: "Độ dài mẫu số phải là 11 ký tự" });
                                            this.focus();
                                            return;
                                        }

                                        var txtHinhThuc = "E";
                                        // var txtLoaihd = $$("dmloaihd_id_dmmauhoadon");

                                        if (txtHinhThuc == "") {
                                            webix.message({ type: "error", text: "Bạn chưa chọn hình thức hóa đơn" });
                                            return;
                                        }

                                        // if (txtLoaihd.getValue() == "") {
                                        //     webix.message({ type: "error", text: "Bạn chưa chọn loại hóa đơn" });
                                        //     return;
                                        // }

                                        // var hinhthuc = "E";
                                        // var list = txtLoaihd.getList();

                                        // var selectedItem = list.getItem(txtLoaihd.getValue());

                                        // if (selectedItem == undefined) return;

                                        // var ma_loai = selectedItem.ma_loai;

                                        // var so_lien = hinhthuc == "E" ? 0 : "";
                                        // var mau_so = ma_loai + so_lien + "/";

                                        // if (newv.startsWith(mau_so) == false) {
                                        //     webix.message({ type: "error", text: "Mẫu số không đúng" });
                                        //     this.focus();
                                        //     return;
                                        // }

                                        var sttMau = newv.substring(newv.length - 3, newv.length);
                                        var patt = /^[0-9][0-9][0-9]$/;

                                        if (patt.test(sttMau) == false) {
                                            webix.message({ type: "error", text: "3 ký tự cuối phải là số" });
                                            this.focus();
                                        }
                                    }
                                }
                            },
                            {
                                view: "text",
                                label: "Ký hiệu",
                                name: "invoiceSeries",
                                id: "inv_Invoice_invoiceSeries",
                                // labelWidth: 110,
                                required: true,
                                invalidMessage: "Ký hiệu không được bỏ trống",
                                attributes: { maxlength: 6 },
                                on: {
                                    onChange: function (newv, oldv) {
                                        if (newv == undefined) return;
                                        if (newv == "") return;

                                        if (newv.length != 6) {
                                            webix.message({ type: "error", text: "Độ dài ký hiệu phải là 6 ký tự" });
                                            this.focus();
                                            return;
                                        }

                                        var patt = /^[A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][/]$/;

                                        if (patt.test(newv.substring(0, 3)) == false) {
                                            webix.message({ type: "error", text: "3 ký tự đầu của ký hiệu không đúng" });
                                            this.focus();
                                            return;
                                        }

                                        patt = /^[1-9][1-9]$/;

                                        var nam = newv.substring(3, 5);

                                        if (patt.test(nam) == false) {
                                            webix.message({ type: "error", text: "2 ký tự năm của ký hiệu không đúng" });
                                            this.focus();
                                            return;
                                        }

                                        var now = new Date();
                                        var year = now.getFullYear();

                                        nam = Number("20" + nam);

                                        if (nam < (year - 1) || nam > year) {
                                            webix.message({ type: "error", text: "Năm ký hiệu không đúng" });
                                            this.focus();
                                            return;
                                        }

                                        if (newv.substring(5, 6) != "E") {
                                            webix.message({ type: "error", text: "Ký tự cuối phải là E" });
                                            this.focus();
                                            return;
                                        }

                                    }
                                }
                            },
                        ]
                    },
                    {
                        id: "inv_Invoice_sellerTaxCode",
                        name: "sellerTaxCode",
                        view: "search",
                        label: "MST người bán",
                        //align: "center",
                        placeholder: _("SEARCH") + "...",
                        // width: 300,
                        on: {
                            onTimedKeyPress: function () {
                                // var table = $$("tblSearch");

                                // table.clearAll();
                                // table.load(table.config.url);
                            },
                            onSearchIconClick: function (e) {
                                var text = this.getValue();
                                var datapost = { "masothue": text };
                                var table = $$("main-form");
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();
                                webix.ajax()
                                    .headers({ 'Content-type': 'application/json' })
                                    .post(app.config.host + "/System/GetInfoByMST", datapost, function (text) {

                                        var json = JSON.parse(text);
                                        table.enable();
                                        table.hideProgress();
                                        if (!json.hasOwnProperty("error")) {
                                            $$("inv_Invoice_sellerName").setValue(json.ten_cty);
                                            $$("inv_Invoice_sellerAdress").setValue(json.dia_chi);
                                        }
                                        else {
                                            // nếu không tồn tại mã số thuế phải xóa địa chỉ + tên cty ở đây 
                                            webix.message(json.error, "error", 1500);
                                        }

                                    });
                            }
                        }
                    },
                    { view: "text", label: _("Tên người bán"), name: "sellerName", id: "inv_Invoice_sellerName" },
                    { view: "text", label: _("Địa chỉ"), name: "sellerAdress", id: "inv_Invoice_sellerAdress" },
                    { view: "text", label: _("Tên mặt hàng tiêu biểu"), name: "itemName", id: "inv_Invoice_itemName" },
                    { view: "text", label: _("Tiền trước thuế"), name: "totalAmountWithoutVat", id: "inv_Invoice_totalAmountWithoutVat", inputAlign: "right", type: "number" },
                    { view: "text", label: _("Tiền thuế"), name: "vatAmount", id: "inv_Invoice_vatAmount", inputAlign: "right", type: "number" },
                    { view: "textarea", label: _("GHI_CHU"), name: "ghi_chu", id: "inv_Invoice_ghi_chu" },
                    { view: "text", label: _("Tên File"), name: "fileName", id: "inv_Invoice_fileName", hidden: true },
                    { view: "text", label: _("File Data"), name: "fileData", id: "inv_Invoice_fileData", hidden: true },
                    { view: "text", label: _("Mime Type"), name: "fileType", id: "inv_Invoice_fileType", hidden: true },
                    { view: "text", label: _("File Size"), name: "fileSize", id: "inv_Invoice_fileSize", hidden: true },
                    { view: "text", label: _("File Url"), name: "fileUrl", id: "inv_Invoice_fileUrl", hidden: true },
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                shortcut: "F10", view: "button", label: "Nhận (F10)", type: "form", align: "center",
                                on: {
                                    onClick: function () {
                                        var item = $$("main-form").getValues();
                                        var table = $$("main-form");

                                        if (table.validate()) {
                                            // var fn = Function(check);
                                            // var result = fn();
                                            // console.log(result);
                                            webix.extend(table, webix.ProgressBar);
                                            table.disable();
                                            table.showProgress();
                                            var file = $$("uploader").files.getFirstId();
                                            // if (file == undefined || file == null) {
                                            //     webix.message("Bạn chưa chọn File hóa đơn đính kèm !", "error");
                                            //     table.enable();
                                            //     table.hideProgress();
                                            //     return;
                                            // }
                                            var item1 = [];
                                            item1.push(item);
                                            var data_post = { editmode: editmode, data: item1 };

                                            webix.ajax()
                                                .headers({ 'Content-type': 'application/json' })
                                                .post(app.config.host + "/ManageInvoice/SaveInv", data_post, {
                                                    error: function (text, data, XmlHttpRequest) {
                                                        table.enable();
                                                        table.hideProgress();
                                                        // var json = JSON.parse(text);
                                                        // webix.message(json.ExceptionMessage, "error");
                                                        webix.message(text + XmlHttpRequest, "error");

                                                    }, success: function (text, data, XmlHttpRequest) {
                                                        var json = JSON.parse(text);
                                                        table.enable();
                                                        table.hideProgress();
                                                        if (!json.hasOwnProperty("error")) {
                                                            webix.$$("main").close();
                                                            app.callEvent("dataChangedMain", [editmode, json.data]);
                                                        }
                                                        else {
                                                            webix.message(json.error, "error");
                                                        }
                                                    }
                                                });
                                        }
                                    }
                                },
                                click: function () {
                                    this.callEvent("onClick");

                                }
                            },
                            {
                                view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", align: "center", type: "danger",
                                click: function () {
                                    $$("mylist").clearAll();
                                    $$("mylist").unbind();
                                    var id = $$("uploader").files.getFirstId();
                                    if (id != undefined) {
                                        $$("uploader").stopUpload(id);
                                    }
                                    this.getTopParentView().close();
                                }
                            },
                            {}
                        ]
                    }
                ]
            }
        },
        setValues: function (item, winconfig, params) {
            var win = $$(this.$ui.id);

            var form = win.getChildViews()[1];

            item["invoiceDate"] = removeTimeZoneT(item["invoiceDate"]);
            form.setValues(item);

            if (editmode == 2) {
                $$("uploader").files.parse([
                    { name: item.fileName == null ? "" : item.fileName, sizetext: item.fileSize == null ? "" : item.fileSize, status: "server" }
                ]);
            }
        },
        setEditMode: function (mode) {
            editmode = mode;

            //var form = webix.$$("main-form");
            var form = $$(this.$ui.id);
            //var topParentView = form.getTopParentView();
            form.config.editmode = mode;
        }
    };

});
