define(["app", "models/user", "views/forms/68_bangdanhsachhoadon"], function (app, user, bangdanhsachhoadon) {
    var editmode = 0;

    var fm = webix.Number.numToStr({
        groupDelimiter: ",",
        groupSize: 3,
        decimalDelimiter: ".",
        decimalSize: 0,
    });

    var getNgay = function () {
        var option = $$("lkdlieu").getValue();
        if (option == "Q") {
            var nam = $$("nam").getValue();
            var thang;
            var quy = $$("thang").getValue();
            if (quy == "1") {
                thang = 0;
            } else if (quy == "2") {
                thang = 3;
            } else if (quy == "3") {
                thang = 6;
            } else if (quy == "4") {
                thang = 9;
            }
            var firstDate = new Date(nam, thang, 1);
            var lastDate = new Date(nam, thang + 3, 0);

            $$("tu_ngay").setValue(firstDate);
            $$("tu_ngay").refresh();
            $$("den_ngay").setValue(lastDate);
            $$("den_ngay").refresh();
        } else if (option == "T") {
            var year = $$("nam").getValue();
            var month = $$("thang").getValue();
            var date1 = month + "/01" + "/" + year;

            var date = new Date(date1);
            var firstDay1 = new Date(date.getFullYear(), date.getMonth(), 1);

            var lastDay1 = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            $$("tu_ngay").setValue(firstDay1);
            $$("tu_ngay").refresh();
            $$("den_ngay").setValue(lastDay1);
            $$("den_ngay").refresh();
        } else if (option == "N") {
            var year = $$("nam").getValue();
            var month = $$("thang").getValue();
            var day = $$("ngay").getValue();
            var date1 = month + "/" + day + "/" + year;

            var date = new Date(date1);

            $$("tu_ngay").setValue(date);
            $$("tu_ngay").refresh();
            $$("den_ngay").setValue(date);
            $$("den_ngay").refresh();
        }

        var tableDetail = webix.$$("danhsach_hoadon");
        tableDetail.clearAll();
    };

    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Lưu tạm (F10)",
            id: "btnBTHSave",
            type: "form",
            align: "center",
            width: 120,
            on: {
                onClick: function () {
                    var tableHoadon = $$("bangtonghop-form");
                    if (tableHoadon.validate()) {
                        var ds_hdon = $$("danhsach_hoadon").serialize();
                        if (ds_hdon.length == 0) {
                            webix.message("Bạn chưa nhập chi tiết hóa đơn", "error");
                            return;
                        } else {
                            webix.extend(tableHoadon, webix.ProgressBar);
                            tableHoadon.disable();
                            tableHoadon.showProgress();
                            // console.log(data_post);
                            var limit = 1000;
                            var m = parseInt(ds_hdon.length / limit);
                            if (ds_hdon.length > m * limit) m = m + 1;
                            var apiIndex = 0;
                            var item = $$("bangtonghop-form").getValues();
                            for (var k = 0; k < m; k++) {
                                if (k < m - 1) {
                                    item["details"] = ds_hdon.slice(k * limit, (k + 1) * limit);
                                } else {
                                    item["details"] = ds_hdon.slice(k * limit, ds_hdon.length);
                                }
                                var tgtcthue = 0;
                                var tgtthue = 0;
                                var tgtttoan = 0;
                                item["details"].forEach(function (it) {
                                    tgtcthue += it["ttcthue"];
                                    tgtthue += it["tgtthue"];
                                    tgtttoan += it["tgtttoan"];
                                });
                                item["tgtcthue"] = tgtcthue;
                                item["tgtthue"] = tgtthue;
                                item["tgtttoan"] = tgtttoan;
                                var item1 = [];
                                item1.push(item);
                                var data_post = { editmode: editmode, data: item1 };
                                if (item.lhhoa === "1") {
                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Invoice68/SaveBangTHDLXangDau", data_post, {
                                            error: function (text, data, XmlHttpRequest) {
                                                tableHoadon.enable();
                                                tableHoadon.hideProgress();
                                                // var json = JSON.parse(text);
                                                // webix.message(json.ExceptionMessage, "error");
                                                webix.message(text + XmlHttpRequest, "error");
                                            },
                                            success: function (text, data, XmlHttpRequest) {
                                                var json = JSON.parse(text);
                                                tableHoadon.enable();
                                                tableHoadon.hideProgress();
                                                if (!json.hasOwnProperty("error")) {
                                                    webix.$$("bangtonghop-win").close();
                                                    app.callEvent("dataChangeBangtonghop", [1, null]);
                                                } else {
                                                    webix.message(json.error, "error");
                                                }
                                            },
                                        });
                                } else {
                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Invoice68/SaveBangTHDL", data_post, {
                                            error: function (text, data, XmlHttpRequest) {
                                                tableHoadon.enable();
                                                tableHoadon.hideProgress();
                                                // var json = JSON.parse(text);
                                                // webix.message(json.ExceptionMessage, "error");
                                                webix.message(text + XmlHttpRequest, "error");
                                            },
                                            success: function (text, data, XmlHttpRequest) {
                                                var json = JSON.parse(text);
                                                tableHoadon.enable();
                                                tableHoadon.hideProgress();
                                                if (!json.hasOwnProperty("error")) {
                                                    webix.$$("bangtonghop-win").close();
                                                    app.callEvent("dataChangeBangtonghop", [1, null]);
                                                } else {
                                                    webix.message(json.error, "error");
                                                }
                                            },
                                        });
                                }
                            }
                        }
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        // {
        //     shortcut: "F11",
        //     view: "button",
        //     label: "Xuất tờ khai XML (F11)",
        //     type: "form",
        //     align: "center",
        //     width: 180,
        //     on: {
        //         onClick: function () {
        //         }
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     }
        // },
        // {
        //     shortcut: "F12",
        //     id: "btnBTHSend",
        //     view: "button",
        //     label: "Ký và gửi CQT (F12)",
        //     type: "form",
        //     align: "center",
        //     width: 150,
        //     on: {
        //         onClick: function () {
        //         }
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     }
        // },
        {
            view: "button",
            hotkey: "esc",
            label: "Quay lại (ESC)",
            type: "danger",
            align: "center",
            width: 120,
            click: function () {
                webix.$$("bangtonghop-win").close();
            },
        },
        {},
    ];

    var ui = {
        view: "window",
        modal: true,
        move: true,
        fullscreen: true,
        id: "bangtonghop-win",
        position: "center",
        head: "BẢNG TỔNG HỢP DỮ LIỆU HÓA ĐƠN ĐIỆN TỬ",
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            width: 1500,
            id: "bangtonghop-form",
            complexData: true,
            margin: 0,
            elements: [
                {
                    rows: [
                        {
                            cols: [
                                {},
                                {
                                    view: "label",
                                    label: "[01] Kỳ dữ liệu",
                                    width: 170,
                                },
                                {
                                    id: "lkdlieu",
                                    name: "lkdlieu",
                                    view: "combo",
                                    width: 120,
                                    value: "T",
                                    options: [
                                        { id: "T", value: "Tháng" },
                                        { id: "Q", value: "Quý" },
                                        { id: "N", value: "Ngày" },
                                    ],
                                    on: {
                                        onChange: function (newv, oldv) {
                                            if (newv == "N") {
                                                var k = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
                                                $$("thang").define("options", k);
                                                $$("thang").define("width", 60);
                                                $$("thang").refresh();
                                                $$("ngay").show();
                                                $$("ngay").refresh();
                                            }
                                            if (newv == "T") {
                                                var k = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
                                                $$("thang").define("options", k);
                                                $$("thang").define("width", 120);
                                                $$("thang").refresh();
                                                $$("ngay").hide();
                                                $$("ngay").refresh();
                                            }
                                            if (newv == "Q") {
                                                var k = ["1", "2", "3", "4"];
                                                $$("thang").define("options", k);
                                                $$("thang").define("width", 120);
                                                $$("thang").refresh();
                                                $$("ngay").hide();
                                                $$("ngay").refresh();
                                            }

                                            getNgay();
                                        },
                                    },
                                },
                                {
                                    id: "ngay",
                                    name: "ngay",
                                    view: "combo",
                                    width: 60,
                                    value: "1",
                                    hidden: true,
                                    options: [
                                        "1",
                                        "2",
                                        "3",
                                        "4",
                                        "5",
                                        "6",
                                        "7",
                                        "8",
                                        "9",
                                        "10",
                                        "11",
                                        "12",
                                        "13",
                                        "14",
                                        "15",
                                        "16",
                                        "17",
                                        "18",
                                        "19",
                                        "20",
                                        "21",
                                        "22",
                                        "23",
                                        "24",
                                        "25",
                                        "26",
                                        "27",
                                        "28",
                                        "29",
                                        "30",
                                        "31",
                                    ],
                                    required: true,
                                    invalidMessage: "Tháng/Quý không được để trống !",
                                    on: {
                                        onChange: function () {
                                            getNgay();
                                        },
                                    },
                                },
                                {
                                    id: "thang",
                                    name: "thang",
                                    view: "combo",
                                    width: 120,
                                    value: "1",
                                    options: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
                                    required: true,
                                    invalidMessage: "Tháng/Quý không được để trống !",
                                    on: {
                                        onChange: function () {
                                            getNgay();
                                        },
                                    },
                                },
                                {
                                    id: "nam",
                                    name: "nam",
                                    view: "text",
                                    label: "Năm",
                                    labelWidth: 50,
                                    width: 120,
                                    required: true,
                                    invalidMessage: "Năm không được để trống !",
                                    on: {
                                        onChange: function () {
                                            getNgay();
                                        },
                                    },
                                },
                                { id: "tu_ngay", name: "tu_ngay", view: "datepicker", label: "Từ ngày", hidden: true, width: 200, labelWidth: 100 },
                                { id: "den_ngay", name: "den_ngay", view: "datepicker", label: "Đến ngày:", hidden: true, labelWidth: 100, width: 260 },
                                {
                                    id: "ldau",
                                    name: "ldau",
                                    view: "checkbox",
                                    label: "[02] Lần đầu",
                                    value: 1,
                                    width: 150,
                                    on: {
                                        onChange: function () {
                                            var lanDau = $$("ldau").getValue();
                                            if (lanDau == 0) {
                                                $$("bslthu").define("required", true);
                                                $$("bslthu").define("invalidMessage", "Không được để trống trường này!");
                                                $$("bslthu").enable();
                                                $$("bslthu").refresh();
                                            } else {
                                                $$("bslthu").define("required", false);
                                                $$("bslthu").define("invalidMessage", "");
                                                $$("bslthu").setValue("");
                                                $$("bslthu").disable();
                                                $$("bslthu").refresh();
                                            }
                                        },
                                    },
                                },
                                { id: "bslthu", name: "bslthu", view: "text", label: "[03] Bổ sung lần thứ: ", labelWidth: 140, width: 250, disabled: true },
                                { id: "sdlthu", name: "sdlthu", view: "text", label: "[04] Sửa đổi lần thứ: ", labelWidth: 130, width: 250 },
                                {},
                            ],
                        },
                        {
                            cols: [
                                {},
                                {
                                    id: "tnnt",
                                    name: "tnnt",
                                    view: "text",
                                    label: "[05] Tên người nộp thuế:",
                                    labelWidth: 170,
                                    width: 410,
                                    required: true,
                                    invalidMessage: "Tên người nộp thuế không được để trống !",
                                    readonly: true,
                                },
                                {
                                    id: "mst",
                                    name: "mst",
                                    view: "text",
                                    label: "[06] Mã số thuế:",
                                    labelWidth: 130,
                                    width: 380,
                                    readonly: true,
                                    required: true,
                                    invalidMessage: "Mã số thuế không được để trống !",
                                },
                                { id: "nlap", name: "nlap", view: "datepicker", label: "Ngày lập", width: 190, labelWidth: 70, value: new Date() },
                                { id: "sbthdlieu", name: "sbthdlieu", view: "text", label: "Số:", labelWidth: 50, width: 200, readonly: true },
                                {},
                            ],
                        },
                        {
                            cols: [
                                {},
                                {
                                    id: "hddin",
                                    name: "hddin",
                                    label: "Hóa đơn đặt in:",
                                    labelWidth: 170,
                                    width: 410,
                                    view: "combo",
                                    value: "0",
                                    options: [
                                        { id: "0", value: "Hóa đơn điện tử" },
                                        { id: "1", value: "Hóa đơn đặt in" },
                                    ],
                                },
                                {
                                    id: "lhhoa",
                                    name: "lhhoa",
                                    view: "text",
                                    label: "Loại hàng hóa:",
                                    labelWidth: 130,
                                    width: 380,
                                    view: "combo",
                                    value: "9",
                                    options: [
                                        { id: "1", value: "Xăng dầu" },
                                        { id: "2", value: "Vận tải hàng không" },
                                        { id: "9", value: "Khác" },
                                    ],
                                    on: {
                                        onChange: function () {
                                            var loaiHangHoa = $$("lhhoa").getValue();
                                            if (loaiHangHoa === "1") {
                                                $$("lkdlieu").setValue("N");
                                                $$("lkdlieu").disable();
                                                $$("danhsach_hoadon").clearAll();
                                                $$("tgtcthue").setValue("");
                                                $$("tgtthue").setValue("");
                                                $$("tgtttoan").setValue("");
                                            } else {
                                                $$("lkdlieu").setValue("T");
                                                $$("lkdlieu").enable();
                                                $$("danhsach_hoadon").clearAll();
                                                $$("tgtcthue").setValue("");
                                                $$("tgtthue").setValue("");
                                                $$("tgtttoan").setValue("");
                                            }
                                        },
                                    },
                                },
                                { width: 390 },
                                {},
                            ],
                        },
                    ],
                },
                {
                    rows: [
                        {
                            cols: [
                                {
                                    view: "button",
                                    label: "Chọn hóa đơn",
                                    id: "btnTaiHoaDon",
                                    width: 150,
                                    on: {
                                        onItemClick: function (id, e) {
                                            var item = $$("bangtonghop-form").getValues();
                                            var tableHoadon = $$("bangtonghop-form");
                                            if (tableHoadon.validate()) {
                                                var loaiHangHoa = $$("lhhoa").getValue();
                                                if (loaiHangHoa !== "1") {
                                                    var detailForm = this.getTopParentView().$scope.ui(bangdanhsachhoadon.$ui);
                                                    bangdanhsachhoadon.setEditMode(1);
                                                    bangdanhsachhoadon.setParams(item);
                                                    detailForm.show();
                                                } else {
                                                    var detailForm = this.getTopParentView().$scope.ui(bangdanhsachhoadon.$ui);
                                                    bangdanhsachhoadon.setEditMode(1);
                                                    bangdanhsachhoadon.setParams(item);
                                                    detailForm.show();
                                                }
                                            }
                                        },
                                    },
                                },
                                {
                                    view: "button",
                                    label: "Chọn toàn bộ",
                                    id: "btnAllHoaDon",
                                    width: 150,
                                    on: {
                                        onItemClick: function (id, e) {
                                            var tableHoadon = $$("bangtonghop-form");
                                            if (tableHoadon.validate()) {
                                                webix.confirm({
                                                    text: "Tự động chọn tất cả hóa đơn trong kì",
                                                    ok: "Có",
                                                    cancel: "Không",
                                                    callback: function (res) {
                                                        if (res) {
                                                            var params = $$("bangtonghop-form").getValues();
                                                            var tableHoadon = webix.$$("danhsach_hoadon");
                                                            if (tableHoadon.validate()) {
                                                                webix.extend($$("bangtonghop-form"), webix.ProgressBar);
                                                                $$("bangtonghop-form").disable();
                                                                $$("bangtonghop-form").showProgress();
                                                                webix
                                                                    .ajax()
                                                                    .headers({ "Content-type": "application/json" })
                                                                    .post(app.config.host + "/Invoice68/GetHoadonChoXuLyall", params, {
                                                                        error: function (text, data, XmlHttpRequest) {
                                                                            $$("bangtonghop-form").enable();
                                                                            $$("bangtonghop-form").hideProgress();
                                                                            webix.message(text, "error");
                                                                        },
                                                                        success: function (text, data, XmlHttpRequest) {
                                                                            var json = JSON.parse(text);
                                                                            if (!json.hasOwnProperty("error")) {
                                                                                //console.log(json);
                                                                                tableHoadon.clearAll();
                                                                                tableHoadon.parse(json);

                                                                                tableHoadon.refresh();
                                                                            } else {
                                                                                webix.message(json.error, "error");
                                                                            }
                                                                            $$("bangtonghop-form").enable();
                                                                            $$("bangtonghop-form").hideProgress();
                                                                        },
                                                                    });
                                                            }
                                                        }
                                                    },
                                                });
                                            }
                                        },
                                    },
                                },
                                {
                                    view: "button",
                                    label: "Xóa",
                                    id: "btnXoaHoaDon",
                                    width: 120,
                                    on: {
                                        onItemClick: function (id, e) {
                                            var table = webix.$$("danhsach_hoadon");
                                            table.editStop();
                                            var item = table.getSelectedItem();
                                            if (!webix.isUndefined(item)) {
                                                table.remove(item.id);
                                                table.clearSelection();

                                                var table = webix.$$("danhsach_hoadon");
                                                var data = table.serialize();

                                                var tien_tt = 0;
                                                var tien_thue = 0;
                                                var tong_tien = 0;

                                                for (var i = 0; i < data.length; i++) {
                                                    var item = data[i];
                                                    tien_tt += item["ttcthue"] == null ? 0 : item["ttcthue"];
                                                    tien_thue += item["tgtthue"] == null ? 0 : item["tgtthue"];
                                                    tong_tien += item["tgtttoan"] == null ? 0 : item["tgtttoan"];
                                                }

                                                $$("tgtcthue").setValue(fm(tien_tt));
                                                $$("tgtthue").setValue(fm(tien_thue));
                                                $$("tgtttoan").setValue(fm(tong_tien));

                                                return;
                                            }
                                            var editor = table.getEditState();
                                            if (!webix.isUndefined(editor)) {
                                                table.remove(editor.row);
                                                table.clearSelection();

                                                var table = webix.$$("danhsach_hoadon");
                                                var data = table.serialize();

                                                var tien_tt = 0;
                                                var tien_thue = 0;
                                                var tong_tien = 0;

                                                for (var i = 0; i < data.length; i++) {
                                                    var item = data[i];
                                                    tien_tt += item["ttcthue"] == null ? 0 : item["ttcthue"];
                                                    tien_thue += item["tgtthue"] == null ? 0 : item["tgtthue"];
                                                    tong_tien += item["tgtttoan"] == null ? 0 : item["tgtttoan"];
                                                }

                                                $$("tgtcthue").setValue(fm(tien_tt));
                                                $$("tgtthue").setValue(fm(tien_thue));
                                                $$("tgtttoan").setValue(fm(tong_tien));

                                                return;
                                            }
                                        },
                                    },
                                },
                            ],
                        },
                        {
                            view: "datatable",
                            id: "danhsach_hoadon",
                            minHeight: 350,
                            name: "danhsach_hoadon",
                            select: true,
                            scroll: true,
                            resizeColumn: true,
                            // footer: true,
                            editable: false,
                            tooltip: true,
                            scheme: {
                                $init: function (obj) {},
                            },
                            on: {
                                "data->onStoreUpdated": function () {
                                    this.data.each(function (obj, i) {
                                        obj.stt = i + 1;
                                    });
                                },
                            },
                            columns: [
                                {
                                    id: "stt",
                                    name: "stt",
                                    header: "STT",
                                    width: 40,
                                },
                                {
                                    id: "khieu",
                                    name: "khieu",
                                    header: "Ký hiệu",
                                    width: 120,
                                    // footer: "Tổng cộng"
                                },
                                { id: "shdon", name: "shdon", header: "Số hóa đơn", width: 120 },
                                {
                                    id: "nlap",
                                    name: "nlap",
                                    header: "Ngày hóa đơn",
                                    width: 120,
                                    format: function (text) {
                                        var format = webix.Date.dateToStr("%d/%m/%Y");
                                        text = removeTimeZoneT(text);
                                        return format(text);
                                    },
                                },
                                { id: "tnmua", name: "tnmua", header: "Tên người mua", width: 350 },
                                { id: "mstnmua", name: "mstnmua", header: "Mã số thuế người mua/mã khách hàng", width: 150 },
                                { id: "thhdvu", name: "thhdvu", header: "Mặt hàng", width: 150 },
                                {
                                    id: "sluong",
                                    name: "sluong",
                                    header: "Số lượng hàng hóa",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 4,
                                    }),
                                    css: { "text-align": "right" },
                                },
                                {
                                    id: "ttcthue",
                                    name: "ttcthue",
                                    header: "Tiền trước thuế",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 4,
                                    }),
                                    css: { "text-align": "right" },
                                    // footer: {
                                    //     content: "summColumn"
                                    // }
                                },
                                {
                                    id: "tsuat",
                                    name: "tsuat",
                                    header: "Thuế suất",
                                    width: 150,
                                    css: { "text-align": "right" },
                                },
                                {
                                    id: "tgtthue",
                                    name: "tgtthue",
                                    header: "Tiền thuế",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 4,
                                    }),
                                    css: { "text-align": "right" },
                                    // footer: {
                                    //     content: "summColumn"
                                    // }
                                },
                                {
                                    id: "tgtttoan",
                                    name: "tgtttoan",
                                    header: "Tiền thanh toán",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 4,
                                    }),
                                    css: { "text-align": "right" },
                                    // footer: {
                                    //     content: "summColumn"
                                    // }
                                },
                                {
                                    id: "tthai",
                                    name: "tthai",
                                    header: "Trạng thái",
                                    width: 150,
                                    format: function (text) {
                                        var result = "";
                                        if (text == 0) {
                                            result = "Mới";
                                        } else if (text == 1) {
                                            result = "Hủy";
                                        } else if (text == 2) {
                                            result = "Điều chỉnh";
                                        } else if (text == 3) {
                                            result = "Thay thế";
                                        } else if (text == 4) {
                                            result = "Giải trình";
                                        } else if (text == 5) {
                                            result = "Sai sót do tổng hợp";
                                        }
                                        return result;
                                    },
                                },
                                { id: "tthdlquan", name: "tthdlquan", header: "Thông tin hóa đơn liên quan", width: 250 },
                                { id: "gchu", name: "gchu", header: "Ghi chú", width: 150 },
                            ],
                        },
                        {
                            cols: [
                                {
                                    id: "tgtcthue",
                                    name: "tgtcthue",
                                    view: "text",
                                    label: "Tiền trước thuế",
                                    readonly: true,
                                    labelWidth: 100,
                                    css: "mcls_readonly_text",
                                    // format: webix.Number.numToStr({
                                    //     groupDelimiter: ",",
                                    //     groupSize: 3,
                                    //     decimalDelimiter: ".",
                                    //     decimalSize: 0,
                                    // })
                                },
                                {
                                    id: "tgtthue",
                                    name: "tgtthue",
                                    view: "text",
                                    label: "Tiền thuế",
                                    readonly: true,
                                    labelWidth: 100,
                                    css: "mcls_readonly_text",
                                    // format: webix.Number.numToStr({
                                    //     groupDelimiter: ",",
                                    //     groupSize: 3,
                                    //     decimalDelimiter: ".",
                                    //     decimalSize: 0,
                                    // })
                                },
                                {
                                    id: "tgtttoan",
                                    name: "tgtttoan",
                                    view: "text",
                                    readonly: true,
                                    labelWidth: 100,
                                    css: "mcls_readonly_text",
                                    label: "Tiền thanh toán",
                                    // format: webix.Number.numToStr({
                                    //     groupDelimiter: ",",
                                    //     groupSize: 3,
                                    //     decimalDelimiter: ".",
                                    //     decimalSize: 0,
                                    // })
                                },
                            ],
                        },
                    ],
                },
                {
                    margin: 10,
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },
        setValues: function (item) {
            var win = $$(this.$ui.id);
            var form = win.getChildViews()[1];

            item["nlap"] = removeTimeZoneT(item["nlap"]);
            form.setValues(item);

            $$("tgtcthue").setValue(fm(item.tgtcthue));
            $$("tgtthue").setValue(fm(item.tgtthue));
            $$("tgtttoan").setValue(fm(item.tgtttoan));

            getNgay();

            var tableDetail = webix.$$("danhsach_hoadon");
            if (item.lhhoa === 1) {
                var url = app.config.host + "/Invoice68/GetDataBangTHDLXangDauDetail?id=" + item.id;
            } else {
                var url = app.config.host + "/Invoice68/GetDataBangTHDLDetail?id=" + item.id;
            }
            tableDetail.clearAll();
            tableDetail.load(url);
        },
    };
});
