define(["app", "locale"], function (app, _) {
    var editmode = 0;

    return {
        $ui: {
            view: "window", modal: true, id: "tokenfile-win", position: "center",
            head: "Thông tin Token File",
            //height:420,		
            body: {
                paddingY: 10,
                paddingX: 10,
                view: "form",
                id: "tokenfile-form",
                complexData: true,
                margin: 0,
                elements: [
                    {
                        view: "textarea",
                        name: "file_path",
                        labelPosition: "top",
                        label: _("PATH"),
                        height: 100,
                        required: true,
                        validate: webix.rules.isNotEmpty,
                        invalidMessage: "Đường dẫn không được để trống !"
                    },
                    {
                        view: "text",
                        name: "pass",
                        labelPosition: "top",
                        label: _("PASSWORD"),
                        type: "password",
                        required: true,
                        invalidMessage: "Password không được để trống !",
                        validate: webix.rules.isNotEmpty
                    },
                    {},
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                shortcut: "F10", view: "button", label: "Lưu (F10)", type: "form", align: "center", width: 120,
                                on: {
                                    onClick: function () {
                                        if ($$("tokenfile-form").validate()) {
                                            var form = webix.$$("tokenfile-form");

                                            var item = form.getValues();

                                            var khData = {};

                                            khData.windowid = app.path[1].params[0];
                                            khData.editmode = editmode;
                                            khData.data = [];

                                            khData.data.push(item);

                                            webix.ajax()
                                                .headers({ 'Content-type': 'application/json' })
                                                .post(app.config.host + "/Token/SaveTokenFile", khData, function (text) {

                                                    var json = JSON.parse(text);

                                                    if (!json.hasOwnProperty("error")) {
                                                        $$("tokenfile-win").close();
                                                        app.callEvent("dataChanged", [editmode, json.data]);
                                                    }
                                                    else {
                                                        webix.message(json.error, "error");
                                                    }


                                                });
                                        }
                                    }
                                },
                                click: function () {
                                    this.callEvent("onClick");

                                }
                            },
                            {
                                view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", align: "center", width: 120, type: "danger",
                                click: function () {
                                    webix.$$("tokenfile-win").close();
                                }
                            },
                            {}
                        ]
                    }

                ]
            }

        },
        setEditMode: function (mode) {
            editmode = mode;
        }
    };

});