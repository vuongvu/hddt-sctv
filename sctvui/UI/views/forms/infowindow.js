define([
	"app",
	"locale",
	"models/user"
], function (app, _, user) {
	var editmode = 0;

	var _date = new Date();
	var _month = _date.getMonth() + 1;
	function subviewT() {
		$$('cbQuy').define('label', "Tháng");
		$$('cbQuy').refresh();
		$$('cbQuy').define('options', ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]);
		//webix.extend(config, $$('cbQuy').config);
		//webix.ui(config, $$('cbThang'));
		//$$('cbThang').destructor();
	}

	function subviewQ() {

		$$('cbQuy').define('label', "Quý");
		$$('cbQuy').refresh();
		$$('cbQuy').define('options', ["1", "2", "3", "4"]);
		// webix.extend($$('cbQuy').config, config, true);
		//webix.ui(config, $$('cbQuy'));
		//$$('cbQuy').destructor();
	}

	return {
		$ui: {
			view: "window",
			modal: true,
			position: "center",
			head: "Thông tin Tab",
			resize: true,
			body: {
				paddingY: 10,
				paddingX: 10,
				elementsConfig: { labelWidth: 60 },
				view: "form",
				complexData: true,
				margin: 0,
				elements: []
			}

		},
		initUI: function (infoWindowId) {

			var url = app.config.host + "/System/ExecuteCommand";

			var parameters = {
				command: "CM00013",
				parameter: {
					wb_infowindow_id: infoWindowId
				}
			};

			webix.ajax().headers({ "Content-Type": "application/json" })
				.post(url, parameters, function (response) {
					var result = JSON.parse(response);

					var win = webix.$$(infoWindowId);

					win.define("width", result[0].window_width);
					win.define("height", result[0].window_height);

					var form = win.getChildViews()[1];
					form.clear();

					var view_extension = {
						cols: [
							{
								view: "radio",
								id: "head_extension",
								value: 1,
								options: [
									{ "id": 1, "value": "Tháng" }, // the initially selected item
									{ "id": 2, "value": "Quý" }
								],
								on: {
									onChange: function (newv, oldv) {
										var d = new Date();
										var quarter = Math.floor((d.getMonth() / 3));
										var quarter1 = quarter + 1;
										if (newv == 2) {
											subviewQ();
											$$("cbQuy").setValue(quarter1);
										}
										if (newv == 1) {
											subviewT();
											$$("cbQuy").setValue(d.getMonth() + 1);
										}
									}
								}
							},
							{
								id: "cbQuy",
								name: "quy",
								view: "combo",
								label: "Tháng",
								//height: 30,
								value: _month,
								options: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
								on: {
									onChange: function () {
										var d = new Date();
										var quarter = Math.floor((d.getMonth() / 3));
										var quarter1 = quarter + 1;

										var option = $$('head_extension').getValue();
										if (option == 2) {
											var nam = d.getFullYear();
											var thang;
											var quy = $$("cbQuy").getValue();
											if (quy == 1) {
												thang = 0;
											}
											if (quy == 2) {
												thang = 3;
											}
											if (quy == 3) {
												thang = 6
											}
											if (quy == 4) {
												thang = 9
											}
											var firstDate = new Date(nam, thang, 1);
											var lastDate = new Date(nam, thang + 3, 0);

											$$(win.config.id + "_tu_ngay").setValue(firstDate);
											$$(win.config.id + "_tu_ngay").refresh();
											$$(win.config.id + "_den_ngay").setValue(lastDate);
											$$(win.config.id + "_den_ngay").refresh();
										}
										if (option == 1) {
											var year = d.getFullYear();
											var month = $$("cbQuy").getValue();
											var date1 = month + "/01" + "/" + year;

											var date = new Date(date1);
											var firstDay1 = new Date(date.getFullYear(), date.getMonth(), 1);

											var lastDay1 = new Date(date.getFullYear(), date.getMonth() + 1, 0);
											$$(win.config.id + "_tu_ngay").setValue(firstDay1);
											$$(win.config.id + "_tu_ngay").refresh();
											$$(win.config.id + "_den_ngay").setValue(lastDay1);
											$$(win.config.id + "_den_ngay").refresh();
										}
									}
								}
							}
						]
					};

					var formValues = {};

					var rows_max = result[0].rows_max;

					var el = [];

					for (var i = 1; i <= rows_max; i++) {
						el.push({ cols: [] });
					}

					for (var i = 0; i < result.length; i++) {

						var field = result[i];

						var row = field.row;

						var item = {
							view: field.type_editor,
							name: field.name,
							label: _(field.caption),
							width: field.width,
							labelPosition: field.label_position,
							labelWidth: field.label_width,
							format: field.format,
							hidden: field.hidden == "C" ? true : false,
							id: win.config.id + "_" + field.name
						};

						if (field.default_value != null) {
							var defaultValue = field.default_value;

							var date = new Date();
							var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
							var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

							if (defaultValue.startsWith("@Default=")) {
								defaultValue = defaultValue.replace("@Default=", "");
								if (defaultValue == "MA_DVCS" || defaultValue == "BRANCH_CODE") {
									item.value = user.getDvcsID();
								}
								else if (defaultValue == "USER") {
									item.value = user.getCurrentUser();
								}
								else if (defaultValue == "MA_CT") {
									item.value = "MA_CT";
								}
								else if (defaultValue == "FIRST_DATE") {
									item.value = firstDay;
								}
								else if (defaultValue == "LAST_DATE") {
									item.value = lastDay;
								}
								else {
									item.value = defaultValue;
								}
							}
						}

						if (item.hidden == true) {
							formValues[item.name] = item.value;
							continue;
						}

						if (item.view == "combo" || item.view == "multiselect") {
							item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.wb_references_id;
						}

						if (item.view == "multicombo"){
							item.suggest= {
								selectAll: true,
								body: {
									url: app.config.host + "/System/GetDataByReferencesId?id=" + field.wb_references_id                                
								}
							};
						}

						if (item.view == "radio") {
							var ajax = webix.ajax();
							ajax.master = {
								radio_id: item.id,
								win_id: win.config.id
							};

							ajax.get(app.config.host + "/System/GetDataByReferencesId?id=" + field.wb_references_id, function (text) {
								var json = JSON.parse(text);
								var radio = $$(this.radio_id);
								radio.define("options", json);
								radio.refresh();

								$$(this.win_id).resize();
							});
						}

						if (item.view == "treesuggest") {

							//item.REF_ID = field.REF_ID.toString();
							var listColumns = JSON.parse(field.list_column);

							for (var v = 0; v < listColumns.length; v++) {
								listColumns[v].header = _(listColumns[v].id);
							}

							item.view = "combo";
							item.options = {
								view: "treesuggest",
								url: {
									$proxy: true,
									load: function (view, callback, params) {
										var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

										webix.ajax(url, function (text) {
											var json = JSON.parse(text);
											var data = listToTree(json);
											view.parse(data, "json");
										});

									}
								},
								body: {
									columns: listColumns,
									refid: field.wb_references_id,
									filterMode: {
										level: false,
										showSubItems: false
									}
								}
							};
						}

						if (item.view == "gridcombo") {

							var listColumns = JSON.parse(field.list_column);

							for (var v = 0; v < listColumns.length; v++) {
								listColumns[v].header = _(listColumns[v].id);
							}

							item.view = "combo";
							item.options = {
								view: "gridsuggest",
								url: app.config.host + "/api/System/GetDataByReferencesId?id=" + field.wb_references_id,
								body: {
									columns: listColumns,
									refid: field.wb_references_id
								}
							};
						}

						if (item.view == "gridsuggest") {
							var listColumns = JSON.parse(field.list_column);

							for (var v = 0; v < listColumns.length; v++) {
								listColumns[v].header = _(listColumns[v].id);
							}

							item.view = "text";
							item.suggest = {
								view: "gridsuggest",
								value_suggest: field.value_suggest,
								body: {
									columns: listColumns,
									refid: field.wb_references_id,
									datatype: "json",
									dataFeed: function (filtervalue, filter) {
										if (filtervalue.length < 3) return;

										var refid = this.config.refid;
										var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
										var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

										this.load(pathUrl + "&" + urldata, this.config.datatype);
									},

								},
								on: {
									onValueSuggest: function (obj) {
										if (this.config.value_suggest != null) {
											var fn = Function("obj", "id", this.config.value_suggest);
											fn(obj, this.config.id);
										}
									}
								}
							};
						}

						if (item.name == "tu_ngay") {
							item.value = firstDay;
						}

						if (item.name == "den_ngay") {
							item.value = lastDay;
						}

						formValues[item.name] = item.value;
						el[row - 1].cols.push(item);
					}

					form.setValues(formValues);

					var buttons = {
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", view: "button", label: "Lưu (F10)",
								width: 120,
								type: "form", align: "center",
								on: {
									onClick: function () {
										var form = this.getParentView().getParentView();
										var win = this.getTopParentView();

										var item = form.getValues();
										// xóa header ngày tháng trong sproc ko cần
										delete item.quy;
										var load_event = win.config.load_event == null ? "loadReport" : win.config.load_event;

										app.callEvent(load_event, [win.config.report_id, win.config.id, item, win.config.menuitem]);

										win.close();
									}
								},
								click: function () {
									this.callEvent("onClick");

								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)",
								width: 120,
								align: "center", type: "danger",
								click: function () {
									this.getTopParentView().close();
								}
							},
							{}
						]
					};

					el.push(buttons);

					webix.ui(el, form);
					if ($$(win.config.id + "_tu_ngay") != undefined && $$(win.config.id + "_den_ngay") != undefined) {
						form.addView(view_extension, 0);
					}
					win.getHead().setHTML(result[0]["window_name"]);

					//win.config.height= result[0]["WINDOW_HEIGHT"];
					//win.config.width= result[0]["WINDOW_WIDTH"];

					if (win.config.editmode != null) {
						if (win.config.editmode == 1) {
							form.setValues(win.config.formvalues);
						}
					}

					//win.resize();

				});
		},
		setEditMode: function (mode) {
			editmode = mode;
		},
		setValues: function (item) {
			var win = $$(this.$ui.id);
			var form = win.getChildViews()[1];

			form.setValues(item);
		}
	};

});
