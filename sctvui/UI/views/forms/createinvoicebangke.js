define(["app", "locale"], function (app, _) {

    function initUI(item) {
        $$('sobangke').setValue(item.list_number);
    }

    return {
        $ui: {
            view: "window",
            id: "crinvoice",
            head: "Tạo hóa đơn từ bảng kê",
            modal: true,
            width: 400,
            position: "center",
            // css: { "font-weight": "bold", "text-transform": "uppercase" },
            body: {
                view: "form", id: "crinvoiceform", elements: [

                    { view: "text", label: _("SO_BANG_KE"), id: "sobangke", name: "list_number", readonly: true, labelWidth: 90 },
                    {
                        view: "richselect",
                        id: "inv_InvoiceCode_id",
                        name: "inv_InvoiceCode_id",
                        label: _("MAU_SO"),
                        labelWidth: 90,
                        //options: app.config.host +"/System/GetAllDmDvcs",
                        options: {
                            view: "gridsuggest",
                            textValue: "ky_hieu",
                            body: {
                                url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00027",
                                textValue: "ky_hieu",
                                columns: [
                                    {
                                        id: "mau_so",
                                        width: 100,
                                        header: _("MAU_SO")
                                    },
                                    {
                                        id: "ky_hieu",
                                        width: 100,
                                        header: _("KY_HIEU")
                                    },
                                    {
                                        id: "ma_loai",
                                        width: 100,
                                        header: _("MA_LOAI")
                                    },
                                    {
                                        id: "ngay_bd_sd",
                                        width: 150,
                                        header: _("NGAY_BD_SD"),
                                        format: function (text) {
                                            var format = webix.Date.dateToStr("%d/%m/%Y");
                                            text = removeTimeZoneT(text);
                                            return format(text);
                                        }
                                    },
                                    {
                                        id: "tu_so",
                                        width: 100,
                                        header: _("TU_SO")
                                    },
                                    {
                                        id: "den_so",
                                        width: 100,
                                        header: _("DEN_SO")
                                    }
                                ]
                            }
                        }
                    },
                    { view: "datepicker", label: _("NGAY_HDON"), id: "ngayhd", name: "ngay_hd", value: new Date(), labelWidth: 90 },
                    {
                        cols: [

                            {
                                view: "button", value: "Đồng ý", type: "form", click: function () {

                                    var item = $$("crinvoiceform").getValues();
                                    console.log(item);

                                    var url = app.config.host + "/Invoice/CreateInvoiceFromList";
                                    webix.ajax()
                                        .headers({ 'Content-type': 'application/json' })
                                        .post(url, item, function (text) {
                                            var data = JSON.parse(text);

                                            if (!data.hasOwnProperty("error")) {
                                                webix.message("Tạo hóa đơn thành công !");
                                                $$("crinvoice").close();
                                            }
                                            else {
                                                webix.message(data.error, "error");
                                            }
                                        });
                                }
                            },
                            {
                                view: "button", hotkey: "esc", value: "Hủy bỏ", click: function () {
                                    this.getTopParentView().hide();
                                }
                            }
                        ]
                    }
                ]
            }
        },
        initUI: initUI
    };

});