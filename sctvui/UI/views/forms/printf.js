define([
    "app",
    "locale"
], function (app, _) {
    'use strict';

    function initUI(url) {

    };

    return {
        $ui: {
            view: "window",
            move: true,
            position: "center",
            id: "nampv",
            modal: true,
            // borderless: false,
            width: 800,
            height: 700,
            //fullscreen: true,
            css: { "box-shadow": "0 -2px #3498db inset !important", "background": "#ffffff !important" },
            head: {
                //view: "toolbar",
                css: { "box-shadow": "0 -2px #3498db inset !important" },
                //height: 15,
                cols: [
                    { gravity: 1, template: "" },
                    {
                        view: "icon", icon: "times", hotkey: "esc", click: function () {
                            $$("nampv").close();
                        }
                    }
                ]
            },
            body: {
                rows: [
                    {
                        view: "iframe", id: "frame-body",css:{"width":"80%", "height":"80%"},
                        src: "https://minvoice.vn/"
                    },
                    //{ gravity: 0.1, template: "" }
                ]
            },
            on: {
                onBeforeShow: function () {
                    var link = $$("nampv").config.url;
                    //$$("myframe").getIframe();
                    $$("frame-body").define("src", link);

                }
            }
        },
        $oninit: function (view, $scope) {

        },
        initUI: initUI
    }
});