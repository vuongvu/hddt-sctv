define(["app", "locale"], function (app, _) {
    // var dialog = webix.ui({
    // 	id: "uploader",
    // 	view: "uploader",
    // 	value: 'Chọn file',
    // 	name: "uploader",
    // 	accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    // 	link: "mylist",
    // 	autosend: false,
    // 	//borderless: true,
    // 	multiple: false,
    // 	upload: app.config.host + "/System/UploadExcel",
    // 	on: {
    // 		onAfterFileAdd: function (file) {
    // 			//console.log(file);
    // 			// var kk = {
    // 			// 	name: file.name,
    // 			// 	lastModified: file.lastModified,
    // 			// 	lastModifiedDate: file.lastModifiedDate,
    // 			// 	webkitRelativePath: file.webkitRelativePath,
    // 			// 	id: file.id,
    // 			// 	size: file.size,
    // 			// 	sizetext: file.sizetext,
    // 			// 	status: file.status,
    // 			// 	type: file.type
    // 			// };
    // 			// $$("mylist").add(kk);

    // 			// console.log($$("mylist").getIdByIndex(0));
    // 			// var id = $$("mylist").getIdByIndex(0);
    // 			// console.log($$("mylist").getItem(id));
    // 			$$("mylist").refresh();
    // 			//$$("mylist").show();

    // 		},
    // 		onUploadComplete: function (response) {
    // 			// var topParentView = this.getTopParentView();

    // 			// if (topParentView.config.tableId !== undefined) {
    // 			// 	var table = $$(topParentView.config.tableId);

    // 			// 	if (table != null) {
    // 			// 		table.clearAll();
    // 			// 		table.load(table.config.url);
    // 			// 	}
    // 			// }
    // 			// webix.message("Upload File thành công !", "debug");
    // 			// webix.$$("uploadfile-win").close();
    // 		},
    // 		onFileUploadError: function (file, response) {
    // 			console.log(file);
    // 			console.log(response);
    // 		}
    // 	},
    // 	apiOnly: true,
    // });

    return {
        $ui: {
            view: "window",
            modal: true,
            id: "uploadfile-win",
            position: "center",
            head: "Nhận dữ liệu từ file Excel",
            width: 500,
            height: 500,
            body: {
                paddingY: 10,
                paddingX: 10,
                elementsConfig: { labelWidth: 100 },
                view: "form",
                id: "uploadfile-form",
                complexData: true,
                elements: [
                    {
                        cols: [
                            {
                                view: "toolbar",
                                css: { background: "#e6f7ff !important" },
                                rows: [
                                    // { gravity: 2 },
                                    {
                                        view: "button",
                                        type: "icon",
                                        icon: "fas fa-folder-open",
                                        label: "Chọn file",
                                        width: 110,
                                        on: {
                                            onClick: function () {
                                                webix.$$("uploader").fileDialog();
                                            },
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        },
                                    },
                                    {
                                        view: "button",
                                        type: "icon",
                                        icon: "fas fa-download",
                                        label: "Tải file mẫu",
                                        width: 110,
                                        on: {
                                            onClick: function () {
                                                var uploader = webix.$$("uploader");
                                                var formData = uploader.config.formData;
                                                var filename = formData["filename"] == null ? "template.xlsx" : formData["filename"];
                                                var table = $$("uploadfile-win");
                                                // webix.extend(table, webix.OverlayBox);
                                                webix.extend(table, webix.ProgressBar);
                                                table.disable();
                                                table.showProgress();
                                                webix
                                                    .ajax()
                                                    .bind({ filename: filename })
                                                    .response("arraybuffer")
                                                    .headers({ "Content-type": "application/json" })
                                                    .get(app.config.host + "/System/DownloadFileExcelTemplate?id=" + formData["excel_id"], function (text, data, XmlHttpRequest) {
                                                        webix.delay(
                                                            function (filename) {
                                                                var file = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                                                                var fileURL = URL.createObjectURL(file);

                                                                var link = document.createElement("a");
                                                                document.body.appendChild(link);
                                                                link.download = filename;
                                                                link.href = fileURL;
                                                                link.click();
                                                                document.body.removeChild(link); // remove the link when done
                                                                //document.location.href = fileURL;
                                                            },
                                                            null,
                                                            [filename],
                                                            500
                                                        );

                                                        table.enable();
                                                        table.hideProgress();
                                                    });
                                            },
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        },
                                    },
                                    {
                                        shortcut: "F10",
                                        view: "button",
                                        type: "icon",
                                        icon: "fas fa-check",
                                        label: "Nhận file (F10)",
                                        width: 120,
                                        on: {
                                            onClick: function () {
                                                var table = $$("uploadfile-win");
                                                // webix.extend(table, webix.OverlayBox);
                                                webix.extend(table, webix.ProgressBar);
                                                table.disable();
                                                table.showProgress();
                                                var file = $$("uploader").files.getFirstId();
                                                if (file == undefined || file == null) {
                                                    webix.message("Bạn chưa chọn file !", "error");
                                                    $$("uploadfile-win").enable();
                                                    $$("uploadfile-win").hideProgress();
                                                    return;
                                                }
                                                // var kk = $$("uploader").files.data.pull;
                                                // console.log(kk);
                                                $$("uploader").send(function (response) {
                                                    var message = response.message || "Nhận dữ liệu Excel thành công !";
                                                    console.log(response);
                                                    console.log(message);
                                                    if (response != undefined) {
                                                        if (!response.hasOwnProperty("error")) {
                                                            $$("uploadfile-win").enable();
                                                            $$("uploadfile-win").hideProgress();
                                                            //this.callEvent("onUploadComplete");
                                                            // success call event onUploadComplete ở trên
                                                            var topParentView = this.getTopParentView();

                                                            if (topParentView.config.callback) {
                                                                topParentView.config.callback();
                                                                return;
                                                            }

                                                            if (topParentView.config.tableId !== undefined) {
                                                                var table = $$(topParentView.config.tableId);

                                                                if (table != null && table.config.url != undefined) {
                                                                    table.clearAll();
                                                                    table.load(table.config.url);
                                                                }
                                                            }

                                                            webix.alert({
                                                                title: "Thành công !",
                                                                text: "<div class='v-message'>" + message + "</div>",
                                                                type: "alert-warning",
                                                                callback: function (result) {
                                                                    webix.$$("uploadfile-win").close();
                                                                },
                                                            });

                                                            //webix.message("Nhận dữ liệu Excel thành công !", "debug");
                                                        } else {
                                                            $$("uploadfile-win").enable();
                                                            $$("uploadfile-win").hideProgress();

                                                            webix.alert({
                                                                title: "Lỗi !",
                                                                text: response.error,
                                                                type: "alert-error",
                                                                callback: function (result) {
                                                                    webix.$$("uploadfile-win").close();
                                                                },
                                                                // callback: function (result) {
                                                                // 	webix.alert({
                                                                // 		title: "Custom title",
                                                                // 		ok: "Custom text",
                                                                // 		text: "Result: " + result
                                                                // 	});
                                                                // }
                                                            });
                                                            //webix.message(response.error, "error");
                                                        }
                                                    } else {
                                                        $$("uploadfile-win").enable();
                                                        $$("uploadfile-win").hideProgress();
                                                    }
                                                });
                                            },
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        },
                                    },
                                    {
                                        hotkey: "esc",
                                        view: "button",
                                        type: "icon",
                                        icon: "fas fa-times",
                                        label: "Hủy bỏ (ESC)",
                                        width: 110,
                                        on: {
                                            onClick: function () {
                                                $$("mylist").clearAll();
                                                $$("mylist").unbind();
                                                var id = $$("uploader").files.getFirstId();
                                                if (id != undefined) {
                                                    $$("uploader").stopUpload(id);
                                                }
                                                webix.$$("uploadfile-win").close();
                                            },
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        },
                                    },
                                ],
                            },
                            {
                                rows: [
                                    // {view:"text",id:"df",label:"errr"},
                                    {
                                        id: "uploader",
                                        view: "uploader",
                                        value: "Chọn file",
                                        hide: true,
                                        name: "uploader",
                                        accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                        link: "mylist",
                                        autosend: false,
                                        //borderless: true,
                                        multiple: false,
                                        upload: app.config.host + "/System/UploadExcel",
                                        on: {
                                            onAfterRender: function () {
                                                //console.log('called once after first rendering:');
                                                this.hide();
                                            },
                                            onAfterFileAdd: function (file) {
                                                var fileName = file.name;
                                                if (!fileName.endsWith(".xls") && !fileName.endsWith(".xlsx")) {
                                                    webix.alert({
                                                        title: "Lỗi !",
                                                        text: "File không đúng định dạng Excel!",
                                                        type: "alert-error",
                                                        callback: function () {
                                                            $$("uploader").files.data.clearAll();
                                                        },
                                                    });
                                                }
                                                //console.log(file);
                                                // var kk = {
                                                // 	name: file.name,
                                                // 	lastModified: file.lastModified,
                                                // 	lastModifiedDate: file.lastModifiedDate,
                                                // 	webkitRelativePath: file.webkitRelativePath,
                                                // 	id: file.id,
                                                // 	size: file.size,
                                                // 	sizetext: file.sizetext,
                                                // 	status: file.status,
                                                // 	type: file.type
                                                // };
                                                // $$("mylist").add(kk);

                                                // console.log($$("mylist").getIdByIndex(0));
                                                // var id = $$("mylist").getIdByIndex(0);
                                                // console.log($$("mylist").getItem(id));
                                                $$("mylist").refresh();
                                                //$$("mylist").show();
                                            },
                                            onUploadComplete: function (response) {
                                                // var topParentView = this.getTopParentView();

                                                // if (topParentView.config.tableId !== undefined) {
                                                // 	var table = $$(topParentView.config.tableId);

                                                // 	if (table != null) {
                                                // 		table.clearAll();
                                                // 		table.load(table.config.url);
                                                // 	}
                                                // }
                                                // webix.message("Upload File thành công !", "debug");
                                                // webix.$$("uploadfile-win").close();

                                                if (response === undefined) return;

                                                if (response.hasOwnProperty("error")) {
                                                    webix.message({ type: "error", text: response.error });
                                                }
                                            },
                                            onFileUploadError: function (file, response) {
                                                console.log(file);
                                                console.log(response);
                                                $$("uploadfile-win").enable();
                                                $$("uploadfile-win").hideProgress();
                                            },
                                        },
                                        apiOnly: true,
                                    },
                                    {
                                        view: "list",
                                        id: "mylist",
                                        type: "uploader",
                                        height: 100,
                                        //autoheight: true
                                        borderless: true,
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        },
    };
});
