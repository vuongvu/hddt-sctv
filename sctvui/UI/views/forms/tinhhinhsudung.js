define(["app", "locale", "models/user"], function (app, _, user) {

    function initUI(item) {
        if (item.tokhai_thang == 1) {
            $$('kybc').setValue("Kỳ báo cáo: Tháng " + moment(item.tu_ngay).format("MM") + " năm " + moment(item.den_ngay).format("YYYY"));
        }
        if (item.tokhai_quy == 1) {
            $$('kybc').setValue("Kỳ báo cáo: Quý " + (Math.floor((item.tu_ngay.getMonth() / 3)) + 1) + " năm " + moment(item.den_ngay).format("YYYY"));
        }
        $$('tungay').setValue("( từ " + moment(item.tu_ngay).format("DD/MM/YYYY") + " đến " + moment(item.den_ngay).format("DD/MM/YYYY") + " )");
        $$('zmau312').setValue(item.mau_312);
        $$('zmau310').setValue(item.mau_310);
        $$('dataform').config.item = item;

        webix.ajax()
            .headers({ 'Content-type': 'application/json' })
            .post(app.config.host + "/Bcthsd/Check_xulyHD", item, function (text) {
                var data = JSON.parse(text);

                if (!data.hasOwnProperty("error")) {
                    $$('tccanhan').setValue("[02] Tên tổ chức, cá nhân " + data.ten_dvcs);
                    $$('mst').setValue("[03] Mã số thuế " + data.ms_thue);
                }
                else {
                    webix.message(data.error, "error");
                }
            });

        webix.ajax()
            .headers({ 'Content-type': 'application/json' })
            .get(app.config.host + "/Bcthsd/GetInfoDonvi", function (text) {
                var data = JSON.parse(text);
                //console.log(data);
                if (!data.hasOwnProperty("error")) {
                    $$('nguoi_dai_dien').setValue(data.nguoi_dai_dien);
                    $$('nguoi_lap').setValue(data.nguoi_lap);
                    // $$('mst').setValue("[03] Mã số thuế " + data.MS_THUE);
                }
                else {
                    webix.message(data.error, "error");
                }
            });

        // webix.ajax()
        //     .headers({ 'Content-type': 'application/json' })
        //     .get(app.config.host + "/System/ExecuteQuery?sql=SELECT * FROM dmdvcs", function (text) {
        //         var data = JSON.parse(text);
        //         if (!data.hasOwnProperty("error")) {
        //             $$('nguoi_dai_dien').setValue(data.nguoi_dai_dien);
        //             // $$('mst').setValue("[03] Mã số thuế " + data.MS_THUE);
        //         }
        //         else {
        //             webix.message(data.error, "error");
        //         }
        //     });

        webix.ajax()
            .headers({ "Conten-type": "application/json" })
            .get(app.config.host + "/Bcthsd/GetTenFile", function (text) {
                var data = JSON.parse(text);
                if (item.tokhai_thang == 1) {
                    var filename = data.name + "-T" + item.thang + item.nam + "-L00";
                    $$('tenfile').setValue(filename);
                    return;
                }
                if (item.tokhai_quy == 1) {
                    var filename = data.name + "-Q" + item.quy + item.nam + "-L00";
                    $$('tenfile').setValue(filename);
                    return;
                }

            })
    }

    return {
        $ui: {
            view: "window",
            fullscreen: true,
            modal: true, id: "thsd-win", position: "center",
            head: {
                //view: "toolbar",
                // css: { "box-shadow": "0 -2px #3498db inset !important", "background": "#ffffff !important" },
                //css: { "background": "#e6f2ff !important" },
                // height: 25,
                cols: [
                    { gravity: 1, template: "" },
                    {
                        view: "icon", icon: "times", hotkey: "esc", click: function () {
                            $$("thsd-win").close();
                        }
                    }
                ]
            },
            //height:420,		
            body: {
                paddingY: 10,
                paddingX: 10,
                view: "form",
                id: "thsd-form",
                complexData: true,
                margin: 0,
                elements: [
                    {
                        rows: [
                            {
                                view: "label",
                                id: "dataform",
                                hidden: true
                            },
                            {
                                view: "label",
                                id: "tenfile",
                                hidden: true
                            },
                            {
                                view: "label",
                                label: "BÁO CÁO TÌNH HÌNH SỬ DỤNG HÓA ĐƠN (BC26/AC)",
                                css: { "font-weight": "bold", "font-size": "15px" },
                                inputWidth: 100,
                                align: "center"
                            },
                            {
                                view: "label",
                                id: "kybc",
                                name: "kybc",
                                label: "Kỳ báo cáo: Tháng ",
                                inputWidth: 100,
                                align: "center"

                            },
                            {
                                view: "label",
                                label: "Từ ngày: ",
                                id: "tungay",
                                inputWidth: 100,
                                align: "center"
                            },
                            {
                                view: "label",
                                label: "[02] Tên tổ chức, cá nhân ",
                                id: "tccanhan",
                                inputWidth: 100,
                                align: "left"
                            },
                            {
                                view: "label",
                                label: "[03] Mã số thuế ",
                                id: "mst",
                                inputWidth: 100,
                                align: "left"
                            },
                            {
                                cols: [
                                    {
                                        view: "checkbox",
                                        id: "zmau312",
                                        name: "zmau_312",
                                        label: "Mẫu 3.12 (Kỳ báo cáo cuối cùng)",
                                        labelWidth: 200
                                    },
                                    {
                                        view: "checkbox",
                                        id: "zmau310",
                                        name: "zmau_310",
                                        label: "Mẫu 3.10 (Chuyển địa điểm)",
                                        labelWidth: 200
                                    }
                                ]

                            },
                            {
                                id: "dataBC",
                                view: "datatable",
                                editable: true,
                                editaction: "dblclick",
                                select: true,
                                datafetch: 2,
                                loadahead: 15,
                                drag: true,
                                fixedRowHeight: false, rowLineHeight: 25, rowHeight: 25,
                                on: {
                                    onColumnResize: function () {
                                        this.adjustRowHeight("so1", true);
                                        this.render();
                                    }
                                },
                                columns: [
                                    { id: "stt", header: "<center>STT</center>", width: 50 },
                                    { id: "code", header: "<center>Mã loại hóa đơn</center>", width: 100 },
                                    { id: "name", header: "<center>Tên loại hóa đơn</center>", width: 160 },
                                    { id: "mau_hd", header: "<center>Mẫu hóa đơn</center>", width: 120 },
                                    { id: "ky_hieu", header: "<center>Ký hiệu</center>", width: 100 },
                                    {
                                        id: "ton_dau_ky", name: "namdaika", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "ton_dau",
                                                groupText: "Tổng số tồn đầu kỳ", colspan: 1
                                            },
                                            "Tổng số "], width: 150
                                    },
                                    {
                                        id: "tu_so1", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr1",
                                                groupText: "Số tồn đầu kỳ", colspan: 2
                                            },
                                            "Từ số"]
                                    },
                                    { id: "den_so1", batch: "gr1", header: [null, "Đến số"] },
                                    {
                                        id: "tu_so2", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr2",
                                                groupText: "Số mua/phát hành trong kỳ", colspan: 2
                                            },
                                            "Từ số"]
                                    },
                                    { id: "den_so2", batch: "gr2", header: [null, "Đến số"] },
                                    {
                                        id: "tu_so3", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr3",
                                                groupText: "Tổng số sử dụng, xóa bỏ, mất, hủy", colspan: 3
                                            },
                                            "Từ số"]
                                    },
                                    { id: "den_so3", batch: "gr3", header: [null, "Đến số"] },
                                    { id: "cong", batch: "gr3", header: [null, "Cộng"] },
                                    {
                                        id: "sl_sd", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr4",
                                                groupText: "Đã sử dụng", colspan: 1
                                            },
                                            "Số lượng"], width: 150
                                    },
                                    {
                                        id: "so_luong1", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr5",
                                                groupText: "Xóa bỏ", colspan: 2
                                            },
                                            "Số lượng"], width: 110
                                    },
                                    { id: "so1", batch: "gr5", header: [null, "Số"] },
                                    {
                                        id: "so_luong2", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr6",
                                                groupText: "Mất", colspan: 2
                                            },
                                            "Số lượng"], width: 110
                                    },
                                    { id: "so2", batch: "gr6", header: [null, "Số"] },
                                    {
                                        id: "so_luong3", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr7",
                                                groupText: "Hủy", colspan: 2
                                            },
                                            "Số lượng"], width: 110
                                    },
                                    { id: "so3", batch: "gr7", header: [null, "Số"] },
                                    {
                                        id: "tu_so4", header: [
                                            {
                                                content: "columnGroup", closed: false, batch: "gr8",
                                                groupText: "Tồn cuối kỳ", colspan: 3
                                            },
                                            "Từ số"]
                                    },
                                    { id: "den_so4", batch: "gr8", header: [null, "Đến số"] },
                                    { id: "so_luong4", batch: "gr8", header: [null, "Số lượng"] }

                                ]
                            },
                            {
                                view: "fieldset",
                                label: "Thông tin đơn vị",
                                body: {
                                    rows: [
                                        {
                                            view: "text",
                                            id: "nguoi_dai_dien",
                                            name: "nguoi_dai_dien",
                                            //labelPosition: "top",
                                            label: "Người đại diện",
                                            css: "mcls_readonly_text",
                                            labelWidth: 100,
                                            readonly: true,
                                            width: 500
                                        },
                                        {
                                            cols: [
                                                {
                                                    id: "nguoi_lap",
                                                    view: "text",
                                                    name: "nguoi_lap",
                                                    //labelPosition: "top",
                                                    css: "mcls_readonly_text",
                                                    labelWidth: 100,
                                                    label: "Người lập",
                                                    readonly: true,
                                                    width: 500
                                                },
                                                {
                                                    id: "ngay_lap",
                                                    view: "datepicker",
                                                    name: "ngay_lap",
                                                    css: "mcls_readonly_text",
                                                    value: new Date(),
                                                    label: "Ngày lập",
                                                    readonly: true,
                                                    width: 500
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                            {
                                margin: 10,
                                cols: [
                                    {},
                                    {
                                        shortcut: "F11", view: "button", icon: "refresh", type: "form", label: "Tải dữ liệu (F11)", align: "center", width: 120,
                                        on: {
                                            onClick: function () {

                                                webix.ajax()
                                                    .headers({ 'Content-type': 'application/json' })
                                                    .post(app.config.host + "/Bcthsd/BcTinhhinhsudung", $$('dataform').config.item, function (text) {
                                                        var data = JSON.parse(text);
                                                        //console.log(data);
                                                        var table = webix.$$("dataBC");
                                                        table.clearAll();
                                                        table.parse(data);
                                                    });
                                            }
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        }
                                    },
                                    {
                                        shortcut: "F10", id: "btnGhi", view: "button", icon: "refresh", label: "Lưu (F10)", type: "danger", align: "center", width: 120,
                                        on: {
                                            onClick: function () {
                                                var ss = $$("dataBC").serialize();

                                                var dataBC = {
                                                    data: ss,
                                                    nguoi_lap: $$('nguoi_lap').getValue(),
                                                    nguoi_dai_dien: $$('nguoi_dai_dien').getValue(),
                                                    ngay_lap: $$('ngay_lap').getValue()
                                                };

                                                webix.ajax()
                                                    .headers({ 'Content-type': 'application/json' })
                                                    .post(app.config.host + "/Bcthsd/bcthsd_INSERT", dataBC, function (text) {
                                                        var data = JSON.parse(text);
                                                        if (data.hasOwnProperty("error")) {
                                                            webix.message(data.error);
                                                        }
                                                        else {
                                                            webix.message(data.ok, "debug");
                                                        }
                                                    });
                                            }
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        }
                                    },
                                    {
                                        shortcut: "F9", view: "button", type: "form", icon: "refresh", label: "Export XML (F9)", align: "center", width: 120,
                                        on: {
                                            onClick: function () {

                                                webix.confirm({
                                                    text: "Bạn có muốn ghi báo cáo trước khi kết suất XML không ?", ok: "Có", cancel: "Không",
                                                    callback: function (res) {
                                                        if (res) {
                                                            var ss = $$("dataBC").serialize();

                                                            var dataBC = {
                                                                data: ss,
                                                                nguoi_lap: $$('nguoi_lap').getValue(),
                                                                nguoi_dai_dien: $$('nguoi_dai_dien').getValue(),
                                                                ngay_lap: $$('ngay_lap').getValue()
                                                            };
                                                            var box = webix.modalbox({
                                                                title: "Đang kết suất XML ....",
                                                                text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                                                width: 350,
                                                                height: 200
                                                            });
                                                            webix.ajax()
                                                                .headers({ 'Content-type': 'application/json' })
                                                                .post(app.config.host + "/Bcthsd/bcthsd_INSERT", dataBC, function (text) {
                                                                    var data = JSON.parse(text);
                                                                    if (data.hasOwnProperty("error")) {
                                                                        webix.modalbox.hide(box);
                                                                        webix.message(data.error, "error");
                                                                    }
                                                                    else {
                                                                        var item = $$('dataform').config.item;
                                                                        // console.log(item);
                                                                        var input = {
                                                                            ma_dvcs: user.getDvcsID(),
                                                                            tokhai_quy: item.tokhai_quy,
                                                                            tokhai_thang: item.tokhai_thang,
                                                                            nam: item.nam,
                                                                            thang: item.thang,
                                                                            tu_ngay: item.tu_ngay,
                                                                            den_ngay: item.den_ngay
                                                                        }

                                                                        webix.ajax()
                                                                            .response("arraybuffer")
                                                                            .get(app.config.host + "/Bcthsd/ExportFileXML", input, function (text, data) {
                                                                                var file = new Blob([data], { type: 'application/xml' });
                                                                                webix.html.download(file, $$('tenfile').getValue());
                                                                                webix.modalbox.hide(box);
                                                                            });
                                                                    }
                                                                });

                                                        }
                                                        else {
                                                            var item = $$('dataform').config.item;
                                                            // console.log(item);
                                                            var input = {
                                                                ma_dvcs: user.getDvcsID(),
                                                                tokhai_quy: item.tokhai_quy,
                                                                tokhai_thang: item.tokhai_thang,
                                                                nam: item.nam,
                                                                thang: item.thang,
                                                                tu_ngay: item.tu_ngay,
                                                                den_ngay: item.den_ngay
                                                            }
                                                            var box = webix.modalbox({
                                                                title: "Đang kết suất XML ....",
                                                                text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                                                width: 350,
                                                                height: 200
                                                            });
                                                            webix.ajax()
                                                                .response("arraybuffer")
                                                                .get(app.config.host + "/Bcthsd/ExportFileXML", input, function (text, data) {
                                                                    var file = new Blob([data], { type: 'application/xml' });
                                                                    webix.html.download(file, $$('tenfile').getValue());
                                                                    webix.modalbox.hide(box);
                                                                });
                                                        }
                                                    }
                                                });
                                            }
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        }
                                    },
                                    {
                                        shortcut: "F8", view: "button", type: "form", icon: "far fa-refresh", label: "Print (F8)", align: "center", width: 120,
                                        on: {
                                            onClick: function () {
                                                var item = $$('dataform').config.item;
                                                // console.log(item);
                                                var input = {
                                                    ma_dvcs: user.getDvcsID(),
                                                    bc_soluong: item.bc_soluong,
                                                    nam: item.nam,
                                                    thang: item.thang,
                                                    tokhai_quy: item.tokhai_quy,
                                                    tokhai_thang: item.tokhai_thang
                                                }
                                                var box = webix.modalbox({
                                                    title: "Đang in báo cáo ....",
                                                    text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                                    width: 350,
                                                    height: 200
                                                });
                                                webix.ajax()
                                                    .response("arraybuffer")
                                                    .get(app.config.host + "/Bcthsd/PrintPDF?ma_dvcs=" + user.getDvcsID() + "&bc_soluong=" + item.bc_soluong + "&nam=" + item.nam + "&thang=" + item.thang + "&tokhai_quy=" + item.tokhai_quy + "&tokhai_thang=" + item.tokhai_thang, function (text, data) {
                                                        var file = new Blob([data], { type: 'application/pdf' });
                                                        var fileURL = URL.createObjectURL(file);
                                                        webix.modalbox.hide(box);
                                                        //  var printWindow = window.open(fileURL, '', 'width=800,height=500');
                                                        //           printWindow.print()
                                                        // var a = document.createElement("a");
                                                        // a.target = '_blank';
                                                        // document.body.appendChild(a);

                                                        // a.href = fileURL;
                                                        // a.download = "fileName.pdf";
                                                        // a.click();
                                                        //$$("frame-body").define("src", fileURL);
                                                        //kk.show();
                                                        window.open(fileURL, '_blank');

                                                        // webix.html.download(file, $$('tenfile').getValue());
                                                    });
                                            }
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        }
                                    },
                                    {}
                                    /*{
                                        view: "button", hotkey: "esc", label: "Hủy bỏ", align: "center", width: 120,
                                        click: function () {
                                            $$("tinhhinhsudung").removeView("dataBC");
                                            // webix.$$("tinhhinhsudung").close();
                                        }
                                    }*/
                                ]
                            }
                        ]
                    }
                ]
            }

        },
        initUI: initUI
    };

});