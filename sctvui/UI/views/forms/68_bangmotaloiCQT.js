define(["app", "locale", "models/user", "views/forms/68_invoiceForms"], function (app, _, user, invoiceForms) {
    "use strict";

    var editmode = 0;
    var hdon_id = "";

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            css: "headformchil",
            head: {
                height: 35,
                cols: [
                    {
                        id: "head-icondksd",
                        view: "template",
                        css: "headform",
                        width: 35,
                        template: "",
                    },
                    {
                        id: "head-formdksd",
                        view: "template",
                        css: "headform",
                        template: function (obj) {
                            return "<div>Danh sách lỗi phản hồi từ CQT<div/>";
                        },
                    },
                ],
            },
            id: "responseMessageForms1",
            resize: true,
            width: 800,
            height: 400,
            move: true,
            on: {
                onBeforeShow: function () {},
            },
            body: {
                view: "form",
                id: "message",
                // width: 1500,
                position: "center",
                // autowidth:true,
                paddingY: 10,
                paddingX: 15,
                complexData: true,
                scroll: true,
                margin: 0,
                elements: [
                    {
                        // id: "header",
                        template: "<center><b><span style='font-size:17px;'>DANH SÁCH LỖI PHẢN HỒI TỪ CQT</b></center>",
                        borderless: true,
                        height: 40,
                    },
                    // {
                    //     paddingX: 30,
                    //     rows: [
                    //         {
                    //             view: "text",
                    //             label: "Ký hiệu",
                    //             id: "khieu",
                    //             name: "khieu",
                    //         },
                    //         {
                    //             view: "text",
                    //             label: "Số hóa đơn",
                    //             labelWidth: 150,
                    //             id: "shdon",
                    //             name: "shdon",
                    //         },
                    //     ],
                    // },
                    { height: 3 },
                    {
                        rows: [
                            {
                                view: "datatable",
                                id: "danhsach_message",
                                // height: 250,
                                //name: "danhsach_message",
                                // select: true,
                                resizeColumn: true,
                                footer: false,
                                tooltip: true,
                                editable: true,
                                select: "cell",
                                scheme: {
                                    $init: function (obj) {},
                                },
                                url:{
                                    $proxy: true,
                                    load: function (view, callback, params) {
                                        debugger
                                        // var hdon_id = "87de11a4-0b4e-42d4-a86c-45ba10b80337";
                                        if(hdon_id != ""){
                                            var url = app.config.host + "/Invoice68/GetResponseMessage?id=" + hdon_id;
                                            webix.ajax(url, callback, view);
                                        }
                                    }
                                },
                                on: {
                                    "data->onStoreUpdated": function () {
                                        this.data.each(function (obj, i) {
                                            obj.stt = i + 1;
                                        });
                                    },
                                },
                                columns: [
                                    { editor: "text", id: "hdon_id", name: "hdon_id", hidden: true },
                                    {
                                        id: "stt",
                                        name: "stt",
                                        header: "STT",
                                        sort: "int",
                                        width: 40,
                                    },
                                    {
                                        id: "maloi",
                                        name: "maloi",
                                        header: "Mã lỗi",
                                        width: 150,
                                        editor: "text",
                                    },
                                    {
                                        id: "motaloi",
                                        name: "motaloi",
                                        header: "Mô tả lỗi",
                                        width: 300,
                                        editor: "text",
                                    },
                                    {
                                        id: "huongdanxuly",
                                        name: "huongdanxuly",
                                        header: "Hướng dẫn xử lý",
                                        width: 300,
                                        editor: "text",
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        id: "btnHuyHDDT",
                        cols: [
                            {},
                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy bỏ (ESC)",
                                width: 120,
                                type: "danger",
                                align: "center",
                                click: function () {
                                    hdon_id = "";
                                    webix.$$("responseMessageForms1").close();
                                },
                            },
                            {},
                        ],
                    },
                ],
            },
        },
        setEditMode: function (id) {
            hdon_id = id;
            var table = $$("danhsach_message");
            table.load(table.config.url);
        },
        setValues: function (item, winconfig, params) {
            var table = $$("danhsach_message");
            table.setValues(item);
        },
    };
});
