define(["require", "locale", "app"], function (require, _, app) {
    "use strict";

    var layout = {
        view: "window",
        id: "formConfirm",
        head: "Đăng nhập",
        modal: true,
        position: "center",
        // css: { "font-weight": "bold", "text-transform": "uppercase" },
        body: {
            view: "form",
            id: "formConfirmValid",
            // elementsConfig: {
            //     labelPosition: "top",
            //     //validateEvent: "key"
            // },
            elements: [
                {
                    view: "text",
                    label: _("Account"),
                    id: "1",
                    name: "taikhoan",
                    width: 350,
                    invalidMessage: "Tài khoản không được để trống !",
                    required: true,
                    css: "mcls_uppercase",
                },
                {
                    view: "text",
                    label: _("PASSWORD"),
                    id: "pass",
                    name: "matkhau",
                    type: "password",
                    width: 350,
                    invalidMessage: "Mật khẩu không được để trống !",
                    required: true,
                },
                // {
                //     view: "label",
                //     name: "lb1",
                //     id: "lb",
                //     template: "<hr>"
                // },
                {
                    view: "button",
                    css: "mcls_btnRegisterLogin",
                    type: "iconButton",
                    icon: "long-arrow-right",
                    label: _("Phần mềm hóa đơn điện tử SCTV"),
                    width: 250,
                    click: function () {
                        window.open("http://minvoice.vn/dang-ky-su-dung/", "_blank");
                    },
                },
                {
                    cols: [
                        {
                            view: "button",
                            value: "Đồng ý",
                            type: "danger",
                            click: function () {
                                var model = {
                                    username: $$("1").getValue(),
                                    password: $$("pass").getValue(),
                                };
                                if ($$("formConfirmValid").validate()) {
                                    var box = webix.modalbox({
                                        title: "Đang cập nhật phần mềm ....",
                                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                        width: 350,
                                        height: 200,
                                    });
                                    var urlCheck = app.config.host + "/System/CheckPassUpdate";

                                    var url = app.config.host + "/System/UpdateToWeb";
                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(urlCheck, model, function (text) {
                                            var data = JSON.parse(text);

                                            if (!data.hasOwnProperty("error")) {
                                                webix
                                                    .ajax()
                                                    .headers({ "Content-type": "application/json" })
                                                    .post(url, {
                                                        error: function (text, data, XmlHttpRequest) {
                                                            webix.modalbox.hide(box);
                                                            webix.message(XmlHttpRequest.statusText, "error");
                                                        },
                                                        success: function (text, data, XmlHttpRequest) {
                                                            //console.log(text);
                                                            var data = JSON.parse(text);

                                                            if (data.error == "" || data.error == null) {
                                                                webix.modalbox.hide(box);
                                                                webix.message(data.ok, "debug");

                                                                webix.$$("formConfirm").close();
                                                            } else {
                                                                webix.$$("formConfirm").close();
                                                                webix.modalbox.hide(box);
                                                                webix.message(data.error, "error");
                                                            }
                                                        },
                                                    });
                                            } else {
                                                webix.modalbox.hide(box);
                                                webix.message(data.error, "error");
                                            }
                                        });
                                    //this.getTopParentView().hide();
                                }
                            },
                        },
                        {
                            view: "button",
                            hotkey: "esc",
                            label: "Hủy bỏ",
                            type: "iconButton",
                            icon: "times",
                            click: function () {
                                //this.getFormView().save();
                                $$("formConfirm").close();
                                //this.getTopParentView().hide();
                            },
                        },
                    ],
                },
            ],
        },
    };

    return {
        $ui: layout,
        $oninit: function () {
            $$("formConfirmValid").focus();
        },
    };
});
