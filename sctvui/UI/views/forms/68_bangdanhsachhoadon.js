define(["app", "models/user", "views/forms/68_bangtonghopform"], function (app, user, bangtonghopform) {
    var editmode = 0;
    var loaihanghoa = '2';
    var params;

    var fm = webix.Number.numToStr({
        groupDelimiter: ",",
        groupSize: 3,
        decimalDelimiter: ".",
        decimalSize: 0,
    });
    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Nhận (F10)",
            type: "form",
            align: "center",
            width: 120,
            on: {
                onClick: function () {
                    var table = webix.$$("danhsach_hoadonFrm");
                    var data = table.serialize();
                    var lstTemp = [];
                    var putData = "";
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        if (item.chon != undefined) {
                            if (item.chon == "C") {
                                //item.mstnmua = item["mst"];
                                lstTemp.push(item);
                                putData += "'" + item.hdon_id + "',";
                            }
                        }
                    }

                    putData = putData.substring(0, putData.length - 1);

                    if (lstTemp.length == 0) {
                        webix.message({ type: "error", text: "Bạn chưa chọn hóa đơn" });
                        return;
                    }

                    var params = {};
                    params["data"] = putData;
                    var lhhoa = $$("bangtonghop-form").getValues().lhhoa;

                    if (lhhoa === "1") {
                        webix
                            .ajax()
                            .headers({ "Content-type": "application/json" })
                            .post(app.config.host + "/Invoice68/GetDetailHoaDonXangDauChoXuLy", params, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.message(text + XmlHttpRequest, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var json = JSON.parse(text);
    
                                    if (!json.hasOwnProperty("error")) {
                                        if(json[0].shdon != null){
                                            var errorMessenger = "";
                                            for (var index = 0; index < json.length; index++) {
                                                if (index == (json.length - 1)) {
                                                    errorMessenger += json[index].shdon
                                                } else {
                                                    errorMessenger += json[index].shdon + ", ";
                                                }
                                            }
                                            webix.message("Các số hoá đơn sau chưa nhập đơn vị tính của hàng hoá: " + errorMessenger, "error");
                                            return;
                                        }
                                        var frm = webix.$$("danhsach_hoadon");
                                        frm.clearAll();
                                        frm.parse(json);
    
                                        var tien_tt = 0;
                                        var tien_thue = 0;
                                        var tong_tien = 0;
    
                                        for (var i = 0; i < json.length; i++) {
                                            var item = json[i];
                                            tien_tt += item["ttcthue"] == null ? 0 : item["ttcthue"];
                                            tien_thue += item["tgtthue"] == null ? 0 : item["tgtthue"];
                                            tong_tien += item["tgtttoan"] == null ? 0 : item["tgtttoan"];
                                        }
    
                                        $$("tgtcthue").setValue(fm(tien_tt));
                                        $$("tgtthue").setValue(fm(tien_thue));
                                        $$("tgtttoan").setValue(fm(tong_tien));
    
                                        $$("danhsachhoadon-win").close();
                                    } else {
                                        webix.message(json.error, "error");
                                    }
                                },
                            });
                    } else {
                        webix
                            .ajax()
                            .headers({ "Content-type": "application/json" })
                            .post(app.config.host + "/Invoice68/GetDetailHoadonChoXuLy", params, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.message(text + XmlHttpRequest, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var json = JSON.parse(text);
    
                                    if (!json.hasOwnProperty("error")) {
                                        var frm = webix.$$("danhsach_hoadon");
                                        frm.clearAll();
                                        frm.parse(json);
    
                                        var tien_tt = 0;
                                        var tien_thue = 0;
                                        var tong_tien = 0;
    
                                        for (var i = 0; i < json.length; i++) {
                                            var item = json[i];
                                            tien_tt += item["ttcthue"] == null ? 0 : item["ttcthue"];
                                            tien_thue += item["tgtthue"] == null ? 0 : item["tgtthue"];
                                            tong_tien += item["tgtttoan"] == null ? 0 : item["tgtttoan"];
                                        }
    
                                        $$("tgtcthue").setValue(fm(tien_tt));
                                        $$("tgtthue").setValue(fm(tien_thue));
                                        $$("tgtttoan").setValue(fm(tong_tien));
    
                                        $$("danhsachhoadon-win").close();
                                    } else {
                                        webix.message(json.error, "error");
                                    }
                                },
                            });
                    }

                    // var frm = webix.$$("danhsach_hoadon");
                    // frm.clearAll();
                    // frm.parse(lstTemp);
                    // $$("tgtcthue").setValue(fm(tien_tt));
                    // $$("tgtthue").setValue(fm(tien_thue));
                    // $$("tgtttoan").setValue(fm(tong_tien));
                    // $$("danhsachhoadon-win").close();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            hotkey: "esc",
            label: "Hủy (ESC)",
            type: "danger",
            align: "center",
            width: 120,
            click: function () {
                webix.$$("danhsachhoadon-win").close();
            },
        },
        {},
    ];

    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "danhsachhoadon-win",
        position: "center",
        head: "Chọn hóa đơn vào bảng tổng hợp dữ liệu điện tử",
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            minWidth: 900,
            id: "danhsachhoadon-form",
            complexData: true,
            margin: 0,
            elements: [
                {
                    view: "datatable",
                    id: "danhsach_hoadonFrm",
                    name: "danhsach_hoadonFrm",
                    select: true,
                    resizeColumn: true,
                    footer: false,
                    editable: false,
                    tooltip: true,
                    minHeight: 400,
                    scheme: {
                        $init: function (obj) {},
                    },
                    on: {},
                    columns: [
                        {
                            id: "chon",
                            header: [{ text: "", content: "masterCheckbox" }],
                            width: 55,
                            checkValue: "C",
                            uncheckValue: "K",
                            template: "{common.checkbox()}",
                        },
                        {
                            id: "nky",
                            name: "nky",
                            header: ["Ngày giờ ký", { content: "datepickerFilter" }],
                            width: 120,
                            format: function (text) {
                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                text = removeTimeZoneT(text);
                                return format(text);
                            },
                        },
                        {
                            id: "khieu",
                            name: "khieu",
                            header: [
                                "Ký hiệu",
                                {
                                    content: "textFilter",
                                },
                            ],
                            width: 120,
                        },
                        {
                            id: "shdon",
                            name: "shdon",
                            header: ["Số hóa đơn", { content: "textFilter" }],
                            width: 120,
                        },
                        {
                            id: "mst",
                            name: "mst",
                            header: ["Mã số thuế", { content: "textFilter" }],
                            width: 150,
                        },
                        {
                            id: "ten",
                            name: "ten",
                            header: [
                                "Tên đơn vị/Tên người mua",
                                {
                                    content: "textFilter",
                                },
                            ],
                            width: 350,
                        },

                        {
                            id: "tgtttbso",
                            name: "tgtttbso",
                            header: ["Thành tiền sau thuế", { content: "textFilter" }],
                            width: 150,
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 0,
                            }),
                            css: {
                                "text-align": "right",
                            },
                        },
                        {
                            id: "tdlap",
                            name: "tdlap",
                            header: ["Thời điểm lập", { content: "datepickerFilter" }],
                            width: 120,
                            format: function (text) {
                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                text = removeTimeZoneT(text);
                                return format(text);
                            },
                        },
                        {
                            id: "nglap",
                            name: "nglap",
                            header: "Người lập",
                            header: ["Người lập", { content: "textFilter" }],
                            width: 150,
                        },
                    ],
                },
                {
                    margin: 10,
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },
        setParams: function (value) {
            params = value;
            var table = $$("danhsach_hoadonFrm");
            webix.extend(table, webix.ProgressBar);
            table.disable();
            table.showProgress();
            webix
                .ajax()
                .headers({ "Content-type": "application/json" })
                .post(app.config.host + "/Invoice68/GetHoadonChoXuLy", params, {
                    error: function (text, data, XmlHttpRequest) {
                        table.enable();
                        table.hideProgress();
                        webix.message(text, "error");
                    },
                    success: function (text, data, XmlHttpRequest) {
                        var json = JSON.parse(text);
                        if (!json.hasOwnProperty("error")) {
                            //console.log(json);
                            $$("danhsach_hoadonFrm").clearAll();
                            $$("danhsach_hoadonFrm").parse(json);

                            if (json["data"].length > 0) {
                                $$("tgtcthue").setValue(fm(json["data"][0]["tong_dt"]));
                                $$("tgtthue").setValue(fm(json["data"][0]["tong_tienthue"]));
                            }

                            $$("danhsach_hoadonFrm").refresh();
                        } else {
                            webix.message(json.error, "error");
                        }
                        table.enable();
                        table.hideProgress();
                    },
                });
        },
        setValues: function (item) {},
    };
});
