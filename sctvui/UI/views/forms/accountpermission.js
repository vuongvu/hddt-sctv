define(["app", "locale"], function (app, _) {
    function initUI(item) {
        var url = app.config.host + "/System/ExecuteCommand";

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(
                url,
                {
                    command: "CM00005",
                    parameter: {
                        wb_user_id: item.id,
                    },
                },
                function (text) {
                    var json = JSON.parse(text);

                    var dtChuKySo = $$("dtChuKySo");

                    dtChuKySo.parse(json, "json");
                }
            );

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(
                url,
                {
                    command: "CM00006",
                    parameter: {
                        wb_user_id: item.id,
                    },
                },
                function (text) {
                    var json = JSON.parse(text);
                    var dtMauHoaDon = $$("dtMauHoaDon");

                    dtMauHoaDon.parse(json, "json");

                    /* for(var i=0;i<json.length;i++){
					var item = json[i];
	
					if (item.VC_NHOMQUYEN_id!=null){
						trtBaoCao.checkItem(item.id);
					}
				} */
                }
            );

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(
                url,
                {
                    command: "CM000100",
                    parameter: {
                        wb_user_id: item.id,
                    },
                },
                function (text) {
                    var json = JSON.parse(text);
                    var dtMauHoaDon = $$("dtMauHoaDon68");

                    dtMauHoaDon.parse(json, "json");

                    /* for(var i=0;i<json.length;i++){
					var item = json[i];
	
					if (item.VC_NHOMQUYEN_id!=null){
						trtBaoCao.checkItem(item.id);
					}
				} */
                }
            );

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(
                url,
                {
                    command: "CM00008",
                    parameter: {
                        wb_user_id: item.id,
                    },
                },
                function (text) {
                    var json = JSON.parse(text);
                    var dtDonvi = $$("dtDonvi");

                    dtDonvi.parse(json, "json");
                }
            );
        var is_new_user = item.is_new_user == null ? "C" : item.is_new_user;

        $$("chkIssigninvoice").setValue(item.is_sign_invoice);
        $$("chkIssigninvoicehsm").setValue(item.is_sign_invoice_hsm);
        $$("chkIsviewuser").setValue(item.is_view_user);
        $$("chkIsedituser").setValue(item.is_edit_user);
        $$("chkIsdeluser").setValue(item.is_del_user);
        $$("chkIsnewuser").setValue(is_new_user);
    }

    return {
        $ui: {
            view: "window",
            modal: true,
            id: "phanquyen-win",
            position: "center",
            head: "Thông tin Tab",
            fullscreen: true,
            body: {
                rows: [
                    {
                        borderless: true,
                        view: "tabview",
                        scrollX: true,
                        multiview: {
                            fitBiggest: true,
                        },
                        cells: [
                            {
                                header: _("CHU_KY_SO"),
                                body: {
                                    id: "dtChuKySo",
                                    view: "datatable",
                                    resizeRow: true,
                                    fixedRowHeight: false,
                                    rowLineHeight: 25,
                                    rowHeight: 70,
                                    //threeState: true,
                                    columns: [
                                        {
                                            id: "chon1",
                                            header: [{ text: "", content: "masterCheckbox" }, _("CHON")],
                                            width: 55,
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            template: "{common.checkbox()}",
                                        },
                                        {
                                            id: "cer_serial",
                                            header: _("SO_SERIAL"),
                                            width: 280,
                                        },
                                        {
                                            id: "subject_name",
                                            header: "Chủ thể",
                                            width: 500,
                                        },
                                        {
                                            id: "begin_date",
                                            header: "Ngày bắt đầu",
                                            width: 150,
                                        },
                                        {
                                            id: "end_date",
                                            header: "Ngày kết thúc",
                                            width: 150,
                                        },
                                    ],
                                },
                            },
                            {
                                header: _("MAU_HDON"),
                                body: {
                                    id: "dtMauHoaDon",
                                    view: "datatable",
                                    scheme: {
                                        $init: function (obj) {
                                            //creating fake dates for the sample
                                            obj.date = new Date(obj.ngay_bd_sd);
                                        },
                                    },
                                    columns: [
                                        {
                                            id: "chon1",
                                            header: [{ text: "", content: "masterCheckbox" }, _("CHON")],
                                            width: 55,
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            template: "{common.checkbox()}",
                                        },
                                        {
                                            id: "ma_loai",
                                            header: [_("MA_LOAI"), { content: "textFilter" }],
                                            width: 100,
                                        },
                                        {
                                            id: "ten_loai",
                                            header: [_("TEN_LOAI"), { content: "textFilter" }],
                                            width: 250,
                                        },
                                        {
                                            id: "mau_so",
                                            header: [_("MAU_SO"), { content: "textFilter" }],
                                            width: 100,
                                        },
                                        {
                                            id: "ky_hieu",
                                            header: [_("KY_HIEU"), { content: "textFilter" }],
                                            width: 100,
                                        },
                                        {
                                            id: "tu_so",
                                            header: [_("TU_SO"), { content: "textFilter" }],
                                            width: 100,
                                        },
                                        {
                                            id: "den_so",
                                            header: [_("DEN_SO"), { content: "textFilter" }],
                                            width: 100,
                                        },
                                        {
                                            id: "date",
                                            header: [_("NGAY_BD_SD"), { content: "dateRangeFilter" }],
                                            width: 150,
                                            format: function (text) {
                                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                                text = removeTimeZoneT(text);
                                                return format(text);
                                            },
                                        },
                                    ],
                                },
                            },
                            {
                                header: _("Mẫu hóa đơn NĐ123"),
                                body: {
                                    id: "dtMauHoaDon68",
                                    view: "datatable",
                                    scheme: {
                                        $init: function (obj) {
                                            //creating fake dates for the sample
                                            //obj.date = new Date(obj.ngay_bd_sd);
                                        },
                                    },
                                    columns: [
                                        {
                                            id: "chon1",
                                            header: [{ text: "", content: "masterCheckbox" }, _("CHON")],
                                            width: 55,
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            template: "{common.checkbox()}",
                                        },
                                        {
                                            id: "lhdon",
                                            header: [_("Loại hóa đơn"), { content: "textFilter" }],
                                            width: 140,
                                        },
                                        {
                                            id: "hthuc",
                                            header: [_("Hình thức"), { content: "textFilter" }],
                                            width: 250,
                                        },
                                        {
                                            id: "khdon",
                                            header: [_("Kiểu hóa đơn"), { content: "textFilter" }],
                                            width: 140,
                                        },
                                        {
                                            id: "khhdon",
                                            header: [_("KY_HIEU"), { content: "textFilter" }],
                                            width: 100,
                                        },
                                    ],
                                },
                            },
                            {
                                header: _("DONVI"),
                                hidden: true,
                                body: {
                                    id: "dtDonvi",
                                    view: "datatable",
                                    columns: [
                                        {
                                            id: "chon1",
                                            header: [{ text: "", content: "masterCheckbox" }, _("CHON")],
                                            width: 55,
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            template: "{common.checkbox()}",
                                        },
                                        {
                                            id: "ma_dvcs",
                                            header: _("MA"),
                                            width: 100,
                                        },
                                        {
                                            id: "ten_dvcs",
                                            header: _("TEN"),
                                            width: 450,
                                        },
                                    ],
                                },
                            },
                            {
                                header: _("QUYEN_TRUY_CAP_HD"),
                                body: {
                                    paddingY: 10,
                                    paddingX: 10,
                                    rows: [
                                        {
                                            id: "chkIssigninvoice",
                                            view: "checkbox",
                                            name: "issigninvoice",
                                            labelRight: _("KY_HOA_DON"),
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            labelWidth: 0,
                                        },
                                        {
                                            id: "chkIssigninvoicehsm",
                                            view: "checkbox",
                                            name: "issigninvoicehsm",
                                            labelRight: _("KY_HOA_DON_HSM"),
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            labelWidth: 0
                                        },
                                        {
                                            id: "chkIsviewuser",
                                            view: "checkbox",
                                            name: "isviewuser",
                                            labelRight: _("IS_VIEW_USER"),
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            labelAlign: "right",
                                            labelWidth: 0,
                                        },
                                        {
                                            id: "chkIsnewuser",
                                            view: "checkbox",
                                            name: "isnewuser",
                                            labelRight: _("IS_NEW_USER"),
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            labelWidth: 0,
                                        },
                                        {
                                            id: "chkIsedituser",
                                            view: "checkbox",
                                            name: "isedituser",
                                            labelRight: _("IS_EDIT_USER"),
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            labelWidth: 0,
                                        },
                                        {
                                            id: "chkIsdeluser",
                                            view: "checkbox",
                                            name: "isdeluser",
                                            labelRight: _("IS_DEL_USER"),
                                            checkValue: "C",
                                            uncheckValue: "K",
                                            labelWidth: 0,
                                        },
                                    ],
                                },
                            },
                        ],
                    },
                    {
                        cols: [
                            {},
                            {
                                shortcut: "F10",
                                view: "button",
                                label: "Lưu (F10)",
                                type: "form",
                                align: "center",
                                width: 100,
                                on: {
                                    onClick: function () {
                                        var dtChuKySo = $$("dtChuKySo");
                                        var dtMauHoaDon = $$("dtMauHoaDon");
                                        var dtMauHoaDon68 = $$("dtMauHoaDon68");
                                        var dtDonvi = $$("dtDonvi");

                                        var win = this.getTopParentView();

                                        var data = {
                                            wb_user_id: win.config.wb_user_id,
                                            ckspermission: dtChuKySo.serialize(),
                                            invtemppermission: dtMauHoaDon.serialize(),
                                            dtMauHoaDon68: dtMauHoaDon68.serialize(),
                                            //dvpermission: dtDonvi.serialize(),
                                            invoicepermission: {
                                                issigninvoice: $$("chkIssigninvoice").getValue(),
                                                issigninvoicehsm: $$("chkIssigninvoicehsm").getValue(),
                                                isviewuser: $$("chkIsviewuser").getValue(),
                                                isedituser: $$("chkIsedituser").getValue(),
                                                isdeluser: $$("chkIsdeluser").getValue(),
                                                isnewuser: $$("chkIsnewuser").getValue(),
                                            },
                                        };

                                        var url = app.config.host + "/Account/Phanquyen";

                                        webix
                                            .ajax()
                                            .bind(data)
                                            .headers({ "Content-type": "application/json" })
                                            .post(url, data, function (text) {
                                                var result = JSON.parse(text);

                                                if (!result.hasOwnProperty("error")) {
                                                    var table = webix.$$("dmwindowData");
                                                    var item = table.getSelectedItem();

                                                    item.is_sign_invoice = this.invoicepermission.issigninvoice;
                                                    item.is_sign_invoice_hsm = this.invoicepermission.issigninvoicehsm;
                                                    item.is_view_user = this.invoicepermission.isviewuser;
                                                    item.is_edit_user = this.invoicepermission.isedituser;
                                                    item.is_del_user = this.invoicepermission.isdeluser;
                                                    item.is_new_user = this.invoicepermission.isnewuser;

                                                    table.updateItem(item.id, item);
                                                    webix.$$("phanquyen-win").close();
                                                } else {
                                                    webix.message({ type: "error", text: result.error });
                                                }
                                            });
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },
                            {
                                view: "button",
                                shortcut: "esc",
                                type: "danger",
                                label: "Hủy bỏ (ESC)",
                                align: "center",
                                width: 100,
                                on: {
                                    onClick: function () {
                                        this.getTopParentView().close();
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },
                            {},
                        ],
                    },
                    {
                        height: 5,
                    },
                ],
            },
        },
        initUI: initUI,
    };
});
