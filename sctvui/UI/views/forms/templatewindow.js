define([
	"app",
	"locale",
	"models/user",
	"views/forms/editwindow",
	"views/forms/selectwindow"
],function(app,_,user,editwindow,selectwindow){
	var editmode = 0;	
    var tables = [];

	var windowno = app.path[1].params[0];
    var col=[];
	var rw =[];					
	var el = [];
	var head_name=[];
	var head;
	var url = app.config.host + "/api/System/GetAllByWindowNo?window_id=" + windowno;
		    
	return {		
		$ui:{
			view:"window", modal:true, id:"window-win", position:"center",
			//width:800,
			head:"Thông tin khách hàng",
			//fullscreen:true,			
			body:{
                paddingY:10, 
                paddingX:10, 
                view:"form", 
				id:"window-form", 
				complexData:true,
				margin:0,
				borderless:true,
                elements:el				
			}			
		},	
		initUI: function(windowconfig) {
			
			var rp = windowconfig;
			var rows_max = rp['data'][0]['rows_max'];
			var vc_window = rp["data"][0];
			var tabs = vc_window.Tabs;

			var win = $$(vc_window.window_id);

			if (typeof win.showProgress === "undefined")
				webix.extend(win, webix.ProgressBar);
			
			el = [];
			col = [] ;			

			if (vc_window.width==null && vc_window.height==null){
				win.config.fullscreen = true;				
			}

			if (vc_window.after_save!=null){
				win.config.after_save = vc_window.after_save;
			}

			var layoutdetail = vc_window.layoutdetail;
			webix.ui(JSON.parse(layoutdetail),$$("window-form"));

			var fields = rp['data'][0]['Tabs'][0]['Fields'];

			for(var i=0;i<fields.length;i++){
				
				var field = fields[i];				
				
				if (field.hidden == true){
					continue;
				}

				var row = field.row;

				var item = {
					view: field.type_editor, 
					name: field.column_name,
					label: _(field.column_name), 
					id: field.column_name+"_"+rp['data'][0]['Tabs'][0]["tab_table"],
					width: field.width,
					labelPosition: field.label_position,
					labelWidth: field.label_width,
					format: field.format,
					value_change: field.value_change,
					fieldExpression: field.field_expression,
					data_width: field.data_width,
					column_type: field.column_type,
					on:{
						
					}
				};

				if (field.height!=null){
					if (field.height!=0){
						item.height = field.height;
					}
				}

				if (field.caption!=null){
					item.label = _(field.caption);
				}				

				if (field.read_only == "C"){
					item.readonly = true;
				}
				
				if (item.view == "combo"){
					item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
				}						

				if (item.view == "checkbox"){
					if (field.label_position == "right"){
						item.labelRight = _(field.column_name) ;
						item.label = "";							
					}

					if (field.column_type == "char"){
						item.checkValue = "C";
						item.uncheckValue = "K";
					}

					if (field.column_type == "tinyint"){
						item.checkValue = "1";
						item.uncheckValue = "0";
					}
				}

				if (item.view == "autonumeric"){
					item.inputAlign ="right";
				}

				if (item.view == "treesuggest"){
					
					//item.REF_ID = field.REF_ID.toString();
					var listColumns = JSON.parse(field.list_column);

					for(var v=0;v<listColumns.length;v++){
						listColumns[v].header =_(listColumns[v].id);
					}
					
					item.view = "combo";
					item.options = {
						view: "treesuggest",
						textValue: field.display_field,
						url: {
							$proxy:true,
							load:function(view, callback, params){
									var url = app.config.host + "/api/System/GetDataByReferencesId?id=" + view.config.refid;

									webix.ajax(url, function(text){
										var json = JSON.parse(text);
										var data = listToTree(json);
										view.parse(data,"json");
									});
									
							}
						},
						body: {
							columns: listColumns,
							refid: field.ref_id,
							filterMode:{
								level:false,
								showSubItems:false
							}						
						}
					};

					item.on.onKeyPress = function(code, e){
						if (e.code == "F4"){
							alert("Moi");
						}
					};
				}

				if (item.view == "gridcombo"){
					
					var listColumns = JSON.parse(field.list_column);

					for(var v=0;v<listColumns.length;v++){
						listColumns[v].header =_(listColumns[v].id);
					}
					
					item.view = "combo";
					item.options = {
						view: "gridsuggest",
						url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
						body: {
							columns: listColumns,
							refid: field.REF_ID,
							textValue: field.display_field											
						}
					};

					item.on.onKeyPress = function(code, e){
						if (e.code == "F4"){
							alert("Moi");
						}
					};
				}

				if (item.view == "gridsuggest"){
					var listColumns = JSON.parse(field.list_column);

					for(var v=0;v<listColumns.length;v++){
						listColumns[v].header =_(listColumns[v].id);
					}

					item.view = "text";
					item.suggest = {
						view: "gridsuggest",
						value_suggest: field.value_suggest,
						body:{
							columns:listColumns,
							refid: field.ref_id,
							datatype:"json",
							dataFeed: function(filtervalue, filter){
								if(filtervalue.length<3) return;

								var refid = this.config.refid;
								var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
								var urldata = "filtervalue="+encodeURIComponent(filtervalue);
								
								this.load(pathUrl+"&"+urldata, this.config.datatype);
							},
							
						},
						on: {
							onValueSuggest: function(obj){
								var master = $$(this.config.master);

								if (master.config.fieldExpression!=null){
									
									var win = master.getTopParentView();
									var form = win.getChildViews()[1];
									var parentItem = form.getValues();

									var fields = master.config.fieldExpression.split(';');

									for(var i=0;i<fields.length;i++){
										var field = fields[i].replace("{","").replace("}","");
										var array = field.split(":");

										parentItem[array[1]] = obj[array[0]];
									}

									form.setValues(parentItem);
								}

								if (this.config.value_suggest!=null){								
									var fn = Function("obj","id",this.config.value_suggest);
									fn(obj,this.config.id);
								}
							}							
						}
					};

					item.on.onKeyPress = function(code, e){
						if (e.code == "F2"){
							var suggest = $$(this.config.suggest);
							var refid = suggest.config.body.refid;

							suggest.hide();

							var sql = "SELECT a.*,b.window_id as WINDOWNO FROM wb_references a INNER JOIN wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='"+refid +"'";
							var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;

							var win = this.getTopParentView();
							var form = win.getChildViews()[1];

							var editorId = this.config.id;
							var formId = form.config.id;
							
							var a= webix.ajax(url);
							var b= this.config.suggest;
							var c = this.config.id;
							var d = this.config.fieldExpression;
							var e = editorId;
							var f = formId;
							

							webix.promise.all([a,b,c,d,e,f]).then(function(results){
								var res_a = results[0].json();
								var suggest = $$(results[1]);								

								suggest.hide();

								var url = app.config.host + "/System/GetConfigWinByNo?id="+ res_a[0]["WINDOWNO"] ;

								var ajax = webix.ajax();
								ajax.master = {
									searchId : results[2],
									fieldExpression: d,
									valueField: res_a[0]["value_field"],
									formId: f,
									editorId:e
								};

								ajax.get(url,function(text){
									var windowconfig = JSON.parse(text);
									var txtSearch = $$(this.searchId);

									var win = txtSearch.$scope.ui(selectwindow.$ui);
									
									win.config.valueField = this.valueField;
									win.config.fieldExpression = this.fieldExpression;
									win.config.formId = this.formId;
									win.config.editorId = this.editorId;

									selectwindow.initUI(windowconfig);

									win.getHead().setHTML(windowconfig.data[0].window_name);
									win.show();

								});


							});


							var win = this.$scope.ui(selectwindow.$ui);
							win.show();							
						}

						if (e.code == "F4" || e.code == "F3"){
							var suggest = $$(this.config.suggest);
							var refid = suggest.config.body.refid;

							var sql = "SELECT a.*,b.window_id as WINDOWNO FROM wb_references a INNER JOIN wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='"+refid +"'";
							var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;

							var win = this.getTopParentView();
							var form = win.getChildViews()[1];

							var editorId = this.config.id;
							var formId = form.config.id;

							var ajax = webix.ajax();
							
							ajax.master = {
								editorId: editorId,
								formId: formId,
								fieldExpression: this.config.fieldExpression,
								editmode: e.code == "F3" ? 2 : 1,
								editorvalue: this.getValue(),
								suggest: this.config.suggest
							};							

							ajax.get(url,function(text){
								var json = JSON.parse(text);
								
								if (json.length>0){

									json[0].editorId = this.editorId;
									json[0].formId = this.formId;
									json[0].fieldExpression = this.fieldExpression;
									json[0].editmode = this.editmode;
									json[0].editorvalue = this.editorvalue;
									json[0].suggest = this.suggest;

									var windowno = json[0].WINDOWNO;					

									var url = app.config.host + "/api/System/GetAllByWindowNo?window_id="+windowno ;
									var a = webix.ajax(url);
									url = app.config.host +"/api/System/GetDataByReferences?id="+json[0].id+"&value_field=" + this.editorvalue;
									var b = webix.ajax(url);
									var c= this.editmode;
									var d = json[0];

									webix.promise.all([a,b,c,d]).then(function(results){
										
										var windowconfig = results[0].json();
										var res_b = results[1].json();
										var mode = results[2];
										var master = results[3];

										var vc_window = windowconfig["data"][0];

										if (mode == 1){
											editwindow.$ui.id = vc_window.window_id;
											editwindow.$ui.editorId = master.editorId;
											editwindow.$ui.valueField = master.value_field;
											editwindow.$ui.fieldExpression = master.fieldExpression;
											editwindow.$ui.formId = master.formId;

											var win = webix.ui(editwindow.$ui);            
											
											editwindow.initUI(windowconfig);
											editwindow.setEditMode(mode);
											editwindow.setValues(null,windowconfig);
											
											$$(master.suggest).hide();
											win.show();
										}
										else {

											if (res_b.length == 0){
												webix.alert({
													title: "Thông báo",
													text: "Không tìm thấy dữ liệu"
												});

												return;
											}

											var ajax = webix.ajax();
											
											ajax.master = {
												master: master,
												vc_window: vc_window												
											};

											var url = app.config.host + "/api/System/GetDataById?window_id="+ vc_window.window_id + "&id="+res_b[0].doc_id;

											ajax.get(url,function(text){
												var json = JSON.parse(text);
												var data = json.data[0];

												var vc_window = this.vc_window;
												var master = this.master;

												editwindow.$ui.id = vc_window.window_id;
												editwindow.$ui.editorId = master.editorId;
												editwindow.$ui.valueField = master.value_field;
												editwindow.$ui.fieldExpression = master.fieldExpression;
												editwindow.$ui.formId = master.formId;

												var win = webix.ui(editwindow.$ui);            
												
												editwindow.initUI(windowconfig);
												editwindow.setEditMode(2);
												editwindow.setValues(data,windowconfig);
												
												$$(master.suggest).hide();
												win.show();

											});
										}
									});									
								}
							});

							
						}
					};
				}

				if (item.value_change!=null || field.column_type == "nvarchar" || field.column_type == "varchar" || field.column_type == "char"){
					item.on.onChange = function(newv,oldv){
							var data_width = this.config.data_width;

							if (data_width< newv.length){
								webix.message("Số lượng ký tự tối đa là " + data_width.toString());
								return;
							}

							if (this.config.value_change!=null){
								
								
								var fn = Function("newv","oldv","id",this.config.value_change);
								fn(newv,oldv,this.config.id);
							}
						}
				}
				
				webix.ui(item,$$(item.id));
				//el[row-1].cols.push(item);
								
			}

			if (vc_window.window_type == "MasterDetail"){

				var cells = [];

                for(var i=0;i<tabs.length;i++){
                    var tab = tabs[i];

                    if (tab.TAB_TYPE == "Master"){
                        continue;
                    }

                    var subCols = [];

                    for(var j=0;j<tab.Fields.length;j++){
                        var field = tab.Fields[j];

                        var subCol = {
                            id: field.column_name, 
							header: field.caption!=null ? _(field.caption) : _(field.column_name),
							editor: field.type_editor,
							hidden: field.hidden,
							value_change: field.value_change
						};
						

                        if (field.column_type == "date" || field.column_type == "datetime" ){
                            subCol.format = function(text) {
                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                text = removeTimeZoneT(text);                            
                                return format(text);
                            };
						}

						if (field.column_type == "decimal"){
							subCol.css = { "text-align": "right" };
							subCol.format = webix.Number.numToStr({
												groupDelimiter:",",
												groupSize:3,
												decimalDelimiter:".",
												decimalSize:0
											});
						}
						
						
                        
                        if (field.column_width == null){
                            subCol.fillspace = true;
                        }
                        else {
                            subCol.width = field.column_width;
						}

						if (field.type_editor == "combo"){
							subCol.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id
						}

						if (field.type_editor == "datepicker"){
							subCol.editor = "date";
						}
						
						if (field.type_editor == "treesuggest"){
							var listColumns = JSON.parse(field.list_column);

							for(var v=0;v<listColumns.length;v++){
								listColumns[v].header =_(listColumns[v].id);
							}
							
							var popup = webix.ui({
								view:"treesuggest",								
								body:{
									columns: listColumns,
									refid: field.ref_id,
									url: {
										$proxy:true,
										load:function(view, callback, params){
												var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

												webix.ajax(url, function(text){
													var json = JSON.parse(text);
													var data = listToTree(json);
													view.parse(data,"json");
												});
												
										}
									},
									filterMode:{
										level:false,
										showSubItems:false
									}	
								}
							});											

							subCol.popup = popup;
							subCol.editor = "combo";
							
						}

						if (field.type_editor == "gridcombo"){
							var listColumns = JSON.parse(field.list_column);

							for(var v=0;v<listColumns.length;v++){
								listColumns[v].header =_(listColumns[v].id);
							}
							
							var popup = webix.ui({
								view:"gridsuggest",	
								textValue: field.display_field,
								body:{
									columns: listColumns,
									refid: field.ref_id,
									textValue: field.display_field,
									tableId: "edit_"+tab.tab_table,
									url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id								
								}
							});											

							subCol.popup = popup;
							subCol.editor = "combo";
							
							
						}

						if (field.type_editor == "gridsuggest"){
							var listColumns = JSON.parse(field.list_column);

							for(var v=0;v<listColumns.length;v++){
								listColumns[v].header =_(listColumns[v].id.toUpperCase());
							}

							subCol.editor = "text";
							subCol.suggest = {
								view: "gridsuggest",
								value_suggest: field.value_suggest,
								fieldExpression: field.field_expression,
								displayField: field.display_field,
								tableId: "edit_"+tab.tab_table,
								columnName:field.column_name,
								body:{
									columns:listColumns,
									refid: field.ref_id,									
									datatype:"json",
									dataFeed: function(filtervalue, filter){
										if(filtervalue.length<3) return;

										var refid = this.config.refid;
										var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
										var urldata = "filtervalue="+encodeURIComponent(filtervalue);
										
										this.load(pathUrl+"&"+urldata, this.config.datatype);
									}
								},
								on: {
									onValueSuggest: function(obj){
										
										if (this.config.fieldExpression!=null){
											var table = $$(this.config.tableId);
											var selectedItem = table.getSelectedItem();

											var fields = this.config.fieldExpression.split(';');

											for(var i=0;i<fields.length;i++){
												var field = fields[i].replace("{","").replace("}","");
												var array = field.split(":");

												selectedItem[array[1]] = obj[array[0]];
											}

											table.updateItem(selectedItem.id,selectedItem);
										}

										if (this.config.value_suggest!=null){								
											var fn = Function("obj","id",this.config.value_suggest);
											fn(obj,this.config.id);
										}
									}
									
								}
							}

							
						}
                        
                        subCols.push(subCol);
                    }

                    if (tab.tab_type=="Detail"){
                        var item = {
                            header: tab.tab_name,
                            select: "cell",
                            body: {
								rows:[
									{
										view:"toolbar",
										height:25,
										cols:[
											{
												view:"icon",
												icon:"plus",
												on: {
													onItemClick: function(id,e){
														var parentView = this.getParentView().getParentView();
														var childViews = parentView.getChildViews();

														for(var i=0;i<childViews.length;i++){
															var childview = childViews[i];

															if (childview.config.view=="datatable"){
																var table = childview;

																var item = {
																	id: webix.uid()
																};

																var vc_window = windowconfig['data'][0];																
																var tabs = vc_window.Tabs;															

																for(var i=0;i<tabs.length;i++){
																	var tab = tabs[i];
																	if (tab.tab_type == "Detail"){
																		var fields=tab.Fields;

																		for(var j=0;j<fields.length;j++){
																			var field = fields[j];

																			if (field.column_name == "ma_dvcs"){
																				item[field.column_name] = user.getDvcsID();
																			}																			
																			
																			if (field.default_value!=null){
																				var defaultValue = field.default_value.toString();

																				if (defaultValue.startsWith("@Default=")){
																					item[field.column_name] = defaultValue.replace("@Default=","");
																				}
																				
																				if (defaultValue.startsWith("@JS=")){
																					var fn = Function(defaultValue.replace("@JS=",""));
																					item[field.column_name] = fn();
																				}
																			}
																		}
																	}
																}								
																

																table.add(item);

																table.select(item.id, table.config.columns[0].id, false);
																table.edit({row: item.id, column: table.config.columns[0].id});

																break;
															}
														}
														
													}
												}
											},
											{
												view:"icon",
												icon:"minus",
												on: {
													onItemClick: function(id,e){
														var parentView = this.getParentView().getParentView();
														var childViews = parentView.getChildViews();

														for(var i=0;i<childViews.length;i++){
															var childview = childViews[i];

															if (childview.config.view=="datatable"){
																var table = childview;

																table.editStop();

																var item = table.getSelectedItem();
												
																if (!webix.isUndefined(item)){
																	table.remove(item.id);
																	table.clearSelection();
																	return;
																}

																var editor = table.getEditState();

																if (!webix.isUndefined(editor)){
																	table.remove(editor.row);
																	table.clearSelection();
																	return;
																}

																break;
															}
														}
														
													}
												}
											}
										]
									},
									{
										view: "datatable",
										id:"edit_"+tab.tab_table,
										tab_table: tab.tab_table,
										tab_id: tab.tab_id,
										editable:true,
										select: "cell",
										select_change: tab.select_change,
										cell_editstop: tab.cell_editstop,
										columns: subCols,
										on:{
											onAfterEditStop:function(state, editor, ignore){
												
												if (this.config.cell_editstop!=null){
													var fn = Function("columnId","tableId",this.config.cell_editstop);
													fn(editor.config.id,this.config.id);
												}

												if (editor.config.value_change!=null){
													var selectedItem = null;

													if (typeof editor.getPopup === "function") { 
														var popup = editor.getPopup();
													
														if (popup!=null){
															var list = popup.getList();

															if (list!=null){
																selectedItem = list.getSelectedItem();
															}
														}
													}
													
													var fn = Function("newv","oldv","id","selectedItem",editor.config.value_change);
													fn(state.value,state.old,editor.config.id,selectedItem);
												}

											},
											onKeyPress: function(code,e){
												var table = this;

												if (e.key == "F8"){
													var toolbar = this.getParentView().getChildViews()[0];
													var btnMinus = toolbar.getChildViews()[1];

													btnMinus.callEvent("onItemClick");
												}

												if (e.key == "F2"){

													var editor = table.getEditState();

													if (editor === false) return;

													var suggest = $$(editor.config.suggest);
													var refid = suggest.config.body.refid;

													var sql = "SELECT a.*,b.window_id as WINDOWNO FROM wb_references a INNER JOIN wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='"+refid +"'";
													var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;

													var a= webix.ajax(url);
													var b= editor.config.suggest;
													var c = table.config.id;
													var d = suggest.config.fieldExpression;
													var e = editor.config.id;
													var f = suggest.config.displayField;
													
													webix.promise.all([a,b,c,d,e,f]).then(function(results){
														var res_a = results[0].json();
														var suggest = $$(results[1]);								

														suggest.hide();

														var url = app.config.host + "/System/GetConfigWinByNo?id="+ res_a[0]["WINDOWNO"] ;

														var ajax = webix.ajax();
														ajax.master = {
															tableId : results[2],
															fieldExpression: d,
															valueField: res_a[0]["value_field"],
															editorId:null,
															columnName: e,
															displayField:f
														};

														ajax.get(url,function(text){
															var windowconfig = JSON.parse(text);
															var table = $$(this.tableId);

															var win = table.$scope.ui(selectwindow.$ui);
															
															win.config.valueField = this.valueField;
															win.config.fieldExpression = this.fieldExpression;
															win.config.tableId = this.tableId;
															win.config.editorId = null;
															win.config.columnName = this.columnName;
															win.config.displayField = this.displayField;

															selectwindow.initUI(windowconfig);
															
															win.getHead().setHTML(windowconfig.data[0].window_name);
															win.show();

														});


													});


													var win = this.$scope.ui(selectwindow.$ui);
													win.show();
												}

												if (e.key == "F4" || e.key == "F3"){
													var editor = table.getEditState();

													if (editor === false){
														var toolbar = this.getParentView().getChildViews()[0];
														var btnPlus = toolbar.getChildViews()[0];

														btnPlus.callEvent("onItemClick");

													}

													if (!webix.isUndefined(editor)){
														var suggest = $$(editor.config.suggest);
														var refid = suggest.config.body.refid;

														var sql = "SELECT a.*,b.WINDOW_ID as WINDOWNO FROM VC_REFERENCES a INNER JOIN VC_WINDOW b ON a.WINDOW_ID=b.id  WHERE a.id='"+refid +"'";
														var url = app.config.host + "/api/System/ExecuteQuery?sql=" + sql;

														var tableId = table.config.id;
														var columnName = editor.config.id;

														var ajax = webix.ajax();
														
														ajax.master = {
															editorId: null,
															tableId: tableId,
															fieldExpression: suggest.config.fieldExpression,
															columnName: columnName,
															editmode: e.key == "F3" ? 2 : 1,
															editorvalue: editor.getValue(),
															suggest: editor.config.suggest
														};

														ajax.get(url,function(text){
															var json = JSON.parse(text);
															
															if (json.length>0){

																json[0].editorId = this.editorId;
																json[0].tableId = this.tableId;
																json[0].fieldExpression = this.fieldExpression;
																json[0].columnName = this.columnName;
																json[0].editmode = this.editmode;
																json[0].editorvalue = this.editorvalue;
																json[0].suggest = this.suggest;

																var windowno = json[0].WINDOWNO;															

																var url = app.config.host + "/api/System/GetAllByWindowNo?window_id="+windowno ;
																var a = webix.ajax(url);
																url = app.config.host +"/api/System/GetDataByReferences?id="+json[0].id+"&value_field=" + this.editorvalue;
																var b = webix.ajax(url);
																var c= this.editmode;
																var d = json[0];

																webix.promise.all([a,b,c,d]).then(function(results){
										
																	var windowconfig = results[0].json();
																	var res_b = results[1].json();
																	var mode = results[2];
																	var master = results[3];

																	var vc_window = windowconfig["data"][0];

																	if (mode == 1){
																		editwindow.$ui.id = vc_window.window_id;
																		editwindow.$ui.editorId = master.editorId;
																		editwindow.$ui.valueField = master.value_field;
																		editwindow.$ui.fieldExpression = master.fieldExpression;
																		editwindow.$ui.formId = master.formId;
																		editwindow.$ui.tableId = master.tableId;
																		editwindow.$ui.columnName = master.columnName;

																		var win = webix.ui(editwindow.$ui);            
																		
																		editwindow.initUI(windowconfig);
																		editwindow.setEditMode(mode);
																		editwindow.setValues(null,windowconfig);
																		
																		$$(master.suggest).hide();
																		win.show();
																	}
																	else {
																		if (res_b.length == 0){
																			webix.alert({
																				title: "Thông báo",
																				text: "Không tìm thấy dữ liệu"
																			});

																			return;
																		}
																		var ajax = webix.ajax();
																		
																		ajax.master = {
																			master: master,
																			vc_window: vc_window												
																		};

																		var url = app.config.host + "/api/System/GetDataById?window_id="+ vc_window.window_id + "&id="+res_b[0].doc_id;

																		ajax.get(url,function(text){
																			var json = JSON.parse(text);
																			var data = json.data[0];

																			var vc_window = this.vc_window;
																			var master = this.master;

																			editwindow.$ui.id = vc_window.window_id;
																			editwindow.$ui.editorId = master.editorId;
																			editwindow.$ui.valueField = master.value_field;
																			editwindow.$ui.fieldExpression = master.fieldExpression;
																			editwindow.$ui.formId = master.formId;
																			editwindow.$ui.tableId = master.tableId;
																			editwindow.$ui.columnName = master.columnName;

																			var win = webix.ui(editwindow.$ui);            
																			
																			editwindow.initUI(windowconfig);
																			editwindow.setEditMode(2);
																			editwindow.setValues(data,windowconfig);
																			
																			$$(master.suggest).hide();
																			win.show();

																		});
																	}
																});																
															}
														});

														return;
													}
													
												}
											},
											onSelectChange:function(){
												var selectedItem = this.getSelectedId();
												
												if (this.config.select_change!=null){
													var fn = Function("selectedItem",this.config.select_change);
													fn(selectedItem);
												}
											}
										}
									}
								]
                                
                            }
                        };

                        cells.push(item);
                    }
				}
				
                var subview = {
                    borderless:true,
                    view:"tabview",
                    scrollX:true,
					cells: cells,
					tabbar:{
						width:200
					}					
				};

				if (vc_window.width!=null || vc_window.height!=null){
					subview.height = 250;
				}

				//el.push(subview);
				
				if (vc_window.row_tab!=null){
					el[vc_window.row_tab-1]=subview;
				}
				else {
					el.push(subview);
				}
				
            }
			
			var cols = [
				{},
				{ 
					shortcut:"F10",  
					view:"button", label:"Nhận", type:"form", align:"center", 
					width:100,
					on: {
						onClick: function() {
							var form = webix.$$("window-form");
							
							var item = form.getValues();
							
							var khData={};

							khData.windowid=String(rp['data'][0]['window_id']);
							khData.editmode=editmode;
							khData.data=[];

							var vc_window = rp['data'][0];

							if (vc_window.window_type == "MasterDetail"){
								
								var tabs = vc_window.Tabs;
								var details = [];

								for(var i=0;i<tabs.length;i++){
									var tab = tabs[i];

									if (tab.tab_type == "Detail"){
										var table = webix.$$("edit_"+tab.tab_table);
										
										details.push({
											tab_id: tab.tab_id,
											tab_table: tab.tab_table,
											data: table.serialize()
										});
									}
								}

								item["details"] = details;
							}							
							
							khData.data.push(item);
							
							this.getTopParentView().disable();
							this.getTopParentView().showProgress();
							
							webix.ajax()
								.headers({'Content-type':'application/json'})
								.post( app.config.host + "/System/Save", khData, function(text){									
							
									var json = JSON.parse(text);

									$$(json.windowid).enable();
									$$(json.windowid).hideProgress();
												
									if (!json.hasOwnProperty("error")){
										var after_save = $$(json.windowid).config.after_save;

										if (after_save!=null){
											var fn = Function("editmode","data",after_save);
											fn(editmode,json.data);
										}										

										$$(json.windowid).close();
										app.callEvent("dataChanged",[editmode,json.data]);
									}
									else {
										webix.message(json.error);
									}
									
									
								});	
							
						}
					},
					click:function(){								
						this.callEvent("onClick");							
											
					},
				},
				{ 
					view:"button", shortcut:"esc", label:"Hủy bỏ",align:"center",
					width:100,
					on:{
						onClick: function(){
							this.getTopParentView().close();
						}
					},
					click:function(){
						this.callEvent("onClick");
					}
				},
				{}
			];			
			
			/* el.push({
				margin:10,
				cols:cols
			}); */

			$$("window-form").addView({margin:10,cols:cols},$$("window-form").getChildViews().length);
			$$("window-form").$view.style.borderWidth = 0;
			//webix.ui(el, $$("window-form"));

			win.getHead().setHTML(windowconfig.data[0].window_name);
		},
		focusFirstElement:function(){
			var form = $$("window-form");
			form.focus(form.elements[0]);
		},	
		setEditMode: function(mode) {
			editmode = mode;
		},		
		setValues: function(item,winconfig,params){

			var form = webix.$$("window-form");
			var vc_window = winconfig['data'][0];

			if (item == null){
				var tabs = vc_window.Tabs;
				
				item = {};

				if (params!=null){
					item = params;
				}

                for(var i=0;i<tabs.length;i++){
                    var tab = tabs[i];
                    if (tab.tab_type == "Normal" || tab.tab_type == "Master"){
                        var fields=tab.Fields;

                        for(var j=0;j<fields.length;j++){
							var field = fields[j];
							
							if (field.column_name == "ma_dvcs"){
								item[field.column_name] = user.getDvcsID();
							}

							                            
                            if (field.default_value!=null){
                                var defaultValue = field.default_value.toString();

                                if (defaultValue.startsWith("@Default=")){
                                    item[field.column_name] = defaultValue.replace("@Default=","");
								}
								
								if (defaultValue.startsWith("@JS=")){
									var fn = Function(defaultValue.replace("@JS=",""));
									item[field.column_name] = fn();
								}
                            }
                        }
                    }
				}
				
				form.setValues(item);
			}
			else {

				form.setValues(item);	
				
				var tabs = vc_window.Tabs;

				for(var i=0;i<tabs.length;i++){
					var tab = tabs[i];

					if (tab.tab_type != "Detail")
					{
						var fields=tab.Fields;

						for(var j=0;j<fields.length;j++){
							var field = fields[j];
							
							if (field.column_type == "date" || field.column_type == "datetime"){
								item[field.column_name] = removeTimeZoneT(item[field.column_name])
								form.setValues(item);
							}														
							
						}							

						break;
					}				
					
				}

				if (vc_window.window_type == "MasterDetail"){
					

					for(var i=0;i<tabs.length;i++){
						var tab = tabs[i];

						if (tab.tab_type == "Master")
						{
							continue;
						}

						var table = webix.$$("edit_"+tab.tab_table);

						var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id="+ vc_window.window_id
										+"&id="+item.id + "&tab_table="+tab.tab_table;
						table.clearAll();
						table.load(url);
						
					}
				}
				
			}
		},
		copyValues: function(item,winconfig){

			var form = webix.$$("window-form");
			var vc_window = winconfig['data'][0];
							

			if (vc_window.window_type == "MasterDetail"){
				var tabs = vc_window.Tabs;

				for(var i=0;i<tabs.length;i++){
					var tab = tabs[i];

					if (tab.tab_type == "Master")
					{
						continue;
					}

					var table = webix.$$("edit_"+tab.tab_table);

					var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id="+ vc_window.window_id
									+"&id="+item.id + "&tab_table="+tab.tab_table;
					table.clearAll();
					table.load(url);
					
				}
			}

			var tabs = vc_window.Tabs;

			for(var i=0;i<tabs.length;i++){
				var tab = tabs[i];
				if (tab.tab_type == "Normal" || tab.tab_type == "Master"){
					var fields=tab.Fields;

					for(var j=0;j<fields.length;j++){
						var field = fields[j];

						if (field.column_name == tab.tab_table + "_id"){
							item[field.column_name] = null;
						}
						
						if (field.column_name == "ma_dvcs"){
							item[field.column_name] = user.getDvcsID();
						}

													
						if (field.default_value!=null){
							var defaultValue = field.default_value.toString();

							if (defaultValue.startsWith("@Default=")){
								item[field.column_name] = defaultValue.replace("@Default=","");
							}
							
							if (defaultValue.startsWith("@JS=")){
								var fn = Function(defaultValue.replace("@JS=",""));
								item[field.column_name] = fn();
							}
						}
					}
				}
			}

			form.setValues(item);
			
		}

	};

});