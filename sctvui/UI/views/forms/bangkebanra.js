define([
    "app",
    "locale",
    "models/user"
], function (app, _, user) {
    var editmode = 0;

    var date = new Date();
    var _month = date.getMonth() + 1;
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    function subviewT() {
        $$('cbQuy').define('label', "Tháng");
        $$('cbQuy').refresh();
        $$('cbQuy').define('options', ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]);
        //webix.extend(config, $$('cbQuy').config);
        //webix.ui(config, $$('cbThang'));
        //$$('cbThang').destructor();
    }

    function subviewQ() {

        $$('cbQuy').define('label', "Quý");
        $$('cbQuy').refresh();
        $$('cbQuy').define('options', ["1", "2", "3", "4"]);
        // webix.extend($$('cbQuy').config, config, true);
        //webix.ui(config, $$('cbQuy'));
        //$$('cbQuy').destructor();
    }

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            head: "Bảng kê bán ra",
            width: 550,
            resize: true,
            body: {
                paddingY: 10,
                paddingX: 10,
                elementsConfig: { labelWidth: 60 },
                view: "form",
                id: "bangkebr",
                complexData: true,
                margin: 0,
                elements: [
					{
                        view: "multicombo",
                        label: _("DONVI"),
                        name: "lst_branch_code",
                        labelWidth: 80,
                        suggest: {
                            selectAll: true,
                            body: {
                                url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00198"                                
                            }
                        }                        
                    },
                    {
                        cols: [
                            {
                                view: "radio",
                                id: "head_extension",
                                value: 1,
                                options: [
                                    { "id": 1, "value": "Tháng" }, // the initially selected item
                                    { "id": 2, "value": "Quý" }
                                ],
                                on: {
                                    onChange: function (newv, oldv) {
                                        var d = new Date();
                                        var quarter = Math.floor((d.getMonth() / 3));
                                        var quarter1 = quarter + 1;
                                        if (newv == 2) {
                                            subviewQ();
                                            $$("cbQuy").setValue(quarter1);
                                        }
                                        if (newv == 1) {
                                            subviewT();
                                            $$("cbQuy").setValue(d.getMonth() + 1);
                                        }
                                    }
                                }
                            },
                            {
                                id: "cbQuy",
                                name: "quy",
                                view: "combo",
                                label: "Tháng",
                                value: _month,
                                options: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
                                on: {
                                    onChange: function () {
                                        var d = new Date();
                                        var quarter = Math.floor((d.getMonth() / 3));
                                        var quarter1 = quarter + 1;

                                        var option = $$('head_extension').getValue();
                                        if (option == 2) {
                                            var nam = d.getFullYear();
                                            var thang;
                                            var quy = $$("cbQuy").getValue();
                                            if (quy == 1) {
                                                thang = 0;
                                            }
                                            if (quy == 2) {
                                                thang = 3;
                                            }
                                            if (quy == 3) {
                                                thang = 6
                                            }
                                            if (quy == 4) {
                                                thang = 9
                                            }
                                            var firstDate = new Date(nam, thang, 1);
                                            var lastDate = new Date(nam, thang + 3, 0);

                                            $$("tu_ngay").setValue(firstDate);
                                            $$("tu_ngay").refresh();
                                            $$("den_ngay").setValue(lastDate);
                                            $$("den_ngay").refresh();
                                        }
                                        if (option == 1) {
                                            var year = d.getFullYear();
                                            var month = $$("cbQuy").getValue();
                                            var date1 = month + "/01" + "/" + year;

                                            var date = new Date(date1);
                                            var firstDay1 = new Date(date.getFullYear(), date.getMonth(), 1);

                                            var lastDay1 = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                                            $$("tu_ngay").setValue(firstDay1);
                                            $$("tu_ngay").refresh();
                                            $$("den_ngay").setValue(lastDay1);
                                            $$("den_ngay").refresh();
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    {
                        view: "datepicker", label: _("TU_NGAY"), name: "tu_ngay", id: "tu_ngay", value: firstDay, labelWidth: 80
                    },
                    {
                        view: "datepicker", label: _("DEN_NGAY"), name: "den_ngay", id: "den_ngay", value: lastDay, labelWidth: 80
                    },
                    {
                        view: "multicombo",
                        label: _("KY_HIEU"),
                        name: "ky_hieu",
                        labelWidth: 80,
                        // required: true,
                        // invalidMessage: "Tên truy cập không để trống hoặc nhập không đúng (a-z,A-Z,0-9)",
                        // validate: webix.rules.isNotEmpty,
                        suggest: {
                            selectAll: true,
                            body: {
                                url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00050",
                                //template: webix.template("#value#")
                            }
                        }
                        //value: "2"
                    },
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                shortcut: "F10", view: "button", label: "Nhận (F10)", width: 100, type: "form", align: "center",
                                on: {
                                    onClick: function () {
                                        var form = this.getParentView().getParentView();
                                        var win = this.getTopParentView();

                                        var item = form.getValues();
                                        var load_event = "loadBangkebanra";

                                        app.callEvent(load_event, [item]);

                                        win.close();
                                    }
                                },
                                click: function () {
                                    this.callEvent("onClick");

                                }
                            },
                            {
                                view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", width: 100, align: "center", type: "danger",
                                click: function () {
                                    this.getTopParentView().close();
                                }
                            },
                            {}
                        ]
                    }
                ]
            }

        },

        setValues: function (item) {
            var win = $$(this.$ui.id);
            var form = win.getChildViews()[1];

            form.setValues(item);
        }
    };

});
