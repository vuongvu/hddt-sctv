define([
	"app",
	"locale",
	"models/user"
],function(app,_,user){
	var editmode = 0;	
    var tables = [];

	var windowno = app.path[1].params[0];
    var col=[];
	var rw =[];					
	var el = [];
	var head_name=[];
	var head;
	var url = app.config.host + "/System/GetAllByWindowNo?window_id=" + windowno;

	function setParentValue(windowId){

		var win = $$(windowId);
		var form = win.getChildViews()[1];							
		var item = form.getValues();
		var valueField = win.config.valueField;

		if (win.config.editorId == null){
			var table = $$(win.config.tableId);
			var selectedItem = table.getSelectedItem();

			selectedItem[win.config.columnName] = item[valueField];			

			if (win.config.fieldExpression!=null){
				var fields = win.config.fieldExpression.split(';');

				for(var i=0;i<fields.length;i++){
					var field = fields[i].replace("{","").replace("}","");
					var array = field.split(":");

					selectedItem[array[1]] = item[array[0]];
				}
			}

			table.updateItem(selectedItem.id,selectedItem);

		}
		else {
			var editor = $$(win.config.editorId);
						

			editor.setValue(item[valueField]);

			if (win.config.fieldExpression!=null){
				var formParent = $$(win.config.formId);
				var parentItem = formParent.getValues(); 

				var fields = win.config.fieldExpression.split(';');

				for(var i=0;i<fields.length;i++){
					var field = fields[i].replace("{","").replace("}","");
					var array = field.split(":");

					parentItem[array[1]] = item[array[0]];
				}

				formParent.setValues(parentItem);
			}
		}
		

	}
		    
	return {		
		$ui:{
			view:"window", modal:true, id:"editwindow-win", position:"center",
			//width:800,
			head:"Thông tin khách hàng",
			//fullscreen:true,			
			body:{
                paddingY:10, 
                paddingX:10, 
                view:"form", 
				id:"editwindow-form", 
				complexData:true,
				margin:0,
                elements:el				
			}			
		},	
		initUI: function(windowconfig) {
			
			var rp = windowconfig;
			var max_row = rp['data'][0]['max_row'];
			var vc_window = rp["data"][0];
			var tabs = vc_window.Tabs;

			var win = $$(vc_window.code);

			if (typeof win.showProgress === "undefined")
				webix.extend(win, webix.ProgressBar);
			
			el = [];
			col = [] ;			

			if (vc_window.width==null && vc_window.height==null){
				win.config.fullscreen = true;				
			}

			if (vc_window.after_save!=null){
				win.config.after_save = vc_window.after_save;
			}

			for(var i=1;i<=max_row;i++){
				el.push({ cols:[]});
			}

			var fields = rp['data'][0]['Tabs'][0]['Fields'];

			for(var i=0;i<fields.length;i++){
				
				var field = fields[i];				
				
				if (field.hidden == true){
					continue;
				}

				var row = field.row;

				var item = {
					view: field.type_editor, 
					name: field.column_name,
					label: _(field.column_name), 
					id: field.column_name+"_"+rp['data'][0]['Tabs'][0]["tab_table"],
					width: field.width,
					labelPosition: field.label_position,
					labelWidth: field.label_width,
					format: field.format,
					css:field.css,
					value_change: field.value_change,
					on:{
						
					}
				};

				if (field.caption!=null){
					item.label = _(field.caption);
				}

				if (field.readonly == "C"){
					item.readonly = true;
				}
				
				if (item.view == "combo"){
					item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id
				}						

				if (item.view == "checkbox"){
					if (field.label_position == "right"){
						item.labelRight = _(field.column_name) ;
						item.label = "";							
					}

					if (field.column_type == "char" || field.column_type == "nvarchar" || field.column_type == "nchar" ){
						item.checkValue = "C";
						item.uncheckValue = "K";
					}

					if (field.COLUMN_TYPE == "int"){
						item.checkValue = "1";
						item.uncheckValue = "0";
					}

					if (field.COLUMN_TYPE == "bit"){
						item.checkValue = true;
						item.uncheckValue = false;
					}
				}
				
				if (item.view == "autonumeric"){
					item.inputAlign ="right";
				}

				if (item.view == "treesuggest"){
					
					var listColumns = JSON.parse(field.list_column);

					for(var v=0;v<listColumns.length;v++){
						listColumns[v].header =_(listColumns[v].id);
					}
					
					item.view = "combo";
					item.options = {
						view: "treesuggest",
						url: {
							$proxy:true,
							load:function(view, callback, params){
									var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

									webix.ajax(url, function(text){
										var json = JSON.parse(text);
										var data = listToTree(json);
										view.parse(data,"json");
									});
									
							}
						},
						body: {
							columns: listColumns,
							refid: field.ref_id,
							filterMode:{
								level:false,
								showSubItems:false
							}						
						}
					};

					item.on.onKeyPress = function(code, e){
						if (e.code == "F4"){
							alert("Moi");
						}
					};
				}

				if (item.view == "gridcombo"){
					
					var listColumns = JSON.parse(field.list_column);

					for(var v=0;v<listColumns.length;v++){
						listColumns[v].header =_(listColumns[v].id);
					}
					
					item.view = "combo";
					item.options = {
						view: "gridsuggest",
						url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
						body: {
							columns: listColumns,
							refid: field.ref_id												
						}
					};

					item.on.onKeyPress = function(code, e){
						if (e.code == "F4"){
							alert("Moi");
						}
					};
				}

				if (item.view == "gridsuggest"){
					var listColumns = JSON.parse(field.list_column);

					for(var v=0;v<listColumns.length;v++){
						listColumns[v].header =_(listColumns[v].id);
					}

					item.view = "text";
					item.suggest = {
						view: "gridsuggest",
						value_suggest: field.value_suggest,
						body:{
							columns:listColumns,
							refid: field.ref_id,
							datatype:"json",
							dataFeed: function(filtervalue, filter){
								if(filtervalue.length<3) return;

								var refid = this.config.refid;
								var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
								var urldata = "filtervalue="+encodeURIComponent(filtervalue);
								
								this.load(pathUrl+"&"+urldata, this.config.datatype);
							},
							
						},
						on: {
							onValueSuggest: function(obj){
								if (this.config.value_suggest!=null){								
									var fn = Function("obj","id",this.config.value_suggest);
									fn(obj,this.config.id);
								}
							}							
						}
					};

					item.on.onKeyPress = function(code, e){
						
					};
				}

				if (field.valid_rule!=null){
					var rule = field.valid_rule;

					if (rule == "isNotEmpty"){
						item.required = true;
						item.invalidMessage = item.label + " không được bỏ trống";
						item.validate = webix.rules.isNotEmpty;
					}

					if (rule == "isSpecialCharacter"){
						item.required = true;
						item.invalidMessage = item.label + " không được bỏ trống (a-z,A-Z,0-9)";
						item.validate = webix.rules.isSpecialCharacter;
					}

					if (rule == "isEmail"){
						item.invalidMessage = item.label + " không hợp lệ";
						item.validate = webix.rules.isEmail;
					}

					if (rule == "isTaxCode"){
						item.invalidMessage = item.label + " không hợp lệ";
						item.validate = webix.rules.isTaxCode;
					}
				}

				if (item.value_change!=null || item.css!=null || item.validate!=null){
					item.on.onChange = function(newv,oldv){

							if (this.config.validate!=null && editmode!=0 && oldv!=undefined && newv!= undefined){
								this.validate();
						    }

						   if (this.config.css!=null){
							   if (this.config.css == "mcls_uppercase" && newv !== undefined && oldv!== undefined ){
								   if (newv.toUpperCase()!=oldv.toUpperCase()){
									 webix.$$(this.config.id).setValue(newv.toUpperCase());
								   }
									
							   }
						   }

							if (this.config.value_change!=null){
								
								var fn = Function("newv","oldv","id",this.config.value_change);
								fn(newv,oldv,this.config.id);
							}
						}
				}
				

				el[row-1].cols.push(item);
								
			}
			
			if (vc_window.window_type == "MasterDetail"){

				var cells = [];

                for(var i=0;i<tabs.length;i++){
                    var tab = tabs[i];

                    if (tab.tab_type == "Master"){
                        continue;
                    }

                    var subCols = [];

                    for(var j=0;j<tab.Fields.length;j++){
                        var field = tab.Fields[j];

                        var subCol = {
                            id: field.column_name, 
							header: field.caption!=null ? _(field.caption) : _(field.column_name),
							editor: field.type_editor,
							hidden: field.hidden,
							value_change: field.value_change
						};
						

                        if (field.column_type == "date" || field.column_type == "datetime" ){
                            subCol.format = function(text) {
                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                text = removeTimeZoneT(text);                            
                                return format(text);
                            };
						}

						if (field.column_type == "decimal"){
							subCol.css = { "text-align": "right" };
							subCol.format = webix.Number.numToStr({
												groupDelimiter:",",
												groupSize:3,
												decimalDelimiter:".",
												decimalSize:0
											});
						}
						
						
                        
                        if (field.column_width == null){
                            subCol.fillspace = true;
                        }
                        else {
                            subCol.width = field.column_width;
						}

						if (field.type_editor == "combo"){
							subCol.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id
						}

						if (field.type_editor == "datepicker"){
							subCol.editor = "date";
						}
						
						if (field.type_editor == "treesuggest"){
							var listColumns = JSON.parse(field.list_column);

							for(var v=0;v<listColumns.length;v++){
								listColumns[v].header =_(listColumns[v].id);
							}
							
							var popup = webix.ui({
								view:"treesuggest",								
								body:{
									columns: listColumns,
									refid: field.ref_id,
									url: {
										$proxy:true,
										load:function(view, callback, params){
												var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

												webix.ajax(url, function(text){
													var json = JSON.parse(text);
													var data = listToTree(json);
													view.parse(data,"json");
												});
												
										}
									},
									filterMode:{
										level:false,
										showSubItems:false
									}	
								}
							});											

							subCol.popup = popup;
							subCol.editor = "combo";
							
						}

						if (field.type_editor == "gridcombo"){
							var listColumns = JSON.parse(field.list_column);

							for(var v=0;v<listColumns.length;v++){
								listColumns[v].header =_(listColumns[v].id);
							}
							
							var popup = webix.ui({
								view:"gridsuggest",								
								body:{
									columns: listColumns,
									refid: field.ref_id,
									url: app.config.host + "/System/GetDataByReferencesId?id=" + field.REF_ID								
								}
							});											

							subCol.popup = popup;
							subCol.editor = "combo";
							
						}

						if (field.type_editor == "gridsuggest"){
							var listColumns = JSON.parse(field.list_column);

							for(var v=0;v<listColumns.length;v++){
								listColumns[v].header =_(listColumns[v].id);
							}

							subCol.editor = "text";
							subCol.suggest = {
								view: "gridsuggest",
								value_suggest: field.value_suggest,
								body:{
									columns:listColumns,
									refid: field.ref_id,									
									datatype:"json",
									dataFeed: function(filtervalue, filter){
										if(filtervalue.length<3) return;

										var refid = this.config.refid;
										var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
										var urldata = "filtervalue="+encodeURIComponent(filtervalue);
										
										this.load(pathUrl+"&"+urldata, this.config.datatype);
									}
								},
								on: {
									onValueSuggest: function(obj){
										if (this.config.value_suggest!=null){								
											var fn = Function("obj","id",this.config.value_suggest);
											fn(obj,this.config.id);
										}
									}
									
								}
							}

							
						}
                        
                        subCols.push(subCol);
                    }

                    if (tab.tab_type=="Detail"){
                        var item = {
                            header: tab.name,
                            select: "cell",
                            body: {
								rows:[
									{
										view:"toolbar",
										height:25,
										cols:[
											{
												view:"icon",
												icon:"plus",
												on: {
													onItemClick: function(id,e){
														var parentView = this.getParentView().getParentView();
														var childViews = parentView.getChildViews();

														for(var i=0;i<childViews.length;i++){
															var childview = childViews[i];

															if (childview.config.view=="datatable"){
																var table = childview;

																var item = {
																	id: webix.uid()
																};

																var vc_window = windowconfig['data'][0];																
																var tabs = vc_window.Tabs;															

																for(var i=0;i<tabs.length;i++){
																	var tab = tabs[i];
																	if (tab.tab_type == "Detail"){
																		var fields=tab.Fields;

																		for(var j=0;j<fields.length;j++){
																			var field = fields[j];

																			if (field.column_name == "ma_dvcs"){
																				item[field.column_name] = user.getDvcsID();
																			}

																			
																			
																			if (field.default_value!=null){
																				var defaultValue = field.DEFAULT_VALUE.toString();

																				if (defaultValue.startsWith("@Default=")){
																					item[field.column_name] = defaultValue.replace("@Default=","");
																				}
																				
																				if (defaultValue.startsWith("@JS=")){
																					var fn = Function(defaultValue.replace("@JS=",""));
																					item[field.column_name] = fn();
																				}
																			}
																		}
																	}
																}								
																

																table.add(item);

																break;
															}
														}
														
													}
												}
											},
											{
												view:"icon",
												icon:"minus",
												on: {
													onItemClick: function(id,e){
														var parentView = this.getParentView().getParentView();
														var childViews = parentView.getChildViews();

														for(var i=0;i<childViews.length;i++){
															var childview = childViews[i];

															if (childview.config.view=="datatable"){
																var table = childview;

																table.editStop();

																var item = table.getSelectedItem();
												
																if (!webix.isUndefined(item)){
																	table.remove(item.id);
																	table.clearSelection();
																	return;
																}

																var editor = table.getEditState();

																if (!webix.isUndefined(editor)){
																	table.remove(editor.row);
																	table.clearSelection();
																	return;
																}

																break;
															}
														}
														
													}
												}
											}
										]
									},
									{
										view: "datatable",
										id:"edit_"+tab.table_name,
										tab_table: tab.table_name,
										tab_id: tab.code,
										editable:true,
										select: "cell",
										select_change: tab.select_change,
										columns: subCols,
										on:{
											onAfterEditStop:function(state, editor, ignore){												
												if (editor.config.value_change!=null){
													var selectedItem = null;

													if (typeof editor.getPopup === "function") { 
														var popup = editor.getPopup();
													
														if (popup!=null){
															var list = popup.getList();

															if (list!=null){
																selectedItem = list.getSelectedItem();
															}
														}
													}
													
													var fn = Function("newv","oldv","id","selectedItem",editor.config.value_change);
													fn(state.value,state.old,editor.config.id,selectedItem);
												}

											},
											onKeyPress: function(code,e){
												var table = this;

												if (e.key == "F4"){
													var editor = table.getEditState();

													if (!webix.isUndefined(editor)){
														alert("test")
														return;
													}
												}
											},
											onSelectChange:function(){
												var selectedItem = this.getSelectedId();
												
												if (this.config.select_change!=null){
													var fn = Function("selectedItem",this.config.select_change);
													fn(selectedItem);
												}
											}
										}
									}
								]
                                
                            }
                        };

                        cells.push(item);
                    }
				}
				
                var subview = {
                    borderless:true,
                    view:"tabview",
                    scrollX:true,
					cells: cells,
					tabbar:{
						width:200
					}					
				};

				if (vc_window.width!=null || vc_window.height!=null){
					subview.height = 250;
				}

				//el.push(subview);
				
				if (vc_window.tab_row!=null){
					el[vc_window.tab_row-1]=subview;
				}
				else {
					el.push(subview);
				}
				
            }
			
			var cols = [
				{},
				{ 
					shortcut:"F10",  
					view:"button", label:"Nhận (F10)", type:"form", align:"center", 
					width:100,
					on: {
						onClick: function() {
							var parent = this.getTopParentView();
							var form = parent.getChildViews()[1];

							if (form.validate()==false){
								return;
							}
							
							var item = form.getValues();
							
							var khData={};

							khData.windowid=String(rp['data'][0]['code']);
							khData.editmode=editmode;
							khData.data=[];

							var vc_window = rp['data'][0];

							if (vc_window.window_type == "MasterDetail"){
								
								var tabs = vc_window.Tabs;
								var details = [];

								for(var i=0;i<tabs.length;i++){
									var tab = tabs[i];

									if (tab.tab_type == "Detail"){
										var table = webix.$$("edit_"+tab.table_name);
										
										details.push({
											tab_id: tab.code,
											tab_table: tab.table_name,
											data: table.serialize()
										});
									}
								}

								item["details"] = details;
							}							
							
							khData.data.push(item);
							
							this.getTopParentView().disable();
							this.getTopParentView().showProgress();
							
							webix.ajax()
								.headers({'Content-type':'application/json'})
								.post( app.config.host + "/System/Save", khData, function(text){									
							
									var json = JSON.parse(text);

									$$(json.windowid).enable();
									$$(json.windowid).hideProgress();
												
									if (!json.hasOwnProperty("error")){
										var after_save = $$(json.windowid).config.after_save;

										if (after_save!=null){
											var fn = Function("editmode","data",after_save);
											fn(editmode,json.data);
										}
										
										setParentValue(json.windowid)

										$$(json.windowid).close();										
									}
									else {
										webix.message(json.error);
									}
									
									
								});	
							
						}
					},
					click:function(){								
						this.callEvent("onClick");							
											
					},
				},
				{ 
					view:"button", shortcut:"esc", label:"Hủy bỏ (ESC)", type:"danger", align:"center",
					width:100,
					on:{
						onClick: function(){
							this.getTopParentView().close();
						}
					},
					click:function(){
						this.callEvent("onClick");
					}
				},
				{}
			];			
			
			el.push({
				margin:10,
				cols:cols
			});
			
			webix.ui(el, $$("editwindow-form"));
			
			win.getHead().setHTML(windowconfig.data[0].name);
		},		
		setEditMode: function(mode) {
			editmode = mode;
		},		
		setValues: function(item,winconfig){

			var form = webix.$$("editwindow-form");
			var vc_window = winconfig['data'][0];

			if (item == null){
				var tabs = vc_window.Tabs;
				
				item = {};

                for(var i=0;i<tabs.length;i++){
                    var tab = tabs[i];
                    if (tab.tab_type == "Normal" || tab.tab_type == "Master"){
                        var fields=tab.Fields;

                        for(var j=0;j<fields.length;j++){
							var field = fields[j];
							
							if (field.column_name == "ma_dvcs"){
								item[field.column_name] = user.getDvcsID();
							}

							                            
                            if (field.default_value!=null){
                                var defaultValue = field.default_value.toString();

                                if (defaultValue.startsWith("@Default=")){
                                    item[field.column_name] = defaultValue.replace("@Default=","");
								}
								
								if (defaultValue.startsWith("@JS=")){
									var fn = Function(defaultValue.replace("@JS=",""));
									item[field.column_name] = fn();
								}
                            }
                        }
                    }
				}
				
				form.setValues(item);
			}
			else {

				form.setValues(item);				

				if (vc_window.window_type == "MasterDetail"){
					var tabs = vc_window.Tabs;

					for(var i=0;i<tabs.length;i++){
						var tab = tabs[i];

						if (tab.tab_type == "Master")
						{
							continue;
						}

						var table = webix.$$("edit_"+tab.tab_table);

						var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id="+ vc_window.window_id
										+"&id="+item.id + "&tab_table="+tab.tab_table;
						table.clearAll();
						table.load(url);
						
					}
				}
			}
		}		
	};

});