define(["app", "locale"], function (app, _) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "dmtab-win", position: "center",
			head: "Thông tin bản quyền",
			body: {

				paddingY: 10,
				paddingX: 10,
				elementsConfig: {
					labelWidth: 60,
					labelPosition: "top"
				},
				view: "form",
				id: "license-form",
				complexData: true,
				margin: 0,
				borderless: true,
				elements: [
					{
						view: "text",
						name: "ms_thue",
						label: _("MS_THUE"),
						width: 450,
						readonly: true,
						css: "mcls_readonly_text"
					},
					{
						view: "text",
						name: "ten_dvcs",
						label: _("TEN_DON_VI"),
						readonly: true,
						css: "mcls_readonly_text"
					},
					{
						view: "autonumeric",
						name: "sl_hd",
						label: _("SL_HD_DK"),
						readonly: true,
						css: "mcls_readonly_text"
					},
					{
						view: "autonumeric",
						name: "sl_dk_them",
						label: _("SL_HD_THEM"),
						required: true,
						invalidMessage: "Số lượng đăng ký thêm không được bằng 0 !",
						validate: webix.rules.isNotEmpty
					},
					{
						view: "text",
						name: "key_license",
						label: _("KEY_LICENSE"),
						readonly: true,
						inputAlign: "center",
						css: "mcls_license_text"
					},
					{
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", id: "btnSave", view: "button", label: "Nhận (F10)", type: "form", align: "center", width: 120,
								on: {
									onClick: function () {
										var form = webix.$$("license-form");
										var item = form.getValues();
										if (item.sl_dk_them == 0 || item.sl_dk_them == "" || item.sl_dk_them == null) {
											webix.message("Số lượng đăng ký thêm không được để trống hoặc bằng 0 !", "error");
											return false;
										}

										var url = app.config.host + "/License/Register";

										webix.extend(form, webix.ProgressBar);
										form.disable();
										form.showProgress();

										webix.ajax()
											.bind(this)
											.headers({ 'Content-type': 'application/json' })
											.post(url, item, function (text) {
												var result = JSON.parse(text);

												if (!result.hasOwnProperty("error")) {
													var interVal = setInterval(function () {

														var url = app.config.host + "/License/CheckLicense?id=" + result.id;

														webix.ajax(url, {
															error: function (text, data, XmlHttpRequest) {
																webix.message(text);

																var form = webix.$$("license-form");
																form.enable();
																form.hideProgress();

																clearInterval(interVal);

															},
															success: function (text, data, XmlHttpRequest) {
																var result = JSON.parse(text);

																if (!result.hasOwnProperty("error")) {

																	var form = webix.$$("license-form");

																	var values = form.getValues();
																	values.key_license = result.key_license;
																	values.sl_dk_them = 0;
																	values.sl_hd = result.sl_hd;

																	form.setValues(values);

																	form.enable();
																	form.hideProgress();

																	clearInterval(interVal);
																}
															}
														});

													}, 3000);
												}
												else {
													webix.message(result.error);

													var form = webix.$$("license-form");
													form.enable();
													form.hideProgress();

												}
											});
									}
								},
								click: function () {
									this.callEvent("onClick");

								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)",type: "danger", align: "center", width: 120,
								click: function () {
									webix.$$("dmtab-win").close();
								}
							},
							{}
						]
					}

				]
			}
		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});