define(["app"], function (app) {

	return {
		$ui: {
			view: "window", modal: true, id: "listuser-win", position: "center",
			head: "Đổi mật khẩu",
			body: {
				paddingY: 10,
				paddingX: 10,
				//elementsConfig:{labelWidth: 120}, 
				view: "form",
				id: "listuser-form",
				complexData: true,
				elements: [
					{ view: "text", name: "oldpass", required: true, invalidMessage: "Mật khẩu cũ không được bỏ trống", type: "password", label: "Mật khẩu cũ", labelPosition: "top" },
					{ view: "text", name: "newpass", required: true, invalidMessage: "Mật khẩu mới không được bỏ trống", type: "password", label: "Mật khẩu mới", labelPosition: "top" },
					{ view: "text", name: "confirmpass", required: true, invalidMessage: "Xác nhận mật khẩu không được bỏ trống", type: "password", label: "Xác nhận mật khẩu", labelPosition: "top" },
					{
						margin: 10,
						cols: [
							{},
							{
								view: "button", label: "Lưu (F10)", hotkey: "F10", type: "form", align: "center", width: 120, click: function () {

									var form = webix.$$("listuser-form");

									if (form.validate() == false) {
										return;
									}

									var item = form.getValues();

									if (item.newpass != item.confirmpass) {
										webix.message("Mật khẩu mới không giống nhau", "error");
										return;
									}

									webix.ajax()
										.post(app.config.host + "/Account/ChangePass", item, function (text) {
											var data = JSON.parse(text);

											if (!data.hasOwnProperty("error")) {
												webix.$$("listuser-win").close();
												webix.message("Đổi mật khẩu thành công !", "success");
											}
											else {
												webix.message(data.error, "error");
											}
										});
								}
							},
							{
								view: "button", label: "Hủy bỏ (ESC)", hotkey: "esc", type: "danger", align: "center", width: 120, click: function () {
									webix.$$("listuser-win").close();
								}
							}
						]
					}

				]
			}
		}
	};

});