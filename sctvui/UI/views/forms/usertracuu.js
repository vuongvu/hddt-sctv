define([
    "app",
    "models/user",
    "views/menus/version"
], function (app, user, version) {
    var editmode = 0;
    function initUI(item) {
        debugger;
        var form = $$("usertracuu-win");

        webix.extend(form, webix.ProgressBar);
        form.disable();
        form.showProgress();

        var mst = version.getMST();
        var schema = version.GetSchema();
        //console.log(schema);
        item.mst = mst;
        // item.mst = "0100686209";
        item.schema_name = schema;
        webix.ajax()
            .headers({ 'content-type': 'application/json' })
            .post(app.config.host + "/Invoice/GetUserTracuu", item, function (result) {
                form.enable();
                form.hideProgress();
                var data = JSON.parse(result);
                if (!data.hasOwnProperty("error")) {
                    var btnEdit = webix.$$("btnSavve");
                    btnEdit.define("label", "Sửa");
                    btnEdit.refresh();
                    data.ma_dt = data.code_customer;
                    data.mst = data.tax_code;
                    $$("usertracuu-form").setValues(data);
                }
                else {
                    webix.message(data.error, "debug");
                    $$("schema_name").setValue(item.schema_name);
                    $$("mst").setValue(item.mst);
                    // $$("mst").setValue("0100686209");
                    $$("email").setValue(item.email);
                    $$("username").setValue(item.code.replace(/[^a-zA-Z0-9 ]/g, ''));
                    $$("ma_dt").setValue(item.code);
                }
            });

    }
    return {
        $ui: {
            view: "window", modal: true, id: "usertracuu-win", position: "center", move: true, resize: true,
            head: "Thông tin khách hàng hỗ trợ",
            body: {
                paddingY: 10,
                paddingX: 10,
                //elementsConfig: { labelWidth: 100 },
                view: "form",
                width: 500,
                // height: 150,
                id: "usertracuu-form",
                complexData: true,
                margin: 0,
                elements: [
                    {
                        view: "text",
                        label: "Chema name: ",
                        labelWidth: 100,
                        name: "schema_name",
                        id: "schema_name",
                        readonly: true,
                        hidden: true,
                        css: "mcls_readonly_text"
                    },
                    {
                        view: "text",
                        label: "Mã số thuế: ",
                        labelWidth: 80,
                        name: "mst",
                        id: "mst",
                        readonly: true,
                        css: "mcls_readonly_text",
                        hidden:true
                    },
                    {
                        view: "text",
                        label: "Mã KH : ",
                        labelWidth: 100,
                        name: "ma_dt",
                        id: "ma_dt",
                        readonly: true,
                        css: "mcls_readonly_text"
                    },
                    {
                        view: "text",
                        label: "Username ",
                        labelWidth: 100,
                        name: "username",
                        id: "username",
                        // readonly: true,
                        readonly: true,
                        css: "mcls_readonly_text",
                        required: true,
                        invalidMessage: "Tên truy cập không để trống hoặc nhập không đúng (a-z,A-Z,0-9)",
                        validate: webix.rules.isSpecialCharacter,
                    },
                    {
                        view: "text",
                        label: "Password mới:",
                        labelWidth: 100,
                        name: "password",
                        id: "password",
                        //value: "minvoice",
                        required: true,
                        invalidMessage: "Password không được để trống",
                    },
                    {
                        view: "text",
                        label: "Email: ",
                        labelWidth: 100,
                        name: "email",
                        id: "email"
                    },
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                view: "button", id: "btnSavve", type: "image", image: "assets/imgs/save.png", label: "Lưu", width: 60, click: function () {

                                    var form = webix.$$("usertracuu-form");
                                    var item = form.getValues();

                                    if ($$("usertracuu-form").validate()) {
                                        webix.extend(form, webix.ProgressBar);
                                        form.disable();
                                        form.showProgress();
                                        webix.ajax()
                                            .headers({ 'Content-type': 'application/json' })
                                            .post(app.config.host + "/Invoice/CreateUser_tracuu", item, function (text) {
                                                var data = JSON.parse(text);

                                                if (!data.hasOwnProperty("error")) {
                                                    webix.message(data.ok, "success");
                                                    webix.$$("usertracuu-win").close();
                                                }
                                                else {
                                                    webix.message(data.error, "error");
                                                }
                                                form.enable();
                                                form.hideProgress();
                                            });
                                    }
                                }
                            },
                            {
                                view: "button", type: "image", image: "assets/imgs/reset.png", label: "Reset Password", width: 130, click: function () {
                                    var form = webix.$$("usertracuu-form");
                                    var item = form.getValues();

                                    webix.confirm({
                                        text: "Bạn có chắc chắn muốn reset mật khẩu về mật khẩu mặc định ?", ok: "Có", cancel: "Không",
                                        callback: function (res) {
                                            if (res) {
                                                webix.extend(form, webix.ProgressBar);
                                                form.disable();
                                                form.showProgress();
                                                webix.ajax()
                                                    .headers({ 'Content-type': 'application/json' })
                                                    .post(app.config.host + "/Invoice/ResetPassUserTracuu", item, function (text) {
                                                        var data = JSON.parse(text);

                                                        if (!data.hasOwnProperty("error")) {
                                                            webix.message(data.ok, "success");
                                                            webix.$$("usertracuu-win").close();
                                                        }
                                                        else {
                                                            webix.message(data.error, "error");
                                                        }
                                                        form.enable();
                                                        form.hideProgress();
                                                    });
                                            }
                                        }
                                    });
                                }
                            },
                            {
                                view: "button", id: "btnXoa", type: "image", image: "assets/imgs/xoa.png", label: "Xóa", width: 60, click: function () {
                                    var form = webix.$$("usertracuu-form");
                                    var item = form.getValues();
                                    webix.confirm({
                                        text: "Bạn có chắc chắn muốn xóa tài khoản ?", ok: "Có", cancel: "Không",
                                        callback: function (res) {
                                            if (res) {
                                                webix.ajax()
                                                    .headers({ 'Content-type': 'application/json' })
                                                    .post(app.config.host + "/Invoice/DeleteUserTracuu", item, function (text) {
                                                        var data = JSON.parse(text);

                                                        if (!data.hasOwnProperty("error")) {
                                                            webix.message(data.ok, "success");
                                                            webix.$$("usertracuu-win").close();
                                                        }
                                                        else {
                                                            webix.message(data.error, "error");
                                                        }
                                                    });
                                            }
                                        }
                                    });
                                }
                            },
                            {
                                view: "button", type: "image", image: "assets/imgs/close.png", label: "Hủy bỏ", hotkey: "esc", width: 80, click: function () {
                                    webix.$$("usertracuu-win").close();
                                }
                            },
                            {}
                        ]
                    }

                ]
            }
        },
        initUI: initUI,
        setEditMode: function (mode) {
            editmode = mode;
        },
        setValues: function (item) {
            var form = webix.$$("usertracuu-form");
            // var table = webix.$$("discountTable");

            form.setValues(item);
            // table.parse(item.hp_hsgiamtrus, "json");

        }
    };
});