﻿﻿define(["app", "locale"], function (app, _) {
    var editmode = 0;

    return {
        $ui: {
            view: "window",
            modal: true,
            id: "accountHSM-win",
            position: "center",
            head: "Người sử dụng",
            width: 500,
            height: 480,
            body: {
                paddingY: 10,
                paddingX: 10,
                elementsConfig: { labelWidth: 60 },
                view: "form",
                id: "accountHSM-form",
                complexData: true,
                margin: 0,
                elements: [
                    {
                        view: "text",
                        name: "username",
                        labelPosition: "top",
                        required: true,
                        label: "Tài khoản",
                        id: "accountHSM-username",
                        invalidMessage: "Tên truy cập không để trống hoặc nhập không đúng (a-z,A-Z,0-9)",
                        validate: webix.rules.isSpecialCharacter,
                        css: "mcls_uppercase",
                        on: {
                            onChange: function (newv, oldv) {
                                if (newv != undefined) {
                                    if (oldv != undefined) {
                                        if (newv.toUpperCase() != oldv.toUpperCase()) {
                                            webix.$$(this.config.id).setValue(newv.toUpperCase());
                                        }
                                    } else {
                                        webix.$$(this.config.id).setValue(newv.toUpperCase());
                                    }
                                }
                            },
                        },
                    },
                    {
                        view: "text",
                        name: "ma_dvcs",
                        required: true,
                        labelPosition: "top",
                        label: "Mã đơn vị",
                        id: "accountHSM-ma_dvcs",
                        invalidMessage: "Mã đơn vị không được để trống",
                        validate: webix.rules.isSpecialCharacter,
                    },
                    {
                        view: "text",
                        type: "password",
                        name: "password",
                        required: true,
                        labelPosition: "top",
                        label: "Mật khẩu",
                        id: "accountHSM-password",
                        invalidMessage: "Mật khẩu không được để trống",
                    },
                    {
                        view: "text",
                        type: "password",
                        name: "password_level2",
                        required: true,
                        labelPosition: "top",
                        label: "Mật khẩu cấp 2",
                        id: "accountHSM-password_level2",
                        //validate: webix.rules.isEmail,
                        invalidMessage: "Mật khẩu cấp 2 không được để trống",
                    },

                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                shortcut: "F10",
                                view: "button",
                                label: "Lưu (F10)",
                                type: "form",
                                align: "center",
                                on: {
                                    onClick: function () {
                                        var form = webix.$$("accountHSM-form");
                                        var table = webix.$$("dmwindowData");
                                        var itemData = table.getSelectedItem();
                                        if (form.validate() == false) {
                                            return;
                                        }

                                        var item = form.getValues();
                                        item.wb_user_id = itemData.wb_user_id;
                                        item.window_type = "account";

                                        var url = app.config.host + "/Account/Update_HSM";
                                        webix.extend(form, webix.ProgressBar);
                                        form.disable();
                                        form.showProgress();
                                        webix
                                            .ajax()
                                            .headers({ "Content-type": "application/json" })
                                            .post(url, item, function (text) {
                                                var data = JSON.parse(text);

                                                form.enable();
                                                form.hideProgress();
                                                if (!data.hasOwnProperty("error")) {
                                                    item.id = data.id;
                                                    webix.$$("accountHSM-win").close();
                                                    webix.message("Lưu thành công", "success");
                                                    // app.callEvent("dataChanged", [editmode, data.item]);
                                                } else {
                                                    webix.message({ type: "error", text: data.error });
                                                }
                                            });
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },

                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy (ESC)",
                                type: "danger",
                                align: "center",
                                click: function () {
                                    webix.$$("accountHSM-win").close();
                                },
                            },
                            {
                                id: "huyUserHSM",
                                shortcut: "F8",
                                view: "button",
                                label: "Xóa (F8)",
                                type: "delete",
                                align: "center",
                                hidden: true,
                                on: {
                                    onClick: function () {
                                        webix.confirm({
                                            text: "Bạn chắc chắn muốn xóa tài khoản HSM?",
                                            ok: "Có",
                                            cancel: "Không",
                                            callback: function (res) {
                                                if (res) {
                                                    var form = webix.$$("accountHSM-form");
                                                    var table = webix.$$("dmwindowData");
                                                    var itemData = table.getSelectedItem();

                                                    var item = { wb_user_id: itemData.wb_user_id };

                                                    var url = app.config.host + "/Account/Delete_HSM";
                                                    webix.extend(form, webix.ProgressBar);
                                                    form.disable();
                                                    form.showProgress();
                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(url, item, function (text) {
                                                            var data = JSON.parse(text);

                                                            form.enable();
                                                            form.hideProgress();
                                                            if (!data.hasOwnProperty("error")) {
                                                                item.id = data.id;
                                                                webix.$$("accountHSM-win").close();
                                                                webix.message("Xóa thành công", "success");
                                                                // app.callEvent("dataChanged", [editmode, data.item]);
                                                            } else {
                                                                webix.message({ type: "error", text: data.error });
                                                            }
                                                        });
                                                }
                                            },
                                        });
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },
                            {},
                        ],
                    },
                ],
            },
        },
        // setEditMode: function (mode) {
        // 	editmode = mode;
        // }
    };
});
