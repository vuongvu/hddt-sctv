define(["app", "locale"], function (app, _) {
    var editmode = 0;
    // {
    //     "paddingY":3, "paddingX":3, "view":"form", "id":"window-form", "complexData":true, "margin":0, "borderless":true, "elements":
    //     [{
    //         "cols": [
    //             {
    //             "view": "fieldset", "label": "Thông tin chung", "body": {
    //                 "rows": [{
    //                     "cols":
    //                     [{ "id": "MA_DT0_DPKT", "view": "text", "label": "Mã đối tượng", "labelWidth": 100, "width": 200, "name": "MA_DT0" }, { "id": "TEN_DT0_DPKT", "view": "text", "label": "Tên đối tượng", "labelWidth": 110, "name": "TEN_DT0" }]
    //                 }, { "id": "ONG_BA_DPKT", "view": "text", "label": "Ông bà", "labelWidth": 100, "name": "ONG_BA" }, { "id": "CT_GOC_DPKT", "view": "text", "label": "Chứng từ gốc", "labelWidth": 100, "name": "CT_GOC" }, { "id": "DIA_CHI_DPKT", "view": "text", "label": "Địa chỉ", "labelWidth": 100, "name": "DIA_CHI" }, { "id": "DIEN_GIAI_DPKT", "view": "text", "label": "Diễn giải", "labelWidth": 100, "name": "DIEN_GIAI" }]
    //             }
    //         }, 
    //         { "width": 10 },
    //          {
    //             "view": "fieldset", "label": "Chứng từ", "body": {
    //                 "rows":
    //                 [
    //                     { "id": "NGAY_CT_DPKT", "view": "datepicker", "label": "Ngày chứng từ", "labelWidth": 110, "width": 300, "name": "NGAY_CT" }, { "id": "SO_CT_DPKT", "view": "text", "labelWidth": 110, "label": "Số chứng từ", "name": "SO_CT" }, 
    //                     { "cols": [{ "id": "MA_NT_DPKT", "view": "text", "name": "MA_NT", "label": "Mã ngoại tệ", "labelWidth": 110, "width": 170 }, { "id": "TY_GIA_DPKT", "view": "text", "name": "TY_GIA", "label": "Tỷ giá", "labelWidth": 50 }] }, { "id": "MA_HT_DPKT", "view": "text", "labelWidth": 110, "label": "Mã hạch toán", "name": "MA_HT" }, { "id": "MA_BP_DPKT", "view": "text", "labelWidth": 110, "label": "Mã bộ phận", "name": "MA_BP" }]
    //             }
    //         }]
    //     },
    //     {
    //         "view": "tabview", "id": "tab_replace", "cells": [{ "header": "Chi tiết thu" },
    //         { "header": "Chi tiết thuế" }]
    //     }, {
    //         "cols": [{ "id": "T_TIEN_TT_DPKT", "view": "text", "label": "Tổng tiền trước thuế", "labelWidth": 170, "name": "T_TIEN_TT" },
    //         { "id": "T_THUE_DPKT", "view": "text", "label": "Tổng tiền thuế", "labelWidth": 130, "name": "T_THUE" }, { "id": "T_TIEN_DPKT", "view": "text", "label": "Tổng tiền", "labelWidth": 100, "name": "T_TIEN" }]
    //     },
    //     {
    //         "cols": [{ "id": "T_TIEN_TT_NT_DPKT", "view": "text", "label": "Tổng tiền trước thuế NT", "labelWidth": 170, "name": "T_TIEN_TT_NT" },
    //         { "id": "T_THUE_NT_DPKT", "view": "text", "label": "Tổng tiền thuế NT", "labelWidth": 130, "name": "T_THUE_NT" },
    //         { "id": "T_TIEN_NT_DPKT", "view": "text", "label": "Tổng tiền NT", "labelWidth": 100, "name": "T_TIEN_NT" }]
    //     }]
    // }
    return {
        $ui: {
            view: "window", modal: true, id: "window-win", position: "center",
            //width:800,
            head: "Nam đại ka",
            fullscreen: true,
            body: {
                paddingY: 10,
                paddingX: 10,
                view: "form",
                id: "window-form",
                complexData: true,
                keepViews: true,
                margin: 0,
                elements: [
                    {
                        cols: [
                            {
                                view: "fieldset",
                                id: "2",
                                label: "Field Set 1",
                                body: {
                                    rows: [
                                        {
                                            column_type: "datetime",
                                            css: null,
                                            data_width: null,
                                            fieldExpression: null,
                                            format: "%d/%m/%Y",
                                            id: "inv_invoiceIssuedDate_inv_InvoiceAuth",
                                            keepViews: true,
                                            label: "Ngày hóa đơn",
                                            labelPosition: "left",
                                            labelWidth: 100,
                                            name: "inv_invoiceIssuedDate",
                                            on: {},
                                            resizeColumn: true,
                                            value_change: null,
                                            view: "datepicker",
                                            width: null
                                        },
                                        {
                                            column_type: "nvarchar",
                                            css: "mcls_readonly_text",
                                            data_width: null,
                                            fieldExpression: null,
                                            format: null,
                                            id: "inv_invoiceNumber_inv_InvoiceAuth",
                                            keepViews: true,
                                            label: "Số hóa đơn",
                                            labelPosition: "left",
                                            labelWidth: 100,
                                            name: "inv_invoiceNumber",
                                            on: {},
                                            readonly: true,
                                            resizeColumn: true,
                                            value_change: null,
                                            view: "text",
                                            width: null
                                        }
                                    ]
                                }
                            },
                            {
                                view: "fieldset",
                                id: "22",
                                label: "Thông tin 2",
                                body: {
                                    rows: [
                                        {
                                            column_type: "datetime",
                                            css: null,
                                            data_width: null,
                                            fieldExpression: null,
                                            format: "%d/%m/%Y",
                                            id: "inv_invoiceIssuedDate_inv_InvoiceAuth1",
                                            keepViews: true,
                                            label: "Ngày hóa đơn",
                                            labelPosition: "left",
                                            labelWidth: 100,
                                            name: "inv_invoiceIssuedDate4",
                                            on: {},
                                            resizeColumn: true,
                                            value_change: null,
                                            view: "datepicker",
                                            width: null
                                        },
                                        {
                                            column_type: "nvarchar",
                                            css: "mcls_readonly_text",
                                            data_width: null,
                                            fieldExpression: null,
                                            format: null,
                                            id: "inv_invoiceNumber_inv_InvoiceAuth1",
                                            keepViews: true,
                                            label: "Số hóa đơn",
                                            labelPosition: "left",
                                            labelWidth: 100,
                                            name: "inv_invoiceNumber5",
                                            on: {},
                                            readonly: true,
                                            resizeColumn: true,
                                            value_change: null,
                                            view: "text",
                                            width: null
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        cols: [
                            { view: "text", label: "Email1" },
                            { view: "text", label: "ahihi", id: "lala" }
                        ]

                    }

                ]
            }
        },
        setEditMode: function (mode) {
            editmode = mode;
        }
    };

});