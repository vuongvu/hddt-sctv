define(["app", "models/user"], function (app, user) {
    var editmode = 0;

    var table = {
        id: "hoadon",
        view: "datatable",
        select: true,
        resizeColumn: true,
        footer: false,
        editable: false,
        tooltip: true,
        scheme: {},
        on: {
            "data->onStoreUpdated": function () {
                this.data.each(function (obj, i) {
                    obj.stt = i + 1;
                });
            },
        },
        columns: [
            {
                id: "stt",
                name: "stt",
                header: "STT",
                width: 40,
            },
            {
                id: "khieu",
                name: "khieu",
                header: "Ký hiệu mẫu hóa đơn và ký hiệu hóa đơn",
                width: 200,
            },
            {
                id: "shdon",
                name: "shdon",
                header: "Số hóa đơn điện tử",
                width: 120,
            },
            {
                id: "tdlap",
                name: "tdlap",
                header: "Ngày lập hóa đơn",
                width: 120,
                format: webix.i18n.dateFormatStr,
                editor: "date",
            },
            {
                id: "ladhddt",
                name: "ladhddt",
                header: "Loại áp dụng hóa đơn điện tử",
                template: function (obj) {
                    switch (obj.ladhddt) {
                        case 1:
                            return "<span>HĐĐT Nghị đinh số 123/2020/NĐ-CP</span> ";
                        case 2:
                            return "<span>HĐĐT có mã của cơ quan thu</span> ";
                        case 3:
                            return "<span>Các loại hóa đơn theo Nghị đinh 51/2010</span> ";
                        case 4:
                            return "<span>Hóa đơn đặt in theo Nghị định số 123/2020/NĐ-C</span> ";
                        default:
                            break;
                    }
                },
                width: 180,
            },
            {
                id: "tctbao",
                name: "tctbao",
                header: "Hủy/Giải trình",
                width: 180,
            },
            {
                id: "ldo",
                name: "ldo",
                header: "Lý do",
                width: 180,
            },
        ],
        pager: "pagerA",
    };

    var buttons = {
        cols: [
            {},
            {
                shortcut: "F8",
                view: "button",
                type: "icon",
                icon: "save",
                label: "Lưu tạm (F8)",
                width: 120,
                align: "center",
                on: {
                    onClick: function () {},
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
            {
                shortcut: "F9",
                view: "button",
                type: "icon",
                icon: "signal",
                label: "Ký chờ xử lý (F9)",
                width: 150,
                align: "center",
                on: {
                    onClick: function () {},
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
            {
                shortcut: "F10",
                type: "icon",
                icon: "paper-plane",
                view: "button",
                label: "Ký và gửi CQT (F10)",
                width: 150,
                align: "center",
                on: {
                    onClick: function () {},
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
            {
                shortcut: "F11",
                view: "button",
                label: "Kết xuất mẫu 04 (F11)",
                type: "icon",
                icon: "file-export",
                width: 150,
                align: "center",
                on: {
                    onClick: function () {},
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
            {
                view: "button",
                hotkey: "esc",
                label: "Hủy bỏ (ESC)",
                width: 120,
                type: "icon",
                icon: "ban",
                align: "center",
                click: function () {
                    webix.$$("huyhoadon-win").close();
                },
            },
            {},
        ],
    };

    var ui = {
        id: "previewView",
        rows: [
            {
                cols: [
                    {
                        height: 600,
                        view: "template",
                        id: "pdfViewInfoOld",
                    },
                    {
                        height: 600,
                        view: "template",
                        id: "pdfViewInfoNew",
                    },
                ],
            },
            buttons,
        ],
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },

        setValues: function (item) {},
    };
});
