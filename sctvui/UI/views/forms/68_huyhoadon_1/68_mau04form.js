define(["app", "models/user"], function (app, user) {
    var controls = {
        cols: [
            {
                view: "icon",
                icon: "plus",
                on: {
                    onItemClick: function (id, e) {
                        var table = webix.$$("danhsachhoadonhuy");
                        table.add({});
                    },
                },
            },
            {
                view: "icon",
                icon: "minus",
                on: {
                    onItemClick: function (id, e) {
                        var table = webix.$$("danhsachhoadonhuy");
                        table.editStop();
                        var item = table.getSelectedItem();
                        if (!webix.isUndefined(item)) {
                            table.remove(item.id);
                            table.clearSelection();
                            return;
                        }
                        var editor = table.getEditState();
                        if (!webix.isUndefined(editor)) {
                            table.remove(editor.row);
                            table.clearSelection();
                            return;
                        }
                    },
                },
            },
        ],
    };

    var controls2 = {
        cols: [
            {},
            {
                view: "button",
                type: "icon",
                type: "form",
                label: "Lưu và Chuyển bước 2 (F10)",
                shortcut: "F10",
                width: 200,
                align: "center",
                on: {
                    onClick: function () {
                        $$("tabbar").setValue("formView");
                    },
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
            {
                view: "button",
                type: "icon",
                type: "danger",
                label: "Thoát (ESC)",
                hotkey: "esc",
                width: 150,
                align: "center",
                on: {
                    onClick: function () {
                        webix.$$("huyhoadon-win").close();
                    },
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
            {},
        ],
    };

    var table = {
        id: "danhsachhoadonhuy",
        view: "datatable",
        select: true,
        resizeColumn: true,
        footer: false,
        editable: true,
        tooltip: true,
        height: 200,
        scheme: {},
        on: {
            "data->onStoreUpdated": function () {
                this.data.each(function (obj, i) {
                    obj.stt = i + 1;
                });
            },
        },
        columns: [
            {
                id: "stt",
                name: "stt",
                header: "STT",
                width: 40,
            },
            {
                id: "khieu",
                name: "khieu",
                header: "Ký hiệu mẫu hóa đơn và ký hiệu hóa đơn",
                width: 200,
            },
            {
                id: "shdon",
                name: "shdon",
                header: "Số hóa đơn điện tử",
                width: 120,
            },
            {
                id: "tdlap",
                name: "tdlap",
                header: "Ngày lập hóa đơn",
                width: 120,
                format: webix.i18n.dateFormatStr,
                editor: "date",
            },
            {
                id: "ladhddt",
                name: "ladhddt",
                header: "Loại áp dụng hóa đơn điện tử",
                template: function (obj) {
                    switch (obj.ladhddt) {
                        case 1:
                            return "<span>HĐĐT Nghị đinh số 123/2020/NĐ-CP</span> ";
                        case 2:
                            return "<span>HĐĐT có mã của cơ quan thu</span> ";
                        case 3:
                            return "<span>Các loại hóa đơn theo Nghị đinh 51/2010</span> ";
                        case 4:
                            return "<span>Hóa đơn đặt in theo Nghị định số 123/2020/NĐ-C</span> ";
                        default:
                            break;
                    }
                },
                width: 180,
            },
            {
                id: "tctbao",
                name: "tctbao",
                header: "Hủy/Giải trình",
                width: 180,
            },
            {
                id: "ldo",
                name: "ldo",
                header: "Lý do",
                width: 180,
            },
        ],
        pager: "pagerA",
    };

    var pagination = {
        id: "pagerA",
        view: "pager",
        template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
        container: "paging_here",
        size: 50,
        group: 5,
    };

    var ui = {
        id: "infoView",
        margin: 0,
        rows: [
            controls,
            table,
            pagination,
            {
                height: 10,
            },
            controls2,
        ],
    };

    return {
        $ui: ui,
    };
});
