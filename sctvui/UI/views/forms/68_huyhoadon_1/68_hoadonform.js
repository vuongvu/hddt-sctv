define(["app", "models/user", "locale"], function (app, user, _) {
    var header = {
        view: "template",
        template: "Hóa đơn gia trị gia tăng (01GTKT)",
        type: "header",
    };

    var master = {
        cols: [
            {
                view: "fieldset",
                label: "Thông tin hóa đơn",
                body: {
                    rows: [
                        {
                            cols: [
                                {
                                    id: "tdlap",
                                    view: "datepicker",
                                    label: "Ngày hóa đơn",
                                    labelWidth: 100,
                                    name: "tdlap",
                                },
                                {
                                    id: "shdon",
                                    view: "text",
                                    label: "Số hóa đơn",
                                    labelWidth: 100,
                                    name: "shdon",
                                },
                            ],
                        },
                        {
                            cols: [
                                {
                                    id: "tmau",
                                    view: "text",
                                    label: "Mẫu hóa đơn",
                                    labelWidth: 100,
                                    name: "tmau",
                                },
                                {
                                    id: "khieu",
                                    view: "text",
                                    label: "Ký hiệu",
                                    labelWidth: 100,
                                    name: "khieu",
                                },
                            ],
                        },
                        {
                            cols: [
                                {
                                    id: "dvtte",
                                    view: "text",
                                    label: "Mã tiền tệ",
                                    labelWidth: 100,
                                    name: "dvtte",
                                },
                                {
                                    id: "tgia",
                                    view: "text",
                                    label: "Tỷ giá",
                                    labelWidth: 100,
                                    name: "tgia",
                                },
                            ],
                        },
                        {
                            cols: [
                                {
                                    id: "htttoan",
                                    view: "combo",
                                    label: "HT thanh toán",
                                    labelWidth: 100,
                                    name: "htttoan",
                                },
                                {
                                    id: "sdhang",
                                    view: "text",
                                    label: "Số đơn hàng",
                                    labelWidth: 100,
                                    name: "sdhang",
                                },
                            ],
                        },
                        {
                            id: "stknban",
                            view: "text",
                            label: "STK bên bán",
                            labelWidth: 100,
                            name: "stknban",
                        },
                    ],
                },
            },
            {
                width: 10,
            },
            {
                view: "fieldset",
                label: "Thông tin khách hàng",
                body: {
                    rows: [
                        {
                            cols: [
                                {
                                    id: "mst",
                                    view: "search",
                                    labelWidth: 110,
                                    label: "Mã số thuế",
                                    name: "mst",
                                },
                                {
                                    id: "mnmua",
                                    view: "gridsuggest",
                                    label: "Mã đối tượng",
                                    labelWidth: 110,
                                    width: 300,
                                    name: "mnmua",
                                },
                            ],
                        },
                        {
                            cols: [
                                {
                                    id: "tnmua",
                                    view: "text",
                                    name: "tnmua",
                                    label: "Tên người mua",
                                    labelWidth: 110,
                                },
                                {
                                    id: "email",
                                    view: "text",
                                    labelWidth: 110,
                                    label: "Email",
                                    name: "email",
                                },
                            ],
                        },
                        {
                            id: "ten",
                            view: "text",
                            name: "ten",
                            label: "Tên đơn vị",
                            labelWidth: 110,
                        },
                        {
                            id: "dchi",
                            view: "text",
                            labelWidth: 110,
                            label: "Địa chỉ",
                            name: "dchi",
                        },
                        {
                            id: "stknmua",
                            view: "text",
                            label: "STK người mua",
                            labelWidth: 110,
                            name: "stknmua",
                        },
                        {
                            id: "mccqthue",
                            view: "combo",
                            label: "Cơ quan thuế",
                            labelWidth: 110,
                            name: "mccqthue",
                        },
                    ],
                },
            },
        ],
    };

    var detail = {
        borderless: true,
        scrollX: true,
        tabbar: {
            width: 200,
        },
        view: "tabview",
        id: "tab_replace",
        cells: [
            {
                header: "Chi tiết hóa đơn (01GTKT)",
                body: {
                    rows: [
                        {
                            cols: [
                                {
                                    view: "icon",
                                    icon: "plus",
                                    on: {
                                        onItemClick: function (id, e) {
                                            var table = webix.$$("chitiethoadon");
                                            table.add({});
                                        },
                                    },
                                },
                                {
                                    view: "icon",
                                    icon: "minus",
                                    on: {
                                        onItemClick: function (id, e) {
                                            var table = webix.$$("chitiethoadon");
                                            table.editStop();
                                            var item = table.getSelectedItem();
                                            if (!webix.isUndefined(item)) {
                                                table.remove(item.id);
                                                table.clearSelection();
                                                return;
                                            }
                                            var editor = table.getEditState();
                                            if (!webix.isUndefined(editor)) {
                                                table.remove(editor.row);
                                                table.clearSelection();
                                                return;
                                            }
                                        },
                                    },
                                },
                            ],
                        },
                        {
                            view: "datatable",
                            id: "chitiethoadon",
                            height: 220,
                            name: "chitiethoadon",
                            resizeColumn: true,
                            footer: false,
                            editable: true,
                            tooltip: true,
                            scheme: {
                                $init: function (obj) {},
                            },
                            on: {
                                "data->onStoreUpdated": function () {
                                    this.data.each(function (obj, i) {
                                        obj.stt = i + 1;
                                    });
                                },
                            },
                            columns: [
                                {
                                    id: "cthdon_id",
                                    name: "cthdon_id",
                                    hidden: true,
                                },
                                {
                                    id: "hdon_id",
                                    name: "hdon_id",
                                    hidden: true,
                                },
                                {
                                    width: 120,
                                    id: "choose",
                                    header: "Chọn",
                                    editor: "checkbox",
                                    width: 90,
                                    template: function () {
                                        return "<input disabled class='webix_table_checkbox' type='checkbox' >";
                                    },
                                },
                                {
                                    id: "stt",
                                    header: "STT",
                                    width: 40,
                                },
                                {
                                    id: "ma",
                                    name: "ma",
                                    header: "Mã hàng",
                                    width: 150,
                                    editor: "combo",
                                    suggest: {
                                        view: "gridsuggest",
                                        body: {
                                            url: null,
                                            columns: [
                                                {
                                                    id: "ma",
                                                    header: "Mã hàng hóa",
                                                    width: 120,
                                                },
                                                {
                                                    id: "ten",
                                                    header: "Tên hàng hóa",
                                                    width: 250,
                                                },
                                            ],
                                            dataFeed: function (filtervalue, filter) {},
                                        },
                                        on: {
                                            onValueSuggest: function (obj) {},
                                        },
                                    },
                                },
                                {
                                    id: "ten",
                                    name: "Tên hàng hóa",
                                    header: "Tên hàng hóa",
                                    width: 250,
                                    editor: "text",
                                },
                                {
                                    id: "mdvtinh",
                                    name: "mdvtinh",
                                    header: "ĐVT",
                                    width: 150,
                                    editor: "combo",
                                    suggest: {
                                        view: "gridsuggest",
                                        body: {
                                            url: null,
                                            columns: [
                                                {
                                                    id: "ten",
                                                    header: "Tên ĐVT",
                                                    width: 150,
                                                },
                                            ],
                                            dataFeed: function (filtervalue, filter) {},
                                        },
                                        on: {
                                            onValueSuggest: function (obj) {},
                                        },
                                    },
                                },
                                {
                                    id: "sluong",
                                    name: "sluong",
                                    header: "Số lượng",
                                    width: 120,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "autonumeric",
                                },
                                {
                                    id: "dgia",
                                    name: "dgia",
                                    header: "Đơn giá",
                                    width: 120,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "autonumeric",
                                },

                                {
                                    id: "thtien",
                                    name: "thtien",
                                    header: "Tiền trước thuế",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "autonumeric",
                                },

                                {
                                    id: "tlckhau",
                                    name: "tlckhau",
                                    header: "% CK",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "autonumeric",
                                },

                                {
                                    id: "stckhau",
                                    name: "stckhau",
                                    header: "Tiền CK",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "autonumeric",
                                },

                                {
                                    id: "tsuat",
                                    name: "tsuat",
                                    header: "Thuế suất",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "combo",
                                    options: null,
                                },

                                {
                                    id: "tthue",
                                    name: "tthue",
                                    header: "Tiền thuế",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "autonumeric",
                                },

                                {
                                    id: "tgtien",
                                    name: "tgtien",
                                    header: "Tiền sau thuế",
                                    width: 150,
                                    format: webix.Number.numToStr({
                                        groupDelimiter: ",",
                                        groupSize: 3,
                                        decimalDelimiter: ".",
                                        decimalSize: 0,
                                    }),
                                    css: {
                                        "text-align": "right",
                                    },
                                    editor: "autonumeric",
                                },
                            ],
                        },
                    ],
                },
            },
        ],
    };

    var totalFooter = {
        rows: [
            {
                view: "treetable",
                autoheight: true,
                autowidth: true,
                columns: [
                    {
                        id: "value",
                        header: "Loại thuế",
                        width: 250,
                        template: "{common.icon()} #value#",
                    },
                    {
                        id: "ttthue_total",
                        header: "Thành tiền trước thuế",
                        width: 200,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: {
                            "text-align": "right",
                        },
                    },
                    {
                        id: "tckhau_total",
                        header: "Tiền chiết khấu",
                        width: 200,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: {
                            "text-align": "right",
                        },
                    },
                    {
                        id: "tthue_total",
                        header: "Tiền thuế",
                        width: 200,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: {
                            "text-align": "right",
                        },
                    },
                    {
                        id: "ttsthue_total",
                        header: "Tiền thuế",
                        width: 200,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: {
                            "text-align": "right",
                        },
                    },
                ],
                data: [
                    {
                        id: "1",
                        value: "Tổng cộng hàng hóa dịch vụ",
                        open: true,
                        ttthue_total: 3000000,
                        tckhau_total: 500000,
                        tthue_total: 100000,
                        ttsthue_total: 3100000,
                        data: [
                            {
                                id: "1.1",
                                value: "Thuế suất 0%",
                                ttthue_total: 1000000,
                                tckhau_total: 0,
                                tthue_total: 0,
                                ttsthue_total: 1000000,
                            },
                            {
                                id: "1.2",
                                value: "Thuế suất 5%",
                                ttthue_total: 1000000,
                                tckhau_total: 0,
                                tthue_total: 50000,
                                ttsthue_total: 1050000,
                            },
                            {
                                id: "1.3",
                                value: "Thuế suất 10%",
                                ttthue_total: 1000000,
                                tckhau_total: 500000,
                                tthue_total: 50000,
                                ttsthue_total: 1050000,
                            },
                            {
                                id: "1.4",
                                value: "Không chịu thuế GTGT",
                                ttthue_total: 1000000,
                                tckhau_total: 0,
                                tthue_total: 0,
                                ttsthue_total: 0,
                            },
                            {
                                id: "1.5",
                                value: "Không kê thai, tính nộp thuế GTGT",
                                ttthue_total: 0,
                                tckhau_total: 0,
                                tthue_total: 0,
                                ttsthue_total: 0,
                            },
                        ],
                    },
                    {
                        id: "2",
                        value: "Tổng cộng phí lệ phí",
                        open: true,
                        ttthue_total: 0,
                        tckhau_total: 0,
                        tthue_total: 0,
                        ttsthue_total: 0,
                    },
                    {
                        id: "3",
                        value: "Tổng cộng thanh toán",
                        open: true,
                        ttthue_total: 0,
                        tckhau_total: 0,
                        tthue_total: 0,
                        ttsthue_total: 0,
                    },
                    {
                        id: "4",
                        value: "Đọc tiền bằng chữ",
                        open: true,
                        ttthue_total: 0,
                        tckhau_total: 0,
                        tthue_total: 0,
                        ttsthue_total: 0,
                    },
                ],
            },
            {
                id: "amount_to_word_inv_invoiceauth",
                view: "text",
                label: "Đọc tiền bằng chữ",
                labelWidth: 150,
                name: "amount_to_word",
            },
        ],
    };

    var controls = {
        cols: [
            {
                view: "button",
                label: "Chuyển đến giai đoạn 3",
                width: 200,
                on: {
                    onClick: function () {
                        $$("tabbar").setValue("previewView");
                        var win = webix.$$("previewView");
                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();

                        var url = "http://localhost:18374/hopdong/printhopdong?id=b9496ff0-360d-4852-8153-4b3c54fbe167&type=pdf";
                        webix.ajax().get(url, function (response) {
                            win.enable();
                            win.hideProgress();
                            var file = new Blob([response], {
                                type: "application/pdf",
                            });
                            var fileURL = URL.createObjectURL(file);

                            var builder = '<iframe class="responsive-iframe" src="' + fileURL + '" frameborder="0" height="700px" width="100%"></iframe>';

                            $$("pdfViewInfo").setHTML(builder);
                        });
                    },
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
            {
                view: "button",
                label: "Quay lại",
                width: 120,
                on: {
                    onClick: function () {
                        $$("tabbar").setValue("infoView");
                    },
                },
                click: function () {
                    this.callEvent("onClick");
                },
            },
        ],
    };

    var ui = {
        view: "form",
        scroll: true,
        id: "formView",
        elements: [header, master, detail, totalFooter, controls],
    };
    return {
        $ui: {
            // paddingY: 10,
            // paddingX: 10,
            // elementsConfig: { labelWidth: 100 },
            view: "form",
            id: "huyhoadon_add",
            // complexData: true,
            scroll: true,
            margin: 0,
            on: {
                // onViewShow: function () {
                // }
            },
            elements: [
                {
                    view: "scrollview",
                    scroll: "y",
                    body: {
                        paddingX: 18,
                        rows: [
                            {
                                cols: [
                                    {
                                        view: "fieldset",
                                        label: "Thông tin hóa đơn",
                                        body: {
                                            rows: [
                                                {
                                                    cols: [
                                                        { view: "text", label: "cctbao_id", id: "cctbao_id", name: "cctbao_id", hidden: true },
                                                        {
                                                            view: "datepicker",
                                                            name: "tdlap",
                                                            label: "Ngày lập",
                                                            //labelPosition: "top",
                                                            id: "tdlap",
                                                            css: "mcls_readonly_text",
                                                            readonly: true,
                                                        },
                                                        {
                                                            view: "text",
                                                            name: "shdon",
                                                            id: "shdon",
                                                            label: "Số HĐ",
                                                            css: "mcls_readonly_text",
                                                            readonly: true,
                                                        },
                                                    ],
                                                },
                                                {
                                                    cols: [
                                                        {
                                                            view: "text",
                                                            label: _("Ký hiệu"),
                                                            name: "khieu",
                                                            css: "mcls_readonly_text",
                                                            readonly: true,
                                                            id: "khieu",
                                                            // type:"number",
                                                            attributes: { maxlength: 7 },
                                                        },
                                                        {
                                                            view: "text",
                                                            label: _("Số đơn hàng"),
                                                            name: "sdhang",
                                                            id: "sdhang",
                                                        },
                                                    ],
                                                },
                                                {
                                                    cols: [
                                                        {
                                                            view: "combo",
                                                            name: "dvtte",
                                                            label: "Mã tiền tệ",
                                                            //labelPosition: "top",
                                                            id: "dvtte",
                                                            required: true,
                                                            value: "1",
                                                            invalidMessage: "Trạng thái hóa đơn không thể bỏ trống !",
                                                            options: {
                                                                view: "gridsuggest",
                                                                value: "1",
                                                                template: "#value#",
                                                                url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00188",
                                                                body: {
                                                                    columns: [{ id: "value", header: _("Mã tiền tệ"), width: 220 }],
                                                                    dataFeed: function (filtervalue, filter) {
                                                                        if (filtervalue.length < 1) {
                                                                            this.load(this.config.url);
                                                                            return;
                                                                        }

                                                                        var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00188";
                                                                        var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                        this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                                    },
                                                                },
                                                            },
                                                        },
                                                        {
                                                            view: "text",
                                                            label: "Tỷ giá",
                                                            type: "number",
                                                            value: 1,
                                                            name: "tgia",
                                                            id: "tgia",
                                                            on: {
                                                                onChange: function (newv, oldv) {
                                                                    if (newv == undefined) return;
                                                                    if (newv == "") return;
                                                                    if (!webix.rules.isNumber(newv)) {
                                                                        webix.message("Tỷ giá phải nhập số", "error");
                                                                        this.focus();
                                                                        return;
                                                                    }
                                                                },
                                                            },
                                                        },
                                                    ],
                                                },
                                                {
                                                    view: "combo",
                                                    name: "htttoan",
                                                    label: "HT thanh toán",
                                                    //labelPosition: "top",
                                                    value: "đồng",
                                                    id: "htttoan",
                                                    options: {
                                                        view: "gridsuggest",
                                                        template: "#name#",
                                                        body: {
                                                            scroll: true,
                                                            autoheight: false,
                                                            columns: [
                                                                // { id: "id", header: _("MS_THUE"), width: 150 },
                                                                { id: "code", header: _("Mã ngoại tệ"), width: 150 },
                                                                { id: "name", header: _("Tên ngoại tệ"), width: 550 },
                                                            ],
                                                            dataFeed: function (filtervalue, filter) {
                                                                if (filtervalue.length < 1) return;

                                                                var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00252";
                                                                var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                            },
                                                        },
                                                        url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00252",
                                                    },
                                                    on: {},
                                                },
                                                {
                                                    view: "text",
                                                    label: _("STK người bán"),
                                                    id: "stknban",
                                                    name: "stknban",
                                                },
                                            ],
                                        },
                                    },
                                    {
                                        view: "fieldset",
                                        label: "Thông tin khách hàng",
                                        body: {
                                            rows: [
                                                {
                                                    cols: [
                                                        {
                                                            id: "mst",
                                                            name: "mst",
                                                            view: "search",
                                                            label: "Mã số thuế",
                                                            //align: "center",
                                                            validate: webix.rules.isTaxCode,
                                                            invalidMessage: "Mã số thuế không hợp lệ",
                                                            placeholder: _("SEARCH") + "...",
                                                            // width: 300,
                                                            on: {
                                                                onChange: function (newv, oldv) {
                                                                    if (newv == undefined) return;
                                                                    if (newv == "") return;
                                                                    if (!webix.rules.isTaxCode(newv)) {
                                                                        webix.message("Mã số thuế không hợp lệ", "error");
                                                                        this.focus();
                                                                        return;
                                                                    }
                                                                    // if (newv.length != 11) {
                                                                    //     webix.message({ type: "error", text: "Độ dài mẫu số phải là 11 ký tự" });
                                                                    //     this.focus();
                                                                    //     return;
                                                                    // }
                                                                },
                                                                onTimedKeyPress: function () {
                                                                    // var table = $$("tblSearch");
                                                                    // table.clearAll();
                                                                    // table.load(table.config.url);
                                                                },
                                                                onSearchIconClick: function (e) {
                                                                    var mst = this.getValue();
                                                                    // window.location.href = "http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst;
                                                                    window.open("http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst);
                                                                },
                                                            },
                                                        },
                                                        {
                                                            view: "text",
                                                            editor: "textsearch",
                                                            label: "Mã đối tượng",
                                                            id: "mnmua",
                                                            name: "mnmua",
                                                        },
                                                    ],
                                                },
                                                {
                                                    cols: [
                                                        { view: "text", label: _("Tên người mua"), name: "tnmua", id: "tnmua" },
                                                        { view: "text", label: _("Email"), name: "email", id: "email" },
                                                    ],
                                                },
                                                { view: "text", label: _("Tên đơn vị"), name: "ten", id: "ten" },
                                                { view: "text", label: _("Địa chỉ"), name: "dchi", id: "dchi" },
                                                { view: "text", label: _("STK người mua"), name: "stknmua", id: "stknmua" },
                                            ],
                                        },
                                    },
                                ],
                            },
                            {
                                borderless: true,
                                view: "tabview",
                                scrollX: true,
                                tabbar: {
                                    width: 200,
                                },
                                cells: [
                                    {
                                        header: "Chi tiết hóa đơn",
                                        select: "cell",
                                        body: {
                                            rows: [
                                                {
                                                    id: "mainInvoiceManage",
                                                    css: { "padding-bottom": "5px !important" },
                                                    // view: "toolbar",
                                                    // height: 18,
                                                    cols: [
                                                        {
                                                            view: "button",
                                                            type: "htmlbutton",
                                                            width: 65,
                                                            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F9)" + "</span>",
                                                            // id: "btnPrint",
                                                            // view: "button", type: "icon",
                                                            // //view: "icon",
                                                            id: "addrow",
                                                            // label: " (F9)",
                                                            // width: 60,
                                                            // icon: "fas fa-plus",
                                                            css: "btnForm",
                                                            //css: { "font-size": "15px" },
                                                            on: {
                                                                onItemClick: function (id, e) {
                                                                    var table = webix.$$("hoadon68_chitiet");
                                                                    var item = { id: webix.uid() };
                                                                    table.add(item);
                                                                    table.select(item.id, "ma", false);
                                                                    table.edit({ row: item.id, column: "ma" });
                                                                },
                                                            },
                                                        },
                                                        {
                                                            view: "button",
                                                            type: "htmlbutton",
                                                            width: 65,
                                                            label: '<i class="fa fa-minus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F8)" + "</span>",
                                                            // view: "button",
                                                            id: "removerow",
                                                            // type: "icon",
                                                            // //view: "icon",
                                                            // label: " (F8)",
                                                            // width: 60,
                                                            css: "btnForm",
                                                            // icon: "fas fa-minus",
                                                            on: {
                                                                onItemClick: function (id, e) {
                                                                    var table = webix.$$("hoadon68_chitiet");

                                                                    table.editStop();

                                                                    var item = table.getSelectedItem();

                                                                    if (!webix.isUndefined(item)) {
                                                                        table.remove(item.id);
                                                                        table.clearSelection();
                                                                        return;
                                                                    }

                                                                    var editor = table.getEditState();

                                                                    if (!webix.isUndefined(editor)) {
                                                                        table.remove(editor.row);
                                                                        table.clearSelection();
                                                                        return;
                                                                    }
                                                                },
                                                            },
                                                        },
                                                        {
                                                            view: "button",
                                                            type: "htmlbutton",
                                                            width: 65,
                                                            label: '<i class="fa fa-copy fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F7)" + "</span>",
                                                            view: "button",
                                                            id: "copyrow",
                                                            // type: "icon",
                                                            //view: "icon",
                                                            // label: " (F7)",
                                                            // width: 60,
                                                            // icon: "fas fa-copy",
                                                            css: "btnForm",
                                                            on: {
                                                                onItemClick: function (id, e) {
                                                                    var table = webix.$$("hoadon68_chitiet");

                                                                    table.editStop();

                                                                    var item = table.getSelectedItem();
                                                                    if (item == null || item == undefined) {
                                                                        webix.message("Bạn chưa chọn dòng cần copy !", "error");
                                                                        return;
                                                                    }
                                                                    //item.id = webix.uid();
                                                                    var copy = webix.copy(item);
                                                                    copy.id = webix.uid();
                                                                    //copy[tab.tab_table + "_id"] = null;

                                                                    table.add(copy);
                                                                    table.unselect(item.id);
                                                                    table.select(copy.id, "ma", false);
                                                                    table.edit({ row: copy.id, column: "ma" });
                                                                },
                                                            },
                                                        },
                                                        {
                                                            view: "checkbox",
                                                            id: "check_chietkhau",
                                                            value: 0,
                                                            labelRight: _("Chiết khấu"),
                                                            labelWidth: 2,
                                                            tooltip: "Tích vào đây nếu hóa đơn có chiết khấu",
                                                            on: {
                                                                onChange: function (newv, oldv) {
                                                                    if (newv == 1) {
                                                                        $$("hoadon68_chitiet").showColumn("stckhau");
                                                                        $$("hoadon68_chitiet").showColumn("tlckhau");
                                                                    } else {
                                                                        $$("hoadon68_chitiet").hideColumn("stckhau");
                                                                        $$("hoadon68_chitiet").hideColumn("tlckhau");
                                                                    }
                                                                },
                                                            },
                                                        },
                                                    ],
                                                },
                                                {
                                                    view: "datatable",
                                                    resizeColumn: true,
                                                    id: "hoadon68_chitiet",
                                                    // tab_table: tab.tab_table,
                                                    // tab_id: tab.tab_id,
                                                    editable: true,
                                                    select: "cell",
                                                    height: 260,
                                                    drag: true,
                                                    // select_change: tab.select_change,
                                                    // cell_editstop: tab.cell_editstop,
                                                    // after_addrow: tab.after_addrow,
                                                    // after_delrow: tab.after_delrow,
                                                    // oncheck: tab.oncheck,
                                                    columns: [
                                                        { id: "stt", header: "", width: 30 },
                                                        {
                                                            id: "ma",
                                                            header: _("Mã hàng hóa"),
                                                            editor: "textsearch",
                                                            width: 100,
                                                            editable: true,
                                                            suggest: {
                                                                view: "gridsuggest",
                                                                // value_suggest: field.value_suggest,
                                                                // fieldExpression: field.field_expression,
                                                                // displayField: field.display_field,
                                                                // tableId: "edit_inv_InvoiceDetail",
                                                                columnName: "ma_hanghoa",
                                                                body: {
                                                                    columns: [
                                                                        { id: "id", hidden: true, header: "id" },
                                                                        { id: "code", width: 120, header: "code" },
                                                                        { id: "name", width: 350, header: "name" },
                                                                    ],
                                                                    scroll: true,
                                                                    autoheight: false,
                                                                    datatype: "json",
                                                                    dataFeed: function (filtervalue, filter) {
                                                                        if (filtervalue.length < 1) return;

                                                                        var refid = this.config.refid;
                                                                        var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00005";
                                                                        var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                        this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                                    },
                                                                },
                                                                on: {
                                                                    onValueSuggest: function (obj) {
                                                                        debugger;
                                                                        var table = $$("hoadon68_chitiet");
                                                                        var selectedItem = table.getSelectedItem();
                                                                        selectedItem["ma"] = obj["code"];
                                                                        selectedItem["ten"] = obj["name"];
                                                                        selectedItem["mdvtinh"] = obj["unit_code"];
                                                                        selectedItem["dgia"] = obj["unit_price"];
                                                                        selectedItem["tsuat"] = obj["vat_type"];
                                                                        table.updateItem(selectedItem.id, selectedItem);
                                                                    },
                                                                },
                                                            },
                                                        },
                                                        {
                                                            id: "ten",
                                                            header: _("Tên hàng hóa"),
                                                            editor: "text",
                                                            width: 350,
                                                            editable: true,
                                                        },
                                                        {
                                                            id: "mdvtinh",
                                                            header: _("ĐVT"),
                                                            editor: "text",
                                                            width: 80,
                                                            editable: true,
                                                            suggest: {
                                                                view: "gridsuggest",
                                                                columnName: "name",
                                                                body: {
                                                                    columns: [
                                                                        { id: "id", hidden: true, header: "id" },
                                                                        { id: "name", width: 120, header: "name" },
                                                                    ],
                                                                    scroll: true,
                                                                    autoheight: false,
                                                                    datatype: "json",
                                                                    dataFeed: function (filtervalue, filter) {
                                                                        if (filtervalue.length < 1) return;

                                                                        var refid = this.config.refid;
                                                                        var pathUrl = app.config.host + "/System/GetDataReferencesByRefId?id=RF00035";
                                                                        var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                                                                        this.load(pathUrl + "&" + urldata, this.config.datatype);
                                                                    },
                                                                },
                                                                on: {
                                                                    onValueSuggest: function (obj) {
                                                                        var table = $$("hoadon68_chitiet");
                                                                        var selectedItem = table.getSelectedItem();
                                                                        selectedItem["mdvtinh"] = obj["code"];

                                                                        table.updateItem(selectedItem.id, selectedItem);
                                                                    },
                                                                },
                                                            },
                                                        },
                                                        {
                                                            id: "sluong",
                                                            header: _("Số lượng"),
                                                            editor: "autonumeric",
                                                            width: 100,
                                                            editable: true,
                                                            css: { "text-align": "right" },
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                        },
                                                        {
                                                            id: "dgia",
                                                            header: _("Đơn giá"),
                                                            width: 150,
                                                            editor: "autonumeric",
                                                            editable: true,
                                                            css: { "text-align": "right" },
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                            // value_change: field.value_change
                                                        },
                                                        {
                                                            id: "thtien",
                                                            header: _("Tiền trước thuế"),
                                                            width: 150,
                                                            editor: "autonumeric",
                                                            editable: true,
                                                            css: { "text-align": "right" },
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                            // value_change: field.value_change
                                                        },
                                                        {
                                                            id: "tlckhau",
                                                            header: _("% CK"),
                                                            width: 80,
                                                            editor: "autonumeric",
                                                            editable: true,
                                                            hidden: true,
                                                            css: { "text-align": "right" },
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                        },
                                                        {
                                                            id: "stckhau",
                                                            header: _("Tiền CK"),
                                                            width: 150,
                                                            editor: "autonumeric",
                                                            editable: true,
                                                            css: { "text-align": "right" },
                                                            hidden: true,
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                        },
                                                        {
                                                            id: "tsuat",
                                                            header: _("Thuế suất"),
                                                            width: 100,
                                                            //  editor: "combo",
                                                            editor: "autonumeric",
                                                            editable: true,
                                                            css: { "text-align": "right" },
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                            // on: {
                                                            //     onChange: function (newv, oldv) {
                                                            //     }
                                                            // },
                                                            // options: app.config.host + "/System/GetDataByReferencesId?id=D6FAEAFC-4EF2-4ABD-AB45-F3DCB4805740",
                                                            // optionslist: true
                                                        },
                                                        {
                                                            id: "tthue",
                                                            header: _("Tiền thuế"),
                                                            width: 150,
                                                            editor: "autonumeric",
                                                            editable: true,
                                                            css: { "text-align": "right" },
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                        },
                                                        {
                                                            id: "tgtien",
                                                            header: _("Tiền sau thuế"),
                                                            width: 150,
                                                            editor: "autonumeric",
                                                            editable: true,
                                                            css: { "text-align": "right" },
                                                            format: webix.Number.numToStr({
                                                                groupDelimiter: ".",
                                                                groupSize: 3,
                                                                decimalDelimiter: ",",
                                                                decimalSize: 2,
                                                            }),
                                                        },
                                                        {
                                                            id: "kmai",
                                                            header: _("Khuyến mại"),
                                                            width: 100,
                                                            editor: "checkbox",
                                                            editable: true,
                                                            template: "{common.checkbox()}",
                                                            checkValue: true,
                                                            uncheckValue: false,
                                                        },
                                                    ],
                                                    on: {
                                                        "data->onStoreUpdated": function () {
                                                            this.data.each(function (obj, i) {
                                                                obj.stt = i + 1;
                                                            });
                                                        },
                                                        onAfterLoad: function () {
                                                            var table = this;
                                                            var data = table.serialize();
                                                            data.forEach(function (entry) {
                                                                if (entry["stckhau"] != undefined || entry["tlckhau"] != undefined) {
                                                                    if (entry["stckhau"] != 0 || entry["tlckhau"] != 0) {
                                                                        var ck = $$("check_chietkhau");
                                                                        if (ck != undefined) {
                                                                            ck.setValue(1);
                                                                            return false;
                                                                            // $$("edit_" + tab.tab_table).showColumn("inv_discountAmount");
                                                                            // $$("edit_" + tab.tab_table).showColumn("inv_discountPercentage");
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        },
                                                        onCheck: function (row, column, state) {
                                                            var table = this;
                                                            if (column == "khuyen_mai") {
                                                                if (state == true) {
                                                                    // var selectedItem = table.getItem(row);
                                                                    // selectedItem.don_gia = null;
                                                                    // selectedItem.tien_tt = null;
                                                                    // selectedItem.pt_ck = null;
                                                                    // selectedItem.tien_ck = null;
                                                                    // selectedItem.pt_thue = null;
                                                                    // selectedItem.tien_thue = null;
                                                                    // selectedItem.tong_tien = null;
                                                                    // table.updateItem(selectedItem.id, selectedItem);
                                                                    // table.callEvent("onAfterAdd");
                                                                }
                                                            }
                                                        },
                                                        onAfterAdd: function (id, index) {
                                                            var table = $$(this.config.id);
                                                            var data = table.serialize();

                                                            var tong_tien_tt_dp = 0;
                                                            var tong_tien_thue_dp = 0;
                                                            var tong_tien_ck_dp = 0;
                                                            var tong_tien_dp = 0;
                                                            data.forEach(function (entry) {
                                                                tong_tien_tt_dp += entry["tien_tt"] == null ? 0 : entry["tien_tt"];
                                                                tong_tien_thue_dp += entry["tien_thue"] == null ? 0 : entry["tien_thue"];
                                                                tong_tien_ck_dp += entry["tien_ck"] == null ? 0 : entry["tien_ck"];
                                                                tong_tien_dp += entry["tong_tien"] == null ? 0 : entry["tong_tien"];

                                                                // $$("inv_Invoice_tien_tt").setValue(tong_tien_tt_dp);
                                                                // $$("inv_Invoice_tien_ck").setValue(tong_tien_ck_dp);
                                                                // $$("inv_Invoice_tien_thue").setValue(tong_tien_thue_dp);
                                                                // $$("inv_Invoice_tong_tien").setValue(tong_tien_dp);
                                                            });
                                                        },
                                                        onAfterEditStop: function (state, editor, ignore) {
                                                            var table = $$(this.config.id);
                                                            var selectedItem = table.getItem(editor.row);
                                                            var columnId = editor.config.id;
                                                            debugger;
                                                            var so_luong = webix.rules.isNumber(selectedItem.sluong) ? selectedItem.sluong : 0;
                                                            var don_gia = webix.rules.isNumber(selectedItem.dgia) ? selectedItem.dgia : 0;
                                                            var tien_tt = webix.rules.isNumber(selectedItem.thtien) ? selectedItem.thtien : 0;
                                                            var pt_ck = webix.rules.isNumber(selectedItem.tlckhau) ? selectedItem.tlckhau : 0;
                                                            var tien_ck = webix.rules.isNumber(selectedItem.stckhau) ? selectedItem.stckhau : 0;
                                                            var pt_thue = webix.rules.isNumber(selectedItem.tsuat) ? selectedItem.tsuat : 0;
                                                            var tien_thue = webix.rules.isNumber(selectedItem.tthue) ? selectedItem.tthue : 0;
                                                            var tong_tien = webix.rules.isNumber(selectedItem.tgtien) ? selectedItem.tgtien : 0;
                                                            if (
                                                                columnId == "tsuat" ||
                                                                columnId == "sluong" ||
                                                                columnId == "dgia" ||
                                                                columnId == "thtien" ||
                                                                columnId == "tgtien" ||
                                                                columnId == "tlckhau" ||
                                                                columnId == "stckhau"
                                                            ) {
                                                                if (don_gia < 0) {
                                                                    don_gia = 0;
                                                                }
                                                                if (so_luong < 0) {
                                                                    so_luong = 0;
                                                                }

                                                                if (don_gia > 0) {
                                                                    tien_tt = so_luong * don_gia;
                                                                }

                                                                if (pt_ck < 0) {
                                                                    pt_ck = 0;
                                                                }
                                                                if (pt_ck > 100) {
                                                                    pt_ck = 0;
                                                                }

                                                                tien_ck = (tien_tt * pt_ck) / 100;
                                                                if (tien_ck - tien_tt > 0) {
                                                                    tien_ck = 0;
                                                                }
                                                                tien_thue = ((tien_tt - tien_ck) * pt_thue) / 100;

                                                                tong_tien = Math.round(tien_tt - tien_ck + parseFloat(tien_thue));
                                                            }
                                                            selectedItem.sluong = so_luong;
                                                            selectedItem.dgia = don_gia;
                                                            selectedItem.tsuat = pt_thue;
                                                            selectedItem.thtien = tien_tt;
                                                            selectedItem.tlckhau = pt_ck;
                                                            selectedItem.stckhau = tien_ck;
                                                            selectedItem.tthue = tien_thue;
                                                            selectedItem.tgtien = tong_tien;
                                                            table.updateItem(selectedItem.id, selectedItem);

                                                            var data = table.serialize();
                                                            var tien_tt_all = 0;
                                                            var tien_thue_all = 0;
                                                            var tien_ck_all = 0;
                                                            var tong_tien_all = 0;

                                                            var tien_tt_10 = 0;
                                                            var tien_thue_10 = 0;
                                                            var tien_ck_10 = 0;
                                                            var tong_tien_10 = 0;

                                                            var tien_tt_5 = 0;
                                                            var tien_thue_5 = 0;
                                                            var tien_ck_5 = 0;
                                                            var tong_tien_5 = 0;

                                                            var tien_tt_0 = 0;
                                                            var tien_thue_0 = 0;
                                                            var tien_ck_0 = 0;
                                                            var tong_tien_0 = 0;

                                                            var tien_tt_kct = 0;
                                                            var tien_thue_kct = 0;
                                                            var tien_ck_kct = 0;
                                                            var tong_tien_kct = 0;

                                                            var tien_tt_kkk = 0;
                                                            var tien_thue_kkk = 0;
                                                            var tien_ck_kkk = 0;
                                                            var tong_tien_kkk = 0;

                                                            data.forEach(function (entry) {
                                                                tien_tt_all += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                tien_thue_all += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                tien_ck_all += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                tong_tien_all += entry["tgtien"] == null ? 0 : entry["tgtien"];

                                                                if (entry["tsuat"] == 10) {
                                                                    tien_tt_10 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                    tien_thue_10 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                    tien_ck_10 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                    tong_tien_10 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                }
                                                                if (entry["tsuat"] == 5) {
                                                                    tien_tt_5 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                    tien_thue_5 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                    tien_ck_5 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                    tong_tien_5 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                }
                                                                if (entry["tsuat"] == 0) {
                                                                    tien_tt_0 += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                    tien_thue_0 += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                    tien_ck_0 += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                    tong_tien_0 += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                }
                                                                if (entry["tsuat"] == -1) {
                                                                    tien_tt_kct += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                    tien_thue_kct += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                    tien_ck_kct += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                    tong_tien_kct += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                }
                                                                if (entry["tsuat"] == -2) {
                                                                    tien_tt_kkk += entry["thtien"] == null ? 0 : entry["thtien"];
                                                                    tien_thue_kkk += entry["tthue"] == null ? 0 : entry["tthue"];
                                                                    tien_ck_kkk += entry["stckhau"] == null ? 0 : entry["stckhau"];
                                                                    tong_tien_kkk += entry["tgtien"] == null ? 0 : entry["tgtien"];
                                                                }
                                                            });

                                                            $$("tgtcthue").setValue(tien_tt_all);
                                                            $$("tgtcthue10").setValue(tien_tt_10);
                                                            $$("tgtcthue5").setValue(tien_tt_5);
                                                            $$("tgtcthue0").setValue(tien_tt_0);
                                                            $$("tgtcthuekct").setValue(tien_tt_kct);
                                                            $$("tgtcthuekkk").setValue(tien_tt_kkk);

                                                            $$("ttcktmai").setValue(tien_ck_all);
                                                            $$("ttcktmai10").setValue(tien_ck_10);
                                                            $$("ttcktmai5").setValue(tien_ck_5);
                                                            $$("ttcktmai0").setValue(tien_ck_0);
                                                            $$("ttcktmaikct").setValue(tien_ck_kct);
                                                            $$("ttcktmaikkk").setValue(tien_ck_kkk);

                                                            $$("tgtthue").setValue(tien_thue_all);
                                                            $$("tgtthue10").setValue(tien_thue_10);
                                                            $$("tgtthue5").setValue(tien_thue_5);
                                                            $$("tgtthue0").setValue(tien_thue_0);
                                                            $$("tgtthuekct").setValue(tien_thue_kct);
                                                            $$("tgtthuekkk").setValue(tien_thue_kkk);

                                                            $$("tgtttbso").setValue(tong_tien_all);
                                                            $$("tgtttbso10").setValue(tong_tien_10);
                                                            $$("tgtttbso5").setValue(tong_tien_5);
                                                            $$("tgtttbso0").setValue(tong_tien_0);
                                                            $$("tgtttbsokct").setValue(tong_tien_kct);
                                                            $$("tgtttbsokkk").setValue(tong_tien_kkk);
                                                        },
                                                    },
                                                },
                                            ],
                                        },
                                    },
                                ],
                            },
                            {
                                cols: [
                                    {
                                        rows: [
                                            { view: "label", label: "Loại thuế", css: "lblTongtien" },
                                            {
                                                view: "label",
                                                label: " + Tổng cộng hàng hóa, dịch vụ",
                                                height: 22,
                                                css: { "padding-left": "5px", "font-weight": "bold", "background-color": "#86b2cf" },
                                            },
                                            { view: "label", label: "Thuế suất 10%", css: "lblSubTongtien", height: 22 },
                                            { view: "label", label: "Thuế suất 5%", css: "lblSubTongtien", height: 22 },
                                            { view: "label", label: "Thuế suất 0%", css: "lblSubTongtien", height: 22 },
                                            { view: "label", label: "Không chịu thuế GTGT", css: "lblSubTongtien", height: 22 },
                                            { view: "label", label: "Không kê khai, tính nộp thuế GTGT", css: "lblSubTongtien", height: 22 },
                                            // { view: "label", label: "Tổng cộng phí lệ phí", css: { "font-weight": "bold", "background-color": "#86b2cf" } , height: 22 },
                                            // { view: "label", label: "Tổng cộng thanh toán", css: { "font-weight": "bold", "background-color": "#86b2cf" } , height: 22 },
                                        ],
                                    },
                                    {
                                        rows: [
                                            { view: "label", label: "Thành tiền trước thuế", css: "lblTongtien" },
                                            { view: "text", format: "1.111,00", id: "tgtcthue", name: "tgtcthue", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtcthue10", name: "tgtcthue10", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtcthue5", name: "tgtcthue5", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtcthue0", name: "tgtcthue0", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtcthuekct", name: "tgtcthuekct", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtcthuekkk", name: "tgtcthuekkk", css: "txtTongtien", readonly: true, height: 22 },
                                            // { view: "text", id: "2", name: "2", css: "txtTongtien", readonly: true, height: 22 }
                                        ],
                                    },
                                    {
                                        rows: [
                                            { view: "label", label: "Tiền chiết khấu", css: "lblTongtien" },
                                            { view: "text", format: "1.111,00", id: "ttcktmai", name: "ttcktmai", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "ttcktmai10", name: "ttcktmai10", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "ttcktmai5", name: "ttcktmai5", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "ttcktmai0", name: "ttcktmai0", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "ttcktmaikct", name: "ttcktmaikct", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "ttcktmaikkk", name: "ttcktmaikkk", css: "txtTongtien", readonly: true, height: 22 },
                                        ],
                                    },
                                    {
                                        rows: [
                                            { view: "label", label: "Tiền thuế", css: "lblTongtien" },
                                            { view: "text", format: "1.111,00", id: "tgtthue", name: "tgtthue", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtthue10", name: "tgtthue10", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtthue5", name: "tgtthue5", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtthue0", name: "tgtthue0", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtthuekct", name: "tgtthuekct", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtthuekkk", name: "tgtthuekkk", css: "txtTongtien", readonly: true, height: 22 },
                                        ],
                                    },
                                    {
                                        rows: [
                                            { view: "label", label: "Tổng tiền sau thuế", css: "lblTongtien" },
                                            {
                                                view: "text",
                                                id: "tgtttbso",
                                                name: "tgtttbso",
                                                css: "txtTongtien",
                                                readonly: true,
                                                height: 22,
                                                on: {
                                                    onChange: function (newv, oldv) {
                                                        var total_amount = $$("tgtttbso").getValue();
                                                        var type_curency = $$("dvtte").getValue();
                                                        var url = app.config.host + "/Invoice68/Amount_ToWord?number=" + total_amount + "&type_amount=" + type_curency;
                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-type": "application/json" })
                                                            .get(url, {
                                                                error: function (text, data, XmlHttpRequest) {
                                                                    //webix.modalbox.hide(box);
                                                                    webix.alert({
                                                                        title: "Lỗi !",
                                                                        text: err,
                                                                        type: "alert-error",
                                                                    });
                                                                },
                                                                success: function (text, data, XmlHttpRequest) {
                                                                    var c_result = JSON.parse(text);
                                                                    $$("tgtttbchu").setValue(c_result.data);
                                                                },
                                                            });
                                                    },
                                                },
                                                format: "1.111,00",
                                            },
                                            { view: "text", format: "1.111,00", id: "tgtttbso10", name: "tgtttbso10", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtttbso5", name: "tgtttbso5", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtttbso0", name: "tgtttbso0", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtttbsokct", name: "tgtttbsokct", css: "txtTongtien", readonly: true, height: 22 },
                                            { view: "text", format: "1.111,00", id: "tgtttbsokkk", name: "tgtttbsokkk", css: "txtTongtien", readonly: true, height: 22 },
                                        ],
                                    },
                                ],
                            },
                            {
                                cols: [
                                    { view: "label", width: 309, label: "Đọc tiền bằng chữ", css: { "font-weight": "bold", "background-color": "#86b2cf", "padding-left": "5px" }, height: 22 },
                                    { view: "text", id: "tgtttbchu", name: "tgtttbchu", css: "txtTongtien", readonly: true, height: 22 },
                                ],
                            },
                            {
                                margin: 10,
                                cols: [
                                    {},
                                    {
                                        shortcut: "F10",
                                        view: "button",
                                        label: "Lưu tạm (F10)",
                                        type: "form",
                                        align: "center",
                                        width: 120,
                                        on: {
                                            onClick: function () {
                                                var item = $$("huyhoadon_add").getValues();
                                                var table = $$("huyhoadon_add");

                                                if (table.validate()) {
                                                    // var fn = Function(check);
                                                    // var result = fn();
                                                    // console.log(result);
                                                    webix.extend(table, webix.ProgressBar);
                                                    table.disable();
                                                    table.showProgress();

                                                    var details = [];
                                                    details.push({ data: $$("hoadon68_chitiet").serialize() });
                                                    item["details"] = details;

                                                    var item1 = [];
                                                    item1.push(item);
                                                    var data_post = { editmode: editmode, data: item1 };

                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(app.config.host + "/Invoice68/hoadonSaveChange", data_post, {
                                                            error: function (text, data, XmlHttpRequest) {
                                                                table.enable();
                                                                table.hideProgress();
                                                                // var json = JSON.parse(text);
                                                                // webix.message(json.ExceptionMessage, "error");
                                                                webix.message(text + XmlHttpRequest, "error");
                                                            },
                                                            success: function (text, data, XmlHttpRequest) {
                                                                var json = JSON.parse(text);
                                                                table.enable();
                                                                table.hideProgress();
                                                                if (!json.hasOwnProperty("error")) {
                                                                    webix.$$("invoice68").close();
                                                                    app.callEvent("dataChangedInvoie68", [editmode, json.data]);
                                                                } else {
                                                                    webix.message(json.error, "error");
                                                                }
                                                            },
                                                        });
                                                }
                                            },
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        },
                                    },
                                    {
                                        shortcut: "F11",
                                        view: "button",
                                        label: "Ký và gửi CQT (F11)",
                                        type: "form",
                                        align: "center",
                                        width: 150,
                                        on: {
                                            onClick: function () {
                                                var item = $$("huyhoadon_add").getValues();
                                                var table = $$("huyhoadon_add");

                                                if (table.validate()) {
                                                    // var fn = Function(check);
                                                    // var result = fn();
                                                    // console.log(result);
                                                    webix.extend(table, webix.ProgressBar);
                                                    table.disable();
                                                    table.showProgress();

                                                    var details = [];
                                                    details.push({ data: $$("hoadon68_chitiet").serialize() });
                                                    item["details"] = details;

                                                    var item1 = [];
                                                    item1.push(item);
                                                    var data_post = { editmode: editmode, data: item1 };

                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(app.config.host + "/Invoice68/hoadonSaveChange", data_post, {
                                                            error: function (text, data, XmlHttpRequest) {
                                                                table.enable();
                                                                table.hideProgress();
                                                                // var json = JSON.parse(text);
                                                                // webix.message(json.ExceptionMessage, "error");
                                                                webix.message(text + XmlHttpRequest, "error");
                                                            },
                                                            success: function (text, data, XmlHttpRequest) {
                                                                var json = JSON.parse(text);
                                                                table.enable();
                                                                table.hideProgress();
                                                                if (!json.hasOwnProperty("error")) {
                                                                    webix.$$("invoice68").close();
                                                                    app.callEvent("dataChangedInvoie68", [editmode, json.data]);

                                                                    var box = webix.modalbox({
                                                                        title: "Chọn chứng thư số",
                                                                        buttons: ["Nhận", "Hủy bỏ"],
                                                                        text: "<div id='ctnCerts'></div>",
                                                                        top: 100,
                                                                        width: 750,
                                                                        callback: function (result) {
                                                                            switch (result) {
                                                                                case "1":
                                                                                    webix.modalbox.hide();
                                                                                    break;
                                                                                case "0":
                                                                                    var table = $$("tblCerts");
                                                                                    var data = table.serialize();

                                                                                    var cert = null;

                                                                                    for (var i = 0; i < data.length; i++) {
                                                                                        if (data[i].chon == true) {
                                                                                            cert = data[i];
                                                                                            break;
                                                                                        }
                                                                                    }

                                                                                    if (cert == null) {
                                                                                        webix.message("Bạn chưa chọn chứng thư số");
                                                                                        return;
                                                                                    }

                                                                                    var item = json.data;

                                                                                    var postData = {
                                                                                        branch_code: user.getDvcsID(),
                                                                                        username: user.getCurrentUser(),
                                                                                        cer_serial: cert.cer_serial,
                                                                                        hdon_id: item.id,
                                                                                    };

                                                                                    webix
                                                                                        .ajax()
                                                                                        .headers({ "Content-Type": "application/json" })
                                                                                        .post(app.config.host + "/Invoice68/SignInvoiceCertFile68", postData, function (text) {
                                                                                            var result = JSON.parse(text);

                                                                                            if (result.hasOwnProperty("error")) {
                                                                                                webix.message({ type: "error", text: result.error });
                                                                                                return;
                                                                                            }

                                                                                            if (result.ok == true) {
                                                                                                var table = webix.$$("invoiceViews");
                                                                                                table.clearAll();
                                                                                                table.load(table.config.url);
                                                                                                // webix.ajax()
                                                                                                //     .headers({ 'Content-type': 'application/json' })
                                                                                                //     .post(app.config.host + "/System/ExecuteCommand",
                                                                                                //     {
                                                                                                //         command: "CM00025",
                                                                                                //         parameter: {

                                                                                                //         }
                                                                                                //     }
                                                                                                //     , function (text) {
                                                                                                //         var json = JSON.parse(text);
                                                                                                //         // auto send SMS
                                                                                                //         if (json[1].value == "C") {
                                                                                                //             var data = { id: item.inv_InvoiceAuth_id };

                                                                                                //             webix.ajax()
                                                                                                //                 .headers({ 'Content-type': 'application/json' })
                                                                                                //                 .post(app.config.host + "/Sms/NotifyInvoice", data, function (text) {
                                                                                                //                     var data = JSON.parse(text);

                                                                                                //                     if (!data.hasOwnProperty("error")) {

                                                                                                //                     }
                                                                                                //                     else {
                                                                                                //                         webix.message(data.error, "error");
                                                                                                //                     }
                                                                                                //                 });
                                                                                                //         }

                                                                                                //     });

                                                                                                webix.modalbox.hide();
                                                                                            }
                                                                                        });

                                                                                    return;
                                                                            }
                                                                        },
                                                                    });

                                                                    var tblMatHang = webix.ui({
                                                                        container: "ctnCerts",
                                                                        id: "tblCerts",
                                                                        view: "datatable",
                                                                        borderless: true,
                                                                        resizeColumn: true,
                                                                        height: 350,
                                                                        columns: [
                                                                            {
                                                                                id: "chon",
                                                                                header: _("CHON"),
                                                                                width: 65,
                                                                                checkValue: true,
                                                                                uncheckValue: false,
                                                                                template: "{common.checkbox()}",
                                                                            },
                                                                            {
                                                                                id: "cer_serial",
                                                                                header: _("SO_SERIAL"),
                                                                                width: 150,
                                                                            },
                                                                            {
                                                                                id: "subject_name",
                                                                                header: _("SUBJECTNAME"),
                                                                                width: 450,
                                                                            },
                                                                            {
                                                                                id: "begin_date",
                                                                                header: _("NGAY_BAT_DAU"),
                                                                                width: 200,
                                                                            },
                                                                            {
                                                                                id: "end_date",
                                                                                header: _("NGAY_KET_THUC"),
                                                                                width: 200,
                                                                            },
                                                                            {
                                                                                id: "issuer",
                                                                                header: _("DONVI"),
                                                                                width: 450,
                                                                            },
                                                                        ],
                                                                        url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                                                    });
                                                                } else {
                                                                    webix.message(json.error, "error");
                                                                }
                                                            },
                                                        });
                                                }
                                            },
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        },
                                    },
                                    {
                                        view: "button",
                                        hotkey: "esc",
                                        label: "Hủy bỏ (ESC)",
                                        align: "center",
                                        type: "danger",
                                        width: 120,
                                        click: function () {
                                            this.getTopParentView().close();
                                        },
                                    },
                                    {},
                                ],
                            },
                        ],
                    },
                },
            ],
        },
    };
});
