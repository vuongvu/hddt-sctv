define(["app"],function(app){
	
	return {		
		$ui:{
			view:"window", modal:true, id:"uploadfile-win", position:"center",
			head:"Tải file",
			body:{
                paddingY:20, 
                paddingX:30, 
                elementsConfig:{labelWidth: 100}, 
                view:"form", 
				id:"uploadfile-form", 
				complexData:true,
                elements:[
					{ 
						id:"uploader",
						view: "uploader", 
						value: 'Upload file', 
						name:"file",
						//accept:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
						link:"mylist",
						//upload: app.config.host + "/System/UploadExcel" ,
						on: {
							onUploadComplete: function(response){
								
								if (response.hasOwnProperty("error")){
									webix.message({ type:"error",text:response["error"]});
									return;
								}

								var topParentView = this.getTopParentView();

								if (topParentView.config.tableId !== undefined){
									var table = $$(topParentView.config.tableId);

									if (table!=null){
										table.clearAll();
										table.load(table.config.url);
									}
								}
								
								webix.$$("uploadfile-win").close();	
							}
						}
					},
					{
						view:"list",  
						id:"mylist", 
						type:"uploader",
						autoheight:true, borderless:true	
					},
					{
						view: "button",
						label: "Hủy bỏ",
						click: function() {
							webix.$$("uploadfile-win").close();							
						}
					}
				]
			}			
			
		}	
		
	};

});