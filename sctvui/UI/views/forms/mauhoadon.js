define(["app"], function (app) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "mauhoadon-win", position: "center",
			head: "Mẫu hóa đơn",
			height: 250,
			body: {
				paddingY: 10,
				paddingX: 10,
				//elementsConfig:{labelWidth: 120}, 
				view: "form",
				id: "mauhoadon-form",
				complexData: true,
				margin: 0,
				elements: [
					{
						view: "text",
						label: "Tên mẫu",
						name: "name",
						required: true,
						invalidMessage: "Tên mẫu không được bỏ trống",
						width: 600,
						labelWidth: 90
					},
					{
						view: "combo",
						label: "Hình thức",
						name: "invoice_type",
						id: "hinhthuc_dmmauhoadon",
						required: true,
						invalidMessage: "Hình thức không được bỏ trống",
						labelWidth: 90,
						options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00009",
						on: {
							onChange: function (newv, oldv) {
								var txtSolien = $$("so_lien_dmmauhoadon");

								if (newv == "E") {
									txtSolien.setValue(0);
									txtSolien.define("readonly", true);
								}
								else {
									txtSolien.define("readonly", false);
								}

								txtSolien.refresh();


							}
						}
					},
					{
						view: "combo",
						label: "Loại hóa đơn",
						name: "sl_invoice_type_id",
						id: "dmloaihd_id_dmmauhoadon",
						required: true,
						invalidMessage: "Loại hóa đơn không được bỏ trống",
						labelWidth: 90,
						//options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00008",
						options: {
							body: {
								url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00008",
								ready: function () {
									var form = $$("mauhoadon-form");
									var values = form.getValues();

									if (values.sl_invoice_type_id == null || values.sl_invoice_type_id == undefined || values.sl_invoice_type_id == "") {

										var data = this.serialize();

										for (var i = 0; i < data.length; i++) {
											var item = data[i];

											if (item.code == "01GTKT") {
												$$("dmloaihd_id_dmmauhoadon").setValue(item.id);
												break;
											}
										}
									}

								}
							}
						},
						on: {
							onChange: function (newv, oldv) {
								var list = this.getPopup().getList();
								var item = list.getItem(newv);

								if (item === undefined) {
									return;
								}

								var txtMauso = $$("mau_so_dmmauhoadon");

								var txtSolien = $$("so_lien_dmmauhoadon");
								var so_lien = txtSolien.getValue();
								var mau_so = item.code + so_lien + "/001";

								txtMauso.setValue(mau_so);
							}
						}
					},
					{
						cols: [
							{
								view: "text",
								label: "Mẫu số",
								name: "template_code",
								id: "mau_so_dmmauhoadon",
								labelWidth: 90,
								required: true,
								invalidMessage: "Mẫu số không được bỏ trống",
								attributes: {
									maxlength: 11
								},
								on: {
									onChange: function (newv, oldv) {
										if (newv == undefined) return;
										if (newv == "") return;

										if (newv.length != 11) {
											webix.message({ type: "error", text: "Độ dài mẫu số phải là 11 ký tự" });
											this.focus();
											return;
										}

										var txtHinhThuc = $$("hinhthuc_dmmauhoadon");
										var txtLoaihd = $$("dmloaihd_id_dmmauhoadon");

										if (txtHinhThuc.getValue() == "") {
											webix.message({ type: "error", text: "Bạn chưa chọn hình thức hóa đơn" });
											return;
										}

										if (txtLoaihd.getValue() == "") {
											webix.message({ type: "error", text: "Bạn chưa chọn loại hóa đơn" });
											return;
										}

										var hinhthuc = txtHinhThuc.getValue();
										var list = txtLoaihd.getList();

										var selectedItem = list.getItem(txtLoaihd.getValue());

										if (selectedItem == undefined) return;

										var ma_loai = selectedItem.code;

										var so_lien = hinhthuc == "E" ? 0 : "";
										var mau_so = ma_loai + so_lien + "/";

										if (newv.startsWith(mau_so) == false) {
											webix.message({ type: "error", text: "Mẫu số không đúng" });
											this.focus();
											return;
										}

										var sttMau = newv.substring(newv.length - 3, newv.length);
										var patt = /^[0-9][0-9][0-9]$/;

										if (patt.test(sttMau) == false) {
											webix.message({ type: "error", text: "3 ký tự cuối phải là số" });
											this.focus();
										}
									}
								}
							},
							{
								view: "text",
								label: "Ký hiệu",
								name: "invoice_series",
								labelWidth: 110,
								required: true,
								invalidMessage: "Ký hiệu không được bỏ trống",
								attributes: {
									maxlength: 6
								},
								on: {
									onChange: function (newv, oldv) {
										if (newv == undefined) return;
										if (newv == "") return;

										if (newv.length != 6) {
											webix.message({ type: "error", text: "Độ dài ký hiệu phải là 6 ký tự" });
											this.focus();
											return;
										}

										var patt = /^[A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][/]$/;

										if (patt.test(newv.substring(0, 3)) == false) {
											webix.message({ type: "error", text: "3 ký tự đầu không đúng" });
											this.focus();
											return;
										}

										patt = /^[0-9][0-9]$/;

										var nam = newv.substring(3, 5);

										if (patt.test(nam) == false) {
											webix.message({ type: "error", text: "2 ký tự năm không đúng" });
											this.focus();
											return;
										}

										var now = new Date();
										var year = now.getFullYear();

										nam = Number("20" + nam);

										if (nam < (year - 1) || nam > year) {
											webix.message({ type: "error", text: "Năm ký hiệu không đúng" });
											this.focus();
											return;
										}

										if (newv.substring(5, 6) != "E") {
											webix.message({ type: "error", text: "Ký tự cuối phải là E" });
											this.focus();
											return;
										}

									}
								}
							},
						]
					},
					{
						cols: [
							{
								view: "text",
								label: "Số liên",
								name: "copy_number",
								id: "so_lien_dmmauhoadon",
								labelWidth: 90,
								hidden: true,
								value: 0
							},
							{
								view: "text",
								label: "Số dòng in mẫu",
								name: "row_print_template",
								labelWidth: 110,
								hidden: true,
								value: 10
							},
						]
					},

					/* {
						borderless: true,
						view: "tabview",
						scrollX: true,
						cells: [
							{
								header: "Thông tin mẫu hóa đơn",
								select: "cell",
								body: {
									rows: [
										
									]
								}
							},
							{
								header: "Tiêu đề liên",
								select: "cell",
								body: {
									rows: [
										{
											cols: [
												{
													view: "text", label: "Liên 1", name: "copy1", width: 300
												},
												{
													view: "text", label: "Liên 2", name: "copy2", width: 300
												},
											]

										},
										{
											cols: [
												{
													view: "text", label: "Liên 3", name: "copy3"
												},
												{
													view: "text", label: "Liên 4", name: "copy4"
												},
											]

										},
										{
											cols: [
												{
													view: "text", label: "Liên 5", name: "copy5"
												},
												{
													view: "text", label: "Liên 6", name: "copy6"
												},
											]

										},
										{
											cols: [
												{
													view: "text", label: "Liên 7", name: "copy7"
												},
												{
													view: "text", label: "Liên 8", name: "copy8"
												},
											]

										},
										{
											cols: [
												{
													view: "text", label: "Liên 9", name: "copy9"
												},
												{}
											]
										}
									]
								}
							}
						]						
					}, */
					{},
					{
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", view: "button", label: "Lưu (F10)", type: "form", align: "center", width: 120,
								on: {
									onClick: function () {
										var form = webix.$$("mauhoadon-form");

										if (form.validate() == false) return;

										var item = form.getValues();

										var khData = {};

										khData.windowid = app.path[1].params[0];
										khData.editmode = editmode;
										khData.data = [];

										khData.data.push(item);

										webix.ajax()
											.headers({ 'Content-type': 'application/json' })
											.post(app.config.host + "/System/Save", khData, function (text) {
												var json = JSON.parse(text);

												if (!json.hasOwnProperty("error")) {
													$$("mauhoadon-win").close();
													app.callEvent("dataChanged", [editmode, json.data]);
													if(json.hasOwnProperty("ok") && editmode == 1 && json.ok == "true")
													{
														webix.message("Thêm mới mẫu hóa đơn thành công!", "success");
													}
												}
												else {
													webix.message(json.error);
												}

											});
									}
								},
								click: function () {
									this.callEvent("onClick");

								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 120,
								click: function () {
									webix.$$("mauhoadon-win").close();
								}
							},
							{}
						]
					}

				]
			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});