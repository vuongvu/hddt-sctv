define(["app"], function (app) {
    
    var editmode = 0;

    return {
        $ui: {
            view: "window", modal: true, id: "duyethoadon-win", position: "center",
            head: "Chi tiết hóa đơn",
            height: 800,
            width: 5000,
            autoheight: false,
            body: {
                paddingY: 20,
                paddingX: 20,
                //elementsConfig:{labelWidth: 120}, 
                view: "form",
                id: "duyethoadon-form",
                complexData: true,
                elements: [
                    {
                        view: "fieldset",
                        label: "Thông tin hóa đơn",
                        body: {
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: "datepicker",
                                            label: "Ngày hóa đơn",
                                            name: "invoice_issued_date",
                                            // name: "date",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                            format: webix.Date.dateToStr("%d/%m/%Y")
                                        },
                                        {
                                            view: "text",
                                            label: "Mã mẫu hóa đơn",
                                            name: "template_code",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Ký hiệu",
                                            name: "invoice_series",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Số hóa đơn",
                                            name: "invoice_number",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                          
                                        },
                                    ]
                                },
                                {
                                    cols: [
                                        {
                                            view: "text",
                                            label: "Trạng thái",
                                            name: "status",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Hình thức thanh toán",
                                            name: "payment_method_name",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Mã tiền tệ",
                                            name: "currency_code",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Tỷ giá",
                                            name: "exchange_rate",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                    ]
                                },
                                {
                                    cols: [
                                        {
                                            view: "text",
                                            label: "NH bên mua",
                                            name: "buyer_bank_account",
                                            readonly: true,
                                            width: 600,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "TK bên mua",
                                            name: "buyer_bank_account",
                                            readonly: true,
                                            width: 600,
                                            labelWidth: 150,
                                        },
                                    ]
                                }
                            ]
                        }
                    },

                    {
                        view: "fieldset",
                        label: "Thông tin khách hàng",
                        body: {
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: "text",
                                            label: "Mã khách hàng",
                                            name: "customer_code",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Mã số thuế",
                                            name: "buyer_taxcode",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Email",
                                            name: "buyer_email",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        {
                                            view: "text",
                                            label: "Điện thoại",
                                            name: "buyer_tel",
                                            readonly: true,
                                            width: 300,
                                            labelWidth: 150,
                                        },
                                        
                                    ]
                                },
                                {
                                    cols: [{
                                        view: "text",
                                        label: "Tên người mua",
                                        name: "buyer_display_name",
                                        readonly: true,
                                        width: 1200,
                                        labelWidth: 150,
                                    }]
                                },
                                {
                                    cols: [
                                        {
                                            view: "text",
                                            label: "Địa chỉ",
                                            name: "buyer_address_line",
                                            readonly: true,
                                            width: 1200,
                                            labelWidth: 150,
                                        },
                                    ]
                                },
                                {
                                    cols: [
                                        {
                                            view: "text",
                                            label: "Tên đơn vị",
                                            name: "buyer_legal_name",
                                            readonly: true,
                                            width: 1200,
                                            labelWidth: 150,
                                        },
                                    ]
                                }
                            ]
                        }
                    },

                    // {
                    //     view: "fieldset",
                    //     label: "Chi tiết hóa đơn",
                        
                    // },

                    {  
                        borderless:true,
                        scrollX:true,
                        tabbar:{  
                           "width":200
                        },
                        view:"datatable",
                        id:"detail",
                        on: {
                            "data->onStoreUpdated": function () {
                                this.data.each(function (obj, i) {
                                    obj.stt = i + 1;
                                });
                            }
                        },
                        columns:[
                            {
                                id: "stt",
                                header: "STT",
                                width: 40,
                            },
                            {
                                id: "item_code",
                                name: "item_code",
                                header: "Mã hàng",
                                width: 150,
                                editor: "combo",
                            },
                            {
                                id: "item_name",
                                name: "item_name",
                                header: "Tên hàng hóa",
                                width: 250,
                                editor: "text"
                            },
                            {
                                id: "unit_code",
                                name: "unit_code",
                                header: "ĐVT",
                                width: 150,
                                editor: "combo",
                            },
                            {
                                id: "quantity",
                                name: "quantity",
                                header: "Số lượng",
                                width: 120,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "autonumeric",
                            },
                            {
                                id: "unit_price",
                                name: "unit_price",
                                header: "Đơn giá",
                                width: 120,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "autonumeric",
                            },

                            {
                                id: "total_amount_without_vat",
                                name: "total_amount_without_vat",
                                header: "Tiền trước thuế",
                                width: 150,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "autonumeric",
                            },

                            {
                                id: "discount_percentage",
                                name: "discount_percentage",
                                header: "% CK",
                                width: 150,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "autonumeric",
                            },

                            {
                                id: "discount_amount",
                                name: "discount_amount",
                                header: "Tiền CK",
                                width: 150,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "autonumeric",
                            },

                            {
                                id: "tax_type",
                                name: "tax_type",
                                header: "% Thuế",
                                width: 150,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "combo",
                                options: null
                            },

                            {
                                id: "vat_amount",
                                name: "vat_amount",
                                header: "Tiền thuế",
                                width: 150,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "autonumeric",
                            },

                            {
                                id: "total_amount",
                                name: "total_amount",
                                header: "Tổng tiền",
                                width: 150,
                                format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0,
                                }),
                                css: {
                                    "text-align": "right",
                                },
                                editor: "autonumeric",
                            },
                        ],
                        data: [
                           
                        ],

                        // load:function(view, params){
                        //     var data = webix.ajax().get(app.config.host + "System/GetInvoiceDetail?id=" + "inv_invoiceauth_id");
                        // }
                     },

                    {
                        cols: [
                            {
                                view: "text",
                                label: "Tổng tiền trước thuế",
                                name: "total_amount_without_vat",
                                readonly: true,
                                width: 300,
                                labelWidth: 150,
                                format: "1.111",
                            },
                            {
                                view: "text",
                                label: "Tổng tiền CK",
                                name: "discount_percentage",
                                readonly: true,
                                width: 300,
                                labelWidth: 150,
                                format: "1.111",
                            },
                            {
                                view: "text",
                                label: "Tiền thuế",
                                name: "vat_amount",
                                readonly: true,
                                width: 300,
                                labelWidth: 150,
                                format: "1.111",
                            },
                            {
                                view: "text",
                                label: "Thành tiền",
                                name: "total_amount",
                                readonly: true,
                                width: 300,
                                labelWidth: 150,
                                format: "1.111",
                            },
                        ]
                    },

                    {},
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                view: "button", hotkey: "esc", label: "Đóng (ESC)", type: "danger", align: "center", width: 120,
                                click: function () {
                                    webix.$$("duyethoadon-win").close();
                                }
                            },
                            {}
                        ]
                    }

                ]
            }

        },
        setEditMode: function (mode) {
            editmode = mode;
        }

    };

});