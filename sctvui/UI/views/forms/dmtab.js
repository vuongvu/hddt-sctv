define(["app"], function (app) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "dmtab-win", position: "center",
			head: "Thông tin Tab",
			fullscreen: true,
			body: {
				view: "scrollview",
				scroll: "native-y",
				body: {
					paddingY: 10,
					paddingX: 10,
					elementsConfig: { labelWidth: 100 },
					view: "form",
					id: "dmtab-form",
					complexData: true,
					margin: 0,
					elements: [
						{
							cols: [
								{ view: "text", name: "code", label: "TAB ID", id: "dmtab-id" },
								{ view: "text", name: "ord", label: "STT", id: "dmtab-stt" },
							]
						},
						{ view: "text", name: "name", label: "Tab Name", id: "dmtab-name" },
						{ view: "text", name: "table_name", label: "Tab Table", id: "dmtab-table" },
						{
							cols: [
								{
									view: "combo",
									name: "tab_type",
									label: "TAB TYPE",
									options: ["Normal", "Detail", "Master"],
									id: "dmtab-type"
								},
								{
									view: "combo",
									name: "tab_view",
									label: "TAB VIEW",
									options: ["Grid", "Tree"],
									id: "dmtab-view"
								}
							]
						},
						{ view: "text", name: "foreign_key", label: "FOREIGN KEY", id: "dmtab-FOREIGN-KEY" },
						{ view: "text", name: "insert_cmd", label: "INSERT CMD", id: "dmtab-insert-cmd" },
						{ view: "text", name: "update_cmd", label: "UPDATE CMD", id: "dmtab-update-cmd" },
						{ view: "text", name: "delete_cmd", label: "DELETE CMD", id: "dmtab-delete-cmd" },
						{ view: "textarea", name: "select_cmd", label: "SELECT CMD", height: 60, id: "dmtab-select-cmd" },
						{ view: "textarea", name: "select_change", label: "SELECT CHANGE", height: 150, id: "dmtab-select-change" },
						{ view: "textarea", name: "cell_editstop", label: "CELL EDITSTOP", height: 150, id: "dmtab-cell-editstop" },
						{ view: "textarea", name: "after_addrow", label: "AFTER ADDROW", height: 150, id: "dmtab-afteraddrow" },
						{ view: "textarea", name: "after_delrow", label: "AFTER DELROW", height: 150, id: "dmtab-afterdelrow" },
						{ view: "textarea", name: "oncheck", label: "ON CHECK", height: 150, id: "dmtab-oncheck" },
						{ view: "text", name: "prefix", label: "PREFIX", id: "dmtab-prefix" },
						{ view: "text", name: "order_by", label: "ORDER BY", id: "dmtab-orderby" },
						{
							margin: 10,
							cols: [
								{},
								{
									shortcut: "F10", view: "button", label: "Nhận (F10)", type: "form", align: "center", width: 140,
									on: {
										onClick: function () {
											var form = webix.$$("dmtab-form");
											var item = form.getValues();

											var url = app.config.host + "/WbTab/Update";

											webix.ajax()
												.headers({ 'Content-type': 'application/json' })
												.post(url, item, function (text) {
													var data = JSON.parse(text);

													if (!data.hasOwnProperty("error")) {
														item.id = data.id;
														webix.$$("dmtab-win").close();
														app.callEvent("dataChanged", [editmode, item]);
													}
													else {
														webix.message(data.error);
													}
												});
										}
									},
									click: function () {
										this.callEvent("onClick");
									}
								},
								{
									view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 140,
									click: function () {
										webix.$$("dmtab-win").close();
									}
								}
							]
						}

					]
				}

			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});