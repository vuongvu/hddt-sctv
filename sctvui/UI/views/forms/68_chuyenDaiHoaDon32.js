define(["app", "locale", "models/user", "views/commonHD", "views/windowhoadon78"], function (app, _, user, commonHD, windowhoadon78) {
    var editmode = 0;
    var param;
    var branh_code;
    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Nhận (F10)",
            type: "form",
            align: "center",
            width: 120,
            on: {
                onClick: function () {
                    setTimeout(function () {
                        if ($$("chuyendai-form").validate()) {
                            var item = $$("chuyendai-form").getValues();

                            var kyHieuHoaDon = {
                                cctbao_id: $$("kyhieuhoadon_78_id").getValue(),
                                khieu: $$("kyhieuhoadon_78_id").data.text,
                                lhdclquan: $$("lhdclquan").getValue(),
                                khmshdclquan: $$("khmshdclquan").getValue(),
                                khhdclquan: $$("khhdclquan").getValue(),
                                shdclquan: $$("shdclquan").getValue(),
                                nlhdclquan: $$("nlhdclquan").getValue(),
                                case: $$("loaichuyendai_id").getValue(),
                                dvtte: "VND",
                                tgia: "1",
                                htttoan: "Tiền mặt/Chuyển khoản",
                            };
                            if (kyHieuHoaDon.case == 1) {
                                kyHieuHoaDon.tthdon = 2;
                                kyHieuHoaDon.tthdon = 2;
                            } else if (kyHieuHoaDon.case == 2) {
                                kyHieuHoaDon.tthdon = 19;
                                kyHieuHoaDon.tthdon = 19;
                            } else if (kyHieuHoaDon.case == 3) {
                                kyHieuHoaDon.tthdon = 21;
                                kyHieuHoaDon.tthdon = 21;
                                kyHieuHoaDon.note = "reduce_change_invoice";
                            } else if (kyHieuHoaDon.case == 4) {
                                kyHieuHoaDon.tthdon = 23;
                                kyHieuHoaDon.tthdon = 23;
                            }
                            switch (item.loaichuyendai_id) {
                                case "1":
                                    commonHD.ExcuteReplaceInvoice_32(kyHieuHoaDon);
                                    break;
                                case "2":
                                    commonHD.ExcuteIncreaseChangeInvoice_32(kyHieuHoaDon);
                                    break;
                                case "3":
                                    commonHD.ExcuteReduceChangeInvoice_32(kyHieuHoaDon);
                                    break;
                                case "4":
                                    commonHD.ExcuteInfoChangeInvoice_32(kyHieuHoaDon);
                                    break;
                                default:
                                    break;
                            }
                            $$("chuyendai-win").close();
                        }
                    }, 100);
                },
            },

            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            hotkey: "esc",
            label: "Hủy bỏ (ESC)",
            type: "danger",
            align: "center",
            width: 120,
            click: function () {
                var table = $$("invoiceViews").getParentView();
                console.log(table);
                webix.$$("chuyendai-win").close();
            },
        },
        {},
    ];
    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "chuyendai-win",
        position: "center",
        head: "Chuyển dải hoá đơn hóa đơn",
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            width: 500,
            id: "chuyendai-form",
            complexData: true,
            margin: 0,
            elements: [
                // {
                //     view: "text",
                //     label: "Tên mẫu",
                //     name: "ten_mau",
                //     required: true,
                //     invalidMessage: "Tên mẫu không được bỏ trống",
                //     labelWidth: 120
                // },
                {
                    view: "combo",
                    name: "kyhieuhoadon_78_id",
                    label: "Kí hiệu hóa đơn",
                    //labelPosition: "top",
                    id: "kyhieuhoadon_78_id",
                    required: true,
                    labelWidth: 200,
                    invalidMessage: "Kí hiệu hóa đơn không được bỏ trống",
                },
                {
                    view: "combo",
                    name: "loaichuyendai_id",
                    label: "Loại chuyển dải",
                    id: "loaichuyendai_id",
                    required: true,
                    labelWidth: 200,
                    value: "1",
                    options: [
                        { id: 1, value: "Thay thế" },
                        { id: 2, value: "Điều chỉnh tăng" },
                        { id: 3, value: "Điều chỉnh giảm" },
                        { id: 4, value: "Điều chỉnh thông tin" },
                    ],
                },
                {
                    view: "text",
                    name: "loaihdlq",
                    label: "Loại HĐLQ",
                    id: "lhdclquan",
                    labelWidth: 200,
                    value: "3",
                    readonly: true,
                },
                {
                    view: "text",
                    name: "khmshdclquan",
                    label: "Kí hiệu mẫu số HĐLQ",
                    labelWidth: 200,
                    id: "khmshdclquan",
                    validate: webix.rules.isn,
                    invalidMessage: "Kí hiệu mẫu số hóa đơn liên quan không được bỏ trống",
                    attributes: { maxlength: 11 },
                    required: true,
                },
                {
                    view: "text",
                    name: "khhdclquan",
                    label: "Kí hiệu HĐLQ",
                    labelWidth: 200,
                    id: "khhdclquan",
                    invalidMessage: "Kí hiệu mẫu số hóa đơn liên quan không được bỏ trống",
                    attributes: { maxlength: 8 },
                    required: true,
                },
                {
                    view: "text",
                    name: "shdclquan",
                    label: "Số hóa đơn LQ",
                    labelWidth: 200,
                    id: "shdclquan",
                    invalidMessage: "Số hóa đơn liên quan không được bỏ trống",
                    attributes: { maxlength: 8 },
                    required: true,
                },
                {
                    view: "datepicker",
                    name: "nlhdclquan",
                    label: "Ngày lập hóa đơn liên quan",
                    labelWidth: 200,
                    id: "nlhdclquan",
                    invalidMessage: "Ngày lập hóa đơn không được bỏ trống",
                    required: true,
                },
                {
                    margin: 10,
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
        },
        initUI: function (item) {
            param = item;
            $$("kyhieuhoadon_78_id").define("options", app.config.host + "/System/GetDataByReferencesId?id=" + param["data"][0].WinParams[0].wb_references_id);
            var table = $$("invoiceViews");
            var item = table.getSelectedItem();
            $$("kyhieuhoadon_78_id").setValue($$("kyhieu_id").getValue());
            $$("kyhieuhoadon_78_id").refresh();
        },
        setValues: function (item) {
            $$("chuyendai-form").setValues(item);
        },
    };
});
