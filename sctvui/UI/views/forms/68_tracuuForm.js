define(["app", "models/user"], function (app, user) {
    var editmode = 0;
    var param;

    var button = [
        {},
        {
            shortcut: "F10",
            view: "button",
            label: "Nhận (F10)",
            type: "form",
            align: "center",
            width: 120,
            on: {
                onClick: function () {
                    var self = this;
                    setTimeout(function () {
                        var item = $$("formtable").getValues();
                        var table = $$("viewtable");

                        if ($$("formtable").validate()) {
                            webix.extend(table, webix.ProgressBar);
                            table.disable();
                            table.showProgress();

                            item.editmode = editmode;
                            webix
                                .ajax()
                                .headers({ "Content-type": "application/json" })
                                .post(app.config.host + "/Invoice68/UpdateCreatTraCuu", item, {
                                    error: function (text, data, XmlHttpRequest) {
                                        var table = $$("formtable");
                                        table.enable();
                                        table.hideProgress();
                                        // var json = JSON.parse(text);
                                        // webix.message(json.ExceptionMessage, "error");
                                        webix.message(text + XmlHttpRequest, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var json = JSON.parse(text);
                                        table.enable();
                                        table.hideProgress();
                                        if (!json.hasOwnProperty("error")) {
                                            $$("form-win").close();
                                            table.clearAll();
                                            table.load(table.config.url);
                                        } else {
                                            webix.message(json.error, "error");
                                        }
                                    },
                                });
                        }
                    }, 100);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            hotkey: "esc",
            label: "Hủy bỏ (ESC)",
            type: "danger",
            align: "center",
            width: 120,
            click: function () {
                webix.$$("form-win").close();
            },
        },
        {},
    ];

    var ui = {
        view: "window",
        modal: true,
        move: true,
        id: "form-win",
        position: "center",
        head: "Thêm mới/Chỉnh sửa user tra cứu",
        on: {
            onBeforeShow: function () {
                if (editmode == 2) {
                    $$("username").define("readonly",true);
                }
                if (editmode == 1) {
                    $$("active").define("readonly",true);
                }
            },
        },
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            width: 500,
            id: "formtable",
            complexData: true,
            margin: 0,
            elements: [
                {
                    view: "text",
                    label: "Mã số thuế",
                    name: "username",
                    id: "username",
                    labelWidth: 120,
                    required: true,
                    invalidMessage: "Không được bỏ trống",
                    validate: webix.rules.isNotEmpty
                },
                {
                    view: "text",
                    label: "Email",
                    name: "email",
                    id: "email",
                    labelWidth: 120,
                    required: true,
                    invalidMessage: "Không được bỏ trống",
                    validate: webix.rules.isNotEmpty
                },
                {
                    view: "text",
                    label: "Tên khách hàng",
                    name: "fullname",
                    id: "fullname",
                    labelWidth: 120,
                },
                {
                    view: "combo",
                    label: "Trạng thái tài khoản",
                    name: "active",
                    id: "active",
                    labelWidth: 120,
                    value:1,
                    options:[
                        {id:"1", value:"Hoạt động"},
                        {id:"2",value:"Tạm khóa"},
                    ]
                },
                {
                    margin: 10,
                    cols: button,
                },
            ],
        },
    };

    return {
        $ui: ui,
        setEditMode: function (mode) {
            editmode = mode;
            if(editmode == "1") {
                $$("form-win").getHead().setHTML("Thêm mới tài khoản tra cứu");
            }
            if(editmode == "2") {
                $$("form-win").getHead().setHTML("Chỉnh sửa tài khoản tra cứu");
            }
        },
        initUI: function (item) {
            param = item;
        },
        setValues: function (item) {
            $$("formtable").setValues(item);
        },
    };
});
