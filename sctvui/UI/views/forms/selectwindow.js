define([
	"app",
	"locale"
],function(app,_){

	return {		
		$ui:{   
			view:"window", modal:true, id:"select-win", position:"center",
			head:"Thông tin Tab",
			fullscreen:true,
			body:{
                rows:[
					{
						height:35,
						type: "toolbar",
						cols:[
							{
								view: "button", 
								type: "icon", 
								icon:"check-square", 
								label: "Chọn" ,
								width:65,								
								on: {
									onClick: function(){
										var win = this.getTopParentView();

										var valueField = win.config.valueField;
										var displayField = win.config.displayField;
										var fieldExpression = win.config.fieldExpression;
										var table= win.getChildViews()[1].getChildViews()[1];

										if (win.config.editorId!=null){

											if (valueField!=null){									
												var selectedItem = table.getSelectedItem();
												var editor = $$(win.config.editorId);
												editor.setValue(selectedItem[valueField]);
											}

											if (displayField !== undefined){
												if (displayField!=null){
													var selectedItem = table.getSelectedItem();
													var editor = $$(win.config.editorId);
													editor.setValue(selectedItem[displayField]);
												}
											}

											if (fieldExpression!=null){
												var formParent = $$(win.config.formId);
												var parentItem = formParent.getValues(); 
												var selectedItem = table.getSelectedItem();
												var fields = fieldExpression.split(';');

												for(var i=0;i<fields.length;i++){
													var field = fields[i].replace("{","").replace("}","");
													var array = field.split(":");

													parentItem[array[1]] = selectedItem[array[0]];
												}

												formParent.setValues(parentItem);
												$$(win.config.editorId).focus();
											}
										}
										else {
											var tableParent = $$(win.config.tableId);
											var columnName = win.config.columnName;
											var item = tableParent.getSelectedItem();

											if (valueField!=null){									
												var selectedItem = table.getSelectedItem();
												item[columnName] = selectedItem[valueField];												
											}

											if (displayField !== undefined){
												if (displayField!=null){
													var selectedItem = table.getSelectedItem();
													item[columnName] = selectedItem[displayField];
												}
											}

											if (fieldExpression!=null){												
												var selectedItem = table.getSelectedItem();
												var fields = fieldExpression.split(';');

												for(var i=0;i<fields.length;i++){
													var field = fields[i].replace("{","").replace("}","");
													var array = field.split(":");

													item[array[1]] = selectedItem[array[0]];
												}												
											}

											tableParent.updateItem(item);
											//webix.UIManager.setFocus(tableParent.config.id);
											//tableParent.edit({row: item.id, column: columnName});	
										}										

										this.getTopParentView().close();
										if (win.config.tableId!=null){
											webix.UIManager.setFocus(win.config.tableId);
										}
										
									}
								},
								click: function(){                
									this.callEvent("onClick");
								}
							},
							{
								view: "button", 
								type: "icon", 
								icon:"close", 
								label: "Thoát" ,
								width:65,								
								on: {
									onClick: function(){
										this.getTopParentView().close();										
									}
								},
								click: function(){                
									this.callEvent("onClick");
								}
							}

						]
					},
					{
						view: "datatable",
						select: "row",
						datafetch: 50,
						on:{
							onKeyPress: function(code,e){
								var table = this;

								if (e.key == "Enter"){
									var win = this.getTopParentView();
									var toolbar= win.getChildViews()[1].getChildViews()[0];

									var btnChon = toolbar.getChildViews()[0];
									btnChon.callEvent("onClick");
								}
							}
						},
						pager : "selectPagerA"
					},
					{
						id: "selectPagerA",
						view:"pager",
						size: 50,
						group: 5,
						template:"{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}"
					}
				]
			}				
			
		},
		initUI: function(windowconfig){
			var win=$$("select-win");

			var table= win.getChildViews()[1].getChildViews()[1];
			table.clearAll();

			var vc_window = windowconfig["data"][0];
			var tabs = vc_window.Tabs;

			var masterTab = tabs[0];

			var fields = windowconfig['data'][0]['Tabs'][0]['Fields'];
			var cols = [];

			for(var i=0;i<masterTab.Fields.length;i++){
				var field = masterTab.Fields[i];

				if(field.hide_in_grid==true){
					continue;
				}			
					
				var item = {
					id: field.column_name, 
					header: _(field.caption),
					columnType: field.column_type 					
				};

				if (field.type_filter!=null){
                    if (field.type_filter!=""){
                        item.header = [_(item.header), { content: field.type_filter}]
                    }
                }

				if (field.column_width == null){
					item.fillspace = true;
				}
				else {
					item.width = field.column_width;
				}

				if (field.type_editor == "combo"){
					item.editor = field.type_editor;
					item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                }

				cols.push(item);
			}

			table.config.columns = cols;			
			table.refreshColumns();
			
			table.config.windowno = vc_window.window_id;

			var url = {
                    $proxy:true,
                    load:function(view, callback, params){                        
                        var windowno = view.config.windowno;
						var url = app.config.host + "/System/GetDataByWindowNo";
						
						var parameters = {
							window_id: windowno
						};

                        if (params==null){
														
							parameters["start"] = 0;
							parameters["count"] = view.config.datafetch;
							parameters["continue"] = null;
							parameters["filter"] = [];
							parameters["infoparam"] = null;
							parameters["tlbparam"] = [];

                        }
                        else {
                            							
							parameters["start"] = params.start;
							parameters["count"] = params.count;
							parameters["continue"] = params.continue;
							parameters["filter"] = [];
							parameters["infoparam"] = null;
							parameters["tlbparam"] = [];

                            if (params.filter!=null){
                                var array=[];

                                for(var property in params.filter){

                                    var column = view.getColumnConfig(property);

                                    if (column !== undefined){
                                         var item = {
                                            columnName : property,
                                            columnType : column.columnType,
                                            value: params.filter[property]
                                        };

                                        array.push(item);
                                    }
                                    
                                   
								}
								
								parameters["filter"] = array;                            
                                
                            }
                                                       
                        }

                        if (view.config.infoparam!=null){
							parameters["infoparam"] = view.config.infoparam;
                        }
						
						webix.ajax().bind(view)
							.headers({"Content-Type": "application/json"})
							.post(url, parameters, callback);
                        
                    }
            };
				
			table.define("url",url );
		}		
	};

});