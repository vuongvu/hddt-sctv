define(["app", "locale", "models/user", "views/modules/minvoiceplugin"], function (app, _, user, plugin) {
    var editmode = 0;

    function signOnServerFile(data, cert, server_cert) {

        var parameters = {
            xml: data["InvoiceXmlData"],
            refUri: "#data",
            certSerial: cert.so_serial,
            tokenSerial: cert.tokenSerial,
            signId: "seller",
            signTag: "Signatures",
            pass: cert.pass,
            path: cert.path,
            inv_InvoiceAuth_id: data["inv_InvoiceAuth_id"]
        };

        webix.ajax()
            .headers({ "Content-Type": "application/json" })
            .post(server_cert + "/Token/SignXmlByFile", parameters, function (text) {
                var json = JSON.parse(text);

                if (json.hasOwnProperty("error")) {
                    webix.message(json["error"], "error");
                }
                else {
                    var parameters = {
                        InvoiceXmlData: json.xml,
                        inv_InvoiceAuth_id: json.inv_InvoiceAuth_id
                    };
                    //console.log(parameters);

                    webix.ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(app.config.host + "/Invoice/SaveXml", parameters, function (text) {
                            var result = JSON.parse(text);

                            if (result.trang_thai == "Đã ký") {

                                // var table = webix.$$("dmwindowData");
                                // table.clearAll();
                                // table.load(table.config.url);

                                webix.ajax()
                                    .headers({ 'Content-type': 'application/json' })
                                    .post(app.config.host + "/System/ExecuteCommand",
                                    {
                                        command: "CM00025",
                                        parameter: {

                                        }
                                    }, function (text) {
                                        var json = JSON.parse(text);
                                        // auto send SMS 
                                        if (json[1].gia_tri == "C") {
                                            var data = { id: item.inv_InvoiceAuth_id };

                                            webix.ajax()
                                                .headers({ 'Content-type': 'application/json' })
                                                .post(app.config.host + "/Sms/NotifyInvoice", data, function (text) {
                                                    var data = JSON.parse(text);

                                                    if (!data.hasOwnProperty("error")) {


                                                    }
                                                    else {
                                                        webix.message(data.error, "error");
                                                    }
                                                });
                                        }
                                        // send email ma = 'AUTO_SEND_EMAIL'
                                        // if (json[0].gia_tri == "C") {
                                        //     var data = { inv_InvoiceAuth_id: item.inv_InvoiceAuth_id, nguoinhan: item.inv_buyerEmail, tieude: "Thông báo xuất hóa đơn điện tử" };

                                        //     webix.ajax()
                                        //         .headers({ 'Content-type': 'application/json' })
                                        //         .post(app.config.host + "/Invoice/AutoSendInvoiceByEmail", data, function (text) {
                                        //             var data = JSON.parse(text);

                                        //             if (!data.hasOwnProperty("error")) {


                                        //             }
                                        //             else {
                                        //                 webix.message(data.error, "error");
                                        //             }
                                        //         });
                                        // }


                                    });
                                // webix.modalbox.hide();
                                // webix.message("Ký hóa đơn thành công !", "debug");
                            }

                        });
                }
            });
    }

    function signCertFile() {
        var json = $$("tblHoadon").serialize();
        var data = [];

        for (var i = 0; i < json.length; i++) {
            var item = json[i];

            if (item.chon1 != null && item.chon1 != undefined) {
                if (item.chon1 == "C") {
                    var inv = {
                        inv_invoiceauth_id: item.inv_InvoiceAuth_id
                    };

                    data.push(inv);
                }
            }
        }

        var txtCer = $$("txtCerSerial");
        var cert = txtCer.getList().getSelectedItem();

        var postData = {
            invs: data,
            so_serial: cert.so_serial
        };

        var url = app.config.host + "/Invoice/SignListInvoiceCertFile";

        webix.ajax()
            .headers({ "Content-Type": "application/json" })
            .post(url, postData, function (text) {
                var res = JSON.parse(text);

                if (res.hasOwnProperty("error")) {
                    webix.message({ type: "error", text: res.error });
                }
                else {
                    webix.message("Đã thực hiện xong");
                }
            });
    }

    var layout = {
        view: "window", modal: true, id: "chukyso-win", position: "center",
        head: "Ký hóa đơn chờ ký",
        width: 600,
        height: 600,
        on: {
            onBeforeShow: function () {
                webix.ajax()
                    .headers({ 'Content-type': 'application/json' })
                    .post(app.config.host + "/System/ExecuteCommand", {
                        command: "CM00019",
                        parameter: {
                            ma_dvcs: ""
                        }
                    }, {
                        error: function (text, data, XmlHttpRequest) {
                            //webix.modalbox.hide(box);
                            webix.alert({
                                title: "Lỗi !",
                                text: err,
                                type: "alert-error"
                            });
                        },
                        success: function (text, data, XmlHttpRequest) {
                            var c_result = JSON.parse(text);

                            for (var i = 0; i < c_result.length; i++) {
                                var item = c_result[i];

                                if (item.ma == "KYTAPTRUNG") {
                                    $$("chukyso-win").config.kytaptrung = item.gia_tri;
                                }
                                if (item.ma == "SERVER_CERT") {
                                    $$("chukyso-win").config.server_cert = item.gia_tri;
                                }
                            }
                        }
                    });
            }
        },
        body: {
            paddingY: 10,
            paddingX: 10,
            view: "form",
            id: "chukyso-form",
            complexData: true,
            margin: 0,
            elements: [
                {
                    cols: [
                        {
                            view: "datepicker",
                            name: "tu_ngay",
                            format: "%d/%m/%Y",
                            label: "Từ ngày",
                            labelPosition: "top",
                            value: new Date()
                        },
                        {
                            view: "datepicker",
                            name: "den_ngay",
                            format: "%d/%m/%Y",
                            label: "Đến ngày",
                            labelPosition: "top",
                            value: new Date()
                        },
                    ]
                },
                {
                    view: "combo",
                    name: "inv_InvoiceCode_id",
                    label: "Ký hiệu",
                    labelPosition: "top",
                    options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00029",
                    required: true,
                    invalidMessage: "Ký hiệu hóa đơn không được để trống !"
                    // validate: webix.rules.isEmpty
                },
                {
                    view: "combo",
                    name: "cerSerial",
                    label: "Chứng thư số",
                    labelPosition: "top",
                    id: "txtCerSerial",
                    required: true,
                    invalidMessage: "Chứng thư số không được để trống !",
                    options: {
                        view: "gridsuggest",
                        url: app.config.host + "/Invoice/GetListCertificates",
                        body: {
                            columns: [
                                {
                                    id: "so_serial",
                                    header: _("SO_SERIAL"),
                                    width: 150
                                },
                                {
                                    id: "subject_name",
                                    header: _("SUBJECTNAME"),
                                    width: 550
                                },
                                {
                                    id: "begin_date",
                                    header: _("NGAY_BAT_DAU"),
                                    width: 150,
                                    format: function (text) {
                                        var format = webix.Date.dateToStr("%d/%m/%Y");
                                        text = removeTimeZoneT(text);
                                        return format(text);
                                    }
                                },
                                {
                                    id: "end_date",
                                    header: _("NGAY_KET_THUC"),
                                    width: 150,
                                    format: function (text) {
                                        var format = webix.Date.dateToStr("%d/%m/%Y");
                                        text = removeTimeZoneT(text);
                                        return format(text);
                                    }
                                }
                            ]
                        }

                    }
                },
                {
                    view: "template",
                    template: "Danh sách hóa đơn chờ ký",
                    type: "section"
                },
                {

                    view: "datatable",
                    select: "row",
                    id: "tblHoadon",
                    resizeColumn: true,
                    datafetch: 50,
                    on: {
                        "data->onStoreUpdated": function () {
                            this.data.each(function (obj, i) {
                                obj.index = i + 1;
                            })
                        }
                        // onKeyPress: function (code, e) {
                        //     var table = this;

                        //     if (e.key == "Enter") {
                        //         var win = this.getTopParentView();
                        //         var toolbar = win.getChildViews()[1].getChildViews()[0];

                        //         var btnChon = toolbar.getChildViews()[0];
                        //         btnChon.callEvent("onClick");
                        //     }
                        // }
                    },
                    columns: [
                        {
                            id: "chon1",
                            header: [{ text: "", content: "masterCheckbox" }],
                            width: 55,
                            checkValue: 'C',
                            uncheckValue: 'K',
                            template: "{common.checkbox()}"
                        },
                        { id: "index", sort: "int", header: "STT", width: 50 },
                        { id: "inv_InvoiceAuth_id", header: "ID hóa đơn", width: 250, hidden: true },
                        {
                            id: "invoice_issued_date", header: "Ngày hóa đơn", width: 150, format: function (text) {
                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                text = removeTimeZoneT(text);
                                return format(text);
                            }
                        },
                        { id: "inv_invoiceNumber", header: "Số hóa đơn" },
                        {
                            id: "total_amount", header: "Tổng tiền", width: 150, format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 0
                            })
                        }
                    ],
                    pager: "selectPagerA"
                },
                {
                    id: "selectPagerA",
                    view: "pager",
                    size: 50,
                    group: 5,
                    template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}"
                },
                {
                    margin: 10,
                    cols: [
                        {},
                        {
                            shortcut: "F9", view: "button", label: "Tải dữ liệu (F9)", type: "form", align: "center", width: 120,
                            on: {
                                onClick: function () {
                                    var form = webix.$$("chukyso-form");
                                    if (form.validate()) {
                                        var item = form.getValues();

                                        var parameter = {
                                            command: "CM00020",
                                            parameter: {
                                                ma_dvcs: user.getDvcsID(),
                                                tu_ngay: item.tu_ngay,
                                                den_ngay: item.den_ngay,
                                                inv_InvoiceCode_id: item.inv_InvoiceCode_id
                                            }
                                        };

                                        var url = app.config.host + "/System/ExecuteCommand";

                                        webix.extend(form, webix.ProgressBar);
                                        //form.disable();
                                        form.showProgress();

                                        webix.ajax()
                                            .headers({ 'Content-type': 'application/json' })
                                            .post(url, parameter, function (text) {

                                                var json = JSON.parse(text);

                                                if (!json.hasOwnProperty("error")) {

                                                    if (json.length == 0) {
                                                        webix.message("Không có dữ liệu", "error");
                                                        var table = $$("tblHoadon");
                                                        //table.load(table.config.url);
                                                        table.clearAll();
                                                        form.hideProgress();
                                                        return;
                                                    }
                                                    else {
                                                        form.hideProgress();
                                                        $$("btnNhan").enable();
                                                        var table = $$("tblHoadon");
                                                        //table.load(table.config.url);
                                                        table.clearAll();
                                                        var table = $$("tblHoadon");
                                                        table.parse(json);
                                                    }
                                                }
                                            });
                                    }
                                }
                            },
                            click: function () {
                                this.callEvent("onClick");
                            }
                        },
                        {
                            shortcut: "F10", view: "button", id: "btnNhan", name: "btnNhan", type: "form", label: "Nhận (F10)", align: "center",
                            on: {
                                onClick: function () {
                                    var form = webix.$$("chukyso-form");

                                    webix.extend(form, webix.ProgressBar);
                                    form.disable();
                                    form.showProgress();
                                    var json = $$("tblHoadon").serialize();

                                    if (json.length == 0) {

                                        webix.message("Không có dữ liệu !", "error");
                                        form.enable();
                                        form.hideProgress();
                                        return;
                                    }
                                    var win = $$("chukyso-win");

                                    var kytaptrung = win.config.kytaptrung;
                                    var server_cert = win.config.server_cert;

                                    if (kytaptrung == "FILE") {
                                        var box = webix.modalbox({
                                            title: "Đang ký hóa đơn .... ",
                                            buttons: ["Hủy bỏ"],
                                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                            width: 350,
                                            height: 200,
                                            callback: function (result) {
                                                switch (result) {
                                                    case "0":
                                                        //clearTimeout(interVal);
                                                        webix.modalbox.hide(box);
                                                        break;
                                                }
                                            }
                                        });
                                        var txtCer = $$("txtCerSerial");
                                        var cert = txtCer.getList().getSelectedItem();
                                        var d = 0;
                                        var d1 = 0;
                                        for (var i = 0; i < json.length; i++) {
                                            var item = json[i];

                                            if (item.chon1 != undefined) {
                                                if (item.chon1 == "C") {
                                                    d1 = d1 + 1;
                                                    console.log(d1);
                                                    var a = webix.ajax(app.config.host + "/Invoice/ExportXml?id=" + item.inv_InvoiceAuth_id);

                                                    webix.promise.all([a]).then(function (results) {
                                                        var a_result = results[0].json();

                                                        signOnServerFile(a_result, cert, server_cert);
                                                        d = d + 1;
                                                        console.log(d);

                                                        if (d == d1) {
                                                            webix.modalbox.hide(box);
                                                        }
                                                        form.enable();
                                                        form.hideProgress();
                                                        //webix.modalbox.hide(box);
                                                        //clearTimeout(interVal);

                                                    }).fail(function (err) {

                                                        form.enable();
                                                        form.hideProgress();

                                                        webix.alert({
                                                            title: "Lỗi !",
                                                            text: err,
                                                            type: "alert-error"
                                                        });
                                                    });
                                                }
                                            }
                                        }
                                        //console.log(d);

                                        // var interVal = setTimeout(function () {

                                        //     var txtCer = $$("txtCerSerial");
                                        //     var cert = txtCer.getList().getSelectedItem();

                                        //         clearTimeout(interVal);
                                        //         webix.modalbox.hide(box);

                                        // }, 500);

                                        var tableHD = webix.$$("tblHoadon");
                                        tableHD.clearAll();
                                        form.enable();
                                        form.hideProgress();

                                        //tableHD.load(tableHD.config.url);

                                        // var table = webix.$$("windowData");
                                        // table.clearAll();
                                        // table.load(table.config.url);
                                    }
                                    else {
                                        if (kytaptrung == "C") {
                                            var promises = [];

                                            for (var i = 0; i < json.length; i++) {

                                                if (json[i]["chon1"] != null && json[i]["chon1"] == "C") {
                                                    var a = webix.ajax().get(app.config.host + "/Invoice/ExportXml?id=" + json[i].inv_InvoiceAuth_id);
                                                    promises.push(a);
                                                }
                                            }

                                            webix.promise.all(promises).then(function (results) {
                                                var promises = [];

                                                var server_cert = win.config.server_cert;

                                                var txtCer = $$("txtCerSerial");
                                                var cert = txtCer.getList().getSelectedItem();

                                                for (var i = 0; i < results.length; i++) {
                                                    var json = results[i].json();

                                                    var parameters = {
                                                        xml: json["InvoiceXmlData"],
                                                        refUri: "#data",
                                                        certSerial: cert.so_serial,
                                                        tokenSerial: cert.tokenSerial,
                                                        signId: "seller",
                                                        signTag: "Signatures",
                                                        pass: cert.pass,
                                                        inv_InvoiceAuth_id: json["inv_InvoiceAuth_id"]
                                                    };

                                                    var a = webix.ajax()
                                                        .headers({ "Content-Type": "application/json" })
                                                        .post(server_cert + "/Token/SignXml", parameters, function (text) {
                                                        });

                                                    promises.push(a);
                                                }

                                                webix.promise.all(promises).then(function (results) {
                                                    var promises = [];

                                                    for (var i = 0; i < results.length; i++) {
                                                        var json = results[i].json();

                                                        var parameters = {
                                                            InvoiceXmlData: json.xml,
                                                            inv_InvoiceAuth_id: json.inv_InvoiceAuth_id
                                                        };

                                                        var a = webix.ajax()
                                                            .headers({ "Content-Type": "application/json" })
                                                            .post(server_cert + "/Invoice/SaveXml", parameters, function (text) {
                                                            });

                                                        promises.push(a);
                                                    }

                                                    webix.promise.all(promises).then(function (results) {
                                                        var table = webix.$$("windowData");
                                                        table.clearAll();
                                                        table.load(table.config.url);

                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();

                                                        var win = $$("chukyso-win");
                                                        win.close();
                                                    }).fail(function (err) {
                                                        alert(err);

                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();
                                                    });


                                                }).fail(function (err) {
                                                    alert(err);

                                                    var form = webix.$$("chukyso-form");
                                                    form.enable();
                                                    form.hideProgress();
                                                });

                                            }).fail(function (err) {
                                                alert(err);

                                                var form = webix.$$("chukyso-form");
                                                form.enable();
                                                form.hideProgress();
                                            });
                                        }
                                        else {
                                            var txtCer = $$("txtCerSerial");
                                            var cert = txtCer.getList().getSelectedItem();

                                            if (cert.cert_type == "FILE") {
                                                signCertFile();
                                                return;
                                            }

                                            var data = [];
                                            for (var i = 0; i < json.length; i++) {
                                                if (json[i]["chon1"] != null && json[i]["chon1"] == "C") {
                                                    data.push(json[i].inv_InvoiceAuth_id);
                                                }
                                            }
                                            //var url = app.config.host + "/Invoice/GetXmlSignInvoices";
                                            // var url = app.config.host + "/Invoice/GetXmlSignInvoices?inv_InvoiceCode_id=" + item.inv_InvoiceCode_id
                                            //     + "&tu_ngay=" + moment(item.tu_ngay).format("DD/MM/YYYY") + "&den_ngay=" + moment(item.den_ngay).format("DD/MM/YYYY") + "&inv_invoiceAuth_id=" + promises_auth_base;

                                            webix.extend(form, webix.ProgressBar);
                                            form.disable();
                                            form.showProgress();


                                            var hub = plugin.hub();
                                            var hubconnect = plugin.conn();
                                            if (hubconnect.state != 1) {
                                                var form = webix.$$("chukyso-form");
                                                form.enable();
                                                form.hideProgress();
                                                webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                return;
                                            }
                                            else {

                                                hub.off('resCommand');
                                                hub.on('resCommand', function (result) {

                                                    var json = JSON.parse(result);
                                                    //console.log(json);
                                                    if (json.hasOwnProperty("error")) {
                                                        webix.message({ type: 'error', text: json.error });

                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();
                                                    }
                                                    else {
                                                        var table = webix.$$("windowData");
                                                        table.clearAll();
                                                        table.load(table.config.url);

                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();

                                                        var win = $$("chukyso-win");
                                                        win.close();

                                                        // webix.message("Ký thành công !", "success");
                                                    }
                                                });

                                                // hub.off('resNotice');
                                                // hub.on('resNotice', function (result) {
                                                //     webix.message(result, "debug");
                                                // });

                                                var form = webix.$$("chukyso-form");
                                                var item = form.getValues();

                                                var token = user.getAccessToken();
                                                var dvcs = user.getDvcsID();
                                                var language = user.getLanguage();

                                                var bear = token + ";" + dvcs + ";" + language;
                                                var url;
                                                if (window.location.protocol == "file:") {
                                                    url = "http://localhost:18374";
                                                }
                                                else {
                                                    url = window.location.origin;
                                                }

                                                var arg = {
                                                    idData: "#data",
                                                    data: data,
                                                    // mst: "0100686209",
                                                    mst: "minvoice", // đang fix cứng.sau này có thể fix
                                                    username: user.getCurrentUser(),
                                                    idSigner: "seller",
                                                    tagSign: "Signatures",
                                                    token: bear,
                                                    url: url,
                                                    listcerts: [
                                                        {
                                                            so_serial: item.cerSerial
                                                        }
                                                    ]
                                                };

                                                hub.invoke('execCommand', 'SignListInvoiceV2', JSON.stringify(arg))
                                                    .done(function () {
                                                        console.log("Success SignListInvoiceV2!")
                                                    })
                                                    .fail(function (error) {
                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();
                                                        webix.message(error, "error");
                                                    });
                                            }


                                        }
                                    }
                                }
                            },
                            click: function () {
                                this.callEvent("onClick");
                            }
                        },
                        {
                            view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 100,
                            click: function () {
                                webix.$$("chukyso-win").close();
                            }
                        },
                        {}
                    ]
                }
            ]
        }
    };
    return {
        $ui: layout,
        setEditMode: function (mode) {
            editmode = mode;
        }
    };

});