define([
    "app",
    "locale",
    "models/user",
    "views/modules/minvoiceplugin"
], function (app, _, user, plugin) {
    var editmode = 0;

    return {
        $ui: {
            view: "window", modal: true, id: "chukyso-win", position: "center",
            head: "Ký hóa đơn chờ ký",
            //fullscreen: true,
            width: 600,
            height: 600,
            body: {
                paddingY: 10,
                paddingX: 10,
                view: "form",
                id: "chukyso-form",
                complexData: true,
                margin: 0,
                elements: [
                    {
                        cols: [
                            {
                                view: "datepicker",
                                name: "tu_ngay",
                                format: "%d/%m/%Y",
                                label: "Từ ngày",
                                width: 290,
                                labelPosition: "top"
                            },
                            {
                                view: "datepicker",
                                name: "den_ngay",
                                format: "%d/%m/%Y",
                                label: "Đến ngày",
                                width: 290,
                                labelPosition: "top"
                            },
                        ]
                    },
                    {
                        view: "combo",
                        name: "inv_InvoiceCode_id",
                        label: "Ký hiệu",
                        width: 600,
                        labelPosition: "top",
                        options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00029"
                    },
                    {
                        view: "combo",
                        width: 600,
                        name: "cerSerial",
                        label: "Chứng thư số",
                        labelPosition: "top",
                        id: "txtCerSerial",
                        options: {
                            view: "gridsuggest",
                            url: app.config.host + "/Invoice/GetListCertificates",
                            body: {
                                columns: [
                                    {
                                        id: "so_serial",
                                        header: _("SO_SERIAL"),
                                        width: 150
                                    },
                                    {
                                        id: "subjectname",
                                        header: _("SUBJECTNAME"),
                                        width: 550
                                    },
                                    {
                                        id: "ngaybatdau",
                                        header: _("NGAY_BAT_DAU"),
                                        width: 150,
                                        format: function (text) {
                                            var format = webix.Date.dateToStr("%d/%m/%Y");
                                            text = removeTimeZoneT(text);
                                            return format(text);
                                        }
                                    },
                                    {
                                        id: "ngayketthuc",
                                        header: _("NGAY_KET_THUC"),
                                        width: 150,
                                        format: function (text) {
                                            var format = webix.Date.dateToStr("%d/%m/%Y");
                                            text = removeTimeZoneT(text);
                                            return format(text);
                                        }
                                    }
                                ]
                            }

                        }
                    },
                    {
                        view: "template",
                        template: "Danh sách hóa đơn chờ ký",
                        type: "section"
                    },
                    {

                        view: "datatable",
                        select: "row",
                        id: "tblHoadon",
                        resizeColumn: true,
                        datafetch: 50,
                        on: {
                            "data->onStoreUpdated": function () {
                                this.data.each(function (obj, i) {
                                    obj.index = i + 1;
                                })
                            }
                            // onKeyPress: function (code, e) {
                            //     var table = this;

                            //     if (e.key == "Enter") {
                            //         var win = this.getTopParentView();
                            //         var toolbar = win.getChildViews()[1].getChildViews()[0];

                            //         var btnChon = toolbar.getChildViews()[0];
                            //         btnChon.callEvent("onClick");
                            //     }
                            // }
                        },
                        columns: [
                            {
                                id: "chon1",
                                header: [{ text: "", content: "masterCheckbox" }],
                                width: 55,
                                checkValue: 'C',
                                uncheckValue: 'K',
                                template: "{common.checkbox()}"
                            },
                            { id: "index", sort: "int", header: "STT", width: 50 },
                            { id: "inv_InvoiceAuth_id", header: "ID hóa đơn", width: 250, hidden: true },
                            {
                                id: "invoice_issued_date", header: "Ngày hóa đơn", width: 150, format: function (text) {
                                    var format = webix.Date.dateToStr("%d/%m/%Y");
                                    text = removeTimeZoneT(text);
                                    return format(text);
                                }
                            },
                            { id: "inv_invoiceNumber", header: "Số hóa đơn" },
                            {
                                id: "total_amount", header: "Tổng tiền", width: 150, format: webix.Number.numToStr({
                                    groupDelimiter: ",",
                                    groupSize: 3,
                                    decimalDelimiter: ".",
                                    decimalSize: 0
                                })
                            }
                        ],
                        pager: "selectPagerA"
                    },
                    {
                        id: "selectPagerA",
                        view: "pager",
                        size: 50,
                        group: 5,
                        template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}"
                    },
                    {
                        margin: 10,
                        cols: [
                            {},
                            {
                                shortcut: "F9", view: "button", label: "Tải dữ liệu (F9)", type: "form", align: "center", width: 120,
                                on: {
                                    onClick: function () {
                                        var form = webix.$$("chukyso-form");

                                        var item = form.getValues();

                                        var parameter = {
                                            command: "CM00020",
                                            parameter: {
                                                ma_dvcs: user.getDvcsID(),
                                                tu_ngay: item.tu_ngay,
                                                den_ngay: item.den_ngay,
                                                inv_InvoiceCode_id: item.inv_InvoiceCode_id
                                            }
                                        };

                                        var url = app.config.host + "/System/ExecuteCommand";

                                        webix.extend(form, webix.ProgressBar);
                                        //form.disable();
                                        form.showProgress();

                                        webix.ajax()
                                            .headers({ 'Content-type': 'application/json' })
                                            .post(url, parameter, function (text) {

                                                var json = JSON.parse(text);
                                              
                                                if (!json.hasOwnProperty("error")) {

                                                    if (json.length == 0) {
                                                        webix.message("Không có dữ liệu", "error");
                                                        var table = $$("tblHoadon");
                                                        //table.load(table.config.url);
                                                        table.clearAll();
                                                        form.hideProgress();
                                                        return;
                                                    }
                                                    else {
                                                        form.hideProgress();
                                                        $$("btnNhan").enable();
                                                        var table = $$("tblHoadon");
                                                        //table.load(table.config.url);
                                                        table.clearAll();
                                                        var table = $$("tblHoadon");
                                                        table.parse(json);
                                                    }
                                                }
                                            });
                                    }
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                }
                            },
                            {
                                shortcut: "F10", id: "btnNhan", name: "btnNhan", view: "button", label: "Nhận (F10)", align: "center", width: 80,
                                on: {
                                    onClick: function () {
                                        var form = webix.$$("chukyso-form");

                                        // var item = form.getValues();

                                        // var parameter = {
                                        //     command: "CM00020",
                                        //     parameter: {
                                        //         ma_dvcs: user.getDvcsID(),
                                        //         tu_ngay: item.tu_ngay,
                                        //         den_ngay: item.den_ngay,
                                        //         inv_InvoiceCode_id: item.inv_InvoiceCode_id
                                        //     }
                                        // };

                                        // var url = app.config.host + "/System/ExecuteCommand";

                                        webix.extend(form, webix.ProgressBar);
                                        form.disable();
                                        form.showProgress();
                                        var json = $$("tblHoadon").serialize();
                                        if (json.length == 0) {
                                            webix.message("Không có dữ liệu !", "error");
                                            form.enable();
                                            form.hideProgress();
                                            return;
                                        }
                                        //console.log(json);

                                        // webix.ajax()
                                        // .headers({ 'Content-type': 'application/json' })
                                        // .post(url, parameter, function (text) {

                                        //     var json = JSON.parse(text);

                                        //     if (!json.hasOwnProperty("error")) {

                                        //         if (json.length == 0) {
                                        //             webix.message("Không có dữ liệu");
                                        //             return;
                                        //         }

                                        //         console.log(json);
                                        //         console.log(json.data);

                                        // var dshoadon = this.$scope.ui(dshoadon.$ui);
                                        // dshoadon.$ui.show();
                                        var win = $$("chukyso-win");
                                        var server_cert = win.config.server_cert;

                                        if (server_cert == null) {
                                            var hub = plugin.hub();
                                            hub.off('resCommand');

                                            hub.on('resCommand', function (result) {
                                                var json = JSON.parse(result);

                                                if (json.hasOwnProperty("error")) {
                                                    webix.message({ type: 'error', text: json.error });

                                                    var form = webix.$$("chukyso-form");
                                                    form.enable();
                                                    form.hideProgress();
                                                }
                                                else {
                                                    var promises = [];

                                                    var array = json.data;

                                                    for (var i = 0; i < array.length; i++) {
                                                        var item = array[i];

                                                        var parameters = {
                                                            InvoiceXmlData: item.InvoiceXmlData,
                                                            inv_InvoiceAuth_id: item.inv_InvoiceAuth_id
                                                        };

                                                        var a = webix.ajax()
                                                            .headers({ "Content-Type": "application/json" })
                                                            .post(app.config.host + "/Invoice/SaveXml", parameters);

                                                        promises.push(a);
                                                    }

                                                    webix.promise.all(promises).then(function (results) {
                                                        var table = webix.$$("windowData");
                                                        table.clearAll();
                                                        table.load(table.config.url);

                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();

                                                        var win = $$("chukyso-win");
                                                        win.close();
                                                    });


                                                }
                                            });

                                            var promises = [];

                                            for (var i = 0; i < json.length; i++) {
                                                if (json[i].chon1 != null && json[i].chon1 != "") {
                                                    if (json[i].chon1 == "C") {
                                                        //console.log(json[i]);
                                                        var a = webix.ajax().get(app.config.host + "/Invoice/ExportXml?id=" + json[i].inv_InvoiceAuth_id);
                                                        promises.push(a);
                                                    }
                                                }

                                            }

                                            webix.promise.all(promises).then(function (results) {
                                                var arrays = [];

                                                for (var i = 0; i < results.length; i++) {
                                                    arrays.push(results[i].json());
                                                }

                                                var form = webix.$$("chukyso-form");
                                                var item = form.getValues();

                                                var arg = {
                                                    idData: "#data",
                                                    data: arrays,
                                                    idSigner: "seller",
                                                    tagSign: "Signatures",
                                                    listcerts: [
                                                        {
                                                            so_serial: item.cerSerial
                                                        }
                                                    ]
                                                };

                                                hub.invoke('execCommand', 'SignListInvoices', JSON.stringify(arg));
                                            });
                                        }
                                        else {

                                            var promises = [];

                                            // for (var i = 0; i < json.length; i++) {
                                            //     if (json[i].chon1 != null && json[i].chon1 != "") {
                                            //         if (json[i].chon1 == "C") {

                                            //             var a = webix.ajax().get(app.config.host + "/Invoice/ExportXml?id=" + json[i].inv_InvoiceAuth_id);
                                            //             promises.push(a);
                                            //         }
                                            //     }

                                            // }

                                            for (var i = 0; i < json.length; i++) {
                                                // console.log(json[i]);
                                                if (json[i].chon1 == null && json[i].chon1 == "") {
                                                    continue;
                                                }
                                                else {
                                                    if (json[i].chon1 == "C") {
                                                        //console.log(json[i]);
                                                        var a = webix.ajax().get(app.config.host + "/Invoice/ExportXml?id=" + json[i].inv_InvoiceAuth_id);

                                                        //var text = xhr.responseText;
                                                        // var b = JSON.parse(a.responseText);
                                                        //console.log(a);
                                                        promises.push(a);

                                                    }
                                                }

                                            }

                                            webix.promise.all(promises).then(function (results) {
                                                var promises = [];
                                                console.log(results);
                                                var server_cert = win.config.server_cert;


                                                var txtCer = $$("txtCerSerial");
                                                var cert = txtCer.getList().getSelectedItem();

                                                for (var i = 0; i < results.length; i++) {
                                                    var json = results[i].json();

                                                    var parameters = {
                                                        xml: json["InvoiceXmlData"],
                                                        refUri: "#data",
                                                        certSerial: cert.so_serial,
                                                        tokenSerial: cert.tokenSerial,
                                                        signId: "seller",
                                                        signTag: "Signatures",
                                                        pass: cert.pass,
                                                        inv_InvoiceAuth_id: json["inv_InvoiceAuth_id"]
                                                    };

                                                    var a = webix.ajax()
                                                        .headers({ "Content-Type": "application/json" })
                                                        .post(server_cert + "/Token/SignXml", parameters, function (text) {
                                                            //console.log(text);
                                                        });

                                                    promises.push(a);
                                                }

                                                webix.promise.all(promises).then(function (results) {
                                                    var promises = [];

                                                    for (var i = 0; i < results.length; i++) {
                                                        var json = results[i].json();

                                                        var parameters = {
                                                            InvoiceXmlData: json.xml,
                                                            inv_InvoiceAuth_id: json.inv_InvoiceAuth_id
                                                        };

                                                        var a = webix.ajax()
                                                            .headers({ "Content-Type": "application/json" })
                                                            .post(server_cert + "/Invoice/SaveXml", parameters, function (text) {
                                                                //console.log(text);
                                                            });

                                                        promises.push(a);
                                                    }
                                                    webix.promise.all(promises).then(function (results) {
                                                        var table = webix.$$("windowData");
                                                        table.clearAll();
                                                        table.load(table.config.url);

                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();

                                                        var win = $$("chukyso-win");
                                                        win.close();
                                                    }).fail(function (err) {
                                                        alert(err);

                                                        var form = webix.$$("chukyso-form");
                                                        form.enable();
                                                        form.hideProgress();
                                                    });


                                                }).fail(function (err) {
                                                    alert(err);

                                                    var form = webix.$$("chukyso-form");
                                                    form.enable();
                                                    form.hideProgress();
                                                });

                                            }).fail(function (err) {
                                                alert(err);

                                                var form = webix.$$("chukyso-form");
                                                form.enable();
                                                form.hideProgress();
                                            });

                                        }
                                    }
                                    // else {
                                    //     webix.message(json.error);

                                    //     var form = webix.$$("chukyso-form");
                                    //     form.enable();
                                    //     form.hideProgress();

                                    // }

                                    //});
                                    // }
                                },
                                click: function () {
                                    this.callEvent("onClick");

                                }
                            },
                            {
                                view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 100,
                                click: function () {
                                    webix.$$("chukyso-win").close();
                                }
                            },
                            {}
                        ]
                    }

                ]
            }
            // onBeforeShow: function()
            // {
            //     webix.$$("btnNhan").define("disabled", true);
            //     webix.$$('btnNhan').disable();
            // }

        },
        // $oninit: function (view, $scope) {
        //     webix.$$("btnNhan").disable();
        // },
        setEditMode: function (mode) {
            editmode = mode;
        }
    };

});