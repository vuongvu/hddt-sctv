define(["app", "locale"], function (app, _) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "chukyso-win", position: "center",
			head: "Thông tin chứng thư số",
			// height: 490,
			body: {
				paddingY: 10,
				paddingX: 10,
				view: "form",
				id: "chukyso-form",
				complexData: true,
				margin: 0,
				elements: [
					{
						view: "text",
						name: "cer_serial",
						labelPosition: "top",
						label: _("SO_SERIAL"),
						readonly: true,
						css: "mcls_readonly_text",
						width: 550
					},
					{
						view: "textarea",
						name: "subject_name",
						labelPosition: "top",
						label: _("SUBJECTNAME"),
						readonly: true,
						css: "mcls_readonly_text",
						height: 100
					},
					{
						view: "textarea",
						name: "issuer",
						labelPosition: "top",
						label: _("DONVI"),
						css: "mcls_readonly_text",
						readonly: true,
						height: 80
					},
					{
						cols: [
							{
								view: "datepicker",
								label: _("NGAY_BAT_DAU"),
								labelPosition: "top",
								name: "begin_date",
								timepicker: true,
								format: "%d/%m/%Y %H:%i:%s",
								css: "mcls_readonly_text",
								readonly: true
							},
							{
								view: "datepicker",
								label: _("NGAY_KET_THUC"),
								labelPosition: "top",
								name: "end_date",
								format: "%d/%m/%Y %H:%i:%s",
								timepicker: true,
								css: "mcls_readonly_text",
								readonly: true
							}
						]
					},
					{
						view: "text",
						name: "token_serial",
						label: _("TOKEN_SERIAL"),
						labelPosition: "top",
						css: "mcls_readonly_text",
						readonly: true
					},
					{
						view: "text",
						name: "signer",
						label: _("NGUOI_KY"),
						labelPosition: "top",
					},

					/* {
						view: "checkbox",
						name: "chon",
						label: _("CHON"),
						checkValue: true,
						uncheckValue: false,
					}, */
					{},
					{
						margin: 10,
						cols: [
							{},
							{
								shortcut: "F10", view: "button", label: "Lưu (F10)", type: "form", align: "center", width: 120,
								on: {
									onClick: function () {
										var form = webix.$$("chukyso-form");

										var item = form.getValues();

										var khData = {};

										khData.windowid = app.path[1].params[0];
										khData.editmode = editmode;
										khData.data = [];

										khData.data.push(item);

										webix.ajax()
											.headers({ 'Content-type': 'application/json' })
											.post(app.config.host + "/System/Save", khData, function (text) {

												var json = JSON.parse(text);

												if (!json.hasOwnProperty("error")) {
													$$("chukyso-win").close();
													app.callEvent("dataChanged", [editmode, json.data]);
												}
												else {
													webix.message(json.error);
												}


											});
									}
								},
								click: function () {
									this.callEvent("onClick");

								}
							},
							{
								view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 120,
								click: function () {
									webix.$$("chukyso-win").close();
								}
							},
							{}
						]
					}

				]
			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});