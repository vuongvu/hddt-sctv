define(["app"],function(app){
	var editmode = 0;
	var col=[];
					
	var el = [];
	var head_name=[];
	var head;
    var url = app.config.host + "/api/System/GetAllByWindowId?id=09a8b24b-52b5-46a6-931d-d6f005da0dd7";
    webix.ajax(url, function (text, xml, xhr) {

        var rp = JSON.parse(text);
        head_name.push(String(rp['data'][0]['Tabs'][0]['TAB_NAME']));
        for (var i = 0; i < (rp['data'][0]['ROWS_MAX']); i++) {
			var col1=[];
			
			for (var j = 0; j < (Object.keys(rp['data'][0]['Tabs'][0]['Fields']).length); j++) {
				
				if(i==(Number(rp['data'][0]['Tabs'][0]['Fields'][j]['ROW'])-1)){
					var el1=[];
					col1.push({ view:"text", name: '' + String(rp['data'][0]['Tabs'][0]['Fields'][j]['COLUMN_NAME']) + '',label: '' + String(rp['data'][0]['Tabs'][0]['Fields'][j]['COLUMN_NAME']) + '' , 
			  			id: 'dmhv-'+ String(rp['data'][0]['Tabs'][0]['Fields'][j]['COLUMN_NAME']) + '',width:Number(rp['data'][0]['Tabs'][0]['Fields'][j]['WIDTH'])});
					
						  
				}
				
							
			}
			
			if(col1.length>0){
				el.push({
				cols:col1
			});
			}
			
        }
		col.push({});
		col.push({ 
			shortcut:"F10",  view:"button", label:"Nhận", type:"form", align:"center", 
			on: {
				onClick: function() {
					var form = webix.$$("dmhv-form");
						var item = form.getValues();
						var hvData={};
						hvData.windowno=String(rp['data'][0]['ID']);
						hvData.editmode=editmode;
						hvData.data=[];
						hvData.data.push(item);
						console.log(item);
						console.log(hvData);
						webix.ajax().post( app.config.host + "/api/System/Save", hvData, function(text){
						var data = JSON.parse(text);
											
						if (!data.hasOwnProperty("error")){
							item.id = data.id;
							webix.$$("dmhv-win").close();
							app.callEvent("dataChanged",[editmode,item]);
						}
						else {
							webix.message(data.error);
						}
					});
									}
				},
			click:function(){								
				this.callEvent("onClick");							
									
			}
		});	
		col.push({ view:"button", hotkey:"esc", label:"Hủy bỏ",align:"center", 
			click:function(){
				webix.$$("dmhv-win").close();
			}
		});	
		col.push({});	
		el.push({
			margin:10,
			cols:col
		});
        $$("dmhvData").define("elements",el );;
        $$("dmhvData").refresh();
		//console.log(head_name[0]);
    });
	  
	return {		
		$ui:{
			view:"window", modal:true, id:"dmhv-win", position:"center",
			head:'' + head + '',
			//fullscreen:true,
			//width: 350,
			body:{
                paddingY:20, 
                paddingX:30, 
                elementsConfig:{labelWidth: 80}, 
                view:"form", 
				id:"dmhv-form", 
				complexData:true,
                elements:
                    el
					// {
					// 	margin:10,
					// 	cols:[
					// 		{},
					// 		{ 
					// 			shortcut:"F10",  view:"button", label:"Nhận", type:"form", align:"center", width:120, 
					// 			on: {
					// 				onClick: function() {
					// 					var form = webix.$$("dmhv-form");
					// 					var item = form.getValues();

					// 					webix.ajax().post( app.config.host + "/api/Dmhv/Update", item, function(text){
					// 						var data = JSON.parse(text);
											
					// 						if (!data.hasOwnProperty("error")){
					// 							item.id = data.id;
					// 							webix.$$("dmhv-win").close();
					// 							app.callEvent("dataChanged",[editmode,item]);
					// 						}
					// 						else {
					// 							webix.message(data.error);
					// 						}
					// 					});
					// 				}
					// 			},
					// 			click:function(){								
					// 				this.callEvent("onClick");							
									
					// 			}
					// 		},
					// 		{ view:"button", hotkey:"esc", label:"Hủy bỏ",align:"center", width:120, 
					// 			click:function(){
					// 				webix.$$("dmhv-win").close();
					// 			}
					// 		}
					// 	]
					// }

				
			}			
			
		},		
		setEditMode: function(mode) {
			editmode = mode;
		}
	};

});