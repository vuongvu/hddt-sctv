define(["app", "locale", "models/user", "models/findjson", "views/commonHD"], function (app, _, user, findJson, commonHD) {
    var editmode = 0;
    var tongtienHDonBke = 0;
    var mau04 = {};
    formatSoDefault = {
        groupDelimiter: ",",
        groupSize: 3,
        decimalDelimiter: ".",
        decimalSize: 0,
    };

    var el = [];

    function renderFormat(val) {
        if (typeof val == "undefined" || val == null || val == "" || val == 0) val = "VND";
        var url = app.config.host + "/Invoice68/GetDecimalPlace?ma_nt=" + val;
        var table_chitiet = webix.$$("hoadon68_chitiet");
        var table_phi = webix.$$("hoadon68_phi");
        var table = $$("invoiceForms");

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .get(url, function (text) {
                var arr = JSON.parse(text);
                var decimal_place = Array(Number(arr[0]["hoadon123"]) + 1).join("9");
                var decimal_format = Number(arr[0]["hoadon123"]);
                var format_numeric = {
                    maximumValue: Number("999999999999." + decimal_place),
                    minValue: Number("-999999999999." + decimal_place),
                    decimalPlaces: Number(decimal_format),
                };
                formatSoDefault = {
                    groupDelimiter: ",",
                    groupSize: 3,
                    decimalDelimiter: ".",
                    decimalSize: decimal_format,
                };

                var formatSo = webix.Number.numToStr(formatSoDefault);

                var arrayString_chitiet = ["sluong", "dgia", "thtien", "stckhau", "tthue", "tgtien"];
                for (var i = 0; i < arrayString_chitiet.length; i++) {
                    var format_config = table_chitiet.getColumnConfig(arrayString_chitiet[i]);
                    if (format_config.isFmNumber === true) {
                        format_config.format = formatSo;
                        format_config.numericConfig = format_numeric;
                    }
                }

                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];

                var format_config_phi = table_phi.getColumnConfig("tienphi");
                if (format_config.isFmNumber === true) {
                    format_config_phi.format = formatSo;
                    format_config_phi.numericConfig = format_numeric;
                }

                table_chitiet.refreshColumns();
                table_phi.refreshColumns();

                for (var i = 0; i < arrayString.length; i++) {
                    $$(arrayString[i]).refresh();
                }
            });
    }

    //FormButton
    var cols = [
        {
            margin: 10,
            cols: [
                {},
                {
                    shortcut: "F10",
                    view: "button",
                    id: "btnBangKeSave",
                    label: "Lưu (F10)",
                    type: "form",
                    align: "center",
                    width: 120,
                    hidden: true,
                    on: {
                        onClick: function () {
                            var self = this;
                            setTimeout(function () {
                                var tongtienBangKeCT = webix.$$("tgtttbso").getValue();
                                var item = $$("invoiceForms").getValues();
                                var table = $$("invoiceForms");
                                var record = $$("hoadon68_chitiet").serialize();
                                var mauso = $$("khieu").getValue().toString()[0];
                                for (var i = 0; i < record.length; i++) {
                                    var it = record[i];

                                    if (it["ten"] == null || it["ten"] == "") {
                                        webix.message("Bạn chưa nhập tên hàng");
                                        return;
                                    }
                                    if (it.kmai.toString() === "1") {
                                        var bool = Number(it.tgtien) == Number(it.thtien) + Number(it.tthue);
                                        if (!bool) {
                                            webix.message("Tiền trước thuế + Tiền thuế != tiền sau thuế");
                                            return;
                                        }
                                        if ((it["tsuat"] == null || it["tsuat"] == "") && (mauso === "1" || mauso === "5")) {
                                            webix.message("Bạn chưa nhập thuế suất hàng hóa " + it["ten"]);
                                            return;
                                        }
                                    }
                                }

                                if (tongtienHDonBke != tongtienBangKeCT) {
                                    webix.message("Tổng tiền chi tiết bảng kê khác tổng tiền của hoá đơn!", "error");
                                    return false;
                                }

                                if (table.validate()) {
                                    // var fn = Function(check);
                                    // var result = fn();
                                    // console.log(result);
                                    webix.extend(table, webix.ProgressBar);
                                    table.disable();
                                    table.showProgress();

                                    var details = [];
                                    details.push({ data: record });
                                    item["details"] = details;
                                    var phi = [];
                                    phi.push({
                                        data: $$("hoadon68_phi").serialize(),
                                    });
                                    item["hoadon68_phi"] = phi;
                                    // hóa đơn không mã
                                    item.is_hdcma = 1;
                                    var item1 = [];
                                    item1.push(item);
                                    var data_post = { editmode: editmode, data: item1 };

                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Invoice68/hoadonSaveChange", data_post, {
                                            error: function (text, data, XmlHttpRequest) {
                                                table.enable();
                                                table.hideProgress();
                                                // var json = JSON.parse(text);
                                                // webix.message(json.ExceptionMessage, "error");
                                                webix.message(text + XmlHttpRequest, "error");
                                            },
                                            success: function (text, data, XmlHttpRequest) {
                                                var json = JSON.parse(text);
                                                table.enable();
                                                table.hideProgress();
                                                if (!json.hasOwnProperty("error")) {
                                                    webix.message("Thêm bảng kê thành công !", "success");
                                                    webix.$$("invoice68").close();
                                                    app.callEvent("dataChangedInvoie68", [editmode, json.data]);
                                                } else {
                                                    webix.message(json.error, "error");
                                                }
                                            },
                                        });
                                }
                            }, 100);
                        },
                    },
                    click: function () {
                        this.callEvent("onClick");
                    },
                },
                {
                    view: "button",
                    id: "btnBangKeDelete",
                    label: "Xoá",
                    type: "danger",
                    align: "center",
                    width: 80,
                    hidden: true,
                    on: {
                        onClick: function () {
                            var self = this;
                            setTimeout(function () {
                                var item = $$("invoiceForms").getValues();
                                var url = app.config.host + "/Invoice68/DeleteBangKeHoaDon?id=" + item.id;
                                var table = $$("invoiceForms");
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();
                                webix
                                    .ajax()
                                    .headers({ "Content-Type": "application/json" })
                                    .post(url, function (result) {
                                        if (result == "true") {
                                            webix.message("Đã xoá bảng kê !");
                                            table.enable();
                                            table.hideProgress();
                                            webix.$$("invoice68").close();
                                            app.callEvent("dataChangedInvoie68", [editmode, null]);
                                        } else {
                                            webix.message("Xoá không thành công !", "error");
                                        }
                                    });
                            }, 100);
                        },
                    },
                    click: function () {
                        this.callEvent("onClick");
                    },
                },
                {
                    shortcut: "F10",
                    view: "button",
                    id: "btnHoadonSave",
                    label: "Lưu tạm (F10)",
                    type: "form",
                    align: "center",
                    width: 120,
                    on: {
                        onClick: function () {
                            var self = this;
                            setTimeout(function () {
                                var item = $$("invoiceForms").getValues();
                                if (item.nlap_temp != null) {
                                    var nlap_temp = new Date(item.nlap_temp);
                                    if ($$("nlap").getValue() < nlap_temp) {
                                        webix.message("Ngày hoá đơn thay thế hoặc điều chỉnh không được nhỏ hơn hoá đơn gốc!", "error");
                                        return;
                                    }
                                }
                                var table = $$("invoiceForms");
                                var record = $$("hoadon68_chitiet").serialize();
                                var mauso = $$("khieu").getValue().toString()[0];
                                for (var i = 0; i < record.length; i++) {
                                    var it = record[i];

                                    if (it["ten"] == null || it["ten"] == "") {
                                        webix.message("Bạn chưa nhập tên hàng");
                                        return;
                                    }
                                    if (it.kmai.toString() === "1") {
                                        /*var bool = Number(it.tgtien) == Number(it.thtien) + Number(it.tthue);
                                    if (!bool) {
                                        webix.message("Tiền trước thuế + Tiền thuế != tiền sau thuế");
                                        return;
                                    }*/
                                        if ((it["tsuat"] == null || it["tsuat"] == "") && (mauso === "1" || mauso === "5")) {
                                            webix.message("Bạn chưa nhập thuế suất hàng hóa " + it["ten"]);
                                            return;
                                        }
                                    }
                                }

                                if (table.validate()) {
                                    var TTKhac = [];
                                    Object.keys(item).forEach(function (key) {
                                        if (key.includes("TTKhac_")) {
                                            var test = $$(key);
                                            if (test.config.editor !== "nummeric" || test.config.editor !== "autonumeric")
                                                TTKhac.push({
                                                    ttruong: key.replace("TTKhac_", ""),
                                                    kdlieu: "decimal",
                                                    dlieu: item[key],
                                                });
                                            else if (test.config.view !== "text") TTKhac.push({ TTruong: key, KDLieu: "string", DLieu: item[key] });
                                            else if (test.config.view !== "datepicker") TTKhac.push({ TTruong: key, KDLieu: "dateTime", DLieu: item[key] });
                                        }
                                    });
                                    var TTKhacItem = [];
                                    TTKhacItem.push({
                                        data: TTKhac,
                                    });
                                    item["hoadon68_khac"] = TTKhacItem;
                                    // var fn = Function(check);
                                    // var result = fn();
                                    // console.log(result);
                                    webix.extend(table, webix.ProgressBar);
                                    table.disable();
                                    table.showProgress();

                                    var details = [];
                                    details.push({ data: record });
                                    item["details"] = details;
                                    var phi = [];
                                    phi.push({
                                        data: $$("hoadon68_phi").serialize(),
                                    });
                                    item["hoadon68_phi"] = phi;
                                    // hóa đơn không mã
                                    item.is_hdcma = 1;
                                    var item1 = [];
                                    item1.push(item);
                                    var data_post = { editmode: editmode, data: item1 };

                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Invoice68/hoadonSaveChange", data_post, {
                                            error: function (text, data, XmlHttpRequest) {
                                                table.enable();
                                                table.hideProgress();
                                                // var json = JSON.parse(text);
                                                // webix.message(json.ExceptionMessage, "error");
                                                webix.message(text + XmlHttpRequest, "error");
                                            },
                                            success: function (text, data, XmlHttpRequest) {
                                                var json = JSON.parse(text);
                                                table.enable();
                                                table.hideProgress();
                                                if (!json.hasOwnProperty("error")) {
                                                    webix.$$("invoice68").close();
                                                    if (item.hdon_id_old !== null && item.hdon_id === "" && item.tthai_old != "Đã ký") {
                                                        commonHD.ExcuteShowPopupNotifyErrorInvoice();
                                                    } else {
                                                        app.callEvent("dataChangedInvoie68", [editmode, json.data]);
                                                    }
                                                } else {
                                                    webix.message(json.error, "error");
                                                }
                                            },
                                        });
                                }
                            }, 100);
                        },
                    },
                    click: function () {
                        this.callEvent("onClick");
                    },
                },
                {
                    shortcut: "F11",
                    view: "button",
                    id: "btnHoadonCQT",
                    label: "Ký và gửi CQT (F11)",
                    type: "form",
                    align: "center",
                    width: 150,
                    on: {
                        onClick: function () {
                            var self = this;
                            setTimeout(function () {
                                var item = $$("invoiceForms").getValues();
                                if (item.nlap_temp != null) {
                                    var nlap_temp = new Date(item.nlap_temp);
                                    if ($$("nlap").getValue() < nlap_temp) {
                                        webix.message("Ngày hoá đơn thay thế hoặc điều chỉnh không được nhỏ hơn hoá đơn gốc!", "error");
                                        return;
                                    }
                                }
                                var table = $$("invoiceForms");
                                var record = $$("hoadon68_chitiet").serialize();
                                var mauso = $$("khieu").getValue().toString()[0];
                                for (var i = 0; i < record.length; i++) {
                                    var it = record[i];
                                    if (it["ten"] == null || it["ten"] == "") {
                                        webix.message("Bạn chưa nhập tên hàng");
                                        return;
                                    }
                                    if (it.kmai.toString() === "1") {
                                        /*var bool = Number(it.tgtien) == Number(it.thtien) + Number(it.tthue);
                                    if (!bool) {
                                        webix.message("Tiền trước thuế + Tiền thuế != tiền sau thuế");
                                        return;
                                    }*/
                                        if ((it["tsuat"] == null || it["tsuat"] == "") && (mauso === "1" || mauso === "5")) {
                                            webix.message("Bạn chưa nhập thuế suất hàng hóa " + it["ten"]);
                                            return;
                                        }
                                    }
                                }
                                if (table.validate()) {
                                    var TTKhac = [];
                                    Object.keys(item).forEach(function (key) {
                                        if (key.includes("TTKhac_")) {
                                            var test = $$(key);
                                            if (test.config.editor !== "nummeric" || test.config.editor !== "autonumeric")
                                                TTKhac.push({
                                                    ttruong: key.replace("TTKhac_", ""),
                                                    kdlieu: "decimal",
                                                    dlieu: item[key],
                                                });
                                            else if (test.config.view !== "text") TTKhac.push({ TTruong: key, KDLieu: "string", DLieu: item[key] });
                                            else if (test.config.view !== "datepicker") TTKhac.push({ TTruong: key, KDLieu: "dateTime", DLieu: item[key] });
                                        }
                                    });
                                    var TTKhacItem = [];
                                    TTKhacItem.push({
                                        data: TTKhac,
                                    });
                                    item["hoadon68_khac"] = TTKhacItem;
                                    // var fn = Function(check);
                                    // var result = fn();
                                    // console.log(result);
                                    webix.extend(table, webix.ProgressBar);
                                    table.disable();
                                    table.showProgress();

                                    var details = [];
                                    details.push({ data: $$("hoadon68_chitiet").serialize() });
                                    item["details"] = details;
                                    var phi = [];
                                    phi.push({
                                        data: $$("hoadon68_phi").serialize(),
                                    });
                                    item["hoadon68_phi"] = phi;
                                    // hóa đơn không mã
                                    item.is_hdcma = 1;
                                    var item1 = [];
                                    item1.push(item);
                                    var data_post = { editmode: editmode, data: item1 };

                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Invoice68/hoadonSaveChange", data_post, {
                                            error: function (text, data, XmlHttpRequest) {
                                                table.enable();
                                                table.hideProgress();
                                                // var json = JSON.parse(text);
                                                // webix.message(json.ExceptionMessage, "error");

                                                webix.message(text + XmlHttpRequest, "error");
                                            },
                                            success: function (text, data, XmlHttpRequest) {
                                                var _res = JSON.parse(text);
                                                var json = JSON.parse(text);
                                                table.enable();
                                                table.hideProgress();
                                                if (!_res.hasOwnProperty("error")) {
                                                    webix.$$("invoice68").close();
                                                    app.callEvent("dataChangedInvoie68", [editmode, _res.data]);

                                                    var lsthdon_id = [];
                                                    lsthdon_id.push(_res.data.hdon_id);
                                                    commonHD.ExcuteHoaDon(lsthdon_id, true, true);
                                                } else {
                                                    webix.message(json.error, "error");
                                                }
                                            },
                                        });
                                }
                            }, 100);
                        },
                    },
                    click: function () {
                        this.callEvent("onClick");
                    },
                },
                {
                    view: "button",
                    hotkey: "esc",
                    label: "Hủy bỏ (ESC)",
                    align: "center",
                    type: "danger",
                    width: 120,
                    click: function () {
                        this.getTopParentView().close();
                        app.callEvent("dataChangedInvoie68", [editmode, null]);
                    },
                },
                {},
            ],
        },
    ];
    //FormDetail Tạo item trên trang from từ tab và data

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            css: "headformchil",
            head: {
                //view: "toolbar",
                // css: { "box-shadow": "0 -2px #3498db inset !important", "background": "#ffffff !important" },
                //css: { "background": "#e6f2ff !important" },
                height: 35,
                cols: [
                    {
                        id: "head-iconInvoice",
                        view: "template",
                        css: "headform",
                        width: 35,
                        template: "",
                    },
                    {
                        id: "head-formInvoice",
                        view: "template",
                        css: "headform",
                        template: function (obj) {
                            return "<div>Test Form<div/>";
                        },
                    },
                    {
                        view: "icon",
                        icon: "fas fa-question-circle",
                        id: "help_InvoiceForms",
                        tooltip: _("HELPER"),
                        click: function () {
                            // var form = webix.$$("window-form");
                            // hint_form(form.config.hint);
                        },
                    },
                ],
            },
            id: "invoice68",
            resize: true,
            fullscreen: true,
            move: true,
            on: {
                onBeforeShow: function () {},
            },
            body: {
                paddingY: 10,
                paddingX: 10,
                elementsConfig: { labelWidth: 100 },
                view: "form",
                id: "invoiceForms",
                width: 800,
                height: 500,
                complexData: true,
                margin: 0,
                on: {
                    // onViewShow: function () {
                    // }
                },
                elements: el,
            },
        },
        setValues: function (item, winconfig, params) {
            var win = $$(this.$ui.id);
            var form = win.getChildViews()[1];
            if (item != null) {
                if (item["tdlap"] !== undefined) item["tdlap"] = removeTimeZoneT(item["tdlap"]);
                if (item["cqqdbants"] !== undefined) item["cqqdbants"] = removeTimeZoneT(item["cqqdbants"]);
                if (item["tgvchdtu"] !== undefined) item["tgvchdtu"] = removeTimeZoneT(item["tgvchdtu"]);
                if (item["tgvchdden"] !== undefined) item["tgvchdden"] = removeTimeZoneT(item["tgvchdden"]);
                if (item["nqdbants"] !== undefined) item["nqdbants"] = removeTimeZoneT(item["nqdbants"]);
                if (item["nbke"] !== undefined) item["nbke"] = removeTimeZoneT(item["nbke"]);
                if (item["hdktngay"] !== undefined) item["hdktngay"] = removeTimeZoneT(item["hdktngay"]);
                item["giamthuebanhang20"] = item["giamthuebanhang20"] == true ? 1 : 0;
                if (item["nbke"] !== undefined) item["nbke"] = removeTimeZoneT(item["nbke"]);

                form.setValues(item);

                var tableDetail = webix.$$("hoadon68_chitiet");
                var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
                tableDetail.clearAll();
                tableDetail.load(url);
                var tablePhi = webix.$$("hoadon68_phi");
                var urlPhi = app.config.host + "/Invoice68/GetDataInvoicePhi?id=" + item.id;
                tablePhi.clearAll();
                tablePhi.load(urlPhi);
                var urlKhac = app.config.host + "/Invoice68/GetDataInvoiceKhac?id=" + item.id;
                webix
                    .ajax()
                    .headers({ "Content-type": "application/json" })
                    .get(urlKhac, function (text) {
                        var data = JSON.parse(text);
                        for (var i = 0; i < data.length; i++) {
                            try {
                                if ($$("TTKhac_" + data[i].ttruong) !== undefined && $$("TTKhac_" + data[i].ttruong) !== null) $$("TTKhac_" + data[i].ttruong).setValue(data[i]["dlieu"]);
                            } catch (e) {}
                        }
                    });
            }

            if (editmode == 2) {
            }
            // nếu có mẫu 04 (hóa đơn hủy)
            if (winconfig != null) {
                mau04 = winconfig;
                $$("btnHoadonSaveMau04").show();
                $$("btnHoadonCQT").destructor();
                $$("btnHoadonSave").destructor();
            }
            this.getFormat(item["dvtte"]);
        },
        setValuesBangKe: function (item, winconfig, params) {
            var win = $$(this.$ui.id);
            tongtienHDonBke = item.tgtttbso;
            var form = win.getChildViews()[1];
            if (item != null) {
                item["tdlap"] = removeTimeZoneT(item["tdlap"]);
                form.setValues(item);

                var tableDetail = webix.$$("hoadon68_chitiet");
                var url = "";
                if (editmode == 4) {
                    url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
                } else {
                    url = app.config.host + "/Invoice68/GetDataInvoiceDetail_BangKe?id=" + item.id;
                }
                tableDetail.clearAll();
                tableDetail.load(url);
            }
            // nếu có mẫu 04 (hóa đơn hủy)
            if (winconfig != null) {
                mau04 = winconfig;
                $$("btnHoadonSaveMau04").show();
                $$("btnHoadonCQT").destructor();
                $$("btnHoadonSave").destructor();
            }
            this.getFormat(item["dvtte"]);
        },
        setEditMode: function (mode) {
            editmode = mode;
            //var form = webix.$$("invoiceForms");
            var form = $$(this.$ui.id);
            //var topParentView = form.getTopParentView();
            form.config.editmode = mode;
            var icon = "";
            if (editmode == 1 || editmode == 4) {
                icon = "<span><i class='fa fa-plus' aria-hidden='true'></i></span>";
            } else {
                if (editmode == 2 || editmode == 5) icon = "<i class='fa fa-edit' aria-hidden='true'></i>";
                else icon = "<i class='fa fa-trash fa-lg' aria-hidden='true'></i>";
            }

            webix.$$("head-iconInvoice").setHTML(icon);
            webix.$$("head-iconInvoice").refresh();
        },
        copyValues: function (item) {
            var form = webix.$$("invoiceForms");

            var tableDetail = webix.$$("hoadon68_chitiet");
            var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
            webix
                .ajax()
                .headers({ "Content-type": "application/json" })
                .get(url, function (text) {
                    var data = JSON.parse(text);

                    for (var i = 0; i < data.length; i++) {
                        data[i]["cthdon_id"] = null;
                        data[i]["hdon_id"] = null;
                    }

                    tableDetail.clearAll();
                    tableDetail.parse(data);
                });
            var tablePhi = webix.$$("hoadon68_phi");
            var urlPhi = app.config.host + "/Invoice68/GetDataInvoicePhi?id=" + item.id;
            webix
                .ajax()
                .headers({ "Content-type": "application/json" })
                .get(urlPhi, function (text) {
                    var data = JSON.parse(text);

                    for (var i = 0; i < data.length; i++) {
                        data[i]["phi_id"] = null;
                        data[i]["hdon_id"] = null;
                    }

                    tablePhi.clearAll();
                    tablePhi.parse(data);
                });
            var urlKhac = app.config.host + "/Invoice68/GetDataInvoiceKhac?id=" + item.id;
            webix
                .ajax()
                .headers({ "Content-type": "application/json" })
                .get(urlKhac, function (text) {
                    var data = JSON.parse(text);
                    for (var i = 0; i < data.length; i++) {
                        try {
                            if ($$("TTKhac_" + data[i].ttruong) !== undefined && $$("TTKhac_" + data[i].ttruong) !== null) $$("TTKhac_" + data[i].ttruong).setValue(data[i]["dlieu"]);
                        } catch (e) {}
                    }
                });
            // tableDetail.clearAll();
            // tableDetail.load(url);
            item["tthdon"] = 0;
            item["tthdon_original"] = 0;
            item["tthdon_old"] = 0;
            item["hdon_id"] = null;
            item["hdon68_id_lk"] = null;
            item["nky"] = null;
            item["id"] = webix.uid();
            item["giamthuebanhang20"] = item["giamthuebanhang20"] == true ? 1 : 0;
            item["nbke"] = null;
            form.setValues(item);
            this.getFormat();
        },
        getFormat: function (val) {
            renderFormat(val);
        },
        initUINew: function (cctbao_id) {
            $$("cctbao_id").setValue(cctbao_id);
            webix
                .ajax()
                .headers({ "Content-type": "application/json" })
                .get(app.config.host + "/Invoice68/getKyhieu?cctbao_id=" + cctbao_id, function (text) {
                    var data = JSON.parse(text);

                    if (data.length > 0) {
                        var form = $$("invoiceForms");
                        var values = form.getValues();

                        values["khieu"] = data[0]["khhdon"];
                        values["mcqtcap"] = data[0]["mccqthue"];

                        form.setValues(values);
                    }
                });
            this.getFormat();
        },
        initUI: function (windowconfig) {
            var rp = windowconfig;
            var vc_window = rp["data"][0];
            var tabs = vc_window.Tabs;
            $$("head-formInvoice").define("template", "<div>" + vc_window.name + "<div/>");
            el = [];
            col = [];
            var m = 0;

            if (vc_window.detail_layout != null && vc_window.detail_layout != "") {
                var layoutdetail = JSON.parse(vc_window.detail_layout);

                var layout_element = JSON.stringify(layoutdetail.elements);
                var json_layout = JSON.parse(layout_element);
                //console.log(json_layout);
                for (var i = 0; i < json_layout.length; i++) {
                    el.push(json_layout[i]);
                }

                var jsonElement = findJson.getObjects(JSON.parse(JSON.stringify(el)), "id", "");

                for (var i = 0; i < jsonElement.length; i++) {
                    //var item = jsonElement[i];
                    //var item = jsonElement[i];
                    var fields = rp["data"][0]["Tabs"][0]["Fields"];

                    for (var j = 0; j < fields.length; j++) {
                        var field = fields[j];

                        if (jsonElement[i].name == field.column_name) {
                            item = commonHD.CreatItemMaterFromData(field);
                            jsonElement[i] = item;

                            findJson.findAndReplace(el, jsonElement[i].name, item);
                        }
                    }
                }
                if (vc_window.window_type == "MasterDetail") {
                    for (var i = 0; i < tabs.length; i++) {
                        var tab = tabs[i];

                        if (tab.tab_type == "Master") {
                            continue;
                        }

                        var subCols = [];

                        for (var j = 0; j < tab.Fields.length; j++) {
                            var field = tab.Fields[j];

                            subCols.push(commonHD.CreatItemFromData(field, tab));
                        }

                        if (tab.tab_type == "Detail" && tab.foreign_key === "detail_tab") {
                            var item = {
                                id: "detail_tab",
                                header: "Chi tiết hóa đơn",
                                select: "cell",

                                body: {
                                    rows: [
                                        {
                                            view: "toolbar",
                                            height: 40,
                                            cols: [
                                                {
                                                    view: "button",
                                                    type: "htmlbutton",
                                                    width: 65,
                                                    label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F9)" + "</span>",
                                                    id: "addrow",
                                                    css: "btnForm",
                                                    on: {
                                                        onItemClick: function (id, e) {
                                                            var table = webix.$$("hoadon68_chitiet");
                                                            var item = {
                                                                id: webix.uid(),
                                                            };
                                                            table.add(item);
                                                            table.select(item.id, "ma", false);
                                                            table.edit({
                                                                row: item.id,
                                                                column: "ma",
                                                            });
                                                        },
                                                    },
                                                },
                                                {
                                                    view: "button",
                                                    type: "htmlbutton",
                                                    width: 65,
                                                    label: '<i class="fa fa-minus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F8)" + "</span>",
                                                    id: "removerow",
                                                    css: "btnForm",
                                                    on: {
                                                        onItemClick: function (id, e) {
                                                            var table = webix.$$("hoadon68_chitiet");

                                                            table.editStop();

                                                            var item = table.getSelectedItem();

                                                            if (!webix.isUndefined(item)) {
                                                                table.remove(item.id);
                                                                table.clearSelection();
                                                                return;
                                                            }

                                                            var editor = table.getEditState();

                                                            if (!webix.isUndefined(editor)) {
                                                                table.remove(editor.row);
                                                                table.clearSelection();
                                                                return;
                                                            }
                                                        },
                                                    },
                                                },
                                                {
                                                    view: "button",
                                                    type: "htmlbutton",
                                                    width: 65,
                                                    label: '<i class="fa fa-copy fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F7)" + "</span>",
                                                    view: "button",
                                                    id: "copyrow",
                                                    css: "btnForm",
                                                    on: {
                                                        onItemClick: function (id, e) {
                                                            var table = webix.$$("hoadon68_chitiet");

                                                            table.editStop();

                                                            var item = table.getSelectedItem();
                                                            if (item == null || item == undefined) {
                                                                webix.message("Bạn chưa chọn dòng cần copy !", "error");
                                                                return;
                                                            }
                                                            var copy = webix.copy(item);
                                                            copy.cthdon_id = null;
                                                            copy.id = webix.uid();

                                                            table.add(copy);
                                                            table.unselect(item.id);
                                                            table.select(copy.id, "ma", false);
                                                            table.edit({
                                                                row: copy.id,
                                                                column: "ma",
                                                            });
                                                        },
                                                    },
                                                },
                                            ],
                                        },
                                        {
                                            view: "datatable",
                                            resizeColumn: true,
                                            id: "hoadon68_chitiet",
                                            editable: true,
                                            select: "cell",
                                            drag: true,
                                            scroll: true,
                                            columns: subCols,
                                            select_change: tab.select_change,
                                            cell_editstop: tab.cell_editstop,
                                            after_addrow: tab.after_addrow,
                                            after_delrow: tab.after_delrow,
                                            oncheck: tab.oncheck,
                                            on: {
                                                "data->onStoreUpdated": function () {
                                                    this.data.each(function (obj, i) {
                                                        obj.stt = i + 1;
                                                    });
                                                },
                                                onAfterLoad: function () {
                                                    var table = this;
                                                    var data = table.serialize();
                                                    data.forEach(function (entry) {
                                                        if (entry["stckhau"] != undefined || entry["tlckhau"] != undefined) {
                                                            if (entry["stckhau"] != 0 || entry["tlckhau"] != 0) {
                                                                var ck = $$("check_chietkhau");
                                                                if (ck != undefined) {
                                                                    ck.setValue(1);
                                                                    return false;
                                                                }
                                                            }
                                                        }
                                                    });
                                                },
                                                onCheck: function (row, column, state) {
                                                    var table = this;
                                                    if (column == "khuyen_mai") {
                                                        if (state == true) {
                                                        }
                                                    }
                                                },
                                                onAfterAdd: function (id, index) {
                                                    if (this.config.after_addrow != null) {
                                                        var fn = Function("id", "index", "tableId", this.config.after_addrow);
                                                        fn(id, index, this.config.id);
                                                    }
                                                },
                                                onAfterDelete: function (id) {
                                                    if (this.config.after_delrow != null) {
                                                        var fn = Function("id", "tableId", this.config.after_delrow);
                                                        fn(id, this.config.id);
                                                    }
                                                },
                                                onAfterEditStop: function (state, editor, ignore) {
                                                    if (state.value === state.old) return;

                                                    if (this.config.cell_editstop != null) {
                                                        var fn = Function("state", "editor", "ignore", "tableId", this.config.cell_editstop);
                                                        fn(state, editor, ignore, this.config.id);
                                                    }
                                                },
                                            },
                                        },
                                    ],
                                },
                            };

                            findJson.findAndReplace(el, tab.foreign_key, item);
                            m++;
                            //cells.push(item);
                        }
                        if (tab.tab_type == "Detail" && tab.foreign_key === "phi_tab") {
                            var item = {
                                id: "phi_tab",
                                header: "Tiền phí/lệ phí",
                                select: "cell",
                                hidden: tab.hidden,
                                body: {
                                    rows: [
                                        {
                                            view: "toolbar",
                                            height: 40,
                                            cols: [
                                                {
                                                    view: "button",
                                                    type: "htmlbutton",
                                                    width: 65,
                                                    label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F9)" + "</span>",
                                                    id: "addrow2",
                                                    css: "btnForm",
                                                    on: {
                                                        onItemClick: function (id, e) {
                                                            var table = webix.$$("hoadon68_phi");
                                                            var item = {
                                                                id: webix.uid(),
                                                                tienphi: 0,
                                                            };
                                                            table.add(item);
                                                            table.select(item.id);
                                                            table.edit({
                                                                row: item.id,
                                                                column: "tnphi",
                                                            });
                                                        },
                                                    },
                                                },
                                                {
                                                    view: "button",
                                                    type: "htmlbutton",
                                                    width: 65,
                                                    label: '<i class="fa fa-minus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F8)" + "</span>",
                                                    id: "removerow2",
                                                    css: "btnForm",
                                                    on: {
                                                        onItemClick: function (id, e) {
                                                            var table = webix.$$("hoadon68_phi");
                                                            table.editStop();
                                                            var item = table.getSelectedItem();
                                                            if (!webix.isUndefined(item)) {
                                                                table.remove(item.id);
                                                                table.clearSelection();
                                                                return;
                                                            }

                                                            var editor = table.getEditState();

                                                            if (!webix.isUndefined(editor)) {
                                                                table.remove(editor.row);
                                                                table.clearSelection();
                                                                return;
                                                            }
                                                        },
                                                    },
                                                },
                                                {
                                                    view: "button",
                                                    type: "htmlbutton",
                                                    width: 65,
                                                    label: '<i class="fa fa-copy fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + " (F7)" + "</span>",
                                                    view: "button",
                                                    id: "copyrow2",
                                                    css: "btnForm",
                                                    on: {
                                                        onItemClick: function (id, e) {
                                                            var table = webix.$$("hoadon68_phi");

                                                            table.editStop();

                                                            var item = table.getSelectedItem();
                                                            if (item == null || item == undefined) {
                                                                webix.message("Bạn chưa chọn dòng cần copy !", "error");
                                                                return;
                                                            }
                                                            var copy = webix.copy(item);
                                                            copy.id = webix.uid();

                                                            table.add(copy);
                                                            table.unselect(item.id);
                                                            table.select(copy.id);
                                                            table.edit({
                                                                row: copy.id,
                                                                column: "tnphi",
                                                            });
                                                        },
                                                    },
                                                },
                                            ],
                                        },
                                        {
                                            view: "datatable",
                                            resizeColumn: true,
                                            id: "hoadon68_phi",
                                            editable: true,
                                            select: "cell",
                                            drag: true,
                                            scroll: true,
                                            columns: subCols,
                                            select_change: tab.select_change,
                                            cell_editstop: tab.cell_editstop,
                                            after_addrow: tab.after_addrow,
                                            after_delrow: tab.after_delrow,
                                            oncheck: tab.oncheck,
                                            on: {
                                                onAfterDelete: function (id) {
                                                    if (this.config.after_delrow != null) {
                                                        var fn = Function("id", "tableId", this.config.after_delrow);
                                                        fn(id, this.config.id);
                                                    }
                                                },
                                                onAfterEditStop: function (state, editor, ignore) {
                                                    if (state.value === state.old) return;

                                                    if (this.config.cell_editstop != null) {
                                                        var fn = Function("state", "editor", "ignore", "tableId", this.config.cell_editstop);
                                                        fn(state, editor, ignore, this.config.id);
                                                    }
                                                },
                                            },
                                        },
                                    ],
                                },
                            };

                            findJson.findAndReplace(el, "phi_tab", item);
                            m++;
                        }
                    }
                }

                el.push({
                    // paddingY:10,
                    margin: 10,
                    cols: cols,
                });
                webix.ui(el, $$("invoiceForms"));
                //$$("tab_replace").define("tabbar",{width:150 * m});
                //console.log(jsonElement);
            }
        },
    };
});
