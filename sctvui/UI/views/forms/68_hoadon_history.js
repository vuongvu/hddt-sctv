define(["app", "locale", "models/user", "views/forms/68_invoiceForms", "views/forms/68_bangmotaloiCQT"], function (app, _, user, invoiceForms, bangmotaloiCQT) {
    "use strict";

    var editmode = 0;
    var hdon_id = "";

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            css: "headformchil",
            head: {
                height: 35,
                cols: [
                    {
                        id: "head-icondksd",
                        view: "template",
                        css: "headform",
                        width: 35,
                        template: "",
                    },
                    {
                        id: "head-formdksd",
                        view: "template",
                        css: "headform",
                        template: function (obj) {
                            return "<div>Lịch sử xử lý<div/>";
                        },
                    },
                ],
            },
            id: "responseMessageForms",
            resize: true,
            width: 1000,
            height: 500,
            move: true,
            on: {
                onBeforeShow: function () {},
            },
            body: {
                view: "form",
                id: "message",
                // width: 1500,
                position: "center",
                // autowidth:true,
                paddingY: 10,
                paddingX: 15,
                complexData: true,
                scroll: true,
                margin: 0,
                elements: [
                    {
                        // id: "header",
                        template: "<center><b><span style='font-size:17px;'>LỊCH SỬ XỬ LÝ</b></center>",
                        borderless: true,
                        height: 40,
                    },
                    { height: 3 },
                    {
                        rows: [
                            {
                                view: "datatable",
                                id: "danhsach_history",
                                // height: 250,
                                //name: "danhsach_history",
                                // select: true,
                                resizeColumn: true,
                                footer: false,
                                tooltip: true,
                                editable: true,
                                select: "cell",
                                scheme: {
                                    $init: function (obj) {},
                                },
                                url:{
                                    $proxy: true,
                                    load: function (view, callback, params) {
                                        // var hdon_id = "87de11a4-0b4e-42d4-a86c-45ba10b80337";
                                        if(hdon_id != ""){
                                            var url = app.config.host + "/Invoice68/GetHistoryInvoice?id=" + hdon_id;
                                            webix.ajax(url, callback, view);
                                        }
                                    }
                                },
                                onClick: {
                                    ShowResponseMessage: function (event, cell, target){
                                        debugger
                                        //var win = table.$scope.ui(bangmotaloiCQT.$ui);
                                        webix.ui(bangmotaloiCQT.$ui);
                                        var ref_id = cell.row;
                                        bangmotaloiCQT.setEditMode(ref_id);
                                        // // var win = table.$scope.ui(hoadonHistory.$ui);
                                        // // hoadonHistory.setEditMode(hdon_id);
                                        // $$("danhsach_history").show();
                                        webix.ui(bangmotaloiCQT.$ui).show();
                                    }
                                },
                                on: {
                                    "data->onStoreUpdated": function () {
                                        this.data.each(function (obj, i) {
                                            obj.stt = i + 1;
                                        });
                                    },
                                },
                                columns: [
                                    { editor: "text", id: "ref_id", name: "ref_id", hidden: true },
                                    {
                                        id: "stt",
                                        name: "stt",
                                        header: "STT",
                                        sort: "int",
                                        width: 40,
                                    },
                                    {
                                        id: "tgian_gui",
                                        name: "tgian_gui",
                                        header: "Thời gian",
                                        width: 150,
                                        editor: "text",
                                        format: function (text) {
                                            var format = webix.Date.dateToStr("%d/%m/%Y %H:%i:%s.%S");
                                            var time = new Date(text);
                                            text = removeTimeZoneT(time);
                                            return format(text);
                                        },
                                    },
                                    {
                                        id: "nguoi_gui",
                                        name: "nguoi_gui",
                                        header: "Nơi gửi",
                                        width: 150,
                                        editor: "text",
                                    },
                                    {
                                        id: "mltdiep_gui",
                                        name: "mltdiep_gui",
                                        header: "Loại Thông điệp",
                                        width: 200,
                                        editor: "text",
                                        template: function(obj){
                                            if((obj.mltdiep_gui == "203" || obj.mltdiep_gui == "200") && obj.note == "app"){
                                                return "Gửi HDDT lên CQT";
                                            }
                                            else if((obj.mltdiep_gui == "400") && obj.note == "app"){
                                                return "Gửi BTHDL lên CQT";
                                            }
                                            else if((obj.mltdiep_gui == "100" || obj.mltdiep_gui == "101") && obj.note == "app"){
                                                return "Gửi Tờ khai lên CQT";
                                            }
                                            // else if(obj.mltdiep_gui == "200" && obj.note == "app"){
                                            //     return "Gửi HDDT có mã lên CQT";
                                            // }
                                            else if(obj.mltdiep_gui == "300" && obj.note == "app"){
                                                return "Thông báo hóa đơn sai sót";
                                            }
                                            else if(obj.mltdiep_gui == "301" && obj.note == "saisot"){
                                                return "Kết quả tiếp nhận và xử lý hóa đơn sai sót";
                                            }
                                            else if(obj.mltdiep_gui == "202" && obj.note == "capma"){
                                                return "Cơ quan thuế cấp mã hóa đơn";
                                            }
                                            else if(obj.note == "phkt"){
                                                return "Phản hồi kĩ thuật";
                                            }
                                            else if(obj.note == "ktdl"){
                                                return "Kiểm tra dữ liệu";
                                            }
                                            else if(obj.note == "rasoat"){
                                                return "Thông báo HDDT cần rà soát";
                                            }
                                            else{
                                                return "Gửi HDDT lên CQT";
                                            }
                                        }
                                    },
                                    {
                                        id: "noidung",
                                        name: "noidung",
                                        header: "Nội dung",
                                        width: 200,
                                        editor: "text",
                                        template: function (obj) {
                                            if(obj.noidung == "1"){
                                                return "Có lỗi";
                                            } else if (obj.noidung == "" || obj.noidung == null){
                                                return "";
                                            }
                                            else if (obj.noidung == "0"){
                                                return "";
                                            }
                                            else{
                                                return obj.noidung;
                                            }
                                        }
                                    },
                                    {
                                        id: "btnResponseMessage",
                                        name: "View",
                                        header: "Xem chi tiết",
                                        width: 150,
                                        //editor: "text",
                                        template: function (obj) {
                                            if(obj.note == "phkt" || obj.note == "ktdl" || obj.note == "rasoat"){
                                                return "<span class='ShowResponseMessage' style='color: #00b300'><i class='fa fa-eye'></i></span> ";
                                            }
                                            else{
                                                return "";
                                            }
                                            
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        id: "btnHuyHDDT",
                        cols: [
                            {},
                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy bỏ (ESC)",
                                width: 120,
                                type: "danger",
                                align: "center",
                                click: function () {
                                    hdon_id = "";
                                    webix.$$("responseMessageForms").close();
                                },
                            },
                            {},
                        ],
                    },
                ],
            },
        },
        setEditMode: function (id) {
            hdon_id = id;
            var table = $$("danhsach_history");
            table.load(table.config.url);
        },
    };
});
