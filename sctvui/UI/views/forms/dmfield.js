define(["app"], function (app) {
	var editmode = 0;

	return {
		$ui: {
			view: "window", modal: true, id: "dmfield-win", position: "center",
			head: "Thông tin Field",
			fullscreen: true,
			body: {
				view: "scrollview",
				scroll: "native-y",
				body: {
					paddingY: 10,
					paddingX: 10,
					elementsConfig: { labelWidth: 150 },
					view: "form",
					id: "dmfield-form",
					complexData: true,
					margin: 0,
					elements: [
						{
							cols: [
								{ view: "text", name: "ord", label: "STT", id: "dmfield-id" },
								{ view: "text", name: "column_name", label: "Column Name", id: "dmfield-name" }
							]
						},
						{
							cols: [
								{ view: "text", name: "row", label: "Row", id: "dmfield-row" },
								{ view: "text", name: "col", label: "Col", id: "dmfield-col" }
							]
						},
						{
							cols: [
								{ view: "text", name: "width", label: "Width", id: "dmfield-width" },
								{ view: "text", name: "height", label: "Height", id: "dmfield-height" },
							]
						},
						{
							cols:[
								{ view: "text", name: "column_width", label: "Column Width", id: "dmfield-columnwidth" },
								{ view: "text", name: "column_alias", label:"Column alias"}
							]
						},
						{ view: "text", name: "default_value", label: "DEFAULT VALUE" },
						{
							cols: [
								{
									view: "combo",
									name: "column_type",
									label: "Column Type",
									id: "dmfield-columntype",
									suggest: {
										body: {
											url: app.config.host + "/System/GetTypeSQL"
										}
									}
									//options: app.config.host + "/System/GetTypeSQL"
								},
								{
									view: "combo",
									name: "type_editor",
									label: "TYPE EDITOR",
									id: "dmfield-typeeditor",
									suggest: {
										body: {
											data: ["text", "combo", "checkbox", "datepicker", "autonumeric", "treesuggest", "gridsuggest", "gridcombo", "textarea", "multiselect", "select", "ckeditor", "password", "search", "popup"]
										}
									}
									//options: ["text", "combo", "checkbox", "datepicker", "autonumeric", "treesuggest", "gridsuggest", "gridcombo", "textarea", "multiselect", "ckeditor", "password","search"]
								}
							]
						},
						{
							cols: [
								{
									view: "text",
									name: "data_width",
									label: "Data Width"
								},
								{
									view: "text",
									name: "data_decimal",
									label: "Data decimal"
								}
							]
						},
						{
							cols: [
								{ view: "text", name: "label_width", label: "Label Width", id: "dmfield-labelwidth" },
								{ view: "combo", name: "label_position", options: ["left", "top", "right", "bottom"], label: "Label Position", id: "dmfield-labelposition" },
							]

						},
						{
							view: "combo",
							name: "ref_id",
							label: "REF ID",
							id: "dmfield-refid",
							suggest: {
								body: {
									url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00004"
								}
							}
							//options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00004"

						},
						{ view: "text", name: "list_column", label: "LIST COLUMN" },
						{ view: "text", name: "display_field", label: "DISPLAY_FIELD" },
						{ view: "text", name: "format", label: "FORMAT" },
						{ view: "text", name: "field_expression", label: "FIELD_EXPRESSION" },
						{ view: "text", name: "css", label: "Css" },
						{ view: "text", name: "template", label: "Template" },
						{ view: "text", name: "styleinput", label: "Style Input" },
						{ view: "combo", name: "space", options: [{ id: "BEFORE", value: "Trước" }, { id: "AFTER", value: "Sau" }], label: "SPACE" },
						{ view: "text", name: "caption", label: "CAPTION" },
						{ view: "text", name: "read_only", label: "READONLY" },
						{ view: "combo", name: "type_filter", options: ["textFilter", "serverFilter", "numberFilter", "datepickerFilter", "serverDateRangeFilter", "serverSelectFilter"], label: "Type Filter", id: "dmfield-typefilter" },
						{ view: "combo", name: "valid_rule", options: ["isNotEmpty", "isEmail", "isSpecialCharacter", "isTaxCode"], label: "Valid Rule", id: "dmfield-validrule" },
						{ view: "text", name: "total_column", label: "Total Column", id: "dmfield-totalcolumn" },
						{ view: "textarea", height: 160, name: "value_change", label: "VALUE_CHANGE" },
						{ view: "textarea", height: 160, name: "value_suggest", label: "VALUE_SUGGEST" },
						{
							cols: [
								{ view: "checkbox", name: "hide_in_grid", label: "HIDE_IN_GRID", checkValue: "true", uncheckValue: "false", id: "dmfield-hiddeningrid" },
								{ view: "checkbox", name: "hidden", label: "Hidden", checkValue: "true", uncheckValue: "false", id: "dmfield-hidden" }
							]
						},
						{
							margin: 10,
							cols: [
								{},
								{
									shortcut: "F10", view: "button", label: "Nhận (F10)", type: "form", align: "center", width: 120,
									on: {
										onClick: function () {
											var form = webix.$$("dmfield-form");
											var item = form.getValues();

											var url = "/WbField/Update";

											webix.ajax().post(app.config.host + url, item, function (text) {
												var data = JSON.parse(text);

												if (!data.hasOwnProperty("error")) {
													item.id = data.id;
													webix.$$("dmfield-win").close();
													app.callEvent("dataChanged", [editmode, item]);
												}
												else {
													webix.message(data.error);
												}
											});
										}
									},
									click: function () {
										this.callEvent("onClick");
									}
								},
								{
									view: "button", hotkey: "esc", label: "Hủy bỏ (ESC)", type: "danger", align: "center", width: 140,
									click: function () {
										webix.$$("dmfield-win").close();
									}
								}
							]
						}

					]
				}

			}

		},
		setEditMode: function (mode) {
			editmode = mode;
		}
	};

});