define(["app", "locale", "models/user", "views/forms/68_invoiceForms", "views/68_mau0405"], function (app, _, user, invoiceForms, mau0405) {
    "use strict";

    var editmode = 0;

    return {
        $ui: {
            view: "window",
            modal: true,
            position: "center",
            css: "headformchil",
            head: {
                //view: "toolbar",
                // css: { "box-shadow": "0 -2px #3498db inset !important", "background": "#ffffff !important" },
                //css: { "background": "#e6f2ff !important" },
                height: 35,
                cols: [
                    {
                        id: "head-icondksd",
                        view: "template",
                        css: "headform",
                        width: 35,
                        template: "",
                    },
                    {
                        id: "head-formdksd",
                        view: "template",
                        css: "headform",
                        template: function (obj) {
                            return "<div>Thông báo sai sót hóa đơn điện tử<div/>";
                        },
                    },
                    {
                        view: "icon",
                        icon: "fas fa-question-circle",
                        id: "help_dksdForms",
                        tooltip: _("HELPER"),
                        click: function () {
                            // var form = webix.$$("window-form");
                            // hint_form(form.config.hint);
                        },
                    },
                ],
            },
            id: "huyhoadonForms",
            resize: true,
            width: 1100,
            height: 600,
            // fullscreen: true,
            move: true,
            body: {
                view: "form",
                id: "huyhoadon",
                // width: 1500,
                position: "center",
                // autowidth:true,
                paddingY: 10,
                paddingX: 15,
                complexData: true,
                scroll: true,
                margin: 0,
                elements: [
                    {
                        // id: "header",
                        template: "<center><b><span style='font-size:17px;'>THÔNG BÁO SAI SÓT HÓA ĐƠN ĐIỆN TỬ</b></center>",
                        borderless: true,
                        height: 40,
                    },
                    {
                        paddingX: 30,
                        rows: [
                            {
                                cols: [
                                    {},
                                    {
                                        label: "Kính gửi: ",
                                        id: "tcqt",
                                        name: "tcqt",
                                        view: "combo",
                                        labelWidth: 100,
                                        width: 300,
                                        options: {
                                            view: "gridsuggest",
                                            columnName: "name",
                                            url: {
                                                $proxy: true,
                                                load: function (view, callback, params) {
                                                    var url = app.config.host + "/System/GetDataReferencesByRefId?refId=RF00251";
                                                    webix.ajax(url, function (text) {
                                                        var json = JSON.parse(text);
                                                        var data = listToTree(json);
                                                        view.parse(data, "json");
                                                    });
                                                },
                                            },
                                            body: {
                                                columns: [{ id: "name", width: 500, header: "name" }],
                                                scroll: true,
                                                autoheight: false,
                                                datatype: "json",
                                            },
                                            on: {
                                                onValueSuggest: function (obj) {
                                                    $$("mcqt").setValue(obj["code"]);
                                                    $$("mcqt").refresh();
                                                },
                                            },
                                        },
                                        required: true,
                                        invalidMessage: "Kính gửi không được để trống !",
                                    },
                                    {},
                                ],
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        label: "Mã số thuế",
                                        id: "mst",
                                        name: "mst",
                                        required: true,
                                        invalidMessage: "Tên người nộp thuế không được để trống !",
                                    },
                                    {
                                        view: "text",
                                        label: "Mã đơn vị quan hệ ngân sách",
                                        id: "mdvqhnsach",
                                        name: "mdvqhnsach",
                                        labelWidth: 200,
                                        //required: true,
                                        //invalidMessage: "Tên người nộp thuế không được để trống !",
                                    },
                                ],
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        label: "Tên người nộp thuế",
                                        labelWidth: 150,
                                        required: true,
                                        invalidMessage: "Tên người nộp thuế không được để trống !",
                                        id: "tnnt",
                                        name: "tnnt",
                                    },
                                    {
                                        id: "mcqt",
                                        name: "mcqt",
                                        label: "Mã cơ quan thuế",
                                        labelWidth: 150,
                                        required: true,
                                        invalidMessage: "Mã cơ quan thuế quản lý không được để trống !",
                                        view: "text",
                                        attributes: { maxlength: 5 },
                                    },
                                ],
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        label: "Địa danh",
                                        labelWidth: 150,
                                        id: "ddanh",
                                        name: "ddanh",
                                        required: true,
                                        invalidMessage: "Địa danh không được để trống !",
                                    },
                                ],
                            },
                        ],
                    },
                    { height: 3 },
                    {
                        template: "<b><span >Người nộp thuế thông báo về việc hủy/điều chỉnh/thay thế/giải trình hóa đơn điện tử do sai sót như sau:</b>",
                        borderless: true,
                        height: 40,
                    },
                    { height: 3 },
                    {
                        rows: [
                            {
                                id: "btnHuyhoadon",
                                cols: [
                                    {
                                        view: "button",
                                        type: "icon",
                                        //view: "icon",
                                        label: " (F9)",
                                        // width: 60,
                                        autowidth: true,
                                        icon: "fas fa-plus",
                                        on: {
                                            onItemClick: function (id, e) {
                                                var table = webix.$$("danhsach_hoadonhuy");
                                                var item = { id: webix.uid(), ngay: new Date(), ladhddt: 3, tctbao: "1" };
                                                table.add(item);
                                                table.select(item.id, "khieu", false);
                                                // table.edit({ row: item.id, column: "khieu" });
                                            },
                                        },
                                    },
                                    {
                                        view: "button",
                                        type: "icon",
                                        //view: "icon",
                                        autowidth: true,
                                        label: " (F8)",
                                        // width: 60,
                                        icon: "fas fa-minus",
                                        on: {
                                            onItemClick: function (id, e) {
                                                var table = webix.$$("danhsach_hoadonhuy");
                                                table.editStop();
                                                var item = table.getSelectedItem();
                                                if (!webix.isUndefined(item)) {
                                                    table.remove(item.id);
                                                    table.clearSelection();
                                                    return;
                                                }
                                                var editor = table.getEditState();
                                                if (!webix.isUndefined(editor)) {
                                                    table.remove(editor.row);
                                                    table.clearSelection();
                                                    return;
                                                }
                                            },
                                        },
                                    },
                                    {},
                                ],
                            },
                            {
                                view: "datatable",
                                id: "danhsach_hoadonhuy",
                                // height: 250,
                                name: "danhsach_hoadonhuy",
                                // select: true,
                                resizeColumn: true,
                                footer: false,
                                tooltip: true,
                                editable: true,
                                select: "cell",
                                scheme: {
                                    $init: function (obj) { },
                                },
                                on: {
                                    "data->onStoreUpdated": function () {
                                        this.data.each(function (obj, i) {
                                            obj.stt = i + 1;
                                        });
                                    },
                                    onBeforeEditStart: function (id) {
                                        // if (id.column == "stt") {
                                        //     // block editors in 'year' column
                                        //     return false;
                                        // } else if (id.column == "khieu") {
                                        //     return false;
                                        // } else if (id.column == "shdon") {
                                        //     return false;
                                        // }
                                        // else if (id.column == "ladhddt") {
                                        //     return false;
                                        // }
                                        // if (id.column == "ngay") {
                                        //     return false;
                                        // } 
                                        // else if (id.column == "huygiaitrinh") {
                                        //     return false;
                                        // }
                                    },
                                },
                                columns: [
                                    { editor: "text", id: "hdon_id", name: "hdon_id", hidden: true },
                                    {
                                        id: "stt",
                                        name: "stt",
                                        header: "STT",
                                        sort: "int",
                                        width: 40,
                                    },
                                    {
                                        view: "text",
                                        id: "mcqtcap",
                                        name: "mcatcap",
                                        header: "Mã cơ quan thuế cấp",
                                        width: 150,
                                        // editor: "text",
                                        hidden: true,
                                    },
                                    {
                                        view: "text",
                                        id: "mau_so",
                                        name: "mau_so",
                                        header: _("Mẫu số hóa đơn điện tử"),
                                        editor: "text",
                                        width: 250,
                                        require: true,
                                    },
                                    {
                                        view: "text",
                                        id: "khieu",
                                        header: _("Ký hiệu hóa đơn điện tử"),
                                        editor: "text",
                                        width: 250,
                                        require: true,
                                    },
                                    {

                                        view: "text",
                                        id: "shdon",
                                        name: "shdon",
                                        header: "Số hóa đơn điện tử",
                                        width: 200,
                                        editor: "text",
                                        require: true,
                                    },
                                    {
                                        // view: "datepicker",
                                        id: "ngay",
                                        name: "ngay",
                                        header: "Ngày lập hóa đơn",
                                        width: 200,
                                        editor: "date",
                                        require: true,
                                        format: webix.Date.dateToStr("%d/%m/%Y"),
                                        // stringResult: true,
                                    },
                                    {
                                        id: "ladhddt",
                                        name: "ladhddt",
                                        header: "Loại áp dụng hóa đơn điện tử",
                                        editable: false,
                                        width: 200,
                                        // editor: "combo",
                                        require: true,
                                        options: [
                                            // { id: "1", value: "HĐĐT Nghị đinh số 119/2018" },
                                            { id: "1", value: "HĐĐT Nghị đinh số 123/2020/NĐ-CP" },
                                            { id: "2", value: "HĐĐT có mã của cơ quan thuế" },
                                            { id: "3", value: "Các loại hóa đơn theo Nghị đinh 51/2010" },
                                            { id: "4", value: "Hóa đơn đặt in theo Nghị định số 123/2020/NĐ-CP" },
                                        ],
                                    },
                                    {
                                        id: "tctbao",
                                        name: "tctbao",
                                        header: "Hủy/Điều chỉnh/Thay thế/Giải trình",
                                        editable: true,
                                        width: 200,
                                        editor: "combo",
                                        require: true,
                                        options: [
                                            { id: "1", value: "Hủy" },
                                            //{ id: "2", value: "Điều chỉnh" },
                                            // { id: "2", value: "Hủy có lập hóa đơn thay thế" },
                                            // { id: "3", value: "Hủy không lập hóa đơn thay thế" },
                                            { id: "3", value: "Thay thế" },
                                            { id: "4", value: "Giải trình" },
                                            { id: "5", value: "Điều chỉnh tăng" },
                                            { id: "6", value: "Điều chỉnh giảm" },
                                            { id: "7", value: "Điều chỉnh khác" },
                                            // { id: "8", value: "Điều chỉnh thông tin không tạo mới hóa đơn" },
                                        ],
                                    },
                                    {
                                        id: "ldo",
                                        name: "ldo",
                                        header: "Lý do",
                                        editable: true,
                                        editor: "text",
                                        width: 300,
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        id: "btnHuyHDDT",
                        cols: [
                            {},
                            {
                                view: "button",
                                id: "btnStep299",
                                label: "Lưu, chuyển sang bước tiếp theo",
                                type: "form",
                                hidden: true,
                                align: "center",
                                click: function () {
                                    // var btnEdit = $$("btnPlus");
                                    // btnEdit.callEvent("onClick");
                                    var self = this;
                                    setTimeout(function () {
                                        var item = $$("huyhoadon").getValues();
                                        var details = [];
                                        details.push({ data: $$("danhsach_hoadonhuy").serialize() });
                                        item["details"] = details;

                                        var item1 = [];
                                        item1.push(item);
                                        var data_post = { editmode: editmode, data: item1 };

                                        var detailForm = self.$scope.ui(invoiceForms.$ui);

                                        var cctbao_id = webix.$$("kyhieu_id").getValue();
                                        invoiceForms.initUINew(cctbao_id);
                                        invoiceForms.setEditMode(1);
                                        invoiceForms.setValues(null, data_post, null);
                                        detailForm.show();
                                    }, 100);

                                    // $$("huyhoadonForms").close();
                                },
                            },
                            {
                                shortcut: "F10",
                                view: "button",
                                id: "btnGhiHuyhoadon",
                                label: "Ghi (F10)",
                                width: 150,
                                align: "center",
                                type: "form",
                                on: {
                                    onClick: function () {
                                        var self = this;
                                        setTimeout(function () {
                                            var item = $$("huyhoadon").getValues();
                                            var table = $$("huyhoadon");

                                            if (table.validate()) {
                                                var cks = $$("danhsach_hoadonhuy").serialize();
                                                if (cks.length == 0) {
                                                    webix.message("Bạn chưa nhập thông tin hóa đơn cần hủy !", "error");
                                                    return;
                                                }
                                                webix.extend(table, webix.ProgressBar);
                                                table.disable();
                                                table.showProgress();

                                                var details = [];
                                                details.push({ data: cks });
                                                item.details = details;

                                                var item1 = [];
                                                item1.push(item);
                                                var data_post = { editmode: editmode, data: item1 };

                                                webix
                                                    .ajax()
                                                    .headers({ "Content-type": "application/json" })
                                                    .post(app.config.host + "/Invoice68/huyhoadonSave", data_post, {
                                                        error: function (text, data, XmlHttpRequest) {
                                                            table.enable();
                                                            table.hideProgress();
                                                            // var json = JSON.parse(text);
                                                            // webix.message(json.ExceptionMessage, "error");
                                                            webix.message(text + XmlHttpRequest, "error");
                                                        },
                                                        success: function (text, data, XmlHttpRequest) {
                                                            var json = JSON.parse(text);

                                                            table.enable();
                                                            table.hideProgress();
                                                            if (json.hasOwnProperty("error")) {
                                                                webix.message(json.error, "error");
                                                            } else {
                                                                app.callEvent("dataChangedMau0405", [null, null]);
                                                                webix.message("Thành công !", "success");
                                                                webix.$$("huyhoadonForms").close();
                                                                var table1 = webix.$$("mau04_68");
                                                                table1.clearAll();
                                                                table1.load(table1.config.url);
                                                                table1.show();       
                                                            }
                                                        },
                                                    });
                                            }
                                        }, 100);
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");                                                                                            
                                    }                                
                            },
                            {
                                shortcut: "F4",
                                view: "button",
                                id: "btnGhiSignHuyHDDT",
                                label: "Ký và gửi CQT (F14)",
                                width: 150,
                                align: "center",
                                type: "form",
                                on: {
                                    onClick: function () {
                                        var self = this;
                                        setTimeout(function () {
                                            var item = $$("huyhoadon").getValues();
                                            var table = $$("huyhoadon");

                                            if (table.validate()) {
                                                var cks = $$("danhsach_hoadonhuy").serialize();
                                                if (cks.length == 0) {
                                                    webix.message("Bạn chưa nhập thông tin hóa đơn cần hủy !", "error");
                                                    return;
                                                }
                                                var details = [];
                                                details.push({ data: $$("danhsach_hoadonhuy").serialize() });
                                                item["details"] = details;

                                                var item1 = [];
                                                item1.push(item);
                                                var data_post = { editmode: editmode, data: item1 };

                                                var tableDK = $$("huyhoadon");

                                                webix
                                                    .ajax()
                                                    .headers({ "Content-Type": "application/json" })
                                                    .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                                                        var result = JSON.parse(text);
                                                        var kytaptrung = "";
                                                        for (var i = 0; i < result.length; i++) {
                                                            var item = result[i];
                                                            kytaptrung = item.value;
                                                        }

                                                        webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                                            var listcerts = JSON.parse(text);
                                                            if (listcerts.length > 0) {
                                                                var box = webix.modalbox({
                                                                    title: "Chọn chứng thư số",
                                                                    buttons: ["Nhận", "Hủy bỏ"],
                                                                    text: "<div id='ctnCerts'></div>",
                                                                    top: 100,
                                                                    width: 750,
                                                                    callback: function (result) {
                                                                        switch (result) {
                                                                            case "1":
                                                                                webix.modalbox.hide();
                                                                                break;
                                                                            case "0":
                                                                                var table = $$("tblCerts");
                                                                                var data = table.serialize();

                                                                                var cert = null;

                                                                                for (var i = 0; i < data.length; i++) {
                                                                                    if (data[i].chon == true) {
                                                                                        cert = data[i];
                                                                                        break;
                                                                                    }
                                                                                }

                                                                                if (cert == null) {
                                                                                    webix.message("Bạn chưa chọn chứng thư số");
                                                                                    return;
                                                                                }
                                                                                var postData = {
                                                                                    branch_code: user.getDvcsID(),
                                                                                    username: user.getCurrentUser(),
                                                                                    cer_serial: cert.cer_serial,
                                                                                };
                                                                                data_post.sign = postData;

                                                                                webix.extend(tableDK, webix.ProgressBar);
                                                                                tableDK.disable();
                                                                                tableDK.showProgress();
                                                                                webix
                                                                                    .ajax()
                                                                                    .headers({ "Content-type": "application/json" })
                                                                                    .post(app.config.host + "/Invoice68/huyhoadonSaveSign", data_post, {
                                                                                        error: function (text, data, XmlHttpRequest) {
                                                                                            tableDK.enable();
                                                                                            tableDK.hideProgress();
                                                                                            // var json = JSON.parse(text);
                                                                                            // webix.message(json.ExceptionMessage, "error");
                                                                                            webix.message(text + XmlHttpRequest, "error");
                                                                                        },
                                                                                        success: function (text, data, XmlHttpRequest) {
                                                                                            var json = JSON.parse(text);
                                                                                            tableDK.enable();
                                                                                            tableDK.hideProgress();
                                                                                            if (!json.hasOwnProperty("error")) {
                                                                                                webix.message("Gửi thành công đến CQT !", "success");
                                                                                                $$("huyhoadonForms").close();
                                                                                                app.callEvent("dataChangedInvoie68", [1, null]);
                                                                                            } else {
                                                                                                webix.message(json.error, "error");
                                                                                            }
                                                                                        },
                                                                                    });

                                                                            // return;
                                                                        }
                                                                    },
                                                                });
                                                                var tblMatHang = webix.ui({
                                                                    container: "ctnCerts",
                                                                    id: "tblCerts",
                                                                    view: "datatable",
                                                                    borderless: true,
                                                                    resizeColumn: true,
                                                                    height: 350,
                                                                    columns: [
                                                                        {
                                                                            id: "chon",
                                                                            header: _("CHON"),
                                                                            width: 65,
                                                                            checkValue: true,
                                                                            uncheckValue: false,
                                                                            template: "{common.checkbox()}",
                                                                        },
                                                                        {
                                                                            id: "cert_type",
                                                                            header: _("Loại"),
                                                                            width: 450,
                                                                        },
                                                                        {
                                                                            id: "cer_serial",
                                                                            header: _("SO_SERIAL"),
                                                                            width: 150,
                                                                        },
                                                                        {
                                                                            id: "subject_name",
                                                                            header: _("SUBJECTNAME"),
                                                                            width: 450,
                                                                        },
                                                                        {
                                                                            id: "begin_date",
                                                                            header: _("NGAY_BAT_DAU"),
                                                                            width: 200,
                                                                        },
                                                                        {
                                                                            id: "end_date",
                                                                            header: _("NGAY_KET_THUC"),
                                                                            width: 200,
                                                                        },
                                                                        {
                                                                            id: "issuer",
                                                                            header: _("DONVI"),
                                                                            width: 450,
                                                                        },
                                                                    ],
                                                                    url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                                                });
                                                            } else {
                                                                var table = $$("huyhoadon");
                                                                webix.extend(table, webix.ProgressBar);
                                                                table.disable();
                                                                table.showProgress();

                                                                // xem user có tích checkbox ký HSM không?
                                                                var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                                                                webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                                                    if (resultscheckSigHSM[0].json() == "C") {
                                                                        var postData = {
                                                                            branch_code: user.getDvcsID(),
                                                                            username: user.getCurrentUser(),
                                                                            check_HSM: "1",
                                                                        };
                                                                        data_post.sign = postData;

                                                                        webix.extend(tableDK, webix.ProgressBar);
                                                                        tableDK.disable();
                                                                        tableDK.showProgress();
                                                                        webix
                                                                            .ajax()
                                                                            .headers({ "Content-type": "application/json" })
                                                                            .post(app.config.host + "/Invoice68/huyhoadonSaveSign", data_post, {
                                                                                error: function (text, data, XmlHttpRequest) {
                                                                                    tableDK.enable();
                                                                                    tableDK.hideProgress();
                                                                                    // var json = JSON.parse(text);
                                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                                },
                                                                                success: function (text, data, XmlHttpRequest) {
                                                                                    var json = JSON.parse(text);
                                                                                    tableDK.enable();
                                                                                    tableDK.hideProgress();
                                                                                    if (!json.hasOwnProperty("error")) {
                                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                                        $$("huyhoadonForms").close();
                                                                                        app.callEvent("dataChangedInvoie68", [1, null]);
                                                                                    } else {
                                                                                        webix.message(json.error, "error");
                                                                                    }
                                                                                },
                                                                            });
                                                                    } else {
                                                                        webix
                                                                            .ajax()
                                                                            .headers({ "Content-type": "application/json" })
                                                                            .post(app.config.host + "/Invoice68/huyhoadonSave", data_post, {
                                                                                error: function (text, data, XmlHttpRequest) {
                                                                                    table.enable();
                                                                                    table.hideProgress();
                                                                                    // var json = JSON.parse(text);
                                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                                },
                                                                                success: function (text, data, XmlHttpRequest) {
                                                                                    var json = JSON.parse(text);

                                                                                    if (json.hasOwnProperty("error")) {
                                                                                        webix.message(json.error, "error");
                                                                                    } else {
                                                                                        var a = webix.ajax(app.config.host + "/Invoice68/XmlMau0405?id=" + json.id);
                                                                                        var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                                                                        webix.promise.all([a, b, json]).then(function (results) {
                                                                                            // webix.extend(tblInvoice, webix.ProgressBar);
                                                                                            // tblInvoice.disable();
                                                                                            // tblInvoice.showProgress();
                                                                                            var json = results[0].json();

                                                                                            if (json.hasOwnProperty("error")) {
                                                                                                webix.message({ type: "error", text: json.error });
                                                                                            }

                                                                                            var listcerts = results[1].json();
                                                                                            var item = results[2];

                                                                                            if (listcerts.length == 0) {
                                                                                                table.enable();
                                                                                                table.hideProgress();
                                                                                                webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                                                                return;
                                                                                            }

                                                                                            require(["views/modules/minvoiceplugin"], function (plugin) {
                                                                                                var hub = plugin.hub();
                                                                                                var hubconnect = plugin.conn();
                                                                                                if (hubconnect.state != 1) {
                                                                                                    table.enable();
                                                                                                    table.hideProgress();
                                                                                                    webix.message(
                                                                                                        "Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !",
                                                                                                        "error"
                                                                                                    );
                                                                                                    return;
                                                                                                }
                                                                                                hub.off("resCommand");
                                                                                                hub.on("resCommand", function (result) {
                                                                                                    var json = JSON.parse(result);

                                                                                                    if (json.hasOwnProperty("error")) {
                                                                                                        webix.message({ type: "error", text: json.error });
                                                                                                    } else {
                                                                                                        var parameters = {
                                                                                                            xml: json.xml,
                                                                                                            id: item.id,
                                                                                                            tthdon: 3,
                                                                                                        };

                                                                                                        webix
                                                                                                            .ajax()
                                                                                                            .headers({ "Content-Type": "application/json" })
                                                                                                            .post(app.config.host + "/Invoice68/SaveXmlMau0405", parameters, function (text) {
                                                                                                                var result = JSON.parse(text);
                                                                                                                if (result.hasOwnProperty("error")) {
                                                                                                                    webix.message(result.error, "error");
                                                                                                                } else {
                                                                                                                    app.callEvent("dataChangedInvoie68", [editmode, null]);

                                                                                                                    webix.message("Thành công !", "success");
                                                                                                                    webix.$$("huyhoadonForms").close();
                                                                                                                }
                                                                                                            });
                                                                                                    }
                                                                                                    table.enable();
                                                                                                    table.hideProgress();
                                                                                                });

                                                                                                var _listcerts = [];

                                                                                                for (var i = 0; i < listcerts.length; i++) {
                                                                                                    var _cert = listcerts[i];

                                                                                                    _listcerts.push({
                                                                                                        so_serial: _cert.cer_serial,
                                                                                                        ngaybatdau: _cert.begin_date,
                                                                                                        ngayketthuc: _cert.end_date,
                                                                                                        issuer: _cert.issuer,
                                                                                                        subjectname: _cert.subject_name,
                                                                                                    });
                                                                                                }

                                                                                                var arg = {
                                                                                                    idData: "#data",
                                                                                                    xml: json["xml"],
                                                                                                    shdon: "",
                                                                                                    idSigner: "seller",
                                                                                                    tagSign: "NNT",
                                                                                                    dvky: json["dvky"],
                                                                                                    listcerts: _listcerts,
                                                                                                };

                                                                                                hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                                                                    .done(function () {
                                                                                                        console.log("Success !");
                                                                                                    })
                                                                                                    .fail(function (error) {
                                                                                                        webix.message(error, "error");
                                                                                                    });
                                                                                            });
                                                                                        });
                                                                                        // app.callEvent("dataChangedInvoie68", [editmode, null]);

                                                                                        // webix.message("Thành công !", "success");
                                                                                        // webix.$$("huyhoadonForms").close();
                                                                                    }
                                                                                },
                                                                            });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                            }
                                        }, 100);
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },
                            {
                                shortcut: "F4",
                                view: "button",
                                id: "btnSaveSignGiaiTrinh",
                                label: "Ký và gửi CQT",
                                width: 150,
                                hidden: true,
                                align: "center",
                                type: "form",
                                on: {
                                    onClick: function () {
                                        var self = this;
                                        setTimeout(function () {
                                            var item = $$("huyhoadon").getValues();
                                            var table = $$("huyhoadon");

                                            if (table.validate()) {
                                                var cks = $$("danhsach_hoadonhuy").serialize();
                                                if (cks.length == 0) {
                                                    webix.message("Bạn chưa nhập thông tin hóa đơn cần hủy !", "error");
                                                    return;
                                                }
                                                var details = [];
                                                details.push({ data: $$("danhsach_hoadonhuy").serialize() });
                                                item["details"] = details;

                                                var item1 = [];
                                                item1.push(item);
                                                var data_post = { editmode: editmode, data: item1 };

                                                var tableDK = $$("huyhoadon");
                                                webix
                                                    .ajax()
                                                    .headers({ "Content-Type": "application/json" })
                                                    .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                                                        var result = JSON.parse(text);
                                                        var kytaptrung = "";
                                                        for (var i = 0; i < result.length; i++) {
                                                            var item = result[i];
                                                            kytaptrung = item.value;
                                                        }

                                                        webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                                            var listcerts = JSON.parse(text);
                                                            if (listcerts.length > 0) {
                                                                var box = webix.modalbox({
                                                                    title: "Chọn chứng thư số",
                                                                    buttons: ["Nhận", "Hủy bỏ"],
                                                                    text: "<div id='ctnCerts'></div>",
                                                                    top: 100,
                                                                    width: 750,
                                                                    callback: function (result) {
                                                                        switch (result) {
                                                                            case "1":
                                                                                webix.modalbox.hide();
                                                                                break;
                                                                            case "0":
                                                                                var table = $$("tblCerts");
                                                                                var data = table.serialize();

                                                                                var cert = null;

                                                                                for (var i = 0; i < data.length; i++) {
                                                                                    if (data[i].chon == true) {
                                                                                        cert = data[i];
                                                                                        break;
                                                                                    }
                                                                                }

                                                                                if (cert == null) {
                                                                                    webix.message("Bạn chưa chọn chứng thư số");
                                                                                    return;
                                                                                }
                                                                                var postData = {
                                                                                    branch_code: user.getDvcsID(),
                                                                                    username: user.getCurrentUser(),
                                                                                    cer_serial: cert.cer_serial,
                                                                                };
                                                                                data_post.sign = postData;

                                                                                webix.extend(tableDK, webix.ProgressBar);
                                                                                tableDK.disable();
                                                                                tableDK.showProgress();
                                                                                webix
                                                                                    .ajax()
                                                                                    .headers({ "Content-type": "application/json" })
                                                                                    .post(app.config.host + "/Invoice68/giaitrinhSaveSign", data_post, {
                                                                                        error: function (text, data, XmlHttpRequest) {
                                                                                            tableDK.enable();
                                                                                            tableDK.hideProgress();
                                                                                            // var json = JSON.parse(text);
                                                                                            // webix.message(json.ExceptionMessage, "error");
                                                                                            webix.message(text + XmlHttpRequest, "error");
                                                                                        },
                                                                                        success: function (text, data, XmlHttpRequest) {
                                                                                            var json = JSON.parse(text);
                                                                                            tableDK.enable();
                                                                                            tableDK.hideProgress();
                                                                                            if (!json.hasOwnProperty("error")) {
                                                                                                webix.message("Gửi thành công đến CQT !", "success");
                                                                                                $$("huyhoadonForms").close();
                                                                                                app.callEvent("dataChangedInvoie68", [1, null]);
                                                                                            } else {
                                                                                                webix.message(json.error, "error");
                                                                                            }
                                                                                        },
                                                                                    });

                                                                            // return;
                                                                        }
                                                                    },
                                                                });

                                                                var tblMatHang = webix.ui({
                                                                    container: "ctnCerts",
                                                                    id: "tblCerts",
                                                                    view: "datatable",
                                                                    borderless: true,
                                                                    resizeColumn: true,
                                                                    height: 350,
                                                                    columns: [
                                                                        {
                                                                            id: "chon",
                                                                            header: _("CHON"),
                                                                            width: 65,
                                                                            checkValue: true,
                                                                            uncheckValue: false,
                                                                            template: "{common.checkbox()}",
                                                                        },
                                                                        {
                                                                            id: "cert_type",
                                                                            header: _("Loại"),
                                                                            width: 100,
                                                                        },
                                                                        {
                                                                            id: "cer_serial",
                                                                            header: _("SO_SERIAL"),
                                                                            width: 150,
                                                                        },
                                                                        {
                                                                            id: "subject_name",
                                                                            header: _("SUBJECTNAME"),
                                                                            width: 450,
                                                                        },
                                                                        {
                                                                            id: "begin_date",
                                                                            header: _("NGAY_BAT_DAU"),
                                                                            width: 200,
                                                                        },
                                                                        {
                                                                            id: "end_date",
                                                                            header: _("NGAY_KET_THUC"),
                                                                            width: 200,
                                                                        },
                                                                        {
                                                                            id: "issuer",
                                                                            header: _("DONVI"),
                                                                            width: 450,
                                                                        },
                                                                    ],
                                                                    url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                                                });
                                                            } else {
                                                                var table = $$("huyhoadon");
                                                                webix.extend(table, webix.ProgressBar);
                                                                table.disable();
                                                                table.showProgress();

                                                                webix
                                                                    .ajax()
                                                                    .headers({ "Content-type": "application/json" })
                                                                    .post(app.config.host + "/Invoice68/huyhoadonSave", data_post, {
                                                                        error: function (text, data, XmlHttpRequest) {
                                                                            table.enable();
                                                                            table.hideProgress();
                                                                            // var json = JSON.parse(text);
                                                                            // webix.message(json.ExceptionMessage, "error");
                                                                            webix.message(text + XmlHttpRequest, "error");
                                                                        },
                                                                        success: function (text, data, XmlHttpRequest) {
                                                                            var json = JSON.parse(text);

                                                                            if (json.hasOwnProperty("error")) {
                                                                                webix.message(json.error, "error");
                                                                            } else {
                                                                                var a = webix.ajax(app.config.host + "/Invoice68/XmlMau0405?id=" + json.id);
                                                                                var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                                                                webix.promise.all([a, b, json]).then(function (results) {
                                                                                    // webix.extend(tblInvoice, webix.ProgressBar);
                                                                                    // tblInvoice.disable();
                                                                                    // tblInvoice.showProgress();
                                                                                    var json = results[0].json();

                                                                                    if (json.hasOwnProperty("error")) {
                                                                                        webix.message({ type: "error", text: json.error });
                                                                                    }

                                                                                    var listcerts = results[1].json();
                                                                                    var item = results[2];

                                                                                    if (listcerts.length == 0) {
                                                                                        table.enable();
                                                                                        table.hideProgress();
                                                                                        webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                                                        return;
                                                                                    }

                                                                                    require(["views/modules/minvoiceplugin"], function (plugin) {
                                                                                        var hub = plugin.hub();
                                                                                        var hubconnect = plugin.conn();
                                                                                        if (hubconnect.state != 1) {
                                                                                            table.enable();
                                                                                            table.hideProgress();
                                                                                            webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                                                            return;
                                                                                        }
                                                                                        hub.off("resCommand");
                                                                                        hub.on("resCommand", function (result) {
                                                                                            var json = JSON.parse(result);

                                                                                            if (json.hasOwnProperty("error")) {
                                                                                                webix.message({ type: "error", text: json.error });
                                                                                            } else {
                                                                                                var parameters = {
                                                                                                    xml: json.xml,
                                                                                                    id: item.id,
                                                                                                    tthdon: 1,
                                                                                                };

                                                                                                webix
                                                                                                    .ajax()
                                                                                                    .headers({ "Content-Type": "application/json" })
                                                                                                    .post(app.config.host + "/Invoice68/SaveXmlMau0405", parameters, function (text) {
                                                                                                        var result = JSON.parse(text);
                                                                                                        if (result.hasOwnProperty("error")) {
                                                                                                            webix.message(result.error, "error");
                                                                                                        } else {
                                                                                                            app.callEvent("dataChangedInvoie68", [editmode, null]);

                                                                                                            webix.message("Thành công !", "success");
                                                                                                            webix.$$("huyhoadonForms").close();
                                                                                                        }
                                                                                                    });
                                                                                            }
                                                                                            table.enable();
                                                                                            table.hideProgress();
                                                                                        });

                                                                                        var _listcerts = [];

                                                                                        for (var i = 0; i < listcerts.length; i++) {
                                                                                            var _cert = listcerts[i];

                                                                                            _listcerts.push({
                                                                                                so_serial: _cert.cer_serial,
                                                                                                ngaybatdau: _cert.begin_date,
                                                                                                ngayketthuc: _cert.end_date,
                                                                                                issuer: _cert.issuer,
                                                                                                subjectname: _cert.subject_name,
                                                                                            });
                                                                                        }

                                                                                        var arg = {
                                                                                            idData: "#data",
                                                                                            xml: json["xml"],
                                                                                            shdon: "",
                                                                                            idSigner: "seller",
                                                                                            tagSign: "NNT",
                                                                                            dvky: json["dvky"],
                                                                                            listcerts: _listcerts,
                                                                                        };

                                                                                        hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                                                            .done(function () {
                                                                                                console.log("Success !");
                                                                                            })
                                                                                            .fail(function (error) {
                                                                                                webix.message(error, "error");
                                                                                            });
                                                                                    });
                                                                                });
                                                                                // app.callEvent("dataChangedInvoie68", [editmode, null]);

                                                                                // webix.message("Thành công !", "success");
                                                                                // webix.$$("huyhoadonForms").close();
                                                                            }
                                                                        },
                                                                    });
                                                            }
                                                        });
                                                    });
                                            }
                                        }, 100);
                                    },
                                },
                                click: function () {
                                    this.callEvent("onClick");
                                },
                            },
                            {
                                view: "button",
                                hotkey: "esc",
                                label: "Hủy bỏ (ESC)",
                                width: 120,
                                type: "danger",
                                align: "center",
                                click: function () {
                                    app.callEvent("dataChangedInvoie68", [1, null]);
                                    webix.$$("huyhoadonForms").close();
                                },
                            },
                            {},
                        ],
                    },
                ],
            },
        },
        setEditMode: function (mode) {
            editmode = mode;
            var form = $$(this.$ui.id);
            //var topParentView = form.getTopParentView();
            form.config.editmode = mode;
            var icon = "";
            if (editmode == 1) {
                icon = "<span><i class='fa fa-plus' aria-hidden='true'></i></span>";
                var result = webix
                    .ajax()
                    .sync()
                    .get(app.config.host + "/System/GetDmDvcsByUser");
                var json = JSON.parse(result.response);

                // $$("dclhe").setValue(json[0].address);
                $$("tnnt").setValue(json[0].name);
                $$("mst").setValue(json[0].tax_code);
                $$("tcqt").setValue(json[0].cqt);
                // $$("dctdtu").setValue(json[0].email);
                // $$("nlhe").setValue(json[0].manage);
            } else {
                if (editmode == 2) icon = "<i class='fa fa-edit' aria-hidden='true'></i>";
                else icon = "<i class='fa fa-trash fa-lg' aria-hidden='true'></i>";
            }
            webix.$$("head-icondksd").setHTML(icon);
            webix.$$("head-icondksd").refresh();
        },
        setValues: function (item, winconfig, params) {
            // var format = webix.Date.dateToStr("%d/%m/%Y");
            // var time = new Date(item.ngay);
            // text = removeTimeZoneT(time);
            $$("danhsach_hoadonhuy").parse(item.detail);
            $$("huyhoadon").parse(item);
        },
        setValuesTongQuat: function (item, winconfig, params) {
            var data = $$("huyhoadon").getValues();
            data.so = item.so;
            data.ntbccqt = item.ntbccqt;
            data.loai = item.loai;
            $$("huyhoadon").parse(data);
        },
    };
});
