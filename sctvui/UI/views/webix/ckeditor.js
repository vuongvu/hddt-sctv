webix.protoUI({
	name: "ckeditor",
	$init: function (config) {
		this.$view.className += " webix_selectable";
		this._waitEditor = webix.promise.defer();
	},
	defaults: {
		borderless: true,
		language: "vi",
		toolbar: [
			{ name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'] },
			{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
			{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
			{ name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
			'/',
			{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat'] },
			{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'] },
			{ name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
			{ name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
			'/',
			{ name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
			{ name: 'colors', items: ['TextColor', 'BGColor'] },
			{ name: 'tools', items: ['Maximize', 'ShowBlocks'] },
			{ name: 'about', items: ['About'] }
		]
	},
	_init_ckeditor_once: function () {
		var tid = this.config.textAreaID = "t" + webix.uid();
		this.$view.innerHTML = "<textarea id='" + tid + "'>" + this.config.value + "</textarea>";

		this._3rd_editor = CKEDITOR.replace(this.config.textAreaID, {
			toolbar: this.config.toolbar,
			language: this.config.language,
			width: this.$width - 2,
			height: this.$height - 140
		});
		this._waitEditor.resolve(this._3rd_editor);

		this._init_ckeditor_once = function () { };
	},
	_render_ckeditor: function () {
		var initMethod = "replace";
		if (this.config.editorType === "inline") {
			CKEDITOR.disableAutoInline = true;
			initMethod = "inline";
		}

		this._3rd_editor = CKEDITOR[initMethod](this.config.textAreaID, {
			toolbar: this.config.toolbar,
			language: this.config.language,
			width: this.$width - 2,
			height: this.$height - this.config.barHeight


		});
		console.log(this.config.barHeight);
		this._waitEditor.resolve(this._3rd_editor);
	},
	_set_inner_size: function (x, y) {
		//alert(x+"-"+y);
		if (!this._3rd_editor || !this._3rd_editor.container || !this.$width || this.config.editorType === "inline") return;
		this._3rd_editor.resize(x, y);
	},
	$setSize: function (x, y) {
		//alert(x+"-"+y);
		if (webix.ui.view.prototype.$setSize.call(this, x, y)) {
			this._init_ckeditor_once();
			this._set_inner_size(x, y);
		}
	},
	setValue: function (value) {
		this.config.value = value;

		if (this._3rd_editor && this._3rd_editor.status === "ready")
			this._3rd_editor.setData(value);
		else webix.delay(function () {
			this.setValue(value);
		}, this, [], 100);
	},
	getValue: function () {
		return this._3rd_editor ? this._3rd_editor.getData() : this.config.value;
	},
	focus: function () {
		this._focus_await = true;
		if (this._3rd_editor)
			this._3rd_editor.focus();
	},
	getEditor: function (waitEditor) {
		return waitEditor ? this._waitEditor : this._3rd_editor;
	}
}, webix.ui.view);