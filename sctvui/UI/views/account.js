define(["app", "views/forms/account", "views/forms/account_hsm", "locale", "views/forms/accountpermission"], function (app, dmwindowform, account_hsm, _, accountpermission) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Người sử dụng", details: "Danh mục người sử dụng" });

    var controls = [
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: _("THEM") + " (F4)",
            width: 100,
            css: "webix_secondary",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var win = this.$scope.ui(dmwindowform.$ui);
                    dmwindowform.setEditMode(1);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            label: _("SUA") + " (F3)",
            width: 90,
            css: "webix_secondary",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("dmtab-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();
                    if (item.username == "ADMINISTRATOR") {
                        webix.message("Bạn không được phép sửa tài khoản này !", "error");
                        return;
                    }

                    form.setValues(item);

                    dmwindowform.setEditMode(2);
                    detailForm.show();

                    var cb = $$("cbBranchCode");
                    cb.define("readonly", true);
                    cb.refresh();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            label: _("XOA") + " (F8)",
            width: 90,
            css: "webix_secondary",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();
                    if (item.username == "ADMINISTRATOR") {
                        webix.message("Bạn không được phép xóa tài khoản này !", "error");
                        return;
                    }

                    item.window_type = "account";

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.username,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Account/Delete", item, function (text, xml, xhr) {
                                        var json = JSON.parse(text);
                                        if (json.hasOwnProperty("error")) {
                                            webix.message(json.error, "debug");
                                            return;
                                        }
                                        table.remove(item.id);
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnPhanQuyen",
            view: "button",
            type: "icon",
            icon: "fas fa-lock",
            label: _("PHAN_QUYEN"),
            width: 100,
            css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn người sử dụng", "error");
                        return;
                    }

                    var win = this.$scope.ui(accountpermission.$ui);
                    win.config.wb_user_id = item.id;
                    accountpermission.initUI(item);
                    win.getHead().setHTML("Người sử dụng: " + item.username);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnCreateUserHSM",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: "Thêm/Sửa User HSM",
            width: 150,
            css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn người sử dụng", "error");
                        return;
                    }
                    var win = this.$scope.ui(account_hsm.$ui);

                    webix
                        .ajax()
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Account/GetUserHsmById?wb_user_id=" + item.wb_user_id, function (res) {
                            var json = JSON.parse(res);
                            if (json.length > 0) {
                                var form = webix.$$("accountHSM-form");
                                form.setValues(json[0]);
                                $$("huyUserHSM").show();
                            } else {
                                var form = webix.$$("accountHSM-form");
                            }
                        });

                    // var form = webix.$$("accountHSM-form");
                    // form.setValues(item);

                    // dmwindowform.setEditMode(1);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                url: app.config.host + "/Account/AllUsersByBranch",
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    {
                        id: "username",
                        header: ["Tên truy cập", { content: "textFilter" }],
                        width: 150,
                    },
                    {
                        id: "wb_role_id",
                        view: "combo",
                        options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00007",
                        header: ["Nhóm quyền", { content: "selectFilter" }],
                        width: 250,
                    },
                    {
                        id: "email",
                        header: ["Email", { content: "textFilter" }],
                        width: 200,
                    },
                    {
                        id: "fullname",
                        header: ["Tên người sử dụng", { content: "textFilter" }],
                        width: 200,
                    },
                    {
                        id: "is_sign_invoice",
                        header: [
                            "Ký hóa đơn",
                            {
                                content: "selectFilter",
                                options: [
                                    { id: "", value: "" },
                                    { id: "C", value: "C" },
                                    { id: "K", value: "K" },
                                ],
                            },
                        ],
                        width: 100,
                    },
                    {
                        id: "is_sign_invoice_hsm",
                        header: [
                            "Ký hóa đơn HSM",
                            {
                                content: "selectFilter",
                                options: [
                                    { id: "", value: "" },
                                    { id: "C", value: "C" },
                                    { id: "K", value: "K" },
                                ],
                            },
                        ],
                        width: 140,
                    },
                    {
                        id: "is_view_user",
                        header: [
                            "Xem",
                            {
                                content: "selectFilter",
                                options: [
                                    { id: "", value: "" },
                                    { id: "C", value: "C" },
                                    { id: "K", value: "K" },
                                ],
                            },
                        ],
                        width: 60,
                    },
                    {
                        id: "is_new_user",
                        header: [
                            "Mới",
                            {
                                content: "selectFilter",
                                options: [
                                    { id: "", value: "" },
                                    { id: "C", value: "C" },
                                    { id: "K", value: "K" },
                                ],
                            },
                        ],
                        width: 60,
                    },
                    {
                        id: "is_edit_user",
                        header: [
                            "Sửa",
                            {
                                content: "selectFilter",
                                options: [
                                    { id: "", value: "" },
                                    { id: "C", value: "C" },
                                    { id: "K", value: "K" },
                                ],
                            },
                        ],
                        width: 60,
                    },
                    {
                        id: "is_del_user",
                        header: [
                            "Xóa",
                            {
                                content: "selectFilter",
                                options: [
                                    { id: "", value: "" },
                                    { id: "C", value: "C" },
                                    { id: "K", value: "K" },
                                ],
                            },
                        ],
                        width: 60,
                    },
                    {
                        id: "explanation",
                        header: ["Diễn giải", { content: "textFilter" }],
                        width: 350,
                        hidden: true,
                    },
                    {
                        id: "store_code",
                        header: ["Mã cửa hàng", { content: "textFilter" }],
                        width: 150,
                        hidden: true,
                    },
                    {
                        id: "branch_code",
                        header: ["Chi nhánh", { content: "textFilter" }],
                        width: 150,
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
