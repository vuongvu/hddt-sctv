define(["models/user", "views/forms/changepass", "locale", "views/forms/chat", "views/hotroform", "views/menus/version"], function (user, changepass, _, chat, hotroform, version) {
    return {
        $ui: {
            view: "popup",
            id: "profilePopup",
            css: "profilePopup",
            resize: true,
            autofit: true,
            minHeight: 250,
            minWidth: 300,
            body: {
                view: "list",
                select: true,
                datatype: "json",
                css: "profile",
                data: [
                    { id: "1", icon: "mdi mdi-key", value: "USER" },
                    { id: "changepass", icon: "mdi mdi-key", value: _("CHANGE_PASS") },
                    { id: "delcache", icon: "mdi mdi-settings", value: _("DEL_CACHE") },
                    { id: "signOut", icon: "mdi mdi-logout", value: _("LOGOUT") },
                    // { $template: "Separator" },
                    // { id: "hotro", icon: "mdi mdi-ambulance", value: _("MVSUPPORT") },
                    // { id: "chat", icon: "mdi mdi-comment-account-outline", value: _("MVCHAT") }
                    // { $template: "Separator" },
                ],
                template: function (obj) {
                    if (obj.value == "USER") {
                        var k =
                            "<div item_id='id' class='custom_profile'>" +
                            "<p><i class='fas fa-user-circle fa-lg'></i><span style='margin-left:8px;'> " +
                            user.getCurrentUser() +
                            "</span></p><br>" +
                            // + "<p><b>Tên công ty: </b>1</p></br>"
                            // + "<p><b>Mã số thuế: </b>2</p></br>"
                            "<p>Phiên bản: 1.0.0.0</p></br>" +
                            "<p>Copyrights © 2021 Phần mềm Hóa đơn điện tử SCTV</p></br>" +
                            "<a style ='float:right;text-decoration: none;color:blue;' onclick=\"var menu=$$('app:menu'); menu.$scope.show('./infocompany');\" >Xem thêm</a>" +
                            "</div>" +
                            "</div>";
                        return k;
                    } else {
                        return "<span style ='color: #475466' class='webix_icon alerts " + obj.icon + "' ></span><span> " + obj.value + "</span>";
                    }
                },
                type: {
                    height: "auto",
                },
                // view: "submenu",
                // id: "profilePopup",
                // width: 200,
                // padding: 0,
                // data: [
                // 	/* {id: 1, icon: "user", value: "Thông tin cá nhân"},
                // 	{id: 2, icon: "cog", value: "Thông tin tài khoản"},
                // 	{id: 3, icon: "calendar", value: "Lịch làm việc"},
                // 	{id: 5, icon: "tasks", value: "Nhiệm vụ"},
                // 	{ $template:"Separator" }, */
                // 	{ id: "changepass", icon: "mdi mdi-key", value: _("CHANGE_PASS") },
                // 	{ id: "delcache", icon: "mdi mdi-settings", value: _("DEL_CACHE") },
                // 	{ $template: "Separator" },
                // 	// { id: "hotro", icon: "mdi mdi-ambulance", value: _("SCTV Hỗ trợ") },
                // 	// { id: "chat", icon: "mdi mdi-comment-account-outline", value: _("SCTV Chat") },
                // 	// { $template: "Separator" },
                // 	{ id: "signOut", icon: "mdi mdi-logout", value: _("LOGOUT") }

                // ],
                // type: {
                // 	template: function (obj) {
                // 		if (obj.type)
                // 			return "<div class='separator'></div>";
                // 		return "<span style ='color: #475466' class='webix_icon alerts " + obj.icon + "' ></span><span> " + obj.value + "</span>";
                // 	}
                // },
                on: {
                    onItemClick: function (id, e, node) {
                        this.hide();

                        if (id == "signOut") {
                            user.logout();
                            document.location.reload();

                            /* require(["app"], function(app){
								app.router(app.config.start);
							}); */
                        }

                        if (id == "changepass") {
                            var win = this.$scope.ui(changepass.$ui);
                            win.show();
                        }

                        if (id == "hotro") {
                            var json = version.getInfocompany();

                            var win = this.$scope.ui(hotroform.$ui);
                            hotroform.initUI(json);
                            win.show();
                        }

                        if (id == "chat") {
                            var win = this.$scope.ui(chat.$ui);
                            win.show();
                        }

                        if (id == "delcache") {
                            webix.storage.local.remove("APP_CACHE");
                            webix.storage.local.put("APP_CACHE", {});

                            webix.message("Đã xóa cache dữ liệu", "debug");
                        }
                    },
                },
            },
        },
    };
});
