define(["app"], function (app) {

	return {
		$ui: {
			view: "menu",
			id: "app:menu",
			url: app.config.host + "/System/GetMenuBar",
			css: { "background": "#005EB2", "padding-top": "0px" , "width":"70% !important" },
			autowidth:true,
			template: function (obj) {
				var html = '<div class="menubar">'
					+ '<div class="menubarimg">'
					// + '<img src="./assets/imgs/icon/' + obj.icon + '" height="30" width="30" >'
					+ ' ' + obj.icon
					// + '<i class="fad fa-chart-pie fa-2x"></i>'
					+ '</div>'
					+ '<div>' + obj.value + '</div>'
					+ '</div>'
				return html;
			},
			on: {
				onMenuItemClick: function (id) {
					var item = this.getMenuItem(id);

					if (item.url != "") {
						var win = window.open(item.url, '_blank');
						win.focus();
						return;
					}

					if (item.code == "") return;
					if (item.code == "dashboard") {
						$$("head-title").define("hidden", true);
					}
					else {
						$$("head-title").show();
					}

					var start = item.window_id != "" ? item.code + ":" + item.window_id : item.code;
					this.$scope.show("./" + start);

					var _URL = window.location.href;
					// console.log(_URL);
					// console.log(start);
					// webix.message(item.code);
					// webix.message(_URL.includes(item.window_id));
					if (!_URL.includes(item.window_id)) {
						$$("head-title").removeView("tlbParams");
						webix.$$("title").parse({ title: item.value, details: item.details });
					}
					if (item.code == "tinhhinhsudung" || item.code == "bangkebanra") {
						$$("head-title").removeView("tlbParams");
						webix.$$("title").parse({ title: item.value, details: item.details });
					}

					// webix.$$("title").parse({ title: item.value });
				}
			},
			type: {
				subsign: true
			}
		},
		getApiHost: function () {
			return app.config.host;
		}
	};

});

// return {
// 		$ui: {
// 			view: "tree",
// 			id: "app:menu",
// 			width: 250,
// 			//type: "lineTree",
// 			select: true,
// 			url: app.config.host + "/System/GetMenuBar2",
// 			autowidth: true,
// 			css:"sidebarnampv",
// 			template: function (obj, common) {
// 				if (obj.$level > 2) {
// 					return common.icon(obj, common) +  "<i >" + obj.value + "</i>";
// 				} else {
// 					return common.icon(obj, common) +  "<span >" + obj.value + "</span>";
// 				}
// 			},
// 			on: {
// 				onItemClick: function (id) {
// 					var item = this.getItem(id);

// 					if (item.url != "") {
// 						var win = window.open(item.url, '_blank');
// 						win.focus();
// 						return;
// 					}

// 					if (item.code == "") return;

// 					var start = item.window_id != "" ? item.code + ":" + item.window_id : item.code;
// 					this.$scope.show("./" + start);

// 					webix.$$("title").parse({ title: item.value, details: item.details });
// 				}
// 			},
// 			type: {
// 				subsign: true
// 			}
// 		},
// 		getApiHost: function () {
// 			return app.config.host;
// 		}
// 	};

// });

