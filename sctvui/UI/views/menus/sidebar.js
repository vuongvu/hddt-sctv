define(["app"],function(app){
	
	return {
		$ui:{
			width: 220,

			rows:[
				{
					view: "tree",
					//id: "app:menu",
					type: "menuTree2",
					css: "menu",
					activeTitle: true,
					select: true,
					tooltip: {
						template: function(obj){
							return obj.$count?"":obj.details;
						}
					},
					on:{
						onBeforeSelect:function(id){
							if(this.getItem(id).$count){
								debugger;
								return false;
							}
							
						},
						onAfterSelect:function(id){
							
							var item = this.getItem(id);
							
							var start = item.window_id!=null ? item.code + ":" + item.window_id : item.code;
							this.$scope.show("./"+ start);
							
							webix.$$("title").parse({title: item.value, details: item.details});
						}
					},
					url: app.config.host + "/api/System/GetSideBar"
				}
			]
		},
		getApiHost: function(){
			return app.config.host;
		}
	};

});
