﻿// JavaScript source code
define(["app", "models/user"], function (app, user) {
    var checkDataEmpty = 0;
    return {
        $ui: {
            view: "popup",
            id: "notifyPopup",
            css: "notifyPopup",
            resize: true,
            autofit: true,
            minHeight: 250,
            minWidth: 300,
            body: {
                view: "list",
                select: true,
                datatype: "json",
                css: "profile",
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var url = app.config.host + "/Invoice68/GetInvoiceNotify";

                        return webix.ajax(url, callback, view);

                        //webix.ajax().get(app.config.host + "/Invoice68/GetInvoiceNotify", function (text) {
                        //    var listcerts = JSON.parse(text);
                        //    if (listcerts.length > 0) {
                        //        return listcerts;
                        //    }
                        //    else {
                        //        var itemEmpty = {
                        //            tgian_gui: null,
                        //            contentnotify: "Không có dữ liệu để hiển thị."
                        //        };
                        //        listcerts.push(itemEmpty);
                        //        return listcerts;
                        //    }
                        //    //return webix.ajax(url, callback, view);
                        //});
                    }
                },
                template: function (obj) {
                    if (obj.tgian_gui != null) {
                        return "<div><p style='margin-bottom: 0px !important; margin: 0 0 10px; font-size: 14px;'><b>" + obj.contentnotify + "</b></p></div>" + "<div><p style='margin-bottom: 0px !important; margin: 0 0 10px'>" + obj.tgian_gui + "</p></div>";
                    }
                    else {
                        document.getElementsByClassName("notifyPopup")[0].style.height = "80px";
                        return "<div><p style='margin-bottom: 0px !important; margin: 0 0 10px; font-size: 14px;'><b>" + obj.contentnotify + "</b></p></div>";
                    }
                },
                type: {
                    height: "auto",
                },
                on: {
                    onAfterLoad: function () {
                        document.getElementsByClassName("notifyPopup")[0].style.borderRadius = "8px";
                    },
                }
            },
        },
    };
});
