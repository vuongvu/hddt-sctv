
define(["app"
], function (app) {

    function initUI(item) {
        $$("lstUser").select(item.id);
        $$("content").setHTML(item.inv_content);
        $$("notification-win").getHead().setHTML("Thông tin bản cập nhật: " + item.version);
    }

    return {
        $ui: {
            view: "window", modal: true, id: "notification-win", position: "center", resize: true, move: true,
            head: "Thông tin bản cập nhật",
            body: {
                view: "form",
                id: "notification-form",
                height: 500,
                complexData: true,
                elements: [
                    {
                        cols: [
                            {
                                view: "list",
                                id: "lstUser",
                                select: true,
                                on: {
                                    onItemClick: function (id, e, node) {
                                        var item = this.getItem(id);
                                        //webix.message(id, "error");
                                        $$("notification-win").getHead().setHTML("Thông tin bản cập nhật: " + item.version);
                                        $$("content").setHTML(item.inv_content);
                                    }
                                },
                                url: app.config.host + "/System/GetNotification",
                                // data: [
                                //     { id: 1, version: "3.1.0.0", update: "Lorem ipsum dolor sit amet", personId: 1 },
                                //     { id: 2, version: "3.2.0.0", update: "Praesent luctus nulla enim, pellentesque condimentum ", personId: 2 },
                                //     { id: 3, version: "3.3.0.0", update: "Lorem ipsum dolor sit amet", personId: 2 },
                                //     { id: 4, version: "3.4.0.0", update: "Morbi eget facilisis risus", personId: 1 },
                                //     { id: 5, version: "3.5.0.0", update: "Cras lacinia bibendum arcu", personId: 1 }
                                // ],
                                template: "- #version#",
                                //template: "<span class='webix_icon fa-user'></span> <span class='rowBold'>#User# </span>",
                                width: 150
                            },
                            {
                                width: 900,
                                paddingX: 5,
                                paddingX: 5,
                                rows: [
                                    {
                                        view: "template", id: "content", scroll: "xy",
                                        // , gravity: 3
                                        type: { height: "auto" },
                                        // template: function (obj) {
                                        //     return "<span " + (obj.user == user.getCurrentUser() ? "class='rowBold'" : "class='rowBoldGreen'") + ">" + obj.user + "</span> " + obj.value;
                                        // }
                                    },
                                    {
                                        paddingY: 5,
                                        cols: [
                                            {},
                                            // {
                                            //     view: "button", value: "Cập nhật", width: 100, type: "danger",
                                            //     click: function () {

                                            //     }
                                            // },
                                            {
                                                view: "button", type: "danger", label: "Đóng (ESC)", shortcut: "esc", align: "center", width: 100,
                                                on: {
                                                    onClick: function () {
                                                        webix.$$("notification-win").close();
                                                    }
                                                },
                                                click: function () {
                                                    this.callEvent("onClick");
                                                }
                                            },
                                            {}
                                        ]
                                    }
                                ]
                            }
                        ]
                    }

                ]
            }
        },
        initUI: initUI
    };

});