define(["app", "views/forms/chukyso", "locale", "views/modules/minvoiceplugin"], function (app, dmwindowform, _, plugin) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Đăng ký chứng thư số", details: "Danh sách chứng thư số" });

    var controls = [
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: _("THEM") + " (F4)",
            width: 85,
            css: "webix_secondary",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var url = app.config.host + "/System/ExecuteCommand";

                    webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(
                            url,
                            {
                                command: "CM00019",
                                parameter: {
                                    ma_dvcs: "",
                                },
                            },
                            function (text) {
                                var array = JSON.parse(text);

                                var kytaptrung = "";
                                var server_cert = "";

                                for (var i = 0; i < array.length; i++) {
                                    var item = array[i];

                                    if (item.code == "KYTAPTRUNG") {
                                        kytaptrung = item.value;
                                    }

                                    if (item.code == "FILE") {
                                        kytaptrung = item.value;
                                    }

                                    if (item.code == "SERVER_CERT") {
                                        server_cert = item.value;
                                    }
                                }

                                if (kytaptrung == "C") {
                                    var box = webix.modalbox({
                                        title: "Chọn chứng thư số",
                                        buttons: ["Nhận", "Hủy bỏ"],
                                        text: "<div id='ctnCerts'></div>",
                                        top: 100,
                                        width: 750,
                                        callback: function (result) {
                                            switch (result) {
                                                case "1":
                                                    webix.modalbox.hide();
                                                    break;
                                                case "0":
                                                    var table = $$("tblCerts");

                                                    var item = {
                                                        certs: table.serialize(),
                                                    };

                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(app.config.host + "/Token/SaveCerts", item, function (text) {
                                                            var json = JSON.parse(text);

                                                            if (!json.hasOwnProperty("error")) {
                                                                var table = $$("dmwindowData");
                                                                table.clearAll();
                                                                table.load(table.config.url);

                                                                webix.modalbox.hide();
                                                            } else {
                                                                webix.message(json.error);
                                                            }
                                                        });
                                                    break;
                                            }
                                        },
                                    });

                                    var popupText = document.getElementsByClassName("webix_popup_text")[0];
                                    popupText.style.paddingTop = 0;

                                    var tblMatHang = webix.ui({
                                        container: "ctnCerts",
                                        id: "tblCerts",
                                        view: "datatable",
                                        borderless: true,
                                        height: 350,
                                        columns: [
                                            {
                                                id: "chon",
                                                header: _("CHON"),
                                                width: 65,
                                                checkValue: true,
                                                uncheckValue: false,
                                                template: "{common.checkbox()}",
                                            },
                                            {
                                                id: "cert_type",
                                                header: _("Loại"),
                                                width: 100,
                                            },
                                            {
                                                id: "so_serial",
                                                header: _("SO_SERIAL"),
                                                width: 150,
                                            },
                                            {
                                                id: "subjectname",
                                                header: _("SUBJECTNAME"),
                                                width: 450,
                                            },
                                            {
                                                id: "ngaybatdau",
                                                header: _("NGAY_BAT_DAU"),
                                                width: 200,
                                            },
                                            {
                                                id: "ngayketthuc",
                                                header: _("NGAY_HET_HAN"),
                                                width: 200,
                                            },
                                            {
                                                id: "tokenSerial",
                                                header: _("TOKEN_SERIAL"),
                                                width: 150,
                                            },
                                            {
                                                id: "issuer",
                                                header: _("DONVI"),
                                                width: 450,
                                            },
                                        ],
                                        url: server_cert + "/Token/AllCertificates",
                                    });
                                } else {
                                    if (kytaptrung == "FILE") {
                                        var box = webix.modalbox({
                                            title: "Chọn chứng thư số",
                                            buttons: ["Nhận", "Hủy bỏ"],
                                            text: "<div id='ctnCerts'></div>",
                                            top: 100,
                                            width: 750,
                                            callback: function (result) {
                                                switch (result) {
                                                    case "1":
                                                        webix.modalbox.hide();
                                                        break;
                                                    case "0":
                                                        var table = $$("tblCerts");

                                                        var item = {
                                                            certs: table.serialize(),
                                                        };

                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-type": "application/json" })
                                                            .post(app.config.host + "/Token/SaveChukysoFile", item, function (text) {
                                                                var json = JSON.parse(text);

                                                                if (!json.hasOwnProperty("error")) {
                                                                    var table = $$("dmwindowData");
                                                                    table.clearAll();
                                                                    table.load(table.config.url);

                                                                    webix.modalbox.hide();
                                                                } else {
                                                                    webix.message(json.error);
                                                                }
                                                            });
                                                        break;
                                                }
                                            },
                                        });

                                        var popupText = document.getElementsByClassName("webix_popup_text")[0];
                                        popupText.style.paddingTop = 0;

                                        var tblMatHang = webix.ui({
                                            container: "ctnCerts",
                                            id: "tblCerts",
                                            view: "datatable",
                                            borderless: true,
                                            height: 350,
                                            columns: [
                                                {
                                                    id: "chon",
                                                    header: _("CHON"),
                                                    width: 65,
                                                    checkValue: true,
                                                    uncheckValue: false,
                                                    template: "{common.checkbox()}",
                                                },
                                                {
                                                    id: "serial_number",
                                                    header: _("SO_SERIAL"),
                                                    width: 250,
                                                },
                                                {
                                                    id: "file_path",
                                                    header: _("Đường dẫn"),
                                                    width: 250,
                                                },
                                                // {
                                                //     id: "pass",
                                                //     header: _("Mật khẩu"),
                                                //     type:"password",
                                                //     editor:"password",
                                                //     width: 200
                                                // }
                                            ],
                                            url: app.config.host + "/Token/AllCertificatesFile",
                                        });
                                    } else {
                                        var hub = plugin.hub();

                                        hub.off("resCommand");
                                        hub.on("resCommand", function (result) {
                                            var json = JSON.parse(result);

                                            if (json.hasOwnProperty("error")) {
                                                webix.message({ type: "error", text: json.error });
                                            } else {
                                                var callback = json.callback;

                                                if (callback == "RegisterCert") {
                                                    var item = {
                                                        chon: true,
                                                        so_serial: json.data.so_serial,
                                                        subjectname: json.data.subjectname,
                                                        issuer: json.data.issuer,
                                                        ngaybatdau: json.data.ngaybatdau,
                                                        ngayketthuc: json.data.ngayketthuc,
                                                        tokenSerial: "",
                                                    };

                                                    var certs = [];
                                                    certs.push(item);

                                                    var parameter = {
                                                        certs: certs,
                                                    };

                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(app.config.host + "/Token/SaveCerts", parameter, function (text) {
                                                            var json = JSON.parse(text);

                                                            if (!json.hasOwnProperty("error")) {
                                                                var table = $$("dmwindowData");
                                                                table.clearAll();
                                                                table.load(table.config.url);
                                                            } else {
                                                                webix.message(json.error);
                                                            }
                                                        });
                                                }
                                            }
                                        });

                                        var arg = {
                                            callback: "RegisterCert",
                                        };

                                        hub.invoke("execCommand", arg.callback, JSON.stringify(arg));
                                    }
                                }
                            }
                        );
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            label: _("SUA") + " (F3)",
            width: 80,
            css: "webix_secondary",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("chukyso-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    //var chon = item.chon;

                    //item.chon = chon == true ? "true" : "false";

                    var format = webix.Date.dateToStr("%d/%m/%Y %H:%i:%s");

                    item.begin_date = removeTimeZoneT(item.begin_date);
                    item.end_date = removeTimeZoneT(item.end_date);

                    form.setValues(item);

                    dmwindowform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            css: "webix_secondary",
            label: _("XOA") + " (F8)",
            width: 80,
            //css:"button_raised",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    var khData = {};
                    khData.windowid = app.path[1].params[0];
                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(item);

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.cer_serial,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/System/Save", khData, function (text) {
                                        var json = JSON.parse(text);

                                        if (json.hasOwnProperty("error")) {
                                            webix.message(json.error);
                                            return;
                                        }

                                        table.remove(item.id);
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "icon",
            icon: "fas fa-file-text-o",
            css: "webix_secondary",
            label: "Tải file",
            width: "90",
            on: {
                onClick: function () {
                    var form = {
                        view: "form",
                        elements: [
                            {
                                id: "txtPass",
                                view: "text",
                                label: "Mật khẩu",
                                labelPosition: "top",
                                required: true,
                                type: "password",
                            },
                            {
                                view: "uploader",
                                id: "upl1",
                                autosend: false,
                                value: "Chọn file",
                                link: "mylist",
                                upload: app.config.host + "/Token/UploadCertFile",
                            },
                            {
                                view: "list",
                                id: "mylist",
                                type: "uploader",
                                autoheight: true,
                                borderless: true,
                            },
                            {
                                view: "button",
                                label: "Lưu file",
                                type: "form",
                                click: function () {
                                    var formData = {
                                        pass: $$("txtPass").getValue(),
                                    };

                                    $$("upl1").define("formData", formData);

                                    $$("upl1").send(function (res) {
                                        if (res.hasOwnProperty("error")) {
                                            webix.message({ type: "error", text: res.error });
                                        } else {
                                            webix.message("Đã tải thành công");

                                            var win = $$("upl1").getTopParentView();
                                            win.close();

                                            var table = $$("dmwindowData");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        }
                                    });
                                },
                            },
                            {
                                view: "button",
                                label: "Hủy bỏ",
                                type: "danger",
                                shortcut: "esc",
                                click: function () {
                                    var win = this.getTopParentView();
                                    win.close();
                                },
                            },
                        ],
                    };

                    var comp = {
                        view: "window",
                        position: "center",
                        modal: true,
                        head: "Đăng ký File chứng thư số",
                        body: form,
                    };

                    var win = this.$scope.ui(comp);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "icon",
            icon: "mdi mdi-sim",
            css: "webix_secondary",
            label: "Thêm Sim PKI",
            width: "120",
            on: {
                onClick: function () {
                    var form = {
                        id: "formSimPKI",
                        view: "form",
                        elements: [
                            {
                                id: "txtPhone",
                                view: "text",
                                label: "Số điện thoại",
                                labelPosition: "top",
                                required: true,
                                name: "txtPhone",
                                validate: function (value) {
                                    return /([0-9]{9,13})/.test(value);
                                },
                                invalidMessage: "Định dạng điện thoại không đúng, ví dụ: 0862262040",
                            },
                            {
                                view: "button",
                                label: "Lưu",
                                type: "form",
                                click: function () {
                                    var form = this.getParentView();
                                    if (form.validate() == false) {
                                        return;
                                    }
                                    var formData = {
                                        phone: $$("txtPhone").getValue(),
                                    };
                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Token/SaveSimPKI", formData, function (text) {
                                            var json = JSON.parse(text);

                                            if (!json.hasOwnProperty("error")) {
                                                var table = $$("dmwindowData");
                                                table.clearAll();
                                                table.load(table.config.url);
                                                var win = $$("modalSimPKI");
                                                win.close();
                                            } else {
                                                webix.message(json.error);
                                            }
                                        });
                                },
                            },
                            {
                                view: "button",
                                label: "Hủy bỏ",
                                type: "danger",
                                shortcut: "esc",
                                click: function () {
                                    var win = this.getTopParentView();
                                    win.close();
                                },
                            },
                        ],
                    };
                    var comp = {
                        id: "modalSimPKI",
                        view: "window",
                        position: "center",
                        modal: true,
                        head: "Thêm số sim PKI",
                        body: form,
                    };

                    var win = this.$scope.ui(comp);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "icon",
            icon: "mdi mdi-certificate",
            css: "webix_secondary",
            label: "Thêm HSM SCTV",
            width: "130",
            on: {
                onClick: function () {
                    var form = {
                        id: "formHSM",
                        view: "form",
                        elements: [
                            {
                                id: "certId",
                                view: "text",
                                label: "Cert Serial",
                                labelPosition: "top",
                                required: true,
                                name: "certId",
                                css:"mcls_uppercase",
                                // validate: function (value) {
                                //     return /([0-9]{9,13})/.test(value);
                                // },
                                // invalidMessage: "Định dạng điện thoại không đúng, ví dụ: 0862262040",
                            },
                            {
                                view: "button",
                                label: "Lưu",
                                type: "form",
                                click: function () {
                                    var form = this.getParentView();
                                    if (form.validate() == false) {
                                        return;
                                    }
                                    var formData = {
                                        certSerial: $$("certId").getValue().toUpperCase(),
                                    };
                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Token/SaveHSM_SCTV", formData, function (text) {
                                            var json = JSON.parse(text);

                                            if (!json.hasOwnProperty("error")) {
                                                var table = $$("dmwindowData");
                                                table.clearAll();
                                                table.load(table.config.url);
                                                var win = $$("modelHSM");
                                                win.close();
                                            } else {
                                                webix.message(json.error, "error");
                                            }
                                        });
                                },
                            },
                            {
                                view: "button",
                                label: "Hủy bỏ",
                                type: "danger",
                                shortcut: "esc",
                                click: function () {
                                    var win = this.getTopParentView();
                                    win.close();
                                },
                            },
                        ],
                    };
                    var comp = {
                        id: "modelHSM",
                        view: "window",
                        position: "center",
                        modal: true,
                        head: "Thêm HSM SCTV",
                        body: form,
                    };

                    var win = this.$scope.ui(comp);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                datafetch: 100,
                //loadahead: 15,
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var windowno = app.path[1].params[0];
                        var url = app.config.host + "/System/GetDataByWindowNo?window_id=" + windowno;

                        if (params == null) {
                            url = url + "&start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                        } else {
                            url = url + "&start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                            if (params.filter != null) {
                                var array = [];

                                for (var property in params.filter) {
                                    var column = view.getColumnConfig(property);

                                    if (column !== undefined) {
                                        var item = {
                                            columnName: property,
                                            columnType: column.columnType,
                                            value: params.filter[property],
                                        };

                                        array.push(item);
                                    }
                                }

                                //console.log(JSON.stringify(array));
                                if (array.length > 0) {
                                    url = url + "&filter=" + JSON.stringify(array);
                                } else {
                                    url = url + "&filter=null";
                                }
                            } else {
                                url = url + "&filter=null";
                            }
                        }

                        if (view.config.infoparam != null) {
                            url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
                        } else {
                            url = url + "&infoparam=null";
                        }

                        var tlbParams = $$("tlbParams");

                        if (tlbParams !== undefined) {
                            var childViews = tlbParams.getChildViews();

                            var parameters = [];

                            for (var i = 0; i < childViews.length; i++) {
                                var child = childViews[i];

                                var id = child.config.id;
                                var value_field = child.config.value_field;

                                var item = {
                                    columnName: value_field,
                                    value: $$(id).getValue(),
                                };

                                parameters.push(item);
                            }

                            url = url + "&tlbparam=" + JSON.stringify(parameters);
                        } else {
                            url = url + "&tlbparam=null";
                        }

                        //console.log(url);

                        webix.ajax(url, callback, view);
                    },
                },
                columns: [
                    { id: "cer_serial", header: _("SO_SERIAL"), width: 250 },
                    { id: "subject_name", header: _("SUBJECTNAME"), width: 550 },
                    { id: "signer", header: _("NGUOI_KY"), width: 150 },
                    { id: "cert_type", header: "Loại", width: 90 },
                    /* {
                        id: "chon",
                        checkValue: "true",
                        uncheckValue: "false",
                        template: function (obj, common, value) {
                            if (value)
                                return "<div class='webix_icon fa-check-square-o'></div>";
                            else
                                return "<div class='webix_icon fa-square-o'></div>";
                        },
                        header: _("CHON"),
                        width: 100
                    }, */
                    {
                        id: "begin_date",
                        header: _("NGAY_BAT_DAU"),
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y %H:%i:%s");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    {
                        id: "end_date",
                        header: _("NGAY_HET_HAN"),
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y %H:%i:%s");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    { id: "issuer", header: _("DONVI"), width: 550 },
                    {
                        id: "file_name",
                        header: "Tên File",
                        width: 120,
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
