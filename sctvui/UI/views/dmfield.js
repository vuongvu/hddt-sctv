define([
	"app",
	"views/forms/dmfield",
	"locale"
], function (app, dmfieldform, _) {
	'use strict';

	//var windowno = app.path[1].params[0];
	webix.$$("title").parse({ title: "Điều khiển > Danh mục field", details: "Danh mục field system" });
	var submenu = webix.ui({
		view: "submenu",
		width: 300,
		padding: 0,
		data: [
			{ id: "importcolumns", icon: "fas fa-angle-right", value: "Tải cột" },
			{ id: "createtable", icon: "fas fa-angle-right", value: "Tạo bảng dữ liệu" },
			{ id: "createproccrud", icon: "fas fa-angle-right", value: "Tạo thủ tục (Thêm - Sửa - Xóa)" },
			{ id: "processstt", icon: "fas fa-angle-right", value: "Đánh lại số thứ tự" }
		],
		on: {
			onItemClick: function (id, e, node) {
				if (id == "importcolumns") {

					var url = app.config.host + "/System/InsertFieldsByTabId";
					var tabId = webix.$$("txtTabId").getValue();

					webix.ajax()
						.headers({ 'Content-type': 'application/json' })
						.post(url, { id: tabId }, function (text) {
							var json = JSON.parse(text);

							if (!json.hasOwnProperty("error")) {
								loadData();
							}
							else {
								webix.message(json.error);
							}

						});
				}

				if (id == "createtable") {

					var url = app.config.host + "/System/AutoCreateTable";
					var tabId = webix.$$("txtTabId").getValue();

					webix.ajax()
						.headers({ 'Content-type': 'application/x-www-form-urlencoded' })
						.post(url, "id=" + tabId, function (text) {
							var json = JSON.parse(text);

							if (!json.hasOwnProperty("error")) {
								loadData();
							}
							else {
								webix.message(json.error);
							}

						});
				}

				if (id == "createproccrud") {

					var url = app.config.host + "/System/AutoCreateCmd";
					var tabId = webix.$$("txtTabId").getValue();

					webix.ajax()
						//.headers({'Content-type':'application/x-www-form-urlencoded'})
						.post(url, "id=" + tabId, function (text) {
							var json = JSON.parse(text);

							if (!json.hasOwnProperty("error")) {
								loadData();
								webix.message("Đã thực hiện xong");
							}
							else {
								webix.message(json.error);
							}

						});
				}

				if (id == "processstt") {
					var table = webix.$$("dmfieldData");
					var numberRow = table.count();

					var url = "/WbField/Update";

					for (var i = 0; i < numberRow; i++) {
						var id = table.getIdByIndex(i);
						var item = table.getItem(id);

						item.stt = i + 1;

						webix.ajax().post(app.config.host + url, item, function (text) {
							var data = JSON.parse(text);

							if (!data.hasOwnProperty("error")) {
								webix.$$("dmfieldData").updateItem(item.id, item);

							}

						});
					}



				}
			}
		}
	});

	var controls = [
		{
			view: "button", type: "htmlbutton", width: 100,
			label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' +
			'<span class="text">' + "Thêm (F4)" + '</span>',
			id: "btnPlus",
			// view: "button",
			// type: "icon",
			// icon: "fas fa-plus",
			// label: "Thêm (F4)",
			// css: "webix_secondary",
			// width: 90,
			shortcut: "F4",
			on: {
				onClick: function () {
					var win = this.$scope.ui(dmfieldform.$ui);
					var form = webix.$$("dmfield-form");

					if (webix.$$("txtTabId").getValue() == "") {
						webix.message("Bạn chưa chọn Tab Id")
						return;
					}

					var item = {
						wb_tab_id: $$("txtTabId").getValue()
					};

					form.setValues(item);

					dmfieldform.setEditMode(1);
					win.show();
				}
			},
			click: function () {
				this.callEvent("onClick");
			}
		},
		{
			view: "button", type: "htmlbutton", width: 100,
			label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' +
			'<span class="text">' + "Sửa (F3)" + '</span>',
			id: "btnEdit",
			// view: "button",
			// type: "icon",
			// icon: "fas fa-edit",
			// label: "Sửa (F3)",
			// css: "webix_secondary",
			// width: 60,
			shortcut: "F3",
			on: {
				onClick: function () {
					var detailForm = this.$scope.ui(dmfieldform.$ui);
					var form = webix.$$("dmfield-form");

					var table = webix.$$("dmfieldData");
					var item = table.getSelectedItem();

					form.setValues(item);

					dmfieldform.setEditMode(2);
					detailForm.show();
				}
			},
			click: function () {
				this.callEvent("onClick");
			}
		},
		{
			view: "button", type: "htmlbutton", width: 100,
			label: '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> ' +
			'<span class="text">' + "Xóa (F8)" + '</span>',
			id: "btnDelete",
			// view: "button",
			// type: "icon",
			// icon: "fas fa-trash-o",
			// label: "Xóa (F8)",
			// css: "webix_secondary",
			// width: 80,
			shortcut: "F8",
			on: {
				onClick: function () {
					var table = webix.$$("dmfieldData");
					var item = table.getSelectedItem();

					webix.confirm({
						text: "Bạn có muốn xóa " + item.column_name, ok: "Có", cancel: "Không",
						callback: function (res) {
							if (res) {
								webix.ajax().post(app.config.host + "/WbField/Delete", item, function (text, xml, xhr) {
									if (text.hasOwnProperty("error")) {
										webix.message(text.error);
										return;
									}

									table.remove(item.id);

								});

							}
						}
					});
				}
			},
			click: function () {
				this.callEvent("onClick");
			}

		},
		{
			view: "button", type: "htmlbutton", width: 100,
			label: '<i class="fa fa-files-o fa-lg" aria-hidden="true"></i> ' +
			'<span class="text">' + "Sao chép" + '</span>',
			id: "btnCopy",
			// view: "button",
			// type: "icon",
			// icon: "fas fa-files-o",
			// label: "Sao chép",
			// css: "webix_secondary",
			// width: 85,
			on: {
				onClick: function () {
					var detailForm = this.$scope.ui(dmfieldform.$ui);
					var form = webix.$$("dmfield-form");

					var table = webix.$$("dmfieldData");
					var item = table.getSelectedItem();

					if (item == null) {
						webix.message({ type: "error", text: "Bạn chưa chọn mục field" });
						return;
					}

					item.id = null;
					form.setValues(item);

					dmfieldform.setEditMode(1);
					detailForm.show();
				}
			},
			click: function () {
				this.callEvent("onClick");
			}
		},
		{
			view: "button", type: "htmlbutton", width: 110,
			label: '<i class="fa fa-angle-down fa-lg" aria-hidden="true"></i> ' +
			'<span class="text">' + "Chức năng" + '</span>',
			id: "btnChucNang",
			// view: "button",
			// type: "icon",
			// icon: "fas fa-angle-down",
			// label: "Chức năng",
			// css: "webix_secondary",
			// width: 100,
			click: function () {
				submenu.show(webix.$$("btnChucNang").$view);
			}
		},
		{},
		{
			view: "combo",
			width: 400,
			label: "Tab ID",
			id: "txtTabId",
			options: {
				view: "gridsuggest",
				id: "grid_tab",
				// filter: function (obj, value) {
				//     var string = "";
				//     if (obj) {
				//         string += obj.id + " ";
				//         string += obj.window_name + " ";

				//     }
				//     return string.toLowerCase().indexOf(value.toLowerCase()) >= 0;
				// },
				// template: function (item) {
				//     return item.name + " ";
				//},
				template: "#name#",
				width: 550,
				body: {
					scroll: true,
					autoheight: false,
					columns: [
						{ id: "code", header: "Window Id", editor: "text", width: 100 },
						{ id: "name", header: "Window Name", editor: "text", width: 400 }
					],
					url: app.config.host + "/WbTab/All"
				}
			},
			// suggest: {
			// 	url: app.config.host + "/WbTab/All",
			// 	template: "#tab_name#",
			// 	body: {
			// 		template: "#tab_name#"
			// 	}
			// },
			on: {
				onChange: function () {
					loadData();
				}
			}
		}
	];

	var grid = {
		rows: [
			{
				id: "dmfieldData",
				view: "datatable",
				select: true,
				resizeColumn: true,
				datafetch: 2,
				loadahead: 15,
				drag: true,
				on: {
					onItemDblClick: function (id, e, node) {
						var btnEdit = $$("btnEdit");
						btnEdit.callEvent("onClick");
					}
				},
				columns: [
					{ id: "wb_tab_id", header: "Tab Id", width: 100, hidden: true },
					{ id: "ord", header: "STT", width: 75 },
					{ id: "column_name", header: "Column Name", width: 150 },
					{ id: "row", header: "Row", width: 60 },
					{ id: "col", header: "Col", width: 60 },
					{ id: "width", header: "Width", width: 80 },
					{ id: "column_width", header: "Column Width", width: 150 },
					{ id: "column_type", header: "Column Type", width: 150 },
					{ id: "label_width", header: "Label Width", width: 150 },
					{ id: "label_position", header: "Label Position", width: 150 },
					{ id: "space", header: "SPACE", width: 150 },
					{ id: "type_editor", header: "TYPE EDITOR", width: 150 },
					{ id: "ref_id", header: "REF ID", hidden: true, width: 150 },
					{ id: "default_value", header: "Default Value", width: 250 },
					{ id: "hide_in_grid", header: "HIDE_IN_GRID", width: 120, checkValue: true, uncheckValue: false, template: "{common.rcheckbox()}" },
					{ id: "hidden", header: "Hidden", width: 80, checkValue: true, uncheckValue: false, template: "{common.rcheckbox()}" }
				],
				//columns: columns
				pager: "pagerA",
				type: {
					rcheckbox: function (obj, common, value, config) {
						var checked = (value == config.checkValue) ? 'checked="true"' : '';
						return "<input disabled class='webix_table_checkbox' type='checkbox' " + checked + ">";
					}
				}

			},
			{
				id: "pagerA",
				view: "pager",
				size: 50,
				group: 5,
				// animate: { direction:"top" },
				template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}"
			}
		]
	}

	var layout = {
		css: "frmView",
		rows: [
			{
				height: 39,
				css: { "background-color": "#f5f5f5 !important" },
				type: "toolbar",
				margin: 0,
				borderless: true,
				cols: controls
			},
			{
				rows: [
					grid
				]
			}



		]

	};

	function loadData() {
		var tabId = webix.$$("txtTabId").getValue();
		var url = app.config.host + "/WbField/GetFieldsByTabId?id=" + tabId;

		webix.ajax().get(url, function (text) {
			var json = JSON.parse(text);

			var table = webix.$$("dmfieldData");
			table.clearAll();

			for (var i = 0; i < json.length; i++) {
				var item = json[i];

				/* if (item.hidden!=null){
					item.hidden = item.toString();
				} */
			}

			table.parse(json);

		});
	}



	return {
		$ui: layout,
		$oninit: function (view, $scope) {
			$scope.on(app, "dataChanged", function (mode, item) {

				var table = webix.$$("dmfieldData");

				if (mode == 1) {
					table.add(item);
					//table.select(item.iddmfield,false);
				}

				if (mode == 2) {
					var selectedItem = table.getSelectedItem();
					table.updateItem(selectedItem.id, item);
				}

			});
		}
	};
});