define(["app", "locale", "models/user", "views/forms/68_mauthietkeform"], function (app, _, user, mauthietkeform) {
    "use strict";

    webix.$$("title").parse({
        title: "Quản lý mẫu thiết kế hóa đơn",
        details: "Quản lý mẫu thiết kế hóa đơn",
    });

    var subBangKe = webix.ui({
        view: "submenu",
        width: 150,
        padding: 0,
        data: [
            {
                id: "addMauBangKe",
                value: "Thêm mẫu bảng kê",
            },
            {
                id: "taiMauBangKe",
                value: "Tải mẫu bảng kê",
            },
            {
                id: "inMauBangKe",
                value: "In mẫu bảng kê",
            },
        ],
        on: {
            onItemClick: function (id, e, node) {
                var record = this.getItem(id);
                var table = webix.$$("mauthietke");
                var selectedItem = table.getSelectedItem();

                if (record.id == "addMauBangKe") {
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn cần thêm mẫu bảng kê ! ", "error");
                        return;
                    }
                    require(["views/forms/uploadfile"], function (uploadForm) {
                        uploadForm.$ui.tableId = table.config.id;
                        var win = table.$scope.ui(uploadForm.$ui);

                        win.getHead().setHTML("Tải file mẫu bảng kê");
                        win.show();

                        webix.$$("uploader").define("upload", app.config.host + "/Invoice68/UploadBangKeTemplate");
                        webix.$$("uploader").define("formData", { dmmauhoadon_id: selectedItem.id });
                        webix.$$("uploader").refresh();
                    });
                }

                if (record.id == "taiMauBangKe") {
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn cần tải bảng kê! ", "error");
                        return;
                    }
                    if (selectedItem.checkmaubangke == 0) {
                        webix.message("Chưa có mẫu bảng kê để tải ! ", "error");
                        return;
                    }
                    var url = app.config.host + "/Invoice68/DowloadRepxBangKe?id=" + selectedItem.id;
                    var box = webix.modalbox({
                        title: "Đang tải mẫu bảng kê (repx) ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });
                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            var file = new Blob([data], { type: "application/octet-stream" });
                            webix.html.download(file, "BangKeTemplate.repx");
                            webix.modalbox.hide(box);
                        });
                }

                if (record.id == "inMauBangKe") {
                    if (selectedItem == null || selectedItem == undefined) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn cần in bảng kê!", "error");
                        return;
                    }

                    if (selectedItem.checkmaubangke == 0) {
                        webix.message("Mẫu bảng kê của hoá đơn chưa có để in ! ", "error");
                        return;
                    }

                    var box = webix.modalbox({
                        title: "Đang in mẫu bảng kê của hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inMauHoadon_QuanLyMau?id=" + selectedItem.id + "&checkBangKe=true", {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                }
            },
        },
    });

    var initUI = function () {
        //var windowno = app.path[1].params[0];
        var table = webix.$$("mauthietke");
        // table.disable();
        // table.showProgress();
        // table.clearAll();
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var url = app.config.host + "/Invoice68/GetDataQlymau";
                var donVi = user.getDvcsID();
                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }
                var tlbParam = [
                    {
                        columnName: "code",
                        value: donVi,
                    },
                ];
                // tlbParam = [];
                url = url + "&tlbParam=" + JSON.stringify(tlbParam);
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("mauthietke");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(mauthietkeform.$ui);
                    mauthietkeform.setEditMode(1);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var table = webix.$$("mauthietke");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn cần sửa ! ", "error");
                        return;
                    }

                    // if (selectedItem.tt_sd > 0) {
                    //     webix.message("Mẫu thiết kế đã sử dụng không thể sửa ! ", "error");
                    //     return;
                    // }
                    var detailForm = this.$scope.ui(mauthietkeform.$ui);

                    mauthietkeform.setEditMode(2);
                    mauthietkeform.setValues(selectedItem);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("mauthietke");
                    var selectedItem = table.getSelectedItem();

                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu thiết kế ! ", "error");
                        return;
                    }
                    if (selectedItem.tt_sd > 0) {
                        webix.message("Mẫu thiết kế đã sử dụng không thể xóa ! ", "error");
                        return;
                    }
                    selectedItem.editmode = 3;
                    webix.confirm({
                        text: "Bạn có muốn xóa bản ghi",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/deleteMau68", selectedItem, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.error, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            var json = JSON.parse(text);
                                            if (json.error !== undefined) webix.message(json.error, "error");
                                            table.clearAll();
                                            table.load(table.config.url);
                                            table.enable();
                                            table.hideProgress();
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-download fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Tải") + "</span>",
            id: "btnDownload",
            // view: "button",
            // id: "btnDownload",
            // type: "icon",
            // icon: "download",
            // label: "Tải (F8)",
            // width: 100,
            on: {
                onClick: function () {
                    var table = webix.$$("mauthietke");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn cần tải ! ", "error");
                        return;
                    }
                    var url = app.config.host + "/Invoice68/DowloadRepx?id=" + selectedItem.id;
                    var box = webix.modalbox({
                        title: "Đang tải mẫu hóa đơn (repx) ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });
                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            var file = new Blob([data], { type: "application/octet-stream" });
                            webix.html.download(file, "InvoiceTemplate.repx");
                            // var fileURL = URL.createObjectURL(file);
                            //console.log(fileURL);
                            webix.modalbox.hide(box);
                            //window.open(fileURL, '_blank');
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("PRINT") + "</span>",
            id: "btnIn",
            on: {
                onClick: function () {
                    var table = webix.$$("mauthietke");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn cần in !", "error");
                        return;
                    }
                    var box = webix.modalbox({
                        title: "Đang in mẫu hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inMauHoadon_QuanLyMau?id=" + item.id, {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Bảng kê") + "</span>",
            id: "btnBangKeMauIn",
            click: function () {
                subBangKe.show(webix.$$("btnBangKeMauIn").$view);
            },
        },
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "mauthietke",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function (obj) {
                        // if (obj.date_new) {
                        //     obj.date_new = new Date(obj.date_new);
                        // }
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onBeforeLoad: function () {
                        this.showOverlay("<p><i style='font-size:450%;' class='fa fa-spin fa-spinner fa-pulse'></i></p>");
                    },
                    onAfterLoad: function (id) {
                        this.hideOverlay();
                    },
                },
                columns: [
                    {
                        id: "qlkhsdung_id",
                        header: "qlkhsdung_id",
                        hidden: true,
                    },
                    {
                        id: "stt",
                        header: "STT",
                        sort: "int",
                        width: 40,
                        columnType: "numeric",
                    },
                    {
                        id: "ntao",
                        header: [
                            "Ngày tạo",
                            {
                                content: "serverDateRangeFilter",
                            },
                        ],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        columnType: "datetime",
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "mmau",
                        header: [
                            "Mã mẫu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "tmau",
                        header: [
                            "Tên mẫu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },

                    {
                        id: "ngtao",
                        header: [
                            "Người tạo",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "tt_sdung",
                        header: ["Sử dụng"],
                        css: {
                            "text-align": "center",
                        },
                        editor: "checkbox",
                        width: 90,
                        template: function (obj) {
                            var checked = obj.tt_sd > 0 ? 'checked="true"' : "";
                            return "<input disabled class='webix_table_checkbox' type='checkbox' " + checked + ">";
                        },
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };

    function loadData() {
        var table = webix.$$("mauthietke");
        table.clearAll();
        table.load(table.config.url);
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var table = webix.$$("mauthietke");
            webix.extend(view, webix.ProgressBar);

            $scope.on(app, "dataChanged", function (mode, item) {
                table.clearAll();
                table.load(table.config.url);
            });

            $scope.on(app, "filterTable", function (report_id, infoWindowId, item) {
                var infoparam = {
                    infowindow_id: infoWindowId,
                    parameter: item,
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
