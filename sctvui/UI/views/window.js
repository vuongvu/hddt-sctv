define(["app", "views/forms/window", "views/forms/infowindow", "locale", "models/user", "views/forms/printf", "views/forms/sendemail", "views/forms/lichsuemail", "views/commonHD"], function (
    app,
    windowform,
    infowindow,
    _,
    user,
    printf,
    sendemail,
    his_email,
    commonHD
) {
    "use strict";

    var layoutId = "";
    var col = [];
    var sub_col = [];
    var windowconfig = null;
    var buttons = [];
    //var windowno = "":

    var subChucNang = webix.ui({
        view: "submenu",
        width: 300,
        padding: 0,
        data: [],
        on: {
            onItemClick: function (id, e, node) {
                var item = this.getItem(id);

                this.hide();

                var fn = Function("_", "user", item.on_click);
                fn(_, user);
            },
        },
    });

    var initUI = function () {
        var windowno = app.path[1].params[0];

        col = [];
        sub_col = [];
        var query = {
            command: "SYS002",
            parameter: {
                windowno: windowno,
            },
        };
        //var query = "SELECT * FROM #SCHEMA_NAME#.wb_menu WHERE window_code='" + windowno + "' LIMIT 1 OFFSET 0";
        // var url = app.config.host + "/System/Query";
        var url = app.config.host + "/System/ExecuteCommand";

        webix
            .ajax()
            .headers({ "Content-Type": "application/json" })
            .post(url, query, function (text) {
                var json = JSON.parse(text);

                if (json.length > 0) {
                    if (webix.$$("title") != undefined) {
                        webix.$$("title").parse({ title: json[0]["name"], details: json[0]["details"] });
                    }
                }
            });

        url = app.config.host + "/System/ExecuteCommand";

        $$("tlbMain").removeView("btnChucNang");

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(
                url,
                {
                    command: "CM00004",
                    parameter: {
                        window_id: windowno,
                    },
                },
                function (text) {
                    var json = JSON.parse(text);

                    if (json.length > 0) {
                        $$("cbMauIn").define("options", json);
                        $$("cbMauIn").setValue(json[0].id);
                        $$("cbMauIn").show();
                        $$("cbTypeIn").show();
                        $$("btnPrint").show();
                    } else {
                        $$("cbMauIn").define("options", []);
                        $$("cbMauIn").hide();
                        $$("cbTypeIn").hide();
                        $$("btnPrint").hide();
                    }

                    $$("cbMauIn").refresh();
                }
            );

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(
                url,
                {
                    command: "CM00002",
                    parameter: {
                        window_id: windowno,
                    },
                },
                function (text) {
                    var json = JSON.parse(text);

                    if (buttons.length > 0) {
                        for (var i = 0; i < buttons.length; i++) {
                            var item = buttons[i];
                            $$("tlbMain").removeView(item.id);
                        }
                    }

                    buttons = [];
                    var menus = [];

                    var stt = 7;

                    for (var i = 0; i < json.length; i++) {
                        var item = json[i];

                        if (item.is_group_function == "C") {
                            if (item.hidden != 1) {
                                menus.push({
                                    id: item.name,
                                    value: _(item.caption),
                                    on_click: item.on_click,
                                });
                            }
                        } else {
                            if (item.hidden != 1) {
                                var fn = new Function(item.on_click);

                                var button = {
                                    view: "button",
                                    type: "htmlbutton",
                                    width: item.width,
                                    label: '<i class="fa fa-' + item.icon + ' fa-lg aria-hidden="true"></i> ' + '<span class="text">' + _(item.caption) + "</span>",
                                    id: item.name,
                                    // label: _(item.caption),
                                    // view: "button",
                                    // type: "icon",
                                    // css: "webix_secondary",
                                    // icon: "fas fa-" + item.icon,
                                    // width: item.width,
                                    click: fn,
                                };

                                buttons.push(button);
                                $$("tlbMain").addView(button, stt);

                                stt = stt + 1;
                            }
                        }
                    }

                    if (menus.length > 0) {
                        subChucNang.clearAll();
                        subChucNang.parse(menus, "json");

                        var index = $$("tlbMain").index($$("cbMauIn"));

                        var btnChucNang = {
                            view: "button",
                            type: "htmlbutton",
                            width: 120,
                            label: '<i class="fa fa-tasks fa-lg aria-hidden="true"></i> ' + '<span class="text">' + _("CHUC_NANG") + "</span>",
                            id: "btnChucNang",
                            // label: _("CHUC_NANG"),
                            // view: "button",
                            // type: "icon",
                            // css: "webix_secondary",
                            // icon: "fas fa-tasks",
                            // width: 100,
                            click: function () {
                                subChucNang.show(webix.$$("btnChucNang").$view);
                            },
                        };

                        $$("tlbMain").addView(btnChucNang, index - 1);
                    }
                }
            );

        // lấy thông tin của window
        url = app.config.host + "/System/GetConfigWinByNo?id=" + windowno;

        webix.$$("windowData").getParentView().getParentView().getParentView().disable();
        webix.$$("windowData").getParentView().getParentView().getParentView().showProgress();

        var app_cache = webix.storage.local.get("APP_CACHE");
        // console.log(app_cache);
        //nếu có trong cache
        if (app_cache[windowno] != null) {
            windowconfig = app_cache[windowno];
            initComponents(windowconfig);
        } //không có cache
        else {
            webix.ajax(url, function (text, xml, xhr) {
                var rp = JSON.parse(text);
                windowconfig = rp;

                var app_cache = webix.storage.local.get("APP_CACHE");
                app_cache[windowno] = windowconfig;

                webix.storage.local.put("APP_CACHE", app_cache);
                initComponents(windowconfig);
            });
        }
    };

    var initComponents = function (_windowconfig) {
        //console.log(_windowconfig);
        var k = Object.keys(_windowconfig["data"][0]["Tabs"]).length;
        //console.log(k);
        var vc_window = _windowconfig["data"][0];

        var tabs = vc_window.Tabs;

        var windowno = vc_window.window_id;

        if (vc_window.window_type == "MasterDetail") {
            col.push({
                id: "subRow",
                header: "&nbsp;",
                template: "{common.subrow()}",
                css: "nampvsub",
                width: 40,
            });
        }

        var masterTab = tabs[0];
        //console.log(masterTab);

        if (masterTab.insert_cmd == "" || masterTab.insert_cmd == null) {
            $$("btnPlus").hide();
            $$("btnCopy").hide();
        } else {
            $$("btnPlus").show();
            $$("btnCopy").show();
        }

        if (masterTab.update_cmd == "" || masterTab.update_cmd == null) {
            $$("btnEdit").hide();
        } else {
            $$("btnEdit").show();
        }

        if (masterTab.delete_cmd == "" || masterTab.delete_cmd == null) {
            $$("btnDelete").hide();
        } else {
            $$("btnDelete").show();
        }

        for (var j = 0; j < masterTab.Fields.length; j++) {
            var field = masterTab.Fields[j];

            if (field.hide_in_grid == true) {
                continue;
            }

            var item = {
                id: field.column_name,
                header: _(field.column_name),
                columnType: field.column_type,
                columnAlias: field.column_alias,
                template: field.template,
                columnFormat: field.format,
            };

            if (field.caption != null) {
                item.header = _(field.caption);
            }

            if (field.column_type == "numeric") {
                item.css = { "text-align": "right" };
                item.format = webix.Number.numToStr({
                    groupDelimiter: ",",
                    groupSize: 3,
                    decimalDelimiter: ".",
                    decimalSize: field.data_decimal,
                });
            }

            if (field.type_filter != null) {
                if (field.type_filter != "") {
                    item.header = [_(item.header), { content: field.type_filter }];
                }
            }

            if (field.type_editor == "combo") {
                item.editor = field.type_editor;
                item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
            }

            if (field.column_type == "date" || field.column_type == "datetime") {
                item.format = function (text) {
                    var format = webix.Date.dateToStr(this.columnFormat);
                    text = removeTimeZoneT(text);
                    return format(text);
                };
            }

            if (field.type_editor == "checkbox") {
                if (field.column_type == "boolean") {
                    item.checkValue = true;
                    item.uncheckValue = false;
                }

                item.template = function (obj, common, value) {
                    if (value == true || value == 1) return "<div class='webix_icon fa-check-square-o'></div>";
                    else return "<div class='webix_icon fa-square-o'></div>";
                };
            }

            if (field.column_width == null) {
                item.fillspace = true;
            } else {
                item.width = field.column_width;
            }

            col.push(item);
        }

        var table = $$("windowData");
        table.clearAll();

        if (vc_window.window_type == "MasterDetail") {
            var cells = [];

            for (var i = 0; i < tabs.length; i++) {
                var tab = tabs[i];

                if (tab.tab_type == "Master") {
                    continue;
                }

                var subCols = [];

                for (var j = 0; j < tab.Fields.length; j++) {
                    var field = tab.Fields[j];

                    if (field.hide_in_grid == true) {
                        continue;
                    }

                    var subCol = {
                        id: field.column_name,
                        header: field.caption == null ? _(field.column_name) : _(field.caption),
                        hidden: field.hide_in_grid,
                    };

                    if (field.column_type == "date" || field.column_type == "datetime") {
                        subCol.format = function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        };
                    }

                    if (field.column_type == "numeric") {
                        subCol.css = { "text-align": "right" };
                        subCol.format = webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: field.data_decimal,
                        });
                    }

                    if (field.column_width == null) {
                        subCol.fillspace = true;
                    } else {
                        subCol.width = field.column_width;
                    }

                    subCols.push(subCol);
                }

                if (tab.tab_type == "Detail") {
                    var item = {
                        header: tab.name,
                        select: "cell",
                        body: {
                            view: "datatable",
                            tab_table: tab.table_name,
                            columns: subCols,
                            on: {
                                onBeforeLoad: function () {
                                    this.showOverlay(_("LOADING") + "...");
                                },
                                onAfterLoad: function () {
                                    this.hideOverlay();
                                },
                            },
                        },
                    };
                    cells.push(item);
                }
            }

            var subview = {
                borderless: true,
                view: "tabview",
                height: 200,
                scrollX: true,
                cells: cells,
                multiview: {
                    fitBiggest: true,
                },
            };

            table.define("subview", subview);
        }

        table.define("windowno", windowno);
        table.define("leftSplit", vc_window.fix_left == null ? 0 : vc_window.fix_left);
        table.define("rightSplit", vc_window.fix_right == null ? 0 : vc_window.fix_right);

        table.config.infoparam = null;
        table.config.columns = col;
        table.config.ma_ct = vc_window.ma_ct;

        table.refreshColumns();

        var parentToolBar = $$("tlbMain").getParentView();

        parentToolBar.removeView("tlbParams");
        $$("head-title").show();
        $$("head-title").removeView("tlbParams");
        // toolBar windown parram
        if (vc_window.WinParams != undefined && vc_window.WinParams != null) {
            var toolBarParams = {
                id: "tlbParams",
                view: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                borderless: true,
                height: 35,
                cols: [],
            };

            for (var i = 0; i < vc_window.WinParams.length; i++) {
                var item = vc_window.WinParams[i];

                var comp = {
                    id: webix.uid(),
                    view: item.type_editor,
                    width: item.width,
                    label: _(item.caption),
                    value_field: item.value_field,
                    isDefault: item.has_default,
                    on: {},
                };

                if (comp.view == "combo") {
                    comp.options = {
                        body: {
                            comboId: comp.id,
                            isDefault: item.has_default,
                            url: app.config.host + "/System/GetDataByReferencesId?id=" + item.wb_references_id,
                            ready: function () {
                                if (this.config.isDefault == "C") {
                                    var count = this.count();
                                    if (count > 0) {
                                        var combo = webix.$$(this.config.comboId);
                                        combo.setValue(this.getFirstId());
                                    }
                                }
                            },
                        },
                    };

                    comp.on.onChange = function (newv, oldv) {
                        var table = $$("windowData");
                        table.clearAll();
                        table.load(table.config.url);
                    };
                }

                if (comp.view == "treesuggest") {
                    var listColumns = JSON.parse(item.list_column);

                    for (var v = 0; v < listColumns.length; v++) {
                        listColumns[v].header = _(listColumns[v].id);
                    }

                    comp.view = "combo";

                    comp.options = {
                        view: "treesuggest",
                        textValue: item.display_field,
                        isDefault: item.has_default,
                        url: {
                            $proxy: true,
                            load: function (view, callback, params) {
                                var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

                                webix.ajax(url, function (text) {
                                    debugger;
                                    var json = JSON.parse(text);
                                    var data = listToTree(json);
                                    view.parse(data, "json");
                                });
                            },
                        },
                        body: {
                            isDefault: item.has_default,
                            comboId: comp.id,
                            columns: listColumns,
                            refid: item.wb_references_id,
                            filterMode: {
                                level: false,
                                showSubItems: false,
                            },
                            ready: function () {
                                if (this.config.isDefault == "C") {
                                    var count = this.count();

                                    if (count > 0) {
                                        var combo = webix.$$(this.config.comboId);
                                        combo.setValue(this.getFirstId());
                                    }
                                }
                            },
                        },
                    };

                    comp.on.onChange = function (newv, oldv) {
                        var table = $$("windowData");
                        table.clearAll();
                        table.load(table.config.url);
                    };
                }

                toolBarParams.cols.push(comp);
            }
            $$("head-title").addView(toolBarParams, 1);
            // parentToolBar.addView(toolBarParams);
        }

        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var windowno = app.path[1].params[0];
                var url = app.config.host + "/System/GetDataByWindowNo";
                var start = null;
                var count = null;
                var filter = [];
                var infoparam = null;
                var tlbparam_s = [];

                if (params == null) {
                    url = url + "&start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                    start = 0;
                    count = view.config.datafetch;
                } else {
                    url = url + "&start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;
                    start = params.start;
                    count = params.count;
                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            if (column !== undefined) {
                                var item = {
                                    columnName: property,
                                    columnType: column.columnType,
                                    columnAlias: column.columnAlias,
                                    value: params.filter[property],
                                };
                                array.push(item);
                            }
                        }

                        if (array.length > 0) {
                            filter = array;
                            url = url + "&filter=" + JSON.stringify(array);
                        } else {
                            url = url + "&filter=null";
                        }
                    } else {
                        url = url + "&filter=null";
                    }
                }

                if (view.config.infoparam != null) {
                    infoparam = view.config.infoparam;
                    url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
                } else {
                    url = url + "&infoparam=null";
                }
                var tlbParams = $$("tlbParams");

                var checkValue = true;
                if (tlbParams !== undefined) {
                    var childViews = tlbParams.getChildViews();

                    var parameters = [];

                    for (var i = 0; i < childViews.length; i++) {
                        var child = childViews[i];

                        var id = child.config.id;
                        var value_field = child.config.value_field;

                        if ($$(id).getValue() == "") {
                            checkValue = false;
                            break;
                        }

                        var item = {
                            columnName: value_field,
                            value: $$(id).getValue(),
                        };

                        parameters.push(item);
                    }
                    tlbparam_s = parameters;
                    url = url + "&tlbparam=" + JSON.stringify(parameters);
                } else {
                    url = url + "&tlbparam=null";
                }
                //if (checkValue == false) {
                //    $('.webix_overlay').hide();
                //    webix.message("Không tồn tại ký hiệu!", "error");
                //    return false;
                //}
                var item = {
                    window_id: windowno,
                    start: start,
                    count: count,
                    filter: filter,
                    infoparam: infoparam,
                    tlbparam: tlbparam_s,
                };
                var url1 = null;
                if (checkValue == true) {
                    url1 = app.config.host + "/System/GetDataByWindowNo";
                }
                webix.ajax().headers({ "Content-type": "application/json" }).bind(view).post(url1, item, callback, view);
                //}
                //else {
                //    $('.webix_overlay').hide()
                //}
                // webix.ajax().headers({ 'Content-type': 'application/json' })
                //     .bind(view)
                //     .post(url, item, function (text) {
                //         var json = JSON.parse(text);

                //         this.clearAll();

                //         if (!json.hasOwnProperty("error")) {
                //             this.bind(json);
                //             // this.refresh();
                //             // table.parse(json.data, "json");
                //         }
                //     });

                //    webix.ajax(url, callback, view);
            },
        };

        table.define("url", url);

        if (vc_window.wb_infowindow_id != null) {
            if (vc_window.wb_infowindow_id != "") {
                $$("btnSearch").show();
                table.config.infowindow_id = vc_window.wb_infowindow_id;
            } else {
                $$("btnSearch").hide();
                table.config.infowindow_id = null;
            }
        } else {
            $$("btnSearch").hide();
            table.config.infowindow_id = null;
        }

        webix.$$("windowData").getParentView().getParentView().getParentView().enable();
        webix.$$("windowData").getParentView().getParentView().getParentView().hideProgress();
    };

    webix.ui({
        view: "contextmenu",
        id: "cmenu",
        autowidth: true,
        //css: { "background": "#ccc !important" },
        data: [
            "Thêm",
            { $template: "Separator" },
            "Sửa",
            { $template: "Separator" },
            "Xóa",
            { $template: "Separator" },
            "Xem hóa đơn gốc",
            { $template: "Separator" },
            "Gửi hóa đơn qua email",
            { $template: "Separator" },
            "In hóa đơn",
            { $template: "Separator" },
            "Lịch sử gửi Email",
        ],
        on: {
            onItemClick: function (id) {
                if (this.getItem(id).value == "Thêm") {
                    $$("btnPlus").callEvent("onClick");
                }
                if (this.getItem(id).value == "Sửa") {
                    $$("btnEdit").callEvent("onClick");
                }
                if (this.getItem(id).value == "Xóa") {
                    $$("btnDelete").callEvent("onClick");
                }
                if (this.getItem(id).value == "Xem hóa đơn gốc") {
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();
                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }
                    //console.log(item);
                    if (item.original_invoice_id != null && item.original_invoice_id != undefined) {
                        var box = webix.modalbox({
                            title: "Đang in chứng từ",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        //var newWin = window.open();

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice/PrintInvoice?id=" + item.original_invoice_id + "&type=PDF", {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    alert(text);
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);

                                    webix.modalbox.hide(box);
                                    //newWin.location = fileURL;
                                    window.open(fileURL, "_blank");
                                },
                            });

                        // webix.ajax()
                        // .response("arraybuffer")
                        // .get(app.config.host + "/Invoice/PrintInvoice?id=" + item.inv_originalId + "&type=PDF", function (text, data) {
                        //     var file = new Blob([data], { type: 'application/pdf' });
                        //     var fileURL = URL.createObjectURL(file);
                        //     window.open(fileURL, '_blank');
                        //     // webix.html.download(file, $$('tenfile').getValue());
                        // });
                    } else {
                        webix.message("Không tồn tại hóa đơn gốc !", "error");
                        return;
                    }
                }
                if (this.getItem(id).value == "Lịch sử gửi Email") {
                    var table = webix.$$("windowData");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }
                    var detailForm = webix.$$("windowData").$scope.ui(his_email.$ui);
                    his_email.initUI(selectedItem.inv_invoiceauth_id);
                    detailForm.show();
                }
                if (this.getItem(id).value == "Gửi hóa đơn qua email") {
                    var table = webix.$$("windowData");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }

                    if (selectedItem.status == "Chờ ký" || selectedItem.status == "Chờ duyệt") {
                        webix.message("Hóa đơn chưa được ký", "error");
                        return;
                    }

                    var url = "/Invoice/GetEmailTemplate?type=Gửi hóa đơn&id=" + selectedItem.inv_invoiceauth_id;

                    webix.ajax(app.config.host + url, function (text) {
                        var json = JSON.parse(text);

                        var win = webix.$$("windowData").$scope.ui(sendemail.$ui);

                        var item = {
                            nguoi_gui: json.sender,
                            tieude: json.subject,
                            noidung: json.body,
                            mat_khau: json.pass,
                            inv_invoiceauth_id: selectedItem.inv_invoiceauth_id,
                            type: "Gửi hóa đơn",
                            nguoinhan: selectedItem.buyer_email,
                            his_email: json.his_email,
                        };
                        //console.log(sendemail);

                        sendemail.setValues(item);
                        win.show();
                    });
                }
                if (this.getItem(id).value == "In hóa đơn") {
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dữ liệu cần in", "error");
                        return;
                    }

                    var cbMauIn = $$("cbMauIn");
                    var cbTypeIn = $$("cbTypeIn");

                    var rpParameter = {
                        parameter: item,
                        wb_mauin_id: cbMauIn.getValue(),
                        type: cbTypeIn.getValue(),
                    };

                    var mimetype = cbTypeIn.getValue() == "PDF" ? "application/pdf" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    var url = app.config.host + "/System/InChungTu";

                    var box = webix.modalbox({
                        title: "Đang in chứng từ",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .post(url, rpParameter, {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                alert(text);
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: mimetype });
                                var fileURL = URL.createObjectURL(file);

                                webix.modalbox.hide(box);

                                // var win = $$("windowData").$scope.ui(printf.$ui);
                                // win.config.url = fileURL;
                                // win.show();

                                newWin.location = fileURL;
                            },
                        });
                }
            },
        },
    });
    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            // view: "button",
            // type: "icon",
            // icon: "far fa-refresh",
            // label: _("REFRESH"),
            // width: 95,
            // css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("windowData");
                    table.clearAll();

                    table.load(table.config.url);
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHDTT32(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            // view: "button",
            // type: "icon",
            // icon: "far fa-plus",
            // label: "Thêm (F4)",
            // css: "webix_secondary",
            // width: 90,
            shortcut: "F4",
            css: "btnThem",
            on: {
                onClick: function () {
                    var vc_window = windowconfig["data"][0];
                    windowform.$ui.id = vc_window.code;

                    var detailForm = this.$scope.ui(windowform.$ui);

                    windowform.setEditMode(0);
                    windowform.initUI(windowconfig);

                    var params = null;

                    var tlbParams = $$("tlbParams");

                    if (tlbParams !== undefined) {
                        var childViews = tlbParams.getChildViews();

                        var item = [];

                        for (var i = 0; i < childViews.length; i++) {
                            var child = childViews[i];

                            var id = child.config.id;
                            var value_field = child.config.value_field;

                            item[value_field] = $$(id).getValue();
                        }

                        params = item;
                    }

                    windowform.setValues(null, windowconfig, params);
                    windowform.setEditMode(1);

                    detailForm.show();
                    windowform.focusFirstElement();

                    if (vc_window.after_new != null) {
                        var fn = Function("app", vc_window.after_new);
                        fn(app);
                    }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHDTT32(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            // view: "button",
            // type: "icon",
            // icon: "far fa-edit",
            // label: "Sửa (F3)",
            // width: 90,
            shortcut: "F3",
            // css: "webix_secondary",
            //css:"button_raised",
            on: {
                onClick: function () {
                    var vc_window = windowconfig["data"][0];
                    // console.log(vc_window);
                    windowform.$ui.id = vc_window.code;

                    var detailForm = this.$scope.ui(windowform.$ui);

                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần sửa !", "error");
                        return;
                    }
                    // không cho sửa hóa đơn điều chỉnh, thay thế đang fix cứng

                    if (vc_window.code == "WIN00187" || vc_window.code == "WIN00189" || vc_window.code == "WIN00192" || vc_window.code == "WIN00193") {
                        if (item.trang_thai_hd == 7 || item.trang_thai_hd == 13 || item.trang_thai_hd == 15 || item.trang_thai_hd == 17) {
                            webix.message("Không thể sửa hóa đơn xóa bỏ. Bạn cần phải sử dụng chức năng tương ứng", "error");
                            return;
                        }
                        if (item.trang_thai == "Đã ký") {
                            webix.message("Hóa đơn đã ký, bạn không thể sửa !", "error");
                            return;
                        }
                    }

                    if (item == null) {
                        webix.message({ type: "error", text: "Bạn chưa chọn dòng cần sửa" });
                        return;
                    }

                    windowform.setEditMode(0);
                    windowform.initUI(windowconfig);
                    windowform.setValues(item, windowconfig);
                    windowform.setEditMode(2);

                    detailForm.show();
                    windowform.focusFirstElement();

                    if (vc_window.after_edit != null) {
                        var fn = Function("app", "_", vc_window.after_edit);
                        fn(app, _);
                    }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHDTT32(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-trash",
            // label: "Xóa (F8)",
            // width: 80,
            shortcut: "F8",
            // css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("windowData");

                    if (table.getColumnConfig("colMultiChoice") == undefined) {
                        var item = table.getSelectedItem();
                        if (item == null || item == undefined) {
                            webix.message("Bạn chưa chọn dòng cần xóa !", "error");
                            return;
                        }

                        var khData = {};
                        khData.windowid = windowconfig.data[0].code;
                        khData.editmode = 3;
                        khData.data = [];
                        khData.data.push(item);

                        webix.confirm({
                            text: "Bạn có muốn xóa ",
                            ok: "Có",
                            cancel: "Không",
                            callback: function (res) {
                                if (res) {
                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/System/Save", khData, function (text) {
                                            var response = JSON.parse(text);

                                            if (response.hasOwnProperty("error")) {
                                                webix.message({ type: "error", text: response.error });
                                                return;
                                            }

                                            table.remove(item.id);
                                            var btnRefresh = $$("btnRefresh");
                                            console.log(btnRefresh);
                                            btnRefresh.callEvent("onClick");
                                        });
                                }
                            },
                        });
                    } else {
                        webix.confirm({
                            text: "Bạn có muốn xóa ",
                            ok: "Có",
                            cancel: "Không",
                            callback: function (res) {
                                if (res) {
                                    deleteMultiChoice();
                                }
                            },
                        });
                    }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHDTT32(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-files-o fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("COPY") + "</span>",
            id: "btnCopy",
            // view: "button",
            // type: "icon",
            // icon: "far fa-files-o",
            // label: _("COPY"),
            // width: 85,
            // css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng dữ liệu");
                        return;
                    }

                    var vc_window = windowconfig["data"][0];
                    windowform.$ui.id = vc_window.code;

                    var detailForm = this.$scope.ui(windowform.$ui);

                    const cloneItem = JSON.parse(JSON.stringify(item));

                    windowform.setEditMode(0);
                    windowform.initUI(windowconfig);
                    // windowform.setEditMode(4);
                    windowform.copyValues(cloneItem, windowconfig);
                    windowform.setEditMode(1);
                    detailForm.show();
                    windowform.focusFirstElement();

                    if (vc_window.after_new != null) {
                        var fn = Function("app", vc_window.after_new);
                        fn(app);
                    }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHDTT32(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 110,
            label: '<i class="fa fa-search fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SEARCH") + " (F2)" + "</span>",
            id: "btnSearch",
            // view: "button",
            // type: "icon",
            // icon: "far fa-search",
            // label: _("SEARCH") + " (F2)",
            // width: 110,
            // css: "webix_secondary",
            shortcut: "F2",
            on: {
                onClick: function () {
                    var table = webix.$$("windowData");

                    infowindow.$ui.id = table.config.infowindow_id;
                    infowindow.$ui.load_event = "filterTable";

                    var wininfoparam = this.$scope.ui(infowindow.$ui);

                    if (table.config.infoparam != undefined) {
                        wininfoparam.config.editmode = 1;
                        wininfoparam.config.formvalues = table.config.infoparam.parameter;
                    } else {
                        wininfoparam.config.editmode = 0;
                        wininfoparam.config.formvalues = null;
                    }

                    infowindow.initUI(table.config.infowindow_id);
                    wininfoparam.show();
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHDTT32(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("PRINT") + " (F7)" + "</span>",
            id: "btnPrint",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-print",
            // label: _("PRINT") + " (F7)",
            // width: 70,
            shortcut: "F7",
            // css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dữ liệu cần in", "error");
                        return;
                    }

                    var cbMauIn = $$("cbMauIn");
                    var cbTypeIn = $$("cbTypeIn");

                    var rpParameter = {
                        parameter: item,
                        wb_mauin_id: cbMauIn.getValue(),
                        type: cbTypeIn.getValue(),
                    };

                    var mimetype = cbTypeIn.getValue() == "PDF" ? "application/pdf" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    if (cbTypeIn.getValue() == "Rtf") {
                        mimetype = "application/octet-stream";
                    }

                    var url = app.config.host + "/System/InChungTu";

                    var box = webix.modalbox({
                        title: "Đang in chứng từ",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window;

                    if (cbTypeIn.getValue() == "PDF") {
                        newWin = window.open();
                    }

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .post(url, rpParameter, {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                alert(text);
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: mimetype });
                                webix.modalbox.hide(box);

                                var cbTypeIn = $$("cbTypeIn");

                                if (cbTypeIn.getValue() == "Excel" || cbTypeIn.getValue() == "Rtf") {
                                    var suffix = cbTypeIn.getValue() == "Excel" ? ".xlsx" : ".rtf";

                                    var id = $$("cbMauIn").getValue();
                                    var mauin = $$("cbMauIn").getList().getItem(id);
                                    var export_name = mauin.export_name;

                                    var table = webix.$$("windowData");
                                    var item = table.getSelectedItem();

                                    for (var key in item) {
                                        export_name = export_name.replace(new RegExp("#" + key + "#", "g"), item[key]);
                                    }

                                    export_name = export_name.replace(new RegExp("/", "g"), "");

                                    webix.html.download(file, export_name + suffix);
                                    return;
                                }

                                var fileURL = URL.createObjectURL(file);

                                newWin.location = fileURL;
                            },
                        });
                },

                // onClick: function () {
                //     var table = webix.$$("windowData");
                //     var item = table.getSelectedItem();

                //     if (item == null) {
                //         webix.message("Bạn chưa chọn dữ liệu cần in", "error");
                //         return;
                //     }

                //     var cbMauIn = $$("cbMauIn");
                //     var cbTypeIn = $$("cbTypeIn");

                //     var rpParameter = {
                //         parameter: item,
                //         wb_mauin_id: cbMauIn.getValue(),
                //         type: cbTypeIn.getValue()
                //     };

                //     var mimetype = cbTypeIn.getValue() == "PDF" ? "application/pdf" : (cbTypeIn.getValue() == "Excel" ? "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" : "text/richtext");

                //     var url = app.config.host + "/System/InChungTu";

                //     var box = webix.modalbox({
                //         title: "Đang in chứng từ",
                //         text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                //         width: 350,
                //         height: 200
                //     });

                //     var newWin = window.open();

                //     webix.ajax()
                //         .response("arraybuffer")
                //         .headers({ 'Content-type': 'application/json' })
                //         .post(url, rpParameter, {
                //             error: function (text, data, XmlHttpRequest) {
                //                 webix.modalbox.hide(box);
                //                 alert(text);
                //             },
                //             success: function (text, data, XmlHttpRequest) {
                //                 var file = new Blob([data], { type: mimetype });

                //                 if (cbTypeIn.getValue() == "Rtf") {
                //                     webix.html.download(file, "invoice.rtf");
                //                     webix.modalbox.hide(box);
                //                     return;
                //                 }
                //                 var fileURL = URL.createObjectURL(file);

                //                 webix.modalbox.hide(box);

                //                 // var win = $$("windowData").$scope.ui(printf.$ui);
                //                 // win.config.url = fileURL;
                //                 // win.show();

                //                 newWin.location = fileURL;

                //             }
                //         });

                // }
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHDTT32(self);
            },
        },
        {},
        {
            view: "richselect",
            label: _("MAUIN"),
            width: 250,
            labelWidth: 50,
            id: "cbMauIn",
        },
        {
            view: "richselect",
            //label: _("MAUIN"),
            width: 80,
            labelWidth: 50,
            css: "cbTypePrint",
            id: "cbTypeIn",
            value: "PDF",
            options: [
                { id: "PDF", value: "PDF" },
                // { "id": "Excel", "value": "EXCEL" },
                { id: "Rtf", value: "Rtf" },
            ],
        },
    ];

    var grid = {
        rows: [
            {
                id: "windowData",
                view: "datatable",
                select: true,
                datafetch: 50,
                resizeColumn: true,
                columns: col,
                tooltip: true,
                leftSplit: 0,
                rightSplit: 0,
                headermenu: {
                    //width: 200,
                    autoheight: false,
                    scroll: true,
                    autowidth: true,
                },
                multiview: { keepViews: true },
                keepViews: true,
                //css:"webix_header_border webix_data_border",
                hover: "myhover",
                // tooltip: function (obj, common) {
                //     //obj - row object
                //     console.log(obj);
                //     console.log(common);
                //     //common.column - configuration of related column
                //     //return "<span class='webix_strong'>Rating: </span> " + obj[common.column.id] + "<br/><span class='webix_strong'>Votes: </span>" + obj[(common.column.id + 1)];
                //     return "<span>" + obj + "</span>";
                // },
                on: {
                    onSubViewCreate: function (view, item) {
                        var subView = view;

                        if (subView != null) {
                            var windowno = app.path[1].params[0];

                            var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id=" + windowno + "&id=" + item.id;

                            var detailTables = subView.getChildViews()[1].getChildViews();

                            for (var i = 0; i < detailTables.length; i++) {
                                var dt = detailTables[i];
                                dt.clearAll();
                                dt.load(url + "&tab_table=" + dt.config.tab_table);
                            }
                        }
                    },
                    onSubViewOpen: function (id) {
                        var table = this;
                        var subView = table.getSubView(id);

                        if (subView != null) {
                            var windowno = app.path[1].params[0];

                            var url = app.config.host + "/System/GetDataDetailsByTabTable?window_id=" + windowno + "&id=" + id.row;

                            var detailTables = subView.getChildViews()[1].getChildViews();

                            for (var i = 0; i < detailTables.length; i++) {
                                var dt = detailTables[i];
                                dt.clearAll();
                                dt.load(url + "&tab_table=" + dt.config.tab_table);
                            }
                        }
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onBeforeLoad: function () {
                        this.showOverlay("<p><i style='font-size:450%;' class='fa fa-spin fa-spinner fa-pulse'></i></p>");
                    },
                    onAfterLoad: function (id) {
                        this.hideOverlay();
                        var windowno = app.path[1].params[0];
                        if (windowno != "WIN00187" && windowno != "WIN00189" && windowno != "WIN00193" && windowno != "WIN00192" && windowno != "WIN00194") {
                            $$("cmenu").refresh();
                            $$("cmenu").sizeToContent();
                            $$("cmenu").hideItem("Xem hóa đơn gốc");
                            $$("cmenu").hideItem("Gửi hóa đơn qua email");
                            $$("cmenu").hideItem("In hóa đơn");
                            $$("cmenu").attachTo($$("windowData"));
                        } else {
                            $$("cmenu").refresh();
                            $$("cmenu").showItem("Xem hóa đơn gốc");
                            $$("cmenu").showItem("Gửi hóa đơn qua email");
                            $$("cmenu").showItem("In hóa đơn");
                            $$("cmenu").attachTo($$("windowData"));
                        }
                    },
                },
                subview: {
                    borderless: true,
                    view: "tabview",
                    height: 200,
                    scrollX: true,
                    tabbar: {
                        width: 200,
                    },
                    cells: [],
                },
                pager: "pagerA",
            },
            {
                cols: [
                    {
                        id: "pagerA",
                        view: "pager",
                        size: 50,
                        group: 5,
                        template: function (data, common) {
                            var size = 0;
                            var start = data.page * data.size;
                            var end = start + data.size;
                            if (end > data.count) size = data.count - start;
                            else size = data.size;
                            var end = start + data.size;
                            var html = " <span style='text-align:center; line-height:20px; font-size:10pt;'> Trang:" + (data.page + 1) + "/" + data.limit + " </span> ";
                            var htmlSoBanGhi = " <span style='line-height:20px; font-size:10pt;'> Số bản ghi:" + size + "/" + data.count + " </span> ";
                            return common.first() + common.prev() + html + common.next() + common.last() + htmlSoBanGhi;
                        },
                    },
                    {},
                    {
                        view: "button",
                        type: "icon",
                        icon: "fas fa-th-list",
                        width: 35,
                        on: {
                            onClick: function () {
                                changeMultiChoice();
                            },
                        },
                        click: function () {
                            this.callEvent("onClick");
                        },
                    },
                ],
            },
        ],
    };

    var layout = {
        rows: [
            {
                rows: [
                    {
                        id: "tlbMain",
                        height: 39,
                        type: "toolbar",
                        css: { "background-color": "#f5f5f5 !important" },
                        cols: controls,
                        borderless: true,
                    },
                ],
            },
            {
                rows: [grid],
            },
        ],
    };

    function changeMultiChoice() {
        var table = $$("windowData");
        var columns = webix.toArray(table.config.columns);

        if (table.getColumnConfig("colMultiChoice") == undefined) {
            columns.insertAt(
                {
                    id: "colMultiChoice",
                    header: [{ text: "", content: "masterCheckbox" }],
                    width: 50,
                    checkValue: "C",
                    uncheckValue: "K",
                    template: "{common.checkbox()}",
                },
                0
            );
        } else {
            columns.removeAt(0);
        }

        table.refreshColumns();
    }

    function deleteMultiChoice() {
        var table = $$("windowData");
        var array = [];

        table.eachRow(function (row) {
            var record = table.getItem(row);

            if (record != undefined && record != null) {
                if (record.hasOwnProperty("colMultiChoice")) {
                    if (record.colMultiChoice == "C") {
                        array.push(record.id);
                    }
                }
            }
        }, true);

        if (array.length == 0) {
            webix.message({ type: "error", text: "Không có bản ghi nào được chọn" });
            return;
        }

        var khData = {};
        khData.windowid = windowconfig.data[0].code;
        khData.editmode = 3;
        khData.data = array;

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(app.config.host + "/System/DeleteMultiChoice", khData, function (text) {
                var response = JSON.parse(text);

                if (response.hasOwnProperty("error")) {
                    webix.message({ type: "error", text: response.error });
                    return;
                }

                $$("btnRefresh").callEvent("onClick");
            });
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            webix.extend(view, webix.ProgressBar);

            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("windowData");

                if (mode == 1) {
                    // table.add(item);
                    table.clearAll();
                    table.load(table.config.url);
                }

                if (mode == 2) {
                    // var selectedItem = table.getSelectedItem();
                    // table.updateItem(selectedItem.id, item);
                    table.clearAll();
                    table.load(table.config.url);
                    webix.UIManager.setFocus($$(table.config.id));
                }
            });

            $scope.on(app, "filterTable", function (report_id, infoWindowId, item) {
                var table = $$("windowData");

                /* if (item.hasOwnProperty("p_MA_CT")){
                    item["p_MA_CT"] = table.config.ma_ct;
                } */

                var infoparam = {
                    infowindow_id: infoWindowId,
                    parameter: item,
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
