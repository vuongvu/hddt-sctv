/**
 * Created by Mads on 03-10-2015.
 */
define(["models/user", "locale", "app", "helpers/locale", "views/menus/version", "views/quenmk"], function (user, _, app, locale, version, quenmk) {
    "use strict";
    webix.ui({
        view: "popup",
        id: "my_pop",
        head: "Submenu",
        width: 250,
        body: {
            view: "list",
            data: [{ id: "1", name: "Cập nhật bản cũ", location: "Mobi-Invoice Desktop" }],
            template: "#name# - #location#",
            autoheight: true,
            select: true,
            on: {
                onItemDblClick: function (id) {
                    if (id == "1") {
                        app.router("/updateToWeb");
                    }
                },
            },
        },
    });
    var values = webix.copy(
        {
            username: "",
            password: "",
            rememberme: false,
            language: "vi",
            ma_dvcs: "",
        },
        Cookies.getJSON("Crm")
    );

    var demo = function () {
        webix.message("ấvasv");
    };

    return {
        $ui: {
            css: "bodylogin",
            rows: [
                {
                    //borderless: true,
                    // css: { "box-shadow": "0 15px 10px rgba(0,0,0,0.2)" },
                    height: 80,
                    cols: [
                        { gravity: 1, template: "", width: 150 },
                        {
                            view: "label",
                            template: "<a href='https://www.sctv.com.vn/' target='_blank'><img style='width: 250px; /* height:120px; */position: absolute;' src='assets/imgs/head-banner.png' /></a>",
                            height: 65,
                            borderless: true,
                            css: { padding: "10px", "padding-top": "1px !important" },
                        },
                        {
                            id: "version",
                            borderless: true,
                            view: "label",
                            template: "<div class='hotline'><b style='font-family:Arial;color=white'>Hotline:</b> <label style='color:white'>0896 15 16 17</label> <br /></div>",
                            css: { padding: "10px", "padding-top": "20px", color: "white" },
                        },
                        {
                            rows: [
                                { gravity: 1, template: "" },
                                {
                                    id: "cbLanguage",
                                    view: "combo",
                                    width: 125,
                                    options: [
                                        { id: "vi", value: "Việt Nam" },
                                        /*  { id: "en", value: "English" },
                                        { id: "ko", value: "Korea" } */
                                    ],
                                    // inputAlign: "right",
                                    //css: "cbLanguage",
                                    value: locale.getLang(),
                                    on: {
                                        onChange: function (newv, oldv) {
                                            locale.setLang(newv, null);
                                        },
                                    },
                                },
                                { gravity: 1, template: "" },
                            ],
                        },
                        { gravity: 1, template: "", width: 150 },
                    ],
                },
                {
                    cols: [
                        { gravity: 1, template: "" },
                        // { gravity: 1, template: "" },
                        // { gravity: 1, template: "" },
                        // { gravity: 1, template: "" },
                        // { gravity: 1, template: "" },
                        // { gravity: 1, template: "" },
                        {
                            //  css: { "padding-left": "450px" },
                            rows: [
                                { gravity: 0.05, template: "" },
                                {
                                    view: "form",
                                    gravity: 5,
                                    id: "loginForm",
                                    width: 900,
                                    height: 550,
                                    margin: 5,
                                    paddingX: 50,
                                    css: "login",
                                    borderless: false,
                                    elements: [
                                        {
                                            view: "label",
                                            label: "Đăng nhập",
                                            css: "lbldangnhap",
                                        },
                                        {
                                            view: "combo",
                                            id: "ma_dvcs",
                                            name: "ma_dvcs",
                                            label: _("DONVI"),
                                            width: 330,
                                            css: "donvilogin",
                                            //options: app.config.host +"/System/GetAllDmDvcs",
                                            options: {
                                                view: "gridsuggest",
                                                textValue: "name",
                                                body: {
                                                    //url: app.config.host + "/System/GetAllDmDvcs",
                                                    textValue: "name",
                                                    columns: [
                                                        {
                                                            id: "tax_code",
                                                            width: 120,
                                                            header: _("MS_THUE"),
                                                        },
                                                        {
                                                            id: "name",
                                                            width: 450,
                                                            header: _("DONVI"),
                                                        },
                                                    ],
                                                    ready: function () {
                                                        var values = Cookies.getJSON("Crm");

                                                        var obj = $$("ma_dvcs");
                                                        var list = obj.getList();

                                                        if (values === undefined) {
                                                            if (list.count()) {
                                                                var id = list.getIdByIndex(0);
                                                                obj.setValue(id);
                                                            }
                                                        } else {
                                                            var item = list.getItem(values.ma_dvcs);

                                                            if (item != null && item != undefined) {
                                                                obj.setValue(values.ma_dvcs);
                                                            } else {
                                                                if (list.count()) {
                                                                    var id = list.getIdByIndex(0);
                                                                    obj.setValue(id);
                                                                }
                                                            }
                                                        }
                                                    },
                                                },
                                            },
                                            value: values.ma_dvcs,
                                            required: true,
                                            hidden: true,
                                        },
                                        {
                                            view: "text",
                                            id: "username",
                                            name: "username",
                                            label: _("USERNAME"),
                                            icon: "user",
                                            width: 330,
                                            value: values.username,
                                            required: true,
                                            invalidMessage: "Tên truy cập không để trống hoặc nhập không đúng (a-z,A-Z,0-9)",
                                            validate: webix.rules.isSpecialCharacter,
                                            css: "mcls_uppercase",
                                        },
                                        {
                                            view: "text",
                                            id: "password",
                                            name: "password",
                                            width: 330,
                                            type: "password",
                                            label: _("PASSWORD"),
                                            value: values.password,
                                            invalidMessage: "Mật khẩu không được để trống",
                                            required: true,
                                            css: "passlogin",
                                        },
                                        {
                                            view: "checkbox",
                                            id: "remember",
                                            name: "rememberme",
                                            css: "rememberlogin",
                                            label: _("REMEMBERME") + " ?",
                                            labelPosition: "left",
                                            labelWidth: 130,
                                            checkValue: "true",
                                            uncheckValue: "false",
                                            value: values.rememberme,
                                        },
                                        {
                                            view: "button",
                                            //type: "form",
                                            id: "login",
                                            name: "login",
                                            label: _("DANGNHAP"),
                                            css: "btnlogin",
                                            width: 330,
                                            height: 43,
                                            hotkey: "enter",
                                            type: "form",
                                            click: function () {
                                                var username = $$("username").getValue();
                                                $$("username").setValue(username.toUpperCase());

                                                if ($$("loginForm").validate()) {
                                                    var values = $$("loginForm").getValues();
                                                    if ("true" === values.rememberme) {
                                                        values.language = $$("cbLanguage").getValue();
                                                        Cookies.set("Crm", values);
                                                    } else {
                                                        // Clear cookie
                                                        Cookies.remove("Crm");
                                                    }

                                                    var _ma_dvcs = values.ma_dvcs;

                                                    if ($$("ma_dvcs").config.hidden) {
                                                        _ma_dvcs = "";
                                                    }
                                                    user.login(values.username, values.password, _ma_dvcs, $$("cbLanguage").getValue(), $$("loginForm"));
                                                } else {
                                                    $$("username").focus();
                                                    $$("loginForm").focus();
                                                }
                                            },
                                        },
                                        {
                                            width: 330,
                                            css: "linelogin",
                                            cols: [
                                                {
                                                    view: "button",
                                                    label: _("QUENMK"),
                                                    css: "mcls_btnResetPassLogin",
                                                    type: "iconButton",
                                                    icon: "far fa-key",
                                                    width: 150,
                                                    click: function () {
                                                        var win = this.$scope.ui(quenmk.$ui);
                                                        win.show();
                                                        // app.router("/retrivepass");
                                                    },
                                                },

                                                {
                                                    view: "button",
                                                    css: "mcls_btnRegisterLogin",
                                                    type: "iconButton",
                                                    icon: "far fa-long-arrow-right",
                                                    label: _("DANGKY"),
                                                    width: 150,
                                                    click: function () {
                                                        window.open("https://www.sctv.com.vn/", "_blank");
                                                    },
                                                },
                                            ],
                                        },
                                    ],
                                    elementsConfig: {
                                        labelPosition: "top",
                                        //validateEvent: "key"
                                    },
                                },
                                { gravity: 0.1, temlate: "" },
                            ],
                        },
                        { gravity: 1, template: "" },
                    ],
                },
                {
                    height: 80,
                },
            ],
        },
        $oninit: function () {
            $$("loginForm").focus();
        },
    };
});
