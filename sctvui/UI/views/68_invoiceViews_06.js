define(["app", "locale", "models/user", "views/forms/68_invoiceForms_06", "views/forms/68_huyhoadon", "views/forms/68_huyhoadon_1", "views/forms/68_hoadon_history", "views/commonHD"], function (
    app,
    _,
    user,
    invoiceForms,
    huyhoadon,
    huyhoadon_1,
    hoadonHistory,
    commonHD
) {
    "use strict";
    var fm = function (number) {
        var k = webix.Number.numToStr({
            groupDelimiter: ".",
            groupSize: 3,
            decimalDelimiter: ",",
            decimalSize: 0,
        });
        return k(number);
    };
    var arrayString = [
        "tgtcthue",
        "tgtcthue10",
        "tgtcthue5",
        "tgtcthue0",
        "tgtcthuek",
        "tgtcthuekct",
        "tgtcthuekkk",
        "ttcktmai",
        "ttcktmai10",
        "ttcktmai5",
        "ttcktmai0",
        "ttcktmaik",
        "ttcktmaikct",
        "ttcktmaikkk",
        "tgtthue",
        "tgtthue10",
        "tgtthue5",
        "tgtthue0",
        "tgtthuek",
        "tgtthuekct",
        "tgtthuekkk",
        "tgtttbso",
        "tgtttbso10",
        "tgtttbso5",
        "tgtttbso0",
        "tgtttbsok",
        "tgtttbsokct",
        "tgtttbsokkk",
        "tkcktmn",
        "tgtphi",
        "tgtttbso_last",
    ];
    var op = [];
    var subChucNang = webix.ui({
        view: "submenu",
        width: 300,
        padding: 0,
        data: [
            // {
            //     id: "huyhoadon",
            //     value: "Hủy hóa đơn",
            // },
            // {
            //     id: "huyhoadon2",
            //     value: "Hủy hóa đơn có tạo mới",
            // },
            // {
            //     id: "giaitrinh",
            //     value: "Giải trình",
            // },
            {
                id: "notifyErrorInvoice",
                value: "Thông báo HĐĐT có sai sót",
            },
            {
                id: "replace_invoice",
                value: "Thay thế",
            },
            {
                id: "increase_change_invoice",
                value: "Điều chỉnh tăng",
            },
            {
                id: "reduce_change_invoice",
                value: "Điều chỉnh giảm",
            },
            {
                id: "info_change",
                value: "Điều chỉnh thông tin",
            },
            {
                id: "dowloadXml",
                value: "Tải XML",
            },
            {
                id: "dowloadXmlMessageHD",
                value: "Tải XML thông điệp gửi HĐ",
            },
            // {
            //     id: "nhanExcel",
            //     value: "Nhận Excel hàng loạt",
            // },
        ],
        on: {
            onItemClick: function (id, e, node) {
                var record = this.getItem(id);
                this.hide();
                if (record.id == "nhanExcel") {
                    var table = $$("invoiceViews");

                    require(["views/forms/uploadexcel"], function (uploadForm) {
                        uploadForm.$ui.tableId = table.config.id;
                        var win = table.$scope.ui(uploadForm.$ui);
                        win.show();
                        webix.$$("uploader").define("formData", { excel_id: "EX00011", filename: "Nhan hang loat hoa don 68 (GTGT).xlsx" });
                        webix.$$("uploader").hide();
                    });
                }
                // console.log(record);
                if (record.id == "huyhoadon") {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }
                    if (item.tthdon == 3) {
                        webix.message("Hóa đơn đã Hủy bạn không thể hủy thêm !", "error");
                        return;
                    }
                    if (item.tthai == "Chờ ký") {
                        webix.message("Hóa đơn đang ở trạng thái chờ ký, không thể hủy !", "error");
                        return;
                    }

                    var win = table.$scope.ui(huyhoadon.$ui);
                    huyhoadon.setEditMode(1);

                    webix.$$("btnHuyhoadon").destructor();
                    // console.log(item);
                    var put = {
                        khieu: item.khieu,
                        shdon: item.shdon,
                        ngay: item.nlap,
                        ladhddt: 1,
                        tctbao: 1,
                        hdon_id: item.id,
                    };
                    huyhoadon.setValues(put, null, null);
                    win.show();
                }
                if (record.id == "huyhoadon2") {
                    //    var btnEdit = $$("btnPlus");
                    //     btnEdit.callEvent("onClick");
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }
                    if (item.tthdon == 3) {
                        webix.message("Hóa đơn đã Hủy bạn không thể hủy thêm !", "error");
                        return;
                    }
                    if (item.tthai == "Chờ ký") {
                        webix.message("Hóa đơn đang ở trạng thái chờ ký, không thể hủy !", "error");
                        return;
                    }

                    var win = table.$scope.ui(huyhoadon.$ui);
                    huyhoadon.setEditMode(1);
                    webix.$$("btnStep299").show();
                    webix.$$("btnHuyhoadon").destructor();
                    // webix.$$("danhsach_hoadonhuy").define("editable", false);
                    webix.$$("btnGhiHuyhoadon").destructor();
                    webix.$$("btnGhiSignHuyHDDT").destructor();
                    // webix.$$("btnStep299").define("hidden", false);
                    // webix.$$("btnStep2").show();
                    // webix.$$("btnStep299").hide();
                    // $$("btnHuyHDDT").addView({ view:"button",label: "Bước 2" }, 1);
                    // console.log(item);
                    var put = {
                        khieu: item.khieu,
                        shdon: item.shdon,
                        ngay: item.nlap,
                        ladhddt: 1,
                        tctbao: 1,
                        hdon_id: item.id,
                    };
                    huyhoadon.setValues(put, null, null);
                    win.show();
                }
                if (record.id == "dowloadXml") {
                    //    var btnEdit = $$("btnPlus");
                    //     btnEdit.callEvent("onClick");
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }
                    if (item.tthai == "Chờ ký" || item.tthai == "Chờ gửi") {
                        webix.message("Hóa đơn đang ở trạng thái chờ ký/chờ gửi không thể tải XML !", "error");
                        return;
                    }

                    webix.extend(table, webix.ProgressBar);
                    table.disable();
                    table.showProgress();

                    var url = app.config.host + "/Invoice68/ExportXMLHoadon?id=" + item.id;
                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            table.enable();
                            table.hideProgress();

                            var file = new Blob([data], { type: "application/xml" });
                            webix.html.download(file, "XmlInvoice.xml");
                            // var fileURL = URL.createObjectURL(file);
                            // window.open(fileURL, "_blank");
                        });
                }
                if (record.id == "dowloadXmlMessageHD") {
                    //    var btnEdit = $$("btnPlus");
                    //     btnEdit.callEvent("onClick");
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }
                    if (item.tthai == "Chờ ký" || item.tthai == "Chờ gửi") {
                        webix.message("Hóa đơn chưa được gửi tới CQT, không thể tải XML !", "error");
                        return;
                    }

                    webix.extend(table, webix.ProgressBar);
                    table.disable();
                    table.showProgress();

                    var url = app.config.host + "/Invoice68/ExportXMLThongDiepHoadon?id=" + item.id;
                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            table.enable();
                            table.hideProgress();

                            var file = new Blob([data], { type: "application/xml" });
                            webix.html.download(file, "XmlThongDiepHoaDon.xml");
                            // var fileURL = URL.createObjectURL(file);
                            // window.open(fileURL, "_blank");
                        });
                }
                if (record.id == "giaitrinh") {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }
                    if (item.tthdon != "4" && item.tthdon != "0") {
                        webix.message("Hóa đơn không ở trạng thái rà soát không thể lập giải trình !", "error");
                        return;
                    }

                    var win = table.$scope.ui(huyhoadon.$ui);
                    huyhoadon.setEditMode(1);
                    webix.$$("btnSaveSignGiaiTrinh").show();
                    webix.$$("btnHuyhoadon").destructor();
                    // webix.$$("danhsach_hoadonhuy").define("editable", false);
                    // webix.$$("tctbao").setValue(2);
                    webix.$$("btnGhiHuyhoadon").destructor();
                    webix.$$("btnGhiSignHuyHDDT").destructor();
                    this.hide();
                    var put = {
                        khieu: item.khieu,
                        shdon: item.shdon,
                        ngay: item.nlap,
                        ladhddt: 1,
                        tctbao: 2,
                        hdon_id: item.id,
                    };
                    huyhoadon.setValues(put, null, null);
                    win.show();

                    // var fn = Function("_", "user", item.on_click);
                    // fn(_, user);
                }
                if (record.id == "notifyErrorInvoice") {
                    var table = webix.$$("invoiceViews");
                    var datafrm = table.serialize();
                    var data = [];

                    if (datafrm.length > 0) {
                        for (var i = 0; i < datafrm.length; i++) {
                            if (datafrm[i] != undefined) {
                                if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true) {
                                    data.push(datafrm[i]);
                                }
                            }
                        }
                    }

                    if (data.length > 0) {
                        var cungshd = true;
                        for (var k = 1; k <= data.length - 1; k++) {
                            if (data[k]["stbao"] !== data[0]["stbao"]) {
                                cungshd = false;
                                break;
                            }
                        }
                        if (cungshd === true) {
                            var loai = 1;
                            if (data[0]["stbao"] !== null) loai = 2;
                            for (var i = 0; i < data.length; i++) {
                                if (data[i].tthai == "Chờ gửi" || data[i].tthai == "Chờ ký" || data[i].tthai == "Đã ký") {
                                    webix.message("Hóa đơn chưa gửi không thể thông báo sai sót", "error");
                                    return;
                                }
                                if (data[i].ladhddt == undefined) {
                                    data[i].ladhddt = "1";
                                }
                                if (data[i].tctbao == undefined) {
                                    data[i].tctbao = "1";
                                }
                            }

                            var win = table.$scope.ui(huyhoadon.$ui);
                            huyhoadon.setEditMode(1);
                            webix.$$("btnHuyhoadon").destructor();
                            var tongquat = {
                                so: data[0].stbao,
                                ntbccqt: data[0].ntbao,
                                loai: loai,
                            };
                            huyhoadon.setValuesTongQuat(tongquat, null, null);
                            data.forEach(function (item) {
                                var put = {
                                    khieu: item.khieu,
                                    shdon: item.shdon,
                                    ngay: item.nlap,
                                    ladhddt: item.ladhddt,
                                    tctbao: item.tctbao,
                                    hdon_id: item.id,
                                };
                                huyhoadon.setValues(put, null, null);
                                //this.hide();
                                win.show();
                            });
                        } else {
                            webix.message("Các hóa đơn chọn phải có số thông báo giống nhau", "error");
                        }
                    } else {
                        var item = table.getSelectedItem();

                        var loai = 1;
                        if (item["stbao"] !== null) loai = 2;

                        if (item == null) {
                            webix.message("Bạn chưa chọn hóa đơn", "error");
                            return;
                        }
                        if (item.tthai == "Chờ gửi" || item.tthai == "Chờ ký" || item.tthai == "Đã ký") {
                            webix.message("Hóa đơn chưa gửi không thể thông báo sai sót", "error");
                            return;
                        }

                        if (item.ladhddt == undefined) {
                            item["ladhddt"] = "1";
                        }
                        if (item.tctbao == undefined) {
                            item["tctbao"] = "1";
                        }

                        var win = table.$scope.ui(huyhoadon.$ui);
                        huyhoadon.setEditMode(1);
                        webix.$$("btnHuyhoadon").destructor();

                        var tongquat = {
                            so: item.stbao,
                            ntbccqt: item.ntbao,
                            loai: loai,
                        };
                        huyhoadon.setValuesTongQuat(tongquat, null, null);

                        var put = {
                            khieu: item.khieu,
                            shdon: item.shdon,
                            ngay: item.nlap,
                            ladhddt: item.ladhddt,
                            tctbao: item.tctbao,
                            hdon_id: item.id,
                        };
                        huyhoadon.setValues(put, null, null);

                        //this.hide();
                        win.show();
                    }
                    // var win = table.$scope.ui(huyhoadon.$ui);
                    // huyhoadon.setEditMode(1);
                    // webix.$$("btnHuyhoadon").destructor();

                    // webix.$$("btnSaveSignGiaiTrinh").show();
                    // webix.$$("btnHuyhoadon").destructor();
                    // webix.$$("btnGhiHuyhoadon").destructor();
                    // webix.$$("btnGhiSignHuyHDDT").destructor();
                    // this.hide();
                    // win.show();
                }

                if (record.id == "replace_invoice") {
                    debugger;
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn !", "error");
                        return;
                    }
                    if (item.tthai == "Chờ gửi" || item.tthai == "Chờ ký") {
                        webix.message("Hóa đơn chưa ký không thể thực hiện", "error");
                        return;
                    }
                    var btnPlus = $$("btnPlus");
                    btnPlus.callEvent("onClick");
                    var detailForm = $$("invoiceForms");

                    var ngay_hoa_don = item.nlap;

                    item.nlap = null;
                    item.shdon_temp = item.shdon;
                    item.shdon = "";
                    item.hdon_id_old = item.hdon_id;
                    item.tthdon_old = item.tthdon;
                    item.tthdon = 2;
                    item.tthdon_original = 2;
                    item.nky = "";
                    item.hdon_id = "";

                    for (var i = 0; i < arrayString.length; i++) {
                        item[arrayString[i]] = 0;
                    }
                    item["tgtttbchu"] = "";
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(item, null);

                    var tableDetail = webix.$$("hoadon68_chitiet");

                    tableDetail.clearAll();

                    tableDetail.refresh();

                    detailForm.show();
                }
                if (record.id == "increase_change_invoice") {
                    debugger;
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn !", "error");
                        return;
                    }
                    if (item.tthai == "Chờ gửi" || item.tthai == "Chờ ký") {
                        webix.message("Hóa đơn chưa ký không thể thực hiện", "error");
                        return;
                    }
                    var btnPlus = $$("btnPlus");
                    btnPlus.callEvent("onClick");
                    var detailForm = $$("invoiceForms");

                    var ngay_hoa_don = item.nlap;

                    item.nlap = null;
                    item.shdon_temp = item.shdon;
                    item.shdon = "";
                    item.hdon_id_old = item.hdon_id;
                    item.tthdon_old = item.tthdon;
                    item.tthdon = 19;
                    item.tthdon_original = 19;
                    item.nky = "";
                    item.hdon_id = "";

                    for (var i = 0; i < arrayString.length; i++) {
                        item[arrayString[i]] = 0;
                    }
                    item["tgtttbchu"] = "";
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(item, null);

                    // var tableDetail = webix.$$("hoadon68_chitiet");
                    // tableDetail.clearAll();
                    // tableDetail.refresh();

                    var obj_detail = {
                        id: webix.uid(),
                        kmai: "4",
                        ten: "Hóa đơn điều chỉnh cho hóa đơn số " + item.shdon_temp + " ngày " + moment(ngay_hoa_don).format("DD/MM/YYYY") + " ký hiệu " + item.khieu,
                    };
                    var tableDetail = webix.$$("hoadon68_chitiet");
                    tableDetail.clearAll();
                    tableDetail.add(obj_detail);
                    var columns = tableDetail.config.columns;
                    for (var i = 0; i < columns.length; i++) {
                        var column = columns[i];
                        //column.editor = null;
                    }
                    /*tableDetail.getColumnConfig("sluong").editor = null;
                    tableDetail.getColumnConfig("thtien").editor = null;
                    tableDetail.getColumnConfig("tsuat").editor = null;
                    tableDetail.getColumnConfig("tgtien").editor = null;
                    tableDetail.getColumnConfig("tthue").editor = null;*/
                    tableDetail.refreshColumns();
                    tableDetail.refresh();

                    detailForm.show();

                    //detailForm.show();
                }
                if (record.id == "reduce_change_invoice") {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn !", "error");
                        return;
                    }
                    if (item.tthai == "Chờ gửi" || item.tthai == "Chờ ký") {
                        webix.message("Hóa đơn chưa ký không thể thực hiện", "error");
                        return;
                    }
                    var btnPlus = $$("btnPlus");
                    btnPlus.callEvent("onClick");
                    var detailForm = $$("invoiceForms");

                    var ngay_hoa_don = item.nlap;

                    item.nlap = null;
                    item.shdon_temp = item.shdon;
                    item.shdon = "";
                    item.hdon_id_old = item.hdon_id;
                    item.tthdon_old = item.tthdon;
                    item.tthdon = 21;
                    item.tthdon_original = 21;
                    item.nky = "";
                    item.hdon_id = "";
                    item.note = "reduce_change_invoice";

                    for (var i = 0; i < arrayString.length; i++) {
                        item[arrayString[i]] = 0;
                    }
                    item["tgtttbchu"] = "";
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(item, null);

                    var obj_detail = {
                        id: webix.uid(),
                        kmai: "4",
                        ten: "Hóa đơn điều chỉnh cho hóa đơn số " + item.shdon_temp + " ngày " + moment(ngay_hoa_don).format("DD/MM/YYYY") + " ký hiệu " + item.khieu,
                    };
                    var tableDetail = webix.$$("hoadon68_chitiet");
                    tableDetail.clearAll();
                    tableDetail.add(obj_detail);
                    tableDetail.refresh();

                    detailForm.show();
                }
                if (record.id == "info_change") {
                    debugger;
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn !", "error");
                        return;
                    }
                    if (item.tthai == "Chờ gửi" || item.tthai == "Chờ ký") {
                        webix.message("Hóa đơn chưa ký không thể thực hiện", "error");
                        return;
                    }
                    var btnPlus = $$("btnPlus");
                    btnPlus.callEvent("onClick");
                    var detailForm = $$("invoiceForms");

                    var ngay_hoa_don = item.nlap;

                    item.nlap = null;
                    item.hdon_id_old = item.hdon_id;
                    item.tthdon_old = item.tthdon;
                    item.tthdon = 23;
                    item.tthdon_original = 23;
                    item.shdon_temp = item.shdon;
                    item.shdon = "";
                    item.nky = "";
                    item.hdon_id = "";

                    for (var i = 0; i < arrayString.length; i++) {
                        item[arrayString[i]] = 0;
                    }
                    item["tgtttbchu"] = "";
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(item, null);

                    var obj_detail = {
                        id: webix.uid(),
                        kmai: "4",
                        ten: "Hóa đơn điều chỉnh cho hóa đơn số " + item.shdon_temp + " ngày " + moment(ngay_hoa_don).format("DD/MM/YYYY") + " ký hiệu " + item.khieu,
                    };
                    var tableDetail = webix.$$("hoadon68_chitiet");
                    tableDetail.clearAll();
                    tableDetail.add(obj_detail);
                    var columns = tableDetail.config.columns;
                    for (var i = 0; i < columns.length; i++) {
                        var column = columns[i];
                        //column.editor = null;
                    }
                    // tableDetail.getColumnConfig("sluong").editor = null;
                    // tableDetail.getColumnConfig("thtien").editor = null;
                    // tableDetail.getColumnConfig("tsuat").editor = null;
                    // tableDetail.getColumnConfig("tgtien").editor = null;
                    // tableDetail.getColumnConfig("tthue").editor = null;
                    tableDetail.refreshColumns();
                    tableDetail.refresh();

                    detailForm.show();
                }
            },
        },
    });

    webix.$$("title").parse({ title: "Lập hóa đơn -> Phiếu xuất kho hàng gửi bán đại lý", details: "Lập hóa đơn -> Phiếu xuất kho hàng gửi bán đại lý" });
    webix.ui({
        view: "contextmenu",
        id: "cmenu",
        autowidth: true,
        //css: { "background": "#ccc !important" },
        data: [
            { value: "Thêm (F4)", icon: "far fa-plus" },
            { value: "Sửa (F3)", icon: "far fa-edit" },
            { value: "Xóa (F8)", icon: "far fa-trash" },
            { $template: "Separator" },
            { value: "Tải File đính kèm", icon: "fas fa-file-text-o" },
            { value: "Xem in", icon: "fas fa-print" },
        ],
        on: {
            onItemClick: function (id) {
                if (this.getItem(id).value == "Thêm (F4)") {
                    $$("btnPlus").callEvent("onClick");
                }
                if (this.getItem(id).value == "Sửa (F3)") {
                    $$("btnEdit").callEvent("onClick");
                }
                if (this.getItem(id).value == "Xóa (F8)") {
                    $$("btnDelete").callEvent("onClick");
                }
                if (this.getItem(id).value == "Tải File đính kèm") {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn !", "error");
                        return;
                    }
                    webix.message("Continue !", "debug");
                    //console.log(item);
                    // if (item.inv_originalId != null && item.inv_originalId != undefined) {
                    //     var box = webix.modalbox({
                    //         title: "Đang in chứng từ",
                    //         text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                    //         width: 350,
                    //         height: 200
                    //     });

                    //     //var newWin = window.open();

                    //     webix.ajax()
                    //         .response("arraybuffer")
                    //         .headers({ 'Content-type': 'application/json' })
                    //         .get(app.config.host + "/Invoice/PrintInvoice?id=" + item.inv_originalId + "&type=PDF", {
                    //             error: function (text, data, XmlHttpRequest) {
                    //                 webix.modalbox.hide(box);
                    //                 alert(text);
                    //             },
                    //             success: function (text, data, XmlHttpRequest) {
                    //                 var file = new Blob([data], { type: 'application/pdf' });
                    //                 var fileURL = URL.createObjectURL(file);

                    //                 webix.modalbox.hide(box);
                    //                 //newWin.location = fileURL;
                    //                 window.open(fileURL, '_blank');

                    //             }
                    //         });

                    //     // webix.ajax()
                    //     // .response("arraybuffer")
                    //     // .get(app.config.host + "/Invoice/PrintInvoice?id=" + item.inv_originalId + "&type=PDF", function (text, data) {
                    //     //     var file = new Blob([data], { type: 'application/pdf' });
                    //     //     var fileURL = URL.createObjectURL(file);
                    //     //     window.open(fileURL, '_blank');
                    //     //     // webix.html.download(file, $$('tenfile').getValue());
                    //     // });
                    // }
                    // else {
                    //     webix.message("Không tồn tại hóa đơn gốc !", "error");
                    //     return;
                    // }
                }
            },
        },
    });
    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnReload",
            on: {
                onClick: function () {
                    webix.callEvent("onLoadError");
                    var table = webix.$$("invoiceViews");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Thêm (F4)") + "</span>",
            id: "btnPlus",
            //css:"button_raised",
            on: {
                onClick: function () {
                    webix.callEvent("onLoadError");
                    var detailForm = this.$scope.ui(invoiceForms.$ui);

                    var cctbao_id = $$("kyhieu_id").getValue();
                    invoiceForms.initUINew(cctbao_id);
                    invoiceForms.setEditMode(1);
                    detailForm.show();
                    // if (vc_window.after_new != null) {
                    //     var fn = Function("app", vc_window.after_new);
                    //     fn(app);
                    // }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Sửa (F3)") + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            //css:"button_raised",
            on: {
                onClick: function () {
                    webix.callEvent("onLoadError");
                    var detailForm = this.$scope.ui(invoiceForms.$ui);
                    //var detailForm = this.$scope.ui(webix.$$("main"));
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần sửa !", "error");
                        return;
                    }

                    if (item.tthai != "Chờ ký") {
                        webix.message("Hóa đơn đã ký hoặc đã gửi lên cơ quan thuế, không thể sửa !", "error");
                        return;
                    }
                    // main.setEditMode(0);
                    // main.initUI(windowconfig);

                    item.nlap = new Date(item.nlap);
                    invoiceForms.setEditMode(2);
                    invoiceForms.setValues(item, null);

                    detailForm.show();
                    //main.focusFirstElement();
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Xóa (F8)") + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            //css:"button_raised",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần xóa !", "error");
                        return;
                    }

                    if (item.tthai != "Chờ ký") {
                        webix.message("Không thể xóa hóa đơn đã ký,đã gửi !", "error");
                        return;
                    }
                    var khData = {};
                    // khData.windowid = windowconfig.data[0].window_id;
                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(item);

                    webix.confirm({
                        title: "XÓA HÓA ĐƠN",
                        text: "Bạn có muốn xóa hóa đơn số: <span style='color: red'>" + item.shdon + "</span>",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/hoadonSaveChange", khData, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.ExceptionMessage, "error");
                                            app.callEvent("dataChangedInvoie68", [1, null]);
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            var result = JSON.parse(text);

                                            if (result.hasOwnProperty("error")) {
                                                webix.message({ type: "error", text: result.error });
                                                table.enable();
                                                table.hideProgress();
                                                return;
                                            } else {
                                                table.remove(item.id);
                                                table.enable();
                                                table.hideProgress();
                                            }
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-files-o fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("COPY") + "</span>",
            id: "btnCopy",
            // view: "button",
            // type: "icon",
            // icon: "far fa-files-o",
            // label: _("COPY"),
            // width: 85,
            // css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng dữ liệu");
                        return;
                    }
                    // var vc_window = windowconfig["data"][0];
                    // windowform.$ui.id = vc_window.code;
                    var detailForm = this.$scope.ui(invoiceForms.$ui);
                    item.shdon = null;
                    invoiceForms.setEditMode(1);
                    invoiceForms.copyValues(item);
                    detailForm.show();
                    // windowform.focusFirstElement();
                    // if (vc_window.after_new != null) {
                    //     var fn = Function(vc_window.after_new);
                    //     fn();
                    // }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("PRINT") + "</span>",
            id: "btnPrint",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn cần in !", "error");
                        return;
                    }
                    //webix.message("Chưa có mẫu in ...", "error");
                    // var rpParameter = null;
                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inHoadon?id=" + item.id + "&type=PDF&inchuyendoi=false", {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("In chuyển đổi") + "</span>",
            id: "btnPrintConvert",
            hidden: true,
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn cần in !", "error");
                        return;
                    }
                    //webix.message("Chưa có mẫu in ...", "error");
                    // var rpParameter = null;
                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inHoadon?id=" + item.id + "&type=PDF&inchuyendoi=true", {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-evelop fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Email") + "</span>",
            id: "btnEmail",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    debugger;
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn cần in !", "error");
                        return;
                    }
                    if (item.tthai == "Chờ ký") {
                        webix.message("Hóa đơn chưa ký không thể gửi email !", "error");
                        return;
                    }
                    require(["views/forms/sendemail", "app"], function (sendemail, app) {
                        debugger;
                        var url = "/Invoice68/GetEmailTemplate?type=Gửi hóa đơn&id=" + item.hdon_id;
                        webix.ajax(app.config.host + url, function (text) {
                            var json = JSON.parse(text);
                            var win = webix.$$("invoiceViews").$scope.ui(sendemail.$ui);
                            var hoadon = {
                                nguoi_gui: json.sender,
                                tieude: json.subject,
                                noidung: json.body,
                                mat_khau: json.pass,
                                inv_invoiceauth_id: item.hdon_id,
                                id: item.hdon_id,
                                type: "Gửi hóa đơn",
                                nguoinhan: item.email,
                                his_email: json.his_email,
                                lst_email: json.lst_email,
                                hdon68: 1,
                                alias: json.alias,
                            };

                            //console.log(JSON.stringify(json.lst_email));
                            sendemail.setValues(hoadon);
                            win.show();
                        });
                    });
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Ký chờ xử lý") + "</span>",
            id: "btnKyCXL",
            click: function () {
                //check validate và return ra list hoadon_id
                var lsthdon_id = commonHD.validate_HD_ReturnListHdonId();

                if (lsthdon_id.length > 0) {
                    //call function excute hoa don
                    commonHD.ExcuteHoaDon(lsthdon_id, false);
                }
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Ký và gửi CQT") + "</span>",
            id: "btnKyGuiCQT",
            click: function () {
                //check validate và return ra list hoadon_id
                var lsthdon_id = commonHD.validate_HD_ReturnListHdonId();

                if (lsthdon_id.length > 0) {
                    //call function excute hoa don
                    commonHD.ExcuteHoaDon(lsthdon_id, true);
                }
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Gửi CQT") + "</span>",
            id: "btnGuiCQT",
            click: function () {
                var tableHdon = webix.$$("invoiceViews");
                /*var itemHdon = tableHdon.getSelectedItem();
                console.log(itemHdon);
                if (itemHdon.tthai != "Đã ký") {
                    webix.message("Hóa đơn chưa ký không thể gửi !", "error");
                    return;
                }*/
                var datafrm = tableHdon.serialize();

                var data = [];
                if (datafrm.length > 0) {
                    for (var i = 0; i < datafrm.length; i++) {
                        if (datafrm[i] != undefined) {
                            if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true) {
                                data.push(datafrm[i].hdon_id);
                            }
                        }
                    }
                }

                var postData = {
                    invs: data,
                    mltdiep: "203",
                };

                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].tthai != null && data[i].tthai == "Đã gửi") {
                            webix.message("Hóa đơn đã gửi không thể gửi !", "error");
                            return;
                        }
                    }
                    webix.confirm({
                        text: "Bạn có muốn gửi hóa đơn tới CQT không?",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/SendInvoiceToCQT68", postData, function (text) {
                                        var res = JSON.parse(text);
                                        if (res.hasOwnProperty("error")) {
                                            webix.message({ type: "error", text: res.error });
                                        } else {
                                            webix.message("Gửi CQT thành công!");
                                            var table = $$("invoiceViews");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        }
                                    });
                            }
                        },
                    });
                } else webix.message("Bạn chưa chọn hóa đơn, các hóa đơn đều chưa ở trạng thái kí!");
            },
        },

        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Nghiệp vụ") + "</span>",
            id: "btnChucNang",
            // label: _("CHUC_NANG"),
            // view: "button",
            // type: "icon",
            // css: "webix_secondary",
            // icon: "fas fa-tasks",
            // width: 100,
            click: function () {
                subChucNang.show(webix.$$("btnChucNang").$view);
            },
        },
        // {
        //     view: "button", type: "htmlbutton", width: 115,
        //     label: '<i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i> ' +
        //     '<span class="text">' + _("EXPORT_EXCEL") + '</span>',
        //     id: "btnExport",
        //     on: {
        //         onClick: function () {
        //             var box = webix.modalbox({
        //                 title: "Đang kết suất Excel ... ",
        //                 buttons: ["Hủy bỏ"],
        //                 text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
        //                 width: 350,
        //                 height: 200,
        //                 callback: function (result) {
        //                     switch (result) {
        //                         case "0":
        //                             clearTimeout(interVal);
        //                             webix.modalbox.hide(box);
        //                             break;
        //                     }
        //                 }
        //             });
        //             var interVal = setTimeout(function () {
        //                 var table = $$("invoiceViews");
        //                 webix.toExcel(table, {
        //                     filename: "danh sach hoa don",
        //                     rawValues: true,
        //                     filterHTML: true
        //                 });
        //                 clearTimeout(interVal);
        //                 webix.modalbox.hide(box);
        //             }, 1500);

        //             // var rpParameter = table.config.rpParameter;
        //             // if (rpParameter == undefined || rpParameter == null) {
        //             //     webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
        //             //     return;
        //             // }
        //             // rpParameter.type = "xlsx";

        //             // if (rpParameter != null) {
        //             //     var box = webix.modalbox({
        //             //         title: "Đang kết xuất Excel ....",
        //             //         text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
        //             //         width: 350,
        //             //         height: 200
        //             //     });

        //             //     webix.ajax()
        //             //         .response("arraybuffer")
        //             //         .headers({ 'Content-type': 'application/json' })
        //             //         .post(app.config.host + "/Bcthsd/PrintPDFBangKeBanRa", rpParameter,
        //             //         {
        //             //             error: function (text, data, XmlHttpRequest) {
        //             //                 webix.modalbox.hide(box);
        //             //                 webix.message(XmlHttpRequest.statusText, "error");
        //             //             },
        //             //             success: function (text, data, XmlHttpRequest) {
        //             //                 var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        //             //                 var fileURL = URL.createObjectURL(file);
        //             //                 webix.modalbox.hide(box);
        //             //                 window.location.href = fileURL;
        //             //             }
        //             //         }
        //             //         // function (text, data) {

        //             //         //     var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        //             //         //     var fileURL = URL.createObjectURL(file);

        //             //         //     window.location.href = fileURL;
        //             //         // }
        //             //         );
        //             // }
        //         }
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     }

        // },
        {},
    ];
    var fm = webix.Number.numToStr({
        groupDelimiter: ",",
        groupSize: 3,
        decimalDelimiter: ".",
        decimalSize: 0,
    });
    var grid = {
        rows: [
            {
                view: "datatable",
                id: "invoiceViews",
                select: true,
                resizeColumn: true,
                fixedRowHeight: false,
                // leftSplit: 8,
                // rightSplit: 1,
                rowLineHeight: 25,
                rowHeight: 25,
                tooltip: true,
                //autowidth: true,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var tlbparam_s = [];
                        //get count list combo ký hiệu
                        var countkyHieu = $$("kyhieu_id") != undefined ? $$("kyhieu_id").getPopup().getList().serialize().length : 0;

                        if (countkyHieu > 0) {
                            var url = app.config.host + "/Invoice68/GetDataInvoice";

                            if (params == null) {
                                url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                            } else {
                                url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                                if (params.filter != null) {
                                    var array = [];

                                    for (var property in params.filter) {
                                        var column = view.getColumnConfig(property);
                                        var item = {
                                            columnName: property,
                                            columnType: column.columnType,
                                            value: params.filter[property],
                                        };

                                        array.push(item);
                                    }

                                    //console.log(JSON.stringify(array));
                                    url = url + "&filter=" + JSON.stringify(array);
                                }
                            }
                            var tlbParams = $$("paramKH");

                            if (tlbParams !== undefined) {
                                var childViews = tlbParams.getChildViews();

                                var parameters = [];

                                for (var i = 0; i < childViews.length; i++) {
                                    var child = childViews[i];
                                    // console.log(child);
                                    var id = child.config.id;
                                    //var value_field = child.config.value_field;
                                    var item = {
                                        columnName: "cctbao_id",
                                        value: $$(id).getValue(),
                                    };
                                    if (item.value !== "") {
                                        parameters.push(item);
                                    } else {
                                        return;
                                    }
                                    parameters.push(item);
                                }
                                tlbparam_s = parameters;
                                url = url + "&tlbparam=" + JSON.stringify(parameters);
                            } else {
                                url = url + "&tlbparam=null";
                            }
                            webix.ajax(url, callback, view);
                        }
                    },
                },
                onClick: {
                    ShowHistory: function (event, cell, target) {
                        var table = webix.$$("invoiceViews");
                        var win = table.$scope.ui(hoadonHistory.$ui);
                        var hdon_id = cell.row;
                        hoadonHistory.setEditMode(hdon_id);
                        win.show();
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                    onItemClick: function (id) {
                        if (id.column == "ten_loai") {
                            //webix.confirm()
                            //webix.message("Continue Dowloading ... !", "debug");
                        }
                    },
                    // onClick: function (e) {
                    //     // var view = $$(e);
                    //     // if (!view || !view.getFormView || view.getFormView() != $$("invoiceDate"))
                    //     //     webix.confirm("The changes was not saved");
                    // },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onAfterLoad: function () {
                        // webix.delay(function () {
                        //     this.adjustRowHeight("ten_loai", true);
                        //     this.render();
                        // }, this);
                    },
                    onColumnResize: function () {
                        // this.adjustRowHeight("ten_loai", true);
                        // this.render();
                    },
                    onSubViewCreate: function (view, item) {
                        var subView = view;

                        if (subView != null) {
                            var tableDetail = webix.$$("hoadon68_chitiet_view");
                            var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
                            tableDetail.clearAll();
                            tableDetail.load(url);
                        }
                    },
                },
                subview: {
                    borderless: true,
                    view: "tabview",
                    height: 255,
                    css: "subInvoice",
                    scrollX: true,
                    // tabbar: {
                    //     width: 200
                    // },
                    multiview: {
                        fitBiggest: true,
                    },
                    cells: [
                        {
                            header: "Chi tiết hóa đơn",
                            select: "cell",
                            body: {
                                view: "datatable",
                                resizeColumn: true,
                                margin: 10,
                                id: "hoadon68_chitiet_view",
                                height: 200,
                                columns: [
                                    { id: "stt", header: "", width: 30 },
                                    {
                                        id: "ma",
                                        header: _("Mã hàng hóa"),
                                        editor: "text",
                                        width: 100,
                                        editable: true,
                                    },
                                    {
                                        id: "ten",
                                        header: _("Tên hàng hóa"),
                                        editor: "text",
                                        width: 350,
                                        editable: true,
                                    },
                                    {
                                        id: "mdvtinh",
                                        header: _("ĐVT"),
                                        editor: "text",
                                        width: 80,
                                        editable: true,
                                    },
                                    {
                                        id: "sluong",
                                        header: _("Số lương"),
                                        editor: "autonumeric",
                                        width: 100,
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                    },
                                    {
                                        id: "dgia",
                                        header: _("Đơn giá"),
                                        width: 150,
                                        editor: "autonumeric",
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                        // value_change: field.value_change
                                    },
                                    {
                                        id: "thtien",
                                        header: _("Tiền trước thuế"),
                                        width: 150,
                                        editor: "autonumeric",
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                        hidden: true,
                                        // value_change: field.value_change
                                    },
                                    {
                                        id: "tlckhau",
                                        header: _("% CK"),
                                        width: 80,
                                        editor: "autonumeric",
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                        hidden: true,
                                    },
                                    {
                                        id: "stckhau",
                                        header: _("Tiền CK"),
                                        width: 150,
                                        editor: "autonumeric",
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                        hidden: true,
                                    },
                                    {
                                        id: "tsuat",
                                        header: _("Thuế suất"),
                                        width: 100,
                                        editor: "autonumeric",
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                        hidden: true,
                                    },
                                    {
                                        id: "tthue",
                                        header: _("Tiền thuế"),
                                        width: 150,
                                        editor: "autonumeric",
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                        hidden: true,
                                    },
                                    {
                                        id: "tgtien",
                                        header: _("Thành tiền"),
                                        width: 150,
                                        editor: "autonumeric",
                                        editable: true,
                                        css: { "text-align": "right" },
                                        format: webix.Number.numToStr({
                                            groupDelimiter: ",",
                                            groupSize: 3,
                                            decimalDelimiter: ".",
                                            decimalSize: 2,
                                        }),
                                    },
                                    {
                                        id: "khuyen_mai",
                                        header: _("Khuyến mại"),
                                        width: 100,
                                        editor: "checkbox",
                                        editable: true,
                                        template: "{common.checkbox()}",
                                        checkValue: true,
                                        uncheckValue: false,
                                    },
                                ],
                                on: {
                                    onBeforeLoad: function () {
                                        this.showOverlay(_("LOADING") + "...");
                                    },
                                    onAfterLoad: function () {
                                        this.hideOverlay();
                                    },
                                },
                            },
                        },
                    ],
                },
                columns: [
                    {
                        id: "chon",
                        header: [{ text: "", content: "masterCheckbox" }],
                        width: 30,
                        checkValue: true,
                        uncheckValue: false,
                        template: "{common.checkbox()}",
                    },
                    {
                        id: "subRowMain",
                        header: "&nbsp;",
                        template: "{common.subrow()}",
                        width: 40,
                        // height: 25
                    },
                    {
                        id: "stt",
                        header: ["<center>STT</center>"],
                        //header: "",
                        sort: "int",
                        width: 40,
                        columnType: "nvarchar",
                        //css: { "line-height": "20px !important", "height": "63px !important"}
                    },
                    {
                        id: "ttthdon",
                        header: ["<center>Trạng thái</center>", { content: "textFilter" }],
                        template: "<span class='mcls_status#tthdon#'>#ttthdon#</span>",
                        // template: function (obj, common) {
                        //     console.log(obj.tthdon);
                        //     return "<span class='mcls_status " + obj.tthdon + "'>" + obj.ttthdon + "</span>";
                        // },
                        width: 150,
                        columnType: "nvarchar",
                    },
                    {
                        id: "tthai",
                        header: ["<center>Trạng thái gửi CQT</center>", { content: "textFilter" }],
                        width: 120,
                        columnType: "nvarchar",
                    },
                    {
                        id: "btnMessagetn",
                        header: ["<center>Lịch sử xử lý</center>"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            return "<span class='ShowHistory' style='color: #00b300'><i class='fa fa-eye'></i></span> ";
                        },
                        width: 180,
                    },
                    {
                        id: "issendmail",
                        header: ["<center>Gửi Email</center>"],
                        css: { "text-align": "center" },
                        width: 120,
                        columnType: "nvarchar",
                        checkValue: true,
                        uncheckValue: false,
                        template: "{common.rcheckbox()}",
                    },
                    {
                        id: "error_status",
                        header: ["<center>Có lỗi/Không có lỗi</center>"],
                        css: { "text-align": "center" },
                        width: 120,
                        columnType: "nvarchar",
                        template: function (obj) {
                            if (obj.error_status == "1") {
                                return "<span style='color: #e83717'>Có lỗi</span> ";
                            } else {
                                return "";
                            }
                        },
                    },
                    {
                        id: "nlap",
                        header: ["<center>Ngày hóa đơn</center>", { content: "serverDateRangeFilter" }],
                        width: 120,
                        css: { "text-align": "center" },
                        //format:webix.i18n.dateFormatStr
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        // template: function (obj, common) {
                        //     if(obj.tthai!="Chờ ký"){
                        //         return obj.nlap="";
                        //     }
                        //     else{
                        //         var format = webix.Date.dateToStr("%d/%m/%Y");
                        //         var text = removeTimeZoneT(obj.nlap);
                        //         return format(text);
                        //     }
                        // },
                        columnType: "datetime",
                    },
                    {
                        id: "nky",
                        header: ["<center>Ngày ký</center>", { content: "serverDateRangeFilter" }],
                        width: 150,
                        css: { "text-align": "center" },
                        //format:webix.i18n.dateFormatStr
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y %H:%i:%s");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        columnType: "datetime",
                    },
                    {
                        id: "khieu",
                        header: ["<center>Ký hiệu</center>", { content: "textFilter" }],
                        width: 100,
                        css: { "text-align": "center" },
                        columnType: "nvarchar",
                    },
                    // {
                    //     id: "khieu", header: ["<center>Ký hiệu</center>", { content: "textFilter" }],
                    //     template: function (obj, common) {
                    //         return "<span class='mcls_status " + obj.className + "'>" + obj.ten_trang_thai + "</span>";
                    //     }, width: 100, columnType: "nvarchar"
                    // },
                    { id: "shdon", header: ["<center>Số hóa đơn</center>", { content: "textFilter" }], width: 100, columnType: "numeric", css: { "text-align": "center" } },

                    { id: "mst", header: ["<center>Mã số thuế</center>", { content: "textFilter" }], width: 120, columnType: "nvarchar" },
                    // { id: "mtdiep_gui", header: ["<center>Mã thông điệp gửi đi</center>", { content: "textFilter" }], width: 350, columnType: "nvarchar" },
                    // { id: "mtdiep_cqt", header: ["<center>Mã thông điệp cơ quan thuế</center>", { content: "textFilter" }], width: 300, columnType: "nvarchar" },
                    { id: "ten", header: ["<center>Tên đơn vị/ Tên người mua</center>", { content: "textFilter" }], width: 150, columnType: "nvarchar" },
                    {
                        id: "tgtttbso",
                        header: ["<center>Thành tiền sau thuế</center>", { content: "textFilter" }],
                        width: 150,
                        //  format: webix.Number.numToStr({
                        //     groupDelimiter: ".",
                        //     groupSize: 3,
                        //     decimalDelimiter: ",",
                        //     decimalSize: 0
                        // }),
                        css: { "text-align": "right" },
                        template: function (obj, common) {
                            return obj.tgtttbso == null ? 0 : fm(obj.tgtttbso);
                        },
                        columnType: "numeric",
                    },
                    { id: "htttoan", header: ["<center>Hình thức TT</center>", { content: "textFilter" }], width: 200, columnType: "nvarchar", css: { "text-align": "center" } },
                    // {
                    //     id: "tdlap",
                    //     header: ["<center>Thời điểm lập</center>", { content: "textFilter" }],
                    //     width: 200,
                    //     columnType: "nvarchar",
                    //     format: function (text) {
                    //         var format = webix.Date.dateToStr("%d/%m/%Y");
                    //         text = removeTimeZoneT(text);
                    //         return format(text);
                    //     },
                    // },
                    {
                        id: "nglap",
                        header: ["<center>Người lập</center>", { content: "textFilter" }],
                        width: 200,
                        columnType: "nvarchar",
                    },
                    // { id: "ten_loai", header: ["<center>Tên loại HĐ</center>", { content: "textFilter" }], width: 200, columnType: "nvarchar" }
                ],
                pager: "pagerA",
                type: {
                    rcheckbox: function (obj, common, value, config) {
                        var checked = value == config.checkValue ? 'checked="true"' : "";
                        return "<input disabled class='webix_table_checkbox' type='checkbox' " + checked + ">";
                    },
                },
            },
            {
                // cols: [
                // {
                //     view: "icon",
                //     icon: "fas fa-question-circle",
                //     id: "help_form_main",
                //     tooltip: _("HELPER"),
                //     css: "helper",
                //     click: function () {
                //         webix.alert({
                //             title: "Thông báo !",
                //             text: "Bạn chưa khai báo hướng dẫn nhanh cho màn hình này !",
                //             type: "alert-warning",
                //             callback: function (result) {},
                //         });
                //     },
                // },
                // {
                //     view: "combo",
                //     label: _("MENU_QUICK"),
                //     id: "QuickMenuMain",
                //     hide: true,
                //     options: {
                //         // view: "gridsuggest",
                //         // id: "grid_window",
                //         // template: "#name#",
                //         // body: {
                //         //     scroll: true,
                //         //     autoheight: false,
                //         //     columns: [
                //         //         { id: "window_id", header: "Window Id", editor: "text", width: 100 },
                //         //         { id: "name", header: "Window Name", editor: "text", width: 400 }
                //         //     ],
                //         //     url: app.config.host + "/System/GetQuickMenu"
                //         // },
                //         // on: {
                //         //     onValueSuggest: function (obj) {
                //         //         var start = obj.window_id != "" ? obj.code + ":" + obj.window_id : obj.code;
                //         //         window.location.hash = "!/app/" + start;
                //         //         $$("QuickMenu").setValue(null);
                //         //     }
                //         // }
                //     },
                //     on: {
                //         onChange: function (newv, oldv) {},
                //     },
                // },
                // {},
                // {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                // },
                // ],
            },
        ],
    };

    var layout = {
        css: "frmView",
        rows: [
            {
                id: "paramKH",
                view: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                borderless: true,
                height: 35,
                cols: [
                    {
                        id: "kyhieu_id",
                        view: "combo",
                        width: 300,
                        label: _("Ký hiệu"),
                        options: {
                            body: {
                                //comboId: comp.id,
                                // isDefault: item.has_default,
                                url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00208",
                                ready: function () {
                                    // if (this.config.isDefault == "C") {
                                    var count = this.count();

                                    if (count > 0) {
                                        var combo = webix.$$("kyhieu_id");
                                        combo.setValue(this.getFirstId());
                                    }
                                    // }
                                },
                            },
                        },
                        // value_field: item.value_field,
                        // isDefault: item.has_default,
                        on: {
                            onChange: function (newv, oldv) {
                                var table = $$("invoiceViews");
                                table.clearAll();
                                table.load(table.config.url);
                            },
                        },
                    },
                ],
            },
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChangedInvoie68", function (editmode, item) {
                var table = webix.$$("invoiceViews");

                if (editmode == 1) {
                    table.clearAll();
                    table.load(table.config.url);
                    // console.log(item);
                    // console.log(item.ten_loai);
                    // table.add(item);
                    //table.select(item.id,false);
                }

                if (editmode == 2) {
                    table.clearAll();
                    table.load(table.config.url);
                    // var selectedItem = table.getSelectedItem();
                    // table.updateItem(selectedItem.id, item);

                    // webix.UIManager.setFocus($$(table.config.id));
                }
            });
            // var table = $$("invoiceViews");
            // var url = {
            //     $proxy: true,
            //     load: function (view, callback, params) {
            //         console.log(view.config);
            //         console.log(callback);
            //         console.log(params);
            //         /*var windowno = app.path[1].params[0];
            //         var url = app.config.host + "/System/GetDataByWindowNo?window_id=" + windowno;

            //         if (params == null) {
            //             url = url + "&start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
            //         }
            //         else {
            //             url = url + "&start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

            //             if (params.filter != null) {
            //                 var array = [];

            //                 for (var property in params.filter) {

            //                     var column = view.getColumnConfig(property);

            //                     if (column !== undefined) {
            //                         var item = {
            //                             columnName: property,
            //                             columnType: column.columnType,
            //                             value: params.filter[property]
            //                         };

            //                         array.push(item);
            //                     }
            //                 }

            //                 //console.log(JSON.stringify(array));
            //                 if (array.length > 0) {
            //                     url = url + "&filter=" + JSON.stringify(array);
            //                 }
            //                 else {
            //                     url = url + "&filter=null";
            //                 }

            //             }
            //             else {
            //                 url = url + "&filter=null";
            //             }
            //         }

            //         if (view.config.infoparam != null) {
            //             url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
            //         }
            //         else {
            //             url = url + "&infoparam=null";
            //         }

            //         var tlbParams = $$("tlbParams");

            //         if (tlbParams !== undefined) {
            //             var childViews = tlbParams.getChildViews();

            //             var parameters = [];

            //             for (var i = 0; i < childViews.length; i++) {
            //                 var child = childViews[i];

            //                 var id = child.config.id;
            //                 var value_field = child.config.value_field;

            //                 var item = {
            //                     columnName: value_field,
            //                     value: $$(id).getValue()
            //                 };

            //                 parameters.push(item);
            //             }

            //             url = url + "&tlbparam=" + JSON.stringify(parameters);

            //         }
            //         else {
            //             url = url + "&tlbparam=null";
            //         }*/

            //         //console.log(url);

            //         /* webix.ajax().bind(view)
            //             .get(url,function(text){
            //                 var json =JSON.parse(text);

            //                 this.clearAll();

            //                 if (!json.hasOwnProperty("error")){
            //                     this.parse(json.data,"json");
            //                 }
            //         }); */

            //         //webix.ajax(url, callback, view);

            //     }
            // };

            // table.define("url", url);
        },
    };
});
