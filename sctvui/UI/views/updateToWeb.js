define([
    "app",
    "locale",
    "views/forms/updateToWeb"
], function (app, _, formUpdate) {
    'use strict';

    //var windowno = app.path[1].params[0];
    var windowno = null;
    //webix.$$("title").parse({ title: "Nhận dữ liệu từ VACOMS", details: "Nhận dữ liệu từ VACOMS" });
    var header = {
        //borderless: true,
        css: { "box-shadow": "0 2px 10px rgba(0,0,0,0.2)" },
        height: 135,
        cols: [
            // { gravity: 1, template: "", width: 150 },
            {
                view: "label",
                template: "<a href='http://minvoice.vn/' target='_blank'><img  src='assets/imgs/LogoMinvoice.png' /></a>",
                height: 115,
                borderless: true,
                css: { "padding": "10px" }
            },
            {
                id: "version",
                borderless: true,
                view: "label",
                template: "<div class='hotline'><b style='font-family:Arial;'>Hotline:</b> <label style='color:red'>0901 80 16 18</label> <br /><i style='float:right; color:red'>info@minvoice.vn</i></div>",
                css: { "padding": "40px", "padding-top": "50px" }
            },
            {
                rows: [
                    { gravity: 1, template: "" },
                    {
                        id: "cbLanguage",
                        view: "combo",
                        width: 125,
                        options: [
                            { id: "vi", value: "Việt Nam" },
                            { id: "en", value: "English" },
                            { id: "ko", value: "Korea" }
                        ],
                        // inputAlign: "right",
                        //css: "cbLanguage",
                        value: "vi",
                        // on: {
                        //     onChange: function (newv, oldv) {
                        //         locale.setLang(newv, null);
                        //     }
                        // }
                    },
                    { gravity: 1, template: "" }
                ]

            }
            // { gravity: 1, template: "", width: 150 }
        ]
    };
    var controls = [
        {},
        {
            id: "btnSave",
            view: "button",
            type: "danger",
            icon: "save",
            label: "Thực hiện (F10)",
            //  width: 120,
            shortcut: "F10",
            on: {
                onClick: function () {
                    var frm = this.$scope.ui(formUpdate.$ui);
                    frm.show();
                    // $$("formConfirm").show();
                    // $$("formConfirmValid").bind($$("formInfo"));
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            view: "button",
            label: "Quay lại đăng nhập",
            type: "form",
            icon: "fas fa-repeat",
            click: function () {
                app.router("/login");
            }
        },
        {}
    ];

    var tabview = {
        id: "my_tabview",
        view: "tabview",
        cells: [
            {
                header: "Bảng dữ liệu",
                body: {
                    id: "tblDM",
                    view: "datatable",
                    select: true,
                    editable: false,
                    disable: true,
                    data: [
                        { chon: true, MA: "SYS", TEN_DM: "Bảng hệ thống" },
                        { chon: true, MA: "BQ", TEN_DM: "Bản Quyền" },
                        { chon: true, MA: "CKS", TEN_DM: "Chữ ký số" },
                        { chon: true, MA: "TBH", TEN_DM: "Thông báo hủy" },
                        { chon: true, MA: "QTC", TEN_DM: "Quyền truy cập" },
                        { chon: true, MA: "LDC", TEN_DM: "Loại điều chỉnh" }
                    ],
                    columns: [
                        {
                            id: "chon",
                            header: { content: "masterCheckbox" },
                            template: "{common.checkbox()}",
                            checkValue: true,
                            uncheckValue: false,
                            value: true,
                            width: 50,
                            css: { 'text-align': 'right' }
                        },
                        {
                            id: "MA",
                            header: _("MA"),
                            width: 80
                        },
                        {
                            id: "TEN_DM",
                            header: _("Tên Bảng"),
                            width: 250
                        }
                    ]
                }
            },
            {
                header: "Stored Procedures",
                body: {
                    id: "tblDMCT",
                    view: "datatable",
                    select: true,
                    editable: true,
                    data: [
                        { chon: true, MA_CT: "MNU", TEN_CT: "Get Menu" },
                        { chon: true, MA_CT: "DHB", TEN_CT: "Dashboard" },
                        { chon: true, MA_CT: "CHA", TEN_CT: "Biểu đồ" },
                        { chon: true, MA_CT: "PRI", TEN_CT: "Print Invoice" },
                        { chon: true, MA_CT: "PRL", TEN_CT: "Print License" },
                        { chon: true, MA_CT: "TEM", TEN_CT: "Temple Email" },
                        { chon: true, MA_CT: "TSM", TEN_CT: "Temple SMS" },
                        { chon: true, MA_CT: "SEI", TEN_CT: "Search Invoice" },
                        { chon: true, MA_CT: "ATC", TEN_CT: "Auto Create CMD" },
                        { chon: true, MA_CT: "ATF", TEN_CT: "Auto Create Field" }
                    ],
                    columns: [
                        {
                            id: "chon",
                            header: { content: "masterCheckbox" },
                            template: "{common.checkbox()}",
                            checkValue: true,
                            uncheckValue: false,
                            value: true,
                            width: 50,
                            css: { 'text-align': 'right' }
                        },
                        {
                            id: "MA_CT",
                            header: _("Mã"),
                            width: 80
                        },
                        {
                            id: "TEN_CT",
                            header: _("Tên Stored Procedures"),
                            width: 250
                        }

                    ]
                }
            }
        ]
    };


    var layout = {
        // paddingY: 60,
        // paddingX: 500,
        cols: [
            { gravity: 0.5, template: "" },
            {
                rows: [
                    header,
                    { gravity: 0.1, template: "" },
                    {
                        gravity: 0.1,
                        template: "<div><center><b style='color:#e60000'><i class='fa fa-globe'></i> CẬP NHẬT PHẦN MỀM LÊN PHIÊN BẢN OLNINE (WEB)</b></center></div>", css: { "padding-top": "15px" }
                    },
                    {
                        view: "form",
                        id: "formInfo",
                        paddingY: 10,
                        // width: 700,
                        // height:300,
                        position: "center",
                        paddingX: 10,
                        complexData: true,
                        margin: 0,
                        //  elementsConfig: {
                        //         labelPosition: "top",
                        //         //validateEvent: "key"
                        //     },
                        url: app.config.host + "/System/GetAllDmDvcs",
                        elements: [
                            {
                                cols: [
                                    {
                                        id: "ten_dvcs",
                                        name: "ten_dvcs",
                                        view: "text",
                                        label: "Tên đơn vị",
                                        readonly: true,
                                        css: "mcls_readonly_text"
                                    },
                                    {
                                        id: "ms_thue",
                                        name: "ms_thue",
                                        view: "text",
                                        label: "Mã số thuế",
                                        readonly: true,
                                        css: "mcls_readonly_text"
                                    }
                                ]
                            },
                            {
                                cols: [
                                    {
                                        id: "dia_chi",
                                        view: "text",
                                        name: "dia_chi",
                                        label: "Địa chỉ",
                                        readonly: true,
                                        css: "mcls_readonly_text"
                                    },
                                    {
                                        id: "tk",
                                        name: "gd",
                                        view: "checkbox",
                                        value: 1,
                                        label: "Ghi đè"
                                    }
                                ]
                            },
                            {
                                view: "template",
                                template: "Lựa chọn Bảng/Stored Procedures Update",
                                type: "section"
                            },
                            tabview,
                            {
                                margin: 10,
                                cols: controls
                            }
                        ]
                    },
                    { gravity: 0.5, template: "" }
                ]
            },
            { gravity: 0.5, template: "" }
        ]

    };

    return {
        $ui: layout
    };
});