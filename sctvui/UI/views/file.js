define(["app", "locale", "views/forms/file"], function (app, _, fileform) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Danh sách file báo cáo", details: "Danh sách file báo cáo" });

    var controls = [
        {
            id: "btnRefresh",
            view: "button",
            type: "icon",
            icon: "fas fa-refresh",
            label: _("REFRESH"),
            width: 95,
            // shortcut: "F4",
            css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    table.clearAll();
                    //console.log(table);
                    table.load(table.config.url);
                    //table.refresh();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDowload",
            view: "button",
            type: "icon",
            icon: "fas fa-download",
            label: _("Tải File"),
            width: 90,
            // shortcut: "F4",
            css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();
                    if (item == null) {
                        webix.message("Bạn chưa chọn file cần báo cáo !", "error");
                        return;
                    }
                    var box = webix.modalbox({
                        title: "Đang tải xuống",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    webix
                        .ajax()
                        .response("arraybuffer")
                        //.headers({ 'Content-type': 'application/json' })
                        .get(app.config.host + "/Invoice/Floder?filename=" + item.fName, {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                alert(text);
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "text/csv" });
                                webix.html.download(file, item.fName + ".repx");

                                webix.modalbox.hide(box);
                            },
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        // {
        //     id: "btnTest",
        //     view: "button",
        //     type: "icon",
        //     icon: "refresh",
        //     label: _("TEst"),
        //     width: 90,
        //     // shortcut: "F4",
        //     //css:"button_raised",
        //     on: {
        //         onClick: function () {
        //             var detailForm = this.$scope.ui(fileform.$ui);
        //             detailForm.show();

        //         }
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     }
        // },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                datafetch: 100,
                resizeColumn: true,
                //loadahead: 15,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var windowno = app.path[1].params[0];
                        var url = app.config.host + "/Invoice/FloderInfo";

                        webix.ajax(url, callback, view);
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.index = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnDowload");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    { id: "index", header: "", sort: "int", width: 40 },
                    { id: "fName", header: _("Tên file"), width: 300 },
                    //{ id: "fExtension", header: "Loại", width: 100 },
                    {
                        id: "fTime",
                        header: _("Ngày"),
                        width: 100,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
