define(["app", "locale", "models/user", "views/forms/68_dangkysudung", "views/forms/68_hoadon_history"], function (app, _, user, dangkysudung, hoadonHistory) {
    "use strict";

    webix.$$("title").parse({
        title: "Đăng ký/ Thay đổi thông tin SD HDDT",
        details: "Đăng ký/ Thay đổi thông tin SD HDDT",
    });

    var initUI = function () {
        //var windowno = app.path[1].params[0];
        var table = webix.$$("dangkysudung01Views");
        // table.disable();
        // table.showProgress();
        // table.clearAll();
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var url = app.config.host + "/Invoice68/GetDataDkysdung01";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("dangkysudung01Views");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dangkysudung.$ui);
                    dangkysudung.setEditMode(1);

                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var table = webix.$$("dangkysudung01Views");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu đăng ký cần sửa ! ", "error");
                        return;
                    }
                    // if (selectedItem.trang_thai == 1) {
                    //     webix.message("Đã gửi CQT không thể sửa", "error");
                    //     return;
                    // }
                    var detailForm = this.$scope.ui(dangkysudung.$ui);

                    dangkysudung.setEditMode(2);
                    dangkysudung.setValues(selectedItem);

                    if (selectedItem.trang_thai == 1) {
                        var form = $$("dangkysudung").elements;
                        for (var key in form) {
                            var $el = form[key];
                            $el.define("readonly", "true");
                            $el.define("css", "mcls_readonly_text");
                            $el.refresh();
                        }

                        // $$("btnDKSDSave").destructor();
                        $$("btnDKSDSave1").destructor();
                    }

                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dangkysudung01Views");
                    var selectedItem = table.getSelectedItem();

                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu đăng ký ! ", "error");
                        return;
                    }

                    if (selectedItem.trang_thai == 1) {
                        webix.message("Thông báo đã được ký và gửi CQT. Bạn không thể thực hiện chức năng này ! ", "error");
                        return;
                    }
                    selectedItem.editmode = 3;
                    webix.confirm({
                        text: "Bạn có muốn xóa bản ghi",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/deleteMau01", selectedItem, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.error, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            table.remove(selectedItem.id);
                                            table.enable();
                                            table.hideProgress();
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 150,
            label: '<i class="fa fa-eye fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Ký và gửi CQT (F7)") + "</span>",
            id: "btnPreview",
            shortcut: "F7",
            on: {
                onClick: function () {
                    var tableDK = webix.$$("dangkysudung01Views");
                    var selectedItem = tableDK.getSelectedItem();

                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn mẫu đăng ký ! ", "error");
                        return;
                    }
                    if (selectedItem.trang_thai == 1) {
                        webix.message("Mẫu đã được ký và gửi đi, bạn không thể ký lại ! ", "error");
                        return;
                    }
                    webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                            var result = JSON.parse(text);
                            var kytaptrung = "";
                            for (var i = 0; i < result.length; i++) {
                                var item = result[i];
                                kytaptrung = item.value;
                            }

                            webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                var listcerts = JSON.parse(text);
                                if (listcerts.length > 0) {
                                    var box = webix.modalbox({
                                        title: "Chọn chứng thư số",
                                        buttons: ["Nhận", "Hủy bỏ"],
                                        text: "<div id='ctnCerts'></div>",
                                        top: 100,
                                        width: 750,
                                        callback: function (result) {
                                            switch (result) {
                                                case "1":
                                                    webix.modalbox.hide();
                                                    break;
                                                case "0":
                                                    var table = $$("tblCerts");
                                                    var data = table.serialize();

                                                    var cert = null;

                                                    for (var i = 0; i < data.length; i++) {
                                                        if (data[i].chon == true) {
                                                            cert = data[i];
                                                            break;
                                                        }
                                                    }

                                                    if (cert == null) {
                                                        webix.message("Bạn chưa chọn chứng thư số");
                                                        return;
                                                    }
                                                    var postData = {
                                                        branch_code: user.getDvcsID(),
                                                        username: user.getCurrentUser(),
                                                        cer_serial: cert.cer_serial,
                                                        mau01_id: selectedItem.id,
                                                    };

                                                    if (postData.mau01_id == undefined || postData.mau01_id == "") {
                                                        webix.message("Bạn chưa ghi dữ liệu, không thể ký và gửi đi !", "error");
                                                        return;
                                                    } else {
                                                        webix.extend(tableDK, webix.ProgressBar);
                                                        tableDK.disable();
                                                        tableDK.showProgress();
                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-type": "application/json" })
                                                            .post(app.config.host + "/Invoice68/SignDangkysudung", postData, {
                                                                error: function (text, data, XmlHttpRequest) {
                                                                    tableDK.enable();
                                                                    tableDK.hideProgress();
                                                                    // var json = JSON.parse(text);
                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                },
                                                                success: function (text, data, XmlHttpRequest) {
                                                                    var json = JSON.parse(text);
                                                                    if (json.hasOwnProperty("error")) {
                                                                        webix.message(json.error, "error");
                                                                        tableDK.enable();
                                                                        tableDK.hideProgress();
                                                                    } else {
                                                                        tableDK.enable();
                                                                        tableDK.hideProgress();
                                                                        app.callEvent("dataChangeDangkysudung68", [1, null]);

                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                    }
                                                                },
                                                            });

                                                        // return;
                                                    }
                                            }
                                        },
                                    });

                                    var tblMatHang = webix.ui({
                                        container: "ctnCerts",
                                        id: "tblCerts",
                                        view: "datatable",
                                        borderless: true,
                                        resizeColumn: true,
                                        height: 350,
                                        columns: [
                                            {
                                                id: "chon",
                                                header: _("CHON"),
                                                width: 65,
                                                checkValue: true,
                                                uncheckValue: false,
                                                template: "{common.checkbox()}",
                                            },
                                            {
                                                id: "cert_type",
                                                header: _("Loại"),
                                                width: 100,
                                            },
                                            {
                                                id: "cer_serial",
                                                header: _("SO_SERIAL"),
                                                width: 150,
                                            },
                                            {
                                                id: "subject_name",
                                                header: _("SUBJECTNAME"),
                                                width: 450,
                                            },
                                            {
                                                id: "begin_date",
                                                header: _("NGAY_BAT_DAU"),
                                                width: 200,
                                            },
                                            {
                                                id: "end_date",
                                                header: _("NGAY_KET_THUC"),
                                                width: 200,
                                            },
                                            {
                                                id: "issuer",
                                                header: _("DONVI"),
                                                width: 450,
                                            },
                                        ],
                                        url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                    });
                                } else {
                                    var table = $$("dangkysudung01Views");
                                    webix.extend(table, webix.ProgressBar);
                                    table.disable();
                                    table.showProgress();
                                    // xem user có tích checkbox ký HSM không?
                                    var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                                    webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                        if (resultscheckSigHSM[0].json() == "C") {
                                            // ký HSM
                                            var postData = {
                                                branch_code: user.getDvcsID(),
                                                username: user.getCurrentUser(),
                                                mau01_id: selectedItem.id,
                                            };

                                            var url = app.config.host + "/Invoice68/SignDangkysudung_HSM";

                                            webix
                                                .ajax()
                                                .headers({ "Content-Type": "application/json" })
                                                .post(url, postData, function (text) {
                                                    var res = JSON.parse(text);

                                                    if (res.hasOwnProperty("error")) {
                                                        webix.message({ type: "error", text: res.error });
                                                    } else {
                                                        webix.message("Đã thực hiện xong");
                                                    }
                                                    table.enable();
                                                    table.hideProgress();
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                });
                                        } else {
                                            var a = webix.ajax(app.config.host + "/Invoice68/XmlMau01?id=" + selectedItem.id);
                                            var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                            webix.promise.all([a, b, selectedItem]).then(function (results) {
                                                // webix.extend(tblInvoice, webix.ProgressBar);
                                                // tblInvoice.disable();
                                                // tblInvoice.showProgress();
                                                var json = results[0].json();

                                                if (json.hasOwnProperty("error")) {
                                                    webix.message({ type: "error", text: json.error });
                                                }

                                                var listcerts = results[1].json();
                                                var item = results[2];

                                                if (listcerts.length == 0) {
                                                    table.enable();
                                                    table.hideProgress();
                                                    webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                    return;
                                                }

                                                require(["views/modules/minvoiceplugin"], function (plugin) {
                                                    var hub = plugin.hub();
                                                    var hubconnect = plugin.conn();
                                                    if (hubconnect.state != 1) {
                                                        table.enable();
                                                        table.hideProgress();
                                                        webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                        return;
                                                    }
                                                    hub.off("resCommand");
                                                    hub.on("resCommand", function (result) {
                                                        var json = JSON.parse(result);

                                                        if (json.hasOwnProperty("error")) {
                                                            webix.message({ type: "error", text: json.error });
                                                        } else {
                                                            var parameters = {
                                                                xml: json.xml,
                                                                id: item.id,
                                                            };

                                                            webix
                                                                .ajax()
                                                                .headers({ "Content-Type": "application/json" })
                                                                .post(app.config.host + "/Invoice68/SaveXmlMau01", parameters, function (text) {
                                                                    var result = JSON.parse(text);
                                                                    if (result.hasOwnProperty("error")) {
                                                                        webix.message(result.error, "error");
                                                                    } else {
                                                                        app.callEvent("dataChangeDangkysudung68", [1, null]);

                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                    }
                                                                });
                                                        }
                                                        table.enable();
                                                        table.hideProgress();
                                                    });

                                                    var _listcerts = [];

                                                    for (var i = 0; i < listcerts.length; i++) {
                                                        var _cert = listcerts[i];

                                                        _listcerts.push({
                                                            so_serial: _cert.cer_serial,
                                                            ngaybatdau: _cert.begin_date,
                                                            ngayketthuc: _cert.end_date,
                                                            issuer: _cert.issuer,
                                                            subjectname: _cert.subject_name,
                                                        });
                                                    }

                                                    var arg = {
                                                        idData: "#data",
                                                        xml: json["xml"],
                                                        shdon: "",
                                                        idSigner: "seller",
                                                        tagSign: "NNT",
                                                        listcerts: _listcerts,
                                                    };

                                                    hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                        .done(function () {
                                                            console.log("Success !");
                                                        })
                                                        .fail(function (error) {
                                                            webix.message(error, "error");
                                                        });
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "dangkysudung01Views",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function (obj) {
                        // if (obj.date_new) {
                        //     obj.date_new = new Date(obj.date_new);
                        // }
                    },
                },
                onClick: {
                    dowloadXmlDKSD: function (event, cell, target) {
                        var win = webix.$$("dangkysudung01Views");
                        var record = win.getItem(cell.row);
                        if (record.trang_thai != 1) {
                            webix.message("Mẫu chưa được ký không thể tải !", "error");
                            return;
                        }
                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();

                        var url = app.config.host + "/Invoice68/ExportXMLMau01?id=" + cell.row;
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .get(url, function (text, data) {
                                win.enable();
                                win.hideProgress();

                                var file = new Blob([data], { type: "application/xml" });
                                webix.html.download(file, "Mau01.xml");
                                // var fileURL = URL.createObjectURL(file);
                                // window.open(fileURL, "_blank");
                            });
                        // webix.message("Delete row: " + cell.row);
                        // return false; // here it blocks the default behavior
                    },
                    btndowloadXMLRep: function (event, cell, target) {
                        var win = webix.$$("dangkysudung01Views");
                        var record = win.getItem(cell.row);
                        if (record.ket_qua == null || record.ket_qua == "") {
                            webix.message("Mẫu chưa được CQT trả về không thể tải !", "error");
                            return;
                        }
                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();

                        var url = app.config.host + "/Invoice68/ExportXMLMau02?id=" + cell.row;
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .get(url, function (text, data) {
                                win.enable();
                                win.hideProgress();

                                var file = new Blob([data], { type: "application/xml" });
                                webix.html.download(file, "Mau02.xml");
                                // var fileURL = URL.createObjectURL(file);
                                // window.open(fileURL, "_blank");
                            });
                        // webix.message("Delete row: " + cell.row);
                        // return false; // here it blocks the default behavior
                    },
                    btndowloadXMLRepTiepNhan: function (event, cell, target) {
                        var win = webix.$$("dangkysudung01Views");
                        var record = win.getItem(cell.row);
                        if (record.ketqua_tiepnhan == null || record.ketqua_tiepnhan == "") {
                            webix.message("Mẫu chưa được CQT trả về không thể tải !", "error");
                            return;
                        }
                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();

                        var url = app.config.host + "/Invoice68/ExportXMLMau03?id=" + cell.row;
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .get(url, function (text, data) {
                                win.enable();
                                win.hideProgress();

                                var file = new Blob([data], { type: "application/xml" });
                                webix.html.download(file, "Mau03.xml");
                                // var fileURL = URL.createObjectURL(file);
                                // window.open(fileURL, "_blank");
                            });
                        // webix.message("Delete row: " + cell.row);
                        // return false; // here it blocks the default behavior
                    },
                    InMau01: function (event, cell, target) {
                        var table = webix.$$("dangkysudung01Views");
                        var item = table.getItem(cell.row);

                        //webix.message("Chưa có mẫu in ...", "error");
                        // var rpParameter = null;
                        var box = webix.modalbox({
                            title: "Đang in hóa đơn ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        var newWin = window.open();
                        var html =
                            "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                            '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                            '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                            '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                        newWin.document.write(html);

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inMau01?id=" + item.id + "&type=PDF", {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    },
                    InMau02: function (event, cell, target) {
                        var table = webix.$$("dangkysudung01Views");
                        var item = table.getItem(cell.row);

                        if (item.ket_qua == 3) {
                            webix.message("Chưa có mẫu in ...", "error");
                            return;
                        }

                        var box = webix.modalbox({
                            title: "Đang in hóa đơn ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        var newWin = window.open();
                        var html =
                            "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                            '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                            '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                            '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                        newWin.document.write(html);

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inMau02?id=" + item.id + "&type=PDF", {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    },
                    InMau03: function (event, cell, target) {
                        var table = webix.$$("dangkysudung01Views");
                        var item = table.getItem(cell.row);

                        //webix.message("Chưa có mẫu in ...", "error");
                        // var rpParameter = null;
                        var box = webix.modalbox({
                            title: "Đang in hóa đơn ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        var newWin = window.open();
                        var html =
                            "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                            '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                            '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                            '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                        newWin.document.write(html);

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inMau03?id=" + item.id + "&type=PDF", {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    },
                    ShowHistory: function (event, cell, target) {
                        var table = webix.$$("dangkysudung01Views");
                        var win = table.$scope.ui(hoadonHistory.$ui);
                        var hdon_id = cell.row;
                        hoadonHistory.setEditMode(hdon_id);
                        win.show();
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onBeforeLoad: function () {
                        this.showOverlay("<p><i style='font-size:450%;' class='fa fa-spin fa-spinner fa-pulse'></i></p>");
                    },
                    onAfterLoad: function (id) {
                        this.hideOverlay();
                    },
                },
                columns: [
                    {
                        id: "stt",
                        header: [{ text: "Mẫu 01ĐKTK/HĐĐT - Tờ khai Đăng ký/thay đổi thông tin sử dụng HĐĐT", colspan: "6" }, "STT"],
                        width: 40,
                    },
                    // {
                    //     id: "mtdiep_gui",
                    //     header: ["", "Mã thông điệp"],
                    //     width: 350,
                    // },
                    {
                        id: "ngay_gui",
                        header: ["", "Ngày gửi"],
                        width: 200,
                        css: { "text-align": "center" },
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    {
                        id: "noi_dung",
                        header: ["", "Nội dung"],
                        width: 250,
                    },
                    {
                        id: "btnMessagetn",
                        header: ["", "Lịch sử xử lý"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            return "<span class='ShowHistory' style='color: #00b300'><i class='fa fa-eye'></i></span> ";
                        },
                        width: 180,
                    },
                    {
                        id: "btndowload",
                        header: ["", "Tải về"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.trang_thai == 1) {
                                return "<span class='dowloadXmlDKSD' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            } else return "<span class='dowloadXmlDKSD' style='color: #c8c8cc'><i class='fas fa-download'></i></span> ";
                        },
                        width: 60,
                    },
                    {
                        id: "btnInMau01",
                        header: ["", "In"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.trang_thai == 1) {
                                return "<span class='InMau01' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                            } else return "<span class='InMau01' style='color: #c8c8cc'><i class='fas fa-print'></i></span> ";
                        },
                        width: 60,
                    },
                    {
                        id: "nguoi_gui",
                        header: ["", "Người gửi"],
                        width: 200,
                    },
                    {
                        id: "ketqua_tiepnhan",
                        header: [{ text: "Mẫu 01/TB-TNĐT - Thông tin tiếp nhận tờ khai của Cơ quan Thuế", colspan: "5" }, "Kết quả"],
                        width: 300,
                        template: function (obj) {
                            if (obj.ketqua_tiepnhan == 1) {
                                return "<span style='color: #00b300'>Tiếp nhận tờ khai đăng ký</span> ";
                            } else {
                                if (obj.ketqua_tiepnhan == 2) {
                                    return "<span style='color: #e83717'>Không tiếp nhận tờ khai đăng ký</span> ";
                                } else {
                                    if (obj.ketqua_tiepnhan == 3) {
                                        return "<span style='color: #00b300'>Tiếp nhận tờ khai đăng ký thay đổi</span> ";
                                    } else {
                                        if (obj.ketqua_tiepnhan == 4) {
                                            return "<span style='color: #00b300'>Không tiếp nhận tờ khai đăng ký thay đổi</span> ";
                                        } else {
                                            if (obj.ketqua_tiepnhan == 0) {
                                                return "<span style='color: #00b300'>Không tiếp nhận tờ khai đăng ký thay đổi</span> ";
                                            } else {
                                                return "<span></span>";
                                            }
                                        }
                                    }
                                }
                            }
                        },
                    },
                    {
                        id: "lydo_tiepnhan",
                        header: ["", "Lý do"],
                        width: 300,
                    },
                    {
                        id: "ngay_tbao_tiepnhan",
                        header: ["", "Ngày thông báo"],
                        width: 200,
                        css: { "text-align": "center" },
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    {
                        id: "btndowloadXMLRepTiepNhan",
                        header: ["", "Tải về"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.ketqua_tiepnhan != null && obj.ketqua_tiepnhan != "") {
                                return "<span class='btndowloadXMLRepTiepNhan' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            } else return "<span class='btndowloadXMLRepTiepNhan' style='color: #c8c8cc'><i class='fas fa-download'></i></span> ";
                        },
                        width: 60,
                    },
                    {
                        id: "btnInMau03",
                        header: ["", "In"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.ketqua_tiepnhan != null && obj.ketqua_tiepnhan != "") {
                                return "<span class='InMau03' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                            } else return "<span class='InMau03' style='color: #c8c8cc'><i class='fas fa-print'></i></span> ";
                        },
                        width: 60,
                    },
                    // {
                    //     id: "mtdiep_tiepnhan_cqt",
                    //     header: ["", "Mã thông điệp CQT"],
                    //     width: 350,
                    // },
                    {
                        id: "ket_qua",
                        header: [{ text: "Mẫu 01/TB-ĐKĐT - Thông tin chấp nhận Tờ khai của Cơ quan Thuế", colspan: "5" }, "Kết quả"],
                        width: 200,
                        template: function (obj) {
                            if (obj.ket_qua == 1) {
                                return "<span style='color: #00b300'>Chấp thuận</span> ";
                            } else {
                                if (obj.ket_qua == 2) {
                                    return "<span style='color: #e83717'>Không chấp thuận</span> ";
                                } else {
                                    if (obj.ket_qua == 3) {
                                        return "<span style='color: #00b300'>Đã phản hồi kết quả</span> ";
                                    } else {
                                        return "<span>Chờ phản hồi</span>";
                                    }
                                }
                            }
                        },
                    },
                    {
                        id: "ly_do",
                        header: ["", "Lý do"],
                        width: 300,
                    },
                    {
                        id: "ngay_tbao",
                        header: ["", "Ngày thông báo"],
                        width: 200,
                        css: { "text-align": "center" },
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    {
                        id: "btndowloadXMLRep",
                        header: ["", "Tải về"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.ket_qua != null && obj.ket_qua != "") {
                                return "<span class='btndowloadXMLRep' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            } else return "<span class='btndowloadXMLRep' style='color: #c8c8cc'><i class='fas fa-download'></i></span> ";
                        },
                        width: 60,
                    },
                    {
                        id: "btnInMau02",
                        header: ["", "In"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.ket_qua != null && obj.ket_qua != "") {
                                return "<span class='InMau02' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                            } else return "<span class='InMau02' style='color: #c8c8cc'><i class='fas fa-print'></i></span> ";
                        },
                        width: 60,
                    },
                    // {
                    //     id: "mtdiep_cqt",
                    //     header: ["", "Mã thông điệp CQT"],
                    //     width: 350,
                    // },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",

                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };

    function loadData() {
        var table = webix.$$("dangkysudung01Views");
        table.clearAll();
        table.load(table.config.url);
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var table = webix.$$("dangkysudung01Views");
            webix.extend(view, webix.ProgressBar);
            $scope.on(app, "dataChangeDangkysudung68", function (mode, item) {
                if (mode == 1) {
                    table.clearAll();
                    table.load(table.config.url);
                    // console.log(item);
                    // console.log(item.ten_loai);
                    // table.add(item);
                    //table.select(item.id,false);
                }

                if (mode == 2) {
                    table.clearAll();
                    table.load(table.config.url);
                    // var selectedItem = table.getSelectedItem();
                    // table.updateItem(selectedItem.id, item);

                    // webix.UIManager.setFocus($$(table.config.id));
                }
            });

            $scope.on(app, "filterTable", function (report_id, infoWindowId, item) {
                var infoparam = {
                    infowindow_id: infoWindowId,
                    parameter: item,
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
