﻿define(["app", "locale", "models/user"], function (app, _, user) {
    var formatSoDefault = {
        groupDelimiter: ",",
        groupSize: 3,
        decimalDelimiter: ".",
        decimalSize: 2,
    };
    function f(a) {
        return webix.Number.format(a, formatSoDefault);
    }
    function g(a) {
        return webix.Number.parse(a, formatSoDefault);
    }
    var fmNumber = {
        parse: function (a) {
            return g(a);
        },
        edit: function (a) {
            if (a !== undefined) return f(a);
            return a;
        },
    };
    function CheckKhacNull(value) {
        if (value === undefined || value === null || value === "") return false;
        else return true;
    }
    return {
        $ui: {},

        clear_filter: function (table) {
            table.eachColumn(function (id, col) {
                var filter = this.getFilter(id);
                if (filter) {
                    if (filter.setValue) filter.setValue(""); // suggest-based filters
                    else filter.value = ""; // html-based: select & text
                }
            });
        },
        validate_HD_ReturnListHdonId: function () {
            var tableHdon = webix.$$("invoiceViews");
            var datafrmAll = tableHdon.serialize();
            // get list được chọn
            var lsthdon_id = [];

            var lstDataChoose = datafrmAll
                .filter(function (p) {
                    return p != undefined;
                })
                .filter(function (p) {
                    return p.chon == true;
                });

            if (lstDataChoose.length == 0) {
                webix.message("Vui lòng chọn hóa đơn để xử lý!", "error");
                lsthdon_id = [];
            } else {
                if (
                    lstDataChoose.filter(function (p) {
                        return p.tthai != "Chờ ký";
                    }).length > 0
                ) {
                    webix.message("Danh sách đã chọn tồn tại hóa đơn không ở trạng thái chờ ký !", "error");
                    lsthdon_id = [];
                } else {
                    lsthdon_id = $.map(lstDataChoose, function (item) {
                        return item.hdon_id;
                    });
                }
            }
            return lsthdon_id;
        },

        validate_HD_ReturnListHdonKhongMaVaCoMaId: function () {
            var tableHdon = webix.$$("invoiceViews");
            var datafrmAll = tableHdon.serialize();
            // get list được chọn
            var lsthdon_id_coMa = [];
            var lsthdon_id_khongMa = [];

            var lstDataChoose = datafrmAll
                .filter(function (p) {
                    return p != undefined;
                })
                .filter(function (p) {
                    return p.chon == true;
                });
            var lstDataChooseKhongRoMa = datafrmAll
                .filter(function (p) {
                    return p != undefined;
                })
                .filter(function (p) {
                    return p.chon == true && p.is_hdcma === null;
                });

            var lstDataChooseCoMa = datafrmAll
                .filter(function (p) {
                    return p != undefined;
                })
                .filter(function (p) {
                    return p.chon == true && p.is_hdcma === 1;
                });
            var lstDataChooseKhongMa = datafrmAll
                .filter(function (p) {
                    return p != undefined;
                })
                .filter(function (p) {
                    return p.chon == true && p.is_hdcma === 0;
                });

            if (lstDataChoose.length === 0) {
                webix.message("Vui lòng chọn hóa đơn để xử lý!", "error");
                lsthdon_id = [];
                lsthdon_id_coMa = [];
                lsthdon_id_khongMa = [];
            } else {
                if (
                    lstDataChooseCoMa.filter(function (p) {
                        return p.tthai !== "Chờ ký";
                    }).length > 0 ||
                    lstDataChooseKhongMa.filter(function (p) {
                        return p.tthai !== "Chờ ký";
                    }).length > 0 ||
                    lstDataChooseKhongRoMa.length > 0
                ) {
                    if (lstDataChooseKhongRoMa.length > 0) {
                        webix.message("Danh sách đã chọn tồn tại " + lstDataChooseKhongRoMa.length + " hóa đơn không rõ trạng thái mã !", "error");
                        lsthdon_id = [];
                        lsthdon_id_coMa = [];
                        lsthdon_id_khongMa = [];
                    } else {
                        webix.message("Danh sách đã chọn tồn tại hóa đơn không ở trạng thái chờ ký !", "error");
                        lsthdon_id = [];
                        lsthdon_id_coMa = [];
                        lsthdon_id_khongMa = [];
                    }
                } else {
                    lsthdon_id_coMa = $.map(lstDataChooseCoMa, function (item) {
                        return {
                            hdon_id: item.hdon_id,
                            is_hdcma: item.is_hdcma,
                        };
                    });
                    lsthdon_id_khongMa = $.map(lstDataChooseKhongMa, function (item) {
                        return {
                            hdon_id: item.hdon_id,
                            is_hdcma: item.is_hdcma,
                        };
                    });
                }
            }
            return [lsthdon_id_coMa, lsthdon_id_khongMa];
        },
        ExcuteHoaDon: function (lsthdon_id, checkGuiCQT, checkHasCode) {
            checkHasCode = checkHasCode || false;
            var tableHdon = webix.$$("invoiceViews");
            var ui = tableHdon.getTopParentView();
            webix
                .ajax()
                .headers({ "Content-Type": "application/json" })
                .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                    var result = JSON.parse(text);
                    var kytaptrung = "";
                    for (var i = 0; i < result.length; i++) {
                        var item = result[i];
                        kytaptrung = item.value;
                    }

                    webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                        var listcerts = JSON.parse(text);
                        if (listcerts.length > 0) {
                            var box = webix.modalbox({
                                title: "Chọn chứng thư số",
                                buttons: ["Nhận", "Hủy bỏ"],
                                text: "<div id='ctnCerts'></div>",
                                top: 100,
                                width: 750,
                                callback: function (result) {
                                    switch (result) {
                                        case "1":
                                            webix.modalbox.hide();
                                            break;
                                        case "0":
                                            var table = $$("tblCerts");
                                            var data = table.serialize();

                                            var cert = null;

                                            for (var i = 0; i < data.length; i++) {
                                                if (data[i].chon == true) {
                                                    cert = data[i];
                                                    break;
                                                }
                                            }

                                            if (cert == null) {
                                                webix.message("Bạn chưa chọn chứng thư số");
                                                return;
                                            }

                                            var postData = {
                                                branch_code: user.getDvcsID(),
                                                username: user.getCurrentUser(),
                                                cer_serial: cert.cer_serial,
                                                lsthdon_id: lsthdon_id,
                                                type_cmd: checkHasCode == true ? 200 : 203,
                                            };

                                            if (!checkGuiCQT) {
                                                postData.guiCQT = "NotSend";
                                            }

                                            webix.extend(tableHdon, webix.ProgressBar);
                                            tableHdon.disable();
                                            ui.disable();
                                            tableHdon.showProgress();

                                            var dataPut = { data: [postData] };
                                            webix
                                                .ajax()
                                                .headers({ "Content-Type": "application/json" })
                                                .post(app.config.host + "/Invoice68/SignInvoiceCertFile68", dataPut, function (text) {
                                                    var result = JSON.parse(text);
                                                    var table = webix.$$("invoiceViews");
                                                    table.enable();
                                                    ui.enable();
                                                    table.hideProgress();
                                                    if (result.hasOwnProperty("error")) {
                                                        webix.message({ type: "error", text: result.error });
                                                        return;
                                                    }

                                                    if (result.ok == true) {
                                                        //    if (result.tthai == "Đã ký") {
                                                        //        webix
                                                        //            .ajax()
                                                        //            .headers({ "Content-type": "application/json" })
                                                        //            .post(
                                                        //                app.config.host + "/System/ExecuteCommand",
                                                        //                {
                                                        //                    command: "CM00025",
                                                        //                    parameter: {},
                                                        //                },
                                                        //                function (text) {
                                                        //                    var json = JSON.parse(text);
                                                        //                    // send email ma = 'AUTO_SEND_EMAIL'
                                                        //                    if (json[0].value == "C") {
                                                        //                        var data = {
                                                        //                            lsthdon_id: lsthdon_id,
                                                        //                            nguoinhan: result.email,
                                                        //                            tieude: "Thông báo xuất hóa đơn điện tử",
                                                        //                        };

                                                        //                        webix
                                                        //                            .ajax()
                                                        //                            .headers({ "Content-type": "application/json" })
                                                        //                            .post(app.config.host + "/Invoice68/AutoSendInvoiceByEmail", data, function (text) {
                                                        //                                var data = JSON.parse(text);

                                                        //                                if (!data.hasOwnProperty("error")) {
                                                        //                                } else {
                                                        //                                    webix.message(data.error, "error");
                                                        //                                }
                                                        //                            });
                                                        //                    }
                                                        //                }
                                                        //            );

                                                        //        // webix.modalbox.hide();
                                                        //    }

                                                        var table = webix.$$("invoiceViews");
                                                        table.clearAll();
                                                        table.load(table.config.url);
                                                        webix.modalbox.hide();
                                                    }
                                                });

                                            return;
                                    }
                                },
                            });

                            var tblMatHang = webix.ui({
                                container: "ctnCerts",
                                id: "tblCerts",
                                view: "datatable",
                                borderless: true,
                                resizeColumn: true,
                                height: 350,
                                columns: [
                                    {
                                        id: "chon",
                                        header: _("CHON"),
                                        width: 65,
                                        checkValue: true,
                                        uncheckValue: false,
                                        template: "{common.checkbox()}",
                                    },
                                    {
                                        id: "cert_type",
                                        header: _("Loại"),
                                        width: 100,
                                    },
                                    {
                                        id: "cer_serial",
                                        header: _("SO_SERIAL"),
                                        width: 150,
                                    },
                                    {
                                        id: "subject_name",
                                        header: _("SUBJECTNAME"),
                                        width: 450,
                                    },
                                    {
                                        id: "begin_date",
                                        header: _("NGAY_BAT_DAU"),
                                        width: 200,
                                    },
                                    {
                                        id: "end_date",
                                        header: _("NGAY_KET_THUC"),
                                        width: 200,
                                    },
                                    {
                                        id: "issuer",
                                        header: _("DONVI"),
                                        width: 450,
                                    },
                                ],
                                url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                            });
                        } else {
                            var tblInvoice = webix.$$("invoiceViews");
                            webix.extend(tblInvoice, webix.ProgressBar);
                            tblInvoice.disable();
                            tblInvoice.showProgress();
                            ui.disable();
                            // xem user có tích checkbox ký HSM không?
                            var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                            webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                if (resultscheckSigHSM[0].json() == "C") {
                                    // ký HSM
                                    var postData = {
                                        branch_code: user.getDvcsID(),
                                        username: user.getCurrentUser(),
                                        lsthdon_id: lsthdon_id,
                                        type_cmd: checkHasCode == true ? 200 : 203,
                                    };

                                    if (!checkGuiCQT) {
                                        postData.guiCQT = "NotSend";
                                    }

                                    var url = app.config.host + "/Invoice68/SignListInvoiceCertFile_HSM";

                                    webix
                                        .ajax()
                                        .headers({ "Content-Type": "application/json" })
                                        .post(url, postData, function (text) {
                                            var res = JSON.parse(text);

                                            if (res.hasOwnProperty("error")) {
                                                webix.message({ type: "error", text: res.error });
                                            } else {
                                                webix.message("Đã thực hiện xong");
                                            }
                                            tblInvoice.enable();
                                            ui.enable();
                                            tblInvoice.hideProgress();
                                            var table = webix.$$("invoiceViews");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        });
                                } else {
                                    function excuteHdPlugin(k) {
                                        if (k < lsthdon_id.length) {
                                            var hdon_id = lsthdon_id[k];

                                            var a = webix.ajax(app.config.host + "/Invoice68/ExportInvoiceXml?id=" + hdon_id);
                                            var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                            webix.promise.all([a, b, hdon_id]).then(function (results) {
                                                var json = results[0].json();
                                                var idhoadon = results[2];
                                                if (json.hasOwnProperty("error")) {
                                                    webix.message({ type: "error", text: json.error });
                                                    tblInvoice.enable();
                                                    ui.enable();
                                                    tblInvoice.hideProgress();
                                                    var table = webix.$$("invoiceViews");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                    return;
                                                }
                                                if (json.InvoiceXmlData == "") {
                                                    webix.message({ type: "error", text: "Hóa đơn chưa có chi tiết. Vui lòng kiểm tra lại" });
                                                    tblInvoice.enable();
                                                    ui.enable();
                                                    tblInvoice.hideProgress();
                                                    var table = webix.$$("invoiceViews");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                    return;
                                                }
                                                var listcerts = results[1].json();

                                                if (listcerts.length == 0) {
                                                    webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                    tblInvoice.enable();
                                                    ui.enable();
                                                    tblInvoice.hideProgress();
                                                    var table = webix.$$("invoiceViews");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                    return;
                                                }

                                                require(["views/modules/minvoiceplugin"], function (plugin) {
                                                    var hub = plugin.hub();
                                                    var hubconnect = plugin.conn();
                                                    if (hubconnect.state != 1) {
                                                        tblInvoice.enable();
                                                        ui.enable();
                                                        tblInvoice.hideProgress();
                                                        webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                        return;
                                                    }
                                                    hub.off("resCommand");
                                                    hub.on("resCommand", function (result) {
                                                        var json = JSON.parse(result);
                                                        if (json.hasOwnProperty("error")) {
                                                            webix.message({ type: "error", text: json.error });
                                                            tblInvoice.enable();
                                                            ui.enable();
                                                            tblInvoice.hideProgress();
                                                            var table = webix.$$("invoiceViews");
                                                            table.clearAll();
                                                            table.load(table.config.url);
                                                            return;
                                                        } else {
                                                            var parameters = {
                                                                xml: json.xml,
                                                                xmlbangke: json.xmlbangke,
                                                                hdon_id: idhoadon,
                                                                // shdon: json.shdon,
                                                                username: user.getCurrentUser(),
                                                                type_cmd: checkHasCode == true ? 200 : 203,
                                                            };

                                                            if (!checkGuiCQT) {
                                                                parameters.guiCQT = "NotSend";
                                                            }

                                                            webix
                                                                .ajax()
                                                                .headers({ "Content-Type": "application/json" })
                                                                .post(app.config.host + "/Invoice68/SaveXml", parameters, function (text) {
                                                                    var result = JSON.parse(text);
                                                                    if (result.hasOwnProperty("error")) {
                                                                        webix.message(result.error, "error");
                                                                    } else {
                                                                        //var table = webix.$$("invoiceViews");
                                                                        //table.clearAll();
                                                                        //table.load(table.config.url);
                                                                    }
                                                                    k++;
                                                                    excuteHdPlugin(k);
                                                                });
                                                        }
                                                        //tblInvoice.enable();
                                                        //tblInvoice.hideProgress();
                                                    });

                                                    var _listcerts = [];

                                                    for (var i = 0; i < listcerts.length; i++) {
                                                        var _cert = listcerts[i];

                                                        _listcerts.push({
                                                            so_serial: _cert.cer_serial,
                                                            ngaybatdau: _cert.begin_date,
                                                            ngayketthuc: _cert.end_date,
                                                            issuer: _cert.issuer,
                                                            subjectname: _cert.subject_name,
                                                        });
                                                    }

                                                    var arg = {
                                                        idData: "#data",
                                                        xml: json["InvoiceXmlData"],
                                                        xmlbangke: json["InvoiceXmlBangKe"],
                                                        shdon: json["shdon"],
                                                        idSigner: "seller",
                                                        tagSign: "NBan",
                                                        dvky: json["dvky"],
                                                        listcerts: _listcerts,
                                                    };

                                                    hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                        .done(function () {
                                                            console.log("Success !");
                                                        })
                                                        .fail(function (error) {
                                                            webix.message(error, "error");
                                                        });
                                                });
                                            });
                                        } else {
                                            tblInvoice.enable();
                                            ui.enable();
                                            tblInvoice.hideProgress();
                                            var table = webix.$$("invoiceViews");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        }
                                    }
                                    excuteHdPlugin(0);
                                }
                            });
                        }
                    });
                });
        },
        ExcuteHoaDonForHasCodeAndNotHaveAnyCode: function (lsthdon_id, checkGuiCQT, checkHasCode) {
            checkHasCode = checkHasCode || false;
            var tableHdon = webix.$$("invoiceViews");
            var ui = tableHdon.getTopParentView();
            webix
                .ajax()
                .headers({ "Content-Type": "application/json" })
                .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                    var result = JSON.parse(text);
                    var kytaptrung = "";
                    for (var i = 0; i < result.length; i++) {
                        var item = result[i];
                        kytaptrung = item.value;
                    }

                    webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                        var listcerts = JSON.parse(text);
                        if (listcerts.length > 0) {
                            var box = webix.modalbox({
                                title: "Chọn chứng thư số",
                                buttons: ["Nhận", "Hủy bỏ"],
                                text: "<div id='ctnCerts'></div>",
                                top: 100,
                                width: 750,
                                callback: function (result) {
                                    switch (result) {
                                        case "1":
                                            webix.modalbox.hide();
                                            break;
                                        case "0":
                                            var table = $$("tblCerts");
                                            var data = table.serialize();

                                            var cert = null;

                                            for (var i = 0; i < data.length; i++) {
                                                if (data[i].chon == true) {
                                                    cert = data[i];
                                                    break;
                                                }
                                            }

                                            if (cert == null) {
                                                webix.message("Bạn chưa chọn chứng thư số");
                                                return;
                                            }

                                            var lsCoMa = lsthdon_id[0];
                                            var lsKhongMa = lsthdon_id[1];
                                            var dsHoaDon = lsCoMa.concat(lsKhongMa);

                                            var postData = {
                                                branch_code: user.getDvcsID(),
                                                username: user.getCurrentUser(),
                                                cer_serial: cert.cer_serial,
                                                lsthdon_id: dsHoaDon,
                                            };

                                            if (!checkGuiCQT) {
                                                postData.guiCQT = "NotSend";
                                            }

                                            webix.extend(tableHdon, webix.ProgressBar);
                                            tableHdon.disable();
                                            ui.disable();
                                            tableHdon.showProgress();

                                            var dataPut = { data: [postData] };
                                            webix
                                                .ajax()
                                                .headers({ "Content-Type": "application/json" })
                                                .post(app.config.host + "/Invoice68/SignInvoiceCertFile68ForHasCodeAndNotHaveAnyCode", dataPut, function (text) {
                                                    var result = JSON.parse(text);
                                                    var table = webix.$$("invoiceViews");
                                                    table.enable();
                                                    ui.enable();
                                                    table.hideProgress();
                                                    if (result.hasOwnProperty("error")) {
                                                        webix.message({ type: "error", text: result.error });
                                                        return;
                                                    }

                                                    if (result.ok == true) {
                                                        webix.message("Đã thực hiện xong");
                                                        var table = webix.$$("invoiceViews");
                                                        table.clearAll();
                                                        table.load(table.config.url);
                                                        webix.modalbox.hide();
                                                    }
                                                });

                                            return;
                                    }
                                },
                            });

                            var tblMatHang = webix.ui({
                                container: "ctnCerts",
                                id: "tblCerts",
                                view: "datatable",
                                borderless: true,
                                resizeColumn: true,
                                height: 350,
                                columns: [
                                    {
                                        id: "chon",
                                        header: _("CHON"),
                                        width: 65,
                                        checkValue: true,
                                        uncheckValue: false,
                                        template: "{common.checkbox()}",
                                    },
                                    {
                                        id: "cert_type",
                                        header: _("Loại"),
                                        width: 100,
                                    },
                                    {
                                        id: "cer_serial",
                                        header: _("SO_SERIAL"),
                                        width: 150,
                                    },
                                    {
                                        id: "subject_name",
                                        header: _("SUBJECTNAME"),
                                        width: 450,
                                    },
                                    {
                                        id: "begin_date",
                                        header: _("NGAY_BAT_DAU"),
                                        width: 200,
                                    },
                                    {
                                        id: "end_date",
                                        header: _("NGAY_KET_THUC"),
                                        width: 200,
                                    },
                                    {
                                        id: "issuer",
                                        header: _("DONVI"),
                                        width: 450,
                                    },
                                ],
                                url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                            });
                        } else {
                            var tblInvoice = webix.$$("invoiceViews");
                            webix.extend(tblInvoice, webix.ProgressBar);
                            tblInvoice.disable();
                            ui.disable();
                            tblInvoice.showProgress();

                            // xem user có tích checkbox ký HSM không?
                            var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                            webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                if (resultscheckSigHSM[0].json() == "C") {
                                    var lsCoMa = lsthdon_id[0];
                                    var lsKhongMa = lsthdon_id[1];
                                    var dsHoaDon = lsCoMa.concat(lsKhongMa);

                                    // ký HSM
                                    var postData = {
                                        branch_code: user.getDvcsID(),
                                        username: user.getCurrentUser(),
                                        lsthdon_id: dsHoaDon,
                                        AllHoaDon: "All",
                                    };

                                    if (!checkGuiCQT) {
                                        postData.guiCQT = "NotSend";
                                    }

                                    var url = app.config.host + "/Invoice68/SignListInvoiceCertFile_HSM";

                                    webix
                                        .ajax()
                                        .headers({ "Content-Type": "application/json" })
                                        .post(url, postData, function (text) {
                                            var res = JSON.parse(text);

                                            if (res.hasOwnProperty("error")) {
                                                webix.message({ type: "error", text: res.error });
                                            } else {
                                                webix.message("Đã thực hiện xong");
                                            }
                                            tblInvoice.enable();
                                            ui.enable();
                                            tblInvoice.hideProgress();
                                            var table = webix.$$("invoiceViews");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        });
                                } else {
                                    var lsCoMa = lsthdon_id[0];
                                    var lsKhongMa = lsthdon_id[1];
                                    var dsHoaDonXL = lsCoMa.concat(lsKhongMa);
                                    function excuteHdPlugin(k, dsHoaDon) {
                                        if (k < dsHoaDon.length) {
                                            // debugger;
                                            var hoadon = dsHoaDon[k];

                                            var a = webix.ajax(app.config.host + "/Invoice68/ExportInvoiceXml?id=" + hoadon.hdon_id);
                                            var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                            webix.promise.all([a, b, hoadon]).then(function (results) {
                                                var json = results[0].json();
                                                var item = results[2];

                                                if (item.is_hdcma !== 0 && item.is_hdcma !== 1) {
                                                    webix.message({ type: "error", text: "Kiểm tra lại loại hóa đơn" });
                                                    tblInvoice.enable();
                                                    ui.enable();
                                                    tblInvoice.hideProgress();
                                                    var table = webix.$$("invoiceViews");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                    return;
                                                }

                                                if (json.hasOwnProperty("error")) {
                                                    webix.message({ type: "error", text: json.error });
                                                    tblInvoice.enable();
                                                    ui.enable();
                                                    tblInvoice.hideProgress();
                                                    var table = webix.$$("invoiceViews");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                    return;
                                                }
                                                if (json.InvoiceXmlData == "") {
                                                    webix.message({ type: "error", text: "Hóa đơn chưa có chi tiết. Vui lòng kiểm tra lại" });
                                                    tblInvoice.enable();
                                                    ui.enable();
                                                    tblInvoice.hideProgress();
                                                    var table = webix.$$("invoiceViews");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                    return;
                                                }
                                                var listcerts = results[1].json();

                                                if (listcerts.length == 0) {
                                                    webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                    tblInvoice.enable();
                                                    ui.enable();
                                                    tblInvoice.hideProgress();
                                                    var table = webix.$$("invoiceViews");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                    return;
                                                }

                                                require(["views/modules/minvoiceplugin"], function (plugin) {
                                                    var hub = plugin.hub();
                                                    var hubconnect = plugin.conn();
                                                    if (hubconnect.state != 1) {
                                                        tblInvoice.enable();
                                                        tblInvoice.hideProgress();
                                                        webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                        return;
                                                    }
                                                    hub.off("resCommand");
                                                    hub.on("resCommand", function (result) {
                                                        var json = JSON.parse(result);
                                                        if (json.hasOwnProperty("error")) {
                                                            webix.message({ type: "error", text: json.error });
                                                            tblInvoice.enable();
                                                            ui.enable();
                                                            tblInvoice.hideProgress();
                                                            var table = webix.$$("invoiceViews");
                                                            table.clearAll();
                                                            table.load(table.config.url);
                                                            return;
                                                        } else {
                                                            var parameters = {
                                                                xml: json.xml,
                                                                xmlbangke: json.xmlbangke,
                                                                hdon_id: item.hdon_id,
                                                                // shdon: json.shdon,
                                                                username: user.getCurrentUser(),
                                                                type_cmd: item.is_hdcma === 1 ? 200 : 203,
                                                            };

                                                            if (!checkGuiCQT) {
                                                                parameters.guiCQT = "NotSend";
                                                            }

                                                            webix
                                                                .ajax()
                                                                .headers({ "Content-Type": "application/json" })
                                                                .post(app.config.host + "/Invoice68/SaveXml", parameters, function (text) {
                                                                    var result = JSON.parse(text);
                                                                    if (result.hasOwnProperty("error")) {
                                                                        webix.message(result.error, "error");
                                                                    } else {
                                                                        //var table = webix.$$("invoiceViews");
                                                                        //table.clearAll();
                                                                        //table.load(table.config.url);
                                                                    }
                                                                    k++;
                                                                    excuteHdPlugin(k, dsHoaDon);
                                                                });
                                                        }
                                                        //tblInvoice.enable();
                                                        //tblInvoice.hideProgress();
                                                    });

                                                    var _listcerts = [];

                                                    for (var i = 0; i < listcerts.length; i++) {
                                                        var _cert = listcerts[i];

                                                        _listcerts.push({
                                                            so_serial: _cert.cer_serial,
                                                            ngaybatdau: _cert.begin_date,
                                                            ngayketthuc: _cert.end_date,
                                                            issuer: _cert.issuer,
                                                            subjectname: _cert.subject_name,
                                                        });
                                                    }

                                                    var arg = {
                                                        idData: "#data",
                                                        xml: json["InvoiceXmlData"],
                                                        xmlbangke: json["InvoiceXmlBangKe"],
                                                        shdon: json["shdon"],
                                                        idSigner: "seller",
                                                        tagSign: "NBan",
                                                        dvky: json["dvky"],
                                                        listcerts: _listcerts,
                                                    };

                                                    hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                        .done(function () {
                                                            console.log("Success !");
                                                        })
                                                        .fail(function (error) {
                                                            webix.message(error, "error");
                                                        });
                                                });
                                            });
                                        } else {
                                            tblInvoice.enable();
                                            ui.enable();
                                            tblInvoice.hideProgress();
                                            var table = webix.$$("invoiceViews");
                                            table.clearAll();
                                            table.load(table.config.url);
                                        }
                                    }
                                    excuteHdPlugin(0, dsHoaDonXL);
                                }
                            });
                        }
                    });
                });
        },
        CommonOnclickTaskOfHD: function (self) {
            if ($$("kyhieu_id") != undefined) {
                if ($$("kyhieu_id").getPopup().getList().serialize().length > 0 ? true : false) self.callEvent("onClick");
                else webix.message("Không tồn tại ký hiệu!", "error");
            } else {
                webix.message("Không tồn tại ký hiệu!", "error");
            }
        },
        CommonOnclickTaskOfHDTT32: function (self) {
            self.callEvent("onClick");
        },

        CreatItemFromData: function (field, tab) {
            var format = field.format;
            var isFmNumber = format === "fmNumber";

            // field.format = isFmNumber === true ? null : format;

            var subCol = {
                id: field.column_name,
                header: field.caption != null ? _(field.caption) : _(field.column_name),
                editor: field.type_editor,
                hidden: field.hidden,
                value_change: field.value_change,
                isFmNumber: isFmNumber,
            };

            if (field.read_only == "C" || field.type_editor == "checkbox") {
                delete subCol.editor;
            }

            if (field.type_editor == "checkbox") {
                subCol.template = "{common.checkbox()}";
                subCol.checkValue = true;
                subCol.uncheckValue = false;
            }

            if (field.column_type == "date" || field.column_type == "datetime") {
                subCol.format = function (text) {
                    if (text != null && text != undefined) {
                        var format = webix.Date.dateToStr("%d/%m/%Y");
                        text = removeTimeZoneT(text);
                        return format(text);
                    }
                };
            }

            if (field.column_type == "numeric") {
                subCol.css = { "text-align": "right" };
                subCol.format = webix.Number.numToStr({
                    groupDelimiter: ",",
                    groupSize: 3,
                    decimalDelimiter: ".",
                    decimalSize: field.data_decimal,
                });

                if (field.type_editor == "autonumeric") {
                    if (field.data_decimal >= 0) {
                        var decimal = Array(field.data_decimal + 1).join("9");

                        subCol.numericConfig = {
                            maximumValue: Number("999999999999." + decimal),
                            minValue: Number("-999999999999." + decimal),
                            decimalPlaces: field.data_decimal,
                        };
                    }
                }
                // if (field.data_decimal === "fmNumber") subCol.format = fmNumber;
            }

            if (field.column_width == null) {
                subCol.fillspace = true;
            } else {
                subCol.width = field.column_width;
            }

            if (field.type_editor == "combo" || field.type_editor == "multiselect") {
                if (field.ref_id !== undefined && field.ref_id !== null && field.ref_id !== "") {
                    subCol.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                    subCol.optionslist = true;
                } else {
                    subCol.options = JSON.parse(field.list_column);
                    subCol.value = "1";
                }
            }

            if (field.type_editor == "datepicker") {
                subCol.editor = "date";
            }

            if (field.type_editor == "treesuggest") {
                var listColumns = JSON.parse(field.list_column);

                for (var v = 0; v < listColumns.length; v++) {
                    listColumns[v].header = _(listColumns[v].id);
                }

                var popup = webix.ui({
                    view: "treesuggest",
                    body: {
                        columns: listColumns,
                        refid: field.ref_id,
                        url: {
                            $proxy: true,
                            load: function (view, callback, params) {
                                var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

                                webix.ajax(url, function (text) {
                                    var json = JSON.parse(text);
                                    var data = listToTree(json);
                                    view.parse(data, "json");
                                });
                            },
                        },
                        filterMode: {
                            level: false,
                            showSubItems: false,
                        },
                    },
                });

                subCol.popup = popup;
                subCol.editor = "combo";
            }

            if (field.type_editor == "gridcombo") {
                var listColumns = JSON.parse(field.list_column);

                for (var v = 0; v < listColumns.length; v++) {
                    listColumns[v].header = _(listColumns[v].id);
                }

                var popup = webix.ui({
                    view: "gridsuggest",
                    textValue: field.display_field,
                    body: {
                        columns: listColumns,
                        refid: field.ref_id,
                        textValue: field.display_field,
                        tableId: "edit_" + tab.table_name,
                        url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                    },
                });

                subCol.popup = popup;
                subCol.editor = "combo";
            }
            if (field.type_editor == "search") {
                subCol.onSearchIconClick = function (e) {
                    var form = $$(this.getFormView());

                    var field_express = this.config.fieldExpression;
                    var text = this.getValue();
                    var datapost = { masothue: text };
                    webix
                        .ajax()
                        .headers({ "Content-type": "application/json" })
                        .post(app.config.host + "/System/GetInfoByMST", datapost, function (text) {
                            var json = JSON.parse(text);

                            if (!json.hasOwnProperty("error")) {
                                //console.log(JSON.stringify(json));
                                form.setValues(json);
                            } else {
                                // nếu không tồn tại mã số thuế phải xóa địa chỉ + tên cty ở đây
                                webix.message(json.error, "error", 2000);
                            }
                        });
                };
            }
            if (field.type_editor == "gridsuggest") {
                var listColumns = JSON.parse(field.list_column);

                for (var v = 0; v < listColumns.length; v++) {
                    listColumns[v].header = _(listColumns[v].id.toUpperCase());
                }

                subCol.editor = "textsearch";
                subCol.tableId = "edit_" + tab.table_name;
                subCol.onIconButtonClick = function (e) {
                    var tableId = this.getAttribute("tableId");
                    var table = $$(tableId);
                    tableShowEditWindow(table, 1);
                };

                subCol.suggest = {
                    view: "gridsuggest",
                    value_suggest: field.value_suggest,
                    fieldExpression: field.field_expression,
                    displayField: field.display_field,
                    tableId: "edit_" + tab.table_name,
                    columnName: field.column_name,
                    body: {
                        columns: listColumns,
                        refid: field.ref_id,
                        scroll: true,
                        autoheight: false,
                        datatype: "json",
                        dataFeed: function (filtervalue, filter) {
                            if (filtervalue.length < 1) return;

                            var refid = this.config.refid;
                            var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
                            var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                            this.load(pathUrl + "&" + urldata, this.config.datatype);
                        },
                    },
                    on: {
                        onValueSuggest: function (obj) {
                            if (this.config.fieldExpression != null) {
                                var table = $$("hoadon68_chitiet");
                                var selectedItem = table.getSelectedItem();

                                var fields = this.config.fieldExpression.split(";");

                                for (var i = 0; i < fields.length; i++) {
                                    var field = fields[i].replace("{", "").replace("}", "");
                                    var array = field.split(":");

                                    selectedItem[array[1]] = obj[array[0]];
                                }

                                table.updateItem(selectedItem.id, selectedItem);
                            }

                            if (this.config.value_suggest != null) {
                                var fn = Function("obj", "id", this.config.value_suggest);
                                fn(obj, this.config.id);
                            }
                        },
                    },
                };
            }
            return subCol;
        },

        //FormMater Tạo item form trên trang từ data
        CreatItemMaterFromData: function (field) {
            var item = {
                view: field.type_editor,
                resizeColumn: true,
                //keepViews: true,
                name: field.column_name,
                label: _(field.caption),
                id: field.column_name,
                width: field.width,
                labelPosition: field.label_position,
                labelWidth: field.label_width,
                format: field.format,
                value_change: field.value_change,
                fieldExpression: field.field_expression,
                data_width: field.data_width,
                column_type: field.column_type,
                css: field.css,
                attributes: {},
                on: {},
                hidden: field.hidden,
            };
            if (item.format === "fmNumber") item.format = fmNumber;
            if (field.type_editor == "password") {
                item.view = "text";
                item.type = "password";
            }

            if (item.data_width != null) {
                if (item.data_width != 0) {
                    item.attributes.maxlength = item.data_width;
                    //style: "text-transform: uppercase;"
                }
            }

            if (field.styleinput != null && field.styleinput != "") {
                item.attributes.style = field.styleinput;
            }

            if (field.height != null) {
                if (field.height != 0) {
                    item.height = field.height;
                }
            }

            if (field.caption != null) {
                item.label = _(field.caption);
            }

            if (field.read_only == "C") {
                item.readonly = true;
            }

            if (item.view == "combo") {
                item.suggest = {
                    body: {
                        url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                    },
                };
                //item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
            }

            if (item.view == "select") {
                item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
            }
            if (item.view == "checkbox") {
                if (field.label_position == "right") {
                    item.labelRight = _(field.column_name);
                    item.label = "";
                }

                if (field.column_type == "char") {
                    item.checkValue = "C";
                    item.uncheckValue = "K";
                }

                if (field.column_type == "tinyint") {
                    item.checkValue = "1";
                    item.uncheckValue = "0";
                }

                if (field.column_type == "bit") {
                    item.checkValue = true;
                    item.uncheckValue = false;
                }
            }

            if (item.view == "autonumeric" || field.column_type == "numeric") {
                item.inputAlign = "right";
                item.decimalPlaces = field.data_decimal;
            }

            if (field.valid_rule != null) {
                var rule = field.valid_rule;

                if (rule == "isNotEmpty") {
                    item.required = true;
                    item.invalidMessage = item.label + " không được bỏ trống";
                    item.validate = webix.rules.isNotEmpty;
                } else if (rule == "isSpecialCharacter") {
                    item.required = true;
                    item.invalidMessage = "Không bỏ trống hoặc ký tự không hợp lệ (a-z,A-Z,0-9)";
                    item.validate = webix.rules.isSpecialCharacter;
                } else if (rule == "isEmail") {
                    item.invalidMessage = item.label + " không hợp lệ";
                    item.validate = webix.rules.isEmail;
                } else if (rule == "isTaxCode") {
                    item.invalidMessage = item.label + " không hợp lệ";
                    item.validate = webix.rules.isTaxCode;
                } else {
                    if (rule.trim() !== "") {
                        item.validate = Function("value", field.valid_rule);
                        item.invalidMessage = item.label + " không hợp lệ";
                    }
                }
            }

            if (item.view == "treesuggest") {
                //item.REF_ID = field.REF_ID.toString();
                var listColumns = JSON.parse(field.list_column);

                for (var v = 0; v < listColumns.length; v++) {
                    listColumns[v].header = _(listColumns[v].id);
                }

                item.view = "combo";
                item.options = {
                    view: "treesuggest",
                    textValue: field.display_field,
                    url: {
                        $proxy: true,
                        load: function (view, callback, params) {
                            var url = app.config.host + "/System/GetDataByReferencesId?id=" + view.config.refid;

                            webix.ajax(url, function (text) {
                                var json = JSON.parse(text);
                                var data = listToTree(json);
                                view.parse(data, "json");
                            });
                        },
                    },
                    body: {
                        columns: listColumns,
                        refid: field.ref_id,
                        filterMode: {
                            level: false,
                            showSubItems: false,
                        },
                    },
                };

                item.on.onKeyPress = function (code, e) {
                    if (e.code == "F4") {
                        alert("Moi");
                    }
                };
            }

            if (item.view == "gridcombo") {
                var listColumns = JSON.parse(field.list_column);

                for (var v = 0; v < listColumns.length; v++) {
                    listColumns[v].header = _(listColumns[v].id);
                }

                item.view = "combo";
                item.options = {
                    view: "gridsuggest",
                    template: field.display_field,
                    url: app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id,
                    body: {
                        columns: listColumns,
                        refid: field.REF_ID,
                        textValue: field.display_field,
                    },
                };

                item.on.onKeyPress = function (code, e) {
                    if (e.code == "F4") {
                        alert("Moi");
                    }
                };
            }
            if (item.view == "search") {
                item.on.onSearchIconClick = function (e) {
                    var form = $$(this.getFormView());

                    //get value của form hiện tại để đổi giá trị
                    var mst = this.getValue();
                    // window.location.href = "http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst;
                    window.open("http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp?mst=" + mst);
                };
            }

            if (item.view == "gridsuggest") {
                var listColumns = JSON.parse(field.list_column);

                for (var v = 0; v < listColumns.length; v++) {
                    listColumns[v].header = _(listColumns[v].id);
                }

                item.view = "search";
                item.icon = "mdi mdi-plus";
                item.on.onSearchIconClick = function (e) {
                    showEditWindow(this, 1);
                };
                item.suggest = {
                    view: "gridsuggest",
                    autoheight: false,
                    value_suggest: field.value_suggest,
                    textValue: field.display_field,
                    body: {
                        columns: listColumns,
                        refid: field.ref_id,
                        scroll: true,
                        autoheight: false,
                        datatype: "json",
                        textValue: field.display_field,
                        dataFeed: function (filtervalue, filter) {
                            if (filtervalue.length < 1) return;

                            var refid = this.config.refid;
                            var pathUrl = app.config.host + "/System/GetDataByReferencesId?id=" + refid;
                            var urldata = "filtervalue=" + encodeURIComponent(filtervalue);

                            this.load(pathUrl + "&" + urldata, this.config.datatype);
                        },
                    },
                    on: {
                        onValueSuggest: function (obj) {
                            //console.log(obj);
                            var master = $$(this.config.master);
                            if (master.config.fieldExpression != null) {
                                var win = master.getTopParentView();
                                var form = win.getChildViews()[1];
                                //console.log(form);
                                var parentItem = form.getValues();
                                //console.log(parentItem);
                                var fields = master.config.fieldExpression.split(";");

                                for (var i = 0; i < fields.length; i++) {
                                    var field = fields[i].replace("{", "").replace("}", "");
                                    var array = field.split(":");
                                    //console.log(array);
                                    parentItem[array[1]] = obj[array[0]];
                                }

                                form.setValues(parentItem);
                            }

                            if (this.config.value_suggest != null) {
                                var fn = Function("obj", "id", this.config.value_suggest);
                                fn(obj, this.config.id);
                            }
                        },
                    },
                };

                item.on.onKeyPress = function (code, e) {
                    if (e.code == "F2") {
                        var suggest = $$(this.config.suggest);
                        var refid = suggest.config.body.refid;

                        suggest.hide();
                        var sql = {
                            command: "SYS004",
                            parameter: {
                                refid: refid,
                            },
                        };
                        //var sql = "SELECT a.*,b.window_id as WINDOWNO FROM #SCHEMA_NAME#.wb_references a INNER JOIN #SCHEMA_NAME#.wb_window b ON a.wb_window_id=b.wb_window_id  WHERE a.wb_references_id='" + refid + "'";
                        //var url = app.config.host + "/System/ExecuteQuery?sql=" + sql;
                        var url = app.config.host + "/System/ExecuteCommand";

                        var win = this.getTopParentView();
                        var form = win.getChildViews()[1];

                        var editorId = this.config.id;
                        var formId = form.config.id;

                        var a = webix.ajax().post(url, sql);
                        var b = this.config.suggest;
                        var c = this.config.id;
                        var d = this.config.fieldExpression;
                        var e = editorId;
                        var f = formId;

                        webix.promise.all([a, b, c, d, e, f]).then(function (results) {
                            var res_a = results[0].json();

                            var suggest = $$(results[1]);

                            suggest.hide();

                            var url = app.config.host + "/System/GetConfigWinByNo?id=" + res_a[0]["windowno"];

                            var ajax = webix.ajax();
                            ajax.master = {
                                searchId: results[2],
                                fieldExpression: d,
                                valueField: res_a[0]["value_field"],
                                formId: f,
                                editorId: e,
                            };

                            ajax.get(url, function (text) {
                                var windowconfig = JSON.parse(text);
                                var txtSearch = $$(this.searchId);

                                var win = txtSearch.$scope.ui(selectwindow.$ui);

                                win.config.valueField = this.valueField;
                                win.config.fieldExpression = this.fieldExpression;
                                win.config.formId = this.formId;
                                win.config.editorId = this.editorId;

                                selectwindow.initUI(windowconfig);

                                win.getHead().setHTML(windowconfig.data[0].name);
                                win.show();
                            });
                        });

                        var win = this.$scope.ui(selectwindow.$ui);
                        win.show();
                    }
                };
            }

            if (item.value_change != null || item.css != null || item.validate != null) {
                item.on.onChange = function (newv, oldv) {
                    if (this.config.validate != null) {
                        this.validate();
                    }

                    if (this.config.css != null) {
                        if (this.config.css == "mcls_uppercase" && newv !== undefined && oldv !== undefined) {
                            if (newv.toUpperCase() != oldv.toUpperCase()) {
                                webix.$$(this.config.id).setValue(newv.toUpperCase());
                            }
                        }
                    }

                    if (this.config.value_change != null) {
                        var fn = Function("newv", "oldv", "id", this.config.value_change);
                        fn(newv, oldv, this.config.id);
                    }
                };
            }
            if (item.css === "txtTongtien") item.label = "";
            return item;
        },

        ExeCuteCheckReadCurencyTV: function (checkReadTV) {
            if (checkReadTV != 0) {
                $$("docngoaitetv").setValue(1);
            }

            var total_amount = $$("tgtttbso_last").getValue();

            var type_curency = $$("dvtte").getValue();

            if (
                total_amount !== undefined &&
                total_amount !== null &&
                total_amount.toString() !== " " &&
                total_amount.toString() !== "" &&
                total_amount.toString() !== "0" &&
                checkReadTV.toString() !== "true" &&
                CheckKhacNull(checkReadTV)
            ) {
                var url = "";
                if (type_curency == "VND") {
                    url = app.config.host + "/Invoice68/Amount_ToWord?number=" + total_amount + "&type_amount=" + type_curency;
                } else {
                    url = app.config.host + "/Invoice68/Amount_ToWord_New?number=" + total_amount + "&checkReadTV=" + checkReadTV;
                }

                webix
                    .ajax()
                    .headers({ "Content-type": "application/json" })
                    .get(url, {
                        error: function (text, data, XmlHttpRequest) {
                            //webix.modalbox.hide(box);
                            webix.alert({
                                title: "Lỗi !",
                                text: text.Message,
                                type: "alert-error",
                            });
                        },
                        success: function (text, data, XmlHttpRequest) {
                            var c_result = JSON.parse(text);
                            $$("tgtttbchu").setValue(c_result.data);
                        },
                    });
            } else {
                if (type_curency === "USD") {
                    if (checkReadTV == 0) {
                        $$("tgtttbchu").setValue("Zero dollars.");
                    } else {
                        $$("tgtttbchu").setValue("Không đô la Mỹ.");
                    }
                } else {
                    $$("tgtttbchu").setValue("Không đồng.");
                }
            }
        },
        GetHoaDonChon: function () {
            var tableHdon = webix.$$("invoiceViews");
            var datafrmAll = tableHdon.serialize();
            // get list được chọn
            var lsthdon_id = [];

            var lstDataChoose = datafrmAll
                .filter(function (p) {
                    return p != undefined;
                })
                .filter(function (p) {
                    return p.chon == true;
                });

            if (lstDataChoose.length == 0) {
                webix.message("Vui lòng chọn hóa đơn để xử lý!", "error");
            }
            return lstDataChoose;
        },
        ExcuteShowPopupNotifyErrorInvoice: function () {
            require(["views/forms/68_huyhoadon", "app"], function (huyhoadon, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");
                var item = table.getSelectedItem();
                if (item.tthdon == 11 || item.tthdon == 17) {
                    webix.message("Hóa đơn đã điều chỉnh không thể thực hiện chức năng này", "error");
                    return;
                }

                var datafrm = table.serialize();
                var data = [];

                if (datafrm.length > 0) {
                    for (var i = 0; i < datafrm.length; i++) {
                        if (datafrm[i] != undefined) {
                            if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true) {
                                data.push(datafrm[i]);
                            }
                        }
                    }
                }

                if (data.length > 0) {
                    var cungshd = true;
                    for (var k = 1; k <= data.length - 1; k++) {
                        if (data[k]["stbao"] !== data[0]["stbao"]) {
                            cungshd = false;
                            break;
                        }
                    }
                    if (cungshd === true) {
                        var loai = 1;
                        if (data[0]["stbao"] !== null) loai = 2;
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].tthai == "Chờ gửi" || data[i].tthai == "Chờ ký" || data[i].tthai == "Đã ký") {
                                webix.message("Hóa đơn chưa gửi không thể thông báo sai sót", "error");
                                app.callEvent("dataChangedInvoie68", [1, null]);
                                return;
                            }
                            if (data[i].ladhddt == undefined) {
                                data[i].ladhddt = "1";
                            }
                            if (data[i].tctbao == undefined) {
                                data[i].tctbao = "1";
                            }
                        }

                        var win = table.$scope.ui(huyhoadon.$ui);
                        huyhoadon.setEditMode(1);
                        webix.$$("btnHuyhoadon").destructor();
                        var tongquat = {
                            so: data[0].stbao,
                            ntbccqt: data[0].ntbao,
                            loai: loai,
                        };
                        huyhoadon.setValuesTongQuat(tongquat, null, null);
                        data.forEach(function (item) {
                            var put = {
                                mcqtcap: item.mccqthue,
                                khieu: item.khieu_temp,
                                shdon: item.shdon_temp,
                                ngay: item.nlap_temp,
                                ladhddt: item.ladhddt,
                                tctbao: item.tctbao,
                                hdon_id: item.hdon_id_old,
                            };
                            huyhoadon.setValues(put, null, null);
                            //this.hide();
                            win.show();
                        });
                    } else {
                        webix.message("Các hóa đơn chọn phải có số thông báo giống nhau", "error");
                    }
                } else {
                    var loai = 1;
                    if (item["stbao"] !== null) loai = 2;

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn", "error");
                        return;
                    }

                    if (item.tthai == "Chờ gửi" || item.tthai == "Chờ ký" || item.tthai == "Đã ký") {
                        webix.message("Hóa đơn chưa gửi không thể thông báo sai sót", "error");
                        app.callEvent("dataChangedInvoie68", [1, null]);
                        return;
                    }

                    if (item.ladhddt == undefined) {
                        item["ladhddt"] = "1";
                    }
                    if (item.tctbao == undefined) {
                        item["tctbao"] = "1";
                    }

                    var win = table.$scope.ui(huyhoadon.$ui);
                    huyhoadon.setEditMode(1);
                    webix.$$("btnHuyhoadon").destructor();

                    var tongquat = {
                        so: item.stbao,
                        ntbccqt: item.ntbao,
                        loai: loai,
                    };
                    huyhoadon.setValuesTongQuat(tongquat, null, null);

                    var put = {
                        khieu: item.khieu,
                        shdon: item.shdon_temp,
                        ngay: item.nlap_temp,
                        ladhddt: item.ladhddt,
                        tctbao: item.tctbao,
                        hdon_id: item.hdon_id_old,
                    };
                    huyhoadon.setValues(put, null, null);

                    //this.hide();
                    win.show();
                }
            });
        },
        ExcuteReplaceInvoice: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");
                var item = table.getSelectedItem();

                if (item == null || item == undefined) {
                    webix.message("Bạn chưa chọn hóa đơn !", "error");
                    return;
                }

                if (
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        (item.tthdon == 0 || item.tthdon == 1)) ||
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        item.tthdon == 2) ||
                    (item.tthai == "Chờ phản hồi" && item.tthdon == 3)
                ) {
                    var check = true;
                } else {
                    webix.message("Không thể thực hiện chức năng này", "error");
                    return;
                }

                // var btn = $$("btnPlus");
                // btn.callEvent("onClick");
                //var detailForm = $$("invoiceForms");
                var detailForm;
                var khieu = item.khieu;
                var windowno = app.path[1].params[0];
                var app_cache = webix.storage.local.get("APP_CACHE");
                var windowconfig = app_cache[windowno];
                var ngay_hoa_don = item.nlap;
                item.cctbao_id_temp = item.cctbao_id;
                item.cctbao_id = kyHieuHoaDon.cctbao_id;
                item.khieu_temp = item.khieu;
                item.khieu = kyHieuHoaDon.khieu;
                item.nlap_temp = item.nlap;
                item.nlap = null;
                item.shdon_temp = item.shdon;
                item.shdon = "";
                item.hdon_id_old = item.hdon_id;
                item.tthdon_old = item.tthdon;
                item.tthdon = 2;
                item.tthdon_original = 2;
                item.tthai_old = item.tthai;
                item.nky = "";
                item.hdon_id = "";
                item.loaihdlq = kyHieuHoaDon.loaihdlq;

                for (var i = 0; i < arrayString.length; i++) {
                    item[arrayString[i]] = 0;
                }
                item["tgtttbchu"] = "";

                if (khieu.charAt(1) == "C") {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                    invoiceFormsCode.initUI(windowconfig);
                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceFormsCode.setEditMode(1);
                    invoiceFormsCode.setValues(item, null);
                } else {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(item, null);
                }

                var tableDetail = webix.$$("hoadon68_chitiet");

                tableDetail.clearAll();
                tableDetail.refresh();

                detailForm.show();
            });
        },
        ExcuteReplaceInvoice_32: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var detailForm;
                for (var i = 0; i < arrayString.length; i++) {
                    kyHieuHoaDon[arrayString[i]] = 0;
                }
                kyHieuHoaDon["tgtttbchu"] = "";
                var windowno = app.path[1].params[0];
                var app_cache = webix.storage.local.get("APP_CACHE");
                var windowconfig = app_cache[windowno];
                if (kyHieuHoaDon.khieu.charAt(1) == "C") {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                    invoiceFormsCode.initUI(windowconfig);
                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceFormsCode.setEditMode(1);
                    invoiceFormsCode.setValues(kyHieuHoaDon, null);
                } else {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(kyHieuHoaDon, null);
                }

                var tableDetail = webix.$$("hoadon68_chitiet");

                tableDetail.clearAll();
                tableDetail.refresh();

                detailForm.show();
            });
        },
        ExcuteIncreaseChangeInvoice_32: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");
                // var item = table.getSelectedItem();
                var btn = $$("btnPlus");
                // btn.callEvent("onClick");
                //var detailForm = $$("invoiceForms");
                var detailForm;
                for (var i = 0; i < arrayString.length; i++) {
                    kyHieuHoaDon[arrayString[i]] = 0;
                }
                kyHieuHoaDon["tgtttbchu"] = "";
                var windowno = app.path[1].params[0];
                var app_cache = webix.storage.local.get("APP_CACHE");
                var windowconfig = app_cache[windowno];
                if (kyHieuHoaDon.khieu.charAt(1) == "C") {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                    invoiceFormsCode.initUI(windowconfig);
                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceFormsCode.setEditMode(1);
                    invoiceForms.setValues(kyHieuHoaDon, null);
                } else {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(kyHieuHoaDon, null);
                }

                var tableDetail = webix.$$("hoadon68_chitiet");

                tableDetail.clearAll();
                tableDetail.refresh();

                detailForm.show();
            });
        },
        ExcuteReduceChangeInvoice_32: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");
                // var item = table.getSelectedItem();
                var btn = $$("btnPlus");
                // btn.callEvent("onClick");
                //var detailForm = $$("invoiceForms");
                for (var i = 0; i < arrayString.length; i++) {
                    kyHieuHoaDon[arrayString[i]] = 0;
                }
                kyHieuHoaDon["tgtttbchu"] = "";
                var detailForm;
                var windowno = app.path[1].params[0];
                var app_cache = webix.storage.local.get("APP_CACHE");
                var windowconfig = app_cache[windowno];
                if (kyHieuHoaDon.khieu.charAt(1) == "C") {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                    invoiceFormsCode.initUI(windowconfig);
                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceFormsCode.setEditMode(1);
                    invoiceForms.setValues(kyHieuHoaDon, null);
                } else {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(kyHieuHoaDon, null);
                }

                var ghichu = webix.$$("textGhiChu");
                ghichu.setValue("Lưu ý: Hóa đơn điều chỉnh giảm thì GHI DẤU ÂM (-) đối với chỉ tiêu bị điều chỉnh giảm (Ví dụ: số lượng, đơn giá..)");
                ghichu.show();

                var tableDetail = webix.$$("hoadon68_chitiet");

                tableDetail.clearAll();
                tableDetail.refresh();

                detailForm.show();
            });
        },
        ExcuteInfoChangeInvoice_32: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");
                // var item = table.getSelectedItem();
                var btn = $$("btnPlus");
                // btn.callEvent("onClick");
                //var detailForm = $$("invoiceForms");
                for (var i = 0; i < arrayString.length; i++) {
                    kyHieuHoaDon[arrayString[i]] = 0;
                }
                kyHieuHoaDon["tgtttbchu"] = "";
                var detailForm;
                var windowno = app.path[1].params[0];
                var app_cache = webix.storage.local.get("APP_CACHE");
                var windowconfig = app_cache[windowno];
                if (kyHieuHoaDon.khieu.charAt(1) == "C") {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                    invoiceFormsCode.initUI(windowconfig);
                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceFormsCode.setEditMode(1);
                    invoiceForms.setValues(kyHieuHoaDon, null);
                } else {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(kyHieuHoaDon, null);
                }

                var obj_detail = {
                    id: webix.uid(),
                    kmai: "4",
                    ten: "Hóa đơn điều chỉnh cho hóa đơn số " + kyHieuHoaDon.shdclquan + " ngày " + moment(kyHieuHoaDon.nlhdclquan).format("DD/MM/YYYY") + " ký hiệu " + kyHieuHoaDon.khhdclquan,
                };
                var tableDetail = webix.$$("hoadon68_chitiet");
                tableDetail.clearAll();
                tableDetail.add(obj_detail);
                var columns = tableDetail.config.columns;
                for (var i = 0; i < columns.length; i++) {
                    var column = columns[i];
                }
                tableDetail.refreshColumns();
                tableDetail.refresh();

                detailForm.show();
            });
        },

        ExcuteIncreaseChangeInvoice: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");
                var item = table.getSelectedItem();
                if (item == null || item == undefined) {
                    webix.message("Bạn chưa chọn hóa đơn !", "error");
                    return;
                }

                if (
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        (item.tthdon == 0 || item.tthdon == 1)) ||
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        item.tthdon == 11)
                ) {
                    var check = true;
                } else {
                    webix.message("Không thể thực hiện chức năng này", "error");
                    return;
                }

                var box = webix.modalbox({
                    title: "Chọn mặt hàng cần điều chỉnh",
                    buttons: ["Nhận", "Hủy bỏ"],
                    text: "<div id='ctnMatHang'></div>",
                    top: 100,
                    width: 750,
                    callback: function (result) {
                        switch (result) {
                            case "0":
                                var tblMatHang = $$("tblMatHang");
                                var array = tblMatHang.serialize();

                                var chons = [];

                                for (var i = 0; i < array.length; i++) {
                                    var chon = array[i];
                                    if (chon.hasOwnProperty("chon")) {
                                        if (chon.chon == true) {
                                            chons.push(chon);
                                        }
                                    }
                                }
                                tblMatHang.destructor();

                                webix.modalbox.hide();
                                // var btnPlus = $$("btnPlus");
                                // btnPlus.callEvent("onClick");
                                var detailForm;
                                var khieu = item.khieu;
                                var windowno = app.path[1].params[0];
                                var app_cache = webix.storage.local.get("APP_CACHE");
                                var windowconfig = app_cache[windowno];
                                var ngay_hoa_don = item.nlap;
                                item.cctbao_id_temp = item.cctbao_id;
                                item.cctbao_id = kyHieuHoaDon.cctbao_id;
                                item.khieu_temp = item.khieu;
                                item.khieu = kyHieuHoaDon.khieu;
                                item.nlap_temp = item.nlap;
                                item.nlap = null;
                                item.shdon_temp = item.shdon;
                                item.shdon = "";
                                item.hdon_id_old = item.hdon_id;
                                item.tthdon_old = item.tthdon;
                                item.tthdon = 19;
                                item.tthdon_original = 19;
                                item.tthai_old = item.tthai;
                                item.nky = "";
                                item.hdon_id = "";

                                for (var i = 0; i < arrayString.length; i++) {
                                    item[arrayString[i]] = 0;
                                }
                                item["tgtttbchu"] = "";

                                if (khieu.charAt(1) == "C") {
                                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                                    invoiceFormsCode.initUI(windowconfig);
                                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                                    invoiceFormsCode.setEditMode(1);
                                    invoiceFormsCode.setValues(item, null);
                                } else {
                                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                                    invoiceForms.initUI(windowconfig);
                                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                                    invoiceForms.setEditMode(1);
                                    invoiceForms.setValues(item, null);
                                }

                                var tableDetail = webix.$$("hoadon68_chitiet");
                                tableDetail.clearAll();
                                for (var i = 0; i < chons.length; i++) {
                                    var chon = chons[i];
                                    chon.hdon_id = null;
                                    chon.cthdon_id = null;
                                    chon.id = webix.uid();
                                    tableDetail.add(chon);
                                }
                                tableDetail.refreshColumns();
                                detailForm.show();
                                break;
                            case "1":
                                webix.modalbox.hide();
                                break;
                        }
                    },
                });
                var tblMatHang = webix.ui({
                    container: "ctnMatHang",
                    id: "tblMatHang",
                    view: "datatable",
                    borderless: true,
                    height: 350,
                    columns: [
                        {
                            id: "chon",
                            header: [
                                {
                                    text: "",
                                    content: "masterCheckbox",
                                },
                            ],
                            width: 65,
                            checkValue: true,
                            uncheckValue: false,
                            template: "{common.checkbox()}",
                        },
                        {
                            id: "stt",
                            header: "",
                            width: 30,
                        },
                        {
                            id: "ma",
                            header: _("Mã hàng hóa"),
                            editor: "text",
                            width: 100,
                            editable: true,
                        },
                        {
                            id: "ten",
                            header: _("Tên hàng hóa"),
                            editor: "text",
                            width: 350,
                            editable: true,
                        },
                        {
                            id: "mdvtinh",
                            header: _("ĐVT"),
                            editor: "text",
                            width: 80,
                            editable: true,
                        },
                        {
                            id: "sluong",
                            header: _("Số lương"),
                            editor: "autonumeric",
                            width: 100,
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "dgia",
                            header: _("Đơn giá"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                            // value_change: field.value_change
                        },
                        {
                            id: "thtien",
                            header: _("Tiền trước thuế"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                            // value_change: field.value_change
                        },
                        {
                            id: "tlckhau",
                            header: _("% CK"),
                            width: 80,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "stckhau",
                            header: _("Tiền CK"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "tsuat",
                            header: _("Thuế suất"),
                            width: 100,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "tthue",
                            header: _("Tiền thuế"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "tgtien",
                            header: _("Tiền sau thuế"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                    ],
                });
                var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
                webix
                    .ajax()
                    .headers({
                        "Content-type": "application/json",
                    })
                    .get(url, function (text) {
                        var data = JSON.parse(text);

                        var tblMatHang = $$("tblMatHang");
                        tblMatHang.parse(data);
                    });
            });
        },
        ExcuteReduceChangeInvoice: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");

                var item = table.getSelectedItem();
                if (item == null || item == undefined) {
                    webix.message("Bạn chưa chọn hóa đơn !", "error");
                    return;
                }

                if (
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        (item.tthdon == 0 || item.tthdon == 1)) ||
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        item.tthdon == 11)
                ) {
                    var check = true;
                } else {
                    webix.message("Không thể thực hiện chức năng này", "error");
                    return;
                }

                var box = webix.modalbox({
                    title: "Chọn mặt hàng cần điều chỉnh",
                    buttons: ["Nhận", "Hủy bỏ"],
                    text: "<div id='ctnMatHang'></div>",
                    top: 100,
                    width: 750,
                    callback: function (result) {
                        switch (result) {
                            case "0":
                                var tblMatHang = $$("tblMatHang");
                                var array = tblMatHang.serialize();

                                var chons = [];

                                for (var i = 0; i < array.length; i++) {
                                    var chon = array[i];
                                    if (chon.hasOwnProperty("chon")) {
                                        if (chon.chon == true) {
                                            chons.push(chon);
                                        }
                                    }
                                }
                                tblMatHang.destructor();

                                webix.modalbox.hide();
                                // var btnPlus = $$("btnPlus");
                                // btnPlus.callEvent("onClick");
                                var detailForm;
                                var khieu = item.khieu;
                                var windowno = app.path[1].params[0];
                                var app_cache = webix.storage.local.get("APP_CACHE");
                                var windowconfig = app_cache[windowno];
                                item.cctbao_id_temp = item.cctbao_id;
                                item.cctbao_id = kyHieuHoaDon.cctbao_id;
                                item.khieu_temp = item.khieu;
                                item.khieu = kyHieuHoaDon.khieu;
                                item.nlap_temp = item.nlap;
                                item.nlap = null;
                                item.shdon_temp = item.shdon;
                                item.shdon = "";
                                item.hdon_id_old = item.hdon_id;
                                item.tthdon_old = item.tthdon;
                                item.tthdon = 21;
                                item.tthdon_original = 21;
                                item.tthai_old = item.tthai;
                                item.nky = "";
                                item.hdon_id = "";
                                item.note = "reduce_change_invoice";

                                for (var i = 0; i < arrayString.length; i++) {
                                    item[arrayString[i]] = 0;
                                }
                                item["tgtttbchu"] = "";

                                if (khieu.charAt(1) == "C") {
                                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                                    invoiceFormsCode.initUI(windowconfig);
                                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                                    invoiceFormsCode.setEditMode(1);
                                    invoiceFormsCode.setValues(item, null);
                                } else {
                                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                                    invoiceForms.initUI(windowconfig);
                                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                                    invoiceForms.setEditMode(1);
                                    invoiceForms.setValues(item, null);
                                }

                                var ghichu = webix.$$("textGhiChu");
                                ghichu.setValue("Lưu ý: Hóa đơn điều chỉnh giảm thì GHI DẤU ÂM (-) đối với chỉ tiêu bị điều chỉnh giảm (Ví dụ: số lượng, đơn giá..)");
                                ghichu.show();

                                var tableDetail = webix.$$("hoadon68_chitiet");
                                tableDetail.clearAll();
                                for (var i = 0; i < chons.length; i++) {
                                    var chon = chons[i];
                                    chon.hdon_id = null;
                                    chon.cthdon_id = null;
                                    chon.id = webix.uid();
                                    tableDetail.add(chon);
                                }
                                tableDetail.refreshColumns();
                                console.log(item.khieu);
                                console.log(item.khieu_temp);
                                detailForm.show();
                                break;
                            case "1":
                                webix.modalbox.hide();
                                break;
                        }
                    },
                });
                var tblMatHang = webix.ui({
                    container: "ctnMatHang",
                    id: "tblMatHang",
                    view: "datatable",
                    borderless: true,
                    height: 350,
                    columns: [
                        {
                            id: "chon",
                            header: [
                                {
                                    text: "",
                                    content: "masterCheckbox",
                                },
                            ],
                            width: 65,
                            checkValue: true,
                            uncheckValue: false,
                            template: "{common.checkbox()}",
                        },
                        {
                            id: "stt",
                            header: "",
                            width: 30,
                        },
                        {
                            id: "ma",
                            header: _("Mã hàng hóa"),
                            editor: "text",
                            width: 100,
                            editable: true,
                        },
                        {
                            id: "ten",
                            header: _("Tên hàng hóa"),
                            editor: "text",
                            width: 350,
                            editable: true,
                        },
                        {
                            id: "mdvtinh",
                            header: _("ĐVT"),
                            editor: "text",
                            width: 80,
                            editable: true,
                        },
                        {
                            id: "sluong",
                            header: _("Số lương"),
                            editor: "autonumeric",
                            width: 100,
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "dgia",
                            header: _("Đơn giá"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                            // value_change: field.value_change
                        },
                        {
                            id: "thtien",
                            header: _("Tiền trước thuế"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                            // value_change: field.value_change
                        },
                        {
                            id: "tlckhau",
                            header: _("% CK"),
                            width: 80,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "stckhau",
                            header: _("Tiền CK"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "tsuat",
                            header: _("Thuế suất"),
                            width: 100,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "tthue",
                            header: _("Tiền thuế"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                        {
                            id: "tgtien",
                            header: _("Tiền sau thuế"),
                            width: 150,
                            editor: "autonumeric",
                            editable: true,
                            css: {
                                "text-align": "right",
                            },
                            format: webix.Number.numToStr({
                                groupDelimiter: ",",
                                groupSize: 3,
                                decimalDelimiter: ".",
                                decimalSize: 2,
                            }),
                        },
                    ],
                });
                var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
                webix
                    .ajax()
                    .headers({
                        "Content-type": "application/json",
                    })
                    .get(url, function (text) {
                        var data = JSON.parse(text);

                        var tblMatHang = $$("tblMatHang");
                        tblMatHang.parse(data);
                    });
            });
        },
        ExcuteInfoChangeInvoice: function (kyHieuHoaDon) {
            require(["views/forms/formhoadon78", "views/forms/formhoadoncode78", "app"], function (invoiceForms, invoiceFormsCode, app) {
                var arrayString = [
                    "tgtcthue",
                    "tgtcthue10",
                    "tgtcthue5",
                    "tgtcthue0",
                    "tgtcthuek",
                    "tgtcthuekct",
                    "tgtcthuekkk",
                    "ttcktmai",
                    "ttcktmai10",
                    "ttcktmai5",
                    "ttcktmai0",
                    "ttcktmaik",
                    "ttcktmaikct",
                    "ttcktmaikkk",
                    "tgtthue",
                    "tgtthue10",
                    "tgtthue5",
                    "tgtthue0",
                    "tgtthuek",
                    "tgtthuekct",
                    "tgtthuekkk",
                    "tgtttbso",
                    "tgtttbso10",
                    "tgtttbso5",
                    "tgtttbso0",
                    "tgtttbsok",
                    "tgtttbsokct",
                    "tgtttbsokkk",
                    "tkcktmn",
                    "tgtphi",
                    "tgtttbso_last",
                ];
                var table = webix.$$("invoiceViews");
                var item = table.getSelectedItem();
                if (item == null || item == undefined) {
                    webix.message("Bạn chưa chọn hóa đơn !", "error");
                    return;
                }

                if (
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        (item.tthdon == 0 || item.tthdon == 1)) ||
                    ((item.tthai == "Đã ký" ||
                        item.tthai == "Đã cấp mã" ||
                        item.tthai == "CQT đã nhận" ||
                        item.tthai == "Chấp nhận TBSS" ||
                        item.tthai == "Không chấp nhận TBSS" ||
                        item.tthai == "TBSS sai định dạng" ||
                        item.tthai == "Chờ phản hồi") &&
                        item.tthdon == 11)
                ) {
                    var check = true;
                } else {
                    webix.message("Không thể thực hiện chức năng này", "error");
                    return;
                }

                // var btnPlus = $$("btnPlus");
                // btnPlus.callEvent("onClick");
                // var detailForm = $$("invoiceForms");
                var detailForm;
                var khieu = item.khieu;
                var windowno = app.path[1].params[0];
                var app_cache = webix.storage.local.get("APP_CACHE");
                var windowconfig = app_cache[windowno];
                var ngay_hoa_don = item.nlap;
                item.cctbao_id_temp = item.cctbao_id;
                item.cctbao_id = kyHieuHoaDon.cctbao_id;
                item.khieu_temp = item.khieu;
                item.khieu = kyHieuHoaDon.khieu;
                item.nlap_temp = item.nlap;
                item.nlap = null;
                item.hdon_id_old = item.hdon_id;
                item.tthdon_old = item.tthdon;
                item.tthdon = 23;
                item.tthdon_original = 23;
                item.tthai_old = item.tthai;
                item.shdon_temp = item.shdon;
                item.shdon = "";
                item.nky = "";
                item.hdon_id = "";
                item.mccqthue = "";
                for (var i = 0; i < arrayString.length; i++) {
                    item[arrayString[i]] = 0;
                }
                item["tgtttbchu"] = "";

                if (khieu.charAt(1) == "C") {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceFormsCode.$ui);
                    invoiceFormsCode.initUI(windowconfig);
                    invoiceFormsCode.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceFormsCode.setEditMode(1);
                    invoiceFormsCode.setValues(item, null);
                } else {
                    detailForm = $$("invoiceViews").$scope.ui(invoiceForms.$ui);
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.initUINew(kyHieuHoaDon.cctbao_id);
                    invoiceForms.setEditMode(1);
                    invoiceForms.setValues(item, null);
                }

                var obj_detail = {
                    id: webix.uid(),
                    kmai: "4",
                    ten: "Hóa đơn điều chỉnh cho hóa đơn số " + item.shdon_temp + " ngày " + moment(ngay_hoa_don).format("DD/MM/YYYY") + " ký hiệu " + item.khieu,
                };
                var tableDetail = webix.$$("hoadon68_chitiet");
                tableDetail.clearAll();
                tableDetail.add(obj_detail);
                var columns = tableDetail.config.columns;
                for (var i = 0; i < columns.length; i++) {
                    var column = columns[i];
                    //column.editor = null;
                }
                // tableDetail.getColumnConfig("sluong").editor = null;
                // tableDetail.getColumnConfig("thtien").editor = null;
                // tableDetail.getColumnConfig("tsuat").editor = null;
                // tableDetail.getColumnConfig("tgtien").editor = null;
                // tableDetail.getColumnConfig("tthue").editor = null;
                tableDetail.refreshColumns();
                tableDetail.refresh();

                detailForm.show();
            });
        },
        ExcuteCancellingInvoice: function () {
            var table = webix.$$("invoiceViews");

            var datafrm = table.serialize();
            var data = [];

            if (datafrm.length > 0) {
                for (var i = 0; i < datafrm.length; i++) {
                    if (datafrm[i] != undefined) {
                        if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true) {
                            data.push(datafrm[i]);
                            var item = datafrm[i];
                            if (item == null || item == undefined) {
                                webix.message("Không tìm thấy thông tin hóa đơn!", "error");
                            }

                            if (
                                ((item.tthai == "Đã ký" || item.tthai == "CQT đã nhận" || item.tthai == "CQT không tiếp nhận HĐ" || item.tthai == "Chờ phản hồi") &&
                                    (item.tthdon == 0 || item.tthdon == 1)) ||
                                (item.tthai == "CQT không tiếp nhận HĐ" && (item.tthdon == 19 || item.tthdon == 21 || item.tthdon == 23)) ||
                                (item.tthai == "CQT không tiếp nhận HĐ" && item.tthdon == 2)
                            ) {
                                var check = true;
                            } else {
                                webix.message("Tồn tại hóa đơn không thể thực hiện chức năng này", "error");
                                return;
                            }
                        }
                    }
                }
            }

            if (data.length > 0) {
                var totalHuy = data.length;
                webix.confirm({
                    text: "Hủy hóa đơn trên SCTV.<br /> <span style='color: red;'>Lưu ý</span>: Cần làm thông báo 04/SS-HĐĐT để Hủy hóa đơn với CQT",
                    ok: "Có",
                    width: "500px",
                    cancel: "Không",
                    callback: function (res) {
                        if (res) {
                            for (var i = 0; i < data.length; i++) {
                                var idHoaDon = data[i].id;
                                webix
                                    .ajax()
                                    .headers({
                                        "Content-Type": "application/json",
                                    })
                                    .get(app.config.host + "/Invoice68/uploadCanceledInv?id=" + idHoaDon, function (text) {
                                        var response = JSON.parse(text);
                                        if (response.hasOwnProperty("error")) {
                                            webix.message({
                                                type: "error",
                                                text: response.error,
                                            });
                                            return;
                                        }

                                        totalHuy = totalHuy - 1;
                                    });
                            }

                            if (totalHuy == 0) {
                                webix.message("Hóa đơn hủy thành công");
                            }

                            var btnRefresh = $$("btnReload");
                            btnRefresh.callEvent("onClick");
                        }
                    },
                });
            } else {
                webix.message("Chưa tích chọn hóa đơn hủy!", "error");
            }
        },

        CheckKhacNull: function (value) {
            if (value === undefined || value === null || value === "") return false;
            else return true;
        },
        DuyetHoaDon: function (lsthdon_id, status) {
            var tableHdon = webix.$$("invoiceViews");
            webix
                .ajax()
                .headers({ "Content-Type": "application/json" })
                .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                    var result = JSON.parse(text);
                    var kytaptrung = "";
                    for (var i = 0; i < result.length; i++) {
                        var item = result[i];
                        kytaptrung = item.value;
                    }
                    var postData = {
                        lsthdon_id: lsthdon_id,
                    };

                    webix.extend(tableHdon, webix.ProgressBar);
                    tableHdon.disable();
                    tableHdon.showProgress();

                    var dataPut = { data: [postData], status: status };
                    webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(app.config.host + "/Invoice68/DuyetHoaDon", dataPut, function (text) {
                            var result = JSON.parse(text);
                            var table = webix.$$("invoiceViews");
                            table.enable();
                            table.hideProgress();
                            if (result.hasOwnProperty("error")) {
                                webix.message({ type: "error", text: result.error });
                                return;
                            }

                            if (result.ok == true) {
                                var table = webix.$$("invoiceViews");
                                table.clearAll();
                                table.load(table.config.url);
                                webix.modalbox.hide();
                            }
                        });
                });
        },
    };
});
