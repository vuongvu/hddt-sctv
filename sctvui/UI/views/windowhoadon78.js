define([
    "app",
    "locale",
    "models/user",
    "views/forms/formhoadon78",
    "views/forms/68_huyhoadon",
    "views/forms/68_huyhoadon_1",
    "views/forms/68_hoadon_history",
    "views/commonHD",
    "views/Util",
], function (app, _, user, invoiceForms, huyhoadon, huyhoadon_1, hoadonHistory, commonHD, Util) {
    "use strict";
    var fm = function (number) {
        var k = webix.Number.numToStr({
            groupDelimiter: ".",
            groupSize: 3,
            decimalDelimiter: ",",
            decimalSize: 0,
        });
        return k(number);
    };
    var arrayString = [
        "tgtcthue",
        "tgtcthue10",
        "tgtcthue5",
        "tgtcthue0",
        "tgtcthuek",
        "tgtcthuekct",
        "tgtcthuekkk",
        "ttcktmai",
        "ttcktmai10",
        "ttcktmai5",
        "ttcktmai0",
        "ttcktmaik",
        "ttcktmaikct",
        "ttcktmaikkk",
        "tgtthue",
        "tgtthue10",
        "tgtthue5",
        "tgtthue0",
        "tgtthuek",
        "tgtthuekct",
        "tgtthuekkk",
        "tgtttbso",
        "tgtttbso10",
        "tgtttbso5",
        "tgtttbso0",
        "tgtttbsok",
        "tgtttbsokct",
        "tgtttbsokkk",
        "tkcktmn",
        "tgtphi",
        "tgtttbso_last",
    ];
    var subChucNang = webix.ui({
        view: "submenu",
        width: 300,
        padding: 0,
        on: {
            onItemClick: function (id, e, node) {
                var item = this.getItem(id);

                this.hide();

                var fn = Function("_", "user", item.on_click);
                fn(_, user);
            },
        },
    });

    var subBangKe = webix.ui({
        view: "submenu",
        width: 150,
        padding: 0,
        data: [
            {
                id: "addBangKeHoaDon",
                value: "Thêm bảng kê",
            },
            {
                id: "EditBangKeHoaDon",
                value: "Sửa bảng kê",
            },
            {
                id: "InBangKeHoaDon",
                value: "In bảng kê",
            },
        ],
        on: {
            onItemClick: function (id, e, node) {
                var record = this.getItem(id);

                var table = webix.$$("invoiceViews");
                var item = table.getSelectedItem();

                if (record.id == "addBangKeHoaDon") {
                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng dữ liệu", "error");
                        return;
                    }

                    if (item.tthai != "Chờ ký") {
                        webix.message("Vui lòng chọn hoá đơn chờ ký !", "error");
                        return;
                    }

                    if (item.checksobangke != null) {
                        webix.message("Hoá đơn đã có bảng kê không thể thêm !", "error");
                        return;
                    }

                    var detailForm = table.$scope.ui(invoiceForms.$ui);
                    item.shdon = null;

                    item.nlap = new Date(item.nlap);

                    // ẩn tab_phi
                    if (windowconfig["data"][0].Tabs[2] != undefined) {
                        windowconfig["data"][0].Tabs[2].hidden = true;
                    }

                    invoiceForms.setEditMode(4); // editmode = 4(add bang ke chi tiet) // edit mode =5 (sửa bảng kê chi tiết)
                    invoiceForms.initUI(windowconfig);
                    //$$("phi_tab").hide();
                    $$("head-formInvoice").define("template", "Bảng kê chi tiết hoá đơn");
                    $$("head-formInvoice").refresh();

                    webix.$$("infohdon").disable();
                    webix.$$("infokhang").disable();

                    webix.$$("shdon").config.label = "Số bảng kê";
                    //webix.$$("infohdon").disable();
                    if (item.tthdon === 21) {
                        var ghichu = webix.$$("textGhiChu");
                        ghichu.setValue("Lưu ý: Hóa đơn điều chỉnh giảm thì GHI DẤU ÂM (-) đối với chỉ tiêu bị điều chỉnh giảm (Ví dụ: số lượng, đơn giá..)");
                        ghichu.show();
                        item.note = "reduce_change_invoice";
                    }
                    invoiceForms.setValuesBangKe(item, null);
                    detailForm.show();
                    webix.$$("btnBangKeSave").show();
                    webix.$$("btnHoadonSave").destructor();
                    webix.$$("btnHoadonCQT").destructor();
                }

                if (record.id == "EditBangKeHoaDon") {
                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng dữ liệu", "error");
                        return;
                    }

                    if (item.tthai != "Chờ ký") {
                        webix.message("Vui lòng chọn hoá đơn chờ ký !", "error");
                        return;
                    }

                    if (item.checksobangke == null) {
                        webix.message("Hoá đơn chưa có bảng kê!", "error");
                        return;
                    }

                    // ẩn tab_phi
                    if (windowconfig["data"][0].Tabs[2] != undefined) {
                        windowconfig["data"][0].Tabs[2].hidden = true;
                    }

                    //set số bảng kê cho form sửa bảng kê
                    item.shdon = item.checksobangke;

                    item.nlap = new Date(item.nlap);
                    var detailForm = table.$scope.ui(invoiceForms.$ui);
                    invoiceForms.setEditMode(5); // editmode = 4(add bang ke chi tiet) // edit mode =5 (sửa bảng kê chi tiết)
                    invoiceForms.initUI(windowconfig);
                    if (item.tthdon === 21) {
                        var ghichu = webix.$$("textGhiChu");
                        ghichu.setValue("Lưu ý: Hóa đơn điều chỉnh giảm thì GHI DẤU ÂM (-) đối với chỉ tiêu bị điều chỉnh giảm (Ví dụ: số lượng, đơn giá..)");
                        ghichu.show();
                        item.note = "reduce_change_invoice";
                    }
                    $$("head-formInvoice").define("template", "Bảng kê chi tiết hoá đơn");
                    $$("head-formInvoice").refresh();
                    //set lại title số bảng kê
                    webix.$$("shdon").config.label = "Số bảng kê";

                    webix.$$("infohdon").disable();
                    webix.$$("infokhang").disable();

                    invoiceForms.setValuesBangKe(item, null);
                    detailForm.show();
                    webix.$$("btnBangKeSave").show();
                    webix.$$("btnBangKeDelete").show();
                    webix.$$("btnHoadonSave").destructor();
                    webix.$$("btnHoadonCQT").destructor();
                }

                if (record.id == "InBangKeHoaDon") {
                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng dữ liệu", "error");
                        return;
                    }

                    if (item.checksobangke == null) {
                        webix.message("Hoá đơn chưa có bảng kê!", "error");
                        return;
                    }

                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inHoadon?id=" + item.id + "&type=PDF&inchuyendoi=false&checkBangKe=true", {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                }
            },
        },
    });

    //webix.$$("title").parse({ title: "Lập hóa đơn -> Hóa đơn giá trị gia tăng", details: "Lập hóa đơn -> Hóa đơn giá trị gia tăng" });
    webix.ui({
        view: "contextmenu",
        id: "cmenu",
        autowidth: true,
        //css: { "background": "#ccc !important" },
        data: [
            { value: "Thêm (F4)", icon: "far fa-plus" },
            { value: "Sửa (F3)", icon: "far fa-edit" },
            { value: "Xóa (F8)", icon: "far fa-trash" },
            { $template: "Separator" },
            { value: "Tải File đính kèm", icon: "fas fa-file-text-o" },
            { value: "Xem in", icon: "fas fa-print" },
        ],
        on: {
            onItemClick: function (id) {
                if (this.getItem(id).value == "Thêm (F4)") {
                    $$("btnPlus").callEvent("onClick");
                }
                if (this.getItem(id).value == "Sửa (F3)") {
                    $$("btnEdit").callEvent("onClick");
                }
                if (this.getItem(id).value == "Xóa (F8)") {
                    $$("btnDelete").callEvent("onClick");
                }
                if (this.getItem(id).value == "Tải File đính kèm") {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn !", "error");
                        return;
                    }
                    webix.message("Continue !", "debug");
                }
            },
        },
    });
    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnReload",
            on: {
                onClick: function () {
                    webix.callEvent("onLoadError");
                    var table = webix.$$("invoiceViews");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Thêm (F4)") + "</span>",
            id: "btnPlus",
            //css:"button_raised",
            on: {
                onClick: function () {
                    webix.callEvent("onLoadError");
                    var detailForm = this.$scope.ui(invoiceForms.$ui);

                    // hiện tab_phi
                    if (windowconfig["data"][0].Tabs[2] != undefined) {
                        windowconfig["data"][0].Tabs[2].hidden = false;
                    }

                    var cctbao_id = $$("kyhieu_id").getValue();
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.initUINew(cctbao_id);
                    invoiceForms.setEditMode(1);
                    detailForm.show();
                    // if (vc_window.after_new != null) {
                    //     var fn = Function("app", vc_window.after_new);
                    //     fn(app);
                    // }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Sửa (F3)") + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            //css:"button_raised",
            on: {
                onClick: function () {
                    webix.callEvent("onLoadError");
                    var detailForm = this.$scope.ui(invoiceForms.$ui);
                    //var detailForm = this.$scope.ui(webix.$$("main"));
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần sửa !", "error");
                        return;
                    }

                    if (item.tthai != "Chờ ký" && item.tthai != "Chờ duyệt" && item.tthai != "Chờ sinh số") {
                        webix.message("Hóa đơn đã ký hoặc đã gửi lên cơ quan thuế, không thể sửa !", "error");
                        return;
                    }
                    // main.setEditMode(0);
                    // main.initUI(windowconfig);

                    // hiện tab_phi
                    if (windowconfig["data"][0].Tabs[2] != undefined) {
                        windowconfig["data"][0].Tabs[2].hidden = false;
                    }

                    item.nlap = new Date(item.nlap);

                    invoiceForms.initUI(windowconfig);
                    invoiceForms.setEditMode(2);
                    if (item.tthdon === 21) {
                        var ghichu = webix.$$("textGhiChu");
                        ghichu.setValue("Lưu ý: Hóa đơn điều chỉnh giảm thì GHI DẤU ÂM (-) đối với chỉ tiêu bị điều chỉnh giảm (Ví dụ: số lượng, đơn giá..)");
                        ghichu.show();
                        item.note = "reduce_change_invoice";
                    }
                    invoiceForms.setValues(item, null);

                    detailForm.show();
                    //main.focusFirstElement();
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Xóa (F8)") + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            //css:"button_raised",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var lstDataChoose = commonHD.GetHoaDonChon();
                    var lstDataChooseSort = lstDataChoose.sort(function (a, b) {
                        return Number(a.shdon) - Number(b.shdon);
                    });
                    if (lstDataChooseSort.length === 0) return;
                    var strHoaDonSelect = " ";
                    for (var i = 0; i < lstDataChooseSort.length; i++) {
                        if (
                            commonHD.CheckKhacNull(lstDataChooseSort[i].tthai) &&
                            lstDataChooseSort[i].tthai != "Chờ ký" &&
                            lstDataChooseSort[i].tthai != "Chờ sinh số" &&
                            lstDataChooseSort[i].tthai != "Chờ duyệt"
                        ) {
                            webix.message("Không thể xóa hóa đơn đã ký,đã gửi !", "error");
                            return;
                        }
                        if (i < lstDataChooseSort.length - 1) {
                            if (
                                commonHD.CheckKhacNull(lstDataChooseSort[i].shdon) &&
                                commonHD.CheckKhacNull(lstDataChooseSort[i + 1].shdon) &&
                                Number(lstDataChooseSort[i + 1].shdon) - Number(lstDataChooseSort[i].shdon) >= 2
                            ) {
                                webix.message("Các hóa đơn xóa phải có số hóa đơn liên tiếp nhau", "error");
                                return;
                            } else {
                                strHoaDonSelect += lstDataChooseSort[i].shdon + ",";
                            }
                        } else {
                            strHoaDonSelect += lstDataChooseSort[i].shdon;
                        }
                    }
                    var url = app.config.host + "/Invoice68/hoadonXoaNhieu";
                    var data_post = { data: lstDataChooseSort };
                    webix.confirm({
                        title: "XÓA HÓA ĐƠN",
                        text: "Bạn có muốn xóa hóa đơn số: <span style='color: red'>" + strHoaDonSelect + "</span>",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(url, data_post, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            // var json = JSON.parse(text);
                                            // webix.message(json.ExceptionMessage, "error");
                                            webix.message(text + XmlHttpRequest, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            var result = JSON.parse(text);

                                            if (result.hasOwnProperty("error")) {
                                                webix.message({ type: "error", text: result.error });
                                                table.enable();
                                                table.hideProgress();
                                                return;
                                            } else {
                                                table.enable();
                                                table.clearAll();
                                                table.load(table.config.url);
                                                table.hideProgress();
                                            }
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-files-o fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("COPY") + "</span>",
            id: "btnCopy",
            // view: "button",
            // type: "icon",
            // icon: "far fa-files-o",
            // label: _("COPY"),
            // width: 85,
            // css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng dữ liệu", "error");
                        return;
                    }
                    // var vc_window = windowconfig["data"][0];
                    // windowform.$ui.id = vc_window.code;

                    // hiện tab_phi
                    if (windowconfig["data"][0].Tabs[2] != undefined) {
                        windowconfig["data"][0].Tabs[2].hidden = false;
                    }

                    var detailForm = this.$scope.ui(invoiceForms.$ui);
                    item.shdon = null;
                    invoiceForms.setEditMode(1);
                    invoiceForms.initUI(windowconfig);
                    invoiceForms.copyValues(item);
                    detailForm.show();
                    // windowform.focusFirstElement();
                    // if (vc_window.after_new != null) {
                    //     var fn = Function(vc_window.after_new);
                    //     fn();
                    // }
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Bảng kê") + "</span>",
            id: "btnBangKeOption",
            click: function () {
                subBangKe.show(webix.$$("btnBangKeOption").$view);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("PRINT") + "</span>",
            id: "btnPrint",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn cần in !", "error");
                        return;
                    }
                    //webix.message("Chưa có mẫu in ...", "error");
                    // var rpParameter = null;
                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inHoadon?id=" + item.id + "&type=PDF&inchuyendoi=false", {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("In chuyển đổi") + "</span>",
            id: "btnPrintConvert",
            on: {
                onClick: function () {
                    var table = webix.$$("invoiceViews");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn cần in !", "error");
                        return;
                    }
                    //webix.message("Chưa có mẫu in ...", "error");
                    // var rpParameter = null;
                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inHoadon?id=" + item.id + "&type=PDF&inchuyendoi=true", {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                },
            },
            click: function () {
                var self = this;
                commonHD.CommonOnclickTaskOfHD(self);
            },
        },
        {},
        {},
        {},
        {},
        {},
    ];
    var grid = {
        rows: [
            {
                view: "datatable",
                id: "invoiceViews",
                select: true,
                resizeColumn: true,
                fixedRowHeight: false,
                // leftSplit: 8,
                // rightSplit: 1,
                rowLineHeight: 25,
                rowHeight: 25,
                tooltip: true,
                scrollX: true,
                scroll: true,
                //autowidth: true,
                onClick: {
                    ShowHistory: function (event, cell, target) {
                        var table = webix.$$("invoiceViews");
                        var win = table.$scope.ui(hoadonHistory.$ui);
                        var hdon_id = cell.row;
                        hoadonHistory.setEditMode(hdon_id);
                        win.show();
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                    onItemClick: function (id) {
                        var table = webix.$$("invoiceViews");
                        var item = table.getSelectedItem();
                        item.chon = !item.chon;
                        table.updateItem(id.row, item);
                    },
                    // onClick: function (e) {
                    //     // var view = $$(e);
                    //     // if (!view || !view.getFormView || view.getFormView() != $$("invoiceDate"))
                    //     //     webix.confirm("The changes was not saved");
                    // },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onAfterLoad: function () {
                        // webix.delay(function () {
                        //     this.adjustRowHeight("ten_loai", true);
                        //     this.render();
                        // }, this);
                    },
                    onColumnResize: function () {
                        // this.adjustRowHeight("ten_loai", true);
                        // this.render();
                    },
                    onSubViewCreate: function (view, item) {
                        var subView = view;

                        if (subView != null) {
                            var tableDetail = webix.$$("hoadon68_chitiet_view");
                            var url = app.config.host + "/Invoice68/GetDataInvoiceDetail?id=" + item.id;
                            tableDetail.clearAll();
                            tableDetail.load(url);
                        }
                    },
                },
                subview: {
                    borderless: true,
                    view: "tabview",
                    height: 255,
                    css: "subInvoice",
                    scrollX: true,
                    // tabbar: {
                    //     width: 200
                    // },
                    multiview: {
                        fitBiggest: true,
                    },
                    cells: [
                        {
                            header: "Chi tiết hóa đơn",
                            select: "cell",
                            body: {
                                view: "datatable",
                                resizeColumn: true,
                                margin: 10,
                                id: "hoadon68_chitiet_view",
                                height: 200,
                                columns: [],
                                on: {
                                    onBeforeLoad: function () {
                                        this.showOverlay(_("LOADING") + "...");
                                    },
                                    onAfterLoad: function () {
                                        this.hideOverlay();
                                    },
                                },
                            },
                        },
                    ],
                },
                columns: col,
                pager: "pagerA",
                type: {
                    rcheckbox: function (obj, common, value, config) {
                        var checked = value == config.checkValue ? 'checked="true"' : "";
                        return "<input disabled class='webix_table_checkbox' type='checkbox' " + checked + ">";
                    },
                },
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: function (data, common) {
                    var size = 0;
                    var start = data.page * data.size;
                    var end = start + data.size;
                    if (end > data.count) size = data.count - start;
                    else size = data.size;
                    var end = start + data.size;
                    var html = " <span style='text-align:center; line-height:20px; font-size:10pt;'> Trang:" + (data.page + 1) + "/" + data.limit + " </span> ";
                    var htmlSoBanGhi = " <span style='line-height:20px; font-size:10pt;'> Số bản ghi:" + size + "/" + data.count + " </span> ";
                    return common.first() + common.prev() + html + common.next() + common.last() + htmlSoBanGhi;
                },

                /*"{common.first()} {common.prev()}Trang {common.page()}/#limit#{common.next()}{common.last()} Số bản ghi:#group# /#count#",*/
            },
        ],
    };

    var mini_dashboard = {
        type: "clean",
        borderless: true,
        cols: [
            {
                id: "khongMa",
                name: "khongMa",
                css: "tonghop_label",
                view: "label",
            },
            {
                id: "khongMaError",
                name: "khongMaError",
                css: "tonghop_label",
                view: "label",
            },
            {
                id: "doanhthunocode",
                name: "doanhthunocode",
                css: "tonghop_label",
                view: "label",
            },
        ],
    };

    var layout = {
        id: "main",
        css: "frmView",
        rows: [
            {
                rows: [mini_dashboard],
            },
            {
                id: "tlbMain",
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    var col = [];
    var sub_col = [];
    var windowconfig = null;
    var kihieuRefId = null;

    var initComponents = function (_windowconfig) {
        //console.log(_windowconfig);
        var k = Object.keys(_windowconfig["data"][0]["Tabs"]).length;
        //console.log(k);
        var vc_window = _windowconfig["data"][0];
        webix.$$("title").parse({ title: vc_window["name"], details: "Lập hóa đơn ->" + vc_window["name"], width: 1000 });
        var tabs = vc_window.Tabs;

        var windowno = vc_window.window_id;
        kihieuRefId = "RF00054";
        col = [];
        sub_col = [];
        col.push({
            id: "chon",
            header: [{ text: "", content: "masterCheckbox" }],
            width: 30,
            checkValue: true,
            uncheckValue: false,
            template: "{common.checkbox()}",
        });
        col.push({
            id: "subRowMain",
            header: "&nbsp;",
            template: "{common.subrow()}",
            width: 40,
            // height: 25
        });
        for (var i = 0; i < tabs.length; i++) {
            var tab = tabs[i];
            if (tab.tab_type === "Master") {
                var masterTab = tabs[i];
                for (var j = 0; j < tabs[i].Fields.length; j++) {
                    var field = masterTab.Fields[j];
                    if (field.hide_in_grid == true) {
                        continue;
                    }
                    var item = {
                        id: field.column_name,
                        header: _(field.column_name),
                        columnType: field.column_type,
                        columnAlias: field.column_alias,
                        template: field.template,
                        columnFormat: field.format,
                    };
                    if (item.template !== undefined && item.template !== null && item.template.includes("function")) item.template = Util.StringToFunc(item.template);
                    //if(item.format!== undefined && item.format!== null && item.format.includes("function")) item.template = Util.StringToFunc(item.format);
                    if (field.caption != null) {
                        item.header = _(field.caption);
                    }

                    if (field.column_type == "numeric") {
                        item.css = { "text-align": "right" };
                        item.format = webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: field.data_decimal,
                        });
                    }

                    if (field.type_filter != null) {
                        if (field.type_filter != "") {
                            item.header = [_(item.header), { content: field.type_filter }];
                        }
                    }

                    if (field.type_editor == "combo") {
                        item.editor = field.type_editor;
                        item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                    }

                    if (field.column_type == "date" || field.column_type == "datetime") {
                        item.format = function (text) {
                            var format = webix.Date.dateToStr(this.columnFormat);
                            text = removeTimeZoneT(text);
                            return format(text);
                        };
                    }

                    if (field.type_editor == "checkbox") {
                        if (field.column_type == "boolean") {
                            item.checkValue = true;
                            item.uncheckValue = false;
                        }

                        item.template = function (obj, common, value) {
                            if (value == true || value == 1) return "<div class='webix_icon fa-check-square-o'></div>";
                            else return "<div class='webix_icon fa-square-o'></div>";
                        };
                    }

                    if (field.column_width == null) {
                        item.fillspace = true;
                    } else {
                        item.width = field.column_width;
                    }

                    col.push(item);
                }
            }
            if (tab.tab_type == "Detail") {
                for (var j = 0; j < tab.Fields.length; j++) {
                    var field = tab.Fields[j];

                    if (field.hide_in_grid == true) {
                        continue;
                    }

                    var subCol = {
                        id: field.column_name,
                        header: field.caption == null ? _(field.column_name) : _(field.caption),
                        hidden: field.hide_in_grid,
                    };

                    if (field.column_type == "date" || field.column_type == "datetime") {
                        subCol.format = function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        };
                    }

                    if (field.column_type == "numeric") {
                        subCol.css = { "text-align": "right" };
                        subCol.format = webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: field.data_decimal,
                        });
                    }

                    if (field.column_width == null) {
                        subCol.fillspace = true;
                    } else {
                        subCol.width = field.column_width;
                    }

                    sub_col.push(subCol);
                }
            }
        }
        var table = webix.$$("invoiceViews");
        table.clearAll();
        var subview = {
            borderless: true,
            view: "tabview",
            height: 255,
            css: "subInvoice",
            scrollX: true,
            // tabbar: {
            //     width: 200
            // },
            multiview: {
                fitBiggest: true,
            },
            cells: [
                {
                    header: "Chi tiết hóa đơn",
                    select: "cell",
                    body: {
                        view: "datatable",
                        resizeColumn: true,
                        margin: 10,
                        id: "hoadon68_chitiet_view",
                        height: 200,
                        columns: sub_col,
                        on: {
                            onBeforeLoad: function () {
                                this.showOverlay(_("LOADING") + "...");
                            },
                            onAfterLoad: function () {
                                this.hideOverlay();
                            },
                        },
                    },
                },
            ],
        };
        /*var subview = {
            borderless: true,
            view: "tabview",
            height: 200,
            scrollX: true,
            cells: sub_col,
            multiview: {
                fitBiggest: true,
            },
        };*/
        table.define("subview", subview);
        $$("head-title").show();
        $$("head-title").removeView("paramKH");
        $$("head-title").addView(
            {
                id: "paramKH",
                view: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                borderless: true,
                height: 35,
                cols: [],
            },
            1
        );
        if (vc_window.WinParams != undefined && vc_window.WinParams != null) {
            for (var i = 0; i < vc_window.WinParams.length; i++) {
                var item = vc_window.WinParams[i];
                var comp = {
                    id: item.value_field,
                    view: item.type_editor,
                    width: item.width,
                    label: _(item.caption),
                    value_field: item.value_field,
                    isDefault: item.has_default,
                    on: {},
                };

                if (comp.view == "combo") {
                    comp.options = {
                        body: {
                            comboId: comp.id,
                            isDefault: item.has_default,
                            url: app.config.host + "/System/GetDataByReferencesId?id=" + item.wb_references_id,
                            ready: function () {
                                if (this.config.isDefault == "C") {
                                    var count = this.count();
                                    if (count > 0) {
                                        var combo = webix.$$(this.config.comboId);
                                        combo.setValue(this.getFirstId());
                                    }
                                }
                            },
                        },
                    };

                    comp.on.onChange = function (newv, oldv) {
                        var table = $$("invoiceViews");
                        table.clearAll();
                        table.load(table.config.url);
                    };
                }
                $$("paramKH").removeView(item.value_field);
                $$("paramKH").addView(comp);
            }

            // parentToolBar.addView(toolBarParams);
        }

        table.config.columns = col;
        table.refreshColumns();
        table.define("url", loadUrl);
        table.getParentView().getParentView().getParentView().enable();
        table.getParentView().getParentView().getParentView().hideProgress();
    };

    var loadUrl = {
        $proxy: true,
        load: function (view, callback, params) {
            var tlbparam_s = [];
            //get count list combo ký hiệu
            var url = app.config.host + "/Invoice68/GetDataInvoice";

            if (params == null) {
                commonHD.clear_filter(view);
                var table = $$("invoiceViews");
                table.config.params = null;
                url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
            } else {
                var table = $$("invoiceViews");
                table.define("params", params);
                webix.storage.local.put("currentFilter", params.filter);
                url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                if (params.filter != null) {
                    var array = [];

                    for (var property in params.filter) {
                        var column = view.getColumnConfig(property);
                        var item = {
                            columnName: property,
                            columnType: column.columnType,
                            value: params.filter[property],
                        };

                        array.push(item);
                    }

                    //console.log(JSON.stringify(array));
                    url = url + "&filter=" + encodeURIComponent(JSON.stringify(array));
                }
                url = url + "&filter=null";
            }
            var tlbParams = $$("paramKH");

            if (tlbParams !== undefined) {
                var childViews = tlbParams.getChildViews();

                var parameters = [];

                for (var i = 0; i < childViews.length; i++) {
                    var child = childViews[i];
                    // console.log(child);
                    var id = child.config.id;
                    //var value_field = child.config.value_field;
                    var item = {
                        columnName: "cctbao_id",
                        value: $$(id).getValue(),
                    };
                    if (item.value !== "") {
                        parameters.push(item);
                    } else {
                        return;
                    }
                    //parameters.push(item);
                }
                tlbparam_s = parameters;
                url = url + "&tlbparam=" + JSON.stringify(parameters);
            } else {
                url = url + "&tlbparam=null";
            }
            var urlMiniDashBoard = url.replace("GetDataInvoice", "GetDataInvoiceThongTin");

            var urlMiniDashBoard = url.replace("GetDataInvoice", "GetDataInvoiceThongTin");

            webix.ajax().get(urlMiniDashBoard, function (text) {
                var json = JSON.parse(text);
                $$("khongMa").setValue("<span style=' font-weight:bold'>Số hóa đơn đã gửi CQT: </span>" + json["khongma"]);
                $$("khongMaError").setValue("<span style=' font-weight:bold'>Số hóa đơn CQT thông báo lỗi: </span>" + json["khongmaerror"]);
                $$("doanhthunocode").setValue("<span style=' font-weight:bold'>Doanh thu xuất hóa đơn: </span>" + (fm(json["doanhthunocode"]) ? fm(json["doanhthunocode"]) : 0));
            });

            webix.ajax(url, callback, view);
        },
    };
    var buttons = [];
    var initUI = function () {
        var windowno = app.path[1].params[0];
        var url = app.config.host + "/System/ExecuteCommand";
        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(
                url,
                {
                    command: "CM00002",
                    parameter: {
                        window_id: windowno,
                    },
                },
                function (text) {
                    var json = JSON.parse(text);

                    if (buttons.length > 0) {
                        for (var i = 0; i < buttons.length; i++) {
                            var item = buttons[i];
                            $$("tlbMain").removeView(item.id);
                        }
                    }

                    buttons = [];
                    var menus = [];

                    var stt = controls.filter(function (x) {
                        return x.id !== undefined;
                    }).length;

                    for (var i = 0; i < json.length; i++) {
                        var item = json[i];

                        if (item.is_group_function == "C") {
                            if (item.hidden != 1) {
                                menus.push({
                                    id: item.name,
                                    value: _(item.caption),
                                    on_click: item.on_click,
                                });
                            }
                        } else {
                            if (item.hidden != 1) {
                                var fn = new Function(item.on_click);

                                var button = {
                                    view: "button",
                                    type: "htmlbutton",
                                    width: item.width,
                                    label: '<i class="fa fa-' + item.icon + ' fa-lg aria-hidden="true"></i> ' + '<span class="text">' + _(item.caption) + "</span>",
                                    id: item.name,
                                    // label: _(item.caption),
                                    // view: "button",
                                    // type: "icon",
                                    // css: "webix_secondary",
                                    // icon: "fas fa-" + item.icon,
                                    // width: item.width,
                                    click: fn,
                                };

                                buttons.push(button);
                                $$("tlbMain").addView(button, stt);

                                stt = stt + 1;
                            }
                        }
                    }
                    $$("tlbMain").removeView("btnChucNang");
                    if (menus.length > 0) {
                        subChucNang.clearAll();
                        subChucNang.parse(menus, "json");
                        var btnChucNang = {
                            view: "button",
                            type: "htmlbutton",
                            width: 120,
                            label: '<i class="fa fa-tasks fa-lg aria-hidden="true"></i> ' + '<span class="text">' + _("CHUC_NANG") + "</span>",
                            id: "btnChucNang",
                            // label: _("CHUC_NANG"),
                            // view: "button",
                            // type: "icon",
                            // css: "webix_secondary",
                            // icon: "fas fa-tasks",
                            // width: 100,
                            click: function () {
                                subChucNang.show(webix.$$("btnChucNang").$view);
                            },
                        };
                        $$("tlbMain").addView(btnChucNang, stt);
                    }
                }
            );
        col = [];
        sub_col = [];

        var url = app.config.host + "/System/GetConfigWinByNo?id=" + windowno;
        webix.$$("invoiceViews").getParentView().getParentView().getParentView().disable();
        webix.$$("invoiceViews").getParentView().getParentView().getParentView().showProgress();

        var app_cache = webix.storage.local.get("APP_CACHE");

        if (app_cache[windowno] != null) {
            windowconfig = app_cache[windowno];
            initComponents(windowconfig);
        } //không có cache
        else {
            webix.ajax(url, function (text, xml, xhr) {
                var rp = JSON.parse(text);
                windowconfig = rp;

                var app_cache = webix.storage.local.get("APP_CACHE");
                app_cache[windowno] = windowconfig;

                webix.storage.local.put("APP_CACHE", app_cache);
                initComponents(windowconfig);
            });
        }
    };
    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            webix.extend(view, webix.ProgressBar);
            $scope.on(app, "dataChangedInvoie68", function (editmode, item) {
                var table = webix.$$("invoiceViews");
                table.clearAll();
                table.load(table.config.url);
                //if (editmode == 1) {
                //    table.clearAll();
                //    table.load(table.config.url);
                //}

                //if (editmode == 2) {
                //    table.clearAll();
                //    table.load(table.config.url);
                //}
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
        $onDestruct: function (ui, name, url, scope) {
            $$("tlbParams").Clear();
        },
    };
});
