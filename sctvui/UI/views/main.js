define([
    "app",
    "locale",
    "models/user",
    "views/forms/main"
], function (app, _, user, main) {
    'use strict';

    var op = [];

    webix.$$("title").parse({ title: "Quản lý hóa đơn", details: "Quản lý hóa đơn đầu vào" });
    webix.ui({
        view: "contextmenu",
        id: "cmenu",
        //css: { "background": "#ccc !important" },
        data: ["Thêm", { $template: "Separator" }, "Sửa", { $template: "Separator" }, "Xóa", { $template: "Separator" }, "Tải File đính kèm"],
        on: {
            onItemClick: function (id) {

                if (this.getItem(id).value == "Thêm") {
                    $$("btnPlus").callEvent("onClick");
                }
                if (this.getItem(id).value == "Sửa") {
                    $$("btnEdit").callEvent("onClick");
                }
                if (this.getItem(id).value == "Xóa") {
                    $$("btnDelete").callEvent("onClick");
                }
                if (this.getItem(id).value == "Tải File đính kèm") {
                    var table = webix.$$("mainData");
                    var item = table.getSelectedItem();
                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn !", "error");
                        return;
                    }
                    webix.message("Continue !", "debug");
                    //console.log(item);
                    // if (item.inv_originalId != null && item.inv_originalId != undefined) {
                    //     var box = webix.modalbox({
                    //         title: "Đang in chứng từ",
                    //         text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                    //         width: 350,
                    //         height: 200
                    //     });

                    //     //var newWin = window.open();

                    //     webix.ajax()
                    //         .response("arraybuffer")
                    //         .headers({ 'Content-type': 'application/json' })
                    //         .get(app.config.host + "/Invoice/PrintInvoice?id=" + item.inv_originalId + "&type=PDF", {
                    //             error: function (text, data, XmlHttpRequest) {
                    //                 webix.modalbox.hide(box);
                    //                 alert(text);
                    //             },
                    //             success: function (text, data, XmlHttpRequest) {
                    //                 var file = new Blob([data], { type: 'application/pdf' });
                    //                 var fileURL = URL.createObjectURL(file);

                    //                 webix.modalbox.hide(box);
                    //                 //newWin.location = fileURL;
                    //                 window.open(fileURL, '_blank');

                    //             }
                    //         });

                    //     // webix.ajax()
                    //     // .response("arraybuffer")
                    //     // .get(app.config.host + "/Invoice/PrintInvoice?id=" + item.inv_originalId + "&type=PDF", function (text, data) {
                    //     //     var file = new Blob([data], { type: 'application/pdf' });
                    //     //     var fileURL = URL.createObjectURL(file);
                    //     //     window.open(fileURL, '_blank');
                    //     //     // webix.html.download(file, $$('tenfile').getValue());
                    //     // });
                    // }
                    // else {
                    //     webix.message("Không tồn tại hóa đơn gốc !", "error");
                    //     return;
                    // }
                }

            }
        }
    });
    var controls = [
        {
            id: "btnReload",
            view: "button",
            type: "icon",
            icon: "fas fa-refresh",
            label: _("REFRESH"),
            css: "webix_secondary",
            width: 96,
            on: {
                onClick: function () {
                    var table = webix.$$("mainData");
                    table.clearAll();
                    table.load(table.config.url);
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: "Thêm (F4)",
            width: 90,
            shortcut: "F4",
            css: "webix_secondary",
            //css:"button_raised",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(main.$ui);
                    //main.setValues(null, null, null);
                    main.setEditMode(1);
                    detailForm.show();
                    // if (vc_window.after_new != null) {
                    //     var fn = Function("app", vc_window.after_new);
                    //     fn(app);
                    // }
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "far fa-edit",
            label: "Sửa (F3)",
            width: 90,
            shortcut: "F3",
            css: "webix_secondary",
            //css:"button_raised",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(main.$ui);
                    //var detailForm = this.$scope.ui(webix.$$("main"));
                    var table = webix.$$("mainData");
                    var item = table.getSelectedItem();

                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần sửa !", "error");
                        return;
                    }

                    if (item.$group) {
                        webix.message("Không thể sửa nhóm !", "error");
                        return;
                    }
                    // main.setEditMode(0);
                    // main.initUI(windowconfig);
                    main.setEditMode(2);
                    main.setValues(item, null);

                    detailForm.show();
                    //main.focusFirstElement();
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            label: "Xóa (F8)",
            width: 80,
            shortcut: "F8",
            css: "webix_secondary",
            //css:"button_raised",
            on: {
                onClick: function () {
                    var table = webix.$$("mainData");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần xóa !", "error");
                        return;
                    }
                    if (item.$group) {
                        webix.message("Không thể xóa cột nhóm !", "error");
                        return;
                    }

                    var khData = {};
                    // khData.windowid = windowconfig.data[0].window_id;
                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(item);

                    webix.confirm({
                        title: "XÓA HÓA ĐƠN",
                        text: "Bạn có muốn xóa hóa đơn số: <span style='color: red'>" + item.invoiceNumber + "</span>", ok: "Có", cancel: "Không",
                        callback: function (res) {
                            if (res) {

                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix.ajax()
                                    .headers({ 'Content-type': 'application/json' })
                                    .post(app.config.host + "/ManageInvoice/SaveInv", khData,
                                    {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.ExceptionMessage, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            table.remove(item.id);
                                            table.enable();
                                            table.hideProgress();
                                        }
                                    });
                            }
                        }
                    });
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnDowload",
            view: "button",
            type: "icon",
            icon: "far fa-download",
            css: "webix_secondary",
            label: _("Tải File"),
            width: 80,
            on: {
                onClick: function () {
                    //webix.confirm();
                    //webix.message("Continue ! ", "debug");
                    var table = webix.$$("mainData");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần sửa !", "error");
                        return;
                    }

                    if (item.fileUrl == null || item.fileUrl == "") {
                        webix.message("Hóa đơn không có file đính kèm !", "error");
                        return;
                    }

                    if (item.$group) {
                        webix.message("Không thể sửa nhóm !", "error");
                        return;
                    }

                    webix.confirm({
                        title: "TẢI HÓA ĐƠN",
                        text: "Bạn có muốn tải file hóa đơn số: <span style='color: red'>" + item.invoiceNumber + "</span>", ok: "Có", cancel: "Không",
                        callback: function (res) {
                            if (res) {

                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();
                                webix.ajax()
                                    .response("arraybuffer")
                                    .get(app.config.host + "/ManageInvoice/DowloadFile?url=" + item.fileUrl, function (text, data) {
                                        var file = new Blob([data], { type: item.fileType });
                                        webix.html.download(file, item.fileName);
                                        table.enable();
                                        table.hideProgress();
                                    });
                                // webix.ajax()
                                //     .headers({ 'Content-type': 'application/json' })
                                //     .post(app.config.host + "/ManageInvoice/SaveInv", khData,
                                //     {
                                //         error: function (text, data, XmlHttpRequest) {
                                //             table.enable();
                                //             table.hideProgress();
                                //             var json = JSON.parse(text);
                                //             webix.message(json.ExceptionMessage, "error");
                                //             //webix.message(XmlHttpRequest.statusText, "error");
                                //         },
                                //         success: function (text, data, XmlHttpRequest) {
                                //             table.remove(item.id);
                                //             table.enable();
                                //             table.hideProgress();
                                //         }
                                //     });
                            }
                        }
                    });
                }
            },
            click: function () {
                this.callEvent("onClick");
            }

        },
        {
            id: "btnPrint",
            view: "button",
            type: "icon",
            icon: "far fa-print",
            label: _("PRINT") + " PDF",
            css: "webix_secondary",
            width: 70,
            on: {
                onClick: function () {
                    var table = $$("mainData");

                    // var rpParameter = table.config.rpParameter;
                    var rpParameter = null;
                    // if (rpParameter == undefined || rpParameter == null) {
                    //     webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                    //     return;
                    // }
                    // rpParameter.type = "PDF";
                    //console.log(rpParameter);
                    // if (rpParameter != null) {
                    var box = webix.modalbox({
                        title: "Đang in báo cáo ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200
                    });

                    webix.ajax()
                        .response("arraybuffer")
                        .headers({ 'Content-type': 'application/json' })
                        .post(app.config.host + "/Bcthsd/PrintPDFBangKeBanRa", rpParameter,
                        {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: 'application/pdf' });
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                            }
                        }
                        // function (text, data) {
                        //     var file = new Blob([data], { type: 'application/pdf' });
                        //     var fileURL = URL.createObjectURL(file);
                        //     webix.modalbox.hide(box);
                        //     window.open(fileURL, '_blank');
                        // }
                        );
                    // }
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnExport",
            view: "button",
            type: "icon",
            icon: "far fa-file",
            label: _("EXPORT_EXCEL"),
            css: "webix_secondary",
            width: 115,
            on: {
                onClick: function () {
                    var box = webix.modalbox({
                        title: "Đang kết suất Excel ... ",
                        buttons: ["Hủy bỏ"],
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                        callback: function (result) {
                            switch (result) {
                                case "0":
                                    clearTimeout(interVal);
                                    webix.modalbox.hide(box);
                                    break;
                            }
                        }
                    });
                    var interVal = setTimeout(function () {
                        var table = $$("mainData");
                        webix.toExcel(table, {
                            rawValues: true
                        });
                        clearTimeout(interVal);
                        webix.modalbox.hide(box);
                    }, 1500);

                    // var rpParameter = table.config.rpParameter;
                    // if (rpParameter == undefined || rpParameter == null) {
                    //     webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                    //     return;
                    // }
                    // rpParameter.type = "xlsx";

                    // if (rpParameter != null) {
                    //     var box = webix.modalbox({
                    //         title: "Đang kết xuất Excel ....",
                    //         text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                    //         width: 350,
                    //         height: 200
                    //     });

                    //     webix.ajax()
                    //         .response("arraybuffer")
                    //         .headers({ 'Content-type': 'application/json' })
                    //         .post(app.config.host + "/Bcthsd/PrintPDFBangKeBanRa", rpParameter,
                    //         {
                    //             error: function (text, data, XmlHttpRequest) {
                    //                 webix.modalbox.hide(box);
                    //                 webix.message(XmlHttpRequest.statusText, "error");
                    //             },
                    //             success: function (text, data, XmlHttpRequest) {
                    //                 var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    //                 var fileURL = URL.createObjectURL(file);
                    //                 webix.modalbox.hide(box);
                    //                 window.location.href = fileURL;
                    //             }
                    //         }
                    //         // function (text, data) {

                    //         //     var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    //         //     var fileURL = URL.createObjectURL(file);

                    //         //     window.location.href = fileURL;
                    //         // }
                    //         );
                    // }
                }
            },
            click: function () {
                this.callEvent("onClick");
            }

        },

        {}
    ];
    var fm = webix.Number.numToStr({
        groupDelimiter: ",",
        groupSize: 3,
        decimalDelimiter: ".",
        decimalSize: 0
    });
    var grid = {
        rows: [
            {
                container: "testA",
                view: "treetable",
                id: "mainData",
                select: true,
                resizeColumn: true,
                fixedRowHeight: false,
                rowLineHeight: 25, rowHeight: 25,
                //autowidth: true,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var url = app.config.host + "/ManageInvoice/GetData";

                        if (params == null) {
                            url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                        }
                        else {
                            url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                            if (params.filter != null) {
                                var array = [];

                                for (var property in params.filter) {

                                    var column = view.getColumnConfig(property);

                                    var item = {
                                        columnName: property,
                                        columnType: column.columnType,
                                        value: params.filter[property]
                                    };

                                    array.push(item);
                                }

                                //console.log(JSON.stringify(array));
                                url = url + "&filter=" + JSON.stringify(array);
                            }
                        }

                        webix.ajax(url, callback, view);

                    }
                },
                on: {
                    onItemClick: function (id) {
                        if (id.column == "ten_loai") {
                            //webix.confirm()
                            webix.message("Continue Dowloading ... !", "debug");
                        }
                    },
                    onClick: function (e) {
                        var view = $$(e);
                        if (!view || !view.getFormView || view.getFormView() != $$("invoiceDate"))
                            webix.confirm("The changes was not saved");
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onAfterLoad: function () {
                        webix.delay(function () {
                            this.adjustRowHeight("ten_loai", true);
                            this.render();
                        }, this);
                        //console.log($$("mainData").serialize());
                        this.openAll();
                        $$("cmenu").attachTo($$("mainData"));
                    },
                    onColumnResize: function () {
                        this.adjustRowHeight("ten_loai", true);
                        this.render();
                    }
                    // "data->onStoreUpdated": function () {
                    //     this.data.each(function (obj, i) {
                    //         console.log(obj);
                    //         console.log(i);
                    //         obj.index = i + 1;
                    //     })
                    // }
                },
                columns: [
                    {
                        id: "stt", header: "<center>STT</center>", sort: "int", width: 40
                    },
                    {
                        id: "ten_loai", header: [{ "text": "<center>Hóa đơn chứng từ bán</center>", "colspan": 2 }, { "text": "<center>Số hóa đơn</center>" }],
                        width: 525,
                        // template: function (obj, common) {
                        //     if (obj.$group) return common.treetable(obj, common) + obj.invoiceNumber;
                        //     return obj.invoiceNumber;
                        //}
                        template: function (obj, common) {
                            if (obj.$level == 1) return common.treetable(obj, common) + "<span style='color:red;'>" + obj.value + " ( " + obj.$count + " )" + "</span>";
                            //return obj.invoiceNumber;
                            return "<a href='JavaScript:Void(0);'> " + obj.invoiceNumber + "</a>"
                        }
                    },
                    {
                        id: "invoiceDate", header: [null, { "text": "<center>Ngày hóa đơn</center>" }], width: 150, format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        }
                    },
                    { id: "sellerName", header: ["<center>Tên người mua</center>", { content: "textFilter" }], width: 300 },
                    { id: "sellerTaxCode", header: "<center>Mã số thuế</center>", width: 150 },
                    {
                        id: "totalAmountWithoutVat", header: "<center>Doanh thu chưa có thuế GTGT</center>", width: 200, format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0
                        }),
                        template: function (obj, common) {
                            if (obj.$level == 1) return "<b><span style='color:red;'>" + fm(obj.totalAmountWithoutVat) + "</span></b>";
                            return obj.totalAmountWithoutVat == null ? 0 : fm(obj.totalAmountWithoutVat);
                        }
                    },
                    {
                        id: "vatAmount", header: "<center>Thuế GTGT</center>", width: 100, format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0
                        }),
                        template: function (obj, common) {
                            if (obj.$level == 1) return "<b><span style='color:red;'>" + fm(obj.vatAmount) + "</span></b>";
                            return obj.vatAmount == null ? 0 : fm(obj.vatAmount);
                        }
                    },
                    { id: "ghi_chu", header: "<center>Ghi chú</center>", width: 150 }
                ],
                pager: "pagerA",
                scheme: {
                    $group: {
                        by: "ten_loai",
                        //css:"multiline",
                        map: {
                            vatAmount: ["vatAmount", "sum"],
                            totalAmountWithoutVat: ["totalAmountWithoutVat", "sum"]
                            //title: ["year"]
                        }
                    },
                    //$sort: { by: "stt", dir: "asc" }
                },
                ready: function () {
                    //this.open(this.getFirstId());
                    //this.sort({ by: "stt", dir: "asc" });
                    this.openAll();
                },
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}"
            }
        ]
    }

    var layout = {
        rows: [
            {
                height: 35,
                type: "toolbar",
                cols: controls
            },
            {
                rows: [
                    grid
                ]
            }
        ]

    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {

            $scope.on(app, "dataChangedMain", function (editmode, item) {

                var table = webix.$$("mainData");

                if (editmode == 1) {
                    table.clearAll();
                    table.load(table.config.url);
                    // console.log(item);
                    // console.log(item.ten_loai);
                    // table.add(item);
                    //table.select(item.id,false);
                }

                if (editmode == 2) {
                    table.clearAll();
                    table.load(table.config.url);
                    // var selectedItem = table.getSelectedItem();
                    // table.updateItem(selectedItem.id, item);

                    // webix.UIManager.setFocus($$(table.config.id));
                }

            });
            // var table = $$("mainData");
            // var url = {
            //     $proxy: true,
            //     load: function (view, callback, params) {
            //         console.log(view.config);
            //         console.log(callback);
            //         console.log(params);
            //         /*var windowno = app.path[1].params[0];
            //         var url = app.config.host + "/System/GetDataByWindowNo?window_id=" + windowno;

            //         if (params == null) {
            //             url = url + "&start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
            //         }
            //         else {
            //             url = url + "&start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

            //             if (params.filter != null) {
            //                 var array = [];

            //                 for (var property in params.filter) {

            //                     var column = view.getColumnConfig(property);

            //                     if (column !== undefined) {
            //                         var item = {
            //                             columnName: property,
            //                             columnType: column.columnType,
            //                             value: params.filter[property]
            //                         };

            //                         array.push(item);
            //                     }
            //                 }

            //                 //console.log(JSON.stringify(array));
            //                 if (array.length > 0) {
            //                     url = url + "&filter=" + JSON.stringify(array);
            //                 }
            //                 else {
            //                     url = url + "&filter=null";
            //                 }

            //             }
            //             else {
            //                 url = url + "&filter=null";
            //             }
            //         }

            //         if (view.config.infoparam != null) {
            //             url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
            //         }
            //         else {
            //             url = url + "&infoparam=null";
            //         }

            //         var tlbParams = $$("tlbParams");

            //         if (tlbParams !== undefined) {
            //             var childViews = tlbParams.getChildViews();

            //             var parameters = [];

            //             for (var i = 0; i < childViews.length; i++) {
            //                 var child = childViews[i];

            //                 var id = child.config.id;
            //                 var value_field = child.config.value_field;

            //                 var item = {
            //                     columnName: value_field,
            //                     value: $$(id).getValue()
            //                 };

            //                 parameters.push(item);
            //             }

            //             url = url + "&tlbparam=" + JSON.stringify(parameters);

            //         }
            //         else {
            //             url = url + "&tlbparam=null";
            //         }*/

            //         //console.log(url);

            //         /* webix.ajax().bind(view)
            //             .get(url,function(text){
            //                 var json =JSON.parse(text);

            //                 this.clearAll();

            //                 if (!json.hasOwnProperty("error")){
            //                     this.parse(json.data,"json");
            //                 }
            //         }); */

            //         //webix.ajax(url, callback, view);

            //     }
            // };

            // table.define("url", url);

        }
    };
});