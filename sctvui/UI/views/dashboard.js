define([
	"views/modules/dashline",
	"views/modules/dashline78",

	// "views/modules/data_invoice",
	// "views/menus/version",
	"views/modules/chart",
	"app",
	"models/user"
], function (dashline, dashline78, chart, app, user) {

	webix.$$("title").parse({ title: "Trang chủ", details: "Phân tích và thống kê" });
	//var soluong = version.getWarning();
	var layout = {
		type: "clean",
		borderless: true,
		paddingX: 20,
		rows: [
			//dashline,
			{ height: 15 },
			{
				// paddingX: 35,
				css: "dash_invoice",
				type: "space",
				rows: [dashline]
			},
			{ height: 8 },
			{
				css: "dash_invoice",
				type: "space",
				rows: [dashline78]

			},
			// {
			// 	cols: [
			// 		{
			// 			// paddingX: 18,
			// 			css: "dash_invoice",
			// 			type: "space",
			// 			rows: [chart]
			// 		},
			// 		{ width: 10 },
			// 		{
			// 			// paddingX: 30,
			// 			css: "dash_invoice",
			// 			type: "space",
			// 			rows: []
			// 		}
			// 	]
			// }
			// chart,
			// {
			// 	type: "space",
			// 	rows: [
			// 		{
			// 			type: "wide",
			// 			cols: [
			// 				invoice,
			// 				//orders
			// 			]
			// 		},
			// 		/* {
			// 			type: "wide",
			// 			cols: [
			// 				messages,
			// 				revenue
			// 			]
			// 		},
			// 		{
			// 			type: "wide",
			// 			cols: [
			// 				tasks,
			// 				map
			// 			]
			// 		} */
			// 	]
			// }
		]
	};

	return {
		$ui: layout,
		$oninit: function (view, $scope) {
			var item = {
				command: "NOTYFI_INV_TEMP",
				parameter: {
					username: user.getCurrentUser()
				}
			}

			webix.ajax()
				.headers({ "Content-Type": "application/json" })
				.post(app.config.host + "/System/ExecuteCommand", item, function (text) {

					var res = JSON.parse(text);

					if (res.length > 0 && res != null) {
						var mess = "Thông báo sắp hết hóa đơn :";
						for (var i = 0; i < res.length; i++) {
							mess = mess + "mẫu số: " + res[i].template_code + " ký hiệu: " + res[i].invoice_series;
							// + res[i].quantity;
						}
						webix.message(mess, "debug", 5000);
					}
				});

			var item1 = {
				command: "NOTYFI_INV_TEMP_78",
				parameter: {
					username: user.getCurrentUser()
				}
			}

			webix.ajax()
				.headers({ "Content-Type": "application/json" })
				.post(app.config.host + "/System/ExecuteCommand", item1, function (text) {

					var res = JSON.parse(text);

					if (res.length > 0 && res != null) {
						var mess = "Thông báo sắp hết hóa đơn 78: </br>";
						for (var i = 0; i < res.length; i++) {
							mess = mess + " ký hiệu: " + res[i].ky_hieu + "</br>";
							// + res[i].quantity;
						}
						webix.message(mess, "debug", 5000);
					}
				});
			//console.log(soluong);
			// if (soluong.length > 0 && soluong != null) {
			// 	var mess = "Thông báo :"
			// 	for (var i = 0; i < soluong.length; i++) {
			// 		mess = mess + soluong[i].thongbao;
			// 	}
			// 	webix.message(mess, "debug", 5000);
			// }
		}
	};

});