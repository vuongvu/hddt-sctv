define([
    "app",
    "locale",
    "views/forms/license",
], function (app, _, licenseForm) {
    'use strict';

    webix.$$("title").parse({ title: "Đăng ký bản quyền", details: "Đăng ký số lượng hóa đơn sử dụng" });

    var controls = [
        {
            id: "btnRegFirst",
            view: "button",
            type: "form",
            icon: "registered",
            label: _("DANG_KY_LAN_DAU"),
            width: 150,
            on: {
                onClick: function () {

                    webix.ajax().bind(this).get(app.config.host + "/License/Info", function (text) {
                        var json = JSON.parse(text);

                        if (json.key_license != undefined) {
                            webix.alert({
                                title: _("THONG_BAO"),
                                ok: "OK",
                                text: "Phần mềm đã được đăng ký lần đầu"
                            });
                        }
                        else {
                            var win = this.$scope.ui(licenseForm.$ui);
                            win.getHead().setHTML(_("DANG_KY_LAN_DAU"));
                            win.show();
                            $$("license-form").setValues(json);
                        }
                    });
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnRegMore",
            view: "button",
            type: "danger",
            icon: "registered",
            label: _("DANG_KY_THEM"),
            width: 150,
            on: {
                onClick: function () {

                    webix.ajax().bind(this).get(app.config.host + "/License/Info", function (text) {
                        var json = JSON.parse(text);

                        if (json.key_license != undefined) {
                            var win = this.$scope.ui(licenseForm.$ui);
                            win.getHead().setHTML(_("DANG_KY_THEM"));
                            win.show();
                            $$("license-form").setValues(json);
                        }
                        else {

                            webix.alert({
                                title: _("THONG_BAO"),
                                ok: "OK",
                                text: "Phần mềm chưa được đăng ký lần đầu"
                            });
                        }
                    });


                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {}
    ];

    var layout = {
        rows: [
            {
                height: 35,
                type: "toolbar",
                cols: controls
            },
            {
                view: "iframe",
                id: "framePdf",
                toolbar: "toolbar",
                //src: app.config.host + "/License/PrintLicenseInfo"

            }
        ]

    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var url = app.config.host + "/License/PrintLicenseInfo";

            var form = view;
            webix.extend(form, webix.ProgressBar);
            form.disable();
            form.showProgress();
            webix.ajax()
                .response("arraybuffer")
                .get(url, {
                    error: function (text, data, XmlHttpRequest) {
                        form.enable();
                        form.hideProgress();
                        alert(text);
                    },
                    success: function (text, data, XmlHttpRequest) {
                        var file = new Blob([data], { type: 'application/pdf' });
                        var fileURL = URL.createObjectURL(file);

                        var framePdf = $$("framePdf");
                        framePdf.load(fileURL);

                        form.enable();
                        form.hideProgress();
                    }
                });
        }

    };
});