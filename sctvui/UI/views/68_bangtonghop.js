define(["app", "locale", "models/user", "views/forms/68_bangtonghopform", "views/forms/68_bangdanhsachhoadon", "views/forms/68_hoadon_history"], function (
    app,
    _,
    user,
    bangtonghopform,
    bangdanhsachhoadon,
    hoadonHistory
) {
    "use strict";

    webix.$$("title").parse({
        title: "Bảng tổng hợp hóa đơn",
        details: "Bảng tổng hợp hóa đơn",
    });

    var initUI = function () {
        var table = webix.$$("bangtonghop");
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var url = app.config.host + "/Invoice68/GetBangTHDL";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("bangtonghop");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(bangtonghopform.$ui);
                    bangtonghopform.setEditMode(1);
                    detailForm.show();

                    var url = app.config.host + "/Invoice68/GetInfoCompany";

                    webix.ajax(url, function (text) {
                        var json = JSON.parse(text);
                        webix.$$("tnnt").setValue(json.name);
                        webix.$$("mst").setValue(json.tax_code);
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var table = webix.$$("bangtonghop");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn dòng nào ! ", "error");
                        return;
                    }

                    var detailForm = this.$scope.ui(bangtonghopform.$ui);
                    bangtonghopform.setEditMode(2);
                    bangtonghopform.setValues(selectedItem);
                    if (selectedItem.trang_thai == 1) {
                        var form = $$("bangtonghop-form").elements;
                        for (var key in form) {
                            var $el = form[key];
                            $el.define("readonly", "true");
                            $el.define("css", "mcls_readonly_text");
                            $el.refresh();
                        }

                        $$("btnBTHSave").destructor();
                        $$("btnTaiHoaDon").destructor();
                        $$("btnXoaHoaDon").destructor();
                    }
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("bangtonghop");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn khách hàng ! ", "error");
                        return;
                    }
                    if (selectedItem.trang_thai == 1) {
                        webix.message("Bảng tổng hợp đã gửi CQT không thể xóa ! ", "error");
                        return;
                    }

                    var khData = {};

                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(selectedItem);

                    webix.confirm({
                        text: "Bạn có muốn xóa bản ghi",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/SaveBangTHDL", khData, function (text) {
                                        var response = JSON.parse(text);

                                        if (response.hasOwnProperty("error")) {
                                            webix.message({ type: "error", text: response.error });
                                            return;
                                        }

                                        // table.remove(item.id);
                                        var btnRefresh = $$("btnRefresh");
                                        btnRefresh.callEvent("onClick");
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-eye fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + "In (F7)" + "</span>",
            id: "btnPreview",
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = webix.$$("bangtonghop");
                    var selectedItem = table.getSelectedItem();

                    if (selectedItem == null) {
                        webix.message("Bạn chưa dòng in ! ", "error");
                        return;
                    }
                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);
                    if (selectedItem.lhhoa == 1) {
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inBTHDLieuXangDau?id=" + selectedItem.id, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    } else {
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inBTHDLieu?id=" + selectedItem.id, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },

        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-excel fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + "Export Excel" + "</span>",
            //id: "btnPreview",
            //shortcut: "F7",
            on: {
                onClick: function () {
                    var table = webix.$$("bangtonghop");
                    var selectedItem = table.getSelectedItem();

                    if (selectedItem == null) {
                        webix.message("Bạn chưa dòng in ! ", "error");
                        return;
                    }
                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    // var newWin = window.open();
                    // var html =
                    //     "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                    //     '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                    //     '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                    //     '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    // newWin.document.write(html);

                    if (selectedItem.lhhoa == 1) {
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/excelBTHDLieuXangDau?id=" + selectedItem.id, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                                    webix.modalbox.hide(box);
                                    webix.html.download(file, "bthdl.xlsx");
                                },
                            });
                    } else {
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/excelBTHDLieu?id=" + selectedItem.id, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                                    webix.modalbox.hide(box);
                                    webix.html.download(file, "bthdl.xlsx");
                                },
                            });
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        // {
        //     view: "button",
        //     type: "htmlbutton",
        //     width: 100,
        //     label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Gửi email") + "</span>",
        //     id: "btnSendEmail",
        //     on: {
        //         onClick: function () {
        //             var table = webix.$$("bangtonghop");
        //             var selectedItem = table.getSelectedItem();
        //             if (selectedItem == null) {
        //                 webix.message("Bạn chưa chọn khách hàng ! ", "error");
        //                 return;
        //             }
        //         },
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     },
        // },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-pencil fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Ký gửi CQT") + "</span>",
            id: "btnSign",
            on: {
                onClick: function () {
                    var tableDK = webix.$$("bangtonghop");
                    /*var selectedItem = tableDK.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn dòng ! ", "error");
                        return;
                    }
                    if (selectedItem.trang_thai == 1) {
                        webix.message("Mẫu đã ký và gửi CQT bạn không thể ký lại! ", "error");
                        return;
                    }*/
                    var datafrm = tableDK.serialize();

                    var data_id = [];
                    var data_id_xd = [];

                    if (datafrm.length > 0) {
                        for (var i = 0; i < datafrm.length; i++) {
                            if (datafrm[i] != undefined) {
                                if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true && datafrm[i].trang_thai != 1 && datafrm[i].lhhoa != 1) {
                                    data_id.push(datafrm[i].id);
                                }
                                if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true && datafrm[i].trang_thai != 1 && datafrm[i].lhhoa == 1) {
                                    data_id_xd.push(datafrm[i].id);
                                }
                            }
                        }
                    }
                    if (data_id.length > 0 && data_id_xd.length > 0) {
                        webix.message("Chỉ được chọn các bảng THDL cùng loại hàng hoá để kí!");
                        return;
                    }
                    if (data_id.length > 0 && data_id_xd.length == 0) {
                        webix
                            .ajax()
                            .headers({ "Content-Type": "application/json" })
                            .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                                var result = JSON.parse(text);
                                var kytaptrung = "";
                                for (var i = 0; i < result.length; i++) {
                                    var item = result[i];
                                    kytaptrung = item.value;
                                }

                                webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                    var listcerts = JSON.parse(text);
                                    if (listcerts.length > 0) {
                                        var box = webix.modalbox({
                                            title: "Chọn chứng thư số",
                                            buttons: ["Nhận", "Hủy bỏ"],
                                            text: "<div id='ctnCerts'></div>",
                                            top: 100,
                                            width: 750,
                                            callback: function (result) {
                                                switch (result) {
                                                    case "1":
                                                        webix.modalbox.hide();
                                                        break;
                                                    case "0":
                                                        var table = $$("tblCerts");
                                                        var data = table.serialize();
                                                        var cert = null;

                                                        for (var i = 0; i < data.length; i++) {
                                                            if (data[i].chon == true) {
                                                                cert = data[i];
                                                                break;
                                                            }
                                                        }

                                                        if (cert == null) {
                                                            webix.message("Bạn chưa chọn chứng thư số");
                                                            return;
                                                        }
                                                        var postData = {
                                                            branch_code: user.getDvcsID(),
                                                            username: user.getCurrentUser(),
                                                            cer_serial: cert.cer_serial,
                                                            CheckHSM: "",
                                                            id: data_id,
                                                        };

                                                        webix.extend(tableDK, webix.ProgressBar);
                                                        tableDK.disable();
                                                        tableDK.showProgress();
                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-type": "application/json" })
                                                            .post(app.config.host + "/Invoice68/SignBTHDL", postData, {
                                                                error: function (text, data, XmlHttpRequest) {
                                                                    tableDK.enable();
                                                                    tableDK.hideProgress();
                                                                    // var json = JSON.parse(text);
                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                },
                                                                success: function (text, data, XmlHttpRequest) {
                                                                    var json = JSON.parse(text);
                                                                    tableDK.enable();
                                                                    tableDK.hideProgress();
                                                                    if (!json.hasOwnProperty("error")) {
                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                        // $$("huyhoadonForms").close();
                                                                        app.callEvent("dataChangeBangtonghop", [1, null]);
                                                                    } else {
                                                                        webix.message(json.error, "error");
                                                                    }
                                                                },
                                                            });
                                                        return;
                                                }
                                            },
                                        });
                                        var tblMatHang = webix.ui({
                                            container: "ctnCerts",
                                            id: "tblCerts",
                                            view: "datatable",
                                            borderless: true,
                                            resizeColumn: true,
                                            height: 350,
                                            columns: [
                                                {
                                                    id: "chon",
                                                    header: _("CHON"),
                                                    width: 65,
                                                    checkValue: true,
                                                    uncheckValue: false,
                                                    template: "{common.checkbox()}",
                                                },
                                                {
                                                    id: "cert_type",
                                                    header: _("Loại"),
                                                    width: 100,
                                                },
                                                {
                                                    id: "cer_serial",
                                                    header: _("SO_SERIAL"),
                                                    width: 150,
                                                },
                                                {
                                                    id: "subject_name",
                                                    header: _("SUBJECTNAME"),
                                                    width: 450,
                                                },
                                                {
                                                    id: "begin_date",
                                                    header: _("NGAY_BAT_DAU"),
                                                    width: 200,
                                                },
                                                {
                                                    id: "end_date",
                                                    header: _("NGAY_KET_THUC"),
                                                    width: 200,
                                                },
                                                {
                                                    id: "issuer",
                                                    header: _("DONVI"),
                                                    width: 450,
                                                },
                                            ],
                                            url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                        });
                                    } else {
                                        var table = $$("bangtonghop");
                                        webix.extend(table, webix.ProgressBar);
                                        table.disable();
                                        table.showProgress();

                                        // xem user có tích checkbox ký HSM không?
                                        var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                                        webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                            if (resultscheckSigHSM[0].json() == "C") {
                                                var postData = {
                                                    branch_code: user.getDvcsID(),
                                                    username: user.getCurrentUser(),
                                                    id: data_id,
                                                    CheckHSM: "1",
                                                };

                                                webix.extend(tableDK, webix.ProgressBar);
                                                tableDK.disable();
                                                tableDK.showProgress();
                                                webix
                                                    .ajax()
                                                    .headers({ "Content-type": "application/json" })
                                                    .post(app.config.host + "/Invoice68/SignBTHDL", postData, {
                                                        error: function (text, data, XmlHttpRequest) {
                                                            tableDK.enable();
                                                            tableDK.hideProgress();
                                                            webix.message(text + XmlHttpRequest, "error");
                                                        },
                                                        success: function (text, data, XmlHttpRequest) {
                                                            var json = JSON.parse(text);
                                                            tableDK.enable();
                                                            tableDK.hideProgress();
                                                            if (!json.hasOwnProperty("error")) {
                                                                webix.message("Gửi thành công đến CQT !", "success");
                                                                app.callEvent("dataChangeBangtonghop", [1, null]);
                                                            } else {
                                                                webix.message(json.error, "error");
                                                            }
                                                        },
                                                    });
                                            } else {
                                                var parameters_list = [];

                                                var k = 0; //khai báo biến k để chạy lần lượt từng hoá đơn theo id
                                                function excuteHdPlugin() {
                                                    if (k < data_id.length) {
                                                        var hdon_id = data_id[k];
                                                        var a = webix.ajax(app.config.host + "/Invoice68/XmlBTHDL?id=" + hdon_id);
                                                        var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                                        webix.promise.all([a, b, hdon_id]).then(function (results) {
                                                            var json = results[0].json();

                                                            if (json.hasOwnProperty("error")) {
                                                                webix.message({ type: "error", text: json.error });
                                                            }

                                                            var listcerts = results[1].json();
                                                            var item = results[2];

                                                            if (listcerts.length == 0) {
                                                                table.enable();
                                                                table.hideProgress();
                                                                webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                                return;
                                                            }

                                                            require(["views/modules/minvoiceplugin"], function (plugin) {
                                                                var hub = plugin.hub();
                                                                var hubconnect = plugin.conn();
                                                                if (hubconnect.state != 1) {
                                                                    table.enable();
                                                                    table.hideProgress();
                                                                    webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                                    return;
                                                                }
                                                                hub.off("resCommand");
                                                                hub.on("resCommand", function (result) {
                                                                    var json = JSON.parse(result);

                                                                    if (json.hasOwnProperty("error")) {
                                                                        webix.message({ type: "error", text: json.error });
                                                                    } else {
                                                                        var parameters = {
                                                                            xml: json.xml,
                                                                            id: item,
                                                                            shdon: json.shdon,
                                                                        };
                                                                        parameters_list.push(parameters);
                                                                    }
                                                                    k++;
                                                                    excuteHdPlugin();
                                                                });

                                                                var _listcerts = [];

                                                                for (var i = 0; i < listcerts.length; i++) {
                                                                    var _cert = listcerts[i];

                                                                    _listcerts.push({
                                                                        so_serial: _cert.cer_serial,
                                                                        ngaybatdau: _cert.begin_date,
                                                                        ngayketthuc: _cert.end_date,
                                                                        issuer: _cert.issuer,
                                                                        subjectname: _cert.subject_name,
                                                                    });
                                                                }

                                                                var arg = {
                                                                    idData: "#data",
                                                                    xml: json["xml"],
                                                                    shdon: json.shdon,
                                                                    idSigner: "seller",
                                                                    tagSign: "NNT",
                                                                    listcerts: _listcerts,
                                                                };

                                                                hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                                    .done(function () {
                                                                        console.log("Success !");
                                                                    })
                                                                    .fail(function (error) {
                                                                        webix.message(error, "error");
                                                                    });
                                                            });
                                                        });
                                                    } else {
                                                        if (parameters_list.length > 0) {
                                                            webix
                                                                .ajax()
                                                                .headers({ "Content-Type": "application/json" })
                                                                .post(app.config.host + "/Invoice68/SaveXmlBTHDL", JSON.stringify(parameters_list), function (text) {
                                                                    var result = JSON.parse(text);
                                                                    if (result.hasOwnProperty("error")) {
                                                                        webix.message(result.error, "error");
                                                                    } else {
                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                        app.callEvent("dataChangeBangtonghop", [1, null]);
                                                                    }
                                                                });
                                                        }
                                                        table.enable();
                                                        table.hideProgress();
                                                    }
                                                }

                                                excuteHdPlugin();
                                            }
                                        });
                                    }
                                });
                            });
                    } else if (data_id_xd.length > 0 && data_id == 0) {
                        webix
                            .ajax()
                            .headers({ "Content-Type": "application/json" })
                            .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                                var result = JSON.parse(text);
                                var kytaptrung = "";
                                for (var i = 0; i < result.length; i++) {
                                    var item = result[i];
                                    kytaptrung = item.value;
                                }

                                webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                    var listcerts = JSON.parse(text);
                                    if (listcerts.length > 0) {
                                        var box = webix.modalbox({
                                            title: "Chọn chứng thư số",
                                            buttons: ["Nhận", "Hủy bỏ"],
                                            text: "<div id='ctnCerts'></div>",
                                            top: 100,
                                            width: 750,
                                            callback: function (result) {
                                                switch (result) {
                                                    case "1":
                                                        webix.modalbox.hide();
                                                        break;
                                                    case "0":
                                                        var table = $$("tblCerts");
                                                        var data = table.serialize();
                                                        var cert = null;

                                                        for (var i = 0; i < data.length; i++) {
                                                            if (data[i].chon == true) {
                                                                cert = data[i];
                                                                break;
                                                            }
                                                        }

                                                        if (cert == null) {
                                                            webix.message("Bạn chưa chọn chứng thư số");
                                                            return;
                                                        }
                                                        var postData = {
                                                            branch_code: user.getDvcsID(),
                                                            username: user.getCurrentUser(),
                                                            cer_serial: cert.cer_serial,
                                                            /*id: selectedItem.id,*/
                                                            id: data_id_xd,
                                                        };

                                                        webix.extend(tableDK, webix.ProgressBar);
                                                        tableDK.disable();
                                                        tableDK.showProgress();
                                                        webix
                                                            .ajax()
                                                            .headers({ "Content-type": "application/json" })
                                                            .post(app.config.host + "/Invoice68/SignBTHDLXangDau", postData, {
                                                                error: function (text, data, XmlHttpRequest) {
                                                                    tableDK.enable();
                                                                    tableDK.hideProgress();
                                                                    // var json = JSON.parse(text);
                                                                    // webix.message(json.ExceptionMessage, "error");
                                                                    webix.message(text + XmlHttpRequest, "error");
                                                                },
                                                                success: function (text, data, XmlHttpRequest) {
                                                                    var json = JSON.parse(text);
                                                                    tableDK.enable();
                                                                    tableDK.hideProgress();
                                                                    if (!json.hasOwnProperty("error")) {
                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                        // $$("huyhoadonForms").close();
                                                                        app.callEvent("dataChangeBangtonghop", [1, null]);
                                                                    } else {
                                                                        webix.message(json.error, "error");
                                                                    }
                                                                },
                                                            });
                                                        return;
                                                }
                                            },
                                        });
                                        var tblMatHang = webix.ui({
                                            container: "ctnCerts",
                                            id: "tblCerts",
                                            view: "datatable",
                                            borderless: true,
                                            resizeColumn: true,
                                            height: 350,
                                            columns: [
                                                {
                                                    id: "chon",
                                                    header: _("CHON"),
                                                    width: 65,
                                                    checkValue: true,
                                                    uncheckValue: false,
                                                    template: "{common.checkbox()}",
                                                },
                                                {
                                                    id: "cer_serial",
                                                    header: _("SO_SERIAL"),
                                                    width: 150,
                                                },
                                                {
                                                    id: "subject_name",
                                                    header: _("SUBJECTNAME"),
                                                    width: 450,
                                                },
                                                {
                                                    id: "begin_date",
                                                    header: _("NGAY_BAT_DAU"),
                                                    width: 200,
                                                },
                                                {
                                                    id: "end_date",
                                                    header: _("NGAY_KET_THUC"),
                                                    width: 200,
                                                },
                                                {
                                                    id: "issuer",
                                                    header: _("DONVI"),
                                                    width: 450,
                                                },
                                            ],
                                            url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                        });
                                    } else {
                                        var table = $$("bangtonghop");
                                        webix.extend(table, webix.ProgressBar);
                                        table.disable();
                                        table.showProgress();

                                        // xem user có tích checkbox ký HSM không?
                                        var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                                        webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                            if (resultscheckSigHSM[0].json() == "C") {
                                                var postData = {
                                                    branch_code: user.getDvcsID(),
                                                    username: user.getCurrentUser(),
                                                    id: data_id_xd,
                                                    CheckHSM: "1",
                                                };

                                                webix.extend(tableDK, webix.ProgressBar);
                                                tableDK.disable();
                                                tableDK.showProgress();
                                                webix
                                                    .ajax()
                                                    .headers({ "Content-type": "application/json" })
                                                    .post(app.config.host + "/Invoice68/SignBTHDLXangDau", postData, {
                                                        error: function (text, data, XmlHttpRequest) {
                                                            tableDK.enable();
                                                            tableDK.hideProgress();
                                                            webix.message(text + XmlHttpRequest, "error");
                                                        },
                                                        success: function (text, data, XmlHttpRequest) {
                                                            var json = JSON.parse(text);
                                                            tableDK.enable();
                                                            tableDK.hideProgress();
                                                            if (!json.hasOwnProperty("error")) {
                                                                webix.message("Gửi thành công đến CQT !", "success");
                                                                app.callEvent("dataChangeBangtonghop", [1, null]);
                                                            } else {
                                                                webix.message(json.error, "error");
                                                            }
                                                        },
                                                    });
                                            } else {
                                                var parameters_list = [];

                                                var k = 0; //khai báo biến k để chạy lần lượt từng hoá đơn theo id
                                                function excuteHdPlugin() {
                                                    if (k < data_id_xd.length) {
                                                        var hdon_id = data_id_xd[k];
                                                        var a = webix.ajax(app.config.host + "/Invoice68/XmlBTHDLXangDau?id=" + hdon_id);
                                                        var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                                        webix.promise.all([a, b, hdon_id]).then(function (results) {
                                                            var json = results[0].json();

                                                            if (json.hasOwnProperty("error")) {
                                                                webix.message({ type: "error", text: json.error });
                                                            }

                                                            var listcerts = results[1].json();
                                                            var item = results[2];

                                                            if (listcerts.length == 0) {
                                                                table.enable();
                                                                table.hideProgress();
                                                                webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                                return;
                                                            }

                                                            require(["views/modules/minvoiceplugin"], function (plugin) {
                                                                var hub = plugin.hub();
                                                                var hubconnect = plugin.conn();
                                                                if (hubconnect.state != 1) {
                                                                    table.enable();
                                                                    table.hideProgress();
                                                                    webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                                    return;
                                                                }
                                                                hub.off("resCommand");
                                                                hub.on("resCommand", function (result) {
                                                                    var json = JSON.parse(result);

                                                                    if (json.hasOwnProperty("error")) {
                                                                        webix.message({ type: "error", text: json.error });
                                                                    } else {
                                                                        var parameters = {
                                                                            xml: json.xml,
                                                                            id: item,
                                                                            shdon: json.shdon,
                                                                        };
                                                                        parameters_list.push(parameters);
                                                                    }
                                                                    k++;
                                                                    excuteHdPlugin();
                                                                });

                                                                var _listcerts = [];

                                                                for (var i = 0; i < listcerts.length; i++) {
                                                                    var _cert = listcerts[i];

                                                                    _listcerts.push({
                                                                        so_serial: _cert.cer_serial,
                                                                        ngaybatdau: _cert.begin_date,
                                                                        ngayketthuc: _cert.end_date,
                                                                        issuer: _cert.issuer,
                                                                        subjectname: _cert.subject_name,
                                                                    });
                                                                }

                                                                var arg = {
                                                                    idData: "#data",
                                                                    xml: json["xml"],
                                                                    shdon: json.shdon,
                                                                    idSigner: "seller",
                                                                    tagSign: "NNT",
                                                                    listcerts: _listcerts,
                                                                };

                                                                hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                                    .done(function () {
                                                                        console.log("Success !");
                                                                    })
                                                                    .fail(function (error) {
                                                                        webix.message(error, "error");
                                                                    });
                                                            });
                                                        });
                                                    } else {
                                                        if (parameters_list.length > 0) {
                                                            webix
                                                                .ajax()
                                                                .headers({ "Content-Type": "application/json" })
                                                                .post(app.config.host + "/Invoice68/SaveXmlBTHDLXangDau", JSON.stringify(parameters_list), function (text) {
                                                                    var result = JSON.parse(text);
                                                                    if (result.hasOwnProperty("error")) {
                                                                        webix.message(result.error, "error");
                                                                    } else {
                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                        app.callEvent("dataChangeBangtonghop", [1, null]);
                                                                    }
                                                                });
                                                        }
                                                        table.enable();
                                                        table.hideProgress();
                                                    }
                                                }

                                                excuteHdPlugin();
                                            }
                                        });
                                    }
                                });
                            });
                    } else {
                        webix.message("Chưa tích chọn bảng hoặc bảng đã gửi CQT!");
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-tasks fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Lập bổ sung") + "</span>",
            id: "btnUp",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(bangtonghopform.$ui);
                    bangtonghopform.setEditMode(1);
                    detailForm.show();

                    webix.$$("ldau").setValue(0);

                    var url = app.config.host + "/Invoice68/GetInfoCompany";

                    webix.ajax(url, function (text) {
                        var json = JSON.parse(text);
                        webix.$$("tnnt").setValue(json.name);
                        webix.$$("mst").setValue(json.tax_code);
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-download fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Tải XML") + "</span>",
            id: "btnDownloadXml",
            on: {
                onClick: function () {
                    var table = webix.$$("bangtonghop");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn khách hàng ! ", "error");
                        return;
                    }
                    if (selectedItem.trang_thai != 1) {
                        webix.message("Bảng tổng hợp chưa gửi CQT không tồn tại xml ! ", "error");
                        return;
                    }

                    webix.extend(table, webix.ProgressBar);
                    table.disable();
                    table.showProgress();

                    var url = app.config.host + "/Invoice68/ExportXMLBangTHDL?id=" + selectedItem.id;
                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            table.enable();
                            table.hideProgress();

                            var file = new Blob([data], { type: "application/xml" });
                            webix.html.download(file, "BangTHDL.xml");
                            // var fileURL = URL.createObjectURL(file);
                            // window.open(fileURL, "_blank");
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "bangtonghop",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function (obj) {},
                },
                onClick: {
                    DowloadBTHLDRep: function (event, cell, target) {
                        var win = webix.$$("bangtonghop");
                        var record = win.getItem(cell.row);

                        if (record.trang_thai != 1) {
                            webix.message("Mẫu chưa được ký không thể tải !", "error");
                            return;
                        }

                        if (record.ket_qua == null || record.ket_qua == "") {
                            webix.message("CQT chưa phản hồi !", "error");
                            return;
                        }

                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();
                        var filename = "KetQuaKiemTraBTHDL.xml";
                        var url = app.config.host + "/Invoice68/ExportXMLBTHDLRep?id=" + cell.row;
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .get(url, function (text, data) {
                                win.enable();
                                win.hideProgress();
                                var file = new Blob([data], { type: "application/xml" });
                                webix.html.download(file, filename);
                            });

                        return false;
                    },
                    InBTHDLRep: function (event, cell, target) {
                        var table = webix.$$("bangtonghop");
                        var item = table.getItem(cell.row);

                        if (item.ket_qua == null || item.ket_qua == "") {
                            webix.message("CQT chưa phản hồi !", "error");
                            return;
                        }

                        var box = webix.modalbox({
                            title: "Đang in hóa đơn ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        var newWin = window.open();
                        var html =
                            "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                            '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                            '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                            '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                        newWin.document.write(html);

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inBTHDLRep?id=" + item.id + "&type=PDF", {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    },

                    ShowHistory: function (event, cell, target) {
                        var table = webix.$$("bangtonghop");
                        var win = table.$scope.ui(hoadonHistory.$ui);
                        var hdon_id = cell.row;
                        hoadonHistory.setEditMode(hdon_id);
                        win.show();
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.index = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onAfterLoad: function () {},
                },
                columns: [
                    {
                        id: "chon",
                        header: [{ text: "", content: "masterCheckbox" }],
                        width: 30,
                        checkValue: true,
                        uncheckValue: false,
                        template: "{common.checkbox()}",
                    },
                    {
                        id: "index",
                        header: "STT",
                        sort: "int",
                        width: 40,
                    },
                    {
                        id: "trang_thai",

                        header: [
                            "Trạng thái gửi CQT",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 150,
                        template: function (obj) {
                            return obj.trang_thai == 1
                                ? "<span style='color: #00b300'><i class='fas fa-check'></i></span> Đã gửi CQT"
                                : "<span style='color: red'><i class='fas fa-times'></i></span> Chưa gửi CQT";
                        },
                    },
                    {
                        id: "ldau",
                        header: [
                            "Lần nộp",
                            {
                                content: "selectFilter",
                            },
                        ],
                        width: 120,
                        template: function (obj) {
                            if (obj.ldau == 1) {
                                return "<span style='color: #00b300'>Lần đầu</span> ";
                            } else {
                                return "<span style='color: #e83717'>Bổ sung</span> ";
                            }
                        },
                        css: {
                            "text-align": "center",
                        },
                        columnType: "numeric",
                    },
                    {
                        id: "btnMessagetn",
                        header: ["", "Lịch sử xử lý"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            return "<span class='ShowHistory' style='color: #00b300'><i class='fa fa-eye'></i></span> ";
                        },
                        width: 180,
                    },
                    {
                        id: "kdlieu",
                        header: [
                            "Kỳ dữ liệu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "ngay_ky",
                        header: [
                            "Ngày giờ ký",
                            {
                                content: "serverDateRangeFilter",
                            },
                        ],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y %H:%i:%s");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        css: {
                            "text-align": "center",
                        },
                        columnType: "datetime",
                    },

                    {
                        id: "sbthdlieu",
                        header: [
                            "Số thứ tự bảng",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "tgtcthue",
                        header: [
                            "Tổng tiền trước thuế",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 150,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 4,
                        }),
                        css: {
                            "text-align": "right",
                        },
                    },
                    {
                        id: "tgtthue",
                        header: [
                            "Tổng tiền thuế",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 150,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 4,
                        }),
                        css: {
                            "text-align": "right",
                        },
                    },
                    {
                        id: "tgtttoan",
                        header: [
                            "Tổng tiền thanh toán",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 150,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 4,
                        }),
                        css: {
                            "text-align": "right",
                        },
                    },
                    {
                        id: "nlap",
                        header: [
                            "Ngày lập",
                            {
                                content: "serverDateRangeFilter",
                            },
                        ],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        css: {
                            "text-align": "center",
                        },
                        columnType: "datetime",
                    },
                    {
                        id: "nglap",
                        header: [
                            "Người lập",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    // {
                    //     id: "mtdiep_gui",
                    //     header: [
                    //         "Mã thông điệp",
                    //         {
                    //             content: "textFilter",
                    //         },
                    //     ],
                    //     width: 150,
                    // },
                    {
                        id: "ngky",
                        header: [
                            "Người gửi",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "ket_qua",
                        header: ["", "Kết quả"],
                        css: { "text-align": "center" },
                        width: 150,
                        template: function (obj) {
                            if (obj.ket_qua == 1) {
                                return "<span style='color: #00b300'>Dữ liệu hợp lệ</span> ";
                            } else {
                                if (obj.ket_qua == 2) {
                                    return "<span style='color: #e83717'>Dữ liệu không hợp lệ</span> ";
                                } else {
                                    return "<span>Chờ phản hồi</span>";
                                }
                            }
                        },
                    },
                    {
                        id: "bthDowloadBTHLDRep",
                        header: ["", "Tải về"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.ket_qua != null && obj.ket_qua != "") {
                                return "<span class='DowloadBTHLDRep' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            } else return "<span class='DowloadBTHLDRep' style='color: #c8c8cc'><i class='fas fa-download'></i></span> ";
                        },
                        width: 120,
                    },
                    {
                        id: "btnInBTHDLRep",
                        header: ["", "In"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.ket_qua != null && obj.ket_qua != "") {
                                return "<span class='InBTHDLRep' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                            } else return "<span class='InBTHDLRep' style='color: #c8c8cc'><i class='fas fa-print'></i></span> ";
                        },
                        width: 120,
                    },
                    // {
                    //     id: "mtdiepcqt",
                    //     header: ["", "Mã thông điệp CQT"],
                    //     width: 250,
                    // },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };

    function loadData() {
        var table = webix.$$("bangtonghop");
        table.clearAll();
        table.load(table.config.url);
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var table = webix.$$("bangtonghop");
            webix.extend(view, webix.ProgressBar);

            $scope.on(app, "dataChangeBangtonghop", function (mode, item) {
                if (mode == 1) {
                    table.clearAll();
                    table.load(table.config.url);
                    // console.log(item);
                    // console.log(item.ten_loai);
                    // table.add(item);
                    //table.select(item.id,false);
                }

                if (mode == 2) {
                    table.clearAll();
                    table.load(table.config.url);
                    // var selectedItem = table.getSelectedItem();
                    // table.updateItem(selectedItem.id, item);

                    // webix.UIManager.setFocus($$(table.config.id));
                }
            });
        },

        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
