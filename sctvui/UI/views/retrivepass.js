/**
 * Created by Mads on 03-10-2015.
 */
define(["models/user", "locale", "app", "helpers/locale"], function (user, _, app, locale) {
    "use strict";

    var values = webix.copy(
        {
            username: "",
            password: "",
            rememberme: false,
            language: "vi",
            ma_dvcs: "VP",
        },
        Cookies.getJSON("Crm")
    );

    return {
        $ui: {
            cols: [
                { gravity: 1, template: "" },
                {
                    rows: [
                        { gravity: 1, template: "" },
                        {
                            css: { "background-color": "#cc0000", color: "white", "border-radius": "15px 15px 0px 0px" },
                            cols: [
                                {
                                    view: "template",
                                    type: "header",
                                    template: "Quên mật khẩu ?",
                                    css: { "background-color": "#cc0000", color: "white", "padding-bottom": "5px" },
                                    borderless: true,
                                    //width:180
                                },
                            ],
                        },
                        {
                            cols: [
                                {
                                    view: "form",
                                    gravity: 1,
                                    id: "loginForm",
                                    width: 360,
                                    elementsConfig: {
                                        labelPosition: "top",
                                    },
                                    elements: [
                                        {
                                            view: "template",
                                            template: "<center><a href='http://minvoice.vn/' target='_blank'><img  src='assets/imgs/LogoMinvoice.png' /></a></center>",
                                            height: 130,
                                            borderless: true,
                                            css: { padding: "0px" },
                                        },
                                        {
                                            height: 15,
                                        },
                                        {
                                            view: "text",
                                            //id:          "email",
                                            name: "email",
                                            label: "Nhập địa chỉ email để gửi mật khẩu mới",
                                            icon: "email",
                                            required: true,
                                            invalidMessage: "Bạn chưa nhập địa chỉ email hoặc email định dạng không đúng",
                                            placeholder: "Email",
                                            validate: webix.rules.isEmail,
                                        },
                                        {
                                            view: "button",
                                            //type:"danger",
                                            id: "login",
                                            name: "login",
                                            label: "Gửi yêu cầu tạo mật khẩu",
                                            //hotkey: "enter",
                                            css: "mcls_btnResetPass",
                                            click: function () {
                                                if ($$("loginForm").validate()) {
                                                    var form = $$("loginForm");

                                                    webix.extend(form, webix.ProgressBar);
                                                    form.disable();
                                                    form.showProgress();

                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(app.config.host + "/Account/ResetPass", $$("loginForm").getValues(), function (text, xml, xhr) {
                                                            var json = JSON.parse(text);

                                                            var form = $$("loginForm");
                                                            form.enable();
                                                            form.hideProgress();

                                                            if (json.hasOwnProperty("error")) {
                                                                webix.alert({
                                                                    title: "Thông báo",
                                                                    ok: "OK",
                                                                    text: json.error,
                                                                    type: "alert-error",
                                                                });
                                                            } else {
                                                                webix.alert({
                                                                    title: "Thông báo",
                                                                    ok: "OK",
                                                                    text: "Đã gửi mật khẩu tới email.Bạn hãy kiểm tra lại.",
                                                                });
                                                            }
                                                        });
                                                } else {
                                                    $$("loginForm").focus();
                                                }
                                            },
                                        },
                                        {
                                            view: "button",
                                            label: "Quay lại đăng nhập",
                                            type: "form",
                                            icon: "repeat",
                                            click: function () {
                                                app.router("/login");
                                            },
                                        },
                                    ],
                                },
                                {
                                    id: "version",
                                    //borderless: true,
                                    css: { background: "#e6f7ff !important" },
                                    width: 200,
                                    view: "template",
                                    template:
                                        "<div class=''><p><b>Bước 1:</b></p><label> Quý khách hàng vui lòng nhập Email đăng ký tài khoản !</label> <br />" +
                                        "<p><b>Bước 2:</b></p><label> Đăng nhập tài khoản Email vừa nhập !</label></div>" +
                                        "<p><b>Bước 3:</b></p><label> Hệ thống của SCTV sẽ gửi mật khẩu mới cho Quý khách hàng. Quý khách hàng dùng mật khẩu đó để đăng nhập vào phần mềm !</label></div>",
                                },
                            ],
                        },
                        { gravity: 1, temlate: "" },
                    ],
                },
                { gravity: 1, template: "" },
            ],
        },

        $oninit: function () {
            $$("loginForm").focus();
        },
    };
});
