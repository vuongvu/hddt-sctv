define(["app", "views/forms/tokenfile", "locale"], function (app, dmwindowform, _) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Đăng ký Token File mềm", details: "Danh sách Token File mềm" });

    var controls = [
        {
            id: "btnRefresh",
            view: "button",
            type: "icon",
            icon: "fas fa-refresh",
            label: _("REFRESH"),
            width: 90,
            // shortcut: "F4",
            //css:"button_raised",
            css: "webix_secondary",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    table.clearAll();
                    //console.log(table);
                    table.load(table.config.url);
                    //table.refresh();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: _("THEM") + " (F4)",
            width: 85,
            //css:"button_raised",
            css: "webix_secondary",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("tokenfile-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    // var chon = item.chon;

                    // item.chon = chon == true ? "true" : "false";

                    dmwindowform.setEditMode(1);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            label: _("SUA") + " (F3)",
            width: 75,
            //css:"button_raised",
            css: "webix_secondary",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("tokenfile-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();
                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng cần sửa !", "error");
                        return;
                    }
                    // var chon = item.chon;

                    // item.chon = chon == true ? "true" : "false";

                    form.setValues(item);

                    dmwindowform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            css: "webix_secondary",
            label: _("XOA") + " (F8)",
            width: 75,
            //css:"button_raised",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    var khData = {};
                    khData.windowid = app.path[1].params[0];
                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(item);

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.serialNumber,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/System/Save", khData, function (text) {
                                        if (text.hasOwnProperty("error")) {
                                            webix.message(text.error);
                                            return;
                                        }

                                        table.remove(item.id);
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                datafetch: 100,
                resizeColumn: true,
                //loadahead: 15,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var windowno = app.path[1].params[0];
                        var url = app.config.host + "/System/GetDataByWindowNo";

                        var start = null;
                        var count = null;
                        var filter = [];
                        var infoparam = null;
                        var tlbparam_s = [];

                        if (params == null) {
                            url = url + "&start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                            start = 0;
                            count = view.config.datafetch;
                        } else {
                            url = url + "&start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;
                            start = params.start;
                            count = params.count;
                            if (params.filter != null) {
                                var array = [];

                                for (var property in params.filter) {
                                    var column = view.getColumnConfig(property);

                                    if (column !== undefined) {
                                        var item = {
                                            columnName: property,
                                            columnType: column.columnType,
                                            value: params.filter[property],
                                        };
                                        array.push(item);
                                    }
                                }

                                if (array.length > 0) {
                                    filter = array;
                                    url = url + "&filter=" + JSON.stringify(array);
                                } else {
                                    url = url + "&filter=null";
                                }
                            } else {
                                url = url + "&filter=null";
                            }
                        }

                        if (view.config.infoparam != null) {
                            infoparam = view.config.infoparam;
                            url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
                        } else {
                            url = url + "&infoparam=null";
                        }

                        var tlbParams = $$("tlbParams");

                        if (tlbParams !== undefined) {
                            var childViews = tlbParams.getChildViews();

                            var parameters = [];

                            for (var i = 0; i < childViews.length; i++) {
                                var child = childViews[i];

                                var id = child.config.id;
                                var value_field = child.config.value_field;

                                var item = {
                                    columnName: value_field,
                                    value: $$(id).getValue(),
                                };

                                parameters.push(item);
                            }
                            tlbparam_s = parameters;
                            url = url + "&tlbparam=" + JSON.stringify(parameters);
                        } else {
                            url = url + "&tlbparam=null";
                        }

                        var item = {
                            window_id: windowno,
                            start: start,
                            count: count,
                            filter: filter,
                            infoparam: infoparam,
                            tlbparam: tlbparam_s,
                        };
                        var url1 = app.config.host + "/System/GetDataByWindowNo";
                        webix.ajax().headers({ "Content-type": "application/json" }).bind(view).post(url1, item, callback, view);
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.index = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    { id: "index", header: "", sort: "int", width: 40 },
                    { id: "file_path", header: _("PATH"), width: 350 },
                    { id: "serial_number", header: "Số serial", sort: "int", width: 250 },
                    { id: "serial_label", header: "Serial token", sort: "int", width: 250 },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
