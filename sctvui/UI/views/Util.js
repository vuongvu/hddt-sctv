﻿define(["app", "locale", "models/user"], function (app, _, user) {
    return {
        $ui: {
        },
        StringToFunc:function (s){
            var funcReg = /^(function)([ ]*)(\()(\w*)(\))([ ]*)(\{)([^]*)(})$/gm;
            var match = funcReg.exec(s);

            if(match) {
                return new Function(match[4].split(','), match[8]);
            }
            else return null;
        },
        StringToFunc2:function (s){
            return new Function("return " + "s")()
        }
    };
});
