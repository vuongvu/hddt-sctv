define(["app", "locale", "views/forms/license"], function (app, _, licenseForm) {
    "use strict";

    webix.$$("title").parse({ title: "Gửi yêu cầu hỗ trợ đến SCTV", details: "Gửi yêu cầu hỗ trợ đến SCTV" });

    var layout = {
        // paddingY: 60,
        // paddingX: 500,
        cols: [
            { gravity: 1, template: "" },
            {
                rows: [
                    { gravity: 1, template: "" },
                    // {
                    //     height: 35,
                    //     type: "toolbar",
                    //     cols: controls
                    // },
                    {
                        // view: "scrollview",
                        // scroll: "y",
                        // body: {
                        view: "form",
                        id: "form_hotro",
                        paddingY: 10,
                        // width: 500,
                        // height: 500,
                        position: "center",
                        paddingX: 10,
                        complexData: true,
                        margin: 0,
                        elements: [
                            {
                                view: "text",
                                name: "ms_thue",
                                id: "ms_thue",
                                label: _("MS_THUE"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top",
                            },
                            {
                                view: "text",
                                name: "ten_dvcs",
                                id: "ten_dvcs",
                                label: _("DONVI"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top",
                            },
                            {
                                view: "text",
                                name: "dia_chi",
                                label: _("DIA_CHI"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top",
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "nguoi_dai_dien",
                                        label: _("NGUOI_DAI_DIEN_PL"),
                                        css: "mcls_readonly_text",
                                        readonly: true,
                                        labelPosition: "top",
                                    },
                                    {
                                        view: "text",
                                        name: "email",
                                        label: "Email",
                                        css: "mcls_readonly_text",
                                        readonly: true,
                                        labelPosition: "top",
                                    },
                                ],
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "nguoi_lien_he",
                                        id: "nguoi_lien_he",
                                        label: _("Người liên hệ"),
                                        labelPosition: "top",
                                        required: true,
                                        invalidMessage: "Bạn phải nhập người liên hệ",
                                    },

                                    {
                                        view: "text",
                                        name: "sdt",
                                        id: "sdt",
                                        label: "Số điện thoại",
                                        labelPosition: "top",
                                        required: true,
                                        //webix.ru
                                        invalidMessage: "Bạn phải nhập số điện thoại",
                                    },
                                ],
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "teamview",
                                        id: "teamview",
                                        label: _("Teamview (UltraViewer)"),
                                        labelPosition: "top",
                                    },

                                    {
                                        view: "text",
                                        name: "pass",
                                        id: "pass",
                                        label: "Password",
                                        labelPosition: "top",
                                    },
                                ],
                            },
                            {
                                id: "noi_dung",
                                view: "textarea",
                                name: "noi_dung",
                                label: "Yêu cầu hỗ trợ",
                                height: 160,
                                labelPosition: "top",
                            },

                            {
                                margin: 10,
                                cols: [
                                    {},
                                    {
                                        shortcut: "F10",
                                        view: "button",
                                        label: "Gửi (F10)",
                                        align: "center",
                                        width: 120,
                                        on: {
                                            onClick: function () {
                                                var form = webix.$$("form_hotro");

                                                var item = form.getValues();
                                                var format = webix.Date.dateToStr("%d/%m/%Y - %H:%i");
                                                var string = format(new Date());

                                                var descrip =
                                                    "Ngày :" +
                                                    string +
                                                    "</br>" +
                                                    "Công ty :" +
                                                    item.ten_dvcs +
                                                    "</br>" +
                                                    "Mã số thuế :" +
                                                    item.ms_thue +
                                                    "</br>" +
                                                    "Người liên hệ: " +
                                                    item.nguoi_lien_he +
                                                    "</br>" +
                                                    "Số điện thoại: " +
                                                    item.sdt +
                                                    "</br>" +
                                                    "Teamview (UltraViewer): " +
                                                    item.teamview +
                                                    "</br>" +
                                                    "Pass: " +
                                                    item.pass +
                                                    "</br>" +
                                                    "Nội dung cần hỗ trợ: " +
                                                    item.noi_dung +
                                                    "</br>";

                                                if ($$("form_hotro").validate()) {
                                                    var box = webix.modalbox({
                                                        title: "Đang gửi yêu cầu hỗ trợ đến SCTV",
                                                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                                        width: 350,
                                                        height: 200,
                                                    });

                                                    var data = {
                                                        mst: item.ms_thue,
                                                        noidung: item.noi_dung,
                                                        nguoi_lien_he: item.nguoi_lien_he,
                                                        sdt_nguoi_lien_he: item.sdt,
                                                        ultra_teamview: item.teamview,
                                                        pass: item.pass,
                                                    };
                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(app.config.host + "/System/SendSupport", data, {
                                                            error: function (text, data, XmlHttpRequest) {
                                                                webix.modalbox.hide(box);
                                                                alert(text);
                                                            },
                                                            success: function (text, data, XmlHttpRequest) {
                                                                var json = JSON.parse(text);
                                                                if (!json.hasOwnProperty("error")) {
                                                                    $$("nguoi_lien_he").setValue(null);
                                                                    $$("sdt").setValue(null);
                                                                    $$("teamview").setValue(null);
                                                                    $$("pass").setValue(null);
                                                                    $$("noi_dung").setValue(null);
                                                                    webix.message("Gửi yêu cầu hỗ trợ thành công !", "debug");
                                                                } else {
                                                                    webix.message(json.error, "error");
                                                                }
                                                                webix.modalbox.hide(box);
                                                            },
                                                        });
                                                }
                                            },
                                        },
                                        click: function () {
                                            this.callEvent("onClick");
                                        },
                                    },
                                    {
                                        view: "button",
                                        hotkey: "esc",
                                        label: "Nhập lại (ESC)",
                                        align: "center",
                                        width: 120,
                                        type: "danger",
                                        click: function () {
                                            $$("nguoi_lien_he").setValue(null);
                                            $$("sdt").setValue(null);
                                            $$("teamview").setValue(null);
                                            $$("pass").setValue(null);
                                            $$("noi_dung").setValue(null);
                                            // webix.$$("chukyso-win").close();
                                        },
                                    },
                                    {},
                                ],
                            },
                        ],
                    },
                    { gravity: 1, template: "" },
                ],
            },
            { gravity: 1, template: "" },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var url = app.config.host + "/System/GetInfoCompany";

            webix.ajax(url, function (text) {
                var json = JSON.parse(text);
                $$("form_hotro").setValues(json);
            });
        },
    };
});
