// var form = $$("window-form");
// var values = form.getValues();
// var currency = values["inv_currencyCode"];
// var table = $$(tableId);
// var selectedItem = table.getItem(rowId)
// //var selectedItem = table.getSelectedItem();
// var loai_dc = selectedItem.ma1 == null ? 0 : selectedItem.ma1;

// // Lấy phần thập phân ở danh mục ngoại tệ
// var table_decimalPlaces = {
//     inv_quantity: 0,
//     inv_TotalAmountWithoutVat: 0,
//     inv_vatAmount: 0,
//     inv_TotalAmount: 0
// }

// table.eachColumn(function (columnId) {
//     var a = columnId.toString();
//     if (a == "inv_quantity" || a == "inv_TotalAmountWithoutVat" || a == "inv_vatAmount" || a == "inv_TotalAmount") {
//         var b = table.getColumnConfig(a).numericConfig.decimalPlaces;
//         if (b != null) {
//             table_decimalPlaces[a] = b;
//         }

//     }
// });
// //End call dmnt

// if (loai_dc == 0) {

//     var inv_quantity = webix.rules.isNumber(selectedItem.inv_quantity) ? selectedItem.inv_quantity : 0;
//     var inv_unitPrice = webix.rules.isNumber(selectedItem.inv_unitPrice) ? selectedItem.inv_unitPrice : 0;
//     var inv_TotalAmountWithoutVat = webix.rules.isNumber(selectedItem.inv_TotalAmountWithoutVat) ? selectedItem.inv_TotalAmountWithoutVat : 0;
//     var inv_discountPercentage = webix.rules.isNumber(selectedItem.inv_discountPercentage) ? selectedItem.inv_discountPercentage : 0;
//     var inv_discountAmount = webix.rules.isNumber(selectedItem.inv_discountAmount) ? selectedItem.inv_discountAmount : 0;
//     var ma_thue = webix.rules.isNumber(selectedItem.ma_thue) ? selectedItem.ma_thue : 0;
//     if (ma_thue == -1) { ma_thue = 0; }
//     var inv_vatAmount = webix.rules.isNumber(selectedItem.inv_vatAmount) ? selectedItem.inv_vatAmount : 0;
//     var inv_TotalAmount = webix.rules.isNumber(selectedItem.inv_TotalAmount) ? selectedItem.inv_TotalAmount : 0;
//     var inv_exchangeRate = webix.rules.isNumber($$("inv_exchangeRate_inv_InvoiceAuth").getValue()) ? $$("inv_exchangeRate_inv_InvoiceAuth").getValue() : 0;

//     if (inv_quantity < 0) { inv_quantity = 0; }
//     if (inv_unitPrice < 0) { inv_unitPrice = 0; }
//     if (inv_TotalAmountWithoutVat < 0) { inv_TotalAmountWithoutVat = 0; }

//     if (columnId == "inv_quantity" || columnId == "inv_unitPrice" || columnId == "inv_TotalAmountWithoutVat" || columnId == "inv_TotalAmount" || columnId == "inv_discountPercentage" || columnId == "inv_discountAmount") {

//         if (inv_unitPrice > 0) {
//             inv_TotalAmountWithoutVat = (inv_quantity * inv_unitPrice).toFixed(table_decimalPlaces.inv_TotalAmountWithoutVat);//Update dòng 1
//         } else {
//             if (inv_TotalAmountWithoutVat > 0) {
//                 if (inv_quantity > 0) {
//                     inv_unitPrice = inv_TotalAmountWithoutVat / inv_quantity;
//                 }
//             } else {
//                 if (inv_TotalAmount > 0) {
//                     if (inv_TotalAmountWithoutVat <= 0) {
//                         inv_TotalAmountWithoutVat = (inv_TotalAmount / (1 + ma_thue / 100)).toFixed(table_decimalPlaces.inv_TotalAmountWithoutVat);// Update dòng 2
//                     }

//                     if (inv_quantity > 0) {
//                         inv_unitPrice = inv_TotalAmountWithoutVat / inv_quantity;
//                     }
//                 }
//             }
//         }

//         if (inv_discountPercentage < 0) { inv_discountPercentage = 0; }
//         if (inv_discountPercentage > 100) { inv_discountPercentage = 0; }

//         inv_discountAmount = inv_TotalAmountWithoutVat * inv_discountPercentage / 100;
//         if ((inv_discountAmount - inv_TotalAmountWithoutVat) > 0) {
//             inv_discountAmount = 0;
//         }
//         inv_vatAmount = ((inv_TotalAmountWithoutVat - inv_discountAmount) * ma_thue / 100).toFixed(table_decimalPlaces.inv_vatAmount);  // Update dòng 3
//         //if(inv_TotalAmount <= 0){
//         inv_TotalAmount = currency == "VND" ? Math.round(inv_TotalAmountWithoutVat - inv_discountAmount + parseFloat(inv_vatAmount)) : (inv_TotalAmountWithoutVat - inv_discountAmount + parseFloat(inv_vatAmount));
//         //}

//         selectedItem.inv_quantity = inv_quantity;
//         selectedItem.inv_unitPrice = inv_unitPrice;
//         selectedItem.inv_TotalAmountWithoutVat = inv_TotalAmountWithoutVat;
//         selectedItem.inv_discountPercentage = inv_discountPercentage;
//         selectedItem.inv_discountAmount = inv_discountAmount;
//         selectedItem.inv_vatAmount = inv_vatAmount;
//         selectedItem.inv_TotalAmount = inv_TotalAmount;
//         table.updateItem(selectedItem.id, selectedItem);
//     }


//     if (columnId == "inv_quantity" || columnId == "inv_unitPrice" || columnId == "inv_TotalAmountWithoutVat"
//         || columnId == "inv_discountPercentage" || columnId == "inv_discountAmount"
//         || columnId == "ma_thue" || columnId == "inv_vatAmount" || columnId == "inv_promotion"
//         || columnId == "inv_TotalAmount" || columnId == "check_giam_tru") {

//         var inv_TotalAmountWithoutVat = 0;
//         var inv_discountAmount = 0;
//         var inv_vatAmount = 0;
//         var inv_TotalAmount = 0;
//         var count = table.count();

//         for (var i = 0; i < count; i++) {
//             var rowIndex = table.getIdByIndex(i);
//             var record = table.getItem(rowIndex);

//             var check_giam_tru = record.hasOwnProperty("check_giam_tru") ? record.check_giam_tru : false;

//             if (record.hasOwnProperty("inv_TotalAmountWithoutVat")) {
//                 var value = record.inv_TotalAmountWithoutVat == null ? 0 : Number(record.inv_TotalAmountWithoutVat);
//                 value = check_giam_tru === true ? -1 * value : value;
//                 inv_TotalAmountWithoutVat = currency == "VND" ? Math.round(inv_TotalAmountWithoutVat + value) : inv_TotalAmountWithoutVat + value;
//             }

//             if (record.hasOwnProperty("inv_discountAmount")) {
//                 var value = record.inv_discountAmount == null ? 0 : Number(record.inv_discountAmount);
//                 inv_discountAmount = currency == "VND" ? Math.round(inv_discountAmount + value) : inv_discountAmount + value;
//             }

//             if (record.hasOwnProperty("inv_vatAmount")) {
//                 var value = record.inv_vatAmount == null ? 0 : Number(record.inv_vatAmount);
//                 value = check_giam_tru === true ? -1 * value : value;
//                 inv_vatAmount = currency == "VND" ? Math.round(inv_vatAmount + value) : inv_vatAmount + value;
//             }
//             if (record.hasOwnProperty("inv_TotalAmount")) {
//                 var value = record.inv_TotalAmount == null ? 0 : Number(record.inv_TotalAmount);
//                 value = check_giam_tru === true ? -1 * value : value;
//                 inv_TotalAmount = currency == "VND" ? Math.round(inv_TotalAmount + value) : inv_TotalAmount + value;
//             }
//         }


//         $$("inv_TotalAmountWithoutVat_inv_InvoiceAuth").setValue(inv_TotalAmountWithoutVat);
//         $$("inv_discountAmount_inv_InvoiceAuth").setValue(inv_discountAmount);
//         $$("inv_vatAmount_inv_InvoiceAuth").setValue(inv_vatAmount);
//         //$$("inv_TotalAmount_inv_InvoiceAuth").setValue(inv_TotalAmountWithoutVat - inv_discountAmount + inv_vatAmount);
//         $$("inv_TotalAmount_inv_InvoiceAuth").setValue(inv_TotalAmount);


//     }
// }
// else {
//     if (columnId == "inv_quantity" || columnId == "inv_unitPrice" || columnId == "inv_TotalAmountWithoutVat" || columnId == "inv_TotalAmount" || columnId == "inv_vatAmount" || columnId == "ma1" || columnId == "tieu_thuc") {

//         var inv_quantity = webix.rules.isNumber(selectedItem.inv_quantity) ? selectedItem.inv_quantity : 0;
//         var inv_unitPrice = webix.rules.isNumber(selectedItem.inv_unitPrice) ? selectedItem.inv_unitPrice : 0;
//         var inv_TotalAmountWithoutVat = webix.rules.isNumber(selectedItem.inv_TotalAmountWithoutVat) ? selectedItem.inv_TotalAmountWithoutVat : 0;
//         var inv_discountPercentage = webix.rules.isNumber(selectedItem.inv_discountPercentage) ? selectedItem.inv_discountPercentage : 0;
//         var inv_discountAmount = webix.rules.isNumber(selectedItem.inv_discountAmount) ? selectedItem.inv_discountAmount : 0;
//         var ma_thue = webix.rules.isNumber(selectedItem.ma_thue) ? selectedItem.ma_thue : 0;
//         if (ma_thue == -1) { ma_thue = 0; }
//         var inv_vatAmount = webix.rules.isNumber(selectedItem.inv_vatAmount) ? selectedItem.inv_vatAmount : 0;
//         var inv_TotalAmount = webix.rules.isNumber(selectedItem.inv_TotalAmount) ? selectedItem.inv_TotalAmount : 0;
//         var inv_itemName = selectedItem.inv_itemName;
//         var tieu_thuc = selectedItem.tieu_thuc == null ? "" : selectedItem.tieu_thuc;
//         if (inv_unitPrice > 0) {
//             inv_TotalAmountWithoutVat = inv_quantity * inv_unitPrice;
//         } else {
//             if (inv_TotalAmountWithoutVat > 0) {
//                 if (inv_quantity > 0) {
//                     inv_unitPrice = inv_TotalAmountWithoutVat / inv_quantity;
//                 }
//             } else {
//                 if (inv_TotalAmount > 0) {
//                     inv_TotalAmountWithoutVat = inv_TotalAmount / (1 + ma_thue / 100);
//                     if (inv_quantity > 0) {
//                         inv_unitPrice = inv_TotalAmountWithoutVat / inv_quantity;
//                     }
//                 }
//             }
//         }

//         inv_discountAmount = inv_TotalAmountWithoutVat * inv_discountPercentage / 100;
//         inv_vatAmount = (inv_TotalAmountWithoutVat - inv_discountAmount) * ma_thue / 100;
//         inv_TotalAmount = inv_TotalAmountWithoutVat - inv_discountAmount + inv_vatAmount;
//         if (loai_dc == -1) {
//             //selectedItem.inv_itemName =  "Điều chỉnh giảm " + tieu_thuc + " " + selectedItem.inv_itemName;
//             selectedItem.inv_TotalAmountWithoutVat = inv_TotalAmountWithoutVat * loai_dc;
//             selectedItem.inv_discountAmount = inv_discountAmount * loai_dc;
//             selectedItem.inv_vatAmount = inv_vatAmount * loai_dc;
//             selectedItem.inv_TotalAmount = inv_TotalAmount * loai_dc;
//             selectedItem.inv_unitPrice = inv_unitPrice;
//             selectedItem.inv_quantity = inv_quantity;
//         }
//         if (loai_dc == 1) {
//             //selectedItem.inv_itemName = "Điều chỉnh tăng " + tieu_thuc + " " + selectedItem.inv_itemName;
//             selectedItem.inv_TotalAmountWithoutVat = inv_TotalAmountWithoutVat < 0 ? inv_TotalAmountWithoutVat * -1 : inv_TotalAmountWithoutVat;
//             selectedItem.inv_discountAmount = inv_discountAmount < 0 ? inv_discountAmount * -1 : inv_discountAmount;
//             selectedItem.inv_vatAmount = inv_vatAmount < 0 ? inv_vatAmount * -1 : inv_vatAmount;
//             selectedItem.inv_TotalAmount = inv_TotalAmount < 0 ? inv_TotalAmount * -1 : inv_TotalAmount;
//             selectedItem.inv_unitPrice = inv_unitPrice < 0 ? inv_unitPrice * -1 : inv_unitPrice;
//             selectedItem.inv_quantity = inv_quantity < 0 ? inv_quantity * -1 : inv_quantity;
//         }
//         table.updateItem(selectedItem.id, selectedItem);
//     }


//     if (columnId == "inv_quantity" || columnId == "inv_unitPrice" || columnId == "inv_TotalAmountWithoutVat"
//         || columnId == "inv_discountPercentage" || columnId == "inv_discountAmount"
//         || columnId == "ma_thue" || columnId == "inv_vatAmount" || columnId == "inv_promotion" || columnId == "ma1" || columnId == "inv_TotalAmount") {

//         var inv_TotalAmountWithoutVat = 0;
//         var inv_discountAmount = 0;
//         var inv_vatAmount = 0;

//         var count = table.count();
//         for (var i = 0; i < count; i++) {
//             var rowIndex = table.getIdByIndex(i);
//             var record = table.getItem(rowIndex);

//             if (record.hasOwnProperty("inv_TotalAmountWithoutVat")) {
//                 var value = record.inv_TotalAmountWithoutVat == null ? 0 : Number(record.inv_TotalAmountWithoutVat);
//                 inv_TotalAmountWithoutVat = inv_TotalAmountWithoutVat + value;
//             }

//             if (record.hasOwnProperty("inv_discountAmount")) {
//                 var value = record.inv_discountAmount == null ? 0 : Number(record.inv_discountAmount);
//                 inv_discountAmount = inv_discountAmount + value;
//             }

//             if (record.hasOwnProperty("inv_vatAmount")) {
//                 var value = record.inv_vatAmount == null ? 0 : Number(record.inv_vatAmount);
//                 inv_vatAmount = inv_vatAmount + value;
//             }
//         }

//         if (loai_dc == -1) {
//             $$("inv_TotalAmountWithoutVat_inv_InvoiceAuth").setValue(inv_TotalAmountWithoutVat * loai_dc);
//             $$("inv_discountAmount_inv_InvoiceAuth").setValue(inv_discountAmount * loai_dc);
//             $$("inv_vatAmount_inv_InvoiceAuth").setValue(inv_vatAmount * loai_dc);
//             $$("inv_TotalAmount_inv_InvoiceAuth").setValue((inv_TotalAmountWithoutVat - inv_discountAmount + inv_vatAmount) * loai_dc);
//         }
//         if (loai_dc == 1) {
//             $$("inv_TotalAmountWithoutVat_inv_InvoiceAuth").setValue(inv_TotalAmountWithoutVat < 0 ? inv_TotalAmountWithoutVat * -1 : inv_TotalAmountWithoutVat);
//             $$("inv_discountAmount_inv_InvoiceAuth").setValue(inv_discountAmount < 0 ? inv_discountAmount * -1 : inv_discountAmount);
//             $$("inv_vatAmount_inv_InvoiceAuth").setValue(inv_vatAmount < 0 ? inv_vatAmount * -1 : inv_vatAmount);
//             var tongtien = inv_TotalAmountWithoutVat - inv_discountAmount + inv_vatAmount;
//             $$("inv_TotalAmount_inv_InvoiceAuth").setValue(tongtien < 0 ? tongtien * -1 : tongtien);
//         }
//     }

// }

// var data = table.serialize();
// var tien_tt_all = 0;
// var tien_thue_all = 0
// var tien_ck_all = 0;
// var tong_tien_all = 0

// var tien_tt_10 = 0;
// var tien_thue_10 = 0;
// var tien_ck_10 = 0;
// var tong_tien_10 = 0;

// var tien_tt_5 = 0;
// var tien_thue_5 = 0;
// var tien_ck_5 = 0;
// var tong_tien_5 = 0;

// var tien_tt_0 = 0;
// var tien_thue_0 = 0;
// var tien_ck_0 = 0;
// var tong_tien_0 = 0;

// var tien_tt_kct = 0;
// var tien_thue_kct = 0;
// var tien_ck_kct = 0;
// var tong_tien_kct = 0;

// var tien_tt_kkk = 0;
// var tien_thue_kkk = 0;
// var tien_ck_kkk = 0;
// var tong_tien_kkk = 0;

// data.forEach(function (entry) {
//     tien_tt_all += entry["inv_TotalAmountWithoutVat"] == null ? 0 : entry["inv_TotalAmountWithoutVat"];
//     tien_thue_all += entry["inv_vatAmount"] == null ? 0 : entry["inv_vatAmount"];
//     tien_ck_all += entry["inv_discountAmount"] == null ? 0 : entry["inv_discountAmount"];
//     tong_tien_all += entry["inv_TotalAmount"] == null ? 0 : entry["inv_TotalAmount"];

//     if (entry["ma_thue"] == 10) {
//         tien_tt_10 += entry["inv_TotalAmountWithoutVat"] == null ? 0 : entry["inv_TotalAmountWithoutVat"];
//         tien_thue_10 += entry["inv_vatAmount"] == null ? 0 : entry["inv_vatAmount"];
//         tien_ck_10 += entry["inv_discountAmount"] == null ? 0 : entry["inv_discountAmount"];
//         tong_tien_10 += entry["inv_TotalAmount"] == null ? 0 : entry["inv_TotalAmount"];
//     }
//     if (entry["ma_thue"] == 5) {
//         tien_tt_5 += entry["inv_TotalAmountWithoutVat"] == null ? 0 : entry["inv_TotalAmountWithoutVat"];
//         tien_thue_5 += entry["inv_vatAmount"] == null ? 0 : entry["inv_vatAmount"];
//         tien_ck_5 += entry["inv_discountAmount"] == null ? 0 : entry["inv_discountAmount"];
//         tong_tien_5 += entry["inv_TotalAmount"] == null ? 0 : entry["inv_TotalAmount"];
//     }
//     if (entry["ma_thue"] == 0) {
//         tien_tt_0 += entry["inv_TotalAmountWithoutVat"] == null ? 0 : entry["inv_TotalAmountWithoutVat"];
//         tien_thue_0 += entry["inv_vatAmount"] == null ? 0 : entry["inv_vatAmount"];
//         tien_ck_0 += entry["inv_discountAmount"] == null ? 0 : entry["inv_discountAmount"];
//         tong_tien_0 += entry["inv_TotalAmount"] == null ? 0 : entry["inv_TotalAmount"];
//     }
//     if (entry["ma_thue"] == -1) {
//         tien_tt_kct += entry["inv_TotalAmountWithoutVat"] == null ? 0 : entry["inv_TotalAmountWithoutVat"];
//         tien_thue_kct += entry["inv_vatAmount"] == null ? 0 : entry["inv_vatAmount"];
//         tien_ck_kct += entry["inv_discountAmount"] == null ? 0 : entry["inv_discountAmount"];
//         tong_tien_kct += entry["inv_TotalAmount"] == null ? 0 : entry["inv_TotalAmount"];
//     }
//     if (entry["ma_thue"] == -2) {
//         tien_tt_kkk += entry["inv_TotalAmountWithoutVat"] == null ? 0 : entry["inv_TotalAmountWithoutVat"];
//         tien_thue_kkk += entry["inv_vatAmount"] == null ? 0 : entry["inv_vatAmount"];
//         tien_ck_kkk += entry["inv_discountAmount"] == null ? 0 : entry["inv_discountAmount"];
//         tong_tien_kkk += entry["inv_TotalAmount"] == null ? 0 : entry["inv_TotalAmount"];
//     }

// });

// // $$("tgtcthue").setValue(tien_tt_all);
// $$("tgtcthue10_inv_InvoiceAuth").setValue(tien_tt_10);
// $$("tgtcthue5_inv_InvoiceAuth").setValue(tien_tt_5);
// $$("tgtcthue0_inv_InvoiceAuth").setValue(tien_tt_0);
// $$("tgtcthuekct_inv_InvoiceAuth").setValue(tien_tt_kct);
// $$("tgtcthuekkk_inv_InvoiceAuth").setValue(tien_tt_kkk);

// // $$("ttcktmai").setValue(tien_ck_all);
// $$("ttcktmai10_inv_InvoiceAuth").setValue(tien_ck_10);
// $$("ttcktmai5_inv_InvoiceAuth").setValue(tien_ck_5);
// $$("ttcktmai0_inv_InvoiceAuth").setValue(tien_ck_0);
// $$("ttcktmaikct_inv_InvoiceAuth").setValue(tien_ck_kct);
// $$("ttcktmaikkk_inv_InvoiceAuth").setValue(tien_ck_kkk);

// // $$("tgtthue").setValue(tien_thue_all);
// $$("tgtthue10_inv_InvoiceAuth").setValue(tien_thue_10);
// $$("tgtthue5_inv_InvoiceAuth").setValue(tien_thue_5);
// $$("tgtthue0_inv_InvoiceAuth").setValue(tien_thue_0);
// $$("tgtthuekct_inv_InvoiceAuth").setValue(tien_thue_kct);
// $$("tgtthuekkk_inv_InvoiceAuth").setValue(tien_thue_kkk);

// // $$("tgtttbso").setValue(tong_tien_all);
// $$("tgtttbso10_inv_InvoiceAuth").setValue(tong_tien_10);
// $$("tgtttbso5_inv_InvoiceAuth").setValue(tong_tien_5);
// $$("tgtttbso0_inv_InvoiceAuth").setValue(tong_tien_0);
// $$("tgtttbsokct_inv_InvoiceAuth").setValue(tong_tien_kct);
// $$("tgtttbsokkk_inv_InvoiceAuth").setValue(tong_tien_kkk);