define(["app", "locale"], function (app, _) {
    //var json = version.getInfocompany();
    //$$("hotro-form").setValues(json);

    var layout = {
        view: "window",
        modal: true,
        id: "hotro-win",
        position: "center",
        head: "Gửi yêu cầu hỗ trợ đến SCTV",
        width: 500,
        // height: 700,
        body: {
            paddingY: 10,
            paddingX: 10,
            //elementsConfig:{labelWidth: 120},
            view: "form",
            id: "hotro-form",
            complexData: true,
            elements: [
                {
                    view: "text",
                    name: "ms_thue",
                    id: "ms_thue",
                    label: _("MS_THUE"),
                    css: "mcls_readonly_text",
                    readonly: true,
                    labelPosition: "top",
                },
                {
                    view: "text",
                    name: "ten_dvcs",
                    id: "ten_dvcs",
                    label: _("DONVI"),
                    css: "mcls_readonly_text",
                    readonly: true,
                    labelPosition: "top",
                },
                {
                    view: "text",
                    name: "dia_chi",
                    label: _("DIA_CHI"),
                    css: "mcls_readonly_text",
                    readonly: true,
                    labelPosition: "top",
                },
                {
                    cols: [
                        {
                            view: "text",
                            name: "nguoi_dai_dien",
                            label: _("NGUOI_DAI_DIEN_PL"),
                            css: "mcls_readonly_text",
                            readonly: true,
                            labelPosition: "top",
                        },
                        {
                            view: "text",
                            name: "email",
                            label: "Email",
                            css: "mcls_readonly_text",
                            readonly: true,
                            labelPosition: "top",
                        },
                    ],
                },
                {
                    cols: [
                        {
                            view: "text",
                            name: "nguoi_lien_he",
                            id: "nguoi_lien_he",
                            label: _("Người liên hệ"),
                            labelPosition: "top",
                            required: true,
                            invalidMessage: "Bạn phải nhập người liên hệ",
                        },

                        {
                            view: "text",
                            name: "sdt",
                            id: "sdt",
                            label: "Số điện thoại",
                            labelPosition: "top",
                            required: true,
                            //webix.ru
                            invalidMessage: "Bạn phải nhập số điện thoại",
                        },
                    ],
                },
                {
                    cols: [
                        {
                            view: "text",
                            name: "teamview",
                            id: "teamview",
                            label: _("Teamview (UltraViewer)"),
                            labelPosition: "top",
                        },

                        {
                            view: "text",
                            name: "pass",
                            id: "pass",
                            label: "Password",
                            labelPosition: "top",
                        },
                    ],
                },
                {
                    id: "noi_dung",
                    view: "textarea",
                    name: "noi_dung",
                    label: "Yêu cầu hỗ trợ",
                    height: 120,
                    labelPosition: "top",
                },

                {
                    margin: 10,
                    cols: [
                        {},
                        {
                            shortcut: "F10",
                            view: "button",
                            label: "Gửi (F10)",
                            align: "center",
                            width: 120,
                            type: "form",
                            on: {
                                onClick: function () {
                                    var form = webix.$$("hotro-form");

                                    var item = form.getValues();
                                    var format = webix.Date.dateToStr("%d/%m/%Y - %H:%i");
                                    var string = format(new Date());

                                    var descrip =
                                        "Ngày :" +
                                        string +
                                        "</br>" +
                                        "Công ty :" +
                                        item.ten_dvcs +
                                        "</br>" +
                                        "Mã số thuế :" +
                                        item.ms_thue +
                                        "</br>" +
                                        "Người liên hệ: " +
                                        item.nguoi_lien_he +
                                        "</br>" +
                                        "Số điện thoại: " +
                                        item.sdt +
                                        "</br>" +
                                        "Teamview (UltraViewer): " +
                                        item.teamview +
                                        "</br>" +
                                        "Pass: " +
                                        item.pass +
                                        "</br>" +
                                        "Nội dung cần hỗ trợ: " +
                                        item.noi_dung +
                                        "</br>";

                                    if ($$("hotro-form").validate()) {
                                        var box = webix.modalbox({
                                            title: "Đang gửi yêu cầu hỗ trợ đến SCTV",
                                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                            width: 350,
                                            height: 200,
                                        });

                                        var data = {
                                            mst: item.ms_thue,
                                            noidung: item.noi_dung,
                                            nguoi_lien_he: item.nguoi_lien_he,
                                            sdt_nguoi_lien_he: item.sdt,
                                            ultra_teamview: item.teamview,
                                            pass: item.pass,
                                        };
                                        webix
                                            .ajax()
                                            .headers({ "Content-type": "application/json" })
                                            .post(app.config.host + "/System/SendSupport", data, {
                                                error: function (text, data, XmlHttpRequest) {
                                                    webix.modalbox.hide(box);
                                                    alert(text);
                                                },
                                                success: function (text, data, XmlHttpRequest) {
                                                    var json = JSON.parse(text);
                                                    if (!json.hasOwnProperty("error")) {
                                                        $$("nguoi_lien_he").setValue(null);
                                                        $$("sdt").setValue(null);
                                                        $$("teamview").setValue(null);
                                                        $$("pass").setValue(null);
                                                        $$("noi_dung").setValue(null);
                                                        webix.message("Gửi yêu cầu hỗ trợ thành công !", "debug");
                                                    } else {
                                                        webix.message(json.error, "error");
                                                    }
                                                    webix.modalbox.hide(box);
                                                },
                                            });
                                    }
                                },
                            },
                            click: function () {
                                this.callEvent("onClick");
                            },
                        },
                        {
                            shortcut: "F10",
                            view: "button",
                            label: "Nhập lại (F9)",
                            align: "center",
                            width: 120,
                            type: "form",
                            on: {
                                onClick: function () {
                                    $$("nguoi_lien_he").setValue(null);
                                    $$("sdt").setValue(null);
                                    $$("teamview").setValue(null);
                                    $$("pass").setValue(null);
                                    $$("noi_dung").setValue(null);
                                },
                            },

                            click: function () {
                                this.callEvent("onClick");
                            },
                        },
                        {
                            view: "button",
                            hotkey: "esc",
                            label: "Hủy bỏ (ESC)",
                            align: "center",
                            width: 120,
                            type: "danger",
                            click: function () {
                                webix.$$("hotro-win").close();
                            },
                        },
                        {},
                    ],
                },
            ],
        },
    };
    return {
        $ui: layout,
        initUI: function (json) {
            $$("hotro-form").setValues(json);
        },
    };
});
