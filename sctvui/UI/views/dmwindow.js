define(["app", "views/forms/dmwindow", "locale"], function (app, dmwindowform, _) {
    "use strict";

    var op = [];
    //webix.$$("title").parse({ title: "Danh mục window", details: "Danh mục window system" });
    var controls = [
        {
            height: 38,
            id: "title",
            css: "title",
            width: 150,
            borderless: true,
            //css: { "box-shadow": "0 -2px #3498db inset !important" , "background":"#ffffff !important"},
            template: "<div class='header'>#title#</div>",
            data: { text: "", title: "Danh mục window", details: "Danh mục window" },
        },
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: _("THEM") + " (F4)",
            width: 95,
            css: "webix_secondary",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var win = this.$scope.ui(dmwindowform.$ui);
                    dmwindowform.setEditMode(1);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            label: _("SUA") + " (F3)",
            width: 85,
            css: "webix_secondary",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("dmwindow-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();
                    //console.log(item);
                    form.setValues(item);

                    dmwindowform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            label: _("XOA") + " (F8)",
            width: 85,
            css: "webix_secondary",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.name,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/WbWindow/Delete", item, function (text, xml, xhr) {
                                        if (text.hasOwnProperty("error")) {
                                            webix.message(text.error);
                                            return;
                                        }

                                        table.remove(item.id);
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnCopy",
            view: "button",
            type: "icon",
            icon: "fas fa-files-o",
            label: _("COPY"),
            width: 85,
            css: "webix_secondary",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dữ liệu");
                        return;
                    }

                    webix.confirm({
                        text: "Bạn có muốn sao chép " + item.WINDOW_NAME,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                var query = "EXECUTE sproc_copy_vcwindow'" + item.id + "',null";

                                var url = app.config.host + "/api/System/ExecuteQuery?sql=" + query;

                                webix.ajax(url, function (text) {
                                    var json = JSON.parse(text);

                                    if (json.length > 0) {
                                        webix.message(json[0]["ERROR"]);
                                    } else {
                                        table.clearAll();
                                        table.load(table.config.url);
                                    }
                                });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                datafetch: 100,
                resizeColumn: true,
                //loadahead: 15,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var url = app.config.host + "/WbWindow/GetWindows";

                        if (params == null) {
                            url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                        } else {
                            url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                            if (params.filter != null) {
                                var array = [];

                                for (var property in params.filter) {
                                    var column = view.getColumnConfig(property);

                                    var item = {
                                        columnName: property,
                                        columnType: column.columnType,
                                        value: params.filter[property],
                                    };

                                    array.push(item);
                                }

                                //console.log(JSON.stringify(array));
                                url = url + "&filter=" + JSON.stringify(array);
                            }
                        }

                        webix.ajax(url, callback, view);
                    },
                },
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    { id: "code", header: ["No", { content: "textFilter" }], width: 100 },
                    { id: "name", header: ["Title", { content: "textFilter" }], width: 400 },
                    { id: "window_type", header: "Window Type", fillspace: true },
                    { id: "width", header: "Width", fillspace: true },
                    { id: "height", header: "Height", fillspace: true },
                    { id: "max_row", columnType: "VC_TIEN", header: ["Rows max", { content: "textFilter" }], fillspace: true },
                    { id: "tab_row", header: "Row tab", fillspace: true },
                    { id: "voucher_code", header: "MA_CT", fillspace: true },
                    { id: "sql_code", header: "SQL_CODE", fillspace: true },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                view: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    function loadData() {
        var schoolyearcode = webix.$$("dmwindow").getValue();

        var url = app.config.host + "/api/System/GetWindowById?ID=" + schoolyearcode;

        webix.ajax().get(url, function (text) {
            var data = JSON.parse(text);

            var table = webix.$$("dmwindowData");
            table.clearAll();
            table.parse(data);
        });
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
