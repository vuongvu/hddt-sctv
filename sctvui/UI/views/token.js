define(["app", "views/forms/token", "locale"], function (app, dmwindowform, _) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Đăng ký Token", details: "Danh sách Token" });

    var controls = [
        // {
        //     id: "btnRefrescacash",
        //     view: "button",
        //     type: "icon",
        //     icon: "refresh",
        //     label: _("Tải báo cáo"),
        //     width: 90,
        //     // shortcut: "F4",
        //     //css:"button_raised",
        //     on: {
        //         onClick: function () {
        //             var box = webix.modalbox({
        //                 title: "Đang tải xuống",
        //                 text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
        //                 width: 350,
        //                 height: 200
        //             });

        //             webix.ajax()
        //                 .response("arraybuffer")
        //                 //.headers({ 'Content-type': 'application/json' })
        //                 .get(app.config.host + "/Invoice/Floder?filename=BC_CHITIET_ACB_201812151033.csv", {
        //                     error: function (text, data, XmlHttpRequest) {
        //                         webix.modalbox.hide(box);
        //                         alert(text);
        //                     },
        //                     success: function (text, data, XmlHttpRequest) {
        //                         var file = new Blob([data], { type: 'text/csv' });
        //                         webix.html.download(file, "baocao.csv");

        //                         webix.modalbox.hide(box);
        //                     }
        //                 });
        //         }
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     }
        // },
        {
            id: "btnRefresh",
            view: "button",
            type: "icon",
            icon: "fas fa-refresh",
            label: _("REFRESH"),
            css: "webix_secondary",
            width: 90,
            // shortcut: "F4",
            //css:"button_raised",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    table.clearAll();
                    //console.log(table);
                    table.load(table.config.url);
                    //table.refresh();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: _("THEM") + " (F4)",
            css: "webix_secondary",
            width: 85,
            //css:"button_raised",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var box = webix.modalbox({
                        title: "Chọn Token",
                        buttons: ["Nhận", "Hủy bỏ"],
                        text: "<div id='ctnToken'></div>",
                        top: 100,
                        width: 750,
                        callback: function (result) {
                            switch (result) {
                                case "1":
                                    webix.modalbox.hide();
                                    break;
                                case "0":
                                    var table = $$("tblToken");

                                    var item = {
                                        tokens: table.serialize(),
                                    };

                                    webix
                                        .ajax()
                                        .headers({ "Content-type": "application/json" })
                                        .post(app.config.host + "/Token/SaveTokens", item, function (text) {
                                            var json = JSON.parse(text);

                                            if (!json.hasOwnProperty("error")) {
                                                var table = $$("dmwindowData");
                                                table.clearAll();
                                                table.load(table.config.url);

                                                webix.modalbox.hide();
                                            } else {
                                                webix.message(json.error);
                                            }
                                        });

                                    break;
                            }
                        },
                    });

                    var popupText = document.getElementsByClassName("webix_popup_text")[0];
                    popupText.style.paddingTop = 0;

                    var tblMatHang = webix.ui({
                        container: "ctnToken",
                        id: "tblToken",
                        view: "datatable",
                        borderless: true,
                        height: 350,
                        columns: [
                            {
                                id: "chon",
                                header: _("CHON"),
                                width: 65,
                                checkValue: true,
                                uncheckValue: false,
                                template: "{common.checkbox()}",
                            },
                            {
                                id: "serialNumber",
                                header: _("SO_SERIAL"),
                                width: 150,
                            },
                            {
                                id: "tokenLabel",
                                header: _("TOKEN LABEL"),
                                width: 550,
                            },
                        ],
                        //url: app.config.host + "/Token/SelectTokens"
                    });

                    var url = app.config.host + "/System/ExecuteCommand";

                    webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(
                            url,
                            {
                                command: "CM00019",
                                parameter: {
                                    ma_dvcs: "",
                                },
                            },
                            function (text) {
                                var array = JSON.parse(text);

                                var kytaptrung = "";
                                var server_cert = "";

                                for (var i = 0; i < array.length; i++) {
                                    var item = array[i];

                                    if (item.code == "KYTAPTRUNG") {
                                        kytaptrung = item.gia_tri;
                                    }

                                    if (item.code == "SERVER_CERT") {
                                        server_cert = item.gia_tri;
                                    }
                                }

                                webix.ajax(server_cert + "/Token/SelectTokens", function (text) {
                                    $$("tblToken").clearAll();
                                    $$("tblToken").parse(text, "json");
                                });
                            }
                        );
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            label: _("SUA") + " (F3)",
            css: "webix_secondary",
            width: 75,
            //css:"button_raised",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("chukyso-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();
                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng cần sửa !", "error");
                        return;
                    }
                    /* var chon = item.chon;

                    item.chon = chon == true ? "true" : "false"; */

                    form.setValues(item);

                    dmwindowform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            css: "webix_secondary",
            label: _("XOA") + " (F8)",
            width: 75,
            //css:"button_raised",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    var khData = {};
                    khData.windowid = app.path[1].params[0];
                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(item);

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.serial_number,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/System/Save", khData, function (text) {
                                        if (text.hasOwnProperty("error")) {
                                            webix.message(text.error);
                                            return;
                                        }

                                        table.remove(item.id);
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                datafetch: 100,
                resizeColumn: true,
                //loadahead: 15,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var windowno = app.path[1].params[0];
                        var url = app.config.host + "/System/GetDataByWindowNo?window_id=" + windowno;

                        if (params == null) {
                            url = url + "&start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                        } else {
                            url = url + "&start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                            if (params.filter != null) {
                                var array = [];

                                for (var property in params.filter) {
                                    var column = view.getColumnConfig(property);

                                    if (column !== undefined) {
                                        var item = {
                                            columnName: property,
                                            columnType: column.columnType,
                                            value: params.filter[property],
                                        };

                                        array.push(item);
                                    }
                                }

                                //console.log(JSON.stringify(array));
                                if (array.length > 0) {
                                    url = url + "&filter=" + JSON.stringify(array);
                                } else {
                                    url = url + "&filter=null";
                                }
                            } else {
                                url = url + "&filter=null";
                            }
                        }

                        if (view.config.infoparam != null) {
                            url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
                        } else {
                            url = url + "&infoparam=null";
                        }

                        var tlbParams = $$("tlbParams");

                        if (tlbParams !== undefined) {
                            var childViews = tlbParams.getChildViews();

                            var parameters = [];

                            for (var i = 0; i < childViews.length; i++) {
                                var child = childViews[i];

                                var id = child.config.id;
                                var value_field = child.config.value_field;

                                var item = {
                                    columnName: value_field,
                                    value: $$(id).getValue(),
                                };

                                parameters.push(item);
                            }

                            url = url + "&tlbparam=" + JSON.stringify(parameters);
                        } else {
                            url = url + "&tlbparam=null";
                        }
                        //console.log(url);
                        webix.ajax(url, callback, view);
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.index = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    { id: "index", header: "", sort: "int", width: 40 },
                    { id: "serial_number", header: _("SO_SERIAL"), width: 200 },
                    { id: "token_label", header: "TOKEN LABEL", width: 350 },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
