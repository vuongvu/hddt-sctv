define([
    "app",
    "views/forms/dmwindow",
    "locale",
    "models/user",
    "views/forms/tinhhinhsudung"
], function (app, dmwindowform, _, user, tinhhinhsudungform) {
    'use strict';

    var op = [];
    webix.$$("title").parse({ title: "Tình hình sử dụng hóa đơn", details: "Báo cáo tình hình sử dụng hóa đơn (BC26/AC)" });

    var controls = [
        {},
        {
            id: "btnSave",
            view: "button",
            type: "danger",
            icon: "save",
            label: "Nhận (F10)",
            width: 85,
            shortcut: "F10",
            on: {
                onClick: function () {
                    var form = webix.$$("tinhhinhsudung");
                    var item = form.getValues();
                    var donvi = $$("cbdonvi").getValue();
            
                    if (donvi.length > 0) {
                        webix.$$('btnshow').callEvent("onClick");
                    } else {
                        webix.extend(form, webix.ProgressBar);
                        form.disable();
                        form.showProgress();
                        webix.ajax()
                            .headers({ 'Content-type': 'application/json' })
                            .post(app.config.host + "/Bcthsd/Check_xulyHD", item, function (text) {
                                var data = JSON.parse(text);

                                if (!data.hasOwnProperty("error")) {
                                    webix.$$('btnshow').callEvent("onClick");
                                }
                                else {
                                    webix.message(data.error, "error");
                                }
                                form.enable();
                                form.hideProgress();
                            });
                    }
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnshow",
            view: "button",
            type: "icon",
            icon: "save",
            label: "Nhận",
            hidden: true,
            width: 65,
            shortcut: "F11",
            on: {
                onClick: function () {

                    var form = webix.$$("tinhhinhsudung");
                    var item = form.getValues();
                    var win = this.$scope.ui(tinhhinhsudungform.$ui);

                    tinhhinhsudungform.initUI(item);
                    win.show();
                },
                click: function () {
                    this.callEvent("onClick");
                }
            }
        },
        {}
    ];

    var thang = webix.Date.dateToStr("%n");
    var thangcb = thang(new Date());
    var nam = webix.Date.dateToStr("%Y");
    var cbnam = nam(new Date());

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    var layout = {
        // paddingY: 60,
        // paddingX: 500,
        cols: [
            { gravity: 0.5, template: "" },
            {
                rows: [
                    { gravity: 1, template: "" },
                    {
                        view: "form",
                        id: "tinhhinhsudung",
                        paddingY: 10,
                        // width: 700,
                        // height:300,
                        position: "center",
                        paddingX: 10,
                        complexData: true,
                        margin: 0,
                        elements: [
                            {
                                view: "multicombo",
                                label: _("DONVI"),
                                name: "lst_branch_code",
                                id: "cbdonvi",
                                labelWidth: 80,
                                suggest: {
                                    selectAll: true,
                                    body: {
                                        url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00198"
                                    }
                                }
                            },
                            {
                                cols: [
                                    {
                                        id: "tkThang",
                                        name: "tokhai_thang",
                                        view: "checkbox",
                                        value: 1,
                                        label: "Tờ khai tháng",
                                        labelWidth: 100
                                    },
                                    {
                                        id: "tkQuy",
                                        name: "tokhai_quy",
                                        view: "checkbox",
                                        label: "Tờ khai quý",
                                        labelWidth: 100
                                    }
                                ]
                            },
                            {
                                cols: [
                                    {
                                        id: "cbThang",
                                        name: "thang",
                                        view: "combo",
                                        label: "Tháng",
                                        value: thangcb,
                                        options: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
                                    },
                                    {
                                        id: "cbNam",
                                        name: "nam",
                                        view: "text",
                                        value: cbnam,
                                        label: "Năm"
                                    }
                                ]
                            },
                            {
                                cols: [
                                    {
                                        id: "cbTungay",
                                        view: "datepicker",
                                        name: "tu_ngay",

                                        label: "Từ ngày"

                                    },
                                    {
                                        id: "cbDenngay",
                                        view: "datepicker",
                                        name: "den_ngay",

                                        label: "Đến ngày"
                                    }
                                ]
                            },
                            {
                                view: "fieldset",
                                label: "Chọn phụ lục kê khai",
                                body: {
                                    rows: [
                                        {
                                            view: "checkbox",
                                            id: "mau312",
                                            name: "mau_312",
                                            label: "Mẫu 3.12(Kỳ báo cáo cuối cùng)",
                                            labelWidth: 200
                                        },
                                        {
                                            view: "checkbox",
                                            id: "mau310",
                                            name: "mau_310",
                                            label: "Mẫu 3.10(Chuyển địa điểm)",
                                            labelWidth: 200
                                        },
                                    ]
                                }
                            },
                            {
                                id: "bcSoluong",
                                name: "bc_soluong",
                                view: "checkbox",
                                label: "Báo cáo theo số lượng hóa đơn",
                                labelWidth: 210
                            },
                            // {
                            //     view: "template",
                            //     template: "<hr class='ahii2'>"
                            //    // type: "section"
                            // },
                            {
                                margin: 10,
                                cols: controls

                            }

                        ]
                    },
                    { gravity: 5, template: "" }
                ]
            },
            { gravity: 0.5, template: "" }
        ]

    };

    function subview(config) {
        webix.ui(config, $$('tinhhinhsudung').getChildViews()[2].getChildViews()[0]);
    }

    var d = new Date();
    var quarter = Math.floor((d.getMonth() / 3));
    var quarter1 = quarter + 1;
    var firstDate = new Date(d.getFullYear(), quarter * 3, 1);
    var lastDate = new Date(firstDate.getFullYear(), firstDate.getMonth() + 3, 0)

    return {
        $ui: layout,
        $oninit: function (view, $scope) {

            var year = $$("cbNam").getValue();
            var month = $$("cbThang").getValue();
            var date1 = month + "/01" + "/" + year;

            var date = new Date(date1);
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            $$("cbTungay").setValue(firstDay);
            $$("cbDenngay").setValue(lastDay);
            var tkQuy = $$("tkQuy").getValue();

            $$("tkThang").attachEvent("onChange", function (newv, oldv) {
                if (newv == 1) {
                    $$("tkQuy").setValue(0);
                    $$("tkThang").setValue(1);

                    subview({
                        id: "cbThang",
                        name: "thang",
                        view: "combo",
                        label: "Tháng",
                        value: thangcb,
                        options: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
                    });
                    var ssk = $$('tinhhinhsudung').getChildViews()[2].getChildViews()[0];
                    ssk.define("id", "cbThang");

                    $$("cbThang").attachEvent("onChange", function (newv, oldv) {
                        var year = $$("cbNam").getValue();
                        var month = $$("cbThang").getValue();
                        var date1 = month + "/01" + "/" + year;

                        var date = new Date(date1);
                        var firstDay1 = new Date(date.getFullYear(), date.getMonth(), 1);

                        var lastDay1 = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                        $$("cbTungay").setValue(firstDay1);
                        $$("cbTungay").refresh();
                        $$("cbDenngay").setValue(lastDay1);
                        $$("cbDenngay").refresh();
                    });
                    $$("cbNam").attachEvent("onChange", function (newv, oldv) {
                        var year2 = $$("cbNam").getValue();
                        var month2 = $$("cbThang").getValue();
                        var date1 = month2 + "/01" + "/" + year2;

                        var date2 = new Date(date1);
                        var firstDay2 = new Date(date2.getFullYear(), date2.getMonth(), 1);

                        var lastDay2 = new Date(date2.getFullYear(), date2.getMonth() + 1, 0);
                        $$("cbTungay").setValue(firstDay2);
                        $$("cbTungay").refresh();
                        $$("cbDenngay").setValue(lastDay2);
                        $$("cbDenngay").refresh();
                    });
                    var year = $$("cbNam").getValue();
                    var month = $$("cbThang").getValue();
                    var date1 = month + "/01" + "/" + year;

                    var date = new Date(date1);
                    var firstDay1 = new Date(date.getFullYear(), date.getMonth(), 1);

                    var lastDay1 = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    $$("cbTungay").setValue(firstDay1);
                    $$("cbTungay").refresh();
                    $$("cbDenngay").setValue(lastDay1);
                    $$("cbDenngay").refresh();
                }
                // webix.message("value cũ : "+oldv+" Mới: "+newv);
            });

            $$("tkQuy").attachEvent("onChange", function (newv, oldv) {
                if (newv == 1) {
                    $$("tkThang").setValue(0);
                    subview({
                        id: "cbQuy",
                        name: "quy",
                        view: "combo",
                        label: "Quý",
                        value: quarter1,
                        options: ["1", "2", "3", "4"]
                    });
                    var ssk = $$('tinhhinhsudung').getChildViews()[2].getChildViews()[0];
                    ssk.define("id", "cbQuy");

                    $$("cbQuy").attachEvent("onChange", function (newv, oldv) {
                        var quy = $$("cbQuy").getValue();
                        //console.log(quy);
                        var nam = $$("cbNam").getValue();
                        var thang;
                        if (quy == 1) {
                            thang = 0;
                        }
                        if (quy == 2) {
                            thang = 3;
                        }
                        if (quy == 3) {
                            thang = 6
                        }
                        if (quy == 4) {
                            thang = 9
                        }
                        var firstDate = new Date(nam, thang, 1);
                        var lastDate = new Date(nam, thang + 3, 0);

                        $$("cbTungay").setValue(firstDate);
                        $$("cbTungay").refresh();
                        $$("cbDenngay").setValue(lastDate);
                        $$("cbDenngay").refresh();
                    });

                    var quy = $$("cbQuy").getValue();
                    var nam = $$("cbNam").getValue();
                    var thang;
                    if (quy == 1) {
                        thang = 0;
                    }
                    if (quy == 2) {
                        thang = 3;
                    }
                    if (quy == 3) {
                        thang = 6
                    }
                    if (quy == 4) {
                        thang = 9
                    }
                    var firstDate = new Date(nam, thang, 1);
                    var lastDate = new Date(nam, thang + 3, 0);

                    $$("cbTungay").setValue(firstDate);
                    $$("cbTungay").refresh();
                    $$("cbDenngay").setValue(lastDate);
                    $$("cbDenngay").refresh();

                }

            });

            $$("cbThang").attachEvent("onChange", function (newv, oldv) {
                var year = $$("cbNam").getValue();
                var month = $$("cbThang").getValue();
                var date1 = month + "/01" + "/" + year;

                var date = new Date(date1);
                var firstDay1 = new Date(date.getFullYear(), date.getMonth(), 1);

                var lastDay1 = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                $$("cbTungay").setValue(firstDay1);
                $$("cbTungay").refresh();
                $$("cbDenngay").setValue(lastDay1);
                $$("cbDenngay").refresh();
            });

            $$("cbNam").attachEvent("onChange", function (newv, oldv) {

                if ($$("tkThang").getValue() == 1) {
                    var year2 = $$("cbNam").getValue();
                    var month2 = $$("cbThang").getValue();
                    var date1 = month2 + "/01" + "/" + year2;

                    var date2 = new Date(date1);
                    var firstDay2 = new Date(date2.getFullYear(), date2.getMonth(), 1);

                    var lastDay2 = new Date(date2.getFullYear(), date2.getMonth() + 1, 0);
                    $$("cbTungay").setValue(firstDay2);
                    $$("cbTungay").refresh();
                    $$("cbDenngay").setValue(lastDay2);
                    $$("cbDenngay").refresh();
                }
                if ($$("tkQuy").getValue() == 1) {
                    var quy = $$("cbQuy").getValue();
                    //console.log(quy);
                    var nam = $$("cbNam").getValue();
                    var thang;
                    if (quy == 1) {
                        thang = 0;
                    }
                    if (quy == 2) {
                        thang = 3;
                    }
                    if (quy == 3) {
                        thang = 6
                    }
                    if (quy == 4) {
                        thang = 9
                    }
                    var firstDate = new Date(nam, thang, 1);
                    var lastDate = new Date(nam, thang + 3, 0);

                    $$("cbTungay").setValue(firstDate);
                    $$("cbTungay").refresh();
                    $$("cbDenngay").setValue(lastDate);
                    $$("cbDenngay").refresh();
                }
            });

            $$("mau312").attachEvent("onChange", function (newv, oldv) {
                if (newv == 1) {
                    $$("mau310").setValue(0);
                }

            });

            $$("mau310").attachEvent("onChange", function (newv, oldv) {
                if (newv == 1) {
                    $$("mau312").setValue(0);
                }

            });
        }
    };
});