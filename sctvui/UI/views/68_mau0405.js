define(["app", "locale", "models/user", "views/forms/68_huyhoadon_32-78"], function (app, _, user, form_0405) {
    "use strict";
    var param;
    webix.$$("title").parse({
        title: "Thông báo hủy và rà soát hóa đơn",
        details: "Bảng tổng hợp hóa đơn",
    });

    var initUI = function () {
        //var windowno = app.path[1].params[0];
        var table = webix.$$("mau04_68");
        // table.disable();
        // table.showProgress();
        // table.clearAll();
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var url = app.config.host + "/Invoice68/getDatamau04";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("mau04_68");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(form_0405.$ui);
                    form_0405.setEditMode(1);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var table = webix.$$("mau04_68");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn hóa đơn ! ", "error");
                        return;
                    }
                    if (selectedItem.trang_thai == 1) {
                        webix.message("Mẫu đã ký và gửi CQT bạn không thể sửa! ", "error");
                        return;
                    }

                    if (selectedItem.loai_tbao == "5") {
                        webix.message("Mẫu 05 nhận từ CQT bạn không thể sửa! ", "error");
                        return;
                    }
                    if (selectedItem.is_hdon78 == 1) {
                        webix.message("Mẫu được này không được chỉnh sửa");
                        return;
                    }

                    var detailForm = this.$scope.ui(form_0405.$ui);
                    form_0405.setEditMode(2);
                    var item = table.getSelectedItem();
                    var url = app.config.host + "/Invoice68/GetDataMau0405Detail?id=" + item.id;
                    var result = webix.ajax().sync().get(url);
                    var json = JSON.parse(result.response);
                    item.detail = json;
                    form_0405.setValues(item);
                    var tableDetail = webix.$$("danhsach_hoadonhuy");
                    var url = app.config.host + "/Invoice68/GetDataMau0405Detail?id=" + item.id;
                    tableDetail.clearAll();
                    tableDetail.load(url);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("mau04_68");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn khách hàng ! ", "error");
                        return;
                    }
                    if (selectedItem.trang_thai == 1) {
                        webix.message("Mẫu đã ký và gửi CQT bạn không thể xóa! ", "error");
                        return;
                    }
                    if (selectedItem.loai_tbao == "5") {
                        webix.message("Mẫu 05 nhận từ CQT bạn không thể xóa! ", "error");
                        return;
                    }

                    webix.confirm({
                        text: "Bạn có muốn xóa hóa đơn",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/deleteMau04", selectedItem, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.error, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            table.remove(selectedItem.id);
                                            table.enable();
                                            table.hideProgress();
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        // {
        //     view: "button",
        //     type: "htmlbutton",
        //     width: 100,
        //     label: '<i class="fa fa-eye fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Xem in") + " (F7)" + "</span>",
        //     id: "btnPreview",
        //     shortcut: "F7",
        //     on: {
        //         onClick: function () {
        //             var table = webix.$$("mau04_68");
        //             var selectedItem = table.getSelectedItem();
        //             if (selectedItem == null) {
        //                 webix.message("Bạn chưa dòng in ! ", "error");
        //                 return;
        //             }
        //             var box = webix.modalbox({
        //                 title: "Đang in hóa đơn ....",
        //                 text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
        //                 width: 350,
        //                 height: 200,
        //             });

        //             var newWin = window.open();
        //             var html =
        //                 "<html><head><title>Hóa đơn điện tử SCTV</title>" +
        //                 '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
        //                 '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
        //                 '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
        //             newWin.document.write(html);

        //             webix
        //                 .ajax()
        //                 .response("arraybuffer")
        //                 .headers({ "Content-type": "application/json" })
        //                 .get(app.config.host + "/Invoice68/inMau04?id=" + selectedItem.id + "&type=PDF&loai=" + selectedItem.loai_tbao, {
        //                     error: function (text, data, XmlHttpRequest) {
        //                         webix.modalbox.hide(box);
        //                         webix.message(XmlHttpRequest.statusText, "error");
        //                     },
        //                     success: function (text, data, XmlHttpRequest) {
        //                         var file = new Blob([data], { type: "application/pdf" });
        //                         var fileURL = URL.createObjectURL(file);
        //                         //window.open(fileURL, '_blank');
        //                         webix.modalbox.hide(box);
        //                         newWin.location = fileURL;
        //                     },
        //                 });
        //         },
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     },
        // },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-pencil fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Ký gửi CQT") + "</span>",
            id: "btnSign",
            on: {
                onClick: function () {
                    var tableDK = webix.$$("mau04_68");
                    var selectedItem = tableDK.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn dòng ! ", "error");
                        return;
                    }
                    if (selectedItem.trang_thai == 1) {
                        webix.message("Mẫu đã ký và gửi CQT bạn không thể ký lại! ", "error");
                        return;
                    }

                    webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .get(app.config.host + "/Invoice68/GetSignType", function (text) {
                            var result = JSON.parse(text);
                            var kytaptrung = "";
                            for (var i = 0; i < result.length; i++) {
                                var item = result[i];
                                kytaptrung = item.value;
                            }

                            webix.ajax().get(app.config.host + "/Invoice68/GetListCertificatesFile68", function (text) {
                                var listcerts = JSON.parse(text);
                                if (listcerts.length > 0) {
                                    var box = webix.modalbox({
                                        title: "Chọn chứng thư số",
                                        buttons: ["Nhận", "Hủy bỏ"],
                                        text: "<div id='ctnCerts'></div>",
                                        top: 100,
                                        width: 750,
                                        callback: function (result) {
                                            switch (result) {
                                                case "1":
                                                    webix.modalbox.hide();
                                                    break;
                                                case "0":
                                                    var table = $$("tblCerts");
                                                    var data = table.serialize();

                                                    var cert = null;

                                                    for (var i = 0; i < data.length; i++) {
                                                        if (data[i].chon == true) {
                                                            cert = data[i];
                                                            break;
                                                        }
                                                    }

                                                    if (cert == null) {
                                                        webix.message("Bạn chưa chọn chứng thư số");
                                                        return;
                                                    }
                                                    var postData = {
                                                        branch_code: user.getDvcsID(),
                                                        username: user.getCurrentUser(),
                                                        cer_serial: cert.cer_serial,
                                                        id: selectedItem.id,
                                                    };

                                                    webix.extend(tableDK, webix.ProgressBar);
                                                    tableDK.disable();
                                                    tableDK.showProgress();
                                                    webix
                                                        .ajax()
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(app.config.host + "/Invoice68/huyhoadonSign", postData, {
                                                            error: function (text, data, XmlHttpRequest) {
                                                                tableDK.enable();
                                                                tableDK.hideProgress();
                                                                // var json = JSON.parse(text);
                                                                // webix.message(json.ExceptionMessage, "error");
                                                                webix.message(text + XmlHttpRequest, "error");
                                                            },
                                                            success: function (text, data, XmlHttpRequest) {
                                                                var json = JSON.parse(text);
                                                                tableDK.enable();
                                                                tableDK.hideProgress();
                                                                if (!json.hasOwnProperty("error")) {
                                                                    webix.message("Gửi thành công đến CQT !", "success");
                                                                    // $$("huyhoadonForms").close();
                                                                    app.callEvent("dataChangedMau0405", [1, null]);
                                                                } else {
                                                                    webix.message(json.error, "error");
                                                                }
                                                            },
                                                        });

                                                // return;
                                            }
                                        },
                                    });

                                    var tblMatHang = webix.ui({
                                        container: "ctnCerts",
                                        id: "tblCerts",
                                        view: "datatable",
                                        borderless: true,
                                        resizeColumn: true,
                                        height: 350,
                                        columns: [
                                            {
                                                id: "chon",
                                                header: _("CHON"),
                                                width: 65,
                                                checkValue: true,
                                                uncheckValue: false,
                                                template: "{common.checkbox()}",
                                            },
                                            {
                                                id: "cer_serial",
                                                header: _("SO_SERIAL"),
                                                width: 150,
                                            },
                                            {
                                                id: "subject_name",
                                                header: _("SUBJECTNAME"),
                                                width: 450,
                                            },
                                            {
                                                id: "begin_date",
                                                header: _("NGAY_BAT_DAU"),
                                                width: 200,
                                            },
                                            {
                                                id: "end_date",
                                                header: _("NGAY_KET_THUC"),
                                                width: 200,
                                            },
                                            {
                                                id: "issuer",
                                                header: _("DONVI"),
                                                width: 450,
                                            },
                                        ],
                                        url: app.config.host + "/Invoice68/GetListCertificatesFile68",
                                    });
                                } else {
                                    var table = $$("mau04_68");
                                    webix.extend(table, webix.ProgressBar);
                                    table.disable();
                                    table.showProgress();

                                    // xem user có tích checkbox ký HSM không?
                                    var checkSigHSM = webix.ajax(app.config.host + "/Account/GetInfoUserLogin");
                                    webix.promise.all([checkSigHSM]).then(function (resultscheckSigHSM) {
                                        if (resultscheckSigHSM[0].json() == "C") {
                                            var postData = {
                                                branch_code: user.getDvcsID(),
                                                username: user.getCurrentUser(),
                                                id: selectedItem.id,
                                                check_HSM: "1",
                                            };

                                            webix.extend(tableDK, webix.ProgressBar);
                                            tableDK.disable();
                                            tableDK.showProgress();
                                            webix
                                                .ajax()
                                                .headers({ "Content-type": "application/json" })
                                                .post(app.config.host + "/Invoice68/huyhoadonSign", postData, {
                                                    error: function (text, data, XmlHttpRequest) {
                                                        tableDK.enable();
                                                        tableDK.hideProgress();
                                                        // var json = JSON.parse(text);
                                                        // webix.message(json.ExceptionMessage, "error");
                                                        webix.message(text + XmlHttpRequest, "error");
                                                    },
                                                    success: function (text, data, XmlHttpRequest) {
                                                        var json = JSON.parse(text);
                                                        tableDK.enable();
                                                        tableDK.hideProgress();
                                                        if (!json.hasOwnProperty("error")) {
                                                            webix.message("Gửi thành công đến CQT !", "success");
                                                            // $$("huyhoadonForms").close();
                                                            app.callEvent("dataChangedMau0405", [1, null]);
                                                        } else {
                                                            webix.message(json.error, "error");
                                                        }
                                                    },
                                                });
                                        } else {
                                            var a = webix.ajax(app.config.host + "/Invoice68/XmlMau0405?id=" + selectedItem.id);
                                            var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

                                            webix.promise.all([a, b, selectedItem]).then(function (results) {
                                                // webix.extend(tblInvoice, webix.ProgressBar);
                                                // tblInvoice.disable();
                                                // tblInvoice.showProgress();
                                                var json = results[0].json();

                                                if (json.hasOwnProperty("error")) {
                                                    webix.message({ type: "error", text: json.error });
                                                }

                                                var listcerts = results[1].json();
                                                var item = results[2];

                                                if (listcerts.length == 0) {
                                                    table.enable();
                                                    table.hideProgress();
                                                    webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                                                    return;
                                                }

                                                require(["views/modules/minvoiceplugin"], function (plugin) {
                                                    var hub = plugin.hub();
                                                    var hubconnect = plugin.conn();
                                                    if (hubconnect.state != 1) {
                                                        table.enable();
                                                        table.hideProgress();
                                                        webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                                                        return;
                                                    }
                                                    hub.off("resCommand");
                                                    hub.on("resCommand", function (result) {
                                                        var json = JSON.parse(result);

                                                        if (json.hasOwnProperty("error")) {
                                                            webix.message({ type: "error", text: json.error });
                                                        } else {
                                                            var parameters = {
                                                                xml: json.xml,
                                                                id: selectedItem.id,
                                                                tthdon: 3,
                                                            };

                                                            webix
                                                                .ajax()
                                                                .headers({ "Content-Type": "application/json" })
                                                                .post(app.config.host + "/Invoice68/SaveXmlMau0405", parameters, function (text) {
                                                                    var result = JSON.parse(text);
                                                                    if (result.hasOwnProperty("error")) {
                                                                        webix.message(result.error, "error");
                                                                    } else {
                                                                        webix.message("Gửi thành công đến CQT !", "success");
                                                                        // $$("huyhoadonForms").close();
                                                                        app.callEvent("dataChangedMau0405", [1, null]);
                                                                    }
                                                                });
                                                        }
                                                        table.enable();
                                                        table.hideProgress();
                                                    });

                                                    var _listcerts = [];

                                                    for (var i = 0; i < listcerts.length; i++) {
                                                        var _cert = listcerts[i];

                                                        _listcerts.push({
                                                            so_serial: _cert.cer_serial,
                                                            ngaybatdau: _cert.begin_date,
                                                            ngayketthuc: _cert.end_date,
                                                            issuer: _cert.issuer,
                                                            subjectname: _cert.subject_name,
                                                        });
                                                    }

                                                    var arg = {
                                                        idData: "#data",
                                                        xml: json["xml"],
                                                        shdon: "",
                                                        idSigner: "seller",
                                                        tagSign: "NNT",
                                                        dvky: json["dvky"],
                                                        listcerts: _listcerts,
                                                    };

                                                    hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
                                                        .done(function () {
                                                            console.log("Success !");
                                                        })
                                                        .fail(function (error) {
                                                            webix.message(error, "error");
                                                        });
                                                });
                                            });
                                            // app.callEvent("dataChangedInvoie68", [editmode, null]);

                                            // webix.message("Thành công !", "success");
                                            // webix.$$("huyhoadonForms").close();
                                        }
                                    });
                                }
                            });
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "mau04_68",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                subview: {
                    borderless: true,
                    view: "tabview",
                    height: 255,
                    scrollX: true,
                    cells: [
                        {
                            header: "Chi tiết hóa đơn",
                            body: {
                                view: "datatable",
                                height: 200,
                                resizeColumn: true,
                                // scroll: true,
                                id: "mau04_68_chitiet",
                                headerRowHeight: 50,
                                on: {
                                    "data->onStoreUpdated": function () {
                                        this.data.each(function (obj, i) {
                                            obj.index = i + 1;
                                        });
                                    },
                                },
                                columns: [
                                    {
                                        view: "text",
                                        id: "mau04_id",
                                        name: "mau04_id",
                                        hidden: true,
                                    },
                                    {
                                        view: "text",
                                        id: "mau04_chitiet_id",
                                        name: "mau04_chitiet_id",
                                        hidden: true,
                                    },
                                    {
                                        view: "text",
                                        header: "STT",
                                        id: "index",
                                        name: "index",
                                        width: 80,
                                    },
                                    {
                                        header: "Mẫu số",
                                        id: "mau_so",
                                        width: 120,
                                        // template: function (obj) {
                                        //     return obj.khieu.toString().substring(0, 1);
                                        // },
                                    },

                                    {
                                        id: "khieu",
                                        name: "khieu",
                                        header: "Ký hiệu hóa đơn điện tử",
                                        width: 150,
                                    },
                                    {
                                        id: "shdon",
                                        name: "shdon",
                                        header: "Số hóa đơn điện tử",
                                        width: 120,
                                    },
                                    {
                                        id: "ngay",
                                        name: "ngay",
                                        header: "Ngày lập hóa đơn",
                                        width: 120,
                                        format: function (text) {
                                            var format = webix.Date.dateToStr("%d/%m/%Y");
                                            text = removeTimeZoneT(text);
                                            return format(text);
                                        },
                                    },

                                    {
                                        id: "ladhddt",
                                        name: "ladhddt",
                                        header: "Loại áp dụng hóa đơn điện tử",
                                        template: function (obj) {
                                            switch (obj.ladhddt) {
                                                case 1:
                                                    return "<span>HĐĐT Nghị đinh số 123/2020/NĐ-CP</span> ";
                                                case 2:
                                                    return "<span>HĐĐT có mã của cơ quan thu</span> ";
                                                case 3:
                                                    return "<span>Các loại hóa đơn theo Nghị đinh 51/2010</span> ";
                                                case 4:
                                                    return "<span>Hóa đơn đặt in theo Nghị định số 123/2020/NĐ-C</span> ";
                                                default:
                                                    break;
                                            }
                                        },
                                        width: 150,
                                    },
                                    {
                                        id: "tctbao",
                                        name: "tctbao",
                                        header: "Hủy/giải trình",
                                        width: 150,
                                    },
                                    {
                                        id: "ldo",
                                        name: "ldo",
                                        header: "Lý do",
                                        width: 250,
                                    },
                                    {
                                        id: "tttnhan",
                                        name: "tttnhan",
                                        header: "Trạng thái tiếp nhận",
                                        template: function (obj) {
                                            if (obj.tttnhan == 1) {
                                                return "<span style='color: #00b300'>Chấp thuận</span> ";
                                            } else {
                                                if (obj.tttnhan == 2) {
                                                    return "<span style='color: #e83717'>Không chấp thuận</span> ";
                                                } else {
                                                    return "<span>Chờ phản hồi</span>";
                                                }
                                            }
                                        },
                                        width: 200,
                                    },
                                    {
                                        id: "ly_do",
                                        name: "ldly_doo",
                                        header: "Lý do không tiếp nhận",
                                        width: 250,
                                    },
                                ],
                                data: [
                                    {
                                        index: 1,
                                        khieu: "1KT20YY",
                                        shdon: "0000001",
                                        ngay: new Date(),
                                        ladhddt: "1",
                                        ldo: "Sai thông tin",
                                    },
                                    {
                                        index: 2,
                                        khieu: "1KT20YY",
                                        shdon: "0000002",
                                        ngay: new Date(),
                                        ladhddt: "1",
                                        ldo: "Sai tiền",
                                    },
                                ],
                                // scrollX: false,
                                // autoheight: true,
                            },
                        },
                    ],
                },
                scheme: {
                    $init: function () {
                        // this.data.each(function (obj, i) {
                        //     obj.index = i + 1;
                        // });
                    },
                },
                onClick: {
                    // ShowHistory: function (event, cell, target){
                    //     debugger
                    //     var table = webix.$$("mau04_68");
                    //     var win = table.$scope.ui(hoadonHistory.$ui);
                    //     var hdon_id = cell.row;
                    //     hoadonHistory.setEditMode(hdon_id);
                    //     win.show();
                    // },
                    Dowload0405: function (event, cell, target) {
                        var win = webix.$$("mau04_68");
                        var record = win.getItem(cell.row);

                        if (record.loai_tbao != "5") {
                            if (record.trang_thai != 1) {
                                webix.message("Mẫu chưa được ký không thể tải !", "error");
                                return;
                            }
                        }
                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();
                        var filename = record.loai_tbao == "5" ? "Mau05.xml" : "Mau04.xml";
                        var url = app.config.host + "/Invoice68/ExportXMLMau04?id=" + cell.row;
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .get(url, function (text, data) {
                                win.enable();
                                win.hideProgress();

                                var file = new Blob([data], { type: "application/xml" });
                                webix.html.download(file, filename);
                                // var fileURL = URL.createObjectURL(file);
                                // window.open(fileURL, "_blank");
                            });
                        // webix.message("Delete row: " + cell.row);
                        return false; // here it blocks the default behavior
                    },
                    Dowload0405Rep: function (event, cell, target) {
                        var win = webix.$$("mau04_68");
                        var record = win.getItem(cell.row);

                        if (record.loai_tbao != "5") {
                            if (record.trang_thai != 1) {
                                webix.message("Mẫu chưa được ký không thể tải !", "error");
                                return;
                            }
                        }
                        webix.extend(win, webix.ProgressBar);
                        win.disable();
                        win.showProgress();
                        var filename = record.loai_tbao == "5" ? "Mau05Rep.xml" : "Mau04Rep.xml";
                        var url = app.config.host + "/Invoice68/ExportXMLMau04Rep?id=" + cell.row;
                        webix
                            .ajax()
                            .response("arraybuffer")
                            .get(url, function (text, data) {
                                win.enable();
                                win.hideProgress();

                                var file = new Blob([data], { type: "application/xml" });
                                webix.html.download(file, filename);
                                // var fileURL = URL.createObjectURL(file);
                                // window.open(fileURL, "_blank");
                            });
                        // webix.message("Delete row: " + cell.row);
                        return false; // here it blocks the default behavior
                    },
                    InMau0405: function (event, cell, target) {
                        var table = webix.$$("mau04_68");
                        var item = table.getItem(cell.row);

                        //webix.message("Chưa có mẫu in ...", "error");
                        // var rpParameter = null;
                        var box = webix.modalbox({
                            title: "Đang in hóa đơn ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        var newWin = window.open();
                        var html =
                            "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                            '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                            '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                            '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                        newWin.document.write(html);

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inMau04?id=" + item.id + "&type=PDF&loai=" + item.loai_tbao, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    },
                    InMau0405Rep: function (event, cell, target) {
                        var table = webix.$$("mau04_68");
                        var item = table.getItem(cell.row);

                        //webix.message("Chưa có mẫu in ...", "error");
                        // var rpParameter = null;
                        var box = webix.modalbox({
                            title: "Đang in hóa đơn ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        var newWin = window.open();
                        var html =
                            "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                            '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                            '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                            '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                        newWin.document.write(html);

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .get(app.config.host + "/Invoice68/inMau04Rep?id=" + item.id + "&type=PDF&loai=" + item.loai_tbao, {
                                error: function (text, data, XmlHttpRequest) {
                                    webix.modalbox.hide(box);
                                    webix.message(XmlHttpRequest.statusText, "error");
                                },
                                success: function (text, data, XmlHttpRequest) {
                                    var file = new Blob([data], { type: "application/pdf" });
                                    var fileURL = URL.createObjectURL(file);
                                    //window.open(fileURL, '_blank');
                                    webix.modalbox.hide(box);
                                    newWin.location = fileURL;
                                },
                            });
                    },
                },
                on: {
                    onSubViewCreate: function (view, item) {
                        var subView = view;

                        if (subView != null) {
                            var tableDetail = webix.$$("mau04_68_chitiet");
                            var url = app.config.host + "/Invoice68/GetDataMau0405Detail?id=" + item.id;
                            tableDetail.clearAll();
                            tableDetail.load(url);
                        }
                    },
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onAfterLoad: function () {},
                },
                columns: [
                    {
                        id: "mau04_id",
                        header: "mau04_id",
                        hidden: true,
                    },
                    {
                        template: "{common.subrow()}",
                        width: 50,
                    },
                    {
                        id: "stt",
                        header: "STT",
                        sort: "int",
                        width: 40,
                    },
                    {
                        id: "shdon",
                        header: [
                            "Số hóa đơn",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 200,
                    },
                    {
                        id: "khieu",
                        header: [
                            "Ký hiệu",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 200,
                    },
                    {
                        id: "loai_tbao",
                        header: [
                            "Loại thông báo",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 200,
                        template: function (obj) {
                            return obj.loai_tbao == "5" ? "Hóa đơn cần rà soát" : "Thông báo hóa đơn sai sót";
                        },
                    },
                    {
                        id: "trang_thai",
                        header: [
                            "Trạng thái",
                            {
                                content: "serverSelectFilter",
                                options: [
                                    { id: "", value: "" },
                                    { id: "null", value: "Chưa gửi CQT" },
                                    { id: "1", value: "Đã gửi CQT" },
                                ],
                            },
                        ],
                        width: 150,
                        template: function (obj) {
                            if (obj.loai_tbao == "5") {
                                return "";
                            } else {
                                return obj.trang_thai == 1
                                    ? "<span style='color: #00b300'><i class='fas fa-check'></i></span> Đã gửi CQT"
                                    : "<span style='color: red'><i class='fas fa-times'></i></span> Chưa gửi CQT";
                            }
                        },
                    },
                    {
                        id: "statustn",
                        header: ["<center>Trạng thái tiếp nhận CQT</center>", { content: "textFilter" }],
                        width: 150,
                        columnType: "nvarchar",
                        template: function (obj) {
                            if (obj.xml_nhan != null && obj.xml_nhan != "") {
                                return "Đã nhận phản hồi";
                            } else {
                                return "Chưa nhận phản hồi";
                            }
                        },
                    },

                    // {
                    //     id: "btnMessagetn",
                    //     header: ["", "Lịch sử xử lý"],
                    //     css: { "text-align": "center" },
                    //     template: function (obj) {
                    //         return "<span class='ShowHistory' style='color: #00b300'><i class='fa fa-eye'></i></span> ";
                    //     },
                    //     width: 180,
                    // },

                    // {
                    //     id: "mtdiep_gui",
                    //     header: [
                    //         "Mã thông điệp",
                    //         {
                    //             content: "serverFilter",
                    //         },
                    //     ],
                    //     width: 150,
                    // },
                    {
                        id: "ntbao",
                        header: [
                            "Ngày tạo",
                            {
                                content: "serverDateRangeFilter",
                            },
                        ],
                        width: 120,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        css: { "text-align": "center" },
                        columnType: "datetime",
                    },
                    {
                        id: "ngay_gui",
                        header: [
                            "Ngày gửi",
                            {
                                content: "serverDateRangeFilter",
                            },
                        ],
                        width: 120,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        css: { "text-align": "center" },
                        columnType: "datetime",
                    },
                    {
                        id: "Dowload0405",
                        header: ["", "Tải về"],
                        css: { "text-align": "center" },
                        width: 120,
                        template: function (obj) {
                            if (obj.loai_tbao == "5") {
                                return "<span class='Dowload0405' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            } else {
                                if (obj.trang_thai == 1) {
                                    return "<span class='Dowload0405' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                                } else return "<span class='Dowload0405' style='color: #c8c8cc'><i class='fas fa-download'></i></span> ";
                            }
                        },
                    },
                    {
                        id: "btnInMau0405",
                        header: ["", "In"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.loai_tbao == "5") {
                                return "<span class='InMau0405' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                            } else {
                                if (obj.trang_thai == 1) {
                                    return "<span class='InMau0405' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                                } else return "<span class='InMau0405' style='color: #c8c8cc'><i class='fas fa-print'></i></span> ";
                            }
                        },
                        width: 120,
                    },
                    {
                        id: "nguoi_gui",
                        header: [
                            "Người gửi",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "Dowload0405Rep",
                        header: ["", "Tải về"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.xml_nhan != null && obj.xml_nhan != "") {
                                return "<span class='Dowload0405Rep' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                            } else return "<span class='Dowload0405Rep' style='color: #c8c8cc'><i class='fas fa-download'></i></span> ";
                        },
                        width: 120,
                    },
                    {
                        id: "btnInMau0405Rep",
                        header: ["", "In"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            if (obj.xml_nhan != null && obj.xml_nhan != "") {
                                return "<span class='InMau0405Rep' style='color: #00b300'><i class='fas fa-print'></i></span> ";
                            } else return "<span class='InMau0405Rep' style='color: #c8c8cc'><i class='fas fa-print'></i></span> ";
                        },
                        width: 120,
                    },
                    // {
                    //     id: "mtdiepcqt",
                    //     header: ["", "Mã thông điệp CQT"],
                    //     width: 250,
                    // },
                ],
                // data: [
                //     {
                //         stt: 1,
                //         loai_tb: "4",
                //         trang_thai: "Chờ duyệt",
                //         mso: "001",
                //         ntbao: new Date(),
                //         nguoi_gui: "ADMINNISTRATOR"
                //     },
                //     {
                //         stt: 2,
                //         loai_tb: "5",
                //         trang_thai: "Chờ duyệt",
                //         mso: "002",
                //         ntbao: new Date(),
                //         nguoi_gui: "ADMINNISTRATOR"
                //     }
                // ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                // height: 40,
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var table = webix.$$("mau04_68");

            $scope.on(app, "dataChangedMau0405", function (mode, item) {
                // if (mode == 1) {
                //     table.add(item);
                // }
                // if (mode == 5) {
                table.clearAll();
                table.load(table.config.url);
                // }
            });

            $scope.on(app, "filterTable", function (report_id, infoWindowId, item) {
                var infoparam = {
                    infowindow_id: infoWindowId,
                    parameter: item,
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
