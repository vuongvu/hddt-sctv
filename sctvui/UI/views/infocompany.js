define([
    "app",
    "locale",
    "views/forms/license",
], function (app, _, licenseForm) {
    'use strict';

    webix.$$("title").parse({ title: "Thông tin doanh nghiệp", details: "Cập nhật thông tin doanh nghiệp" });

    var controls = [
        {},
        {
            id: "btnSave",
            view: "button",
            type: "danger",
            //css: "mcls_btn_save", 
            icon: "save",
            label: _("Ghi") + " (F10)",
            width: 85,
            shortcut: "F10",
            on: {
                onClick: function () {

                    var form = webix.$$("foInfoCompany");
                    webix.extend(form, webix.ProgressBar);
                    form.disable();
                    form.showProgress();
                    if (form.validate()) {
                        var item = form.getValues();

                        webix.ajax()
                            .bind(form)
                            .headers({ 'Content-type': 'application/json' })
                            .post(app.config.host + "/System/SaveInfoCompany", item, function (text) {
                                var data = JSON.parse(text);

                                this.enable();
                                this.hideProgress();

                                if (!data.hasOwnProperty("error")) {
                                    webix.message({ type: "debug", text: "Đã lưu thành công" });

                                }
                                else {
                                    webix.message({ type: "error", text: data.error });
                                }
                            });
                    }

                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {}
    ];

    var layout = {
        cols: [
            { gravity: 0.5, template: "" },
            {
                rows: [
                    { height: 20 },
                    {
                        view: "form",
                        id: "foInfoCompany",
                        paddingY: 10,
                        paddingX: 10,
                        complexData: true,
                        margin: 0,
                        elements: [
                            {
                                view: "text",
                                name: "tax_code",
                                label: _("MS_THUE"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "text",
                                name: "name",
                                label: _("DONVI"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "text",
                                name: "address",
                                label: _("DIA_CHI"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "nguoi_dai_dien",
                                        label: _("NGUOI_DAI_DIEN_PL"),
                                        css: "mcls_readonly_text",
                                        readonly: true,
                                        labelPosition: "top"
                                    },
                                    {
                                        view: "text",
                                        name: "email",
                                        label: "Email",
                                        css: "mcls_readonly_text",
                                        readonly: true,
                                        labelPosition: "top"
                                    }
                                ]
                            },
                            {
                                view: "text",
                                name: "begin_date",
                                label: _("NGAY_BD_NAM_TC"),
                                placeholder: "dd/mm",
                                labelPosition: "top",
                                invalidMessage: _("NGAY_BD_NAM_TC") + " không hợp lệ",
                                on: {
                                    onChange: function (newv, oldv) {
                                        this.validate();
                                    }
                                }
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "tel",
                                        label: _("DIEN_THOAI"),
                                        labelPosition: "top",
                                        validate: webix.rules.isPhoneNumber,
                                        invalidMessage: "Định dạng điện thoại nhập không đúng",
                                        on: {
                                            onChange: function (newv, oldv) {
                                                if (newv != "") {
                                                    this.validate();
                                                }
                                            }
                                        }
                                    },

                                    {
                                        view: "text",
                                        name: "fax",
                                        label: "Fax",
                                        labelPosition: "top",
                                        validate: webix.rules.isPhoneNumber,
                                        invalidMessage: "Định dạng fax nhập không đúng",
                                        on: {
                                            onChange: function (newv, oldv) {
                                                if (newv != "") {
                                                    this.validate();
                                                }
                                            }
                                        }
                                    }
                                ]
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "city",
                                        label: _("TINH_THANH"),
                                        labelPosition: "top"
                                    },

                                    {
                                        view: "text",
                                        name: "district",
                                        label: "Quận huyện",
                                        labelPosition: "top"
                                    }
                                ]
                            },
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "bank_account",
                                        label: _("TAI_KHOAN"),
                                        labelPosition: "top",
                                        validate: webix.rules.isPhoneNumber,
                                        invalidMessage: "Định dạng tài khoản nhập không đúng",
                                        on: {
                                            onChange: function (newv, oldv) {
                                                if (newv != "") {
                                                    this.validate();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        view: "text",
                                        name: "bank_name",
                                        label: _("TAI_NH"),
                                        labelPosition: "top"
                                    }
                                ]
                            },
                            {
                                cols: [
                                    {
                                        view: "combo",
                                        name: "provincial_tax_authorities",
                                        label: "Cơ quan thuế cấp tỉnh",
                                        labelPosition: "top",
                                        options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00015",
                                        on: {
                                            onChange: function (newv, oldv) {
                                                webix.ajax()
                                                    .headers({ 'Content-Type': 'application/json' })
                                                    .post(
                                                    app.config.host + "/System/ExecuteCommand",
                                                    {
                                                        command: "CM00007",
                                                        parameter: {
                                                            nhom: newv
                                                        }
                                                    },
                                                    function (text) {
                                                        var result = JSON.parse(text);

                                                        var list = $$("ma_cqthuecapql").getPopup().getList();
                                                        list.clearAll();

                                                        list.parse(result);
                                                    });


                                            }
                                        }
                                    },
                                    {
                                        id: "ma_cqthuecapql",
                                        view: "combo",
                                        name: "manage_tax_authorities",
                                        label: "Cơ quan thuế quản lý",
                                        labelPosition: "top",
                                        options: []
                                    }
                                ]
                            },
                            {
                                view: "fieldset",
                                label: "Thông tin đơn vị chủ quản",
                                body: {
                                    rows: [
                                        {
                                            view: "text",
                                            name: "manage_taxcode",
                                            label: _("MS_THUE"),
                                            validate: webix.rules.isTaxCode,
                                            invalidMessage: "Mã số thuế không hợp lệ",
                                            labelPosition: "top",
                                            on: {
                                                onChange: function (newv, oldv) {
                                                    if (newv != "") {
                                                        this.validate();
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            view: "text",
                                            name: "manage_unit_name",
                                            label: _("DONVI"),
                                            labelPosition: "top"
                                        },
                                        {
                                            view: "text",
                                            name: "manage_email",
                                            validate: webix.rules.isEmail,
                                            invalidMessage: "Email không hợp lệ",
                                            label: _("EMAIL"),
                                            labelPosition: "top",
                                            on: {
                                                onChange: function (newv, oldv) {
                                                    if (newv != "") {
                                                        this.validate();
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                margin: 15,
                                cols: controls
                            }
                        ],
                        rules: {
                            begin_date: function (value) {
                                if (value != "") {

                                    if (value.toString().length != 5)
                                        return false;

                                    var patt = /^[0-1][1-2][/][0-9][0-9]$/;

                                    return patt.test(value);

                                }
                                return true;
                            }
                        }
                    },
                    { height: 10 }
                ]
            },
            { gravity: 0.5, template: "" }

        ]

    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var url = app.config.host + "/System/GetInfoCompany";

            webix.ajax(url, function (text) {
                var json = JSON.parse(text);
                $$("foInfoCompany").setValues(json);

            });
        }

    };
});