define(["app", "locale", "models/user", "views/forms/68_lichsu_history"], function (app, _, user, hoadonHistory) {
    "use strict";
    webix.$$("title").parse({ title: "Lịch sử truyền nhận", details: "Lịch sử truyền nhận" });
    var grid = {
        rows: [
            {
                view: "datatable",
                id: "truyenhanview",
                select: true,
                resizeColumn: true,
                fixedRowHeight: false,
                // leftSplit: 8,
                // rightSplit: 1,
                rowLineHeight: 25,
                rowHeight: 25,
                tooltip: true,
                scroll: true,
                //autowidth: true,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var url = app.config.host + "/Invoice68/GetHistoryTruyenNhan";

                        var tlbParams = $$("param");
                        if (params == null) {
                            url = url + "?start=0&count=" + view.config.datafetch;
                        } else {
                            url = url + "?start=" + params.start + "&count=" + params.count;
                        }

                        var tlbParams = $$("param");

                        if (tlbParams !== undefined) {
                            var childViews = tlbParams.getChildViews();

                            var parameters = [];
                            var format = webix.Date.dateToStr("%Y-%m-%d");
                            for (var i = 0; i < childViews.length; i++) {
                                var child = childViews[i];
                                // console.log(child);
                                var id = child.config.id;
                                //var value_field = child.config.value_field;
                                switch (id) {
                                    case "filter":
                                        url = url + "&filter=" + $$(id).getValue();
                                        break;
                                    case "tggui_tu":
                                        url = url + "&tggui_tu=" + format($$(id).getValue());
                                        break;
                                    case "tggui_den":
                                        url = url + "&tggui_den=" + format($$(id).getValue());
                                        break;
                                    case "check_error":
                                        url = url + "&check_error=" + $$(id).getValue();
                                        break;
                                }
                            }
                        }
                        webix.ajax(url, callback, view);
                    },
                },
                onClick: {
                    dowloadXmlDKSD: function (event, cell, target) {
                        var win = webix.$$("truyenhanview");
                        var record = win.getItem(cell.row);
                        var file = new Blob([record.xml_thongdiep], { type: "application/xml" });
                        webix.html.download(file, "file.xml");
                    },
                    ShowHistory: function (event, cell, target) {
                        var table = webix.$$("truyenhanview");
                        var win = table.$scope.ui(hoadonHistory.$ui);
                        var record = table.getItem(cell.row);
                        var historyid = record.reg_id;
                        hoadonHistory.setEditMode(historyid);
                        win.show();
                    },
                    /*ShowHistory: function (event, cell, target) {
                        var table = webix.$$("truyenhanview");
                        var record = win.getItem(cell.row);
                        var historyid = record.reg_id;
                        hoadonHistory.setEditMode(historyid);
                        hoadonHistory.show();
                    },*/
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                },
                columns: [
                    { id: "stt", header: "STT", width: 50 },
                    {
                        id: "tg_gui",
                        header: ["<center>Thời gian gửi</center>"],
                        // template: function (obj, common) {
                        //     console.log(obj.tthdon);
                        //     return "<span class='mcls_status " + obj.tthdon + "'>" + obj.ttthdon + "</span>";
                        // },
                        width: 250,
                        format: function (text) {
                            var test = new Date(text);
                            if (test !== null && test !== "null") {
                                var format = webix.Date.dateToStr("%d-%m-%Y %H:%i:%s");
                                text = removeTimeZoneT(test);
                                return format(text);
                            }
                            return "";
                        },
                        columnType: "datetime",
                    },
                    {
                        id: "mngui_ft",
                        header: ["<center>Mã nơi gửi</center>"],
                        width: 120,
                        columnType: "nvarchar",
                    },
                    {
                        id: "btnMessagetn",
                        header: "Lịch sử",
                        template: function (obj) {
                            return "<span class='ShowHistory' style='color: #00b300'><i class='fa fa-eye'></i></span> ";
                        },
                        width: 120,
                    },
                    {
                        id: "mltdiep_gui",
                        header: ["<center>Mã loại thông điệp</center>"],
                        width: 120,
                        columnType: "nvarchar",
                    },
                    {
                        id: "mtdiep_gui",
                        header: ["<center>Mã thông điệp</center>"],
                        width: 300,
                        columnType: "xml",
                    },
                    {
                        id: "mtdiep_thamchieu",
                        header: ["<center>Mã thông điệp tham chiếu</center>"],
                        width: 300,
                        columnType: "xml",
                    },
                    {
                        id: "xml_thongdiep",
                        header: ["<center>Xml thông điệp</center>"],
                        css: { "text-align": "center" },
                        template: function (obj) {
                            return "<span class='dowloadXmlDKSD' style='color: #00b300'><i class='fas fa-download'></i></span> ";
                        },
                        width: 120,
                    },
                    {
                        id: "mnnhan",
                        header: ["<center>Mã nơi nhận</center>"],
                        width: 120,
                        columnType: "nvarchar",
                    },
                    {
                        id: "tg_nhan",
                        header: ["<center>Thời gian nhận</center>"],
                        width: 250,
                        format: function (text) {
                            var test = new Date(text);
                            if (test !== null && test !== "null") {
                                var format = webix.Date.dateToStr("%d-%m-%Y %H:%i:%s");
                                text = removeTimeZoneT(test);
                                return format(text);
                            }
                            return "";
                        },
                        columnType: "nvarchar",
                    },
                    {
                        id: "vendor",
                        header: ["<center>Tên giải pháp</center>"],
                        width: 120,
                        columnType: "nvarchar",
                    },
                    {
                        id: "mngui",
                        header: ["<center>Mã số thuế</center>"],
                        width: 120,
                        columnType: "nvarchar",
                    },
                ],
                pager: "pagerA",
            },
            {
                // cols: [
                // {
                //     view: "icon",
                //     icon: "fas fa-question-circle",
                //     id: "help_form_main",
                //     tooltip: _("HELPER"),
                //     css: "helper",
                //     click: function () {
                //         webix.alert({
                //             title: "Thông báo !",
                //             text: "Bạn chưa khai báo hướng dẫn nhanh cho màn hình này !",
                //             type: "alert-warning",
                //             callback: function (result) {},
                //         });
                //     },
                // },
                // {
                //     view: "combo",
                //     label: _("MENU_QUICK"),
                //     id: "QuickMenuMain",
                //     hide: true,
                //     options: {
                //         // view: "gridsuggest",
                //         // id: "grid_window",
                //         // template: "#name#",
                //         // body: {
                //         //     scroll: true,
                //         //     autoheight: false,
                //         //     columns: [
                //         //         { id: "window_id", header: "Window Id", editor: "text", width: 100 },
                //         //         { id: "name", header: "Window Name", editor: "text", width: 400 }
                //         //     ],
                //         //     url: app.config.host + "/System/GetQuickMenu"
                //         // },
                //         // on: {
                //         //     onValueSuggest: function (obj) {
                //         //         var start = obj.window_id != "" ? obj.code + ":" + obj.window_id : obj.code;
                //         //         window.location.hash = "!/app/" + start;
                //         //         $$("QuickMenu").setValue(null);
                //         //     }
                //         // }
                //     },
                //     on: {
                //         onChange: function (newv, oldv) {},
                //     },
                // },
                // {},
                // {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                // },
                // ],
            },
        ],
    };

    var layout = {
        css: "frmView",
        rows: [
            {
                id: "param",
                view: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                borderless: true,
                height: 35,
                cols: [
                    {
                        id: "filter",
                        view: "text",
                        label: "TK nhanh",
                        width: 350,
                        // value_field: item.value_field,
                        // isDefault: item.has_default,
                    },
                    {
                        id: "tggui_tu",
                        view: "datepicker",
                        label: "Gửi từ",
                        width: 200,
                        // value_field: item.value_field,
                        // isDefault: item.has_default,
                    },
                    {
                        id: "tggui_den",
                        view: "datepicker",
                        label: "đến",
                        width: 200,
                        // value_field: item.value_field,
                        // isDefault: item.has_default,
                    },
                    {
                        id: "check_error",
                        view: "checkbox",
                        label: "TĐ lỗi",
                        width: 120,
                        // value_field: item.value_field,
                        // isDefault: item.has_default,
                    },
                    {
                        view: "button",
                        type: "htmlbutton",
                        width: 100,
                        label: '<span class="text">' + _("Refresh") + "</span>",
                        id: "btnReload",
                        on: {
                            onClick: function () {
                                var table = $$("truyenhanview");
                                table.clearAll();
                                var tlbParams = $$("param");
                                if (tlbParams !== undefined) {
                                    var childViews = tlbParams.getChildViews();
                                    for (var i = 0; i < childViews.length; i++) {
                                        var child = childViews[i];
                                        // console.log(child);
                                        var id = child.config.id;
                                        if (id === "filter" || id === "tggui_tu" || id === "tggui_den" || id === "check_error") {
                                            $$(id).setValue(null);
                                        }
                                        //var value_field = child.config.value_field;
                                    }
                                }
                                table.load(table.config.url);
                            },
                        },
                        click: function () {
                            this.callEvent("onClick");
                        },
                    },
                    {
                        view: "button",
                        type: "htmlbutton",
                        width: 100,
                        label: '<span class="text">' + _("Tìm kiếm") + "</span>",
                        id: "btnTimKiem",
                        on: {
                            onClick: function () {
                                var table = $$("truyenhanview");
                                table.clearAll();
                                table.load(table.config.url);
                            },
                        },
                        click: function () {
                            this.callEvent("onClick");
                        },
                    },
                ],
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
    };
});
