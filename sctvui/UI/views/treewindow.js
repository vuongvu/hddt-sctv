define([
    "app",
    "views/forms/window",
    "locale"
], function (app, windowform, _) {
    'use strict';

    var layoutId = "";
    var col = [];
    var sub_col = [];
    var windowconfig = null;
    var buttons = [];
    //var windowno = "":   


    var initUI = function () {
        var windowno = app.path[1].params[0];

        col = [];
        sub_col = [];

        var query = "SELECT * FROM #SCHEMA_NAME#.wb_menu WHERE window_code='" + windowno + "' LIMIT 1 OFFSET 0";
        var url = app.config.host + "/System/Query";

        webix.ajax()
            .headers({"Content-Type":"application/json"})
            .post(url,{sql : query}, function (text) {
            var json = JSON.parse(text);

            if (json.length > 0) {
                webix.$$("title").parse({ title: json[0]["name"], details: json[0]["details"], url_help: json[0]["url_help"] == null || undefined ? "http://huongdansudung.minvoice.com.vn/" : json[0]["url_help"] });
            }
        });

        url = app.config.host + "/System/ExecuteCommand";
        
        webix.ajax()
            .headers({ 'Content-type': 'application/json' })
            .post(url,
            {
                command: "CM00002",
                parameter: {
                    window_id: windowno
                }
            }, function (text) {
                var json = JSON.parse(text);

                if (buttons.length > 0) {
                    for (var i = 0; i < buttons.length; i++) {
                        var item = buttons[i];
                        $$("tlbMain").removeView(item.id);
                    }
                }

                buttons = [];
                var menus = [];

                var stt = 5;

                for (var i = 0; i < json.length; i++) {
                    var item = json[i];

                    if (item.is_group_function == "C") {
                        if (item.hidden != 1) {
                            menus.push({
                                id: item.name,
                                value: _(item.caption),
                                on_click: item.on_click
                            });
                        }
                    }
                    else {
                        if (item.hidden != 1) {
                            var fn = new Function(item.on_click);

                            var button = {
                                id: item.name,
                                label: _(item.caption),
                                view: "button",
                                type: "icon",
                                css: "webix_secondary",
                                icon: "fas fa-" + item.icon,
                                width: item.width,
                                click: fn
                            };

                            buttons.push(button);
                            $$("tlbMain").addView(button, stt);

                            stt=stt+1;
                        }
                    }

                }

                if (menus.length > 0) {
                    subChucNang.clearAll();
                    subChucNang.parse(menus, "json");

                    var index = $$("tlbMain").index($$("cbMauIn"));

                    var btnChucNang = {
                        id: "btnChucNang",
                        label: _("CHUC_NANG"),
                        view: "button",
                        type: "icon",
                        css: "webix_secondary",
                        icon: "fas fa-tasks",
                        width: 100,
                        click: function () {
                            subChucNang.show(webix.$$("btnChucNang").$view);
                        }
                    }

                    $$("tlbMain").addView(btnChucNang, index - 1);
                }


            });

        url = app.config.host + "/System/GetConfigWinByNo?id=" + windowno;

        webix.$$("treewindowData").getParentView().getParentView().getParentView().disable();
        webix.$$("treewindowData").getParentView().getParentView().getParentView().showProgress();

        webix.ajax(url, function (text, xml, xhr) {

            var rp = JSON.parse(text);
            windowconfig = rp;

            var isPlusIcon = false;

            var k = Object.keys(rp['data'][0]['Tabs']).length;
            //console.log(k);
            for (var j = 0; j < (Object.keys(rp['data'][0]['Tabs'][k - 1]['Fields']).length); j++) {

                var field = rp['data'][0]['Tabs'][k - 1]['Fields'][j];
                //console.log(field);
                if (field.hide_in_grid == true) {
                    continue;
                }

                var item = {
                    id: rp['data'][0]['Tabs'][k - 1]['Fields'][j]['column_name'],
                    header: _(String(rp['data'][0]['Tabs'][k - 1]['Fields'][j]['column_name'])),
                    formatColumn: field.format
                };

                if (field.caption != null) {
                    item.header = _(field.caption);
                }

                if (field.type_filter != null) {
                    if (field.type_filter != "") {
                        item.header = [_(item.header), { content: field.type_filter }]
                    }
                }

                if (isPlusIcon == false) {
                    item.template = "{common.space()} {common.icon()} #" + item.id + "#";
                    isPlusIcon = true;
                }

                if (field.FORMAT != null) {
                    if (field.column_type == "decimal") {
                        item.css = { "text-align": "right" };
                        item.format = webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0
                        });
                    }

                    if (field.column_type == "datetime" || field.column_type == "date") {
                        item.format = function (text) {
                            var format = webix.Date.dateToStr(this.formatColumn);
                            text = removeTimeZoneT(text);
                            return format(text);
                        };
                    }
                }

                if (field.type_editor == "combo") {
                    item.editor = field.type_editor;
                    item.options = app.config.host + "/System/GetDataByReferencesId?id=" + field.ref_id;
                }

                if (field.column_width == null) {
                    item.fillspace = true;
                }
                else {
                    item.width = field.column_width;
                }

                col.push(item);
            }

            var table = $$("treewindowData");
            table.clearAll();

            table.config.columns = col;
            table.refreshColumns();

            //table.define("url",app.config.host + "/api/System/GetDataByWindowNo?window_id="+windowno );	

            webix.ajax()
                .headers({ "Content-Type": "application/json" })
                .post(app.config.host + "/System/GetDataByWindowNo",
                {
                    window_id: windowno,
                    start: 0,
                    count: 999999999,
                    continue: null,
                    filter: [],
                    infoparam: null,
                    tlbparam: []
                }, function (text) {
                    var json = JSON.parse(text);
                    //console.log(json);
                    var data = listToTree(json.data, { idKey: "id", parentId: "parent_id" });
                    //console.log(data);
                    $$("treewindowData").parse(data, "json");
                    $$("treewindowData").openAll();
                });

            table.getParentView().getParentView().getParentView().enable();
            table.getParentView().getParentView().getParentView().hideProgress();

        });
    };


    var controls = [
        {
            id: "btnRefresh",
            view: "button",
            type: "icon",
            icon: "fas fa-refresh",
            label: _("REFRESH"),
            css: "webix_secondary",
            width: 95,
            // shortcut: "F4",
            //css:"button_raised",
            on: {
                onClick: function () {
                    loadData();
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: "Thêm",
             css: "webix_secondary",
            width: 65,
            //css:"button_raised",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var vc_window = windowconfig["data"][0];
                    windowform.$ui.id = vc_window.code;

                    var detailForm = this.$scope.ui(windowform.$ui);
                    windowform.setEditMode(0);
                    windowform.initUI(windowconfig);
                    
                    windowform.setValues(null, windowconfig);
                    windowform.setEditMode(1);
                    
                    detailForm.show();

                    if (vc_window.after_new != null) {
                        var fn = Function(vc_window.after_new);
                        fn();
                    }
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
             css: "webix_secondary",
            label: "Sửa",
            width: 55,
            shortcut: "F3",
            on: {
                onClick: function () {
                    var vc_window = windowconfig["data"][0];


                    windowform.$ui.id = vc_window.code;

                    var detailForm = this.$scope.ui(windowform.$ui);

                    var table = webix.$$("treewindowData");
                    var item = table.getSelectedItem();
                    if (item == null || item == "") {
                        webix.message("Bạn chưa chọn dòng cần sửa ! ", "error");
                        return;
                    }
                    windowform.initUI(windowconfig);
                    windowform.setEditMode(2);
                    windowform.setValues(item, windowconfig);

                    detailForm.show();
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
             css: "webix_secondary",
            label: "Xóa",
            width: 55,
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("treewindowData");
                    var item = table.getSelectedItem();

                    if (item.$count > 0) {
                        webix.message("Đã có nhóm con. Không thể xóa");
                        return;
                    }

                    var khData = {};
                    khData.windowid = windowconfig.data[0].code;
                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(item);

                    webix.confirm({
                        text: "Bạn có muốn xóa ", ok: "Có", cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix.ajax()
                                    .headers({ 'Content-type': 'application/json' })
                                    .post(app.config.host + "/System/Save", khData, function (text) {
                                        var json = JSON.parse(text);
                                        //console.log(json);
                                        if (json.hasOwnProperty("error")) {
                                            webix.message(json.error);
                                            return;
                                        }

                                        table.remove(item.id);

                                    });

                            }
                        }
                    });
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnCopy",
            view: "button",
            type: "icon",
            icon: "fas fa-files-o",
             css: "webix_secondary",
            label: _("COPY"),
            width: 85,
            on: {
                onClick: function () {
                    var table = webix.$$("treewindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn dòng dữ liệu !", "error");
                        return;
                    }

                    var vc_window = windowconfig["data"][0];
                    windowform.$ui.id = vc_window.code;

                    var detailForm = this.$scope.ui(windowform.$ui);

                    windowform.initUI(windowconfig);
                    windowform.setEditMode(1);
                    windowform.copyValues(item, windowconfig);

                    detailForm.show();

                    if (vc_window.after_new != null) {
                        var fn = Function(vc_window.after_new);
                        fn();
                    }
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {}
    ]

    var grid = {
        rows: [
            {
                id: "treewindowData",
                view: "treetable",
                select: true,
                resizeColumn: true,
                scheme: {
                    $change: function (item) {
                        if (item.$count > 1980)
                            item.$css = "highlight";
                    }
                },
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    }
                },
                //url: app.config.host + "/api/System/GetDataByWindowNo?window_id="+windowno ,
                columns: col,
                filterMode:{
                    level:false,
                    showSubItems:false
                }
            }
        ]
    }

    var layout = {
        rows: [
            {
                id: "tlbMain",
                height: 35,
                type: "toolbar",
                cols: controls
            },
            {
                rows: [
                    grid
                ]
            }



        ]

    };

    function loadData() {
        var windowno = app.path[1].params[0];
        var table = webix.$$("treewindowData");
        table.clearAll();

        webix.ajax()
            .headers({ "Content-Type": "application/json" })
            .post(app.config.host + "/System/GetDataByWindowNo",
            {
                window_id: windowno,
                start: 0,
                count: 999999999,
                continue: null,
                filter: [],
                infoparam: null,
                tlbparam: []
            }, function (text) {
                var json = JSON.parse(text);
                var data = listToTree(json.data, { idKey: "id", parentId: "parent_id" });
                $$("treewindowData").parse(data, "json");
                $$("treewindowData").openAll();
            });
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {

            webix.extend(view, webix.ProgressBar);

            var windowno = app.path[1].params[0];

            $scope.on(app, "dataChangedLicense", function (mode, item) {
               
                var table = webix.$$("treewindowDataRegister");

                if (mode == 1) {
                    table.clearAll();
                    table.load(table.config.url);
                }

                if (mode == 2) {
                    table.clearAll();
                    table.load(table.config.url);
                }
            });

            $scope.on(app, "dataChanged", function (mode, item) {

                var table = webix.$$("treewindowData");
                var url = app.config.host + "/System/GetDataById?window_id=" + windowno + "&id=" + item.id;

                webix.ajax(url, function (text) {
                    var json = JSON.parse(text);

                    if (mode == 1) {
                        table.data.add(json, 0, json.parent_id);
                        table.select(json.id);
                    }

                    if (mode == 2) {
                        var selectedItem = table.getSelectedItem();
                        //console.log(json.data[0]);
                        if (selectedItem.parent_id != json.parent_id) {

                            var nodeId = table.getSelectedId();
                            table.remove(nodeId);

                            table.clearSelection();

                            table.data.add(json, 0, json.parent_id);
                            table.select(json.id);
                        }
                        else {
                            table.updateItem(selectedItem.id, json);
                        }


                    }


                });



            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        }
    };
});