define(["app", "views/forms/window"], function (app, dmhvform) {
    "use strict";

    //var hvno = app.path[1].params[0];
    var col = [];
    var url = app.config.host + "/api/System/GetAllByWindowId?id=09a8b24b-52b5-46a6-931d-d6f005da0dd7";
    webix.ajax(url, function (text, xml, xhr) {
        var rp = JSON.parse(text);
        //console.log(Object.keys(rp).length);

        for (var j = 0; j < Object.keys(rp["data"][0]["Tabs"][0]["Fields"]).length; j++) {
            col.push({ header: "" + String(rp["data"][0]["Tabs"][0]["Fields"][j]["COLUMN_NAME"]) + "", fillspace: true });
        }
        $$("windowData").define("columns", col);
        $$("windowData").refresh();

        //   console.log(col);
        //   console.log(Object.keys(rp.data).length);
    });

    var controls = [
        {
            id: "btnPlus",
            view: "button",
            type: "iconButton",
            icon: "plus",
            label: "Thêm",
            width: 110,
            css: "button_raised",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var win = this.$scope.ui(dmhvform.$ui);
                    dmhvform.setEditMode(1);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "iconButton",
            icon: "edit",
            label: "Sửa",
            width: 100,
            css: "button_raised",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmhvform.$ui);
                    var form = webix.$$("window-form");

                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    form.setValues(item);

                    dmhvform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "iconButton",
            icon: "trash-o",
            label: "Xóa",
            width: 100,
            css: "button_raised",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("windowData");
                    var item = table.getSelectedItem();

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.ten_hv,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix.ajax().post(app.config.host + "/api/Dmhv/Delete", item, function (text, xml, xhr) {
                                    if (text.hasOwnProperty("error")) {
                                        webix.message(text.error);
                                        return;
                                    }

                                    table.remove(item.id);
                                });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        margin: 10,
        rows: [
            {
                id: "dmhvData",
                view: "datatable",
                select: true,
                datafetch: 2,
                loadahead: 15,
                // url: {
                //     $proxy:true,
                //     load:function(view, callback, params){
                //             var url = app.config.host+ "/api/dmhv/All";

                //             webix.ajax(url, callback, view);
                //     }
                // },
                columns: col,
                //  {id: "ma_hv", header: "Mã hàng" , width: 100 },
                //  {id: "ten_hv", header: "Tên hàng" , fillspace: true },
                // {id: "dvt", header: "Đơn vị tính" , fillspace: true }
                // {id: "quai", header: "Quai", width:80},
                // {id: "mau", header: "Màu sắc", width:80},
                // {id: "doday", header: "Độ dày", width:80},
                // {
                //     id: "gia_ban", header: "Giá bán", width:80 ,
                //     format:webix.Number.numToStr({
                //             groupDelimiter:",",
                //             groupSize:3,
                //             decimalDelimiter:".",
                //             decimalSize:0
                //         }),
                //     css:{'text-align':'right'}
                // }
                //  ],
                //columns: columns
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        type: "space",
        rows: [
            {
                height: 40,
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmhvData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmhv,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
