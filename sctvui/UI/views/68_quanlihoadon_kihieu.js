define(["app", "locale", "models/user", "views/forms/68_kyhieu_hoadon"], function (app, _, user, kyhieuForm) {
    "use strict";

    webix.$$("title").parse({
        title: "Quản lý số lượng HĐ cho từng ký hiệu HĐ",
        details: "Quản lý số lượng HĐ cho từng ký hiệu HĐ",
    });

    var initUI = function () {
        // var windowno = app.path[1].params[0];
        var table = webix.$$("kyhieu_hoadon");
        // table.disable();
        // table.showProgress();
        // table.clearAll();
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var url = app.config.host + "/Invoice68/GetHoaDonkyhieu68";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;
                    var array =[];
                    if (params.filter != null) {
                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                    }
                    url = url + "&filter=" + JSON.stringify(array);
                }
                var arrayParam = [];
                var branchcode = {
                    columnName: "branch_code",
                    value: user.getDvcsID(),
                };
                arrayParam.push(branchcode);
                url = url + "&tlbparam=" + JSON.stringify(arrayParam);
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu_hoadon");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(kyhieuForm.$ui);
                    kyhieuForm.setEditMode(1);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu_hoadon");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn! ", "error");
                        return;
                    }
                    var detailForm = this.$scope.ui(kyhieuForm.$ui);

                    kyhieuForm.setEditMode(2);
                    kyhieuForm.setValues(selectedItem);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu_hoadon");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn ký hiệu hóa đơn cần xóa ! ", "error");
                        return;
                    }
                    if (selectedItem.sosudung > 0) {
                        webix.message("Ký hiệu đã sử dụng không thể xóa ! ", "error");
                        return;
                    }
                    selectedItem.editmode = 3;
                    webix.confirm({
                        text: "Bạn có muốn xóa bản ghi",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/hoadonkihieu68", selectedItem, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.error, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            var res = JSON.parse(text);
                                            if (res.hasOwnProperty("error")) {
                                                webix.message(res.error, "error");
                                            } else {
                                                table.remove(selectedItem.id);
                                                table.enable();
                                                table.hideProgress();
                                            }
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "kyhieu_hoadon",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function (obj) {
                        // if (obj.ngayLap) {
                        //     obj.ngayLap = new Date(obj.ngayLap);
                        // }
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.stt = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onBeforeLoad: function () {
                        this.showOverlay("<p><i style='font-size:450%;' class='fa fa-spin fa-spinner fa-pulse'></i></p>");
                    },
                    onAfterLoad: function (id) {
                        this.hideOverlay();
                    },
                },
                columns: [
                    {
                        id: "id",
                        header: "kyhieu_id",
                        hidden: true,
                    },
                    {
                        id: "kyhieu_id",
                        header: "kyhieu_id",
                        hidden: true,
                    },
                    {
                        id: "stt",
                        header: "STT",
                        sort: "int",
                        width: 40,
                        columnType: "numeric",
                    },
                    {
                        id: "khhdon",
                        header: [
                            "Kí hiệu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 240,
                    },
                    {
                        id: "tongso",
                        header: [
                            "Tổng số",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                    },
                    {
                        id: "sosudung",
                        header: [
                            "Số sử dụng",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "ngaybatdau",
                        format: function (text) {
                            var test = new Date(text);
                            if (test !== null && test !== "null") {
                                var format = webix.Date.dateToStr("%d-%m-%Y %H:%i:%s");
                                text = removeTimeZoneT(test);
                                return format(text);
                            }
                            return "";
                        },
                        width: 240,
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "branch_code",
                        header: [
                            "Đơn vị tạo",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                        css: {
                            "text-align": "center",
                        },
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                // height: 40,
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };
    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var table = webix.$$("kyhieu");
            webix.extend(view, webix.ProgressBar);

            $scope.on(app, "dataChangedKyhieu68", function (mode, item) {
                table.clearAll();
                table.load(table.config.url);
            });

            $scope.on(app, "filterTable", function (report_id, infoWindowId, item) {
                var infoparam = {
                    infowindow_id: infoWindowId,
                    parameter: item,
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
