define(["app"], function (app) {

    return {
        $ui: {
            "type": "clean",
            "cols": [
                {
                    rows: [
                        {
                            "template": "<span class='webix_icon fa-chart-bar' style='color:#FF892A;'></span><b style='color:blue'> Doanh thu ( triệu đồng )</b>",
                            "css": "sub_title",
                            "height": 30,
                            borderless: true
                            // "paddingX": 10,
                            // "paddingY": 10,
                            // "margin-left": 10
                        },
                        {
                            view: "chart",
                            type: "line",
                            barWidth: 165,
                            radius: 0,
                            resize: true,
                            gradient: "falling",
                            // value: "#hd01#",a
                            // label: "#hd01#",
                            preset: "column",
                            //border:false,
                            borderless: true,
                            xAxis: {
                                title: "Tháng",
                                lines: false,
                                template: "<span style = 'font-size:11px'> #thang#</span>"
                            },
                            yAxis: {
                                //  title: "Years",
                                //  lines: true,
                                start: 0,
                                step: 500,
                                end: 5000,
                                template: function (obj) {
                                    return (obj % 500 ? "" : obj)
                                }
                            },
                            legend: {
                                values: [{ text: "01GTKT", color: "#4bbaa4" }, { text: "02GTTT", color: "#ee9e36" }, { text: "03XKNB", color: "#36abee" }, { text: "07KPTQ", color: "#ee3639" }, { text: "01BLP", color: "#914ad8" }],
                                valign: "middle",
                                align: "right",
                                width: 90,
                                layout: "y",
                                marker: {
                                    type: "round",
                                    // width:15,
                                    radius: 3
                                }
                            },
                            eventRadius: 5,
                            series: [
                                {
                                    preset: "column",
                                    barWidth: 65,
                                    alpha: 1,
                                    value: "#hd01#",
                                    color: "#4bbaa4",
                                    item: {
                                        // radius: 3,
                                        // type: "s",
                                        borderColor: "#1777ef",
                                        color: "#ffffff"
                                    },
                                    line: {
                                        color: "#4bbaa4",
                                        width: 2
                                    },
                                    tooltip: {
                                        template: "Type: 01GTKT<br/>Value: #hd01#"
                                    }
                                },
                                {
                                    alpha: 0.5,
                                    value: "#hd02#",
                                    color: "#ee9e36",
                                    item: {
                                        borderColor: "#1777ef",
                                        color: "#ffffff"
                                    },
                                    line: {
                                        color: "#ee9e36",
                                        width: 2
                                    },
                                    tooltip: {
                                        template: "Type: 02GTTT<br/>Value: #hd02#"
                                    }
                                },
                                {
                                    alpha: 0.5,
                                    value: "#hd03#",
                                    color: "#36abee",
                                    item: {
                                        borderColor: "#1777ef",
                                        color: "#ffffff"
                                    },
                                    line: {
                                        color: "#36abee",
                                        width: 2
                                    },
                                    tooltip: {
                                        template: "Type: 03XKNB<br/>Value: #hd03#"
                                    }
                                },
                                {
                                    alpha: 0.5,
                                    value: "#hd07#",
                                    color: "#ee3639",
                                    item: {
                                        borderColor: "#1777ef",
                                        color: "#ffffff"
                                    },
                                    line: {
                                        color: "#ee3639",
                                        width: 2
                                    },
                                    tooltip: {
                                        template: "Type: 07KPTQ<br/>Value: #hd07#"
                                    }
                                },
                                {
                                    alpha: 0.5,
                                    value: "#bienlai#",
                                    color: "#914ad8",
                                    item: {
                                        borderColor: "#1777ef",
                                        color: "#ffffff"
                                    },
                                    line: {
                                        color: "#914ad8",
                                        width: 2
                                    },
                                    tooltip: {
                                        template: "Type: 01BLP<br/>Value: #bienlai#"
                                    }
                                }
                            ],
                            url: app.config.host + "/Invoice/bar_chart"
                        }
                        // {
                        //     view: "chart",
                        //     type: "bar",
                        //     barWidth: 60,
                        //    // preset:"column",
                        //     radius: 2,
                        //     gradient: "rising",
                        //     xAxis: {
                        //         template: "'#thang#"
                        //     },
                        //     yAxis: {
                        //         start: 0,
                        //         step: 100,
                        //         end: 1000
                        //     },
                        //     legend: {
                        //         values: [{ text: "01GTKT", color: "#58dccd" }, { text: "02GTTT", color: "#a7ee70" }, { text: "03XKNB", color: "#36abee" }, { text: "07KPTQ", color: "red" }, { text: "01BLP", color: "blue" }],
                        //         valign: "middle",
                        //         align: "right",
                        //         width: 90,
                        //         layout: "y"
                        //     },
                        //     series: [
                        //         {
                        //             value: "#hd01#",
                        //             color: "#58dccd",
                        //             tooltip: {
                        //                 template: "#hd01#"
                        //             }
                        //         },
                        //         {
                        //             value: "#hd02#",
                        //             color: "#a7ee70",
                        //             tooltip: {
                        //                 template: "#hd02#"
                        //             }
                        //         },
                        //         {
                        //             value: "#hd03#",
                        //             color: "#36abee",
                        //             tooltip: {
                        //                 template: "#hd03#"
                        //             }
                        //         },
                        //         {
                        //             value: "#hd07#",
                        //             color: "#36abee",
                        //             tooltip: {
                        //                 template: "#hd07#"
                        //             }
                        //         },
                        //         {
                        //             value: "#bienlai#",
                        //             color: "#36abee",
                        //             tooltip: {
                        //                 template: "#bienlai#"
                        //             }
                        //         }
                        //     ],
                        //     url: app.config.host + "/Invoice/bar_chart",
                        //     // data: multiple_dataset
                        // },
                    ]
                }
                // {
                //     rows:
                //     [
                //         {
                //             "template": "<span class='webix_icon fa-line-chart' style='color:#FF892A;margin-left:20px'></span><b style='color:#669FC7'> Biểu đồ loại hóa đơn</b>",
                //             "css": "sub_title",
                //             "height": 30,
                //             "paddingX": 0,
                //             "paddingY": 10,
                //             "margin": 0
                //         },
                //         {
                //             view: "chart",
                //             type: "pie",
                //             value: "#soluong#",
                //             color: "#color#",
                //             //label: "#loai#",
                //             borderless: true,
                //             pieInnerText: "#soluong#",
                //             shadow: 0,
                //             //data: month_dataset,
                //             url: app.config.host + "/Invoice/bar_chart2",
                //             legend: {
                //                 // width: 75,
                //                 align: "right",
                //                 valign: "middle",
                //                 template: "#loai#"
                //             }
                //         }
                //     ]

                // }

            ]
        }
    };

});