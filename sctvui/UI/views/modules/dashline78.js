define(["app"], function (app) {
    return {
        $ui: {
            height: 136,
            CSS: "titles",
            borderless: true,
            template: function (data) {
                var t = null;

                if (data.items == undefined) return;
                debugger;
                var items = data.items;
                var html = "<div class='f-grid'>";
                html += "<h2 style= ' padding-left: 20px; margin: inherit '>Hóa đơn 78</h2>";

                for (var i = 0; i < items.length; i++) {
                    t = items[i];

                    var tien = webix.Number.format(t.tien, {
                        groupDelimiter: ",",
                        groupSize: 3,
                        decimalDelimiter: ".",
                        decimalSize: 0,
                    });

                    var code = "duyethoadon";
                    var params = "0" + t.p_id;

                    if (t.p_id == "04") {
                        code = "reportwindow";
                        params = "WIN00022:d13001df-01fe-4d4f-ab65-cc98e7b41661";
                    }
                    html += "<div class='f-grid-col'>";
                    html +=
                        "<i class='fad fa-boxes fa-4x' style=' padding-top: 25px;margin-right: 10px;--fa-primary-color: #b5b8ba; --fa-secondary-color: #dadee0;' fa-swap-opacity fa-5x></i><div><p style='font-size:18px;'>" +
                        t.dien_giai +
                        "</p>";
                    html +=
                        "<p if(" +
                        t.quyen +
                        " ==0 ) { webix.message('Bạn chưa được phân quyền !','error'); return ;} menu.$scope.show('./" +
                        code +
                        ":" +
                        params +
                        "');\" style='color:#005EB2;font-weight:bold;font-size:20px;text-decoration: none;'>" +
                        tien +
                        "</p></div>";
                    html += "</div>";
                }
                html += "</div>";
                return html;
            },
            url: app.config.host + "/Invoice68/DashboardTiles78",
        },
    };
});
