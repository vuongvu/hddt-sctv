define(["app"],function(app){

	return {
		$ui:{
			"type": "clean",
			"rows":[
				{
					"template": "<span class='webix_icon fa-star' style='color:#FF892A'></span><b style='color:#669FC7'> Danh sách mẫu hóa đơn sử dụng</b>", "css": "sub_title", "height": 30
				},
				{
                    "view":"datatable",
                    "css" : "mcls_invoiceDashboard",
                    "url": app.config.host + "/Invoice/DashboardInvoices",
                    "columns": [
                        // {
                        //     id: "trang_thai",
                        //     header:"Trạng thái",
                        //     width:120,
                        //     template:"<span class='mcls_status #className#'>#trang_thai#</span>"
                        // },
                        {
                            id: "mau_so",
                            header:"Mẫu hóa đơn",
                            template: "<a href='#' onclick=\"var menu=$$('app:menu');menu.$scope.show('./window:#win#');\" >#mau_so#</a>",
                            fillspace:1
                        },
                        {
                            id: "ky_hieu",
                            header:"Ký hiệu",
                            fillspace:1
                        },
                        {
                            id: "ngay_bd_sd",
                            header:"Ngày sử dụng",
                            fillspace:1,
                            format: function(text) {
                                var format = webix.Date.dateToStr("%d/%m/%Y");
                                text = removeTimeZoneT(text);                            
                                return format(text);
                            }
                        },
                        {
                            id: "so_luong",
                            header:"Số lượng",
                            width:120,
                            css: {"text-align": "right","color":"blue", "font-weight": "bold"},
                            format: webix.Number.numToStr({
                                groupDelimiter:",",
                                groupSize:3,
                                decimalDelimiter:".",
                                decimalSize:0
                            })
                        },
                        {
                            id: "sl_xuat",
                            header:"Đã xuất",
                            width:120,
                            css: {"text-align": "right","color":"red","font-weight": "bold"},
                            format: webix.Number.numToStr({
                                groupDelimiter:",",
                                groupSize:3,
                                decimalDelimiter:".",
                                decimalSize:0
                            })
                        },
                        {
                            id: "con_lai",
                            header:"Còn lại",
                            width:120,
                            css: {"text-align": "right","font-weight": "bold","color":"green"},
                            format: webix.Number.numToStr({
                                groupDelimiter:",",
                                groupSize:3,
                                decimalDelimiter:".",
                                decimalSize:0
                            })
                        },
                        {
                            id: "trung_binh",
                            header:"Trung bình 1 ngày",
                            width:120,
                            css: {"text-align": "right","font-weight": "bold"},
                            format: webix.Number.numToStr({
                                groupDelimiter:",",
                                groupSize:3,
                                decimalDelimiter:".",
                                decimalSize:0
                            })
                        }
                    ]
                }
			]
		}
	};

});