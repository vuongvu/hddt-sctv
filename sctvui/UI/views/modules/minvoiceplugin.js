
define(["app", "models/user"], function (app, user) {

    var isConnected = false;
    var isWindowXP = function () {
        return navigator.userAgent.indexOf("Windows NT 5.1") != -1;
    }
    var isLocked = false;
    if (!isWindowXP()) {
        var url = "http://localhost:19898/signalr";

        var conn = $.hubConnection(url, {
            useDefaultPath: false
        });
					
        var hub = conn.createHubProxy('invoiceHub');
    
        hub.on('resCommand', function (result) {
        
        });

        conn.disconnected(function () {
            console.log('Connection disconnected');
            isConnected = false;
            isLocked = false;
        });

        conn.start().done(function () {
            console.log("Connected to Plugin Id: " + hub.connection.id);
            isConnected = true;
            isLocked = false;
        })
            .fail(function () {
                isConnected = false;
                var userApp = user.getAppCookie();
                var url = app.config.host + "/System/ExecuteCommand";
                webix.ajax()
                    .headers({ 'Content-type': 'application/json' })
                    .post(url,
                        {
                            command: "CM00005",
                            parameter: {
                                wb_user_id: userApp.wb_user_id
                            }
                        }
                        , function (text) {
                            var json = JSON.parse(text);
                            if (json.length > 0 && json.filter(function (x) { return x.chon1 == 'C' }).length > 0)
                                webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "debug");
                        });
            });
        return {
            hub: function () {
                return hub;
            },
            conn: function () {
                return conn;
            },
            isConnected: isConnected,
            setLocked: function (locked) {
                isLocked = locked;
            },
            getLocked: function () {
                return isLocked;
            }
        };
   
   } else {
        var url = "ws://localhost:19898/service";
        var hub = {};
        window.fn = undefined;
        window.state = undefined;
        hub.ws = undefined;
          
        hub.conn = function () {
            window.state = 1;
            hub.ws = new WebSocket(url);
            // hub.ws.onopen = hub.onOpen;
            hub.ws.onclose = hub.onClose;
            //  hub.ws.onmessage = hub.on;
            hub.ws.onerror = hub.onError;
        }
        var conn = function () {
            return { state: window.state };
        }
        hub.off = function (_) {
            
        }
        hub.invoke = function (_, str2, json) {
            if (hub.ws != undefined) {
                hub.ws.onopen = function () {
                    var obj = {
                        execCommand: str2,
                        arg : JSON.parse(json)
                    }
                    if (hub.ws.readyState == WebSocket.OPEN ){
                        hub.ws.send(JSON.stringify(obj));
                    } 
                }
               
            } else {
                console.log('Websocket is not initialized');
            }
            return hub;
        };
        hub.onError = function (arg) {
            window.state = -1;
            if (hub.ws != undefined) {
                isConnected = false;
                var userApp = user.getAppCookie();
                var url = app.config.host + "/System/ExecuteCommand";
                webix.ajax()
                    .headers({ 'Content-type': 'application/json' })
                    .post(url,
                        {
                            command: "CM00005",
                            parameter: {
                                wb_user_id: userApp.wb_user_id
                            }
                        }
                        , function (text) {
                            console.log(1);
                            var json = JSON.parse(text);
                            if (json.length > 0 && json.filter(function (x) { return x.chon1 == 'C' }).length > 0)
                                webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "debug");
                    });
            } else {
                console.log('Websocket is not initialized');
            }
        }
        hub.on = function (str, fn_in) {
            if (hub.ws != undefined) {
                window.fn = fn_in;
                hub.ws.onmessage = func;
            } else {
                console.log('Websocket is not initialized');
            }
        }
        var func = function (result) {
            return window.fn(result.data);
        }
        hub.onClose = function (arg) {
            if (hub.ws != undefined) {
                console.log('DISCONNECTED');
                isConnected = false;
                isLocked = false;
            } else {
                console.log('Websocket is not initialized');
            }
        };
    
        hub.onOpen = function (arg) {
            if(hub.ws != undefined){
                isConnected = true;
                isLocked = false;
                window.state = hub.ws.readyState;
            } else {
                isConnected = false;
                isLocked = true;
                console.log('Websocket is not initialized');
            }
        };
        hub.done = function(fn) {
            fn();
            return hub;
        }
        hub.fail = function(fn) {
            return hub;
        }
           
        return {
            hub: function () {
                hub.conn();  
                
                return hub;
            }, 
            conn: function () {
                return conn();
            },
            isConnected: isConnected,
            setLocked: function(locked){
                isLocked = locked;
            },
            getLocked: function(){
                return isLocked;
            }
        };
    }
});