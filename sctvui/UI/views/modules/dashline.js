define(["app"], function (app) {

	return {
		$ui: {
			height: 136,
			css: "tiles",
			borderless:true,
			template: function (data) {
				var t = null;

				if (data.items == undefined) return;

				var items = data.items;
				var html = '<div class="f-grid">'
					+ '<div class="f-grid-col">'
					+ 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, nulla! Assumenda dignissimos harum, molestias iure repudiandae ratione praesentium. Illo facilis et velit ut quam omnis, porro molestiae magni, laboriosam ipsa.'
					+ '</div>'
					+ '<div class="f-grid-col">'
					+ 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, quaerat blanditiis. Eligendi quae tenetur ratione repellendus ut.</div>'
					+ '<div class="f-grid-col">'
					+ 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, nulla! Assumenda dignissimos harum, molestias iure repudiandae ratione praesentium. Illo facilis et velit ut quam omnis, porro molestiae magni, laboriosam ipsa.'
					+ '</div>'
					+ '</div>'
				var html = "<div class='f-grid'>";
				html += "<h2 style= ' padding-left: 20px; margin: inherit '>Hóa đơn 32</h2>" ;

				for (var i = 0; i < items.length; i++) {
					t = items[i];

					var tien = webix.Number.format(t.tien, {
						groupDelimiter: ",",
						groupSize: 3,
						decimalDelimiter: ".",
						decimalSize: 0
					});

					var code = "duyethoadon";
					var params = "0" + t.p_id;

					if (t.p_id == "04") {
						code = "reportwindow";
						params = "WIN00022:d13001df-01fe-4d4f-ab65-cc98e7b41661";
					}

					html += "<div class='f-grid-col'>";
					// html += "<img style='padding-top:12px;' src='./assets/imgs/icon/dieukhien.png' height='80' width='80' ><div><p style='font-size:18px;'>"+t.dien_giai+"</p>"
					html += "<i class='fad fa-boxes fa-4x' style=' padding-top: 25px;margin-right: 10px;--fa-primary-color: #b5b8ba; --fa-secondary-color: #dadee0;' fa-swap-opacity fa-5x></i><div><p style='font-size:18px;'>"+t.dien_giai+"</p>"
					html += "<a href='#' onclick=\"var menu=$$('app:menu'); if(" + t.quyen + " ==0 ) { webix.message('Bạn chưa được phân quyền !','error'); return ;} menu.$scope.show('./" + code + ":" + params + "');\" style='color:#005EB2;font-weight:bold;font-size:20px;text-decoration: none;'>"+tien+"</a></div>"
					// html += "<div class='webix_icon icon fa-" + t.icon + "'></div>";
					// html += "<div class='details'><div class='value'>" + tien + "</div><div class='text'>" + t.dien_giai + "</div></div>";
				    // html += "<a href='#' onclick=\"var menu=$$('app:menu'); if(" + t.quyen + " ==0 ) { webix.message('Bạn chưa được phân quyền !','error'); return ;} menu.$scope.show('./" + code + ":" + params + "');\" style='color:white;text-decoration: none;'>"+tien+"</a></div>";
					html += "</div>";
				}
				html += "</div>";
				return html;
			},
			url: app.config.host + "/Invoice/DashboardTiles"
		}

	};

});