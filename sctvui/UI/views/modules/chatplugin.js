define([
    'app',
    'models/user'
], function (app, user) {
    'use strict';
    var hub = null;
    // var ChatUrl = "http://localhost:52527/signalr";
    // //This will hold the connection to the signalr hub   
    // var SignalrConnection = $.hubConnection(ChatUrl, {
    //     useDefaultPath: false
    // });
    // var hub = SignalrConnection.createHubProxy('chatHub');
    // //connecting the client to the signalr hub   
    // hub.on("addNewMessageToPage", function (name, message) {
    //     console.log(name + message);
    // });

    // SignalrConnection.disconnected(function () {
    //     console.log('Connection disconnected');
    // });
    // SignalrConnection.start().done(function () {
    //     console.log("Connected to Signalr Server");
    // })
    //     .fail(function () {
    //         console.log("failed in connecting to the signalr server");
    //     });


    return {
        hub: function () {
            return hub;
        }
    }
});