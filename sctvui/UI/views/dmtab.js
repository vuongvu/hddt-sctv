define(["app", "views/forms/dmtab"], function (app, dmtabform) {
    "use strict";

    //var windowno = app.path[1].params[0];
    // webix.$$("title").parse({ title: "Danh mục Tab", details: "Danh mục Tab system" });
    webix.$$("title").parse({ title: "Điều khiển > Danh mục Tab", details: "Danh mục Tab system" });

    var controls = [
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: "Thêm (F4)",
            css: "webix_secondary",
            width: 95,
            shortcut: "F4",
            on: {
                onClick: function () {
                    var win = this.$scope.ui(dmtabform.$ui);
                    var form = webix.$$("dmtab-form");

                    if (webix.$$("txtWindowId").getValue() == "") {
                        webix.message("Bạn chưa chọn Window Id");
                        return;
                    }

                    var item = {
                        wb_window_id: $$("txtWindowId").getValue(),
                    };

                    form.setValues(item);

                    dmtabform.setEditMode(1);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            label: "Sửa (F3)",
            width: 85,
            css: "webix_secondary",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmtabform.$ui);
                    var form = webix.$$("dmtab-form");

                    var table = webix.$$("dmtabData");
                    var item = table.getSelectedItem();

                    form.setValues(item);

                    dmtabform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            label: "Xóa (F8)",
            css: "webix_secondary",
            width: 85,
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmtabData");
                    var item = table.getSelectedItem();

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.tab_name,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix.ajax().post(app.config.host + "/WbTab/Delete", item, function (text, xml, xhr) {
                                    if (text.hasOwnProperty("error")) {
                                        webix.message(text.error);
                                        return;
                                    }

                                    table.remove(item.id);
                                });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnCopy",
            view: "button",
            type: "icon",
            icon: "fas fa-files-o",
            label: "Sao chép",
            css: "webix_secondary",
            width: 85,
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmtabform.$ui);
                    var form = webix.$$("dmtab-form");

                    var table = webix.$$("dmtabData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message({ type: "error", text: "Bạn chưa chọn mục tab" });
                        return;
                    }

                    item.id = null;
                    form.setValues(item);

                    dmtabform.setEditMode(1);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
        {
            view: "combo",
            width: 450,
            label: "Window ID",
            id: "txtWindowId",
            options: {
                view: "gridsuggest",
                id: "grid_window",
                // filter: function (obj, value) {
                //     var string = "";
                //     if (obj) {
                //         string += obj.id + " ";
                //         string += obj.window_name + " ";

                //     }
                //     return string.toLowerCase().indexOf(value.toLowerCase()) >= 0;
                // },
                // template: function (item) {
                //     return item.name + " ";
                //},
                template: "#name#",
                width: 550,
                body: {
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "code", header: "Window Id", editor: "text", width: 100 },
                        { id: "name", header: "Window Name", editor: "text", width: 400 },
                    ],
                    url: app.config.host + "/WbWindow/All",
                },
            },
            // suggest: {
            // 	url: app.config.host + "/WbWindow/All",
            // 	template: "#window_name#",
            // 	body: {
            // 		template: "#window_name#"
            // 	}
            // },
            on: {
                onChange: function () {
                    loadData();
                },
            },
        },
    ];

    var grid = {
        rows: [
            {
                id: "dmtabData",
                view: "datatable",
                select: true,
                resizeColumn: true,
                datafetch: 2,
                loadahead: 15,
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    { id: "ord", header: "STT", width: 80 },
                    { id: "code", header: "Tab Id", width: 80 },
                    { id: "wb_window_id", header: "Window Id", hidden: true },
                    { id: "name", header: "Tab Name", width: 250 },
                    { id: "table_name", header: "Tab Table", width: 150 },
                    { id: "tab_type", header: "Tab Type", width: 100 },
                    { id: "tab_view", header: "Tab View", width: 100 },
                    { id: "foreign_key", header: "FOREIGN KEY", width: 150 },
                    { id: "insert_cmd", header: "INSERT CMD", width: 150 },
                    { id: "update_cmd", header: "UPDATE CMD", width: 150 },
                    { id: "delete_cmd", header: "DELETE CMD", width: 150 },
                    { id: "select_cmd", header: "SELECT CMD", width: 150 },
                ],
                //columns: columns
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        css: "frmView",
        rows: [
            {
                height: 39,
                css: { "background-color": "#f5f5f5 !important" },
                type: "toolbar",
                margin: 0,
                borderless: true,
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    function loadData() {
        var windowId = webix.$$("txtWindowId").getValue();
        var url = app.config.host + "/WbTab/GetTabsByWindowId?id=" + windowId;

        webix.ajax().get(url, function (text) {
            var json = JSON.parse(text);

            var table = webix.$$("dmtabData");
            table.clearAll();
            table.parse(json);
        });
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmtabData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmtab,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
