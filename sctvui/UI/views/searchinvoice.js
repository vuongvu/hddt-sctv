define(["app", "views/forms/searchinvoice", "locale"], function (app, searchForm, _) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Tra cứu hóa đơn", details: "Hóa đơn đã lập" });

    var controls = [
        {
            id: "txtSearch",
            view: "search",
            align: "center",
            placeholder: _("SEARCH") + "..",
            width: 300,
            on: {
                onTimedKeyPress: function () {
                    var table = $$("tblSearch");

                    table.clearAll();
                    table.load(table.config.url);
                },
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            css: "webix_secondary",
            icon: " fas fa-edit",
            label: _("TIM_CHI_TIET"),
            width: 100,
            on: {
                onClick: function () {
                    var win = this.$scope.ui(searchForm.$ui);
                    searchForm.setValues($$("tblSearch").config.paramFilter);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnPrint",
            view: "button",
            type: "icon",
            icon: "fas fa-print",
            css: "webix_secondary",
            label: _("PRINT"),
            width: 65,
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = webix.$$("tblSearch");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn");
                        return;
                    }

                    var url = app.config.host + "/Invoice/Preview?id=" + item.inv_InvoiceAuth_id;

                    var box = webix.modalbox({
                        title: "Đang in hóa đơn",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                alert(text);
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);

                                webix.modalbox.hide(box);
                                window.open(fileURL, "_blank");
                            },
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "tblSearch",
                view: "datatable",
                select: true,
                datafetch: 100,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var url = app.config.host + "/Invoice/Search";
                        var item = {};

                        var txtSearch = $$("txtSearch");

                        var filter = {};

                        if (txtSearch !== undefined) {
                            var typingFilter = txtSearch.getValue();

                            if (typingFilter != "") {
                                filter.typingFilter = typingFilter;
                            }
                        }

                        if (view.config.paramFilter != null) {
                            filter.paramFilter = view.config.paramFilter;
                        }

                        if (params == null) {
                            item.start = 0;
                            item.count = view.config.datafetch;
                            item.continue = null;
                            item.filter = filter;
                        } else {
                            item.start = params.start;
                            item.count = params.count;
                            item.continue = params.continue;
                            item.filter = filter;
                        }

                        webix.ajax().bind(view).headers({ "Content-Type": "application/json" }).post(url, item, callback);
                    },
                },
                columns: [
                    { id: "mau_hd", header: _("MAU_HD"), width: 150 },
                    { id: "inv_invoiceSeries", header: _("KY_HIEU"), width: 120 },
                    { id: "inv_invoiceNumber", header: _("SO_HDON"), width: 120 },
                    { id: "inv_invoiceIssuedDate", header: _("NGAY_HDON"), width: 150 },
                    { id: "ma_dt", header: _("MA_DT"), width: 150 },
                    { id: "inv_buyerLegalName", header: _("TEN_DON_VI"), width: 250 },
                    { id: "inv_buyerDisplayName", header: _("TEN_NGUOI_MUA"), width: 250 },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $$("tblSearch").config.paramFilter = {};

            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("tblSearch");
                table.config.paramFilter = item;

                table.clearAll();
                table.load(table.config.url);
            });
        },
    };
});
