define(["app", "locale", "views/forms/infowindow", "views/forms/detailreport", "views/forms/window", "models/user"], function (app, _, infowindow, detailreport, windowform, user) {
    "use strict";

    var windowno = app.path[1].params[0];

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnReload",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-refresh",
            // css: "webix_secondary",
            // label: _("REFRESH"),
            // width: 95,
            on: {
                onClick: function () {
                    //console.log($$("cbReport").getValue());
                    if ($$("cbReport").getValue() == null || $$("cbReport").getValue() == "") {
                        webix.message("Bạn chưa chọn báo cáo !", "error");
                        return;
                    }
                    var cbReport = webix.$$("cbReport");
                    var list = cbReport.getList();

                    var item = list.getItem(cbReport.getValue());

                    infowindow.$ui.id = item.wb_infowindow_id;
                    infowindow.$ui.report_id = cbReport.getValue();
                    infowindow.$ui.load_event = "loadReport";

                    if ($$("tblReport").config.rpParameter != null) {
                        infowindow.$ui.editmode = 1;
                        infowindow.$ui.formvalues = $$("tblReport").config.rpParameter.parameters;
                    } else {
                        infowindow.$ui.editmode = 0;
                        infowindow.$ui.formvalues = null;
                    }

                    var wininfoparam = this.$scope.ui(infowindow.$ui);

                    infowindow.initUI(item.wb_infowindow_id);
                    wininfoparam.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("PRINT") + " (PDF)" + "</span>",
            id: "btnPrint",
            // view: "button",
            // type: "icon",
            // css: "webix_secondary",
            // icon: "fas fa-print",
            // label: _("PRINT") + " PDF",
            // width: 70,
            on: {
                onClick: function () {
                    var table = $$("tblReport");

                    var rpParameter = table.config.rpParameter;

                    if (rpParameter == undefined || rpParameter == null) {
                        webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                        return;
                    }
                    rpParameter.type = "PDF";

                    if (rpParameter != null) {
                        var box = webix.modalbox({
                            title: "Đang in báo cáo ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .post(
                                app.config.host + "/System/CreateReport",
                                rpParameter,
                                {
                                    error: function (text, data, XmlHttpRequest) {
                                        webix.modalbox.hide(box);
                                        webix.message(XmlHttpRequest.statusText, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var file = new Blob([data], { type: "application/pdf" });
                                        var fileURL = URL.createObjectURL(file);
                                        webix.modalbox.hide(box);
                                        window.open(fileURL, "_blank");
                                    },
                                }
                                // function (text, data) {
                                //     var file = new Blob([data], { type: 'application/pdf' });
                                //     var fileURL = URL.createObjectURL(file);
                                //     webix.modalbox.hide(box);
                                //     window.open(fileURL, '_blank');
                                // }
                            );
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-file fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("EXPORT_EXCEL") + "</span>",
            id: "btnExport",
            // view: "button",
            // type: "icon",
            // css: "webix_secondary",
            // icon: "fas fa-file",
            // label: _("EXPORT_EXCEL"),
            // width: 120,
            on: {
                onClick: function () {
                    var table = $$("tblReport");

                    var rpParameter = table.config.rpParameter;
                    if (rpParameter == undefined || rpParameter == null) {
                        webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
                        return;
                    }
                    rpParameter.type = "xlsx";

                    if (rpParameter != null) {
                        var box = webix.modalbox({
                            title: "Đang kết xuất Excel ....",
                            text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                            width: 350,
                            height: 200,
                        });

                        webix
                            .ajax()
                            .response("arraybuffer")
                            .headers({ "Content-type": "application/json" })
                            .post(
                                app.config.host + "/System/CreateReport",
                                rpParameter,
                                {
                                    error: function (text, data, XmlHttpRequest) {
                                        webix.modalbox.hide(box);
                                        webix.message(XmlHttpRequest.statusText, "error");
                                    },
                                    success: function (text, data, XmlHttpRequest) {
                                        var file = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                                        //var fileURL = URL.createObjectURL(file);
                                        webix.modalbox.hide(box);
                                        var reportItem = $$("cbReport").getList().getSelectedItem();

                                        if (reportItem != null) {
                                            var export_name = reportItem.export_name;
                                            webix.html.download(file, export_name + ".xlsx");
                                        }

                                        //window.location.href = fileURL;
                                    },
                                }
                                // function (text, data) {

                                //     var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                                //     var fileURL = URL.createObjectURL(file);

                                //     window.location.href = fileURL;
                                // }
                            );
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        // {
        //     view: "button",
        //     type: "htmlbutton",
        //     width: 120,
        //     label: '<i class="fa fa-file fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("Kết xuất Xml") + "</span>",
        //     id: "btnExportXml",
        //     // view: "button",
        //     // type: "icon",
        //     // css: "webix_secondary",
        //     // icon: "fas fa-file",
        //     // label: _("EXPORT_EXCEL"),
        //     // width: 120,
        //     on: {
        //         onClick: function () {
        //             var table = $$("tblReport");

        //             var rpParameter = table.config.rpParameter;
        //             if (rpParameter == undefined || rpParameter == null) {
        //                 webix.message("Bạn chưa chọn báo cáo hoặc chưa tải dữ liệu !", "error");
        //                 return;
        //             }
        //             rpParameter.type = "xlsx";

        //             if (rpParameter != null) {
        //                 var boxMaster = webix.modalbox({
        //                     title: "Đang kết xuất XML ....",
        //                     text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
        //                     width: 350,
        //                     height: 200,
        //                 });
        //                 webix
        //                     .ajax()
        //                     .headers({ "Content-type": "application/json" })
        //                     .post(app.config.host + "/System/CreateXmlBKBR", rpParameter, function (text) {
        //                         var json = JSON.parse(text);
        //                         //webix.modalbox.hide(box);
        //                         if (!json.hasOwnProperty("error")) {
        //                             //console.log(json);
        //                             // to do code ký xml bảng kê
        //                             webix
        //                                 .ajax()
        //                                 .headers({ "Content-Type": "application/json" })
        //                                 .get(app.config.host + "/Invoice68/GetSignType", function (text) {
        //                                     var result = JSON.parse(text);
        //                                     var kytaptrung = "";
        //                                     for (var i = 0; i < result.length; i++) {
        //                                         var item = result[i];
        //                                         kytaptrung = item.value;
        //                                     }
        //                                     if (kytaptrung.toUpperCase() == "FILE") {
        //                                         var box = webix.modalbox({
        //                                             title: "Chọn chứng thư số",
        //                                             buttons: ["Nhận", "Hủy bỏ"],
        //                                             text: "<div id='ctnCerts'></div>",
        //                                             top: 100,
        //                                             width: 750,
        //                                             callback: function (result) {
        //                                                 switch (result) {
        //                                                     case "1":
        //                                                         webix.modalbox.hide();
        //                                                         webix.modalbox.hide(boxMaster);
        //                                                         break;
        //                                                     case "0":
        //                                                         var table = $$("tblCerts");
        //                                                         var data = table.serialize();

        //                                                         var cert = null;

        //                                                         for (var i = 0; i < data.length; i++) {
        //                                                             if (data[i].chon == true) {
        //                                                                 cert = data[i];
        //                                                                 break;
        //                                                             }
        //                                                         }

        //                                                         if (cert == null) {
        //                                                             webix.message("Bạn chưa chọn chứng thư số");
        //                                                             return;
        //                                                         }

        //                                                         var postData = {
        //                                                             branch_code: user.getDvcsID(),
        //                                                             username: user.getCurrentUser(),
        //                                                             cer_serial: cert.cer_serial,
        //                                                             type: "xml",
        //                                                             xml: json.data,
        //                                                             // hdon_id: itemHdon.id,
        //                                                             // type_cmd: 202
        //                                                         };
        //                                                         // webix.extend(tableHdon, webix.ProgressBar);
        //                                                         // tableHdon.disable();
        //                                                         // tableHdon.showProgress();
        //                                                         webix
        //                                                             .ajax()
        //                                                             .response("arraybuffer")
        //                                                             .headers({ "Content-type": "application/json" })
        //                                                             .post(app.config.host + "/Invoice68/SignXmlBangKeBanRa", postData, {
        //                                                                 error: function (text, data, XmlHttpRequest) {
        //                                                                     webix.modalbox.hide(boxMaster);
        //                                                                     webix.message(XmlHttpRequest.statusText, "error");
        //                                                                 },
        //                                                                 success: function (text, data, XmlHttpRequest) {
        //                                                                     var file = new Blob([data], { type: "application/xml" });
        //                                                                     //var fileURL = URL.createObjectURL(file);
        //                                                                     webix.modalbox.hide(boxMaster);
        //                                                                     var reportItem = $$("cbReport").getList().getSelectedItem();

        //                                                                     if (reportItem != null) {
        //                                                                         var export_name = reportItem.export_name;
        //                                                                         webix.html.download(file, export_name + ".xml");
        //                                                                     }
        //                                                                     //window.location.href = fileURL;
        //                                                                 },
        //                                                             });
        //                                                 }
        //                                             },
        //                                         });

        //                                         var tblMatHang = webix.ui({
        //                                             container: "ctnCerts",
        //                                             id: "tblCerts",
        //                                             view: "datatable",
        //                                             borderless: true,
        //                                             resizeColumn: true,
        //                                             height: 350,
        //                                             columns: [
        //                                                 {
        //                                                     id: "chon",
        //                                                     header: _("CHON"),
        //                                                     width: 65,
        //                                                     checkValue: true,
        //                                                     uncheckValue: false,
        //                                                     template: "{common.checkbox()}",
        //                                                 },
        //                                                 {
        //                                                     id: "cer_serial",
        //                                                     header: _("SO_SERIAL"),
        //                                                     width: 150,
        //                                                 },
        //                                                 {
        //                                                     id: "subject_name",
        //                                                     header: _("SUBJECTNAME"),
        //                                                     width: 450,
        //                                                 },
        //                                                 {
        //                                                     id: "begin_date",
        //                                                     header: _("NGAY_BAT_DAU"),
        //                                                     width: 200,
        //                                                 },
        //                                                 {
        //                                                     id: "end_date",
        //                                                     header: _("NGAY_KET_THUC"),
        //                                                     width: 200,
        //                                                 },
        //                                                 {
        //                                                     id: "issuer",
        //                                                     header: _("DONVI"),
        //                                                     width: 450,
        //                                                 },
        //                                             ],
        //                                             url: app.config.host + "/Invoice68/GetListCertificatesFile68",
        //                                         });
        //                                     } else {
        //                                         if (kytaptrung.toUpperCase() == "K") {
        //                                             var b = webix.ajax(app.config.host + "/Invoice68/GetListCertificates");

        //                                             webix.promise.all([json, b]).then(function (results) {
        //                                                 var listcerts = results[1].json();
        //                                                 var item = results[0];

        //                                                 if (listcerts.length == 0) {
        //                                                     webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
        //                                                     return;
        //                                                 }

        //                                                 require(["views/modules/minvoiceplugin"], function (plugin) {
        //                                                     var hub = plugin.hub();
        //                                                     var hubconnect = plugin.conn();
        //                                                     if (hubconnect.state != 1) {
        //                                                         webix.modalbox.hide(boxMaster);
        //                                                         webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
        //                                                         return;
        //                                                     }
        //                                                     hub.off("resCommand");
        //                                                     hub.on("resCommand", function (result) {
        //                                                         var json = JSON.parse(result);
        //                                                         //console.log(json);
        //                                                         if (json.hasOwnProperty("error")) {
        //                                                             webix.message({ type: "error", text: json.error });
        //                                                         } else {
        //                                                             var reportItem = $$("cbReport").getList().getSelectedItem();
        //                                                             var blob = new Blob([json.xml], { type: "application/xml" });

        //                                                             if (reportItem != null) {
        //                                                                 var export_name = reportItem.export_name;
        //                                                                 webix.html.download(blob, export_name + ".xml");
        //                                                             }
        //                                                         }
        //                                                         webix.modalbox.hide(boxMaster);
        //                                                     });

        //                                                     var _listcerts = [];

        //                                                     for (var i = 0; i < listcerts.length; i++) {
        //                                                         var _cert = listcerts[i];

        //                                                         _listcerts.push({
        //                                                             so_serial: _cert.cer_serial,
        //                                                             ngaybatdau: _cert.begin_date,
        //                                                             ngayketthuc: _cert.end_date,
        //                                                             issuer: _cert.issuer,
        //                                                             subjectname: _cert.subject_name,
        //                                                         });
        //                                                     }

        //                                                     var arg = {
        //                                                         idData: "#data",
        //                                                         xml: item["data"],
        //                                                         shdon: "",
        //                                                         idSigner: "nnt",
        //                                                         tagSign: "NNT",
        //                                                         listcerts: _listcerts,
        //                                                     };

        //                                                     hub.invoke("execCommand", "SignInvoice", JSON.stringify(arg))
        //                                                         .done(function () {
        //                                                             console.log("Success !");
        //                                                         })
        //                                                         .fail(function (error) {
        //                                                             webix.message(error, "error");
        //                                                         });
        //                                                 });
        //                                             });
        //                                         }
        //                                     }
        //                                 });
        //                         } else {
        //                             webix.modalbox.hide(boxMaster);
        //                             webix.message(json.error, "error", 2000);
        //                         }
        //                     });
        //                 // webix.ajax()
        //                 //     .response("arraybuffer")
        //                 //     .headers({ 'Content-type': 'application/json' })
        //                 //     .post(app.config.host + "/System/CreateReport", rpParameter,
        //                 //     {
        //                 //         error: function (text, data, XmlHttpRequest) {
        //                 //             webix.modalbox.hide(box);
        //                 //             webix.message(XmlHttpRequest.statusText, "error");
        //                 //         },
        //                 //         success: function (text, data, XmlHttpRequest) {
        //                 //             var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        //                 //             //var fileURL = URL.createObjectURL(file);
        //                 //             webix.modalbox.hide(box);
        //                 //             var reportItem = $$("cbReport").getList().getSelectedItem();

        //                 //             if (reportItem != null) {
        //                 //                 var export_name = reportItem.export_name;
        //                 //                 webix.html.download(file, export_name + ".xlsx");
        //                 //             }

        //                 //             //window.location.href = fileURL;
        //                 //         }
        //                 //     }
        //                 //     // function (text, data) {

        //                 //     //     var file = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        //                 //     //     var fileURL = URL.createObjectURL(file);

        //                 //     //     window.location.href = fileURL;
        //                 //     // }
        //                 //     );
        //             }
        //         },
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     },
        // },
        {},
        {
            id: "cbReport",
            view: "combo",
            width: 450,
            label: "Chọn báo cáo",
            labelWidth: 90,
            suggest: {
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var windowno = app.path[1].params[0];
                        var url = app.config.host + "/System/ExecuteCommand";

                        var parameters = {
                            command: "CM00016",
                            parameter: {
                                window_id: windowno,
                                username: user.getCurrentUser(),
                            },
                        };

                        webix.ajax().bind(view).headers({ "Content-Type": "application/json" }).post(url, parameters, callback);
                    },
                },
                template: "#name#",
                body: {
                    template: "#name#",
                },
            },
            on: {
                onChange: function () {
                    var report_id = this.getValue();

                    if (report_id == "") {
                        return;
                    }
                    var name = $$("cbReport").getText();

                    webix.$$("title").parse({ title: name, details: name });
                    initTable(report_id);
                },
            },
        },
    ];

    var grid = {
        rows: [
            {
                id: "tblReport",
                view: "datatable",
                resizeColumn: true,
                select: true,
                css: "report",
                columns: [],
                headermenu: {
                    width: 200,
                    autoheight: false,
                    scroll: true,
                },
                scheme: {
                    $change: function (item) {
                        if (item.BOLD == "C") item.$css = "rowBold";
                    },
                },
                pager: "pagerA",
                on: {
                    onBeforeContextMenu: function (id) {
                        var item = this.getItem(id);

                        if (item.DOC_ID != null && item.MA_CT != null) {
                            var mn = $$("ctxMenu");
                            var menuitem = mn.getItem("SUACTGOC");
                            if (menuitem == null) {
                                mn.add({ id: "SUACTGOC", value: "Sửa chứng từ" });
                            }
                        }

                        webix.delay(function () {
                            this.clearSelection();
                            this.select(id.row, id.column);
                        }, this);
                    },
                },
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var contextmenu = webix.ui({
        view: "contextmenu",
        id: "ctxMenu",
        on: {
            onMenuItemClick: function (id) {
                var item = this.getItem(id);

                if (id == "SUACTGOC") {
                    var table = $$("tblReport");
                    var row = table.getSelectedItem();

                    var sql = "SELECT TOP 1 a.* FROM VC_WINDOW a WHERE a.ma_ct='" + row.MA_CT + "'";
                    var url = app.config.host + "/api/System/ExecuteQuery?sql=" + sql;

                    webix.ajax(url, function (text) {
                        var json = JSON.parse(text);
                        var data = json[0];

                        var table = $$("tblReport");
                        var row = table.getSelectedItem();

                        var a = webix.ajax(app.config.host + "/api/System/GetDataById?window_id=" + data.WINDOW_ID + "&id=" + row.DOC_ID);
                        var b = webix.ajax(app.config.host + "/api/System/GetAllByWindowNo?window_id=" + data.WINDOW_ID);

                        webix.promise.all([a, b]).then(function (results) {
                            var a_result = results[0].json();
                            var windowconfig = results[1].json();

                            var vc_window = windowconfig["data"][0];
                            windowform.$ui.id = vc_window.WINDOW_ID;

                            var table = $$("tblReport");
                            var detailForm = table.$scope.ui(windowform.$ui);

                            var item = a_result.data[0];

                            windowform.initUI(windowconfig);
                            windowform.setEditMode(2);
                            windowform.setValues(item, windowconfig);

                            detailForm.show();
                        });
                    });
                } else {
                    var ctx = this.getContext();

                    detailreport.$ui.id = "dtrp_" + item.MENU_ID;
                    detailreport.$ui.parentParameter = ctx.obj.config.rpParameter.parameters;
                    detailreport.$ui.parentRow = ctx.obj.getSelectedItem();

                    this.hide();

                    var win = ctx.obj.$scope.ui(detailreport.$ui);
                    detailreport.initUI(item);
                    win.show();
                }
            },
        },
    });

    var layout = {
        rows: [
            {
                height: 39,
                type: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                borderless: true,
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    function initTable(report_id) {
        var url = app.config.host + "/System/ExecuteCommand";

        var parameters = {
            command: "CM00012",
            parameter: {
                wb_report_id: report_id,
            },
        };

        webix
            .ajax()
            .headers({ "Content-Type": "application/json" })
            .post(url, parameters, function (text) {
                var json = JSON.parse(text);

                var table = webix.$$("tblReport");
                table.clearAll();
                table.config.rpParameter = null;

                var cols = [];
                var isFooter = false;

                for (var i = 0; i < json.length; i++) {
                    var col = json[i];
                    //console.log(col);

                    var item = {
                        id: col.value_field,
                        header: "<center>" + _(col.header_title) + "</center>",
                        width: col.width,
                        hidden: col.hidden == "C" ? true : false,
                    };

                    // cột tổng dưới footer
                    if (col.footer === "" || col.footer === null) {
                    } else {
                        var temp = JSON.parse(col.footer);
                        item.footer = temp;

                        isFooter = true;
                    }

                    //console.log(col.sum);
                    if (col.header != null) {
                        if (col.header != "") {
                            var headers = JSON.parse(col.header);

                            for (var j = 0; j < headers.length; j++) {
                                var hd = headers[j];
                                if (hd != null) {
                                    hd.text = "<center>" + _(hd.text) + "</center>";
                                }
                            }

                            item.header = headers;
                        }
                    }

                    if (col.data_type == "datetime" || col.data_type == "date") {
                        item.format = function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        };
                    }

                    if (col.data_type == "decimal" || col.data_type == "int" || col.data_type == "integer" || col.data_type == "numeric") {
                        item.css = { "text-align": "right" };
                        item.format = webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        });
                    }

                    if (col.data_type == "boolean") {
                        item.template = function (obj, common, value) {
                            if (value) return "<div class='webix_icon fa-check-square-o'></div>";
                            else return "<div class='webix_icon fa-square-o'></div>";
                        };
                    }

                    cols.push(item);
                }

                table.define("footer", isFooter);

                table.config.columns = cols;
                table.config.report_id = json[0]["wb_report_id"];
                table.refreshColumns();
            });
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var windowno = app.path[1].params[0];
            var query = "SELECT * FROM #SCHEMA_NAME#.wb_menu WHERE window_code='" + windowno + "' LIMIT 1";

            var url = app.config.host + "/System/ExecuteQuery?sql=" + encodeURIComponent(query);

            webix.ajax(url, function (text) {
                var json = JSON.parse(text);

                if (json.length > 0) {
                    var title = $$("title");

                    if (title != undefined && title != null) {
                        title.parse({ title: json[0]["name"], details: json[0]["details"] });
                    }
                }
            });

            if (app.path[1].params[1] != null) {
                var id = app.path[1].params[1];
                $$("cbReport").setValue(id);

                var load_event = "loadReport";
                var report_id = "d13001df-01fe-4d4f-ab65-cc98e7b41661";
                var id = "5accccd3-51bd-4370-b130-b01ab35d6c86";

                var now = new Date();
                now.setHours(0, 0, 0, 0);

                var item = {
                    ma_dvcs: user.getDvcsID(),
                    tu_ngay: null,
                    den_ngay: now,
                };

                var table = $$("tblReport");
                table.report_id = report_id;

                var rpParameter = {
                    parameters: item,
                    infowindow_id: id,
                    type: "json",
                    report_id: report_id,
                };

                table.config.rpParameter = rpParameter;

                webix
                    .ajax()
                    .headers({ "Content-type": "application/json" })
                    .post(app.config.host + "/System/CreateReport", rpParameter, function (text) {
                        var data = JSON.parse(text);

                        var table = $$("tblReport");
                        table.clearAll();
                        table.parse(data, "json");
                    });
            }

            $scope.on(app, "loadReport", function (report_id, infoWindowId, item) {
                var table = $$("tblReport");
                // webix.extend(table, webix.OverlayBox);
                webix.extend(table, webix.ProgressBar);
                table.disable();
                table.showProgress();
                //table.showOverlay("<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>");
                if (table.config.report_id == report_id) {
                    var rpParameter = {
                        parameters: item,
                        infowindow_id: infoWindowId,
                        type: "json",
                        report_id: report_id,
                    };

                    table.config.rpParameter = rpParameter;

                    webix
                        .ajax()
                        .headers({ "Content-type": "application/json" })
                        .post(app.config.host + "/System/CreateReport", rpParameter, function (text) {
                            webix.delay(
                                function () {
                                    var data = JSON.parse(text);

                                    var table = $$("tblReport");
                                    table.clearAll();
                                    table.parse(data, "json");
                                },
                                null,
                                null,
                                1000
                            );

                            table.enable();
                            table.hideProgress();
                            // var data = JSON.parse(text);

                            // var table = $$("tblReport");
                            // table.clearAll();
                            // table.parse(data, "json");
                        });
                }
            });

            $scope.on(app, "loadDetailReport", function (report_id, infoWindowId, item, menuitem) {
                var win = $$("dtrp_" + menuitem.MENU_ID);
                var table = win.getChildViews()[1].getChildViews()[1];

                if (table.config.report_id == report_id) {
                    var rpParameter = {
                        parameters: item,
                        infowindow_id: infoWindowId,
                        type: "json",
                        report_id: report_id,
                    };

                    table.config.rpParameter = rpParameter;

                    var ajax = webix.ajax();
                    ajax.master = menuitem;

                    ajax.headers({ "Content-type": "application/json" }).post(app.config.host + "/api/System/CreateReport", rpParameter, function (text) {
                        var win = $$("dtrp_" + this.MENU_ID);
                        var table = win.getChildViews()[1].getChildViews()[1];

                        var data = JSON.parse(text);

                        table.clearAll();
                        table.parse(data, "json");
                    });
                }
            });
        },
        initUI: function () {},
    };
});
