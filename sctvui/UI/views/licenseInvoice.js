define([
    "app",
    "locale",
    "models/user",
    "views/menus/version",
], function (app, _, user, version) {
    'use strict';

    var op = [];
    webix.$$("title").parse({ title: "Thông tin doanh nghiệp", details: "Cập nhật thông tin doanh nghiệp" });

    var controls = [
        {},
        {
            id: "btnSave",
            view: "button",
            type: "danger",
            icon: "save",
            label: "Cấp bản quyền (F10)",
            //width: 135,
            shortcut: "F10",
            on: {
                onClick: function () {
                    var form = webix.$$("foInfoCompany");

                    if (form.validate()) {
                        var item = form.getValues();
                        if (item.floderId == null || item.floderId == "") {
                            var box = webix.modalbox({
                                title: "Đang tạo thư mục  ....",
                                text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                width: 350,
                                height: 200
                            });

                            webix.ajax()
                                .headers({ 'Content-type': 'application/json' })
                                .post(app.config.host + "/License/SaveInfoCompany", item, function (text) {
                                    var data = JSON.parse(text);

                                    if (!data.hasOwnProperty("error")) {
                                        webix.message({ type: "debug", text: "Cấp bản quyền thành công" });
                                        var table = webix.$$("foInfoCompany");
                                        table.refresh();

                                        table.load(table.config.url);
                                        webix.modalbox.hide(box);
                                    }
                                    else {
                                        webix.message({ type: "error", text: data.error });
                                        webix.modalbox.hide(box);
                                    }
                                });
                        }
                        else {
                            webix.message("Khách hàng đã được cấp bản quyền !", "error");
                            return;
                        }
                    }
                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {}
    ];

    var layout = {
        // paddingY: 60,
        // paddingX: 500,
        cols: [
            { gravity: 0.5, template: "" },
            {
                rows: [
                    { gravity: 1, template: "" },
                    {
                        view: "form",
                        id: "foInfoCompany",
                        paddingY: 15,
                        // width: 700,
                        // height:300,
                        position: "center",
                        paddingX: 15,
                        complexData: true,
                        margin: 0,
                        elements: [
                            {
                                cols: [
                                    {
                                        view: "text",
                                        name: "ms_thue",
                                        label: _("MS_THUE"),
                                        css: "mcls_license_text",
                                        readonly: true,
                                        labelPosition: "top"
                                    },
                                    {
                                        view: "text",
                                        name: "nguoi_dai_dien",
                                        label: _("NGUOI_DAI_DIEN_PL"),
                                        css: "mcls_readonly_text",
                                        readonly: true,
                                        labelPosition: "top"
                                    }
                                ]

                            },
                            {
                                view: "text",
                                name: "ten_dvcs",
                                label: _("DONVI"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "text",
                                name: "dia_chi",
                                label: _("DIA_CHI"),
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "fieldset",
                                label: "Thông tin đơn vị chủ quản",
                                body: {
                                    rows: [
                                        {
                                            view: "text",
                                            name: "mst_dvcq",
                                            label: _("MS_THUE"),
                                            validate: webix.rules.isTaxCode,
                                            invalidMessage: "Mã số thuế không hợp lệ",
                                            labelPosition: "top",
                                            on: {
                                                onChange: function (newv, oldv) {
                                                    if (newv != "") {
                                                        this.validate();
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            view: "text",
                                            name: "ten_dvcq",
                                            label: _("DONVI"),
                                            labelPosition: "top"
                                        },
                                        {
                                            view: "text",
                                            name: "email_dvcq",
                                            validate: webix.rules.isEmail,
                                            invalidMessage: "Email không hợp lệ",
                                            label: _("EMAIL"),
                                            labelPosition: "top",
                                            on: {
                                                onChange: function (newv, oldv) {
                                                    if (newv != "") {
                                                        this.validate();
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                view: "text",
                                name: "floderId",
                                label: _("Key"),
                                css: "mcls_license_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "text",
                                name: "floderTitle",
                                label: _("Title"),
                                validate: webix.rules.isEmpty,
                                invalidMessage: "Bạn phải nhập tiêu đề cho thư mục !",
                                required: true,
                                //css: "mcls_readonly_text",
                                //readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "text",
                                name: "floderDes",
                                label: _("Description"),
                                validate: webix.rules.isEmpty,
                                invalidMessage: "Bạn phải nhập diễn dải cho thư mục !",
                                required: true,
                                //css: "mcls_readonly_text",
                                //readonly: true,
                                labelPosition: "top"
                            },
                            {
                                margin: 15,
                                cols: controls

                            }

                        ]
                    },
                    { gravity: 5, template: "" }
                ]
            },
            { gravity: 0.5, template: "" }
        ]

    };


    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var url = app.config.host + "/System/GetInfoCompany";
            $$("foInfoCompany").config.url = url;
            webix.ajax(url, function (text) {
                var json = JSON.parse(text);
                $$("foInfoCompany").setValues(json);

            });
        }
    };
});