define([
    "app",
    "locale",
    "views/forms/license",
], function (app, _, licenseForm) {
    'use strict';

    webix.$$("title").parse({ title: "Cập nhật phần mềm", details: "Cập nhật phần mềm" });

    var controls = [
        {},
        {
            id: "btnUpdate",
            view: "button",
            type: "danger",
            //css: "mcls_btn_save", 
            icon: "save",
            label: _("Cập nhật (F10)"),
            // width: 100,
            shortcut: "F10",
            on: {
                onClick: function () {

                    var form = webix.$$("updateform");

                    if (form.validate()) {
                        webix.confirm({
                            title: "Cập nhật phần mềm",
                            text: "Bạn có muốn cập nhật phần mềm ", ok: "Có", cancel: "Không",
                            callback: function (res) {
                                if (res) {
                                    var box = webix.modalbox({
                                        title: "Đang tải cập nhật",
                                        buttons: ["Hủy bỏ"],
                                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                                        width: 350,
                                        height: 200,
                                        callback: function (result) {
                                            switch (result) {
                                                case "0":
                                                    clearTimeout(interVal);
                                                    webix.modalbox.hide(box);
                                                    break;
                                            }
                                        }
                                    });
                                    var interVal = setTimeout(function () {

                                        webix.ajax()
                                            .headers({ 'Content-type': 'application/json' })
                                            .post(app.config.host + "/System/UpdateVersion", function (text) {
                                                var data = JSON.parse(text);
                                                if (!data.hasOwnProperty("error")) {
                                                    webix.modalbox.hide(box);
                                                    clearTimeout(interVal);

                                                    webix.message("Cập nhật thành công !", "debug");
                                                }
                                                else {
                                                    webix.modalbox.hide(box);
                                                    clearTimeout(interVal);
                                                    webix.message(data.error, "error");
                                                }
                                            });

                                    }, 3000);
                                }
                            }
                        });
                    }

                }
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {}
    ];

    var layout = {
        cols: [
            { gravity: 0.5, template: "" },
            {
                rows: [
                    { gravity: 0.2, template: "" },
                    {
                        view: "template", template: "<div><span style = 'padding-left:15px; margin-top:65px;'><i class='fas fa-star'></i> Thông tin cập nhật</span></div>",
                        css: { "background-color": "white", "color": "red", "border-radius": "15px 15px 0px 0px", "font-size": "15px", "margin-top":"65px" },
                        type: "header"
                    },
                    {
                        view: "form",
                        id: "updateform",
                        paddingY: 10,
                        paddingX: 10,
                        complexData: true,
                        margin: 0,
                        elements: [
                            {
                                view: "text",
                                name: "old_ver",
                                label: "Version hiện tại",
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "text",
                                name: "new_ver",
                                label: "Version mới nhất",
                                css: "mcls_readonly_text",
                                readonly: true,
                                labelPosition: "top"
                            },
                            {
                                view: "fieldset",
                                label: "Thông tin bản cập nhật",
                                height: 200,
                                body: {
                                    rows: [
                                        {
                                            view: "textarea",
                                            name: "info_update",
                                            labelPosition: "top",
                                            css: "mcls_readonly_text",
                                            readonly: true,
                                        }
                                    ]
                                }
                            },
                            {
                                margin: 15,
                                cols: controls
                            }
                        ]

                    },
                    { gravity: 1, template: "" }
                ]
            },
            { gravity: 0.5, template: "" }
        ]
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var url = app.config.host + "/System/GetVersionUpdate";

            webix.ajax(url, function (text) {
                var json = JSON.parse(text);
                $$("updateform").setValues(json);

            });
        }

    };
});