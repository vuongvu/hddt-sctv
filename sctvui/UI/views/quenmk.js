define(["app", "locale"], function (app, _) {
    //var json = version.getInfocompany();
    //$$("hotro-form").setValues(json);

    var layout = {
        view: "window",
        modal: true,
        id: "retrivepass-win",
        position: "center",
        head: {
            // height: 40,
            rows: [
                {
                    height: 15,
                    borderless: true,
                    cols: [
                        { gravity: 1, template: "" },
                        {
                            view: "icon",
                            icon: "fal fa-times",
                            css: "icquenmk",
                            hotkey: "esc",
                            click: function () {
                                $$("retrivepass-win").close();
                            },
                        },
                    ],
                },
                {
                    id: "head-form",
                    height: 60,
                    view: "template",
                    css: "quenmklock",
                    borderless: true,
                    template: function (obj) {
                        return '<img src="assets/imgs/locker.png" >';
                    },
                },
                {
                    view: "label",
                    label: "Quên mật khẩu",
                    css: "quenmk",
                },
            ],
        },
        width: 500,
        minheight: 240,
        body: {
            paddingY: 10,
            paddingX: 30,
            //elementsConfig:{labelWidth: 120},
            view: "form",
            id: "retrivepassForm",
            complexData: true,
            elements: [
                {
                    view: "template",
                    borderless: true,
                    template: "<div>Hệ thống của SCTV sẽ gửi mật khẩu mới cho Quý khách hàng. " + "Quý khách hàng dùng mật khẩu đó để đăng nhập vào phần mềm</div>",
                    css: "quenmktemplate",
                },
                {
                    view: "text",
                    labelPosition: "top",
                    //id:          "email",
                    name: "email",
                    label: "MỜI NHẬP EMAIL",
                    icon: "email",
                    css: "quenmkemail",
                    required: true,
                    invalidMessage: "Bạn chưa nhập địa chỉ email hoặc email định dạng không đúng",
                    placeholder: "ví dụ lan@gmail.com",
                    validate: webix.rules.isEmail,
                },
                // {
                //     view: "fieldset",
                //     label: "Hướng dẫn",
                //     body: {
                //         rows: [
                //             {
                //                 id: "version",
                //                 //borderless: true,
                //                 css: { "background": "#e6f7ff !important" },
                //                 // width: 200,
                //                 view: "template",
                //                 template: "<div class=''><p><b>Bước 1:</b></p><label> Quý khách hàng vui lòng nhập Email đăng ký tài khoản !</label> <br />"
                //                 + "<p><b>Bước 2:</b></p><label> Đăng nhập tài khoản Email vừa nhập !</label></div>"
                //                 + "<p><b>Bước 3:</b></p><label> Hệ thống của SCTV sẽ gửi mật khẩu mới cho Quý khách hàng. Quý khách hàng dùng mật khẩu đó để đăng nhập vào phần mềm !</label></div>"
                //             }
                //         ]
                //     }
                // },

                {
                    margin: 10,
                    cols: [
                        {
                            shortcut: "F10",
                            view: "button",
                            label: "Gửi yêu cầu (F10)",
                            css: "btnlogin",
                            align: "center",
                            height: 43,
                            width: 180,
                            type: "form",
                            on: {
                                onClick: function () {
                                    if ($$("retrivepassForm").validate()) {
                                        var form = $$("retrivepassForm");

                                        webix.extend(form, webix.ProgressBar);
                                        form.disable();
                                        form.showProgress();

                                        webix
                                            .ajax()
                                            .headers({ "Content-type": "application/json" })
                                            .post(app.config.host + "/Account/ResetPass", $$("retrivepassForm").getValues(), function (text, xml, xhr) {
                                                var json = JSON.parse(text);

                                                var form = $$("retrivepassForm");
                                                form.enable();
                                                form.hideProgress();

                                                if (json.hasOwnProperty("error")) {
                                                    // webix.alert({
                                                    //     title: "Thông báo",
                                                    //     ok: "OK",
                                                    //     text: json.error,
                                                    //     type: "alert-error"
                                                    // });
                                                    webix.message(json.error, "error");
                                                } else {
                                                    webix.alert({
                                                        title: "Thông báo",
                                                        ok: "OK",
                                                        text: "Đã gửi mật khẩu tới email.Bạn hãy kiểm tra lại.",
                                                    });
                                                }
                                            });
                                    } else {
                                        $$("retrivepassForm").focus();
                                    }
                                },
                            },
                            click: function () {
                                this.callEvent("onClick");
                            },
                        },
                        // {
                        //     view: "button", hidden: true, hotkey: "esc", label: "Hủy bỏ (ESC)", align: "center", width: 120, type: "danger",
                        //     click: function () {
                        //         webix.$$("retrivepass-win").close();
                        //     }
                        // },
                        {},
                    ],
                },
                { height: 10 },
            ],
        },
    };
    return {
        $ui: layout,
        initUI: function (json) {
            $$("retrivepassForm").setValues(json);
        },
    };
});
