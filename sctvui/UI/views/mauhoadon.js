define(["app", "views/forms/mauhoadon", "locale", "views/modules/minvoiceplugin"], function (app, dmwindowform, _, plugin) {
    "use strict";

    var op = [];

    webix.$$("title").parse({ title: "Mẫu hóa đơn", details: "Danh sách mẫu hóa đơn" });

    var submenu = webix.ui({
        view: "submenu",
        width: 200,
        padding: 0,
        data: [
            // { id: "createInvoiceTemplate", value: "Tạo mẫu hóa đơn" },
            { id: "printInvoiceTemplate", value: "Xem mẫu hóa đơn (PDF)" },
            { id: "printTempHtml", value: "Xem mẫu hóa đơn (Html)" },
            { id: "dowload", value: "Tải file .repx" },
            // { id: "signInvoiceTemplate", value: "Ký mẫu hóa đơn" },
            { id: "uploadInvoiceTemplate", value: "Upload mẫu hóa đơn" },
        ],
        on: {
            onItemClick: function (id, e, node) {
                this.hide();

                if (id == "createInvoiceTemplate") {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn", "error");
                        return;
                    }

                    var box = webix.modalbox({
                        title: "Đang tạo mẫu hóa đơn",
                        buttons: ["Hủy bỏ"],
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                        callback: function (result) {
                            switch (result) {
                                case "0":
                                    clearInterval(interVal);
                                    webix.modalbox.hide(box);
                                    break;
                            }
                        },
                    });

                    var interVal = setInterval(function () {
                        var url = app.config.host + "/Invoice/GetInvoiceTemplate?id=" + item.id;

                        webix.ajax(url, {
                            error: function (text, data, XmlHttpRequest) {
                                alert(text);
                                webix.modalbox.hide(box);
                                clearInterval(interVal);
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var result = JSON.parse(text);

                                if (result.trang_thai_mau == "Đã tạo mẫu") {
                                    var table = webix.$$("dmwindowData");
                                    var item = table.getSelectedItem();

                                    item.trang_thai_mau = "Đã tạo mẫu";
                                    table.updateItem(item);

                                    webix.modalbox.hide(box);
                                    clearInterval(interVal);
                                }
                            },
                        });
                    }, 3000);

                    window.location.href = app.config.host + "/Content/App/ReportDesign/ReportDesign.application?type=99&id=" + item.id;
                }

                if (id == "printInvoiceTemplate") {
                    var item = $$("dmwindowData").getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn", "error");
                        return;
                    }

                    var url = app.config.host + "/Invoice/PrintInvoiceTemplate?id=" + item.id;
                    var box = webix.modalbox({
                        title: "Đang in mẫu hóa đơn (PDF) ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });
                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            var file = new Blob([data], { type: "application/pdf" });
                            //  webix.html.download(file, "ahihi.pdf");
                            var fileURL = URL.createObjectURL(file);
                            //console.log(fileURL);
                            webix.modalbox.hide(box);
                            window.open(fileURL, "_blank");
                        });
                }
                if (id == "dowload") {
                    var item = $$("dmwindowData").getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn", "error");
                        return;
                    }

                    if (item.template_status != "Đã tạo mẫu") {
                        webix.message("Mẫu hóa đơn chưa được tạo, không thể tải xuống !", "error");
                        return;
                    }

                    var url = app.config.host + "/Invoice/DowloadRepx?id=" + item.id;
                    var box = webix.modalbox({
                        title: "Đang tải mẫu hóa đơn (repx) ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });
                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            var file = new Blob([data], { type: "application/octet-stream" });
                            webix.html.download(file, "InvoiceTemplate.repx");
                            // var fileURL = URL.createObjectURL(file);
                            //console.log(fileURL);
                            webix.modalbox.hide(box);
                            //window.open(fileURL, '_blank');
                        });
                }

                if (id == "printTempHtml") {
                    var item = $$("dmwindowData").getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn");
                        return;
                    }

                    var url = app.config.host + "/Invoice/PrintInvoiceTemplate?id=" + item.id + "&type=Html";
                    var box = webix.modalbox({
                        title: "Đang in mẫu hóa đơn (Html) ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });
                    var newwin = window.open();

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, function (text, data) {
                            var file = new Blob([data], { type: "text/html" });
                            var fileURL = URL.createObjectURL(file);
                            webix.modalbox.hide(box);
                            newwin.location = fileURL;
                        });
                }

                if (id == "signInvoiceTemplate") {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn mẫu hóa đơn", "error");
                        return;
                    }

                    var a = webix.ajax(app.config.host + "/Invoice/GetXmlTemplate?id=" + item.id);
                    var b = webix.ajax(app.config.host + "/Invoice/GetListCertificates");
                    var c = webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(app.config.host + "/System/ExecuteCommand", {
                            command: "CM00019",
                            parameter: {
                                ma_dvcs: "",
                            },
                        });

                    webix.promise.all([a, b, c]).then(function (results) {
                        var xmlData = results[0].json();
                        var listcerts = results[1].json();
                        var c_results = results[2].json();

                        if (listcerts.length == 0) {
                            webix.message({ type: "error", text: "Bạn chưa được phân quyền chứng thư số" });
                            return;
                        }

                        var kytaptrung = "";
                        var server_cert = "";

                        for (var i = 0; i < c_results.length; i++) {
                            var item = c_results[i];

                            if (item.code == "KYTAPTRUNG") {
                                kytaptrung = item.value;
                            }

                            if (item.ma == "SERVER_CERT") {
                                server_cert = item.value;
                            }
                        }

                        if (kytaptrung == "K") {
                            var hub = plugin.hub();

                            hub.off("resCommand");
                            hub.on("resCommand", function (result) {
                                var json = JSON.parse(result);

                                if (json.hasOwnProperty("error")) {
                                    webix.message({ type: "error", text: json.error });
                                } else {
                                    var callback = json.callback;

                                    if (callback == "SignInvoice") {
                                        var table = webix.$$("dmwindowData");
                                        var item = table.getSelectedItem();

                                        var parameter = {
                                            id: item.id,
                                            xml: json.xml,
                                        };

                                        webix
                                            .ajax()
                                            .headers({ "Content-type": "application/json" })
                                            .post(app.config.host + "/Invoice/SignXmlTemplate", parameter, function (text) {
                                                var json = JSON.parse(text);

                                                if (!json.hasOwnProperty("error")) {
                                                    var table = $$("dmwindowData");
                                                    table.clearAll();
                                                    table.load(table.config.url);
                                                } else {
                                                    webix.message({ type: "error", text: json.error });
                                                }
                                            });
                                    }
                                }
                            });

                            var _listcerts = [];

                            for (var i = 0; i < listcerts.length; i++) {
                                var item = listcerts[i];

                                _listcerts.push({
                                    so_serial: item.cer_serial,
                                    ngaybatdau: item.begin_date,
                                    ngayketthuc: item.end_date,
                                    issuer: item.issuer,
                                    subjectname: item.subject_name,
                                });
                            }

                            var arg = {
                                callback: "SignInvoice",
                                idData: "#data",
                                xml: xmlData["xml"],
                                idSigner: "seller",
                                tagSign: "Signatures",
                                listcerts: _listcerts,
                            };

                            hub.invoke("execCommand", arg.callback, JSON.stringify(arg));
                        } else {
                            var box = webix.modalbox({
                                title: "Chọn chứng thư số",
                                buttons: ["Nhận", "Hủy bỏ"],
                                text: "<div id='ctnCerts'></div>",
                                top: 100,
                                width: 750,
                                callback: function (result) {
                                    switch (result) {
                                        case "1":
                                            webix.modalbox.hide();
                                            break;
                                        case "0":
                                            var table = $$("tblCerts");
                                            var data = table.serialize();

                                            var cert = null;

                                            for (var i = 0; i < data.length; i++) {
                                                if (data[i].chon == true) {
                                                    cert = data[i];
                                                    break;
                                                }
                                            }

                                            if (cert == null) {
                                                webix.message("Bạn chưa chọn chứng thư số");
                                                return;
                                            }

                                            var table = webix.$$("dmwindowData");
                                            var item = table.getSelectedItem();

                                            webix.ajax(app.config.host + "/Invoice/GetXmlTemplate?id=" + item.id, function (text, data) {
                                                var xmlData = JSON.parse(text);

                                                var parameters = {
                                                    xml: xmlData["xml"],
                                                    refUri: "#data",
                                                    certSerial: cert.cer_serial,
                                                    tokenSerial: cert.token_serial,
                                                    signId: "seller",
                                                    signTag: "Signatures",
                                                    pass: cert.pass,
                                                    inv_InvoiceAuth_id: xmlData["id"],
                                                };

                                                webix
                                                    .ajax()
                                                    .headers({ "Content-Type": "application/json" })
                                                    .post(server_cert + "/Token/SignXml", parameters, function (text) {
                                                        var json = JSON.parse(text);

                                                        if (json.hasOwnProperty("error")) {
                                                            webix.message(json["error"]);
                                                        } else {
                                                            var parameter = {
                                                                id: item.inv_InvoiceAuth_id,
                                                                xml: json.xml,
                                                            };

                                                            webix
                                                                .ajax()
                                                                .headers({ "Content-type": "application/json" })
                                                                .post(app.config.host + "/Invoice/SignXmlTemplate", parameter, function (text) {
                                                                    var json = JSON.parse(text);

                                                                    if (!json.hasOwnProperty("error")) {
                                                                        var table = $$("dmwindowData");
                                                                        table.clearAll();
                                                                        table.load(table.config.url);
                                                                    } else {
                                                                        webix.message({ type: "error", text: json.error });
                                                                    }
                                                                });
                                                        }
                                                    });
                                            });
                                    }
                                },
                            });

                            var popupText = document.getElementsByClassName("webix_popup_text")[0];
                            popupText.style.paddingTop = 0;

                            var tblMatHang = webix.ui({
                                container: "ctnCerts",
                                id: "tblCerts",
                                view: "datatable",
                                borderless: true,
                                height: 350,
                                columns: [
                                    {
                                        id: "chon",
                                        header: _("CHON"),
                                        width: 65,
                                        checkValue: true,
                                        uncheckValue: false,
                                        template: "{common.checkbox()}",
                                    },
                                    {
                                        id: "cert_type",
                                        header: _("Loại"),
                                        width: 100,
                                    },
                                    {
                                        id: "cer_serial",
                                        header: _("SO_SERIAL"),
                                        width: 150,
                                    },
                                    {
                                        id: "subject_name",
                                        header: _("SUBJECTNAME"),
                                        width: 450,
                                    },
                                    {
                                        id: "begin_date",
                                        header: _("NGAY_BAT_DAU"),
                                        width: 200,
                                    },
                                    {
                                        id: "end_date",
                                        header: _("NGAY_KET_THUC"),
                                        width: 200,
                                    },
                                    {
                                        id: "issuer",
                                        header: _("DONVI"),
                                        width: 450,
                                    },
                                ],
                                url: app.config.host + "/Invoice/GetListCertificates",
                            });
                        }
                    });
                }

                if (id == "uploadInvoiceTemplate") {
                    require(["views/forms/uploadfile"], function (uploadForm) {
                        var table = webix.$$("dmwindowData");

                        uploadForm.$ui.tableId = table.config.id;
                        var win = table.$scope.ui(uploadForm.$ui);

                        win.getHead().setHTML("Tải file mẫu hóa đơn");
                        win.show();

                        var item = table.getSelectedItem();

                        webix.$$("uploader").define("upload", app.config.host + "/Invoice/UploadInvTemplate");
                        webix.$$("uploader").define("formData", { dmmauhoadon_id: item.id });
                        webix.$$("uploader").refresh();
                    });
                }
            },
        },
    });

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-plus",
            // css: "webix_secondary",
            // label: _("THEM") + " (F4)",
            // width: 85,
            //css:"button_raised",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var win = this.$scope.ui(dmwindowform.$ui);
                    dmwindowform.setEditMode(1);

                    win.show();

                    var form = $$("mauhoadon-form");
                    var values = form.getValues();

                    values.invoice_type = "E";
                    values.copy_number = 0;
                    values.row_print_template = 10;

                    form.setValues(values);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-edit",
            // css: "webix_secondary",
            // label: _("SUA") + " (F3)",
            // width: 85,
            //css:"button_raised",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("mauhoadon-form");

                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    form.setValues(item);

                    dmwindowform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-trash-o",
            // label: _("XOA") + " (F8)",
            // css: "webix_secondary",
            // width: 85,
            //css:"button_raised",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    var khData = {};
                    khData.windowid = app.path[1].params[0];
                    khData.editmode = 3;
                    khData.data = [];
                    khData.data.push(item);

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.name,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/System/Save", khData, function (text) {
                                        var json = JSON.parse(text);
                                        if (json.hasOwnProperty("error")) {
                                            webix.message({ type: "error", text: json.error });
                                            return;
                                        }

                                        table.remove(item.id);
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-files-o fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("COPY") + " (F7)</span>",
            id: "btnCopy",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-copy",
            // label: _("COPY") + " (F7)",
            // css: "webix_secondary",
            // width: 90,
            //css:"button_raised",
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message({ type: "error", text: "Bạn chưa chọn mẫu cần sao chép" });
                        return;
                    }

                    var comp = {
                        view: "window",
                        modal: true,
                        head: "Sao chép mẫu",
                        position: "center",
                        body: {
                            view: "form",
                            paddingY: 10,
                            paddingX: 10,
                            margin: 0,
                            complexData: true,
                            elements: [
                                {
                                    view: "text",
                                    name: "invoice_series",
                                    labelPosition: "top",
                                    width: 300,
                                    value: item.invoice_series,
                                    required: true,
                                    on: {
                                        onChange: function (newv, oldv) {
                                            if (newv == undefined) return;
                                            if (newv == "") return;

                                            if (newv.length != 6) {
                                                webix.message({ type: "error", text: "Độ dài ký hiệu phải là 6 ký tự" });
                                                this.focus();
                                                return;
                                            }

                                            var patt = /^[A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][/]$/;

                                            if (patt.test(newv.substring(0, 3)) == false) {
                                                webix.message({ type: "error", text: "3 ký tự đầu không đúng" });
                                                this.focus();
                                                return;
                                            }

                                            patt = /^[1-9][0-9]$/;

                                            var nam = newv.substring(3, 5);

                                            if (patt.test(nam) == false) {
                                                webix.message({ type: "error", text: "2 ký tự năm không đúng" });
                                                this.focus();
                                                return;
                                            }

                                            var now = new Date();
                                            var year = now.getFullYear();

                                            nam = Number("20" + nam);

                                            if (nam < year - 1 || nam > year) {
                                                webix.message({ type: "error", text: "Năm ký hiệu không đúng" });
                                                this.focus();
                                                return;
                                            }

                                            if (newv.substring(5, 6) != "E") {
                                                webix.message({ type: "error", text: "Ký tự cuối phải là E" });
                                                this.focus();
                                                return;
                                            }
                                        },
                                    },
                                },
                                {
                                    margin: 10,
                                    cols: [
                                        {},
                                        {
                                            shortcut: "F10",
                                            view: "button",
                                            label: "Lưu (F10)",
                                            type: "form",
                                            align: "center",
                                            width: 120,
                                            on: {
                                                onClick: function () {
                                                    var win = this.getTopParentView();
                                                    var form = win.queryView({ view: "form" });

                                                    if (form.validate() == false) {
                                                        return;
                                                    }
                                                    var item = form.getValues();
                                                    var table = webix.$$("dmwindowData");
                                                    var selectedItem = table.getSelectedItem();

                                                    var url = app.config.host + "/Invoice/CopyTemplate";
                                                    item.id = selectedItem.id;

                                                    webix
                                                        .ajax()
                                                        .bind(win)
                                                        .headers({ "Content-type": "application/json" })
                                                        .post(url, item, function (text) {
                                                            var data = JSON.parse(text);

                                                            if (!data.hasOwnProperty("error")) {
                                                                this.close();
                                                                if (data.hasOwnProperty("ok") && data.ok) {
                                                                    webix.message("Thêm mới mẫu hóa đơn thành công!", "success");
                                                                }
                                                                var table = webix.$$("dmwindowData");
                                                                table.clearAll();
                                                                table.load(table.config.url);
                                                            } else {
                                                                webix.message(data.error, "error");
                                                            }
                                                        });
                                                },
                                            },
                                            click: function () {
                                                this.callEvent("onClick");
                                            },
                                        },
                                        {
                                            view: "button",
                                            hotkey: "esc",
                                            label: "Hủy bỏ (ESC)",
                                            align: "center",
                                            type: "danger",
                                            width: 120,
                                            click: function () {
                                                var win = this.getTopParentView();
                                                win.close();
                                            },
                                        },
                                        {},
                                    ],
                                },
                            ],
                            rules: {
                                $obj: function () {
                                    var data = this.getValues();
                                    var newv = data.invoice_series;

                                    if (newv.length != 6) {
                                        webix.message({ type: "error", text: "Độ dài ký hiệu phải là 6 ký tự" });
                                        return false;
                                    }

                                    var patt = /^[A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][A,B,C,D,E,G,H,K,L,M,N,P,Q,R,S,T,U,V,X,Y][/]$/;

                                    if (patt.test(newv.substring(0, 3)) == false) {
                                        webix.message({ type: "error", text: "3 ký tự đầu không đúng" });
                                        return false;
                                    }

                                    patt = /^[1-9][0-9]$/;

                                    var nam = newv.substring(3, 5);

                                    if (patt.test(nam) == false) {
                                        webix.message({ type: "error", text: "2 ký tự năm không đúng" });
                                        return false;
                                    }

                                    var now = new Date();
                                    var year = now.getFullYear();

                                    nam = Number("20" + nam);

                                    if (nam < year - 1 || nam > year) {
                                        webix.message({ type: "error", text: "Năm ký hiệu không đúng" });
                                        return false;
                                    }

                                    if (newv.substring(5, 6) != "E") {
                                        webix.message({ type: "error", text: "Ký tự cuối phải là E" });
                                        return false;
                                    }

                                    return true;
                                },
                            },
                        },
                    };

                    var win = this.$scope.ui(comp);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
            rules: {
                $obj: function () {
                    var data = this.getValues();

                    return true; //success!
                },
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 120,
            label: '<i class="fa fa-tasks fa-lg aria-hidden="true"></i> ' + '<span class="text">' + _("CHUC_NANG") + "</span>",
            id: "btnChucNang",
            // view: "button",
            // type: "icon",
            // icon: "fas fa-angle-down",
            // label: "Chức năng",
            // width: 120,
            // css: "button_raised",
            click: function () {
                submenu.show(webix.$$("btnChucNang").$view);
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                datafetch: 100,
                //loadahead: 15,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var windowno = app.path[1].params[0];
                        var url = app.config.host + "/System/GetDataByWindowNo?window_id=" + windowno;

                        if (params == null) {
                            url = url + "&start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                        } else {
                            url = url + "&start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                            if (params.filter != null) {
                                var array = [];

                                for (var property in params.filter) {
                                    var column = view.getColumnConfig(property);

                                    if (column !== undefined) {
                                        var item = {
                                            columnName: property,
                                            columnType: column.columnType,
                                            value: params.filter[property],
                                        };

                                        if (column.tableAlias != undefined) {
                                            item.tableAlias = column.tableAlias;
                                        }

                                        if (item.columnType == "bit") {
                                            if (item.value != "") {
                                                item.value = item.value == "true" ? true : item.value == "false" ? false : "";
                                            }
                                        }

                                        if (item.columnType == "int" || item.columnType == "decimal") {
                                            if (item.value != "") {
                                                if (webix.rules.isNumber(item.value)) {
                                                    item.value = Number(item.value);
                                                } else {
                                                    item.value = "";
                                                }
                                            }
                                        }

                                        array.push(item);
                                    }
                                }

                                //console.log(JSON.stringify(array));
                                if (array.length > 0) {
                                    url = url + "&filter=" + JSON.stringify(array);
                                } else {
                                    url = url + "&filter=null";
                                }
                            } else {
                                url = url + "&filter=null";
                            }
                        }

                        if (view.config.infoparam != null) {
                            url = url + "&infoparam=" + JSON.stringify(view.config.infoparam);
                        } else {
                            url = url + "&infoparam=null";
                        }

                        var tlbParams = $$("tlbParams");

                        if (tlbParams !== undefined) {
                            var childViews = tlbParams.getChildViews();

                            var parameters = [];

                            for (var i = 0; i < childViews.length; i++) {
                                var child = childViews[i];

                                var id = child.config.id;
                                var value_field = child.config.value_field;

                                var item = {
                                    columnName: value_field,
                                    value: $$(id).getValue(),
                                };

                                parameters.push(item);
                            }

                            url = url + "&tlbparam=" + JSON.stringify(parameters);
                        } else {
                            url = url + "&tlbparam=null";
                        }

                        //console.log(url);

                        webix.ajax(url, callback, view);
                    },
                },
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    { id: "name", columnType: "character varying", header: [_("TEN_MAU"), { content: "serverFilter" }], width: 250 },
                    {
                        id: "sl_invoice_type_id",
                        view: "combo",
                        options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00008",
                        header: [_("LOAI_HOA_DON"), { content: "serverSelectFilter" }],
                        columnType: "uuid",
                        tableAlias: "sl_invoice_template",
                        width: 300,
                    },
                    {
                        id: "invoice_type",
                        view: "combo",
                        options: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00009",
                        header: [_("HINH_THUC"), { content: "serverSelectFilter" }],
                        columnType: "character varying",
                        width: 200,
                    },
                    { id: "template_code", columnType: "character varying", header: [_("MAU_SO"), { content: "serverFilter" }], width: 150 },
                    { id: "invoice_series", columnType: "character varying", header: [_("KY_HIEU"), { content: "serverFilter" }], width: 150 },
                    // { id: "copy_number", columnType: "integer", header: [_("SO_LIEN"), { content: "serverFilter" }], width: 150 },
                    { id: "template_status", columnType: "character varying", header: [_("TRANG_THAI_MAU"), { content: "serverFilter" }], width: 150 },
                    // {
                    //     id: "signed",
                    //     checkValue: true,
                    //     uncheckValue: false,
                    //     template: function (obj, common, value) {
                    //         if (value)
                    //             return "<div class='webix_icon fa-check-square-o'></div>";
                    //         else
                    //             return "<div class='webix_icon fa-square-o'></div>";
                    //     },
                    //     columnType: "boolean",
                    //     header: [_("DA_KY"), { content: "serverFilter" }],
                    //     width: 100
                    // }
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                type: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                view: "toolbar",
                cols: controls,
                borderless: true,
            },
            {
                rows: [grid],
            },
        ],
    };

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
