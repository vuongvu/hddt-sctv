define([
    "views/modules/minvoiceplugin",
    "views/menus/search",
    "views/menus/profile",
    "views/menus/notify",
    //"views/menus/sidebar",
    "views/menus/menubar",
    "models/user",
    "views/menus/version",
    // "views/menus/notification",
    "views/webix/icon",
    "views/webix/menutree",
], function (plugin, search, profile, notify, menuBar, user, version) {
    var apiHost = menuBar.getApiHost();

    webix.attachEvent("onBeforeAjax", function (mode, url, data, request, headers, files, promise) {
        var token = user.getAccessToken();

        if (token == null) {
            webix.message("Authorization has been denied for this request.", "error");
            user.logout();
            document.location.reload();
        }

        var dvcs = user.getDvcsID();

        var language = user.getLanguage();

        var bear = token + ";" + dvcs + ";" + language;

        headers["Authorization"] = "Bear " + bear;
    });

    webix.attachEvent("onLoadError", function (text, xml, ajax, owner) {
        console.log(text);
        console.log(xml);
        console.log(ajax);
        console.log(owner);

        if (text != undefined) {
            //console.log(text);
            var json = JSON.parse(text);

            if (json.Message == "Authorization has been denied for this request.") {
                webix.message(json.Message, "error");
                user.logout();
                document.location.reload();
            }
        }
    });

    // webix.ui({
    // 	view: "popup",
    // 	id: "messagePopup",
    // 	// width: 300,
    // 	// padding: 0,
    // 	// css: "list_popup",
    // 	body: {
    // 		//type: "clean",
    // 		//borderless: true,
    // 		// rows: [
    // 		// 	{
    // 		id: "notice",
    // 		view: "list",
    // 		//autoheight: true,
    // 		url: {
    // 			$proxy: true,
    // 			load: function (view, callback, params) {
    // 				var url = apiHost + "/System/GetNotification";

    // 				webix.ajax().bind(view)
    // 					.headers({ 'Content-type': 'application/json' })
    // 					.get(url, callback);
    // 			}
    // 		},
    // 		// data: [
    // 		// 	{ id: 1, version: "3.1.0.0", update: "Bản cập nhật 3.1.0.0", personId: 1 },
    // 		// 	{ id: 2, version: "3.2.0.0", update: "Bản cập nhật 3.2.0.0", personId: 2 },
    // 		// 	{ id: 3, version: "3.3.0.0", update: "Bản cập nhật 3.3.0.0", personId: 2 },
    // 		// 	{ id: 4, version: "3.4.0.0", update: "Bản cập nhật 3.4.0.0", personId: 1 },
    // 		// 	{ id: 5, version: "3.5.0.0", update: "Bản cập nhật 3.5.0.0", personId: 1 }
    // 		// ],
    // 		template: "#version# - #inv_describe#",
    // 		autoheight: true,
    // 		select: true,
    // 		on: {
    // 			onItemDblClick: function (id) {
    // 				var win = $$("app:menu").$scope.ui(notification.$ui);
    // 				var item = this.getSelectedItem();
    // 				notification.initUI(item);
    // 				win.show();
    // 				// if (id == "1") {
    // 				// 	// app.router("/updateToWeb");
    // 				// 	webix.message("aksjflka", "debug");
    // 				// }
    // 			}
    // 		}
    // 		// type: {
    // 		// 	height: 45,
    // 		// 	// template: function (obj) {
    // 		// 	// 	html += "<div class='text'><div class='name'>" + obj.ten_kh + "<div class='time'>" + obj.ten_nguoi_sd + "</div></div>";
    // 		// 	// 	html += text + "</div>";
    // 		// 	// 	return html;
    // 		// 	// }
    // 		// 	template: "<div style='height:100%;line-height: 22px;float:left'>#version#<br /><i style='color:red'> #update#</i></div>"
    // 		// 	//template: "	<img class='photo' src='assets/imgs/photos/#personId#.png' /><span class='text'>#ten_kh#</span><span class='name'>#ten_nguoi_sd#</span>"

    // 		// }
    // 		// },
    // 		// 	{
    // 		// 		css: "show_all", template: "Show all messages <span class='webix_icon fa-angle-double-right'></span>", height: 40
    // 		// 	}
    // 		// ]

    // 	}
    // });

    //Top toolbar
    var mainToolbar = {
        css: { "background-color": "#005EB2 !important","width": "20% !important" },
        view: "toolbar",
        elements: [
            {
                height: 40,
                id: "person_template",
                css: "header_person",
                view: "template",
                borderless: true,
                url: {
                    $proxy: true,
                    load: function (view, callback, params) {
                        var url = apiHost + "/System/ExecuteCommand";

                        var parameters = {};

                        parameters.command = "CM00010";
                        parameters.parameter = {
                            code: user.getDvcsID(),
                        };

                        webix.ajax().bind(view).headers({ "Content-type": "application/json" }).post(url, parameters, callback);
                    },
                },

                template: function (obj) {
                    var html =
                        "<a class='bell-notify' style='float: left;padding: 0px 16px;' onclick='webix.$$(\"notifyPopup\").show(this)'><i class='fas fa-bell size-bell-notify' style='font-size: 24px;'></i></div></a>";
                    html += "<div style='height:100%;float:left;' onclick='webix.$$(\"profilePopup\").show(this)'>";
                    // html += "<span><i class='fas fa-user-circle fa-3x'></i></span><span class='name' >" + user.getCurrentUser() + "</span>";
                    // html += "<span class='webix_icon mdi mdi-chevron-down'></span></div>";
                    html += "<span data-toggle='tooltip' data-placement='top' title='" + user.getCurrentUser() + "'><i class='fas fa-user-circle fa-3x'></i></span>";
                    html += "</div>";
                    html += "<div class='mainToolbar' ><b>" + obj.name + "</b><br /><i>MST: " + obj.tax_code + "</i></div>";
                    return html;
                },
            },
            // {},
            // { view: "icon", id: "mess", icon: "far fa-bell", value: 2, width: 45, popup: "messagePopup" },
            // { width: 10 }
            //{width:100},
            // {
            // 	id: "version",
            // 	borderless: true,
            // 	view: "template",
            // 	template: "<div style='height:100%;line-height: 22px;float:right'>Copyright © SCTV " + new Date().getFullYear() + "<br /><i style='float:right'>Version: #Version#</i></div>",
            // 	data: version.getVersion()
            // }
            //  var detailForm = this.$scope.ui(hoanthanhform.$ui);
        ],
    };

    var body = {
        rows: [
            { height: 2 },
            {
                id: "head-title",
                css: { "background-color": "#f5f5f5 !important" },
                hidden: true,
                autowidth: true,
                cols: [
                    {
                        height: 30,
                        id: "title",
                        css: "title",
                        width: 360,
                        view: "template",
                        // hidden: true,
                        //css: { "box-shadow": "0 -2px #3498db inset !important" , "background":"#ffffff !important"},
                        //template: "<div class='header'>#title#</div><div class='details'>( #details# )</div>",
                        template: "<span><div class='header'>#title#</div><div class='details'></div></span>",
                        data: { text: "", title: "Trang chủ", details: "Phân tích và thống kê" },
                    },
                ],
            },
            {
                view: "scrollview",
                //scroll: "native-y",
                css: "bodycolor",
                body: {
                    cols: [{ $subview: true }],
                },
            },
        ],
    };

    var layout = {
        rows: [
            {
                // css:{"display":"inline-block !important", "background": "#f5f5f5 !important"},
                cols: [
                    {
                        view: "label",
                        css: "image_toolbar",
                        label: "<a href='https://www.sctv.com.vn/' target='_blank'><img class='photo' style='margin-left: 10px;' src='assets/imgs/LogoAppMinvoice.jpg' /></a>",
                        width:150,
                    },
                    menuBar,
                    mainToolbar,
                ],
            },
            body,
        ],
        // rows: [
        // 	mainToolbar,
        // 	{
        // 		cols: [
        // 			menuBar,
        // 			{ view: "resizer" },
        // 			body
        // 		]
        // 	}
        // ]
    };

    return {
        $ui: layout,
        $menu: "app:menu",
        $oninit: function (view, scope) {
            scope.ui(search.$ui);
            scope.ui(profile.$ui);
            scope.ui(notify.$ui);
            // scope.ui(notification.$ui);

            var js = version.getInfocompany();
            document.title = js.tax_code + " - " + js.name;
        },
    };
});
