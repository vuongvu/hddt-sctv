define(["app", "views/forms/duyethoadon", "locale", "views/modules/minvoiceplugin", "models/user"], function (app, dmwindowform, _, plugin, user) {
    "use strict";
    var windowno = app.path[1].params[0];
    var query = {
        command: "SYS002",
        parameter: {
            windowno: windowno,
        },
    };
    var url = app.config.host + "/System/ExecuteCommand";

    webix
        .ajax()
        .headers({ "Content-Type": "application/json" })
        .post(url, query, function (text) {
            var json = JSON.parse(text);
            if (json.length > 0) {
                if (webix.$$("title") != undefined) {
                    webix.$$("title").parse({ title: json[0]["name"], details: json[0]["details"] });
                    $$("head-title").show();
                }
            }
        });

    function signOnServerFile(data, cert, server_cert) {
        var parameters = {
            xml: data["InvoiceXmlData"],
            refUri: "#data",
            certSerial: cert.so_serial,
            tokenSerial: cert.tokenSerial,
            signId: "seller",
            signTag: "Signatures",
            pass: cert.pass,
            path: cert.path,
            inv_InvoiceAuth_id: data["inv_InvoiceAuth_id"],
        };

        webix
            .ajax()
            .headers({ "Content-Type": "application/json" })
            .post(server_cert + "/Token/SignXmlByFile", parameters, function (text) {
                var json = JSON.parse(text);

                if (json.hasOwnProperty("error")) {
                    webix.message(json["error"], "error");
                } else {
                    var parameters = {
                        InvoiceXmlData: json.xml,
                        inv_InvoiceAuth_id: json.inv_InvoiceAuth_id,
                    };
                    //console.log(parameters);

                    webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(app.config.host + "/Invoice/SaveXml", parameters, function (text) {
                            var result = JSON.parse(text);

                            if (result.trang_thai == "Đã ký") {
                                var table = webix.$$("dmwindowData");
                                table.clearAll();
                                table.load(table.config.url);

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(
                                        app.config.host + "/System/ExecuteCommand",
                                        {
                                            command: "CM00025",
                                            parameter: {},
                                        },
                                        function (text) {
                                            var json = JSON.parse(text);
                                            // auto send SMS
                                            if (json[1].gia_tri == "C") {
                                                var data = { id: item.inv_InvoiceAuth_id };

                                                webix
                                                    .ajax()
                                                    .headers({ "Content-type": "application/json" })
                                                    .post(app.config.host + "/Sms/NotifyInvoice", data, function (text) {
                                                        var data = JSON.parse(text);

                                                        if (!data.hasOwnProperty("error")) {
                                                        } else {
                                                            webix.message(data.error, "error");
                                                        }
                                                    });
                                            }
                                            // send email ma = 'AUTO_SEND_EMAIL'
                                            // if (json[0].gia_tri == "C") {
                                            //     var data = { inv_InvoiceAuth_id: item.inv_InvoiceAuth_id, nguoinhan: item.inv_buyerEmail, tieude: "Thông báo xuất hóa đơn điện tử" };

                                            //     webix.ajax()
                                            //         .headers({ 'Content-type': 'application/json' })
                                            //         .post(app.config.host + "/Invoice/AutoSendInvoiceByEmail", data, function (text) {
                                            //             var data = JSON.parse(text);

                                            //             if (!data.hasOwnProperty("error")) {

                                            //             }
                                            //             else {
                                            //                 webix.message(data.error, "error");
                                            //             }
                                            //         });
                                            // }
                                        }
                                    );
                                // webix.modalbox.hide();
                                // webix.message("Ký hóa đơn thành công !", "debug");
                            }
                        });
                }
            });
    }

    function signOnServer(data, cert, server_cert) {
        var parameters = {
            xml: data["InvoiceXmlData"],
            refUri: "#data",
            certSerial: cert.so_serial,
            tokenSerial: cert.tokenSerial,
            signId: "seller",
            signTag: "Signatures",
            pass: cert.pass,
            inv_InvoiceAuth_id: data["inv_InvoiceAuth_id"],
        };

        webix
            .ajax()
            .headers({ "Content-Type": "application/json" })
            .post(server_cert + "/Token/SignXml", parameters, function (text) {
                var json = JSON.parse(text);

                if (json.hasOwnProperty("error")) {
                    webix.message(json["error"]);
                } else {
                    var parameters = {
                        InvoiceXmlData: json.xml,
                        inv_InvoiceAuth_id: json.inv_InvoiceAuth_id,
                    };
                    //console.log(parameters);

                    webix
                        .ajax()
                        .headers({ "Content-Type": "application/json" })
                        .post(app.config.host + "/Invoice/SaveXml", parameters, function (text) {
                            var result = JSON.parse(text);

                            if (result.trang_thai == "Đã ký") {
                                var table = webix.$$("dmwindowData");
                                table.clearAll();
                                table.load(table.config.url);

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(
                                        app.config.host + "/System/ExecuteCommand",
                                        {
                                            command: "CM00025",
                                            parameter: {},
                                        },
                                        function (text) {
                                            var json = JSON.parse(text);
                                            // auto send SMS
                                            if (json[1].gia_tri == "C") {
                                                var data = { id: item.inv_InvoiceAuth_id };

                                                webix
                                                    .ajax()
                                                    .headers({ "Content-type": "application/json" })
                                                    .post(app.config.host + "/Sms/NotifyInvoice", data, function (text) {
                                                        var data = JSON.parse(text);

                                                        if (!data.hasOwnProperty("error")) {
                                                        } else {
                                                            webix.message(data.error, "error");
                                                        }
                                                    });
                                            }
                                            // send email ma = 'AUTO_SEND_EMAIL'
                                            // if(json[0].gia_tri == "C")
                                            // {
                                            // 	var data = { inv_InvoiceAuth_id: item.inv_InvoiceAuth_id, nguoinhan: item.inv_buyerEmail, tieude: "Thông báo xuất hóa đơn điện tử" };

                                            // 	webix.ajax()
                                            // 		.headers({ 'Content-type': 'application/json' })
                                            // 		.post(app.config.host + "/Invoice/AutoSendInvoiceByEmail", data, function (text) {
                                            // 			var data = JSON.parse(text);

                                            // 			if (!data.hasOwnProperty("error")) {

                                            // 			}
                                            // 			else {
                                            // 				webix.message(data.error, "error");
                                            // 			}
                                            // 		});
                                            // }
                                        }
                                    );

                                // webix.modalbox.hide();
                                // webix.message("Ký hóa đơn thành công !", "debug");
                            }
                        });
                }
            });
    }

    var op = [];

    // webix.$$("title").parse({ title: "Duyệt hóa đơn", details: "Danh sách hóa đơn chờ duyệt" });

    var controls = [
        {
            id: "showPop",
            view: "button",
            type: "icon",
            icon: "fa fa-eye",
            css: "webix_secondary",
            label: "Chi tiết",
            width: 100,

            //QuyenNH
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("duyethoadon-form");
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    dmwindowform.setEditMode(2);

                    item.invoice_issued_date = new Date(item.invoice_issued_date);

                    // item.total_amount_without_vat = item.total_amount_without_vat ? item.total_amount_without_vat : 0;
                    // item.total_amount_without_vat = webix.Number.format(item.total_amount_without_vat,{
                    //     groupDelimiter:",",
                    //     groupSize:3,
                    // });

                    // item.discount_percentage = item.discount_percentage ? item.discount_percentage : 0;
                    // item.discount_percentage = webix.Number.format(item.discount_percentage,{
                    //     groupDelimiter:",",
                    //     groupSize:3,
                    // });

                    // item.vat_amount = item.vat_amount ? item.vat_amount : 0;
                    // item.vat_amount = webix.Number.format(item.vat_amount,{
                    //     groupDelimiter:",",
                    //     groupSize:3,
                    // });

                    // item.total_amount = item.total_amount ? item.total_amount : 0;
                    // item.total_amount = webix.Number.format(item.total_amount,{
                    //     groupDelimiter:",",
                    //     groupSize:3,
                    // });

                    form.setValues(item);
                    detailForm.show();

                    // QuyenNH: đẩy dữ liệu lên modal detail
                    var tableDetail = $$("detail");
                    tableDetail.clearAll();
                    tableDetail.load(app.config.host + "/System/GetInvoiceDetail?id=" + item.inv_invoiceauth_id);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            css: "webix_secondary",
            label: _("DUYET") + " (F10)",
            width: 100,
            //css:"button_raised",
            shortcut: "F10",
            on: {
                onClick: function () {
                    var type = "01";

                    if (app.path.length > 0) {
                        if (typeof app.path[1].params[0] == "undefined") {
                            type = "01";
                        } else {
                            type = app.path[1].params[0];
                        }
                    }
                    if (type == "01") {
                        webix.confirm({
                            text: "Bạn có muốn duyệt hóa đơn không?",
                            ok: "Có",
                            cancel: "Không",
                            callback: function (res) {
                                if (res) {
                                    DuyetHoaDon();
                                }
                            },
                        });
                    } else if (type == "02") {
                        webix.confirm({
                            text: "Bạn có muốn sinh số hóa đơn không?",
                            ok: "Có",
                            cancel: "Không",
                            callback: function (res) {
                                if (res) {
                                    laySoHoaDon();
                                }
                            },
                        });
                    } else if (type == "03") {
                        var form = webix.$$("dmwindowData");
                        var datafrm = form.serialize();

                        var data = [];
                        for (var i = 0; i < datafrm.length; i++) {
                            if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true) {
                                data.push(datafrm[i].inv_invoiceauth_id);
                            }
                        }
                        if (data.length > 0) {
                            webix.confirm({
                                text: "Bạn có muốn ký hóa đơn không?",
                                ok: "Có",
                                cancel: "Không",
                                callback: function (res) {
                                    if (res) {
                                        webix
                                            .ajax()
                                            .headers({ "Content-type": "application/json" })
                                            .post(app.config.host + "/System/CheckInvoiceCancel", JSON.stringify(data), function (text) {
                                                var res = JSON.parse(text);
                                                if (res.hasOwnProperty("error")) {
                                                    webix.message({ type: "error", text: res.error });
                                                } else {
                                                    KyHoaDon();
                                                }
                                            });
                                    }
                                },
                            });
                        } else webix.message("Bạn chưa chọn hóa đơn để ký !");
                    }
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnReject",
            view: "button",
            type: "icon",
            icon: "fas fa-ban",
            css: "webix_secondary",
            label: _("TUCHOI") + " (F11)",
            width: 100,
            //css:"button_raised",
            shortcut: "F11",
            on: {
                onClick: function () {
                    webix.confirm({
                        text: "Bạn có chắc chắn từ chối hóa đơn không?",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                TuChoiHoaDon();
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnPrint",
            view: "button",
            type: "icon",
            icon: "fas fa-print",
            label: _("PRINT") + " (F7)",
            width: 80,
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = webix.$$("dmwindowData");
                    var item = table.getSelectedItem();

                    if (item == null) {
                        webix.message("Bạn chưa chọn hóa đơn");
                        return;
                    }

                    var url = app.config.host + "/Invoice/Preview?id=" + item.inv_invoiceauth_id;

                    var box = webix.modalbox({
                        title: "Đang in hóa đơn",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .get(url, {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                alert(text);
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);

                                webix.modalbox.hide(box);
                                window.open(fileURL, "_blank");
                            },
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnExportExcel",
            view: "button",
            type: "icon",
            icon: "fas fa-file-excel-o",
            css: "webix_secondary",
            label: "Kết xuất Excel",
            width: 100,
            on: {
                onClick: function () {
                    exportExcel();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var grid = {
        rows: [
            {
                id: "dmwindowData",
                view: "datatable",
                select: true,
                resizeColumn: true,
                datafetch: 100,
                url: app.config.host + "/Invoice/GetHoaDonChoDuyet",
                scheme: {
                    $init: function (obj) {
                        //creating fake dates for the sample
                        obj.date = new Date(obj.ngay_hd);
                    },
                },
                columns: [
                    {
                        id: "chon",
                        header: [{ text: "", content: "masterCheckbox" }],
                        width: 65,
                        checkValue: true,
                        uncheckValue: false,
                        template: "{common.checkbox()}",
                    },
                    {
                        id: "date",
                        header: [_("NGAY_HDON"), { content: "dateRangeFilter" }],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                    },
                    { id: "template_code", header: [_("MAU_HD"), { content: "textFilter" }], width: 150 },
                    { id: "invoice_series", header: [_("KY_HIEU"), { content: "textFilter" }], width: 120 },
                    { id: "invoice_number", header: [_("SO_HDON"), { content: "textFilter" }], width: 120 },
                    { id: "ten_dt", header: [_("TEN_DON_VI"), { content: "textFilter" }], width: 350 },
                    {
                        id: "total_amount_without_vat",
                        header: [_("TIEN_TRUOC_THUE"), { content: "numberFilter" }],
                        width: 170,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: { "text-align": "right" },
                    },
                    {
                        id: "discount_percentage",
                        header: [_("TIEN_CK"), { content: "numberFilter" }],
                        width: 150,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: { "text-align": "right" },
                    },
                    {
                        id: "vat_amount",
                        header: [_("TIEN_THUE"), { content: "numberFilter" }],
                        width: 150,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: { "text-align": "right" },
                    },
                    {
                        id: "total_amount",
                        header: [_("THANH_TIEN"), { content: "numberFilter" }],
                        width: 150,
                        format: webix.Number.numToStr({
                            groupDelimiter: ",",
                            groupSize: 3,
                            decimalDelimiter: ".",
                            decimalSize: 0,
                        }),
                        css: { "text-align": "right" },
                    },
                ],
                on: {
                    onItemDblClick: function (id, e, node) {
                        var showPopup = $$("showPop");
                        showPopup.callEvent("onClick");

                        // var table =  webix.$$("dmwindowData");
                        // var item = table.getSelectedItem();
                        // var template_code = item.template_code;
                    },
                    onItemClick: function (id, e, node) {
                        var table1 = webix.$$("dmwindowData");
                        var record = table1.getItem(id.row);
                        record["chon"] = !record["chon"];
                        table1.refresh(id.row);
                    },
                },
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                view: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    function exportExcel() {
        webix.toExcel($$("dmwindowData"), {
            rawValues: true,
            columns: [
                {
                    id: "date",
                    exportType: "date",
                    header: _("NGAY_HDON"),
                    width: 120,
                    exportFormat: "dd/MM/yyyy",
                },
                { id: "template_code", header: _("MAU_HD"), width: 150 },
                { id: "invoice_series", header: _("KY_HIEU"), width: 120 },
                {
                    id: "invoice_number",
                    header: _("SO_HDON"),
                    width: 120,
                },
                { id: "ten_dt", header: _("TEN_DON_VI"), width: 350 },
                {
                    id: "total_amount_without_vat",
                    header: _("TIEN_TRUOC_THUE"),
                    width: 120,
                    exportType: "number",
                    exportFormat: "#,##0",
                },
                {
                    id: "vat_amount",
                    header: _("TIEN_THUE"),
                    width: 120,
                    exportType: "number",
                    exportFormat: "#,##0",
                },
                {
                    id: "total_amount",
                    header: _("THANH_TIEN"),
                    width: 120,
                    exportType: "number",
                    exportFormat: "#,##0",
                },
            ],
        });
    }

    function KyHoaDon() {
        var table = webix.$$("dmwindowData");
        var kytaptrung = table.config.kytaptrung;
        var server_cert = table.config.server_cert;

        if (kytaptrung == "FILE") {
            var box = webix.modalbox({
                title: "Chọn chứng thư số",
                buttons: ["Nhận", "Hủy bỏ"],
                text: "<div id='ctnCerts'></div>",
                top: 100,
                width: 750,
                callback: function (result) {
                    switch (result) {
                        case "1":
                            // var form = webix.$$("dmwindowData");
                            // form.enable();
                            // form.hideProgress();

                            webix.modalbox.hide();
                            break;
                        case "0":
                            var table = $$("tblCerts");
                            var data = table.serialize();

                            var cert = null;

                            for (var i = 0; i < data.length; i++) {
                                if (data[i].chon == true) {
                                    cert = data[i];
                                    break;
                                }
                            }

                            if (cert == null) {
                                webix.message("Bạn chưa chọn chứng thư số");
                                return;
                            }

                            webix.modalbox.hide();

                            var wintable = webix.$$("dmwindowData");
                            var data = wintable.serialize();

                            webix.extend(wintable, webix.ProgressBar);
                            wintable.disable();
                            wintable.showProgress();

                            for (var i = 0; i < data.length; i++) {
                                var item = data[i];
                                if (item.chon != undefined) {
                                    if (item.chon == true) {
                                        var a = webix.ajax(app.config.host + "/Invoice/ExportXml?id=" + item.inv_InvoiceAuth_id);

                                        webix.promise
                                            .all([a])
                                            .then(function (results) {
                                                var a_result = results[0].json();
                                                signOnServerFile(a_result, cert, server_cert);
                                            })
                                            .fail(function (err) {
                                                var form = webix.$$("dmwindowData");
                                                form.enable();
                                                form.hideProgress();

                                                webix.alert({
                                                    title: "Lỗi !",
                                                    text: err,
                                                    type: "alert-error",
                                                });
                                            });
                                    }
                                }
                            }

                            var form = webix.$$("dmwindowData");
                            form.enable();
                            form.hideProgress();

                            break;
                    }
                },
            });

            var popupText = document.getElementsByClassName("webix_popup_text")[0];
            popupText.style.paddingTop = 0;

            var tblMatHang = webix.ui({
                container: "ctnCerts",
                id: "tblCerts",
                view: "datatable",
                borderless: true,
                // xmlData: a_result,
                server_cert: server_cert,
                height: 350,
                columns: [
                    {
                        id: "chon",
                        header: _("CHON"),
                        width: 65,
                        checkValue: true,
                        uncheckValue: false,
                        template: "{common.checkbox()}",
                    },
                    {
                        id: "cert_type",
                        header: _("Loại"),
                        width: 100,
                    },
                    {
                        id: "so_serial",
                        header: _("SO_SERIAL"),
                        width: 150,
                    },
                    {
                        id: "subjectname",
                        header: _("SUBJECTNAME"),
                        width: 450,
                    },
                    {
                        id: "ngaybatdau",
                        header: _("NGAY_BAT_DAU"),
                        width: 200,
                    },
                    {
                        id: "ngayketthuc",
                        header: _("NGAY_KET_THUC"),
                        width: 200,
                    },
                    {
                        id: "issuer",
                        header: _("DONVI"),
                        width: 450,
                    },
                ],
                url: app.config.host + "/Invoice/GetListCertificatesFile",
            });
        } else {
            if (kytaptrung == "C") {
                var box = webix.modalbox({
                    title: "Chọn chứng thư số",
                    buttons: ["Nhận", "Hủy bỏ"],
                    text: "<div id='ctnCerts'></div>",
                    top: 100,
                    width: 750,
                    callback: function (result) {
                        switch (result) {
                            case "1":
                                // var form = webix.$$("dmwindowData");
                                // form.enable();
                                // form.hideProgress();

                                webix.modalbox.hide();
                                break;
                            case "0":
                                var table = $$("tblCerts");
                                var data = table.serialize();

                                var cert = null;

                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].chon == true) {
                                        cert = data[i];
                                        break;
                                    }
                                }

                                if (cert == null) {
                                    webix.message("Bạn chưa chọn chứng thư số");
                                    return;
                                }

                                webix.modalbox.hide();

                                var wintable = webix.$$("dmwindowData");
                                var data = wintable.serialize();

                                webix.extend(wintable, webix.ProgressBar);
                                wintable.disable();
                                wintable.showProgress();

                                for (var i = 0; i < data.length; i++) {
                                    var item = data[i];
                                    if (item.chon != undefined) {
                                        if (item.chon == true) {
                                            var a = webix.ajax(app.config.host + "/Invoice/ExportXml?id=" + item.inv_InvoiceAuth_id);

                                            webix.promise
                                                .all([a])
                                                .then(function (results) {
                                                    var a_result = results[0].json();
                                                    signOnServer(a_result, cert, server_cert);
                                                })
                                                .fail(function (err) {
                                                    var form = webix.$$("dmwindowData");
                                                    form.enable();
                                                    form.hideProgress();

                                                    webix.alert({
                                                        title: "Lỗi !",
                                                        text: err,
                                                        type: "alert-error",
                                                    });
                                                });
                                        }
                                    }
                                }

                                var form = webix.$$("dmwindowData");
                                form.enable();
                                form.hideProgress();

                                break;
                        }
                    },
                });

                var popupText = document.getElementsByClassName("webix_popup_text")[0];
                popupText.style.paddingTop = 0;

                var tblMatHang = webix.ui({
                    container: "ctnCerts",
                    id: "tblCerts",
                    view: "datatable",
                    borderless: true,
                    // xmlData: a_result,
                    server_cert: server_cert,
                    height: 350,
                    columns: [
                        {
                            id: "chon",
                            header: _("CHON"),
                            width: 65,
                            checkValue: true,
                            uncheckValue: false,
                            template: "{common.checkbox()}",
                        },
                        {
                            id: "cert_type",
                            header: _("Loại"),
                            width: 100,
                        },
                        {
                            id: "so_serial",
                            header: _("SO_SERIAL"),
                            width: 150,
                        },
                        {
                            id: "subjectname",
                            header: _("SUBJECTNAME"),
                            width: 450,
                        },
                        {
                            id: "ngaybatdau",
                            header: _("NGAY_BAT_DAU"),
                            width: 200,
                        },
                        {
                            id: "ngayketthuc",
                            header: _("NGAY_KET_THUC"),
                            width: 200,
                        },
                        {
                            id: "issuer",
                            header: _("DONVI"),
                            width: 450,
                        },
                    ],
                    url: app.config.host + "/Invoice/GetListCertificates",
                });
            } else {
                webix.ajax().get(app.config.host + "/Invoice/GetListCertificates", function (text) {
                    var listcerts = JSON.parse(text);

                    if (listcerts.length == 0) {
                        webix.message({ type: "error", text: "Bạn chưa được phân quyền chữ ký số" });
                        return;
                    }

                    // Ký bằng Chữ ký số mềm
                    if (listcerts.length == 1) {
                        var certs = listcerts[0];

                        if (certs.cert_type == "FILE") {
                            console.log("Token File !");
                            var form = webix.$$("dmwindowData");
                            var datafrm = form.serialize();

                            var data = [];
                            for (var i = 0; i < datafrm.length; i++) {
                                if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true) {
                                    data.push(datafrm[i].inv_invoiceauth_id);
                                }
                            }

                            var postData = {
                                username: user.getCurrentUser(),
                                invs: data,
                                so_serial: certs.so_serial,
                            };

                            var url = app.config.host + "/Invoice/SignListInvoiceCertFile";

                            webix
                                .ajax()
                                .headers({ "Content-Type": "application/json" })
                                .post(url, postData, function (text) {
                                    var res = JSON.parse(text);

                                    if (res.hasOwnProperty("error")) {
                                        webix.message({ type: "error", text: res.error });
                                    } else {
                                        webix.message("Đã thực hiện xong");
                                        var table = $$("dmwindowData");
                                        table.clearAll();
                                        table.load(table.config.url);
                                    }
                                });
                            return;
                        }
                    }

                    // ký bằng USB TOKEN
                    console.log("USB Token !");
                    var form = webix.$$("dmwindowData");
                    var datafrm = form.serialize();

                    var data = [];
                    for (var i = 0; i < datafrm.length; i++) {
                        if (datafrm[i]["chon"] != null && datafrm[i]["chon"] == true) {
                            data.push(datafrm[i].inv_invoiceauth_id);
                        }
                    }

                    webix.extend(form, webix.ProgressBar);
                    form.disable();
                    form.showProgress();

                    var hub = plugin.hub();
                    var hubconnect = plugin.conn();
                    if (hubconnect.state != 1) {
                        var form = webix.$$("dmwindowData");
                        form.enable();
                        form.hideProgress();
                        webix.message("Quý khách hàng chưa cài Plugin ký hóa đơn hoặc Plugin ký hóa đơn chưa được bật !", "error");
                        return;
                    } else {
                        hub.off("resCommand");
                        hub.on("resCommand", function (result) {
                            var json = JSON.parse(result);
                            //console.log(json);
                            if (json.hasOwnProperty("error")) {
                                webix.message({ type: "error", text: json.error });

                                var form = webix.$$("dmwindowData");
                                form.enable();
                                form.hideProgress();
                            } else {
                                var table = webix.$$("dmwindowData");
                                table.clearAll();
                                table.load(table.config.url);

                                var form = webix.$$("dmwindowData");
                                form.enable();
                                form.hideProgress();

                                // webix.message("Ký thành công !", "success");
                            }
                        });

                        // hub.off('resNotice');
                        // hub.on('resNotice', function (result) {
                        //     webix.message(result, "debug");
                        // });

                        var token = user.getAccessToken();
                        var dvcs = user.getDvcsID();
                        var language = user.getLanguage();

                        var bear = token + ";" + dvcs + ";" + language;

                        var url;
                        if (window.location.protocol == "file:") {
                            url = "http://localhost:18374";
                        } else {
                            url = window.location.origin;
                        }

                        var arg = {
                            idData: "#data",
                            data: data,
                            // mst: "0100686209",
                            mst: "minvoice", // đang fix cứng.sau này có thể fix
                            username: user.getCurrentUser(),
                            idSigner: "seller",
                            tagSign: "Signatures",
                            listcerts: form.config.lstCert,
                            token: bear,
                            url: url,
                        };

                        hub.invoke("execCommand", "SignListInvoiceV2", JSON.stringify(arg))
                            .done(function () {
                                console.log("Success SignListInvoiceV2!");
                            })
                            .fail(function (error) {
                                var form = webix.$$("dmwindowData");
                                form.enable();
                                form.hideProgress();
                                webix.message(error, "error");
                            });
                    }
                });
            }
        }
    }

    function DuyetHoaDon() {
        var table = webix.$$("dmwindowData");
        var data = table.serialize();

        for (var i = 0; i < data.length; i++) {
            var item = data[i];

            if (item.chon != undefined) {
                if (item.chon == true) {
                    webix
                        .ajax()
                        .bind(item)
                        .sync()
                        .headers({ "Content-type": "application/json" })
                        .post(app.config.host + "/Invoice/DuyetHoaDon", { inv_InvoiceAuth_id: item.inv_invoiceauth_id }, function (text) {
                            var res = JSON.parse(text);
                            if (res.hasOwnProperty("error")) {
                                webix.message({ type: "error", text: res.error });
                                return;
                            }

                            $$("dmwindowData").remove(this.id);
                        });
                }
            }
        }

        table.clearAll();
        table.load(table.config.url);
    }

    function laySoHoaDon() {
        var table = webix.$$("dmwindowData");
        var data = table.serialize();
        var lstTemp = [];

        for (var i = 0; i < data.length; i++) {
            var item = data[i];

            if (item.chon != undefined) {
                if (item.chon == true) {
                    var temp = {
                        inv_invoiceauth_id: item.inv_invoiceauth_id,
                    };
                    lstTemp.push(temp);
                }
            }
        }

        if (lstTemp.length == 0) {
            webix.message({ type: "error", text: "Bạn chưa chọn hóa đơn" });
            return;
        }

        var postData = {
            data: lstTemp,
        };

        webix
            .ajax()
            .headers({ "Content-type": "application/json" })
            .post(app.config.host + "/Invoice/LaySoHoaDon", postData, function (text) {
                var res = JSON.parse(text);

                if (res.hasOwnProperty("error")) {
                    webix.message({ type: "error", text: res.error });
                } else {
                    var table = $$("dmwindowData");
                    table.clearAll();
                    table.load(table.config.url);
                }
            });
    }

    function TuChoiHoaDon() {
        var table = webix.$$("dmwindowData");
        var data = table.serialize();

        for (var i = 0; i < data.length; i++) {
            var item = data[i];

            if (item.chon != undefined) {
                if (item.chon == true) {
                    webix
                        .ajax()
                        .bind(item)
                        .sync()
                        .headers({ "Content-type": "application/json" })
                        .post(app.config.host + "/Invoice/TuChoiHoaDon", { inv_InvoiceAuth_id: item.inv_invoiceauth_id }, function (text) {
                            if (text.hasOwnProperty("error")) {
                                webix.message(text.error);
                                return;
                            }

                            $$("dmwindowData").remove(this.id);
                        });
                }
            }
        }

        table.clearAll();
        table.load(table.config.url);
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var params = app.path[1].params;
            var table = webix.$$("dmwindowData");
            // Lấy thông tin ký tập trung -----------------------------------------
            webix
                .ajax()
                .headers({ "Content-type": "application/json" })
                .post(
                    app.config.host + "/System/ExecuteCommand",
                    {
                        command: "CM00019",
                        parameter: {
                            ma_dvcs: "",
                        },
                    },
                    {
                        error: function (text, data, XmlHttpRequest) {
                            //webix.modalbox.hide(box);
                            webix.alert({
                                title: "Lỗi !",
                                text: err,
                                type: "alert-error",
                            });
                        },
                        success: function (text, data, XmlHttpRequest) {
                            var c_result = JSON.parse(text);

                            for (var i = 0; i < c_result.length; i++) {
                                var item = c_result[i];

                                if (item.ma == "KYTAPTRUNG") {
                                    table.config.kytaptrung = item.gia_tri;
                                }

                                if (item.ma == "SERVER_CERT") {
                                    table.config.server_cert = item.gia_tri;
                                }
                            }
                        },
                    }
                );

            // lấy danh sách chữ ký số
            webix
                .ajax()
                .headers({ "Content-type": "application/json" })
                .get(app.config.host + "/Invoice/GetListCertificates", {
                    error: function (text, data, XmlHttpRequest) {
                        //webix.modalbox.hide(box);
                        webix.alert({
                            title: "Lỗi !",
                            text: err,
                            type: "alert-error",
                        });
                    },
                    success: function (text, data, XmlHttpRequest) {
                        var result = JSON.parse(text);
                        table.config.lstCert = result;
                    },
                });

            table.define("url", app.config.host + "/Invoice/GetHoaDonChoDuyet?trangthai=Chờ duyệt");

            /*webix.$$("title").parse({ title: "Duyệt hóa đơn", details: "Danh sách hóa đơn chờ duyệt" });

            var btnEdit = webix.$$("btnEdit");
            btnEdit.define("label", _("DUYET"));

            if (params[0] != undefined) {
                var type = params[0];

                if (type == "02") {
                    webix.$$("title").parse({ title: "Ký hóa đơn", details: "Danh sách hóa đơn chờ ký" });
                    table.define("url", app.config.host + "/Invoice/GetHoaDonChoDuyet?trangthai=Chờ ký");
                    btnEdit.define("label", _("SIGN"));
                }
            }

            table.clearAll();
            table.load(table.config.url);
            btnEdit.refresh();*/

            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmwindowData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmwindow,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            var params = app.path[1].params;

            var table = webix.$$("dmwindowData");

            var btnEdit = webix.$$("btnEdit");
            btnEdit.define("label", _("DUYET"));

            var btnReject = webix.$$("btnReject");
            btnReject.show();
            //btnEdit.define("label", _("DUYET"));

            //webix.$$("title").parse({ title: "Duyệt hóa đơn", details: "Danh sách hóa đơn chờ duyệt" });

            if (params[0] != undefined) {
                var type = params[0];

                if (type == "01") {
                    //webix.$$("title").parse({ title: "Ký hóa đơn", details: "Danh sách hóa đơn chờ ký" });
                    table.define("url", app.config.host + "/Invoice/GetHoaDonChoDuyet?trangthai=Chờ duyệt");
                    btnEdit.define("label", _("DUYET"));
                    btnReject.hide();
                }
                if (type == "02") {
                    //webix.$$("title").parse({ title: "Ký hóa đơn", details: "Danh sách hóa đơn chờ ký" });
                    table.define("url", app.config.host + "/Invoice/GetHoaDonChoDuyet?trangthai=Chờ sinh số");
                    btnEdit.define("label", _("Sinh số"));
                    btnReject.hide();
                }
                if (type == "03") {
                    //webix.$$("title").parse({ title: "Ký hóa đơn", details: "Danh sách hóa đơn chờ ký" });
                    table.define("url", app.config.host + "/Invoice/GetHoaDonChoDuyet?trangthai=Chờ ký");
                    btnEdit.define("label", _("SIGN"));
                    btnReject.hide();
                }
            }

            table.clearAll();
            table.load(table.config.url);
            btnEdit.refresh();
        },
    };
});
