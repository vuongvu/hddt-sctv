define(["app", "locale", "models/user", "views/forms/68_kyhieuForm"], function (app, _, user, kyhieuForm) {
    "use strict";

    webix.$$("title").parse({
        title: "Quản lý ký hiệu hóa đơn sử dụng",
        details: "Quản lý ký hiệu hóa đơn sử dụng",
    });

    var initUI = function () {
        // var windowno = app.path[1].params[0];
        var table = webix.$$("kyhieu");
        // table.disable();
        // table.showProgress();
        // table.clearAll();
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var tlbparam_s = [];
                var url = app.config.host + "/Invoice68/GetDataquanlykyhieu68";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }
                var tlbParams = $$("paramDonvi");

                if (tlbParams !== undefined) {
                    var childViews = tlbParams.getChildViews();
                    var parameters = [];

                    for (var i = 0; i < childViews.length; i++) {
                        var child = childViews[i];
                        // console.log(child);
                        var id = child.config.id;
                        //var value_field = child.config.value_field;

                        var item = {
                            columnName: "code",
                            value: $$(id).getValue(),
                        };
                        parameters.push(item);
                    }
                    tlbparam_s = parameters;
                    url = url + "&tlbparam=" + JSON.stringify(parameters);
                } else {
                    url = url + "&tlbparam=null";
                }
                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var submenuChucNang = webix.ui({
        id: "submnchucnang",
        view: "submenu",
        width: 250,
        padding: 0,
        data: [
            {
                id: "UploadTemplate",
                value: "Tải mẫu lên",
            },
            {
                id: "SignTemplate",
                value: "Ký mẫu hóa đơn",
            },
            {
                id: "PreviewTemplate",
                value: "Xem mẫu hóa đơn (PDF)",
            },
            {
                id: "DownloadTemplate",
                value: "Tải mẫu thiết kế (.repx)",
            },
        ],
        on: {
            onItemClick: function (id, e, node) {
                this.hide();
                var selectedItem = webix.$$("congvieckinhdoanh").getSelectedItem();
                if (selectedselectedItem == null) {
                    webix.message("Bạn chưa chọn khách hàng !", "error");
                    return;
                }
            },
        },
    });

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(kyhieuForm.$ui);
                    kyhieuForm.setEditMode(1);
                    var params = null;

                    var tlbParams = $$("paramDonvi");

                    if (tlbParams !== undefined) {
                        var childViews = tlbParams.getChildViews();

                        var item = [];

                        for (var i = 0; i < childViews.length; i++) {
                            var child = childViews[i];
                            var id = child.config.id;
                            var value_field = child.config.value_field;

                            item[id] = $$(id).getValue();
                        }

                        params = item;
                    }
                    kyhieuForm.initUI(params);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn khách hàng ! ", "error");
                        return;
                    }
                    if (selectedItem.tt_sd > 0) {
                        webix.message("Ký hiệu đã sử dụng không thể sửa ! ", "error");
                        return;
                    }
                    var detailForm = this.$scope.ui(kyhieuForm.$ui);

                    var params = null;

                    var tlbParams = $$("paramDonvi");

                    if (tlbParams !== undefined) {
                        var childViews = tlbParams.getChildViews();

                        var item = [];

                        for (var i = 0; i < childViews.length; i++) {
                            var child = childViews[i];
                            var id = child.config.id;
                            var value_field = child.config.value_field;

                            item[id] = $$(id).getValue();
                        }

                        params = item;
                    }
                    kyhieuForm.initUI(params);

                    kyhieuForm.setEditMode(2);
                    kyhieuForm.setValues(selectedItem);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn ký hiệu hóa đơn cần xóa ! ", "error");
                        return;
                    }
                    if (selectedItem.tt_sd > 0) {
                        webix.message("Ký hiệu đã sử dụng không thể xóa ! ", "error");
                        return;
                    }
                    selectedItem.editmode = 3;
                    webix.confirm({
                        text: "Bạn có muốn xóa bản ghi",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/deletequanlykyhieu68", selectedItem, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.error, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            var res = JSON.parse(text);
                                            if (res.hasOwnProperty("error")) {
                                                webix.message(res.error, "error");
                                            } else {
                                                table.remove(selectedItem.id);
                                                table.enable();
                                                table.hideProgress();
                                            }
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-print fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("PRINT") + " (F7)" + "</span>",
            id: "btnPrint",
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = webix.$$("kyhieu");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn hóa đơn cần in !", "error");
                        return;
                    }
                    //webix.message("Chưa có mẫu in ...", "error");
                    // var rpParameter = null;
                    var box = webix.modalbox({
                        title: "Đang in hóa đơn ....",
                        text: "<p ><i style='font-size:450%;' class='fa fa-spin fa-spinner'></i></p>",
                        width: 350,
                        height: 200,
                    });

                    var newWin = window.open();
                    var html =
                        "<html><head><title>Hóa đơn điện tử SCTV</title>" +
                        '<link rel="stylesheet" href="assets/fontawesome_pro/css/all.css" type="text/css" charset="utf-8">' +
                        '<link rel="stylesheet" href="assets/fontawesome.css" type="text/css" charset="utf-8">' +
                        '</head><body><center>Đang tải dữ liệu ... <br><p><i style="font-size:200%;" class="fa fa-spin fa-spinner"></i></p></center></body></html>';
                    newWin.document.write(html);

                    webix
                        .ajax()
                        .response("arraybuffer")
                        .headers({ "Content-type": "application/json" })
                        .get(app.config.host + "/Invoice68/inMauHoadon?id=" + item.id, {
                            error: function (text, data, XmlHttpRequest) {
                                webix.modalbox.hide(box);
                                webix.message(XmlHttpRequest.statusText, "error");
                            },
                            success: function (text, data, XmlHttpRequest) {
                                var file = new Blob([data], { type: "application/pdf" });
                                var fileURL = URL.createObjectURL(file);
                                //window.open(fileURL, '_blank');
                                webix.modalbox.hide(box);
                                newWin.location = fileURL;
                            },
                        });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        // {
        //     id: "btnChucNang",
        //     view: "button",
        //     type: "icon",
        //     icon: "fa fa-cube",
        //     label: "Chức năng",
        //     width: 100,
        //     css: "button_raised",
        //     click: function () {
        //         submenuChucNang.show(webix.$$("btnChucNang").$view);
        //     },
        // },
        // {
        //     view: "button",
        //     id: "btnRegister",
        //     type: "icon",
        //     icon: "plus",
        //     label: "Đăng ký",
        //     width: 100,
        //     shortcut: "F1",
        //     on: {
        //         onClick: function () {
        //             var detailForm = this.$scope.ui(dangkyform.$ui);
        //             dangkyform.setEditMode(0);
        //             detailForm.show();
        //         },
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     },
        // },
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "kyhieu",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function (obj) {
                        // if (obj.ngayLap) {
                        //     obj.ngayLap = new Date(obj.ngayLap);
                        // }
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.index = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onBeforeLoad: function () {
                        this.showOverlay("<p><i style='font-size:450%;' class='fa fa-spin fa-spinner fa-pulse'></i></p>");
                    },
                    onAfterLoad: function (id) {
                        this.hideOverlay();
                    },
                },
                columns: [
                    {
                        id: "kyhieu_id",
                        header: "kyhieu_id",
                        hidden: true,
                    },
                    {
                        id: "index",
                        header: "STT",
                        sort: "int",
                        width: 40,
                        columnType: "numeric",
                    },
                    {
                        id: "tmau",
                        header: [
                            "Tên mẫu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "lhdon",
                        header: [
                            "Loại hóa đơn",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "hthuc",
                        header: [
                            "Hình thức",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "khdon",
                        header: [
                            "Kiểu hóa đơn",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "khhdon",
                        header: [
                            "Ký hiệu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "sdmau",
                        header: [
                            "Số dòng in mẫu",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        columnType: "numeric",
                    },
                    // {
                    //     id: "trangThai",
                    //     header: [
                    //         "Số dòng in mẫu",
                    //         {
                    //             content: "textFilter",
                    //         },
                    //     ],
                    //     width: 120,
                    //     template: function (obj) {
                    //         return (
                    //             "<span class='tt_kyhieu_status" +
                    //             obj.trangThai +
                    //             "'>" +
                    //             obj.trangThai +
                    //             "</span>"
                    //         );
                    //     }
                    // },
                    {
                        id: "nlap",
                        header: [
                            "Ngày lập",
                            {
                                content: "serverDateRangeFilter",
                            },
                        ],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        columnType: "datetime",
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "nglap",
                        header: [
                            "Người lập",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                    },
                    {
                        id: "tt_sdung",
                        header: ["Sử dụng"],
                        editor: "checkbox",
                        width: 90,
                        template: function (obj) {
                            var checked = obj.tt_sd > 0 ? 'checked="true"' : "";
                            return "<input disabled class='webix_table_checkbox' type='checkbox' " + checked + ">";
                        },
                        css: {
                            "text-align": "center",
                        },
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                id: "paramDonvi",
                view: "toolbar",
                css: { "background-color": "#f5f5f5 !important" },
                borderless: true,
                height: 35,
                cols: [
                    {
                        id: "code",
                        view: "combo",
                        width: 500,
                        label: _("Đơn vị"),
                        options: {
                            body: {
                                //comboId: comp.id,
                                // isDefault: item.has_default,
                                url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00195",
                                ready: function () {
                                    // if (this.config.isDefault == "C") {
                                    var count = this.count();

                                    if (count > 0) {
                                        var combo = webix.$$("code");
                                        combo.setValue(this.getFirstId());
                                    }
                                    // }
                                },
                            },
                        },
                        // value_field: item.value_field,
                        // isDefault: item.has_default,
                        on: {
                            onChange: function (newv, oldv) {
                                var table = $$("kyhieu");
                                table.clearAll();
                                table.load(table.config.url);
                            },
                        },
                    },
                ],
            },
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                // height: 40,
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };

    function loadData() {
        var table = webix.$$("kyhieu");
        table.clearAll();
        table.load(table.config.url);
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            var table = webix.$$("kyhieu");
            webix.extend(view, webix.ProgressBar);

            $scope.on(app, "dataChangedKyhieu68", function (mode, item) {
                table.clearAll();
                table.load(table.config.url);
            });

            $scope.on(app, "filterTable", function (report_id, infoWindowId, item) {
                var infoparam = {
                    infowindow_id: infoWindowId,
                    parameter: item,
                };

                table.config.infoparam = infoparam;
                table.clearAll();
                table.load(table.config.url);
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
