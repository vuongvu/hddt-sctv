define(["app",
        "locale",
        "views/forms/register_licence_branch"
    ],function(app,_,dmwindowform){

        var itemValue;
	
        function initUI(){
		
            // call API kiểm tra đơn vị này có được đăng ký không?
            var table = webix.$$("treewindowDataRegister");
            var url = {
                $proxy: true,
                load: function (view, callback, params) {
                    var value = itemValue.code;
                    webix.ajax()
                    .headers({ 'Content-type': 'application/json' })
                    .post(app.config.host + "/System/CheckBrandValidation?value=" + value, function (res) {
                        // res = true;
                        if (res == "false") {
                            webix.message({ type: "error", text: "Đơn vị không thể đăng ký" });
                            webix.$$("dangkylicense-win").close();
                        }
                        else {
                            // call API get danh sách đã đăng ký của đơn vị
                            var url = app.config.host + "/System/ExecuteCommand";
                            webix.ajax()
                                .headers({'Content-type':'application/json'})
                                .post(url,
                                    {
                                        command:"CM00097",
                                        parameter:{
                                            branch_id: itemValue.id
                                        }
                                    }
                                    ,function(text){
                                var json = JSON.parse(text);
                                
                                var data = listToTree(json, { idKey: "id", parentId: "parent_id" });
                                
                                var tblData = $$("treewindowDataRegister");
                    
                                tblData.parse(data,"json");
                                tblData.openAll();
                                
                            });
                        }
                    });
                }
            }
            table.define("url", url);
          
        }

    var controls = [
        // {
        //     id: "btnRefreshRegister",
        //     view: "button",
        //     type: "icon",
        //     icon: "fas fa-refresh",
        //     label: _("REFRESH"),
        //     css: "webix_secondary",
        //     width: 95,
        //     on: {
        //         onClick: function () {
                   
        //         }
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     }
        // },
        {
            id: "btnPlusRegister",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: "Thêm mới",
            css: "webix_secondary",
            width: 100,
            //css:"button_raised",
            shortcut: "F4",
            on: {
                onClick: function (item) {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("register_licence-form");
                   
                    var branchCode = itemValue.code;

                    dmwindowform.setEditMode(1);
                    detailForm.show();

                    var cb=$$("cbBranchCode");
                    cb.define("readonly",true);
                    cb.setValue(branchCode);
                    cb.refresh();
                },
            },
            click: function () {
                this.callEvent("onClick");
            }
        },
        {
            id: "btnEditRegister",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            css: "webix_secondary",
            label: "Cập nhật",
            width: 100,
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmwindowform.$ui);
                    var form = webix.$$("register_licence-form");

                    var table = webix.$$("treewindowDataRegister");
                    var item = table.getSelectedItem();
                    item.createdondate = new Date(item.createdondate);
                    form.setValues(item);        
                    dmwindowform.setEditMode(2);

                    detailForm.show();
                }, 
            },
            click: function () {
                this.callEvent("onClick");
            },
            
        },
       
        {},
    ]

    var grid = {
        rows: [
            {
                id: "treewindowDataRegister",
                view: "treetable",
                select: true,
                resizeColumn: true,
                scheme: {
                    $change: function (item) {
                        if (item.$count > 1980)
                            item.$css = "highlight";
                    },
                    $init: function (obj) {

                    },
                },
                
 
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEditRegister = $$("btnEditRegister");
                        btnEditRegister.callEvent("onClick");
                    },
                },
                //url: app.config.host + "/api/System/GetDataByWindowNo?window_id="+windowno ,
                columns: [
                    {
                        id: "stt",
                        header: "STT",
                        sort: "int",
                        width: 40,
                    },
                    {
                        id: "branch_name",
                        header: "Tên khách hàng/ đơn vị",
                        width: 300,
                    },
                    {
                        id: "quantity",
                        header: "Số lượng (hóa đơn)",
                        width: 200,
                    },
                    {
                        id: "createdondate",
                        header: "Ngày đăng ký",
                        width: 200,
                        format: function (text) {
                            var format =  webix.Date.dateToStr("%d/%m/%Y %H:%i:%s");
                            text = removeTimeZoneT(text);
                            return format(text);
                        }
                    },
                ],
                data: [
                           
                ],
                filterMode:{
                    level:false,
                    showSubItems:false
                }
            }
        ]
    }

	return {	
        // $ui: layout,	
		$ui:{   
			view:"window", modal:true, id:"dangkylicense-win", position:"center",
			head:"Đăng ký licence cho khách hàng",
			fullscreen:true,
			body:{
                rows: [
                    {
                        id: "tlbMain",
                        height: 35,
                        type: "toolbar",
                        cols: controls
                    },
                    {
                        rows: [
                            grid
                        ]
                    },
                    { 
                        view:"button", shortcut:"esc", label:"Hủy bỏ (ESC)",type:"danger",align:"center",
                        width:100,
                        on:{
                            onClick: function(){
                                this.getTopParentView().close();
                            }
                        },
                        click:function(){
                            this.callEvent("onClick");
                        }
                    }
                ]
			}			
			
		},	
        
        setItemValue: function (item) {
			itemValue = item;
            initUI();
		}
        
	};

});