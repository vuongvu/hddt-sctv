define(["app", "locale", "models/user", "views/forms/68_tracuuForm"], function (app, _, user, formmaster) {
    "use strict";

    var initUI = function () {
        var table = webix.$$("viewtable");
        var url = {
            $proxy: true,
            load: function (view, callback, params) {
                var tlbparam_s = [];
                var url = app.config.host + "/Invoice68/GetDataTraCuu";

                if (params == null) {
                    url = url + "?start=0&count=" + view.config.datafetch + "&continue=null&filter=null";
                } else {
                    url = url + "?start=" + params.start + "&count=" + params.count + "&continue=" + params.continue;

                    if (params.filter != null) {
                        var array = [];

                        for (var property in params.filter) {
                            var column = view.getColumnConfig(property);

                            var item = {
                                columnName: property,
                                columnType: column.columnType,
                                value: params.filter[property],
                            };

                            array.push(item);
                        }

                        //console.log(JSON.stringify(array));
                        url = url + "&filter=" + JSON.stringify(array);
                    }
                }

                webix.ajax(url, callback, view);
            },
        };
        table.define("url", url);
    };

    var controls = [
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-refresh fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("REFRESH") + "</span>",
            id: "btnRefresh",
            on: {
                onClick: function () {
                    var table = webix.$$("viewtable");
                    table.clearAll();
                    table.load(table.config.url);
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-plus fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("THEM") + " (F4)" + "</span>",
            id: "btnPlus",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(formmaster.$ui);
                    formmaster.setEditMode(1);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-edit fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("SUA") + " (F3)" + "</span>",
            id: "btnEdit",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var table = webix.$$("viewtable");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn khách hàng ! ", "error");
                        return;
                    }
                    var detailForm = this.$scope.ui(formmaster.$ui);
                    formmaster.setEditMode(2);
                    formmaster.setValues(selectedItem);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 100,
            label: '<i class="fa fa-trash fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + _("XOA") + " (F8)" + "</span>",
            id: "btnDelete",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("viewtable");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn ký hiệu hóa đơn cần xóa ! ", "error");
                        return;
                    }

                    selectedItem.editmode = 3;
                    webix.confirm({
                        text: "Bạn có muốn xóa bản ghi",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/UpdateCreatTraCuu", selectedItem, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.error, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            var res = JSON.parse(text);
                                            if (res.hasOwnProperty("error")) {
                                                webix.message(res.error, "error");
                                                table.enable();
                                                table.hideProgress();
                                            } else {
                                                table.clearAll();
                                                table.load(table.config.url);
                                                table.enable();
                                                table.hideProgress();
                                            }
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            view: "button",
            type: "htmlbutton",
            width: 150,
            label: '<i class="fa fa-undo fa-lg" aria-hidden="true"></i> ' + '<span class="text">' + "Reset password" + "</span>",
            id: "btnReset",
            shortcut: "F7",
            on: {
                onClick: function () {
                    var table = webix.$$("viewtable");
                    var selectedItem = table.getSelectedItem();
                    if (selectedItem == null) {
                        webix.message("Bạn chưa chọn ký hiệu hóa đơn cần xóa ! ", "error");
                        return;
                    }

                    selectedItem.editmode = 4;
                    webix.confirm({
                        text: "Reset lại mật khẩu của tài khoản này",
                        ok: "Có",
                        cancel: "Không",
                        callback: function (choose) {
                            if (choose) {
                                webix.extend(table, webix.ProgressBar);
                                table.disable();
                                table.showProgress();

                                webix
                                    .ajax()
                                    .headers({ "Content-type": "application/json" })
                                    .post(app.config.host + "/Invoice68/UpdateCreatTraCuu", selectedItem, {
                                        error: function (text, data, XmlHttpRequest) {
                                            table.enable();
                                            table.hideProgress();
                                            var json = JSON.parse(text);
                                            webix.message(json.error, "error");
                                            //webix.message(XmlHttpRequest.statusText, "error");
                                        },
                                        success: function (text, data, XmlHttpRequest) {
                                            var res = JSON.parse(text);
                                            if (res.hasOwnProperty("error")) {
                                                webix.message(res.error, "error");
                                                table.enable();
                                                table.hideProgress();
                                            } else {
                                                table.clearAll();
                                                table.load(table.config.url);
                                                table.enable();
                                                table.hideProgress();
                                            }
                                        },
                                    });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
    ];

    var table = {
        margin: 0,
        rows: [
            {
                id: "viewtable",
                view: "datatable",
                select: true,
                resizeColumn: true,
                footer: false,
                editable: false,
                tooltip: true,
                scheme: {
                    $init: function (obj) {
                        // if (obj.ngayLap) {
                        //     obj.ngayLap = new Date(obj.ngayLap);
                        // }
                    },
                },
                on: {
                    "data->onStoreUpdated": function () {
                        this.data.each(function (obj, i) {
                            obj.index = i + 1;
                        });
                    },
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                    onBeforeLoad: function () {
                        this.showOverlay("<p><i style='font-size:450%;' class='fa fa-spin fa-spinner fa-pulse'></i></p>");
                    },
                    onAfterLoad: function (id) {
                        this.hideOverlay();
                    },
                },
                columns: [
                    {
                        id: "inv_user_id",
                        header: "inv_user_id",
                        hidden: true,
                    },
                    {
                        id: "index",
                        header: "STT",
                        sort: "int",
                        width: 40,
                        columnType: "numeric",
                    },
                    {
                        id: "username",
                        header: [
                            "Mã số thuế",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 200,
                    },
                    {
                        id: "fullname",
                        header: [
                            "Tên khách hàng",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 200,
                    },
                    {
                        id: "email",
                        header: [
                            "Email",
                            {
                                content: "serverFilter",
                            },
                        ],
                        width: 120,
                        css: {
                            "text-align": "center",
                        },
                    },
                    {
                        id: "active_str",
                        header: [
                            "Trạng thái tài khoản",
                            {
                                content: "textFilter",
                            },
                        ],
                        width: 120,
                        template: "<span class='mcls_status#active#'>#active_str#</span>",
                    },
                    {
                        id: "date_new",
                        header: [
                            "Ngày lập",
                            {
                                content: "serverDateRangeFilter",
                            },
                        ],
                        width: 150,
                        format: function (text) {
                            var format = webix.Date.dateToStr("%d/%m/%Y");
                            text = removeTimeZoneT(text);
                            return format(text);
                        },
                        columnType: "datetime",
                        css: {
                            "text-align": "center",
                        },
                    },
                ],
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
                container: "paging_here",
                size: 50,
                group: 5,
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 39,
                css: "btnControl",
                margin: 0,
                borderless: true,
                type: "toolbar",
                // height: 40,
                cols: controls,
            },
            {
                rows: [table],
            },
        ],
    };
    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            webix.$$("title").parse({
                title: "Quản lý tài khoản tra cứu",
                details: "Quản lý tài khoản tra cứu",
            });
        },
        $onurlchange: function (ui, name, url, scope) {
            initUI();
        },
    };
});
