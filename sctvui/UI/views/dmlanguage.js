define(["app", "views/forms/dmlanguage", "locale"], function (app, dmtabform, _) {
    "use strict";

    //var windowno = app.path[1].params[0];

    var controls = [
        // {
        //     id: "btnRefresh",
        //     view: "button",
        //     type: "icon",
        //     icon: "refresh",
        //     label: _("REFRESH"),
        //     width: 90,
        //     // shortcut: "F4",
        //     //css:"button_raised",
        //     on: {
        //         onClick: function () {
        //             var table = webix.$$("dmtabData");
        //             table.clearAll();

        //             table.load(loadData());
        //         }
        //     },
        //     click: function () {
        //         this.callEvent("onClick");
        //     }
        // },
        {
            id: "btnPlus",
            view: "button",
            type: "icon",
            icon: "fas fa-plus",
            label: _("THEM") + " (F4)",
            width: 90,
            css: "webix_secondary",
            shortcut: "F4",
            on: {
                onClick: function () {
                    var win = this.$scope.ui(dmtabform.$ui);
                    var form = webix.$$("dmtab-form");

                    if (webix.$$("txtWindowId").getValue() == "") {
                        webix.message("Bạn chưa chọn loại ngôn ngữ !", "error");
                        return;
                    }

                    var item = {
                        language_code: $$("txtWindowId").getValue(),
                    };

                    form.setValues(item);

                    dmtabform.setEditMode(1);
                    win.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnEdit",
            view: "button",
            type: "icon",
            icon: "fas fa-edit",
            label: "Sửa (F3)",
            width: 80,
            css: "webix_secondary",
            shortcut: "F3",
            on: {
                onClick: function () {
                    var detailForm = this.$scope.ui(dmtabform.$ui);
                    var form = webix.$$("dmtab-form");

                    var table = webix.$$("dmtabData");
                    var item = table.getSelectedItem();
                    if (item == null || item == undefined) {
                        webix.message("Bạn chưa chọn dòng cần sửa ! ", "error");
                        return;
                    }
                    form.setValues(item);

                    dmtabform.setEditMode(2);
                    detailForm.show();
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {
            id: "btnDelete",
            view: "button",
            type: "icon",
            icon: "fas fa-trash-o",
            label: "Xóa (F8)",
            width: 80,
            css: "webix_secondary",
            shortcut: "F8",
            on: {
                onClick: function () {
                    var table = webix.$$("dmtabData");
                    var item = table.getSelectedItem();

                    webix.confirm({
                        text: "Bạn có muốn xóa " + item.key_lang,
                        ok: "Có",
                        cancel: "Không",
                        callback: function (res) {
                            if (res) {
                                webix.ajax().post(app.config.host + "/System/DeleteLanguages", item, function (text, xml, xhr) {
                                    if (text.hasOwnProperty("error")) {
                                        webix.message(text.error);
                                        return;
                                    }

                                    table.remove(item.id);
                                });
                            }
                        },
                    });
                },
            },
            click: function () {
                this.callEvent("onClick");
            },
        },
        {},
        {
            view: "combo",
            width: 300,
            label: "Ngôn ngữ",
            id: "txtWindowId",
            // options: ["en", "vi"],
            suggest: {
                // body: {
                //     url: app.config.host + "/System/GetDataReferencesByRefId?refId=RF00012"
                // }
                data: [
                    { id: "vi", value: "Việt Nam" },
                    { id: "en", value: "English" },
                    { id: "ko", value: "Korea" },
                ],
            },
            on: {
                onChange: function () {
                    loadData();
                },
            },
        },
    ];

    var grid = {
        rows: [
            {
                id: "dmtabData",
                view: "datatable",
                select: true,
                datafetch: 50,
                loadahead: 15,
                on: {
                    onItemDblClick: function (id, e, node) {
                        var btnEdit = $$("btnEdit");
                        btnEdit.callEvent("onClick");
                    },
                },
                columns: [
                    { id: "wb_lang_id", header: "wb_lang_id", hidden: true },
                    { id: "language_code", header: "LANGUAGES", hidden: true },
                    { id: "key_lang", header: ["Key", { content: "textFilter" }], width: 200 },
                    { id: "value_lang", header: ["Value", { content: "textFilter" }], fillspace: true },
                ],
                //columns: columns
                pager: "pagerA",
            },
            {
                id: "pagerA",
                view: "pager",
                size: 50,
                group: 5,
                template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            },
        ],
    };

    var layout = {
        rows: [
            {
                height: 35,
                type: "toolbar",
                cols: controls,
            },
            {
                rows: [grid],
            },
        ],
    };

    function loadData() {
        var windowId = webix.$$("txtWindowId").getValue();
        var url = app.config.host + "/System/GetLanguagesByMa?lang=" + windowId;

        webix.ajax().get(url, function (text) {
            var json = JSON.parse(text);

            var table = webix.$$("dmtabData");
            table.clearAll();
            table.parse(json);
        });
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            $scope.on(app, "dataChanged", function (mode, item) {
                var table = webix.$$("dmtabData");

                if (mode == 1) {
                    table.add(item);
                    //table.select(item.iddmtab,false);
                }

                if (mode == 2) {
                    var selectedItem = table.getSelectedItem();
                    table.updateItem(selectedItem.id, item);
                }
            });
        },
    };
});
