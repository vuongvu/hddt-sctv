define([

], function () {


    return {
        // findAndReplace: function findAndReplace(object, value, replacevalue) {
        //     for (var x in object) {
        //         if (object.hasOwnProperty(x)) {
        //             if (typeof object[x] == 'object') {
        //                 findAndReplace(object[x], value, replacevalue);
        //             }
        //             if (object[x] == value) {
        //                 object["name"] = replacevalue;
        //                 // break; // uncomment to stop after first replacement
        //             }
        //         }
        //     }
        // },
        getObjects: function getObjects(obj, key, val) {
            var objects = [];
            for (var i in obj) {
                // console.log(obj);
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    objects = objects.concat(getObjects(obj[i], key, val));
                } else
                    //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
                    if (i == key && obj[i] == val || i == key && val == '') { //
                        objects.push(obj);
                    } else if (obj[i] == val && key == '') {
                        //only add if the object is not already in the array
                        if (objects.lastIndexOf(obj) == -1) {
                            objects.push(obj);
                        }
                    }
            }
            return objects;
        },
        findAndReplace: function findAndReplace(object, value, item) {

            for (var x in object) {

                if (typeof object[x] == typeof {}) {
                    findAndReplace(object[x], value, item);
                }
                //nếu tìm thấy name bằng với name của item truyền vào trong object thì thay thế object[name] bằng item
                if (object[x] == value) {
                    //console.log(object);
                    webix.extend(object, item, true);
                    //object["name"] = value[i];
                    // object[element] = replacevalue;
                    //object["name"] = replacevalue;
                    // break; // uncomment to stop after first replacement
                }
            }
            return object;
        },
        //return an array of values that match on a certain key
        getValues: function getValues(obj, key) {
            var objects = [];
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    objects = objects.concat(getValues(obj[i], key));
                } else if (i == key) {
                    objects.push(obj[i]);
                }
            }
            return objects;
        },

        //return an array of keys that match on a certain value
        getKeys: function getKeys(obj, val) {
            var objects = [];
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    objects = objects.concat(getKeys(obj[i], val));
                } else if (obj[i] == val) {
                    objects.push(i);
                }
            }
            return objects;
        }

    };

});