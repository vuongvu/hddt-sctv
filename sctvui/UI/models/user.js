define([], function () {
    var user = null,
        credentials = {},
        oauthUrl = "";
    var host = "";
    var start = "";

    require(["app"], function (app) {
        host = app.config.host;
        start = app.config.start;
    });

    credentials = Cookies.getJSON("appCookie") || {};

    function saveSetting(key, value, reload) {
        if (current_user) {
            var id = current_user.id,
                settings = current_user.settings;

            if (!settings[key] || value != settings[key]) {
                if (reload) document.location.reload();
            }
        }
    }

    return {
        saveSetting: saveSetting,
        getDvcsID: function () {
            var cookie = this.getAppCookie();

            if (cookie == null) {
                return null;
            }

            return cookie.ma_dvcs;
        },
        getCurrentUser: function () {
            var cookie = this.getAppCookie();

            if (cookie == null) {
                return null;
            }

            return cookie.username;
        },
        getLanguage: function () {
            var cookie = this.getAppCookie();

            if (cookie == null) {
                return null;
            }

            return cookie.language;
        },
        getAccessToken: function () {
            var cookie = this.getAppCookie();

            if (cookie === null || cookie === undefined) {
                return null;
            }

            if (cookie.access_token === undefined) {
                return null;
            }

            if (cookie.expires_in + cookie.timestamp < Date.now() / 1000) {
                return null;
            }

            return cookie.access_token;
        },
        getCredentials: function () {
            return credentials;
        },
        getAppCookie: function () {
            var baseUrl = document.location.href;

            if (baseUrl.startsWith("file")) {
                var cookie = webix.storage.local.get("appCookie");
                return cookie;
            } else {
                var cookie = Cookies.getJSON("appCookie");
                return cookie;
            }
        },
        setCurrentUser: function (userId) {
            user = userId;
            return true;
        },
        login: function (username, password, ma_dvcs, language, component) {
            webix.extend(component, webix.ProgressBar);
            component.disable();
            component.showProgress();

            webix
                .ajax()
                .post(
                    host + "/Account/Login",
                    {
                        username: username,
                        password: password,
                        ma_dvcs: ma_dvcs,
                    },
                    function (text, data) {
                        var result = JSON.parse(text);

                        if (result.error != null) {
                            webix.message(result.error, "error");
                            component.enable();
                            component.hideProgress();
                            return;
                        }

                        webix.storage.local.remove("APP_CACHE");
                        webix.storage.local.put("APP_CACHE", {});

                        credentials = {
                            access_token: result.token,
                            expires_in: 3600,
                            token_type: "Bearer",
                            scope: null,
                            username: username,
                            language: language,
                            ma_dvcs: ma_dvcs,
                            wb_user_id: result.wb_user_id,
                        };

                        if (result.ma_dvcs != null) {
                            credentials.ma_dvcs = result.ma_dvcs;
                        }

                        credentials.timestamp = (Date.now() / 1000) | 0;

                        var baseUrl = document.location.href;

                        if (baseUrl.startsWith("file")) {
                            webix.storage.local.put("appCookie", credentials);
                        } else {
                            Cookies.set("appCookie", credentials);
                        }

                        component.$scope.show("./" + start);
                    }
                )
                .then(null, function () {
                    component.enable();
                    component.hideProgress();
                });
        },

        logout: function () {
            credentials = {};

            var baseUrl = document.location.href;

            if (baseUrl.startsWith("file")) {
                webix.storage.local.remove("appCookie");
            } else {
                Cookies.remove("appCookie");
            }
        },
        session: {
            status: function () {
                return new Promise(function (resolve, reject) {
                    console.log("Getting user-status");
                    resolve(true);
                });
            },
        },
    };
});
