﻿namespace ToolReSign.Util
{
    public class CommonConstants
    {
        public static readonly string[] WHITE_LIST_COLUMN = { "on_click", "body", "data", "xml_data", "xml", "data_xml", "content",
            "response", "icon_css", "xml_send", "xml_rep", "dlxml", "dlxml_thue","dlxml_bangke", "ly_do", "xml_nhan", "xml_cqt", "dulieumau", "xml_tdiep_gui", "signature" };

        public static string ServiceName { get; set; } = "ServiceBangTongHopDuLieu";
    }
}
