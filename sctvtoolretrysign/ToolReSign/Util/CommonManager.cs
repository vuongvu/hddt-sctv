﻿using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ToolReSign.Util
{
    public static class CommonManager
    {
        
        public static string RandomPassword(int length)
        {
            RNGCryptoServiceProvider cryptRNG = new RNGCryptoServiceProvider();
            byte[] tokenBuffer = new byte[length];
            cryptRNG.GetBytes(tokenBuffer);
            return Convert.ToBase64String(tokenBuffer);
        }
        

        public static string ExportSaveInvoiceXml(string xml, string security_number)
        {
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;

            doc.LoadXml(xml);

            XmlNode ndSellerAppRecord = doc.GetElementsByTagName("SellerAppRecordId")[0] == null ? doc.GetElementsByTagName("SellerAppRecsttId")[0] : doc.GetElementsByTagName("SellerAppRecordId")[0];
            XmlNode ndTemplateCode = doc.GetElementsByTagName("MauSo")[0];
            XmlNode ndInvoiceSeries = doc.GetElementsByTagName("Kyhieu")[0];
            XmlNode ndInvoiceNumber = doc.GetElementsByTagName("SoHoaDon")[0];
            XmlNode ndInvoiceIssuedDate = doc.GetElementsByTagName("NgayHoaDon")[0];
            XmlNode ndTotalAmount = doc.GetElementsByTagName("TongTien")[0];
            XmlNode ndBuyerTaxcode = doc.GetElementsByTagName("MaSoThueNguoiMua")[0];

            XmlNode ndSellerName = doc.GetElementsByTagName("TenNguoiBan")[0];
            XmlNode ndSellerTaxCode = doc.GetElementsByTagName("MaSoThueNguoiBan")[0];
            XmlNode ndSignatureValue = doc.GetElementsByTagName("SignatureValue")[0];

            //string buyerTaxcode = ndBuyerTaxcode.InnerText.Length == 0 ? "0000000000" : ndBuyerTaxcode.InnerText;
            string totalAmount = ndTotalAmount == null ? "0" : (ndTotalAmount.InnerText.Length == 0 ? "0" : ndTotalAmount.InnerText);

            string qrCode = ndSellerAppRecord?.InnerText + ";" + ndSellerTaxCode?.InnerText + ";" + ndTemplateCode?.InnerText + ";"
                    + ndInvoiceSeries?.InnerText + ";" + ndInvoiceNumber?.InnerText + ";" + ndInvoiceIssuedDate?.InnerText + ";"
                    + totalAmount + ";" + ndBuyerTaxcode?.InnerText + ";";

            byte[] bytes = Encoding.UTF8.GetBytes(ndSignatureValue.InnerText);

            string signer = "";

            string signed_date = "Ngày ký " + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(ndInvoiceIssuedDate.InnerText));
            string signed_company = "Được ký bởi " + ndSellerName.InnerText;
            string software_company = "Được cung cấp giải pháp bởi Tổng Công Ty Viễn Thông Mobifone";

            XmlNode elCertifiedData = doc.GetElementsByTagName("CertifiedData")[0];

            XmlElement el = doc.CreateElement("qrCodeData");
            el.InnerText = qrCode;
            elCertifiedData.AppendChild(el);

            el = doc.CreateElement("ChuKySo");
            el.InnerText = ndSignatureValue.InnerText;
            elCertifiedData.AppendChild(el);

            el = doc.CreateElement("SoBaoMat");
            el.InnerText = security_number;
            elCertifiedData.AppendChild(el);

            el = doc.CreateElement("NguoiKy");
            el.InnerText = signer;
            elCertifiedData.AppendChild(el);

            el = doc.CreateElement("NgayKy");
            el.InnerText = signed_date;
            elCertifiedData.AppendChild(el);

            el = doc.CreateElement("DonViKy");
            el.InnerText = signed_company;
            elCertifiedData.AppendChild(el);

            el = doc.CreateElement("DonViPhanMem");
            el.InnerText = software_company;
            elCertifiedData.AppendChild(el);

            return doc.OuterXml;
        }

        public static string CreateSignature(string idData, string idSigner, string xml, string subjectName, string signatureValue, string x509Certificate, string tenDonVi, string xmlHash, string tagSign)
        {
            XDocument doc = XDocument.Parse(xml);
            try
            {
                //doc.Root?.Add(new XElement("HashData", xmlHash));

                //var certifiedDataNode = doc.Descendants("CertifiedData");
                //certifiedDataNode.ElementAt(0).Add(new XElement("DonViKy", tenDonVi), new XElement("NgayKy", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));

                var signaturesNode = doc.Descendants("Signatures");

                XNamespace ns = "http://www.w3.org/2000/09/xmldsig#";
                var elementKeyInfo = new XElement("KeyInfo",
                                        new XElement("X509Data",
                                            new XElement("X509SubjectName", subjectName),
                                            new XElement("X509Certificate", x509Certificate))
                                     );
                var attrKeyInfo = elementKeyInfo.Attribute("xmlns");
                attrKeyInfo?.Remove();

                signaturesNode.ElementAt(0).Add(
                    new XElement(ns + "Signature",
                        new XAttribute("xmlns", "http://www.w3.org/2000/09/xmldsig#"),
                        new XAttribute("Id", idSigner),
                        new XElement("SignatureValue", signatureValue),
                        elementKeyInfo
                    )
                );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return doc.ToString().Replace("xmlns=\"\"", string.Empty).Replace(" >", ">");

        }
        public static byte[] GetResourceBytes(string resourceName)
        {
            var asm = Assembly.GetExecutingAssembly();

            Stream stream = asm.GetManifestResourceStream(resourceName);


            if (stream == null)

                throw new FileNotFoundException("Resource not found.");

            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public static bool CheckForSqlInjection(string userInput, bool checkHTML = true)
        {
            var isSqlInjection = false;

            if (string.IsNullOrWhiteSpace(userInput))
            {
                return isSqlInjection;
            }

            string[] sqlCheckList = {
                " alter ", ";alter ", " begin ", ";begin ", " create ", ";create ",
                " cursor ", ";cursor ", " declare ", ";declare ", " delete ", ";delete ",
                " drop ", ";drop ", " exec ", ";exec ", " execute ", ";execute ",
                " insert ", ";insert ", " kill ", ";kill ", " update ", ";update "
            };

            var checkString = userInput;//.Replace("'", "''");

            for (var i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if (checkString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    isSqlInjection = true;
                }
            }

            return isSqlInjection;

        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public static void createFolder(string path)
        {
            try
            {
                bool folderExists = Directory.Exists(path);
                if (!folderExists)
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void createFileRandomName(string path, byte[] bytes)
        {
            String fileName = GetTimestamp(DateTime.Now) + ".pdf";

            try
            {
                File.WriteAllBytes(path + "/" + fileName, bytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static bool CheckForRemoteCommand(string userInput)
        {
            var isRemoteCommand = false;

            if (string.IsNullOrWhiteSpace(userInput))
            {
                return isRemoteCommand;
            }

            string[] errorCheckList = { "Start()", ".exe", "System.Diagnostics.Process", ".jar", ".msi", ".vbs", ".bat", ".cs" };

            var checkString = userInput;//.Replace("'", "''");

            for (var i = 0; i <= errorCheckList.Length - 1; i++)
            {
                if (checkString.IndexOf(errorCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    isRemoteCommand = true;
                }
            }

            return isRemoteCommand;

        }

        public static string removeSpecialCharacter(string mst)
        {
            return new String(mst.Where(c => Char.IsLetter(c) || Char.IsDigit(c)).ToArray());
        }

        public static string GetMaThongDiep(string mst)
        {
            // lấy dữ liệu xml thông tin chung
            string mtdiep = "";
            mst = removeSpecialCharacter(mst);
            //string mngui = "K" + mst;
            string mngui = mst;
            // generate mã thông điệp
            string guid_id = removeSpecialCharacter(Guid.NewGuid().ToString().ToUpper());
            mtdiep = string.Format("{0}{1}", mngui, guid_id);

            return mtdiep;
        }

        public static string SignThongDiep(string thongDiepXml, X509Certificate2 cert, string pass)
        {
            XmlDocument document = new XmlDocument();
            document.PreserveWhitespace = false;

            try
            {
                thongDiepXml = thongDiepXml.Replace("'", "''");
                thongDiepXml = thongDiepXml.Replace("\r", "").Replace("\n", "");
                document.LoadXml(thongDiepXml);

                //string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                //string certFile = path + "\\Content\\Cert\\Minvoice.pfx";

                //X509Certificate2 cert = new X509Certificate2(certFile, "Minvoice2018");
                //X509Certificate2 cert = new X509Certificate2(certificate, pass);

                //AsymmetricAlgorithm asymmetricAlgorithm = cert.PrivateKey;

                //SignedXml signedXml = new SignedXml(document);
                //signedXml.SigningKey = asymmetricAlgorithm;

                //KeyInfo keyInfo = new KeyInfo();

                //RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                //keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

                //KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                //keyInfoData.AddSubjectName(cert.SubjectName.Name);
                //keyInfo.AddClause(keyInfoData);

                //signedXml.KeyInfo = keyInfo;

                //Reference reference = new Reference();
                //reference.Uri = "";

                //XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                //reference.AddTransform(env);

                //XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                //reference.AddTransform(env1);

                //signedXml.AddReference(reference);

                ////thẻ SigningTime
                //XmlDocument xmlObject = new XmlDocument();
                //xmlObject.PreserveWhitespace = true;
                //XmlElement xmlProperties = xmlObject.CreateElement("SignatureProperties", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement xmlProperty = xmlObject.CreateElement("SignatureProperty", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement dataElement = xmlObject.CreateElement("SigningTime", SignedXml.XmlDsigNamespaceUrl);
                //DateTime dt = DateTime.Now;
                //dataElement.AppendChild(xmlObject.CreateTextNode(dt.ToString("yyyy-MM-ddTHH:mm:ss")));
                //xmlProperty.AppendChild(dataElement);
                //xmlProperties.AppendChild(xmlProperty);
                //xmlObject.AppendChild(xmlProperties);
                //DataObject dataObject = new DataObject();
                //dataObject.Data = xmlObject.ChildNodes;
                //signedXml.AddObject(dataObject);

                //signedXml.ComputeSignature();

                //XmlElement xmlDigitalSignature = signedXml.GetXml();

                //document.DocumentElement.AppendChild(document.ImportNode(xmlDigitalSignature, true));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return document.OuterXml;
        }
        public static bool CheckMST(X509Certificate2 cert, XmlDocument doc)
        {

            XmlNodeList nodeListNguoiBan = doc.GetElementsByTagName("MST");
            string subjectName = cert.SubjectName.Name;

            bool trungMST = false;

            int indexMST = subjectName.IndexOf("MST");
            int endMST = subjectName.IndexOf(",", indexMST);
            if (endMST < 0) endMST = subjectName.Length - 1;
            string strMST = subjectName.Substring(indexMST, endMST - indexMST).Replace(" ","").Replace("MST:", "");

            foreach (XmlNode node in nodeListNguoiBan)
            {
                if (strMST == node.InnerText)
                {
                    trungMST = true;
                }
                
            }
            if (nodeListNguoiBan.Count == 0)
            {
                trungMST = true;
            }

            return true;
        }
        public static bool CheckData(string data, decimal maxData)
        {

            bool rlt = true;
            if (Encoding.ASCII.GetBytes(data).Length > maxData)
            {
                rlt = false;
            }

            return rlt;
        }

        public static string ThemPhanDonViKiThoiGianKy(string xml,string tendv)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.PreserveWhitespace = false;
                xml = xml.Replace("'", "''");
                xml = xml.Replace("\r", "").Replace("\n", "");
                document.LoadXml(xml);
               
                XmlNode nodeTT = document.SelectSingleNode("/HDon/DLHDon/TTKhac");
                XmlNode nodeTT_BangKe = document.SelectSingleNode("/BKe/DLBKe/TTKhac");
                if (nodeTT != null)
                {
                  
                    XmlElement TTin1 = document.CreateElement("TTin");
                    XmlElement TTruong1 = document.CreateElement("TTruong");
                    TTruong1.InnerText = "DVKy";
                    XmlElement KDLieu1 = document.CreateElement("KDLieu");
                    KDLieu1.InnerText = "string";
                    XmlElement DLieu1 = document.CreateElement("DLieu");
                    DLieu1.InnerText = tendv;
                    TTin1.AppendChild(TTruong1);
                    TTin1.AppendChild(KDLieu1);
                    TTin1.AppendChild(DLieu1);


                    XmlElement TTin2 = document.CreateElement("TTin");
                    XmlElement TTruong2 = document.CreateElement("TTruong");
                    TTruong2.InnerText = "NKy";
                    XmlElement KDLieu2 = document.CreateElement("KDLieu");
                    KDLieu2.InnerText = "date";
                    XmlElement DLieu2 = document.CreateElement("DLieu");
                    DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                    TTin2.AppendChild(TTruong2);
                    TTin2.AppendChild(KDLieu2);
                    TTin2.AppendChild(DLieu2);


                    nodeTT.AppendChild(TTin1);
                    nodeTT.AppendChild(TTin2);
                }

                if (nodeTT_BangKe != null)
                {
                    XmlElement TTin1 = document.CreateElement("TTin");
                    XmlElement TTruong1 = document.CreateElement("TTruong");
                    TTruong1.InnerText = "DVKy";
                    XmlElement KDLieu1 = document.CreateElement("KDLieu");
                    KDLieu1.InnerText = "string";
                    XmlElement DLieu1 = document.CreateElement("DLieu");
                    DLieu1.InnerText = tendv;
                    TTin1.AppendChild(TTruong1);
                    TTin1.AppendChild(KDLieu1);
                    TTin1.AppendChild(DLieu1);


                    XmlElement TTin2 = document.CreateElement("TTin");
                    XmlElement TTruong2 = document.CreateElement("TTruong");
                    TTruong2.InnerText = "NKy";
                    XmlElement KDLieu2 = document.CreateElement("KDLieu");
                    KDLieu2.InnerText = "date";
                    XmlElement DLieu2 = document.CreateElement("DLieu");
                    DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                    TTin2.AppendChild(TTruong2);
                    TTin2.AppendChild(KDLieu2);
                    TTin2.AppendChild(DLieu2);


                    nodeTT_BangKe.AppendChild(TTin1);
                    nodeTT_BangKe.AppendChild(TTin2);
                }


                xml = document.OuterXml;
            }
            catch
            {

            }
            return xml;
        }
    }
}