﻿using System.Data;
using System.Drawing;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Coravel.Invocable;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Serilog;
using ToolReSign.Data;

namespace ToolReSign.Services;

public class ReSignService : IInvocable
{
    private readonly string EXCEL_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private string _from;
    private string _password;
    private string _smtpAddress;
    private int _port;
    private bool _enableSsl;
    private List<Attachment> _attachments;
    private List<Stream> _streams;
    private IMinvoiceDbContext _minvoiceDbContext;
    private IConfiguration _configuration;

    public ReSignService(IConfiguration configuration, IMinvoiceDbContext mInvoiceDbContext)
    {
        _configuration = configuration;
        _from = _configuration["AppSettings:Email"];
        _password = _configuration["AppSettings:Password"];
        _smtpAddress = _configuration["AppSettings:SmtpAddress"];
        _port = int.Parse(_configuration["AppSettings:SmtpPort"]);
        _enableSsl = bool.Parse(_configuration["AppSettings:EnableSSL"]);
        _attachments = new List<Attachment>();
        _streams = new List<Stream>();
        _minvoiceDbContext = mInvoiceDbContext;
    }

    public void Send(string to, string subject, string body)
    {
        try
        {
            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateCertificate);

            Log.Information("Gửi mail đến {To}", to);
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(this._from);
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = Encoding.UTF8;

            foreach (Attachment attach in _attachments)
            {
                mail.Attachments.Add(attach);
            }

            SmtpClient smtp = new SmtpClient(this._smtpAddress, this._port);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.EnableSsl = _enableSsl;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(this._from, this._password);

            smtp.Timeout = 60000;
            smtp.Send(mail);
        }
        catch (Exception ex)
        {
            Log.Error("Error: {Error}", ex.Message);
            throw;
        }
    }

    public void Attach(string fileName, Stream stream, string contentType)
    {
        stream.Position = 0;
        ContentType ct = new ContentType(contentType);
        Attachment attach = new Attachment(stream, ct);

        if (attach.ContentDisposition != null && fileName.Length > 0)
        {
            attach.ContentDisposition.FileName = fileName;
        }

        _attachments.Add(attach);
    }

    public void ClearAttach()
    {
        foreach (var attachment in _attachments)
        {
            attachment.Dispose();
        }

        foreach (var stream in _streams)
        {
            stream.Dispose();
        }

        _streams.Clear();
        _attachments.Clear();
    }

    public async Task Invoke()
    {
        Log.Information("Bắt đầu xử lý vào lúc: {Time}", DateTime.Now);
        await ReSign();
    }

    private async Task ReSign()
    {
        try
        {
            var baseUrl = _configuration.GetValue<string>("AppSettings:BASE_URL");
            var loginUrl = _configuration.GetValue<string>("AppSettings:LOGIN_URL");
            var apiUrl = _configuration.GetValue<string>("AppSettings:SIGN_API_URL");
            var username = _configuration.GetValue<string>("AppSettings:Username");
            var password = _configuration.GetValue<string>("AppSettings:InvoicePassword");
            var branchCode = _configuration.GetValue<string>("AppSettings:BranchCode");

            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseUrl);
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var loginJson = new JObject
            {
                {"username", username},
                {"password", password},
                {"ma_dvcs", branchCode}
            };

            HttpContent request = new StringContent(loginJson.ToString(), Encoding.UTF8, "application/json");

            var responseMessage = await httpClient.PostAsync(loginUrl, request);
            if (responseMessage.IsSuccessStatusCode)
            {
                var content = await responseMessage.Content.ReadAsStringAsync();
                JObject result = JObject.Parse(content);
                var token = result["token"]?.ToString();
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bear", $"{token};{branchCode}");
                Log.Information("Đăng nhập thành công");
            }
            else
            {
                Log.Error("Đăng nhập bị lỗi");
            }

            var sqlCheck =
                "SELECT hdon_id FROM #SCHEMA_NAME#.wb_log_sign WHERE resign_success != true AND is_sent = false AND sign_retry < 5";
            DataTable rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheck);
            var listInvoice = rsTable.AsEnumerable().Select(row => row.Field<Guid>("hdon_id").ToString()).ToArray();
            if (listInvoice.Length > 0)
            {
                JObject model = new JObject();
                JArray arrayModel = new JArray();
                model.Add("branch_code", "VP");
                model.Add("username", "ADMINISTRATOR");
                model.Add("lsthdon_id", JArray.FromObject(listInvoice));
                model.Add("type_cmd", "203");
                model.Add("is_api", "1");
                model.Add("guiCQT", "Notsend");
                arrayModel.Add(model);
                JObject data = new JObject
                {
                    {"data", arrayModel}
                };
                request = new StringContent(data.ToString(), Encoding.UTF8, "application/json");
                responseMessage = await httpClient.PostAsync(apiUrl, request);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var result = JObject.Parse(await responseMessage.Content.ReadAsStringAsync());
                    if (result.ContainsKey("error"))
                    {
                        Log.Error("Ký bị lỗi");
                        Log.Error("Error: {Error}", result["error"]?.ToString());
                    }
                    else
                    {
                        Log.Information("Ký thành công");
                    }
                }
                else
                {
                    var statusCode = responseMessage.StatusCode;
                    Log.Error("Lỗi khi gọi đến api ký");
                    Log.Error("Status code: {StatusCode}", statusCode);
                }
            }
            else
            {
                Log.Information("Không tìm thấy hoá đơn cần ký lại");
            }

            sqlCheck = @"SELECT a.hdon_id, b.shdon, b.khieu FROM #SCHEMA_NAME#.wb_log_sign a
                        JOIN #SCHEMA_NAME#.hoadon68 b on a.hdon_id = b.hdon_id
                        WHERE a.resign_success = false AND is_sent = false AND sign_retry >= 5";
            rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheck);
            if (rsTable.Rows.Count > 0)
            {
                SaveExcel(rsTable);
            }

            if (_attachments.Count > 0)
            {
                List<string> listMail = _configuration.GetSection("AppSettings:ListMail").Get<List<string>>();
                string subject = $"[Thông báo] Hoá đơn ký lỗi ngày {DateTime.Now:yyyy-MM-ddTHH-mm}";
                string body =
                    "Dear anh/chị, <br/> Hệ thông hoá đơn điện tử xin gửi thông tin hoá đơn ký bị lỗi. <br/><br/> Trân trọng!";
                foreach (var mail in listMail)
                {
                    Send(mail, subject, body);
                }
                Log.Information("Gửi mail thành công");
                string listInvoiceId = string.Join("','", rsTable.AsEnumerable().Select(row => row.Field<Guid>("hdon_id").ToString()).ToArray());
                var sqlUpdate =
                    $"UPDATE #SCHEMA_NAME#.wb_log_sign SET is_sent = true WHERE hdon_id IN ('{listInvoiceId}')";
                _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
            }
            else
            {
                Log.Information("Không có hoá đơn bị lỗi hoặc tất cả hoá đơn bị lỗi đã được gửi mail thông báo");
            }
        }
        catch (Exception e)
        {
            var error = e.Message;
            Log.Error("{Error}", error);
        }
        finally
        {
            ClearAttach();
        }
    }

    private void SaveExcel(DataTable data)
    {
        var filePath = _configuration.GetValue<string>("Appsettings:ExcelFilePath");
        FileInfo fileInfo = new FileInfo(filePath);
        if (!fileInfo.Exists)
        {
            throw new Exception("Excel file not found!");
        }

        ExcelPackage excel = new ExcelPackage(fileInfo);

        try
        {
            var workSheet = excel.Workbook.Worksheets[0];
            int row = _configuration.GetValue<int>("AppSettings:ExcelStartRow");
            int stt = 1;
            {
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    if (i < data.Rows.Count - 1)
                    {
                        workSheet.Cells[$"A{row}"].Value = stt;
                        workSheet.Cells[$"A{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        workSheet.Cells[$"B{row}"].Value = data.Rows[i]["hdon_id"].ToString();
                        workSheet.Cells[$"B{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        workSheet.Cells[$"C{row}"].Value = data.Rows[i]["shdon"].ToString();
                        workSheet.Cells[$"C{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        workSheet.Cells[$"D{row}"].Value = data.Rows[i]["khieu"].ToString();
                        workSheet.Cells[$"D{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    }
                    else
                    {
                        workSheet.Cells[$"A{row}"].Value = stt;
                        workSheet.Cells[$"A{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        workSheet.Cells[$"B{row}"].Value = data.Rows[i]["hdon_id"].ToString();
                        workSheet.Cells[$"B{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        workSheet.Cells[$"C{row}"].Value = data.Rows[i]["shdon"].ToString();
                        workSheet.Cells[$"C{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        workSheet.Cells[$"D{row}"].Value = data.Rows[i]["khieu"].ToString();
                        workSheet.Cells[$"D{row}"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                        Stream stream = new MemoryStream();
                        excel.SaveAs(stream);
                        Attach(
                            $"TongHopHoaDonKyLoi_{DateTime.Now:yyyy-MM-ddTHH-mm}.xlsx",
                            stream, EXCEL_CONTENT_TYPE);
                        _streams.Add(stream);
                        excel.Dispose();
                        excel = new ExcelPackage(fileInfo);
                        workSheet = excel.Workbook.Worksheets[0];
                        row = _configuration.GetValue<int>("AppSettings:ExcelStartRow") - 1;
                        stt = 0;
                    }

                    if (i == data.Rows.Count - 1)
                    {
                        excel.Dispose();
                    }

                    row++;
                    stt++;
                }
            }
        }
        catch (Exception ex)
        {
            excel.Dispose();
            Log.Error("Error: {Error}", ex.Message);
            throw;
        }

    }

    static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
    {
        return true;
    }

}