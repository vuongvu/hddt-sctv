using Coravel;
using Serilog;
using ToolReSign.Data;
using ToolReSign.Services;
using ToolReSign.Workers;

// Console.OutputEncoding = Encoding.UTF8;
IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration((hostContext, config) =>
    {
        var env = hostContext.HostingEnvironment;
        config
            .AddJsonFile("appsettings.json", true, true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true);
    })
    .ConfigureServices((hostContext, services) =>
    {
        Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(hostContext.Configuration).CreateLogger();
        services.AddScheduler();
        services.AddHostedService<ReSignSerivceWorker>();
        services.AddSingleton<ReSignService>();
        services.AddSingleton<IMinvoiceDbContext, MInvoiceDbContext>();
    }).UseSerilog()
    .UseWindowsService()
    .Build();

await host.RunAsync();