﻿using System.Data;
using Newtonsoft.Json.Linq;
using ToolReSign.Util;

namespace ToolReSign.Data
{
    public partial interface IMinvoiceDbContext
    {
        void SetSiteHddt(string name);
        void ExecuteNoneQuery(string sql);
        void ExecuteNoneQuery(string sql, DataRow row);

        Task<string> ExecuteNoneQueryAsync(string sql);
        Task<string> ExecuteNoneQueryAsync(string sql, DataRow row);
        Task<string> ExecuteNoneQueryAsyncNotCheckHTML(string sql, DataRow row);
        Task<string> ExecuteNoneQueryAsync(string sql, Dictionary<string, object> parameters);
        Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, DataRow row);
        Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, JObject obj);
        Task<string> ExecuteNoneQueryAsyncNoneSQLInjection(string sql, Dictionary<string, object> parameters);
        DataTableExtend GetDataTable(string sql, CommandType commandType, Dictionary<string, object> parameters);
        DataTableExtend GetDataTable(string sql, CommandType commandType, params object[] parameters);
        DataTableExtend GetDataTable(string sql);
        Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, params object[] parameters);
        Task<DataTableExtend> GetDataTableAsync(string sql, params object[] parameters);
        Task<DataSet> GetDataSetAsyncPrintf(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataTableExtend> GetDataTableAsync(string sql);
        Task<DataTableExtend> GetTransactionDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataSet> GetDataSetAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataSet> GetDataSetAsync(string sql, CommandType commandType, Dictionary<string, object> parameters, string offset);


        Task<string> BeginTransactionAsync();
    
        Task<string> TransactionCommandAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<string> TransactionCommandAsync(string sql, CommandType commandType, JObject parameters);
        Task<string> TransactionCommitAsync();
        Task<string> TransactionRollbackAsync();
        void CloseTransaction();
        string GetSchemaName();

        Task<string> GetSchemaNameAsync();

        DataTable GetDataTableCache(string key);
        Task<DataTable> GetDataTableAsyncCache(string key);

        Task<string> BeginTransactionAsyncNotPool();
        Task<string> TransactionCommandAsyncNotPool(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<string> TransactionCommandAsyncNotPool(string sql, CommandType commandType, JObject parameters);
        Task<string> TransactionCommitAsyncNotPool();
        Task<string> TransactionRollbackAsyncNotPool();
        void CloseTransactionNotPool();

    }
}
