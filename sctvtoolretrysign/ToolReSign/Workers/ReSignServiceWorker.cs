using Coravel.Scheduling.Schedule.Interfaces;
using ToolReSign.Services;

namespace ToolReSign.Workers;

public class ReSignSerivceWorker : BackgroundService
{
    private readonly IConfiguration _configuration;
    private IScheduler _scheduler;

    public ReSignSerivceWorker(IConfiguration configuration, IScheduler scheduler)
    {
        _configuration = configuration;
        _scheduler = scheduler;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var scheduler = _scheduler.OnWorker("SignTask");
        // int hour = _configuration.GetValue<int>("Appsettings:Hour");
        // int minute = _configuration.GetValue<int>("Appsettings:Minute");
        // scheduler.Schedule<ReSignService>().DailyAt(hour, minute).Zoned(TimeZoneInfo.Local);
        scheduler.Schedule<ReSignService>().EveryThirtyMinutes().PreventOverlapping("SignTask");
        await Task.Delay(500, stoppingToken);
    }
}