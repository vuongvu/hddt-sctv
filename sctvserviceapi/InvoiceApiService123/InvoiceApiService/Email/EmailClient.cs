﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApiService.Email
{
    public class EmailClient:IDisposable
    {
        private string _from;
        private string _password;
        private string _smtpAddress;
        private int _port;
        private bool _enableSSL;
        private List<Attachment> attachments;

        public EmailClient()
        {
            this._from = ConfigurationManager.AppSettings["Email"];
            this._password = ConfigurationManager.AppSettings["Password"];
            this._smtpAddress = ConfigurationManager.AppSettings["SmtpAddress"];
            this._port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            this._enableSSL = ConfigurationManager.AppSettings["EnableSSL"] == "true" ? true : false;

            attachments = new List<Attachment>();
        }

        public string GetFrom()
        {
            return this._from;
        }

        public void SetEmailServer(string smtpAddress, int smtpPort, bool enableSSL)
        {
            this._smtpAddress = smtpAddress;
            this._port = smtpPort;
            this._enableSSL = enableSSL;
        }

        public void Send(string to, string subject, string body)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(this._from);
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = UTF8Encoding.UTF8;


            foreach (Attachment attach in attachments)
            {
                mail.Attachments.Add(attach);

            }

            SmtpClient smtp = new SmtpClient(this._smtpAddress, this._port);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.EnableSsl = this._enableSSL;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(this._from, this._password);

            smtp.Timeout = 60000;


            smtp.Send(mail);
        }

        public async Task SendAsync(string to, string subject, string body)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(this._from);
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = UTF8Encoding.UTF8;


            foreach (Attachment attach in attachments)
            {
                mail.Attachments.Add(attach);

            }

            SmtpClient smtp = new SmtpClient(this._smtpAddress, this._port);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.EnableSsl = this._enableSSL;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(this._from, this._password);

            smtp.Timeout = 60000;


            await smtp.SendMailAsync(mail);
        }

        public void Send(string from, string pass, string to, string subject, string body)
        {
            string password = this._from == from ? this._password : pass;

            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(from);
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = UTF8Encoding.UTF8;

            foreach (Attachment attach in attachments)
            {
                mail.Attachments.Add(attach);

            }

            SmtpClient smtp = new SmtpClient(this._smtpAddress, this._port);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.EnableSsl = this._enableSSL;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(from, password);

            smtp.Timeout = 60000;


            smtp.Send(mail);
        }

        public void Send(string from, string pass, string to, string subject, string body, string bcc)
        {
            string password = this._from == from ? this._password : pass;

            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(from);
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = UTF8Encoding.UTF8;

            if (bcc.Length > 0)
            {
                string[] parts = bcc.Split(';');

                foreach (var part in parts)
                {
                    MailAddress mailAddress = new MailAddress(part);
                    mail.Bcc.Add(mailAddress);
                }
            }

            foreach (Attachment attach in attachments)
            {
                mail.Attachments.Add(attach);

            }

            SmtpClient smtp = new SmtpClient(this._smtpAddress, this._port);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.EnableSsl = this._enableSSL;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(from, password);

            smtp.Timeout = 60000;


            smtp.Send(mail);
        }

        public async Task SendAsync(string from, string pass, string to, string subject, string body, string bcc, string alias)
        {
            try
            {
                string password = this._from == from ? this._password : pass;

                MailMessage mail = new MailMessage();

                if (!string.IsNullOrEmpty(alias))
                {
                    mail.From = new MailAddress(from, alias);
                }
                else
                {
                    mail.From = new MailAddress(from);
                }
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.BodyEncoding = UTF8Encoding.UTF8;

                if (bcc.Length > 0)
                {
                    string[] parts = bcc.Split(';');

                    foreach (var part in parts)
                    {
                        MailAddress mailAddress = new MailAddress(part);
                        mail.Bcc.Add(mailAddress);
                    }
                }

                foreach (Attachment attach in attachments)
                {
                    mail.Attachments.Add(attach);

                }

                SmtpClient smtp = new SmtpClient(this._smtpAddress, this._port);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = this._enableSSL;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(from, password);

                smtp.Timeout = 60000;


                await smtp.SendMailAsync(mail);
            }
            catch (Exception ex)
            {
                string mess = ex.Message;
            }
        }


        public void Attach(string fileName, Stream stream, string contentType)
        {
            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(contentType);
            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(stream, ct);

            if (fileName.Length > 0)
            {
                attach.ContentDisposition.FileName = fileName;
            }

            attachments.Add(attach);
        }

        public void ClearAttach()
        {
            if (attachments != null)
            {
                attachments.Clear();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (attachments != null)
                {
                    attachments.Clear();
                    attachments = null;
                }
            }
        }
    }
}
