﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApiService.CheckXml
{
    public class InputValidation
    {
        public string pathXSD { get; set; }
        public string xml { get; set; }
    }
    public class FileVSD
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public string File { get; set; }
        public string Des { get; set; }
    }
    public class ResultValidation
    {
        public string status { get; set; }
        public string mess { get; set; }
        public string mtDiep { get; set; }
    }
    public class ResultCheckSign
    {
        public bool result { get; set; }
        public string sott { get; set; }
        public string mess { get; set; }
    }
    public class ItemValidation
    {
        public string TenNode { get; set; }
        public string mess { get; set; }
    }
}
