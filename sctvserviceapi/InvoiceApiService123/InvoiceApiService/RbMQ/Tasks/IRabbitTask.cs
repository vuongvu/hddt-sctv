﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApiService.RbMQ.Tasks
{
    public interface IRabbitTask
    {
        void CloseTask();
        bool IsOpen();
        string GetTaskName();
    }
}
