﻿using InvoiceApiService.Data;
using InvoiceApiService.Util;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace InvoiceApiService.RbMQ.Tasks
{
    public class VendorReceiveVanResponseKhac : IRabbitTask
    {
        private string _queueName = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_KHAC;

        private string _queueNameConfirm = CommonConstants.QUEUE_VENDOR_SEND_REQUEST_TO_VAN;

        private IModel _channel;

        public VendorReceiveVanResponseKhac()
        {
            InitTask();
        }

        private void InitTask()
        {
            string vendor = CommonConstants.VENDOR;
            RabbitClient client = RabbitClient.GetInstance();
            var conn = client.GetConnection();

            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("x-queue-type", "quorum");

            _channel = conn.CreateModel();
            _channel.QueueDeclare(_queueName + "_" + vendor, true, false, false, args);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(_channel);
            var consumerTag = Guid.NewGuid().ToString();
            consumer.Received += TctConfirmTechnical;

            _channel.BasicConsume(_queueName + "_" + vendor, false, consumerTag, false, false, null, consumer);
        }


        private async void TctConfirmTechnical(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var channel = ((EventingBasicConsumer)sender).Model;

            IMinvoiceDbContext _minvoiceDbContext = null;

            try
            {
                _minvoiceDbContext = new MInvoiceDbContext();
                string mltDiepVendorConfirm = "999";
                string mtdVendorConfirm = null;
                string xmlVendorConfirm = null;

                JObject rabbitObj = JObject.Parse(message);
                string xml = rabbitObj["xml"].ToString();
                string siteHddt = rabbitObj["siteHddt"].ToString();
                string vendor = rabbitObj["vendor"].ToString();
                string privateKey = rabbitObj["privateKey"].ToString();

                _minvoiceDbContext.SetSiteHddt(siteHddt);

                //Message thuế trả về ở đây;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
                XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
                XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
                XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");
                XmlNode mst_nnt = doc.SelectSingleNode("/TDiep/TTChung/MST");

                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("mtdiep_gui", mtdiep_gui.InnerText);

                string sqlSelectt = $"SELECT * FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE mtdiep_gui = @mtdiep_gui ";

                DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(sqlSelectt, CommandType.Text, param1);

                if (tbl.Rows.Count > 0)
                {
                    for (int i = 0; i < tbl.Rows.Count; i++)
                    {
                        string inv_id = tbl.Rows[i]["type_id"].ToString();
                        string mltdiep_gui = tbl.Rows[i]["mltdiep_gui"].ToString();
                        string mdvi = tbl.Rows[i]["mdvi"]?.ToString();
                        string is_api = tbl.Rows[i]["is_api"]?.ToString();

                        if (mltdiep.InnerText == "102")
                        {
                            await xuLy102(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api);
                        }
                        else if (mltdiep.InnerText == "103")
                        {
                            await xuLy103(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api);
                        }
                        else if (mltdiep.InnerText == "104")
                        {
                            await xuLy104(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api);
                        }

                        else if (mltdiep.InnerText == "202")
                        {
                            await xuLy202(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                        }
                        else if (mltdiep.InnerText == "204")
                        {
                            if (mltdiep_gui == "400")
                            {
                                await xuLy204_400(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api);
                            }
                            else if (mltdiep_gui == "300")
                            {
                                await xuLy204_300(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                            }
                            else if (mltdiep_gui == "200")
                            {
                                await xuLy204_200(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                            }
                            else
                            {
                                await xuLy204_203(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                            }
                        }
                        else if (mltdiep.InnerText == "301")
                        {
                            await xuLy301(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                        }

                        await _minvoiceDbContext.BeginTransactionAsync();

                        string insertPhkt = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68_phkt (id, mltdiep_gui, mtdiep_gui, xml_tdiep_gui, mltdtchieu, mtdtchieu, tonghop_gui_tvan_68_id) " +
                        $"  VALUES (@id, @mltdiep_gui, @mtdiep_gui, @xml_tdiep_gui, @mltdtchieu, @mtdtchieu, @tonghop_gui_tvan_68_id)";

                        mtdVendorConfirm = CommonManager.GetMaThongDiep(mst_nnt.InnerText);
                        xmlVendorConfirm = CommonManager.ThongDiepPhanHoi(mltDiepVendorConfirm, mtdVendorConfirm, xml, 0, null);

                        Dictionary<string, object> param2 = new Dictionary<string, object>();
                        param2.Add("id", Guid.NewGuid());
                        param2.Add("mltdiep_gui", mltDiepVendorConfirm);
                        param2.Add("mtdiep_gui", mtdVendorConfirm);
                        param2.Add("xml_tdiep_gui", xmlVendorConfirm);
                        param2.Add("mltdtchieu", mltdiep.InnerText);
                        param2.Add("mtdtchieu", mtdiep.InnerText);
                        param2.Add("tonghop_gui_tvan_68_id", Guid.Parse(tbl.Rows[i]["id"].ToString()));
                        await _minvoiceDbContext.TransactionCommandAsync(insertPhkt, CommandType.Text, param2);

                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                    }

                }
                else
                {
                    await _minvoiceDbContext.BeginTransactionAsync();

                    string insertPhkt = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68_phkt (id, mltdiep_gui, mtdiep_gui, xml_tdiep_gui, mltdtchieu, mtdtchieu, tonghop_gui_tvan_68_id) " +
                   $"  VALUES (@id, @mltdiep_gui, @mtdiep_gui, @xml_tdiep_gui, @mltdtchieu, @mtdtchieu, @tonghop_gui_tvan_68_id)";

                    mtdVendorConfirm = CommonManager.GetMaThongDiep(mst_nnt.InnerText);

                    JObject obj = new JObject();
                    obj.Add("MLoi", CommonConstants.LOI_KHONG_THAY_THONG_DIEP);
                    obj.Add("MTa", "Không tồn tại thông điệp");
                    JArray dsLoi = new JArray();
                    dsLoi.Add(obj);

                    xmlVendorConfirm = CommonManager.ThongDiepPhanHoi(mltDiepVendorConfirm, mtdVendorConfirm, xml, 1, dsLoi);

                    Dictionary<string, object> param3 = new Dictionary<string, object>();
                    param3.Add("id", Guid.NewGuid());
                    param3.Add("mltdiep_gui", mltDiepVendorConfirm);
                    param3.Add("mtdiep_gui", mtdVendorConfirm);
                    param3.Add("xml_tdiep_gui", xmlVendorConfirm);
                    param3.Add("mltdtchieu", mltdiep.InnerText);
                    param3.Add("mtdtchieu", mtdiep.InnerText);
                    param3.Add("tonghop_gui_tvan_68_id", null);
                    await _minvoiceDbContext.TransactionCommandAsync(insertPhkt, CommandType.Text, param3);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }

                channel.BasicAck(e.DeliveryTag, false);

                string timestampConfirm = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string tokenConfirm = FunctionUtil.ComputeSha256Hash(FunctionUtil.ComputeSha256Hash(xmlVendorConfirm) + ":" + mltDiepVendorConfirm + ":" + vendor + ":" + siteHddt + ":" + timestampConfirm + ":" + privateKey);
                string xmlReturn = CommonManager.GenerateXmlPhanHoi(mltDiepVendorConfirm, vendor, siteHddt, timestampConfirm, tokenConfirm, xmlVendorConfirm);

                RabbitClient rabbitClient = RabbitClient.GetInstance();
                rabbitClient.PublishMessageQueue(_queueNameConfirm, xmlReturn);


            }
            catch (Exception ex)
            {
                Log.Error(_queueName + ": " + ex.Message);
                Log.Error(message);

                if (_minvoiceDbContext != null)
                {
                    try
                    {
                        channel.BasicAck(e.DeliveryTag, false);
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionCommitAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        _minvoiceDbContext.CloseTransaction();
                    }
                    catch (Exception) { }
                }

            }

        }

        private async Task xuLy102(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api)
        {
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            XmlNode ket_qua = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/THop");
            XmlNode ly_do = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/DSLDKCNhan");

            await _minvoiceDbContext.BeginTransactionAsync();

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau01_68 SET xml_tiepnhan = @xml_rep,"
                + " ketqua_tiepnhan=@ket_qua, ngay_tbao_tiepnhan=@ngay_tbao,lydo_tiepnhan = @ly_do,"
                + " mtdiep_tiepnhan_cqt=@mtdiep_cqt, mtdiep_tiepnhan_gui=@mtdiep_gui "
                + " WHERE mau01_id = @mau01_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1.Add("xml_rep", xml);
            dic1.Add("mau01_id", Guid.Parse(inv_id));
            dic1.Add("ket_qua", ket_qua.InnerText);
            dic1.Add("ngay_tbao", DateTime.Now);
            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);
            dic1.Add("ly_do", ly_do?.OuterXml);

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);

            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("id", Guid.NewGuid());
            dic2.Add("mtdiep_cqt", mtdiep.InnerText);
            dic2.Add("xml_cqt", xml);
            dic2.Add("mltdiep", mltdiep.InnerText);
            dic2.Add("tgian_gui", DateTime.Now);
            dic2.Add("mdvi", mdvi);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic2);

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

        }

        private async Task xuLy103(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api)
        {
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            XmlNode ket_qua = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/TTXNCQT");
            XmlNode ly_do = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/DSLDKCNhan");

            await _minvoiceDbContext.BeginTransactionAsync();

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau01_68 SET xml_rep = @xml_rep,"
                + " ket_qua=@ket_qua, ngay_tbao=@ngay_tbao,ly_do = @ly_do,"
                + " mtdiep_cqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                + " WHERE mau01_id = @mau01_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1.Add("xml_rep", xml);
            dic1.Add("mau01_id", Guid.Parse(inv_id));
            dic1.Add("ket_qua", ket_qua.InnerText);
            dic1.Add("ngay_tbao", DateTime.Now);
            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);
            dic1.Add("ly_do", ly_do?.OuterXml);

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);

            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("id", Guid.NewGuid());
            dic2.Add("mtdiep_cqt", mtdiep.InnerText);
            dic2.Add("xml_cqt", xml);
            dic2.Add("mltdiep", mltdiep.InnerText);
            dic2.Add("tgian_gui", DateTime.Now);
            dic2.Add("mdvi", mdvi);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic2);

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

        }

        private async Task xuLy104(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api)
        {
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            await _minvoiceDbContext.BeginTransactionAsync();

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau01_68 SET xml_rep = @xml_rep,"
                + " ket_qua=@ket_qua, ngay_tbao=@ngay_tbao, mtdiep_cqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                + " WHERE mau01_id = @mau01_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1.Add("xml_rep", xml);
            dic1.Add("mau01_id", Guid.Parse(inv_id));
            dic1.Add("ket_qua", CommonConstants.TIEP_NHAN_THONG_BAO_UY_NHIEM); // 3 Đã phản hồi kết quả
            dic1.Add("ngay_tbao", DateTime.Now);
            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);

            XmlNodeList dsChapNhan = doc.GetElementsByTagName("TTUNhiem");

            foreach (XmlNode chapNhan in dsChapNhan)
            {
                string sqlUpdateUyNhiem = $"UPDATE #SCHEMA_NAME#.mau01_68_uynhiem SET ket_qua = @ket_qua"
               + " WHERE mau01_id = @mau01_id and mstunhiem = @mstunhiem and ttcdunhiem = @ttcdunhiem ";

                Dictionary<string, object> dic2 = new Dictionary<string, object>();
                dic2.Add("ket_qua", chapNhan.SelectSingleNode("DSLDKCNhan") == null ? 1 : 2); // 1 Chấp nhận
                dic2.Add("mau01_id", Guid.Parse(inv_id));
                dic2.Add("mstunhiem", chapNhan.SelectSingleNode("MST")?.InnerText);
                dic2.Add("ttcdunhiem", chapNhan.SelectSingleNode("TTChuc")?.InnerText);

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateUyNhiem, CommandType.Text, dic2);
            }

            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

            Dictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3.Add("id", Guid.NewGuid());
            dic3.Add("mtdiep_cqt", mtdiep.InnerText);
            dic3.Add("xml_cqt", xml);
            dic3.Add("mltdiep", mltdiep.InnerText);
            dic3.Add("tgian_gui", DateTime.Now);
            dic3.Add("mdvi", mdvi);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic3);

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

        }

        private async Task xuLy202(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            XmlNode maCQT = doc.SelectSingleNode("/TDiep/DLieu/HDon/MCCQT");

            await _minvoiceDbContext.BeginTransactionAsync();

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET mccqthue = @mccqthue, tthai = @tthai, mtdiep_gui=@mtdiep_gui, mtdiep_cqt=@mtdiep_cqt, error_status = @error_status \n"
                            + " WHERE hdon_id = @hdon_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1.Add("hdon_id", Guid.Parse(inv_id));
            dic1.Add("tthai", "Đã cấp mã");
            dic1.Add("mccqthue", maCQT.InnerText);
            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);
            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
            dic1.Add("error_status", "0");  // 0 hóa đơn hợp lệ | 1 hóa đơn k hợp lệ

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);

            //XmlNode xmlHdon = doc.SelectSingleNode("/TDiep/DLieu/");

            string sqlUpdateXML = $"UPDATE #SCHEMA_NAME#.dulieuxml68 SET dlxml_thue = @dlxml_thue WHERE hdon_id = @hdon_id";

            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("hdon_id", Guid.Parse(inv_id));
            dic2.Add("dlxml_thue", doc.OuterXml);
            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateXML, CommandType.Text, dic2);

            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

            Dictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3.Add("log_message_id", Guid.NewGuid());
            dic3.Add("hdon_id", Guid.Parse(inv_id));
            dic3.Add("mtdiep_cqt", mtdiep?.InnerText);
            dic3.Add("mtdiep_gui", mtdiep_gui?.InnerText);
            dic3.Add("xml_cqt", xml);
            dic3.Add("loai_tbao", mltdiep.InnerText);
            dic3.Add("ttchung", ttchung.InnerText);
            dic3.Add("tgian_gui", DateTime.Now);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic3);

            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

            Dictionary<string, object> dic4 = new Dictionary<string, object>();
            dic4.Add("id", Guid.NewGuid());
            dic4.Add("mtdiep_cqt", mtdiep?.InnerText);
            dic4.Add("xml_cqt", xml);
            dic4.Add("mltdiep", mltdiep.InnerText);
            dic4.Add("tgian_gui", DateTime.Now);
            dic4.Add("mdvi", mdvi);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic4);

            // lưu kết quả cấp mã hóa đơn
            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_capmahd_68 (ketqua_capmahd_68_id, mltdiepcqt, macqthue, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, note) VALUES "
                   + " (@ketqua_capmahd_68_id, @mltdiepcqt, @macqthue, @type_id, @nguoi_gui, current_timestamp, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @note)";

            Guid ketqua_capmahd_68_id = Guid.NewGuid();

            Dictionary<string, object> dic5 = new Dictionary<string, object>();
            dic5.Add("ketqua_capmahd_68_id", ketqua_capmahd_68_id);
            dic5.Add("mltdiepcqt", mltdiep.InnerText);
            dic5.Add("macqthue", maCQT.InnerText);
            dic5.Add("type_id", Guid.Parse(inv_id));
            dic5.Add("nguoi_gui", "Cơ quan Thuế");
            dic5.Add("tgian_gui", DateTime.Now);
            dic5.Add("mtdiep_cqt", mtdiep?.InnerText);
            dic5.Add("mtdtchieu", mtdiep_gui?.InnerText);
            dic5.Add("xml_cqt", xml);
            dic5.Add("note", "capma");

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic5);

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

            try
            {
                if (is_api == "1")
                {
                    string[] stringList = siteHddt.Split('.');
                    string str_mst = stringList[0].ToString();

                    RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                    rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            try
            {
                Dictionary<string, object> dic6 = new Dictionary<string, object>();
                dic6.Add("branch_code", mdvi);
                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dic6);
                if (tblSetting.Rows.Count > 0)
                {
                    string value = tblSetting.Rows[0]["value"].ToString();
                    if (value == "C")
                    {
                        await SendEmail(inv_id, siteHddt);
                        // update vào bảng hóa đơn đã gửi Email
                        string sqlUpdateSenMail = "UPDATE #SCHEMA_NAME#.hoadon68 SET isSendmail = true WHERE hdon_id='" + inv_id + "'";
                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdateSenMail);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

        }

        private async Task xuLy204_400(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api)
        {
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            await _minvoiceDbContext.BeginTransactionAsync();
            Dictionary<string, object> parameters2 = new Dictionary<string, object>();

            XmlNodeList ds_Lydo = doc.GetElementsByTagName("LDo");
            XmlNodeList ds_LyDoChung = doc.GetElementsByTagName("LDTTChung");

            XmlNodeList ds_Lydo_kxd_hd = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao/LBTHKXDau/DSBTHop/BTHop/DSLHDon/HDon/DSLDo/LDo");
            XmlNodeList ds_Lydo_kxd_chung = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao/LBTHKXDau/DSBTHop/BTHop/DSLDTTChung/LDTTChung");
            XmlNodeList ds_Lydo_xd_chung = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao/LBTHXDau/DSBTHop/BTHop/DSLDTTChung/LDTTChung");
            XmlNodeList ds_Lydo_khac = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao/KHLKhac/DSLDo");

            // Xử lý bảng tổng hợp dữ liệu
            XmlNode ketQua = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/LTBao");

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET xml_nhan = @xml_rep, ket_qua = @ket_qua, "
                 + " ngay_tbao=@ngay_tbao, mtdiepcqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                 + " WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
            Dictionary<string, object> dic5 = new Dictionary<string, object>();
            dic5.Add("xml_rep", xml);
            dic5.Add("ket_qua", ketQua.InnerText.ToString() == "2" ? 1 : 2);
            dic5.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
            dic5.Add("ngay_tbao", DateTime.Now);
            dic5.Add("mtdiep_cqt", mtdiep.InnerText);
            dic5.Add("mtdiep_gui", mtdiep_gui.InnerText);

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic5);


            string sqlGetBTHDL = $"select * from #SCHEMA_NAME#.bangtonghopdl_68 where bangtonghopdl_68_id = '{inv_id}'::uuid";

            DataTable data = _minvoiceDbContext.GetDataTable(sqlGetBTHDL, CommandType.Text);

            // lưu kết quả kiểm tra dữ liệu

            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                   + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, @tgian_gui, @nguoi_gui, @note, @is_error)";

            Guid ketqua_ktdl_68_id = Guid.NewGuid();
            string is_error = "0";
            if (ds_Lydo_kxd_hd.Count > 0 || ds_Lydo_kxd_chung.Count > 0 || ds_Lydo_xd_chung.Count > 0 || ketQua.InnerText.ToString() != "2")
            {
                is_error = "1";

                // update vào bảng bangtonghopdl_68
                //sqlUpdate = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET error_status = '1' WHERE bangtonghopdl_68_id = '" + inv_id + "'";
                //await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);
            }
            if ((is_error == "1" && (ds_Lydo_kxd_chung.Count > 0 || ds_Lydo_khac.Count > 0 || ds_Lydo_xd_chung.Count > 0)) || ketQua.InnerText.ToString() == "9")
            {
                sqlUpdate = $@"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET error_status = 1  
                                                                WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                dic5.Clear();
                dic5.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic5);
            }
            else if (is_error == "1")
            {
                sqlUpdate = $@"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET error_status = 2  
                                                                WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                dic5.Clear();
                dic5.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic5);
            }


            Dictionary<string, object> dic6 = new Dictionary<string, object>();
            dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
            dic6.Add("type_id", Guid.Parse(inv_id));
            dic6.Add("mtdiep_cqt", mtdiep.InnerText);
            dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
            dic6.Add("xml_cqt", xml);
            dic6.Add("mltdiep_tchieu", mltdiep_gui);
            dic6.Add("tgian_gui", DateTime.Now);
            dic6.Add("nguoi_gui", "Cơ quan Thuế");
            dic6.Add("note", "ktdl");
            dic6.Add("is_error", is_error);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

            // lưu mã lỗi, hướng dẫn xử lý
            foreach (XmlNode node in ds_Lydo)
            {
                string maloi = "";
                string mota = "";
                string hdxly = "";

                maloi = node["MLoi"]?.InnerText;
                mota = node["MTLoi"]?.InnerText;
                hdxly = node["HDXLy"]?.InnerText;

                parameters2.Clear();
                parameters2.Add("id", Guid.NewGuid());
                parameters2.Add("type_id", Guid.Parse(inv_id));
                parameters2.Add("maloi", maloi);
                parameters2.Add("motaloi", mota);
                parameters2.Add("huongdanxuly", hdxly);

                parameters2.Add("tgian", DateTime.Now);
                parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                parameters2.Add("reference_id", ketqua_ktdl_68_id);

                string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                     + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
            }


            if (ds_LyDoChung.Count > 0 || ketQua.InnerText.ToString() == "9")
            {
                foreach (XmlNode node in ds_LyDoChung)
                {
                    string maloi = "";
                    string mota = "";
                    string hdxly = "";

                    maloi = node["MLoi"]?.InnerText;
                    mota = node["MTLoi"]?.InnerText;
                    hdxly = node["HDXLy"]?.InnerText;

                    parameters2.Clear();
                    parameters2.Add("id", Guid.NewGuid());
                    parameters2.Add("type_id", Guid.Parse(inv_id));
                    parameters2.Add("maloi", maloi);
                    parameters2.Add("motaloi", mota);
                    parameters2.Add("huongdanxuly", hdxly);

                    parameters2.Add("tgian", DateTime.Now);
                    parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                    parameters2.Add("reference_id", ketqua_ktdl_68_id);

                    string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                         + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);

                }

                JObject param_update_hoadon = new JObject();
                param_update_hoadon.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
                if (data.Rows[0]["lhhoa"].ToString() != "1")
                {
                    string sqlUpdate3 = $@"update #SCHEMA_NAME#.hoadon68
                                                            set tthai = 'Đã ký'
                                                            where hdon_id in (select b.hdon_id
                                                                 from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                          join #SCHEMA_NAME#.bangtonghopdl_68_chitiet b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                 where a.bangtonghopdl_68_id = @bangtonghopdl_68_id::uuid ) ";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate3, CommandType.Text, param_update_hoadon);
                }
                else
                {
                    string sqlUpdate3 = $@"update #SCHEMA_NAME#.hoadon68 x
                                                            set tthai = 'Đã ký'
                                                            where (select count(*) from (select b.hdon_id
                                                                 from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                          join #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                 where a.bangtonghopdl_68_id = @bangtonghopdl_68_id::uuid and position(x.hdon_id::text in b.hdon_id) >0)a) >0 ";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate3, CommandType.Text, param_update_hoadon);
                }
            }
            else
            {
                JObject param_update_hoadon = new JObject();
                param_update_hoadon.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
                if (data.Rows[0]["lhhoa"].ToString() != "1")
                {
                    string sqlUpdate3 = $@"update #SCHEMA_NAME#.hoadon68
                                                            set tthai = 'CQT đã nhận'
                                                            where hdon_id in (select b.hdon_id
                                                                 from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                          join #SCHEMA_NAME#.bangtonghopdl_68_chitiet b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                 where a.bangtonghopdl_68_id = @bangtonghopdl_68_id::uuid ) ";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate3, CommandType.Text, param_update_hoadon);
                }
                else
                {
                    string sqlUpdate3 = $@"update #SCHEMA_NAME#.hoadon68 x
                                                            set tthai = 'CQT đã nhận'
                                                            where (select count(*) from (select b.hdon_id
                                                                 from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                          join #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                 where a.bangtonghopdl_68_id = @bangtonghopdl_68_id::uuid and position(x.hdon_id::text in b.hdon_id) >0)a) >0 ";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate3, CommandType.Text, param_update_hoadon);
                }
                //end

            }

            //hatm xử lí trường hợp sai từng hóa đơn
            XmlNodeList ketQuaHoaDon = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao/LHDKMa/DSHDon/HDon");
            if (ketQuaHoaDon.Count > 0)
            {
                for (int k = 0; k < ketQuaHoaDon.Count; k++)
                {
                    XmlNode nodehoadon = ketQuaHoaDon.Item(k);
                    //string khieu = nodehoadon.SelectSingleNode("/KHMSHDon").ToString() + nodehoadon.SelectSingleNode("/KHHDon").ToString();
                    //string shdon = nodehoadon.SelectSingleNode("/SHDon").ToString();

                    string sqlHoaDon = "";
                    var ds_LydoHD = nodehoadon.SelectNodes("DSLDo/LDo");
                    var dagui = false;
                    foreach (XmlNode node in ds_LydoHD)
                    {
                        string maloiHD = node["MLoi"].InnerText;
                        if (maloiHD == "40050" || maloiHD == "-4" || maloiHD == "40045")
                        {
                            dagui = true;
                            break;
                        }
                    }
                    if (!dagui)
                    {
                        sqlHoaDon = $@"Select * from #SCHEMA_NAME#.hoadon68 WHERE khieu=@khieu and shdon=@shdon and hdon_id in (select b.hdon_id
                                                                from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                        join #SCHEMA_NAME#.bangtonghopdl_68_chitiet b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                where a.bangtonghopdl_68_id = @p_bangtonghopdl_68_id::uuid)";

                        string khieu = nodehoadon.SelectSingleNode("KHMSHDon").InnerText + nodehoadon.SelectSingleNode("KHHDon").InnerText;
                        string shdon = nodehoadon.SelectSingleNode("SHDon").InnerText;

                        dic6 = new Dictionary<string, object>();
                        dic6.Add("khieu", khieu);
                        dic6.Add("shdon", shdon);
                        dic6.Add("p_bangtonghopdl_68_id", Guid.Parse(inv_id));
                        DataTable hoadon = _minvoiceDbContext.GetDataTable(sqlHoaDon, CommandType.Text, dic6);

                        // hatm: update trạng thái mã thông điệp vào bảng hoadon68

                        dic6 = new Dictionary<string, object>();
                        dic6.Add("hdon_id", Guid.Parse(hoadon.Rows[0]["hdon_id"].ToString()));

                        sqlUpdate = $@"UPDATE #SCHEMA_NAME#.hoadon68 SET tthai ='Đã ký' WHERE hdon_id=@hdon_id::uuid";
                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic6);

                        sqlInsert = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                             + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, current_timestamp, @nguoi_gui, @note, @is_error)";

                        ketqua_ktdl_68_id = Guid.NewGuid();
                        dic6 = new Dictionary<string, object>();
                        dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
                        dic6.Add("type_id", Guid.Parse(hoadon.Rows[0]["hdon_id"].ToString()));
                        dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                        dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                        dic6.Add("xml_cqt", xml);
                        dic6.Add("mltdiep_tchieu", mltdiep_gui);
                        dic6.Add("nguoi_gui", "Cơ quan Thuế");
                        dic6.Add("note", "ktdl");
                        dic6.Add("is_error", "1");

                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

                        // lưu mã lỗi, hướng dẫn xử lý
                        foreach (XmlNode node in ds_LydoHD)
                        {
                            string maloiHD = node["MLoi"].InnerText;
                            string motaHD = node["MTLoi"].InnerText;
                            string hdxlyHD = node["HDXLy"]?.InnerText;

                            parameters2.Clear();
                            parameters2.Add("id", Guid.NewGuid());
                            parameters2.Add("type_id", Guid.Parse(inv_id));
                            parameters2.Add("maloi", maloiHD);
                            parameters2.Add("motaloi", motaHD);
                            parameters2.Add("huongdanxuly", hdxlyHD);

                            parameters2.Add("tgian", DateTime.Now);
                            parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                            parameters2.Add("reference_id", ketqua_ktdl_68_id);

                            string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                                 + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                        }
                    }
                }
            }

            ketQuaHoaDon = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao/LBTHKXDau/DSBTHop/BTHop/DSLHDon/HDon");
            if (ketQuaHoaDon.Count > 0)
            {
                bool daguihettoanbo = true;
                for (int k = 0; k < ketQuaHoaDon.Count; k++)
                {
                    XmlNode nodehoadon = ketQuaHoaDon.Item(k);

                    string sqlHoaDon = "";
                    var ds_LydoHD = nodehoadon.SelectNodes("DSLDo/LDo");
                    var dagui = false;
                    foreach (XmlNode node in ds_LydoHD)
                    {
                        string maloiHD = node["MLoi"].InnerText;
                        if (maloiHD == "40050" || maloiHD == "-4" || maloiHD == "40045")
                        {
                            dagui = true;
                            break;
                        }
                        else
                        {
                            if (daguihettoanbo) daguihettoanbo = false;
                        }
                    }
                    if (!dagui)
                    {
                        sqlHoaDon = $@"Select * from #SCHEMA_NAME#.hoadon68 WHERE khieu=@khieu and shdon=@shdon and hdon_id in (select b.hdon_id
                                                                from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                        join #SCHEMA_NAME#.bangtonghopdl_68_chitiet b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                where a.bangtonghopdl_68_id = @p_bangtonghopdl_68_id::uuid)";

                        string khieu = nodehoadon.SelectSingleNode("KHMSHDon").InnerText + nodehoadon.SelectSingleNode("KHHDon").InnerText;
                        string shdon = nodehoadon.SelectSingleNode("SHDon").InnerText;

                        dic6 = new Dictionary<string, object>();
                        dic6.Add("khieu", khieu);
                        dic6.Add("shdon", shdon);
                        dic6.Add("p_bangtonghopdl_68_id", Guid.Parse(inv_id));
                        DataTable hoadon = _minvoiceDbContext.GetDataTable(sqlHoaDon, CommandType.Text, dic6);

                        // hatm: update trạng thái mã thông điệp vào bảng hoadon68

                        dic6 = new Dictionary<string, object>();
                        dic6.Add("hdon_id", Guid.Parse(hoadon.Rows[0]["hdon_id"].ToString()));

                        sqlUpdate = $@"UPDATE #SCHEMA_NAME#.hoadon68 SET tthai ='Đã ký' WHERE hdon_id=@hdon_id::uuid";
                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic6);

                        sqlInsert = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                             + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, current_timestamp, @nguoi_gui, @note, @is_error)";

                        ketqua_ktdl_68_id = Guid.NewGuid();
                        dic6 = new Dictionary<string, object>();
                        dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
                        dic6.Add("type_id", Guid.Parse(hoadon.Rows[0]["hdon_id"].ToString()));
                        dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                        dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                        dic6.Add("xml_cqt", xml);
                        dic6.Add("mltdiep_tchieu", mltdiep_gui);
                        dic6.Add("nguoi_gui", "Cơ quan Thuế");
                        dic6.Add("note", "ktdl");
                        dic6.Add("is_error", "1");

                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

                        // lưu mã lỗi, hướng dẫn xử lý
                        foreach (XmlNode node in ds_LydoHD)
                        {
                            string maloiHD = node["MLoi"].InnerText;
                            string motaHD = node["MTLoi"].InnerText;
                            string hdxlyHD = node["HDXLy"]?.InnerText;

                            parameters2.Clear();
                            parameters2.Add("id", Guid.NewGuid());
                            parameters2.Add("type_id", Guid.Parse(inv_id));
                            parameters2.Add("maloi", maloiHD);
                            parameters2.Add("motaloi", motaHD);
                            parameters2.Add("huongdanxuly", hdxlyHD);

                            parameters2.Add("tgian", DateTime.Now);
                            parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                            parameters2.Add("reference_id", ketqua_ktdl_68_id);

                            string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                                 + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                        }
                    }
                }
                if (daguihettoanbo)
                {
                    string sqlUpdate2 = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET ket_qua = @ket_qua, error_status = @error_status "
                + " WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                    Dictionary<string, object> dic52 = new Dictionary<string, object>();
                    dic52.Add("ket_qua", 1);
                    dic52.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
                    dic52.Add("error_status", 0);
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate2, CommandType.Text, dic52);
                }
            }

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

        }

        private async Task xuLy204_300(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            XmlNodeList ds_Lydo = doc.GetElementsByTagName("LDo");
            XmlNodeList ds_LyDoChung = doc.GetElementsByTagName("LDTTChung");
            XmlNode ketQua = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/LTBao");

            await _minvoiceDbContext.BeginTransactionAsync();

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau04_68 SET xml_nhan = @xml_rep,"
           + " ntbccqt=@ngay_tbao, mtdiepcqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
           + " WHERE mau04_id = @mau04_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1.Add("xml_rep", xml);
            dic1.Add("mau04_id", Guid.Parse(inv_id));
            dic1.Add("ngay_tbao", DateTime.Now);
            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);

            // lưu kết quả kiểm tra dữ liệu
            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                   + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, current_timestamp, @nguoi_gui, @note, @is_error)";

            Guid ketqua_ktdl_68_id = Guid.NewGuid();
            string is_error = "0";
            if (ds_Lydo.Count > 0 || ketQua.InnerText.ToString() != "2")
            {
                is_error = "1";
            }

            Dictionary<string, object> dic2 = new Dictionary<string, object>();

            dic2.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
            dic2.Add("type_id", Guid.Parse(inv_id));
            dic2.Add("mtdiep_cqt", mtdiep.InnerText);
            dic2.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
            dic2.Add("xml_cqt", xml);
            dic2.Add("mltdiep_tchieu", mltdiep_gui);
            dic2.Add("tgian_gui", DateTime.Now);
            dic2.Add("nguoi_gui", "Cơ quan Thuế");
            dic2.Add("note", "ktdl");
            dic2.Add("is_error", is_error);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic2);

            if (is_error == "1")
            {
                int tttnhan = 2; // Không chấp nhận

                string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan WHERE mau04_id = @mau04_id ";

                Dictionary<string, object> dic3 = new Dictionary<string, object>();

                dic3.Add("mau04_id", Guid.Parse(inv_id));
                dic3.Add("tttnhan", tttnhan);

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, dic3);

                string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                           + " WHERE mau04_id = @mau04_id and hdon_id is not null";
                DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, dic3);

                foreach (DataRow dr in hd_dctt_Table.Rows)
                {
                    string hd_dctt_id = dr["hdon_id"].ToString();
                    string tctbao = dr["tctbao"].ToString();

                    // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                    sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET \n"
                       //+ " tthdon = (CASE WHEN (@tctbao = '1' or @tctbao = '4') and (tthdon in ('1','3')) THEN tthdon_old ELSE tthdon END), \n"
                       + " tthai='TBSS sai định dạng' , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                       + " WHERE hdon_id = @hdon_id ";

                    Dictionary<string, object> dic4 = new Dictionary<string, object>();

                    dic4.Add("hdon_id", Guid.Parse(hd_dctt_id));
                    dic4.Add("mtdiep_cqt", mtdiep.InnerText);
                    dic4.Add("mtdiep_gui", mtdiep_gui.InnerText);
                    dic4.Add("tctbao", tctbao);
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic4);

                    // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                    string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                          + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                    Dictionary<string, object> dic5 = new Dictionary<string, object>();
                    dic5.Add("log_message_id", Guid.NewGuid());
                    dic5.Add("hdon_id", Guid.Parse(hd_dctt_id));
                    dic5.Add("mtdiep_cqt", mtdiep?.InnerText);
                    dic5.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                    dic5.Add("xml_cqt", xml);
                    dic5.Add("loai_tbao", mltdiep.InnerText);
                    dic5.Add("ttchung", ttchung.InnerText);
                    dic5.Add("tgian_gui", DateTime.Now);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic5);

                    // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                    sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                  + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                    Dictionary<string, object> dic6 = new Dictionary<string, object>();
                    dic6.Add("id", Guid.NewGuid());
                    dic6.Add("mtdiep_cqt", mtdiep?.InnerText);
                    dic6.Add("xml_cqt", xml);
                    dic6.Add("mltdiep", mltdiep.InnerText);
                    dic6.Add("tgian_gui", DateTime.Now);
                    dic6.Add("mdvi", mdvi);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

                    // lưu kết quả kiểm tra dữ liệu

                    sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                           + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, current_timestamp, @nguoi_gui, @note, @is_error)";

                    Guid ketqua_ktdl_68_hdon_id = Guid.NewGuid();

                    Dictionary<string, object> dic7 = new Dictionary<string, object>();
                    dic7.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_hdon_id);
                    dic7.Add("type_id", Guid.Parse(hd_dctt_id));
                    dic7.Add("mtdiep_cqt", mtdiep.InnerText);
                    dic7.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                    dic7.Add("xml_cqt", xml);
                    dic7.Add("mltdiep_tchieu", mltdiep_gui);
                    dic7.Add("tgian_gui", DateTime.Now);
                    dic7.Add("nguoi_gui", "Cơ quan Thuế");
                    dic7.Add("note", "ktdl");
                    dic7.Add("is_error", is_error);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic7);

                    try
                    {
                        string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE type_id = @hdon_id";
                        Dictionary<string, object> dic8 = new Dictionary<string, object>();
                        dic8.Add("hdon_id", Guid.Parse(hd_dctt_id));
                        DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, dic8);
                        if (dt_hoadon.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                            {
                                if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                                {
                                    string[] stringList = siteHddt.Split('.');
                                    string str_mst = stringList[0].ToString();
                                    RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                    rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.Message);
                    }
                }

            }

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

        }

        private async Task xuLy204_200(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            XmlNodeList ds_Lydo = doc.GetElementsByTagName("LDo");
            XmlNodeList ds_LyDoChung = doc.GetElementsByTagName("LDTTChung");

            await _minvoiceDbContext.BeginTransactionAsync();

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_cqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui \n"
                               + " WHERE hdon_id = @hdon_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();

            //parameters2.Add("signature", signature.InnerText);
            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);
            dic1.Add("hdon_id", Guid.Parse(inv_id));

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);
            // await _minvoiceDbContext.TransactionCommitAsync();

            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("log_message_id", Guid.NewGuid());
            dic2.Add("hdon_id", Guid.Parse(inv_id));
            dic2.Add("mtdiep_cqt", mtdiep?.InnerText);
            dic2.Add("mtdiep_gui", mtdiep_gui?.InnerText);
            dic2.Add("xml_cqt", xml);
            dic2.Add("loai_tbao", mltdiep.InnerText);
            dic2.Add("ttchung", ttchung.InnerText);
            dic2.Add("tgian_gui", DateTime.Now);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic2);

            // lưu kết quả kiểm tra dữ liệu

            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                   + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, current_timestamp, @nguoi_gui, @note, @is_error)";

            Guid ketqua_ktdl_68_id = Guid.NewGuid();
            string is_error = "0";
            //if (ds_Lydo.Count > 0)
            //{

            is_error = "1";
            // update vào bảng hoadon68
            sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET error_status = (case when tthai = 'Chờ cấp mã' then '1' ELSE error_status END), "
                + "tthai = (case when tthai = 'Chờ cấp mã' then 'CQT không cấp mã' ELSE tthai END) WHERE hdon_id = @hdon_id";
            Dictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3.Add("hdon_id", Guid.Parse(inv_id));
            dic3.Add("mltdtchieu", mltdiep_gui);
            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic3);

            //}

            Dictionary<string, object> dic4 = new Dictionary<string, object>();
            dic4.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
            dic4.Add("type_id", Guid.Parse(inv_id));
            dic4.Add("mtdiep_cqt", mtdiep.InnerText);
            dic4.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
            dic4.Add("xml_cqt", xml);
            dic4.Add("mltdiep_tchieu", mltdiep_gui);
            dic4.Add("tgian_gui", DateTime.Now);
            dic4.Add("nguoi_gui", "Cơ quan Thuế");
            dic4.Add("note", "ktdl");
            dic4.Add("is_error", is_error);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic4);

            foreach (XmlNode node in ds_Lydo)
            {
                // lưu mã lỗi, hướng dẫn xử lý
                string maloi = "";
                string mota = "";
                string hdxly = "";

                Dictionary<string, object> dic5 = new Dictionary<string, object>();

                maloi = node["MLoi"].InnerText;
                mota = node["MTLoi"].InnerText;
                hdxly = node["HDXLy"]?.InnerText;

                dic5.Add("id", Guid.NewGuid());
                dic5.Add("type_id", Guid.Parse(inv_id));

                dic5.Add("maloi", maloi);
                dic5.Add("motaloi", mota);
                dic5.Add("huongdanxuly", hdxly);

                dic5.Add("tgian", DateTime.Now);
                dic5.Add("mtdiep_tchieu", mltdiep_gui);
                dic5.Add("reference_id", ketqua_ktdl_68_id);

                string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                    + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, dic5);
            }

            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

            Dictionary<string, object> dic6 = new Dictionary<string, object>();
            dic6.Add("id", Guid.NewGuid());
            dic6.Add("mtdiep_cqt", mtdiep?.InnerText);
            dic6.Add("xml_cqt", xml);
            dic6.Add("mltdiep", mltdiep.InnerText);
            dic6.Add("tgian_gui", DateTime.Now);
            dic6.Add("mdvi", mdvi);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id
            try
            {
                if (is_api == "1")
                {
                    string[] stringList = siteHddt.Split('.');
                    string str_mst = stringList[0].ToString();
                    RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                    rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

        }

        private async Task xuLy204_203(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            XmlNodeList ds_Lydo = doc.GetElementsByTagName("LDo");
            XmlNodeList ds_LyDoChung = doc.GetElementsByTagName("LDTTChung");

            await _minvoiceDbContext.BeginTransactionAsync();

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_cqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui \n"
                               + " WHERE hdon_id = @hdon_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();

            //parameters2.Add("signature", signature.InnerText);
            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);
            dic1.Add("hdon_id", Guid.Parse(inv_id));

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);
            // await _minvoiceDbContext.TransactionCommitAsync();

            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("log_message_id", Guid.NewGuid());
            dic2.Add("hdon_id", Guid.Parse(inv_id));
            dic2.Add("mtdiep_cqt", mtdiep?.InnerText);
            dic2.Add("mtdiep_gui", mtdiep_gui?.InnerText);
            dic2.Add("xml_cqt", xml);
            dic2.Add("loai_tbao", mltdiep.InnerText);
            dic2.Add("ttchung", ttchung.InnerText);
            dic2.Add("tgian_gui", DateTime.Now);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic2);

            // lưu kết quả kiểm tra dữ liệu

            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                   + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, current_timestamp, @nguoi_gui, @note, @is_error)";

            Guid ketqua_ktdl_68_id = Guid.NewGuid();
            string is_error = "0";
            if (ds_Lydo.Count > 0)
            {
                is_error = "1";
            }

            // update vào bảng hoadon68
            sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET error_status = (case when @is_error = '0' then '0' ELSE (case when error_status = '0' then '0' else '1' end) end), "
                + " tthai = (case when @is_error = '0' then 'CQT đã nhận' ELSE (case when error_status = '0' then 'CQT đã nhận' else 'CQT không tiếp nhận HĐ' end) end) WHERE hdon_id = @hdon_id";
            Dictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3.Add("hdon_id", Guid.Parse(inv_id));
            dic3.Add("mltdtchieu", mltdiep_gui);
            dic3.Add("is_error", is_error);
            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic3);

            Dictionary<string, object> dic4 = new Dictionary<string, object>();
            dic4.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
            dic4.Add("type_id", Guid.Parse(inv_id));
            dic4.Add("mtdiep_cqt", mtdiep.InnerText);
            dic4.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
            dic4.Add("xml_cqt", xml);
            dic4.Add("mltdiep_tchieu", mltdiep_gui);
            dic4.Add("tgian_gui", DateTime.Now);
            dic4.Add("nguoi_gui", "Cơ quan Thuế");
            dic4.Add("note", "ktdl");
            dic4.Add("is_error", is_error);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic4);

            foreach (XmlNode node in ds_Lydo)
            {
                // lưu mã lỗi, hướng dẫn xử lý
                string maloi = "";
                string mota = "";
                string hdxly = "";

                Dictionary<string, object> dic5 = new Dictionary<string, object>();

                maloi = node["MLoi"].InnerText;
                mota = node["MTLoi"].InnerText;
                hdxly = node["HDXLy"]?.InnerText;

                dic5.Add("id", Guid.NewGuid());
                dic5.Add("type_id", Guid.Parse(inv_id));

                dic5.Add("maloi", maloi);
                dic5.Add("motaloi", mota);
                dic5.Add("huongdanxuly", hdxly);

                dic5.Add("tgian", DateTime.Now);
                dic5.Add("mtdiep_tchieu", mltdiep_gui);
                dic5.Add("reference_id", ketqua_ktdl_68_id);

                string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                    + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, dic5);
            }

            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

            Dictionary<string, object> dic6 = new Dictionary<string, object>();
            dic6.Add("id", Guid.NewGuid());
            dic6.Add("mtdiep_cqt", mtdiep?.InnerText);
            dic6.Add("xml_cqt", xml);
            dic6.Add("mltdiep", mltdiep.InnerText);
            dic6.Add("tgian_gui", DateTime.Now);
            dic6.Add("mdvi", mdvi);

            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id
            try
            {
                if (is_api == "1")
                {
                    string[] stringList = siteHddt.Split('.');
                    string str_mst = stringList[0].ToString();
                    RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                    rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

        }

        private async Task xuLy301(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
            XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            XmlNode ldo = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/DSLDKTNhan");

            XmlNodeList dltbao = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao");

            await _minvoiceDbContext.BeginTransactionAsync();

            // get mau04_id trong bảng mau04_68_chietiet

            string sqlSelect_mau04chitiet = $"SELECT mau04_id, khieu, shdon, ladhddt FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                           + " WHERE mau04_id = @mau04_id";
            Dictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1.Add("mau04_id", Guid.Parse(inv_id));

            DataTable mau04_chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04chitiet, CommandType.Text, dic1);

            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau04_68 SET xml_nhan = @xml_rep,"
                + " ntbccqt=@ngay_tbao,ly_do = @ly_do, mtdiepcqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                + " WHERE mau04_id = @mau04_id";
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("xml_rep", xml);
            //parameters2.Add("mau04_id", hdon_id);
            dic2.Add("mau04_id", Guid.Parse(inv_id));
            dic2.Add("ngay_tbao", DateTime.Now);
            dic2.Add("mtdiep_cqt", mtdiep.InnerText);
            dic2.Add("mtdiep_gui", mtdiep_gui.InnerText);
            dic2.Add("ly_do", ldo?.OuterXml);

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic2);

            foreach (XmlNode dl in dltbao)
            {
                XmlNode ttchung_301 = dl["DSHDon"];
                if (ttchung_301 == null)
                {
                    if (mau04_chitiet_Table.Rows.Count > 0)
                    {
                        //string khhdon = mau04_chitiet_Table.Rows[0]["khieu"].ToString();
                        //string khmshdon = khhdon.Substring(0, 1);
                        //string shdon = mau04_chitiet_Table.Rows[0]["shdon"].ToString();
                        //string ladhddt = mau04_chitiet_Table.Rows[0]["ladhddt"].ToString();

                        // cập nhật hóa đơn là chấp nhận

                        string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan, ly_do=@ly_do "
                                   + " WHERE mau04_id = @mau04_id ";

                        Dictionary<string, object> dic3 = new Dictionary<string, object>();

                        dic3.Add("mau04_id", Guid.Parse(inv_id));
                        dic3.Add("ly_do", "");
                        dic3.Add("tttnhan", 1);

                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, dic3);

                        string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                    + " WHERE mau04_id = @mau04_id and hdon_id is not null";
                        DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, dic3);

                        foreach (DataRow dr in hd_dctt_Table.Rows)
                        {
                            string hd_dctt_id = dr["hdon_id"].ToString();
                            string tctbao = dr["tctbao"].ToString();

                            // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                            sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET \n"
                               + " tthdon = (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END), \n"
                               + " tthdon_old = (CASE WHEN @tctbao = '1' THEN tthdon WHEN @tctbao = '4' THEN tthdon ELSE tthdon_old END), \n"
                               + " tthai=@tthai , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                               + " WHERE hdon_id = @hdon_id ";

                            Dictionary<string, object> dic4 = new Dictionary<string, object>();

                            dic4.Add("hdon_id", Guid.Parse(hd_dctt_id));
                            dic4.Add("tthai", "Chấp nhận TBSS");
                            dic4.Add("mtdiep_cqt", mtdiep.InnerText);
                            dic4.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            dic4.Add("tctbao", tctbao);
                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic4);

                            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                            Dictionary<string, object> dic5 = new Dictionary<string, object>();
                            dic5.Add("log_message_id", Guid.NewGuid());
                            dic5.Add("hdon_id", Guid.Parse(hd_dctt_id));
                            dic5.Add("mtdiep_cqt", mtdiep?.InnerText);
                            dic5.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                            dic5.Add("xml_cqt", xml);
                            dic5.Add("loai_tbao", mltdiep.InnerText);
                            dic5.Add("ttchung", ttchung.InnerText);
                            dic5.Add("tgian_gui", DateTime.Now);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic5);

                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                            Dictionary<string, object> dic6 = new Dictionary<string, object>();
                            dic6.Add("id", Guid.NewGuid());
                            dic6.Add("mtdiep_cqt", mtdiep?.InnerText);
                            dic6.Add("xml_cqt", xml);
                            dic6.Add("mltdiep", mltdiep.InnerText);
                            dic6.Add("tgian_gui", DateTime.Now);
                            dic6.Add("mdvi", mdvi);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

                            // lưu kết quả xử lý hóa đơn sai sót

                            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_xulyhdsaisot_68 (ketqua_xulyhdsaisot_68_id, mltdiepcqt, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, tttnhan_cqt, note) VALUES "
                                   + " (@ketqua_xulyhdsaisot_68_id, @mltdiepcqt, @type_id, @nguoi_gui, current_timestamp, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @tttnhan_cqt, @note)";

                            Guid ketqua_xulyhdsaisot_68_id = Guid.NewGuid();

                            Dictionary<string, object> dic7 = new Dictionary<string, object>();
                            dic7.Add("ketqua_xulyhdsaisot_68_id", ketqua_xulyhdsaisot_68_id);
                            dic7.Add("mltdiepcqt", mltdiep.InnerText);
                            dic7.Add("type_id", Guid.Parse(hd_dctt_id));
                            dic7.Add("nguoi_gui", "Cơ quan Thuế");
                            dic7.Add("tgian_gui", DateTime.Now);
                            dic7.Add("mtdiep_cqt", mtdiep?.InnerText);
                            dic7.Add("mtdtchieu", mtdiep_gui?.InnerText);
                            dic7.Add("xml_cqt", xml);
                            dic7.Add("tttnhan_cqt", "Tiếp nhận");
                            dic7.Add("note", "saisot");

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic7);

                            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

                            try
                            {
                                string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 \n"
                                         + " WHERE type_id = @hdon_id";
                                Dictionary<string, object> dic8 = new Dictionary<string, object>();
                                dic8.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, dic8);
                                if (dt_hoadon.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                                    {
                                        if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                                        {
                                            string[] stringList = siteHddt.Split('.');
                                            string str_mst = stringList[0].ToString();
                                            RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                            rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.Message);
                            }
                        }

                    }
                }
                else
                {
                    // nếu kết quả CQT 301 trả về có hóa đơn lỗi thì update hóa đơn lỗi là không chấp nhận

                    List<string> listHdonLoi = new List<string>();

                    XmlNodeList dsHoaDon = doc.GetElementsByTagName("HDon");

                    if (dsHoaDon.Count > 0)
                    {
                        foreach (XmlNode hoadon in dsHoaDon)
                        {
                            string xml_hdon = hoadon.OuterXml;
                            XmlDocument docHd = new XmlDocument();
                            docHd.PreserveWhitespace = true;
                            docHd.LoadXml(xml_hdon);

                            XmlNodeList ds_Lydo = docHd.GetElementsByTagName("LDo");
                            //string tttnccqt = docHd.GetElementsByTagName("TTTNCCQT")[0].InnerText;

                            //XmlNodeList ds_Lydo = hoadon.SelectNodes("HDon/DSLDKTNhan/LDo");
                            //XmlNode tttnccqt = hoadon.SelectSingleNode("/DSLDKTNhan/TTTNCCQT");

                            //string status_Tiepnhan = doc.GetElementsByTagName("TTTNCCQT")[0].InnerText;

                            string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan, ly_do=@ly_do "
                                + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt";

                            Dictionary<string, object> dic9 = new Dictionary<string, object>();
                            //dic9.Add("mau04_id", hdon_id);
                            dic9.Add("mau04_id", Guid.Parse(inv_id));
                            dic9.Add("ly_do", hoadon.SelectSingleNode("DSLDKTNhan")?.OuterXml);
                            dic9.Add("tttnhan", Convert.ToInt32(hoadon.SelectSingleNode("TTTNCCQT")?.InnerText));
                            dic9.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                            //dic9.Add("khieu", hoadon.SelectSingleNode("KHHDon")?.InnerText);
                            dic9.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                            dic9.Add("ladhddt", Convert.ToInt32(hoadon.SelectSingleNode("LADHDDT")?.InnerText));
                            //dic9.Add("tctbao", Convert.ToInt32(hoadon.SelectSingleNode("TCTBao")?.InnerText));

                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, dic9);

                            //int tttnhan = 2;
                            string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                        + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt and hdon_id is not null";
                            DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, dic9);

                            foreach (DataRow dr in hd_dctt_Table.Rows)
                            {
                                string hd_dctt_id = dr["hdon_id"].ToString();
                                string tctbao = dr["tctbao"].ToString();

                                listHdonLoi.Add(hd_dctt_id);

                                // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                                sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET \n"
                                   //+ " tthdon = (CASE WHEN (@tctbao = '1' or @tctbao = '4') and (tthdon in ('1','3')) THEN tthdon_old ELSE tthdon END), \n"
                                   + " tthai=@tthai , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                                   + " WHERE hdon_id = @hdon_id ";

                                Dictionary<string, object> dic10 = new Dictionary<string, object>();

                                dic10.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                dic10.Add("tthai", "Không chấp nhận TBSS");
                                //dic10.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                //dic10.Add("khieu", hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                //dic10.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                                dic10.Add("mtdiep_cqt", mtdiep.InnerText);
                                dic10.Add("mtdiep_gui", mtdiep_gui.InnerText);
                                //dic10.Add("tctbao", tctbao);
                                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic10);

                                // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                      + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                                Dictionary<string, object> dic11 = new Dictionary<string, object>();
                                dic11.Add("log_message_id", Guid.NewGuid());
                                dic11.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                dic11.Add("mtdiep_cqt", mtdiep?.InnerText);
                                dic11.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                                dic11.Add("xml_cqt", xml);
                                dic11.Add("loai_tbao", mltdiep.InnerText);
                                dic11.Add("ttchung", ttchung.InnerText);
                                dic11.Add("tgian_gui", DateTime.Now);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic11);

                                // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                                sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                              + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                                Dictionary<string, object> dic12 = new Dictionary<string, object>();
                                dic12.Add("id", Guid.NewGuid());
                                dic12.Add("mtdiep_cqt", mtdiep?.InnerText);
                                dic12.Add("xml_cqt", xml);
                                dic12.Add("mltdiep", mltdiep.InnerText);
                                dic12.Add("tgian_gui", DateTime.Now);
                                dic12.Add("mdvi", mdvi);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic12);

                                // lưu kết quả xử lý hóa đơn sai sót

                                string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_xulyhdsaisot_68 (ketqua_xulyhdsaisot_68_id, mltdiepcqt, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, tttnhan_cqt, note) VALUES "
                                       + " (@ketqua_xulyhdsaisot_68_id, @mltdiepcqt, @type_id, @nguoi_gui, current_timestamp, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @tttnhan_cqt, @note)";

                                Guid ketqua_xulyhdsaisot_68_id = Guid.NewGuid();

                                Dictionary<string, object> dic13 = new Dictionary<string, object>();
                                dic13.Add("ketqua_xulyhdsaisot_68_id", ketqua_xulyhdsaisot_68_id);
                                dic13.Add("mltdiepcqt", mltdiep.InnerText);
                                dic13.Add("type_id", Guid.Parse(hd_dctt_id));
                                dic13.Add("nguoi_gui", "Cơ quan Thuế");
                                dic13.Add("tgian_gui", DateTime.Now);
                                dic13.Add("mtdiep_cqt", mtdiep?.InnerText);
                                dic13.Add("mtdtchieu", mtdiep_gui?.InnerText);
                                dic13.Add("xml_cqt", xml);
                                dic13.Add("tttnhan_cqt", "Không tiếp nhận");
                                dic13.Add("note", "saisot");

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic13);

                                //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

                                try
                                {
                                    string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 \n"
                                               + " WHERE type_id = @hdon_id";
                                    Dictionary<string, object> dic14 = new Dictionary<string, object>();
                                    dic14.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                    DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, dic14);
                                    if (dt_hoadon.Rows.Count > 0)
                                    {
                                        if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                                        {
                                            if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                                            {
                                                string[] stringList = siteHddt.Split('.');
                                                string str_mst = stringList[0].ToString();
                                                RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                                rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                                            }
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error(ex.Message);
                                }

                                // nếu trạng thái không tiếp nhận, ghi log mã lỗi

                                foreach (XmlNode node in ds_Lydo)
                                {
                                    string maloi = "";
                                    string mota = "";

                                    Dictionary<string, object> dic15 = new Dictionary<string, object>();
                                    dic15.Add("id", Guid.NewGuid());
                                    dic15.Add("type_id", Guid.Parse(inv_id));

                                    maloi = node["MLoi"].InnerText;
                                    mota = node["MTa"].InnerText;
                                    dic15.Add("maloi", maloi);
                                    dic15.Add("motaloi", mota);
                                    dic15.Add("tgian", DateTime.Now);
                                    dic15.Add("mtdiep_tchieu", mtdiep_gui?.InnerText);
                                    dic15.Add("reference_id", ketqua_xulyhdsaisot_68_id);

                                    string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, reference_id) VALUES "
                                                        + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @reference_id)";
                                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, dic15);
                                }

                            }


                        }
                    }

                    if (listHdonLoi.Count > 0)
                    {
                        string strHdonLoi = string.Join("','", listHdonLoi);
                        strHdonLoi = "('" + strHdonLoi + "')";

                        string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan, ly_do=@ly_do "
                                   + " WHERE mau04_id = @mau04_id and hdon_id is not null and hdon_id not in " + strHdonLoi;

                        Dictionary<string, object> dic3 = new Dictionary<string, object>();

                        dic3.Add("mau04_id", Guid.Parse(inv_id));
                        dic3.Add("ly_do", "");
                        dic3.Add("tttnhan", 1);

                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, dic3);

                        string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                    + " WHERE mau04_id = @mau04_id and hdon_id is not null and hdon_id not in " + strHdonLoi;

                        DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, dic3);

                        foreach (DataRow dr in hd_dctt_Table.Rows)
                        {
                            string hd_dctt_id = dr["hdon_id"].ToString();
                            string tctbao = dr["tctbao"].ToString();

                            // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                            sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET \n"
                               + " tthdon = (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END), \n"
                               + " tthdon_old = (CASE WHEN @tctbao = '1' THEN tthdon WHEN @tctbao = '4' THEN tthdon ELSE tthdon_old END), \n"
                               + " tthai=@tthai , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                               + " WHERE hdon_id = @hdon_id ";

                            Dictionary<string, object> dic4 = new Dictionary<string, object>();

                            dic4.Add("hdon_id", Guid.Parse(hd_dctt_id));
                            dic4.Add("tthai", "Chấp nhận TBSS");
                            dic4.Add("mtdiep_cqt", mtdiep.InnerText);
                            dic4.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            dic4.Add("tctbao", tctbao);
                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic4);

                            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                            Dictionary<string, object> dic5 = new Dictionary<string, object>();
                            dic5.Add("log_message_id", Guid.NewGuid());
                            dic5.Add("hdon_id", Guid.Parse(hd_dctt_id));
                            dic5.Add("mtdiep_cqt", mtdiep?.InnerText);
                            dic5.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                            dic5.Add("xml_cqt", xml);
                            dic5.Add("loai_tbao", mltdiep.InnerText);
                            dic5.Add("ttchung", ttchung.InnerText);
                            dic5.Add("tgian_gui", DateTime.Now);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic5);

                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                            Dictionary<string, object> dic6 = new Dictionary<string, object>();
                            dic6.Add("id", Guid.NewGuid());
                            dic6.Add("mtdiep_cqt", mtdiep?.InnerText);
                            dic6.Add("xml_cqt", xml);
                            dic6.Add("mltdiep", mltdiep.InnerText);
                            dic6.Add("tgian_gui", DateTime.Now);
                            dic6.Add("mdvi", mdvi);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

                            // lưu kết quả xử lý hóa đơn sai sót

                            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_xulyhdsaisot_68 (ketqua_xulyhdsaisot_68_id, mltdiepcqt, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, tttnhan_cqt, note) VALUES "
                                   + " (@ketqua_xulyhdsaisot_68_id, @mltdiepcqt, @type_id, @nguoi_gui, current_timestamp, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @tttnhan_cqt, @note)";

                            Guid ketqua_xulyhdsaisot_68_id = Guid.NewGuid();

                            Dictionary<string, object> dic7 = new Dictionary<string, object>();
                            dic7.Add("ketqua_xulyhdsaisot_68_id", ketqua_xulyhdsaisot_68_id);
                            dic7.Add("mltdiepcqt", mltdiep.InnerText);
                            dic7.Add("type_id", Guid.Parse(hd_dctt_id));
                            dic7.Add("nguoi_gui", "Cơ quan Thuế");
                            dic7.Add("tgian_gui", DateTime.Now);
                            dic7.Add("mtdiep_cqt", mtdiep?.InnerText);
                            dic7.Add("mtdtchieu", mtdiep_gui?.InnerText);
                            dic7.Add("xml_cqt", xml);
                            dic7.Add("tttnhan_cqt", "Tiếp nhận");
                            dic7.Add("note", "saisot");

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic7);

                            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

                            try
                            {
                                string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 \n"
                                         + " WHERE type_id = @hdon_id";
                                Dictionary<string, object> dic8 = new Dictionary<string, object>();
                                dic8.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, dic8);
                                if (dt_hoadon.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                                    {
                                        if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                                        {
                                            string[] stringList = siteHddt.Split('.');
                                            string str_mst = stringList[0].ToString();
                                            RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                            rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.Message);
                            }
                        }

                    }

                }
            }

            await _minvoiceDbContext.TransactionCommitAsync();
            _minvoiceDbContext.CloseTransaction();

        }

        public async Task SendEmail(string inv_invoiceauth_id, string siteHddt)
        {
            try
            {
                EmailSend email = new EmailSend();
                await email.SendInvoiceByEmail(inv_invoiceauth_id, "Gửi hóa đơn", siteHddt);
            }
            catch
            {

            }
        }

        public void CloseTask()
        {
            _channel.Close();
        }


        public bool IsOpen()
        {
            return _channel.IsOpen;
        }


        public string GetTaskName()
        {
            return _queueName;
        }
    }
}
