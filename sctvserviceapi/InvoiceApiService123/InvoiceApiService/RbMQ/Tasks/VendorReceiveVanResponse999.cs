﻿using InvoiceApiService.Data;
using InvoiceApiService.Util;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace InvoiceApiService.RbMQ.Tasks
{
    public class VendorReceiveVanResponse999 : IRabbitTask
    {
        private string _queueName = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_999;

        private IModel _channel;

        public VendorReceiveVanResponse999()
        {
            InitTask();
        }

        private void InitTask()
        {
            string vendor = CommonConstants.VENDOR;
            RabbitClient client = RabbitClient.GetInstance();
            var conn = client.GetConnection();

            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("x-queue-type", "quorum");

            _channel = conn.CreateModel();
            _channel.QueueDeclare(_queueName + "_" + vendor, true, false, false, args);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(_channel);
            var consumerTag = Guid.NewGuid().ToString();
            consumer.Received += VanConfirmTechnical;

            _channel.BasicConsume(_queueName + "_" + vendor, false, consumerTag, false, false, null, consumer);
        }

        private async void VanConfirmTechnical(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var channel = ((EventingBasicConsumer)sender).Model;

            IMinvoiceDbContext _minvoiceDbContext = null;

            try
            {
                _minvoiceDbContext = new MInvoiceDbContext();
                JObject rabbitObj = JObject.Parse(message);
                string xml = rabbitObj["xml"].ToString();
                string siteHddt = rabbitObj["siteHddt"].ToString();

                _minvoiceDbContext.SetSiteHddt(siteHddt);

                //Message thuế trả về ở đây;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
                XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");

                //string xmlVanConfirm = doc.GetElementsByTagName("TDiep")[0].OuterXml;

                if (mltdiep.InnerText == "-1")
                {
                    XmlDocument docVan = new XmlDocument();
                    docVan.PreserveWhitespace = true;
                    docVan.LoadXml(xml);

                    string status_Tiepnhan = "1"; // 0 : tiếp nhận; 1: không tiếp nhận

                    string mloi = docVan.GetElementsByTagName("MLoi")[0].InnerText;
                    string mtaloi = docVan.GetElementsByTagName("MTa")[0].InnerText;

                    await _minvoiceDbContext.BeginTransactionAsync();

                    // get hdon id từ mã thông điệp thuế trả về
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("mtdiep_gui", mtdiep_gui?.InnerText);

                    string sqlSelectt = $"SELECT * FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE mtdiep_gui = @mtdiep_gui ";

                    DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(sqlSelectt, CommandType.Text, parameters2);
                    if (tbl.Rows.Count > 0)
                    {
                        for (int i = 0; i < tbl.Rows.Count; i++)
                        {
                            string inv_id = "";
                            string mltdiep_gui = "";

                            inv_id = tbl.Rows[i]["type_id"].ToString();
                            mltdiep_gui = tbl.Rows[i]["mltdiep_gui"].ToString();

                            parameters2.Clear();
                            parameters2.Add("status", status_Tiepnhan);
                            parameters2.Add("message", mtaloi);
                            parameters2.Add("inv_id", Guid.Parse(inv_id));

                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.tonghop_gui_tvan_68 SET status=@status, message=@message WHERE type_id = @inv_id";
                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                            // lưu kết quả phản hồi kĩ thuật

                            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_phkt_68 (ketqua_phkt_68_id, mltdtchieu, type_id, nguoi_gui, tgian_gui, mtdtchieu, note, is_error) VALUES "
                                   + " (@ketqua_phkt_68_id, @mltdtchieu, @type_id, @nguoi_gui, current_timestamp, @mtdtchieu, @note, @is_error)";

                            Guid ketqua_phkt_68_id = Guid.NewGuid();

                            string is_error = "0";
                            if (status_Tiepnhan != "0")
                            {
                                is_error = "1";

                                inv_id = tbl.Rows[i]["type_id"].ToString();
                                mltdiep_gui = tbl.Rows[i]["mltdiep_gui"].ToString();
                                string mdvi = tbl.Rows[i]["mdvi"]?.ToString();
                                string is_api = tbl.Rows[i]["is_api"]?.ToString();

                                if (mltdiep_gui == "400")
                                {
                                    await xuLy204_400(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api);
                                }
                                else if (mltdiep_gui == "300")
                                {
                                    await xuLy204_300(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                                }
                                else if (mltdiep_gui == "200")
                                {
                                    await xuLy204_200(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                                }
                                else
                                {
                                    await xuLy204_203(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                                }

                            }

                            Dictionary<string, object> dic6 = new Dictionary<string, object>();
                            dic6.Add("ketqua_phkt_68_id", ketqua_phkt_68_id);
                            dic6.Add("mltdtchieu", mltdiep_gui);
                            dic6.Add("type_id", Guid.Parse(inv_id));
                            dic6.Add("nguoi_gui", mltdiep.InnerText == "999V" ? "MobiFone TVAN" : "Cơ quan Thuế");
                            dic6.Add("tgian_gui", DateTime.Now);
                            dic6.Add("mtdtchieu", mtdiep_gui?.InnerText);
                            dic6.Add("note", "phkt");
                            dic6.Add("is_error", is_error);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic6);

                            // nếu không tiếp nhận, update vào bảng tonghopmaloiphanhoi_68
                            if (status_Tiepnhan != "0")
                            {
                                parameters2.Clear();
                                parameters2.Add("id", Guid.NewGuid());
                                parameters2.Add("type_id", Guid.Parse(inv_id));

                                parameters2.Add("maloi", mloi);
                                parameters2.Add("motaloi", mtaloi);
                                parameters2.Add("tgian", DateTime.Now);
                                parameters2.Add("mtdiep_tchieu", mtdiep_gui?.InnerText);
                                parameters2.Add("reference_id", ketqua_phkt_68_id);

                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, reference_id) VALUES "
                                                    + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @reference_id)";
                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, parameters2);

                            }

                        }
                        //}

                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                else
                {
                    XmlDocument docVan = new XmlDocument();
                    docVan.PreserveWhitespace = true;
                    docVan.LoadXml(xml);

                    string status_Tiepnhan = docVan.GetElementsByTagName("TTTNhan")[0].InnerText; // 0 : tiếp nhận; 1: không tiếp nhận
                                                                                                  //string ds_Lydo = docVan.GetElementsByTagName("DSLdo")[0].InnerText;

                    XmlNodeList ds_Lydo = docVan.GetElementsByTagName("LDo");

                    await _minvoiceDbContext.BeginTransactionAsync();

                    // get hdon id từ mã thông điệp thuế trả về
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("mtdiep_gui", mtdiep_gui?.InnerText);

                    string sqlSelectt = $"SELECT * FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE mtdiep_gui = @mtdiep_gui ";

                    DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(sqlSelectt, CommandType.Text, parameters2);
                    if (tbl.Rows.Count > 0)
                    {
                        for (int i = 0; i < tbl.Rows.Count; i++)
                        {
                            string inv_id = "";
                            string mltdiep_gui = "";

                            inv_id = tbl.Rows[i]["type_id"].ToString();
                            mltdiep_gui = tbl.Rows[i]["mltdiep_gui"].ToString();

                            parameters2.Clear();
                            parameters2.Add("status", status_Tiepnhan);
                            parameters2.Add("message", ds_Lydo.Count);
                            parameters2.Add("inv_id", Guid.Parse(inv_id));

                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.tonghop_gui_tvan_68 SET status=@status, message=@message WHERE type_id = @inv_id";
                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                            // lưu kết quả phản hồi kĩ thuật

                            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_phkt_68 (ketqua_phkt_68_id, mltdtchieu, type_id, nguoi_gui, tgian_gui, mtdtchieu, note, is_error) VALUES "
                                   + " (@ketqua_phkt_68_id, @mltdtchieu, @type_id, @nguoi_gui, current_timestamp, @mtdtchieu, @note, @is_error)";

                            Guid ketqua_phkt_68_id = Guid.NewGuid();

                            string is_error = "0";
                            if (status_Tiepnhan != "0")
                            {
                                is_error = "1";

                                inv_id = tbl.Rows[i]["type_id"].ToString();
                                mltdiep_gui = tbl.Rows[i]["mltdiep_gui"].ToString();
                                string mdvi = tbl.Rows[i]["mdvi"]?.ToString();
                                string is_api = tbl.Rows[i]["is_api"]?.ToString();


                                if (mltdiep_gui == "400")
                                {
                                    await xuLy204_400(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api);
                                }
                                else if (mltdiep_gui == "300")
                                {
                                    await xuLy204_300(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                                }
                                else if (mltdiep_gui == "200")
                                {
                                    await xuLy204_200(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                                }
                                else
                                {
                                    await xuLy204_203(_minvoiceDbContext, xml, doc, inv_id, mltdiep_gui, mdvi, is_api, siteHddt);
                                }

                            }

                            Dictionary<string, object> dic6 = new Dictionary<string, object>();
                            dic6.Add("ketqua_phkt_68_id", ketqua_phkt_68_id);
                            dic6.Add("mltdtchieu", mltdiep_gui);
                            dic6.Add("type_id", Guid.Parse(inv_id));
                            dic6.Add("nguoi_gui", mltdiep.InnerText == "999V" ? "MobiFone TVAN" : "Cơ quan Thuế");
                            dic6.Add("tgian_gui", DateTime.Now);
                            dic6.Add("mtdtchieu", mtdiep_gui?.InnerText);
                            dic6.Add("note", "phkt");
                            dic6.Add("is_error", is_error);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic6);

                            // nếu không tiếp nhận, update vào bảng tonghopmaloiphanhoi_68
                            if (status_Tiepnhan != "0")
                            {

                                string maloi = "";
                                string mota = "";

                                foreach (XmlNode node in ds_Lydo)
                                {
                                    parameters2.Clear();
                                    parameters2.Add("id", Guid.NewGuid());
                                    parameters2.Add("type_id", Guid.Parse(inv_id));

                                    maloi = node["MLoi"]?.InnerText;
                                    mota = node["MTa"]?.InnerText;
                                    parameters2.Add("maloi", maloi);
                                    parameters2.Add("motaloi", mota);
                                    parameters2.Add("tgian", DateTime.Now);
                                    parameters2.Add("mtdiep_tchieu", mtdiep_gui?.InnerText);
                                    parameters2.Add("reference_id", ketqua_phkt_68_id);

                                    string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, reference_id) VALUES "
                                                        + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @reference_id)";
                                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, parameters2);
                                }
                            }

                        }
                        //}

                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }

                channel.BasicAck(e.DeliveryTag, false);

            }
            catch (Exception ex)
            {
                Log.Error(_queueName + ": " + ex.Message);
                Log.Error(message);

                if (_minvoiceDbContext != null)
                {
                    try
                    {
                        channel.BasicAck(e.DeliveryTag, false);
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionCommitAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        _minvoiceDbContext.CloseTransaction();
                    }
                    catch (Exception) { }
                }
            }

        }

        private async Task xuLy204_400(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api)
        {
            //await _minvoiceDbContext.BeginTransactionAsync();

            string sqlGetBTHDL = $"select * from #SCHEMA_NAME#.bangtonghopdl_68 where bangtonghopdl_68_id = '{inv_id}'::uuid";

            DataTable data = _minvoiceDbContext.GetDataTable(sqlGetBTHDL, CommandType.Text);

            // bảng BTJDL error_status = 1 lỗi toàn bộ bảng,  error_status = 2 lỗi 1 số hóa đơn
            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET xml_nhan = @xml_rep, ket_qua = @ket_qua, "
               + " ngay_tbao=@ngay_tbao, error_status = 1 "
               + " WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";

            Dictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1.Add("xml_rep", xml);
            dic1.Add("ket_qua", 2); // 2 là tiếp nhận => 1 | còn lại là lỗi => 2
            dic1.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
            dic1.Add("ngay_tbao", DateTime.Now);
           
            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);

            JObject param_update_hoadon = new JObject();
            param_update_hoadon.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));

            if (data.Rows[0]["lhhoa"].ToString() != "1")
            {
                string sqlUpdate3 = $@"update #SCHEMA_NAME#.hoadon68
                                                            set tthai = 'Đã ký'
                                                            where hdon_id in (select b.hdon_id
                                                                 from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                          join #SCHEMA_NAME#.bangtonghopdl_68_chitiet b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                 where a.bangtonghopdl_68_id = @bangtonghopdl_68_id::uuid ) ";
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate3, CommandType.Text, param_update_hoadon);
            }
            else
            {
                string sqlUpdate3 = $@"update #SCHEMA_NAME#.hoadon68 x
                                                            set tthai = 'Đã ký'
                                                            where (select count(*) from (select b.hdon_id
                                                                 from #SCHEMA_NAME#.bangtonghopdl_68 a
                                                                          join #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd b on a.bangtonghopdl_68_id = b.bangtonghopdl_68_id
                                                                 where a.bangtonghopdl_68_id = @bangtonghopdl_68_id::uuid and position(x.hdon_id::text in b.hdon_id) >0)a) >0 ";
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate3, CommandType.Text, param_update_hoadon);
            }

            //await _minvoiceDbContext.TransactionCommitAsync();
            //_minvoiceDbContext.CloseTransaction();

        }

        private async Task xuLy204_300(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
            XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");

            //await _minvoiceDbContext.BeginTransactionAsync();

            int tttnhan = 2; // Không chấp nhận

            string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan WHERE mau04_id = @mau04_id ";

            Dictionary<string, object> dic3 = new Dictionary<string, object>();

            dic3.Add("mau04_id", Guid.Parse(inv_id));
            dic3.Add("tttnhan", tttnhan);

            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, dic3);

            string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                       + " WHERE mau04_id = @mau04_id and hdon_id is not null";
            DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, dic3);

            if (hd_dctt_Table.Rows.Count > 0)
            {
                string hd_dctt_id = hd_dctt_Table.Rows[0]["hdon_id"].ToString();
                string tctbao = hd_dctt_Table.Rows[0]["tctbao"].ToString();

                // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                string sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET \n"
                   //+ " tthdon = (CASE WHEN (@tctbao = '1' or @tctbao = '4') and (tthdon in ('1','3')) THEN tthdon_old ELSE tthdon END), \n"
                   + " tthai='TBSS sai định dạng' , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                   + " WHERE hdon_id = @hdon_id ";

                Dictionary<string, object> dic4 = new Dictionary<string, object>();

                dic4.Add("hdon_id", Guid.Parse(hd_dctt_id));
                dic4.Add("mtdiep_cqt", mtdiep.InnerText);
                dic4.Add("mtdiep_gui", mtdiep_gui.InnerText);
                dic4.Add("tctbao", tctbao);
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic4);

                try
                {
                    string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE type_id = @hdon_id";
                    Dictionary<string, object> dic8 = new Dictionary<string, object>();
                    dic8.Add("hdon_id", Guid.Parse(hd_dctt_id));
                    DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, dic8);
                    if (dt_hoadon.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                        {
                            if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                            {
                                string[] stringList = siteHddt.Split('.');
                                string str_mst = stringList[0].ToString();
                                RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }

            }

            //await _minvoiceDbContext.TransactionCommitAsync();
            //_minvoiceDbContext.CloseTransaction();

        }

        private async Task xuLy204_200(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            //await _minvoiceDbContext.BeginTransactionAsync();

            // update vào bảng hoadon68
            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET error_status = (case when tthai = 'Chờ cấp mã' then '1' ELSE error_status END), "
                + "tthai = (case when tthai = 'Chờ cấp mã' then 'CQT không cấp mã' ELSE tthai END) WHERE hdon_id = @hdon_id";
            Dictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3.Add("hdon_id", Guid.Parse(inv_id));
            dic3.Add("mltdtchieu", mltdiep_gui);
            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic3);

            //await _minvoiceDbContext.TransactionCommitAsync();
            //_minvoiceDbContext.CloseTransaction();

            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id
            try
            {
                if (is_api == "1")
                {
                    string[] stringList = siteHddt.Split('.');
                    string str_mst = stringList[0].ToString();
                    RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                    rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

        }

        private async Task xuLy204_203(IMinvoiceDbContext _minvoiceDbContext, string xml, XmlDocument doc, string inv_id, string mltdiep_gui, string mdvi, string is_api, string siteHddt)
        {
            //await _minvoiceDbContext.BeginTransactionAsync();
            string is_error = "1";

            // update vào bảng hoadon68
            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET error_status = (case when @is_error = '0' then '0' ELSE (case when error_status = '0' then '0' else '1' end) end), "
                + " tthai = (case when @is_error = '0' then 'CQT đã nhận' ELSE (case when error_status = '0' then 'CQT đã nhận' else 'CQT không tiếp nhận HĐ' end) end) WHERE hdon_id = @hdon_id";
            Dictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3.Add("hdon_id", Guid.Parse(inv_id));
            dic3.Add("mltdtchieu", mltdiep_gui);
            dic3.Add("is_error", is_error);
            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic3);

            //await _minvoiceDbContext.TransactionCommitAsync();
            //_minvoiceDbContext.CloseTransaction();

            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id
            try
            {
                if (is_api == "1")
                {
                    string[] stringList = siteHddt.Split('.');
                    string str_mst = stringList[0].ToString();
                    RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                    rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

        }

        public void CloseTask()
        {
            _channel.Close();
        }


        public bool IsOpen()
        {
            return _channel.IsOpen;
        }


        public string GetTaskName()
        {
            return _queueName;
        }
    }
}
