﻿using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraReports.UI;
using ICSharpCode.SharpZipLib.Zip;
using InvoiceApiService.Util;
using InvoiceApiService.Data;
using InvoiceApiService.Email;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace InvoiceApiService.Util
{
    public class EmailSend
    {
        IMinvoiceDbContext _minvoiceDbContext = new MInvoiceDbContext();

        public async Task<string> GetInvoiceFileName(string id)
        {
            string fileName = "";
            string mst = "";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("hdon_id", Guid.Parse(id));

            DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id", CommandType.Text, parameters);

            //get mẫu ký hiệu, số HD, mã đơn vị
            string khieu = tblInv_InvoiceAuth.Rows[0]["khieu"].ToString();
            string shdon = tblInv_InvoiceAuth.Rows[0]["shdon"].ToString();
            string ma_dvcs = tblInv_InvoiceAuth.Rows[0]["mdvi"].ToString();

            //get MST
            string qryMST = $"SELECT tax_code FROM #SCHEMA_NAME#.wb_branch WHERE code='{ma_dvcs}'";
            DataTable dmdvcs = await _minvoiceDbContext.GetDataTableAsync(qryMST);
            if (dmdvcs.Rows.Count > 0)
            {
                mst = dmdvcs.Rows[0]["tax_code"].ToString();
            }

            fileName = mst + "_" + khieu + "_" + shdon;

            return fileName;
        }


        public async Task SendInvoiceByEmail(string inv_InvoiceAuth_id, string type, string siteHddt)
        {
            JObject result = new JObject();

            MemoryStream msZip = null;

            string sql = "";

            try
            {
                using (EmailClient _emailService = new EmailClient())
                {
                    _minvoiceDbContext.SetSiteHddt(siteHddt);
                    _emailService.ClearAttach();

                    Dictionary<string, object> dicParam = new Dictionary<string, object>();

                    if (type == "Gửi hóa đơn")
                    {
                        string filename = await GetInvoiceFileName(inv_InvoiceAuth_id);

                        byte[] dataPdf = inHoadon(inv_InvoiceAuth_id, siteHddt);

                        //get xml hóa đơn
                        string xml = ExportXMLHoadon(inv_InvoiceAuth_id);
                        byte[] dataXml = System.Text.UTF8Encoding.UTF8.GetBytes(xml);

                        byte[] dataZip = ExportZipFile(inv_InvoiceAuth_id, dataPdf, dataXml);
                        msZip = new MemoryStream(dataZip);
                        _emailService.Attach(filename + ".zip", msZip, "application/zip");
                    }

                    var tblTempEmail = GetEmailTemplate(inv_InvoiceAuth_id);

                    string smtpAddress = tblTempEmail["smtp_address"].ToString();
                    int smtpPort = Convert.ToInt32(tblTempEmail["smtp_port"]);
                    bool enableSSL = Convert.ToBoolean(tblTempEmail["enable_ssl"]);

                    string nguoinhan = tblTempEmail["nguoi_nhan"].ToString();
                    string tieude = tblTempEmail["subject"].ToString();
                    string noidung = tblTempEmail["body"].ToString();
                    string nguoi_gui = tblTempEmail["sender"].ToString();
                    string mat_khau = tblTempEmail["pass"].ToString();
                    string bcc = "";
                    string alias = tblTempEmail["alias"].ToString();

                    if (smtpAddress.Length > 0)
                    {
                        _emailService.SetEmailServer(smtpAddress, smtpPort, enableSSL);
                    }

                    string[] parts = nguoinhan.Replace(",", ";").Split(';');

                    foreach (var part in parts)
                    {
                        await _emailService.SendAsync(nguoi_gui, mat_khau, part, tieude, noidung, bcc, alias);

                        dicParam.Clear();
                        dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));

                        string sqlSelect_hoadon = $"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@inv_invoiceauth_id";
                        DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, dicParam);

                        string branch_code = tblInvoiceAuth.Rows[0]["mdvi"].ToString();

                        sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email_68(wb_log_email_68_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                             + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("branch_code", branch_code);
                        parameters.Add("send_date", DateTime.Now);
                        parameters.Add("from", nguoi_gui);
                        parameters.Add("to", part);
                        parameters.Add("subject", tieude);
                        parameters.Add("content", noidung);
                        parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));
                        parameters.Add("user_send", "TVAN");
                        parameters.Add("send_type", type);

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Gửi email", ex);
            }
            finally
            {
                if (msZip != null)
                {
                    msZip.Close();
                }
            }

        }
        public string ExportXMLHoadon(string id)
        {
            string qry = $"SELECT dlxml, dlxml_thue FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id ='{id}'";
            DataTable dt = this._minvoiceDbContext.GetDataTableAsync(qry).Result;
            string xml = dt.Rows[0]["dlxml"].ToString();
            string xml_thue = dt.Rows[0]["dlxml_thue"].ToString();

            if (!string.IsNullOrEmpty(xml_thue))
            {
                //XDocument doc = XDocument.Parse(xml_thue);

                //xml_thue = doc.Descendants("HDon").First().ToString();

                XmlDocument document = new XmlDocument();
                document.LoadXml(xml_thue);
                XmlNode nodeHDon = document.SelectSingleNode("/TDiep/DLieu/HDon");
                xml_thue = nodeHDon.OuterXml;

                return xml_thue;
            }
            else
            {
                return xml;
            }

        }
        public byte[] ExportZipFile(string id, byte[] dataPdf, byte[] dataXml)
        {
            byte[] result = null;

            string fileName = GetInvoiceFileName(id).Result;

            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

            ZipEntry newEntry = new ZipEntry(fileName + ".xml");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            MemoryStream inStream = new MemoryStream(dataXml);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            newEntry = new ZipEntry(fileName + ".pdf");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            inStream = new MemoryStream(dataPdf);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;


            result = outputMemStream.ToArray();

            outputMemStream.Close();

            return result;
        }

        public byte[] inHoadon(string id, string siteHddt, bool inchuyendoi = false)
        {
            byte[] bytes = null;

            string msg_tb = "";

            try
            {
                Guid hdon_id = Guid.Parse(id);

                DataTable tblHoadon = this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'").Result;
                DataTable tblInvoiceXmlData = this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id='" + hdon_id + "'").Result;

                string xml;
                if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString()))
                {
                    xml = tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString();
                    XDocument doc = XDocument.Parse(xml);

                    xml = doc.Descendants("HDon").First().ToString();
                }
                else
                {
                    xml = tblInvoiceXmlData.Rows[0]["dlxml"].ToString();
                }

                string sqlDetails = $"SELECT  * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' order by Cast (stt as numeric)";
                DataTable detail = this._minvoiceDbContext.GetDataTableAsync(sqlDetails).Result;

                string[] notGet = new string[] { "kmai", "stt", "ma", "ten", "mdvtinh", "sluong", "dgia", "tlckhau", "stckhau", "thtien", "tsuat", "tsuatstr", "hdon_id", "cthdon_id" };

                string cctbao_id = tblHoadon.Rows[0]["cctbao_id"].ToString();
                int trang_thai_hd = Convert.ToInt32(tblHoadon.Rows[0]["tthdon"]);
                string sbmat = tblHoadon.Rows[0]["sbmat"].ToString();

                string SqlDongMau = $"Select sdmau from #SCHEMA_NAME#.quanlykyhieu68 where qlkhsdung_id = '{cctbao_id}'";
                DataTable dongMau = this._minvoiceDbContext.GetDataTableAsync(SqlDongMau).Result;
                int soDongMau = Convert.ToInt32(dongMau.Rows[0][0].ToString());

                xml = FunctionUtil.AddXml(xml, detail, notGet, "HHDVu", cctbao_id, soDongMau);

                XtraReport report = new XtraReport();
                DataTable tblKyhieu = _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + cctbao_id + "'").Result;
                DataTable tblDmmauhd = _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + tblKyhieu.Rows[0]["qlmtke_id"] + "'").Result;
                string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

                if (invReport.Length > 0)
                {
                    report = ReportUtil.LoadReportFromString(invReport);
                }
                else
                {
                    throw new Exception("Không tải được mẫu hóa đơn");
                }


                report.Name = "XtraReport1";
                report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                {
                    ds.ReadXmlSchema(xmlReader);
                    xmlReader.Close();
                }

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                {
                    ds.ReadXml(xmlReader);
                    xmlReader.Close();
                }

                if (ds.Tables.Contains("TblXmlData"))
                {
                    ds.Tables.Remove("TblXmlData");
                }


                DataTable tblXmlData = new DataTable();
                tblXmlData.TableName = "TblXmlData";
                tblXmlData.Columns.Add("data");

                DataRow r = tblXmlData.NewRow();
                r["data"] = xml;
                tblXmlData.Rows.Add(r);
                ds.Tables.Add(tblXmlData);

                string datamember = report.DataMember;

                if (datamember.Length > 0)
                {
                    if (ds.Tables.Contains(datamember))
                    {
                        DataTable tblChiTiet = ds.Tables[datamember];
                        int rowcount = ds.Tables[datamember].Rows.Count;
                    }
                }


                if (tblHoadon.Rows[0]["hdon68_id_lk"] != null && !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString()))
                {
                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "3")
                    {
                        Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null && !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString()) ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString()) : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = this._minvoiceDbContext.GetDataTableAsync(sql).Result;

                        if (tblInv.Rows.Count > 0)
                        {
                            //    string msg = tblInv.Rows[0]["msg"].ToString();

                            //    if (msg.Length > 0)
                            //    {
                            //        DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=" + trang_thai_hd);
                            //        string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();

                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                                                          " năm " + nam;

                        }
                        //}
                    }

                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "2")
                    {
                        Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())  : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = this._minvoiceDbContext.GetDataTableAsync(sql).Result;

                        if (tblInv.Rows.Count > 0)
                        {
                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                     " năm " + nam;
                        }
                    }

                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "19" ||
                        tblHoadon.Rows[0]["tthdon_original"].ToString() == "21" ||
                        tblHoadon.Rows[0]["tthdon_original"].ToString() == "23")
                    {
                        Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                            ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                            : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                     + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = this._minvoiceDbContext.GetDataTableAsync(sql).Result;

                        if (tblInv.Rows.Count > 0)
                        {
                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                     " năm " + nam;
                        }
                    }
                }

                else
                {
                    if (trang_thai_hd == 5 || trang_thai_hd == 19 || trang_thai_hd == 21 || trang_thai_hd == 23)
                    {
                        string kyHieuMauSoLQ = tblHoadon.Rows[0]["khmshdclquan"].ToString();
                        string kyHieuHoaDonLQ = tblHoadon.Rows[0]["khhdclquan"].ToString();
                        string soHoaDonLQ = tblHoadon.Rows[0]["shdclquan"].ToString();
                        DateTime ngayLap = DateTime.Parse(tblHoadon.Rows[0]["nlhdclquan"].ToString());
                        string ngay = ngayLap.Day.ToString();
                        string thang = ngayLap.Month.ToString();
                        string nam = ngayLap.Year.ToString();
                        msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " + kyHieuMauSoLQ + " ký hiệu " +
                                 kyHieuHoaDonLQ
                                 + " số " + soHoaDonLQ + " ngày " + ngay + " tháng " + thang + " năm " + nam;
                    }

                    if (trang_thai_hd == 2)
                    {
                        string kyHieuMauSoLQ = tblHoadon.Rows[0]["khmshdclquan"].ToString();
                        string kyHieuHoaDonLQ = tblHoadon.Rows[0]["khhdclquan"].ToString();
                        string soHoaDonLQ = tblHoadon.Rows[0]["shdclquan"].ToString();
                        DateTime ngayLap = DateTime.Parse(tblHoadon.Rows[0]["nlhdclquan"].ToString());
                        string ngay = ngayLap.Day.ToString();
                        string thang = ngayLap.Month.ToString();
                        string nam = ngayLap.Year.ToString();
                        msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " + kyHieuMauSoLQ + " ký hiệu " +
                                 kyHieuHoaDonLQ
                                 + " số " + soHoaDonLQ + " ngày " + ngay + " tháng " + thang + " năm " + nam;
                    }
                }

                if (tblHoadon.Rows[0]["dvtte"] != null && tblHoadon.Rows[0]["dvtte"].ToString() == "USD" && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgia"].ToString()))
                {
                    decimal tienthanhtoan = 0;
                    if (tblHoadon.Rows[0]["tgtttbso_last"] != null && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgtttbso_last"].ToString()))
                    {
                        tienthanhtoan = Convert.ToDecimal(tblHoadon.Rows[0]["tgtttbso_last"].ToString());
                    }
                    decimal tigia = Convert.ToDecimal(tblHoadon.Rows[0]["tgia"].ToString());
                    decimal tienquydoi = tienthanhtoan * tigia;
                    if (report.Parameters["QUYDOI"] != null)
                    {
                        report.Parameters["QUYDOI"].Value = tienquydoi.ToString();
                    }
                }

                if (report.Parameters["MSG_TB"] != null)
                {
                    report.Parameters["MSG_TB"].Value = msg_tb;
                }

                if (report.Parameters["MCCQT"] != null)
                {
                    report.Parameters["MCCQT"].Value = tblHoadon.Rows[0]["mccqthue"].ToString();
                }

                // số bảo mật
                if (report.Parameters["SoBaoMat"] != null)
                {
                    report.Parameters["SoBaoMat"].Value = sbmat;
                }


                //check in bảng kê
                if (report.Parameters["CheckBangKe"] != null)
                {
                    report.Parameters["CheckBangKe"].Value = 0;
                }

                try
                {

                    if (report.Parameters["SoThapPhan"] != null)
                    {
                        string sql = "SELECT * FROM #SCHEMA_NAME#.sl_currency a WHERE a.code='" + tblHoadon.Rows[0]["dvtte"].ToString() + "' limit 1";

                        DataTable currency = this._minvoiceDbContext.GetDataTableAsync(sql).Result;
                        if (currency.Rows.Count > 0)
                        {
                            report.Parameters["SoThapPhan"].Value = currency.Rows[0]["hoadon123"].ToString();
                        }
                    }
                }
                catch
                {

                }

                if (inchuyendoi)
                {
                    var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                    if (tblInChuyenDoi != null)
                    {
                        tblInChuyenDoi.Visible = true;
                    }

                    if (report.Parameters["MSG_HD_TITLE"] != null)
                    {
                        report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                    }

                    if (report.Parameters["NGUOI_IN_CDOI"] != null)
                    {
                        report.Parameters["NGUOI_IN_CDOI"].Value = "";
                        report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                    }

                    if (report.Parameters["NGAY_IN_CDOI"] != null)
                    {
                        report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                        report.Parameters["NGAY_IN_CDOI"].Visible = true;
                    }
                }

                report.DataSource = ds;

                try
                {
                    bool giamthuebanhang20 = tblHoadon.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblHoadon.Rows[0]["giamthuebanhang20"].ToString());

                    CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                    string tongTienGiam = double.Parse(DoubleStrToString(tblHoadon.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                    tongTienGiam = tongTienGiam == "" ? "0" : tongTienGiam;
                    string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";

                    if (giamthuebanhang20)
                    {
                        var dt = ds.Tables["TToan"];
                        if (dt.Rows.Count > 0)
                        {
                            if (dt.Columns.Contains("TgTTTBChu"))
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    var docTien = (dt.Rows[i]["TgTTTBChu"]?.ToString()).Trim();

                                    ds.Tables["TToan"].Rows[i]["TgTTTBChu"] = docTien + ". " + Environment.NewLine + thueGTGTBanHang;
                                    report.DataSource = ds;

                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Log.Error(ex);
                }

                report.CreateDocument();

                if (tblHoadon.Rows[0]["tthdon"].ToString() == "3" || tblHoadon.Rows[0]["tthdon"].ToString() == "17")
                {

                    Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                    int pageCount = report.Pages.Count;

                    for (int i = 0; i < pageCount; i++)
                    {
                        PageWatermark pmk = new PageWatermark();
                        pmk.Image = bmp;
                        report.Pages[i].AssignWatermark(pmk);
                    }
                }

                MemoryStream ms = new MemoryStream();

                report.ExportToPdf(ms);

                bytes = ms.ToArray();
                ms.Close();

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public string DoubleStrToString(string strdec)
        {
            try
            {
                if (strdec.Contains("."))
                {
                    string result = strdec.TrimEnd('0').TrimEnd('.');
                    if (result.Length > 0 && result[result.Length - 1] == '.') return result.Substring(0, result.Length - 1);
                    return strdec.Contains(".") ? strdec.TrimEnd('0').TrimEnd('.') : strdec;
                }
                else
                {
                    return strdec;
                }
            }
            catch
            {
                return strdec;
            }
        }

        public JObject GetEmailTemplate(string id, string type = "Gửi hóa đơn")
        {

            JObject result = new JObject();
            try
            {

                DataTable hoadon68 = _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='{id}'").Result;

                string ma_dvcs = hoadon68.Rows[0]["mdvi"].ToString();
                string nguoi_nhan = hoadon68.Rows[0]["email"].ToString();

                DataTable tblTemp = _minvoiceDbContext.GetDataTableAsync("SELECT a.*,a.body as noi_dung,b.code as mdv,b.email as emailuser FROM #SCHEMA_NAME#.pl_tempemail a join #SCHEMA_NAME#.wb_branch b on a.branch_code = b.code WHERE a.yesno='C' and a.tempemail_type= @P_0 AND a.branch_code=@P_1 LIMIT 1 OFFSET 0", type, ma_dvcs).Result;

                string pass1 = tblTemp.Rows[0]["pass"].ToString();
                string alias = tblTemp.Rows[0]["alias"].ToString();

                JArray array = JArray.FromObject(tblTemp);

                result = (JObject)array[0];
                result["pass"] = pass1;
                result["alias"] = alias;

                DataTable tblEmailBody = _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice_68('#SCHEMA_NAME#',@P_0)", Guid.Parse(id)).Result;
                DataTable tblEmailSubject = _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_subject_email_invoice_68('#SCHEMA_NAME#',@P_0)", Guid.Parse(id)).Result;

                string mdvi = hoadon68.Rows[0]["mdvi"]?.ToString();
                DataTable tblBranch = _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code = '{mdvi}'").Result;

                string noi_dung = result["noi_dung"].ToString();
                string schemaName = _minvoiceDbContext.GetSchemaNameAsync().Result;

                string tieude = result["subject"].ToString();
                DateTime dt = Convert.ToDateTime(hoadon68.Rows[0]["nlap"].ToString());

                //tieude = tieude.Replace("#invoice_number#", hoadon68.Rows[0]["shdon"].ToString());
                //tieude = tieude.Replace("#seller_legal_name#", tblBranch.Rows[0]["name"].ToString());
                //tieude = tieude.Replace("#buyer_name#", hoadon68.Rows[0]["tnmua"].ToString());

                string total_amount = DoubleStrToString(hoadon68.Rows[0]["tgtttbso_last"].ToString());
                CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
                string total_amount_convert = double.Parse(total_amount).ToString("#,###", cul.NumberFormat);
                noi_dung = noi_dung.Replace("#total_amount#", total_amount_convert);

                DataRow row = tblEmailBody.Rows[0];

                foreach (DataColumn column in tblEmailBody.Columns)
                {
                    string value = row[column.ColumnName].ToString();
                    noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                }

                DataRow row_sub = tblEmailSubject.Rows[0];
                foreach (DataColumn column in tblEmailSubject.Columns)
                {
                    string value = row_sub[column.ColumnName].ToString();
                    tieude = tieude.Replace("#" + column.ColumnName + "#", value);
                }

                //result["subject"] = result["subject"].ToString().Replace("#invoice_number#", hoadon68.Rows[0]["shdon"].ToString());
                //noi_dung = noi_dung.Replace("#template_code#", hoadon68.Rows[0]["khieu"].ToString().Substring(0, 1));
                //noi_dung = noi_dung.Replace("#invoice_series#", hoadon68.Rows[0]["khieu"].ToString().Substring(1));
                //noi_dung = noi_dung.Replace("#invoice_issued_date#", dt.ToString("dd-MM-yyyy"));
                //noi_dung = noi_dung.Replace("#invoice_number#", hoadon68.Rows[0]["shdon"].ToString());
                //noi_dung = noi_dung.Replace("#buyer_legal_name#", hoadon68.Rows[0]["tnmua"].ToString());
                //noi_dung = noi_dung.Replace("#security_number#", hoadon68.Rows[0]["sbmat"].ToString());
                //noi_dung = noi_dung.Replace("#total_amount#", total_amount_convert);

                //noi_dung = noi_dung.Replace("#seller_legal_name#", tblBranch.Rows[0]["name"].ToString());
                //noi_dung = noi_dung.Replace("#seller_taxcode#", tblBranch.Rows[0]["tax_code"].ToString());
                //noi_dung = noi_dung.Replace("#seller_tel#", tblBranch.Rows[0]["tel"].ToString());
                //noi_dung = noi_dung.Replace("#schema_name#", schemaName);

                result["subject"] = tieude;
                result["noi_dung"] = noi_dung;
                result["body"] = noi_dung;
                result.Add("hd68", 1);
                result.Add("lst_email", array);
                result.Add("nguoi_nhan", nguoi_nhan);
            }
            catch (Exception e)
            {
                result.Add("error", e.Message);
            }
            return result;
        }
        public async Task<string> GetNguoiNhan(string inv_invoiceauth_id)
        {
            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

            DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT buyer_email,status FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);

            return tblInvoice.Rows[0]["buyer_email"].ToString();

        }
    }
}