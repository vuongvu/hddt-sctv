﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApiService.Redis
{
    public class CacheKeyManager
    {
        public static readonly string TAXCODE_KEY = "TAXCODE:{0}";
        public static readonly string SOURCE_TAXCODE_KEY = "SOURCETAXCODE:{0}";

    }
}
