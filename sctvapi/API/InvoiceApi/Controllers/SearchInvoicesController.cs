﻿using InvoiceApi.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace InvoiceApi.Controllers
{
    [AllowAnonymous]
    public class SearchInvoicesController : BaseApiController
    {
        private readonly ISearchInvoicesService _searchInvoicesService;
        private readonly IInvoiceService _invoiceService;

        public SearchInvoicesController(ISearchInvoicesService searchInvoicesSerivce,
                                    IInvoiceService invoiceService
                                )
        {
            this._searchInvoicesService = searchInvoicesSerivce;
            this._invoiceService = invoiceService;
        }

        [HttpPost]
        [Route("SearchInvoices/FindByCustomer")]
        public JArray FindByCustomer(JObject model)
        {
            JArray result = _searchInvoicesService.FindByCustomer(model);
            return result;
        }

        [HttpGet]
        [Route("SearchInvoices/Preview")]
        public async Task<HttpResponseMessage> Preview(string id)
        {

            HttpResponseMessage result = null;

            try
            {


                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService.PrintInvoiceFromId(id, folder, "PDF","");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "invoice.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                JObject json = new JObject();
                json.Add("error", ex.Message);

                string html = json.ToString();

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(html,Encoding.UTF8,"application/json");
                result.Content.Headers.ContentLength = html.Length;
            }

            return result;
        }
    }
}