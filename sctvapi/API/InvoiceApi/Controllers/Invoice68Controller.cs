﻿using InvoiceApi.Data;
using InvoiceApi.Services68;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DevExpress.Data.Browsing.Design;
using InvoiceApi.Services;

namespace InvoiceApi.Controllers
{
    public class Invoice68Controller : BaseApiController
    {
        private IInvoiceService68 _invoiceService68;
        private IMinvoiceDbContext _minvoiceDbContext;
        private IWebHelper _webHelper;

        public Invoice68Controller(IInvoiceService68 invoiceService68, IMinvoiceDbContext minvoiceDbContext)
        {
            this._invoiceService68 = invoiceService68;
            this._minvoiceDbContext = minvoiceDbContext;
        }

        [HttpGet]
        [Route("Invoice68/GetEmailTemplate")]
        public async Task<JObject> GetEmailTemplate(string type, string id)
        {
            Uri uri = this.ActionContext.Request.RequestUri;
            string originalString = uri.Scheme + "://" + uri.Authority;

            return await _invoiceService68.GetEmailTemplate(type, id, this.Ma_dvcs, originalString);
        }
        [HttpPost]
        [Route("Invoice68/SendInvoiceByEmail")]
        public async Task<JObject> SendInvoiceByEmail(JObject model)
        {

            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/Email.html" : "~/Content/report/Email.html";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

            //JObject result = await this._invoiceService68.SendInvoiceByEmail(folder, model, this.UserName, "PDF");
            JObject result = await this._invoiceService68.SendInvoiceByEmail(folder, model, this.Ma_dvcs, model["type"].ToString());

            return result;
        }

        [HttpPost]
        [Route("Invoice68/AutoSendInvoiceByEmail")]
        public async Task<JObject> AutoSendInvoiceByEmail(JObject model)
        {
            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);
            JObject result = await _invoiceService68.AutoSendInvoiceByEmail(folder, model);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/hoadonSaveChange")]
        public async Task<JObject> SaveHoadon(JObject model)
        {
            JObject result = await this._invoiceService68.SaveHoadon(model, this.UserName);

            return result;
        }

        // API phục vụ tích hợp với PMKT
        [HttpPost]
        [Route("Invoice68/SaveHoadon78")]
        public async Task<JObject> SaveHoadon78(JObject model)
        {
            JObject result = await this._invoiceService68.SaveHoadon78(model, this.UserName);

            return result;
        }

        // API phục vụ tích hợp với PMKT
        [HttpPost]
        [Route("Invoice68/SaveHoadon78SCTV")]
        public async Task<JObject> SaveHoadon78SCTV(JObject model)
        {
            JObject result = await this._invoiceService68.SaveHoadon78(model, this.UserName);

            result = FunctionUtil.AddInfoToResult(result);
            return result;
        }

        [HttpPost]
        [Route("Invoice68/SaveListHoadon78")]
        public async Task<JArray> SaveListHoadon78(JObject modelList)
        {
            JArray arr_result = new JArray();
            String editMode = modelList["editmode"]?.ToString();
            JArray arr_model = (JArray)modelList["data"];

            foreach (JObject item in arr_model)
            {
                JObject model = new JObject();
                model.Add("editmode", editMode);

                JArray data_model = new JArray();
                data_model.Add(item);

                model.Add("data", data_model);

                JObject result = await this._invoiceService68.SaveHoadon78(model, this.UserName);
                arr_result.Add(result);
            }

            return arr_result;
        }

        [HttpPost]
        [Route("Invoice68/SaveListHoadon78SCTV")]
        public async Task<JObject> SaveListHoadon78SCTV(JObject modelList)
        {
            JObject resultObj;
            JArray arr_result = new JArray();
            String editMode = modelList["editmode"]?.ToString();
            JArray arr_model = (JArray)modelList["data"];

            foreach (JObject item in arr_model)
            {
                JObject model = new JObject();
                model.Add("editmode", editMode);

                JArray data_model = new JArray();
                data_model.Add(item);

                model.Add("data", data_model);

                JObject result = await this._invoiceService68.SaveHoadon78(model, this.UserName);
                arr_result.Add(result);
            }

            resultObj = FunctionUtil.AddInfoToResult(arr_result);
            return resultObj;
        }

        // quyennh, API giả lập gửi CQT
        [HttpPost]
        [Route("Invoice68/createHoaDonSendTCT")]
        public async Task<JObject> CreateHoaDonSendTCT(JObject model)
        {
            JObject result = await this._invoiceService68.CreateHoaDonSendTCT(model, this.UserName);

            return result;
        }
        // hatm, API kí gửi CQT

        [AllowAnonymous]
        [HttpPost]
        [Route("Invoice68/KiHoaDonSendTCT")]
        public async Task<JObject> CreateHoaDonKiTCT(JObject model)
        {

            JObject result = await this._invoiceService68.KiHoaDonGuiTCT(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/dangkysudungSave")]
        public async Task<JObject> SaveDangkysudung(JObject model)
        {
            JObject result = await this._invoiceService68.SaveDangkysudung(model);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/XmlMau01")]
        public async Task<JObject> XmlMau01(string id)
        {
            JObject jarr = await this._invoiceService68.XmlMau01(id);
            return jarr;
        }

        [HttpPost]
        [Route("Invoice68/SaveXmlMau01")]
        public async Task<JObject> SaveXmlMau01(JObject model)
        {
            JObject result = await this._invoiceService68.SaveXmlMau01(model);

            return result;
        }


        [HttpGet]
        [Route("Invoice68/getDangkysudung")]
        public async Task<JObject> getDangkysudung()
        {
            JObject jarr = await this._invoiceService68.getDangkysudung();
            return jarr;
        }

        [HttpPost]
        [Route("Invoice68/SignDangkysudung")]
        public async Task<JObject> SignDangkysudung(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SignDangkysudung(model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/huyhoadonSave")]
        public async Task<JObject> SaveHuyhoadon(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyhoadon(model);
            return res;
        }


        [HttpPost]
        [Route("Invoice68/huyhoadonSaveSCTV")]
        public async Task<JObject> SaveHuyhoadonSCTV(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyhoadon(model);
            res = FunctionUtil.AddInfoToResult(res);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/huyhoadonSaveSign")]
        public async Task<JObject> huyhoadonSaveSign(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyhoadon(model);
            JArray data = (JArray)model["data"];

            string hdon_id = "";
            if (res.ContainsKey("hdon_id") && (res["hdon_id"] != null || !string.IsNullOrEmpty(res["hdon_id"].ToString())))
            {
                hdon_id = res["hdon_id"].ToString();
            }

            if (res["error"] == null)
            {
                res = await this._invoiceService68.SignHuyhoadon((JObject)model["sign"], res["id"].ToString(), hdon_id, "1");
            }

            return res;
        }


        [HttpPost]
        [Route("Invoice68/huyhoadonSaveSignSCTV")]
        public async Task<JObject> huyhoadonSaveSignSCTV(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyhoadon(model);
            JArray data = (JArray)model["data"];

            if (res["error"] == null)
            {
                res = await this._invoiceService68.SignHuyhoadon((JObject)model["sign"], res["id"].ToString(), res["hdon_id"].ToString(), "1");
            }
            res = FunctionUtil.AddInfoToResult(res);

            return res;
        }

        [HttpPost]
        [Route("Invoice68/huyhoadonSaveSignPretreatment")]
        public async Task<JObject> huyhoadonSaveSignPretreatment(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyhoadonPretreatment(model);
            JArray data = (JArray)model["data"];

            if (res["error"] == null)
            {
                res = await this._invoiceService68.SignHuyhoadon((JObject)model["sign"], res["id"].ToString(), res["inv_invoiceauth_id"].ToString(), "1");
            }

            return res;
        }

        [HttpPost]
        [Route("Invoice68/giaitrinhSaveSign")]
        public async Task<JObject> giaitrinhSaveSign(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyhoadon(model);
            if (res["error"] == null)
            {
                res = await this._invoiceService68.SignHuyhoadon((JObject)model["sign"], res["id"].ToString(), res["hdon_id"].ToString(), "4");
            }
            return res;
        }

        [HttpPost]
        [Route("Invoice68/huyhoadonSign")]
        public async Task<JObject> SignHuyhoadon(JObject model)
        {
            JObject res = new JObject();

            // get hdon_id từ bảng mau04_chitiet

            Dictionary<string, object> parameters2 = new Dictionary<string, object>();
            parameters2.Add("mau04_id", Guid.Parse(model["id"].ToString()));
            string sqlSelect_mau04_chitiet = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                    + " WHERE mau04_id = @mau04_id";
            DataTable mau04_chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04_chitiet, CommandType.Text, parameters2);
            string hdon_id = "";
            if (mau04_chitiet_Table.Rows.Count > 0)
            {
                if (mau04_chitiet_Table.Rows[0]["hdon_id"] != null || !string.IsNullOrEmpty(mau04_chitiet_Table.Rows[0]["hdon_id"].ToString()))
                {
                    hdon_id = mau04_chitiet_Table.Rows[0]["hdon_id"].ToString();
                }
            }
            res = await this._invoiceService68.SignHuyhoadon(model, model["id"].ToString(), hdon_id, "1");
            return res;
        }

        [HttpPost]
        [Route("Invoice68/huyHoadonTaomoi")]
        public async Task<JObject> SaveHuyHoadonTaomoi(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyHoadonTaomoi(model, this.UserName);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/huyHoadonTaomoiUSB")]
        public async Task<JObject> SaveHuyHoadonTaomoiUSB(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyHoadonTaomoiUSB(model, this.UserName);
            return res;
        }


        [HttpGet]
        [Route("Invoice68/ExportXMLHoadon")]
        public async Task<HttpResponseMessage> ExportXMLHoadon(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                //get file name invoice theo Id hóa đơn
                string filename = await _invoiceService68.GetInvoiceFileName(id);

                string xml = await _invoiceService68.ExportXMLHoadon(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = filename + ".xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLThongDiepHoadon")]
        public async Task<HttpResponseMessage> ExportXMLThongDiepHoadon(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                //get file name invoice theo Id hóa đơn
                string filename = await _invoiceService68.GetInvoiceFileName(id);

                string xml = await _invoiceService68.ExportXMLThongDiepHoadon(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "ThongDiep_" + filename + ".xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLMau01")]
        public async Task<HttpResponseMessage> ExportXMLMau01(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLMau01(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau01.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLBangTHDL")]
        public async Task<HttpResponseMessage> ExportXMLBangTHDL(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLBangTHDL(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "BangTHDL.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLMau02")]
        public async Task<HttpResponseMessage> ExportXMLMau02(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLMau02(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau02.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLMau03")]
        public async Task<HttpResponseMessage> ExportXMLMau03(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLMau03(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau03.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLMau04")]
        public async Task<HttpResponseMessage> ExportXMLMau04(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLMau04(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau04.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLMau04Rep")]
        public async Task<HttpResponseMessage> ExportXMLMau04Rep(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLMau04Rep(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau04.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLThongDiepCQT")]
        public async Task<HttpResponseMessage> ExportXMLThongDiepCQT(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLMaThongDiepCQT(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "XMLThongDiep.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMaThongDiepCQT")]
        public async Task<HttpResponseMessage> inXmlThongDiepCQT(string id, string mltdiep)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLMaThongDiepCQT(id);
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = "";
                switch (mltdiep)
                {
                    // sai sót
                    case "301":
                        path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_04.repx" : "~/Content/report/Mau_04.repx";
                        break;
                    // rà soát
                    case "302":
                        path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_05.repx" : "~/Content/report/Mau_05.repx";
                        break;
                    // tờ khai
                    case "102":
                        path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_03.repx" : "~/Content/report/Mau_03.repx";
                        break;
                    case "103":
                        path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_02.repx" : "~/Content/report/Mau_02.repx";
                        break;
                    case "104":
                        path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_02.repx" : "~/Content/report/Mau_02.repx";
                        break;
                    default:
                        break;
                }
                //string path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDL_68.repx" : "~/Content/report/BTHDL_68.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inMaThongDiepCQT(xml, folder, "PDF");

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "ThongDiepCQT.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inBTHDLieu")]
        public async Task<HttpResponseMessage> inBTHDLieu(string id)
        {
            HttpResponseMessage result = null;

            try
            {

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDL_68.repx" : "~/Content/report/BTHDL_68.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inBTHDLieu(id, folder, "PDF");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau04.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inHoadon")]
        public async Task<HttpResponseMessage> inHoadon(string id, string type, bool inchuyendoi, bool checkBangKe = false)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inHoadon(id, folder, type, inchuyendoi, checkBangKe);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/Preview")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Preview(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inHoadon(id, folder, "PDF", false);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMau01")]
        public async Task<HttpResponseMessage> inMau01(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_01.repx" : "~/Content/report/Mau_01.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService68.inMau01(id, folder, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMau02")]
        public async Task<HttpResponseMessage> inMau02(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_02.repx" : "~/Content/report/Mau_02.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService68.inMau02(id, folder, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMau03")]
        public async Task<HttpResponseMessage> inMau03(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_03.repx" : "~/Content/report/Mau_03.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService68.inMau03(id, folder, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMau04")]
        public async Task<HttpResponseMessage> inMau04(string id, string type, string loai)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = string.Empty;
                if (loai == "5")
                {
                    path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_05.repx" : "~/Content/report/Mau_05.repx";
                }
                else
                {
                    path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_04.repx" : "~/Content/report/Mau_04.repx";
                }
                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService68.inMau04(id, folder, type, loai);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMau04Rep")]
        public async Task<HttpResponseMessage> inMau04Rep(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = string.Empty;
                string xml = "";
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id='" + id + "'");
                if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_nhan"].ToString()))
                {
                    xml = tblHoadon.Rows[0]["xml_nhan"].ToString();

                    if (xml.Contains("<MLTDiep>204</MLTDiep>"))
                    {
                        path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDL_KQ_68.repx" : "~/Content/report/BTHDL_KQ_68.repx";
                    }
                    else
                    {
                        path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_06.repx" : "~/Content/report/Mau_06.repx";
                    }
                }

                //path = originalString.StartsWith("/api") ? "~/api/Content/report/Mau_06.repx" : "~/Content/report/Mau_06.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService68.inMau04Rep(id, folder, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice68/SignBTHDL")]
        public async Task<JObject> SignBTHDL(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SignBTHDL(model);
            return res;
        }


        [HttpPost]
        [Route("Invoice68/SaveXmlBTHDL")]
        public async Task<JObject> SaveXmlBTHDL(JArray model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveXmlBTHDL(model);
            return res;
        }

        [HttpGet]
        [Route("Invoice68/XmlBTHDL")]
        public async Task<JObject> XmlBTHDL(string id)
        {
            JObject ar = await this._invoiceService68.XmlBTHDL(id);
            return ar;
        }

        [HttpPost]
        [Route("Invoice68/SignInvoiceCertFile68")]
        public async Task<JObject> SignInvoiceCertFile68(JObject model)
        {
            JObject res = new JObject();

            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

            res = await this._invoiceService68.SignInvoiceCertFile68(folder, model);
            return res;
        }


        [HttpPost]
        [Route("Invoice68/SignInvoiceCertFile68SCTV")]
        public async Task<JObject> SignInvoiceCertFile68SCTV(JObject model)
        {
            JObject res = new JObject();

            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

            res = await this._invoiceService68.SignInvoiceCertFile68SCTV(folder, model);

            res = FunctionUtil.AddInfoToResult(res);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/ReSignInvoiceCertFile68")]
        public async Task<JObject> ReSignInvoiceCertFile68(JObject model)
        {
            JObject res = new JObject();

            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

            res = await this._invoiceService68.ReSignInvoiceCertFile68(folder, model, true);
            return res;
        }


        [HttpPost]
        [Route("Invoice68/SignInvoiceCertFile68ForHasCodeAndNotHaveAnyCode")]
        public async Task<Object> SignInvoiceCertFile68ForHasCodeAndNotHaveAnyCode(JObject model)
        {
            JObject res = new JObject();

            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

            res = await this._invoiceService68.SignInvoiceCertFile68ForHasCodeAndNotHaveAnyCode(folder, model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/DuyetHoaDon")]
        public async Task<Object> DuyetHoaDon(JObject model)
        {

            var res = await this._invoiceService68.DuyetHoaDon(model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/DuyetHoaDon78")]
        public async Task<Object> DuyetHoaDon78(JObject model)
        {
            model["status"] = "Chờ duyệt";
            var res = await this._invoiceService68.DuyetHoaDon(model);
            return res;
        }


        [HttpPost]
        [Route("Invoice68/DuyetHoaDon78SCTV")]
        public async Task<Object> DuyetHoaDon78SCTV(JObject model)
        {
            model["status"] = "Chờ duyệt";
            var res = await this._invoiceService68.DuyetHoaDon(model);

            res = FunctionUtil.AddInfoToResult(res);
            return res;
        }


        [HttpPost]
        [Route("Invoice68/SinhSoHoaDon78")]
        public async Task<Object> SinhSoHoaDon78(JObject model)
        {
            model["status"] = "Chờ sinh số";
            var res = await this._invoiceService68.DuyetHoaDon(model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/SinhSoHoaDon78SCTV")]
        public async Task<Object> SinhSoHoaDon78SCTV(JObject model)
        {
            model["status"] = "Chờ sinh số";
            var res = await this._invoiceService68.DuyetHoaDon(model);

            res = FunctionUtil.AddInfoToResult(res);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/SendInvoiceToCQT68")]
        public async Task<JObject> SendInvoiceToCQT68(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SendInvoiceToCQT68(model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/SendInvoiceToCQT68SCTV")]
        public async Task<JObject> SendInvoiceToCQT68SCTV(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SendInvoiceToCQT68(model);

            res = FunctionUtil.AddInfoToResult(res);
            return res;
        }
        [HttpPost]
        [Route("Invoice68/SignXmlBangKeBanRa")]
        public async Task<HttpResponseMessage> SignXmlBangKeBanRa(JObject obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                string type = (string)obj["type"];

                byte[] bytes = await _invoiceService68.SignXmlBangKeBanRa(obj);

                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.Length;

                if (type == "json")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                }
                else if (type == "PDF")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    response.Content.Headers.ContentDisposition.FileName = "report.pdf";
                }
                else if (type == "xlsx")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    response.Content.Headers.ContentDisposition.FileName = "report.xlsx";
                }
                else if (type == "Rtf")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    response.Content.Headers.ContentDisposition.FileName = "report.rtf";
                }
                else if (type == "xml")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = "report.xml";
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                response.Content.Headers.ContentLength = ex.Message.Length;
            }

            return response;
        }


        [HttpPost]
        [Route("Invoice68/SaveXml")]
        public async Task<JObject> SaveXml(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveXml(model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/SaveXmlPretreatment")]
        public async Task<JObject> SaveXmlPretreatment(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveXmlPretreatment(model);
            return res;
        }

        [HttpGet]
        [Route("Invoice68/ExportInvoiceXml")]
        public async Task<JObject> ExportInvoiceXml(Guid id)
        {
            JObject ar = await this._invoiceService68.ExportInvoiceXml(id);
            return ar;
        }

        [HttpGet]
        [Route("Invoice68/ExportInvoiceXmlPretreatment")]
        public async Task<JObject> ExportInvoiceXmlPretreatment(Guid id)
        {
            JObject ar = await this._invoiceService68.ExportInvoiceXmlPretreatment(id);
            return ar;
        }

        [HttpGet]
        [Route("Invoice68/GetListCertificates")]
        public async Task<JArray> GetListCertificates()
        {
            JArray res = new JArray();
            res = await this._invoiceService68.GetListCertificates(this.UserName, this.Ma_dvcs);
            return res;
        }


        [HttpGet]
        [Route("Invoice68/XmlMau0405")]
        public async Task<JObject> XmlMau0405(string id)
        {
            JObject ar = await this._invoiceService68.XmlMau0405(id);
            return ar;
        }


        [HttpPost]
        [Route("Invoice68/SaveXmlMau0405")]
        public async Task<JObject> SaveXmlMau0405(JObject model)
        {
            JObject ar = await this._invoiceService68.SaveXmlMau0405(model);
            return ar;
        }

        [HttpGet]
        [Route("Invoice68/Amount_ToWord")]
        public async Task<JObject> Amount_ToWord(string number, string type_amount)
        {
            JObject jarr = await this._invoiceService68.Amount_ToWord(number, type_amount);
            return jarr;
        }

        [HttpGet]
        [Route("Invoice68/getKyhieu")]
        public async Task<JArray> getKyhieu(string cctbao_id)
        {
            JArray result = await this._invoiceService68.getKyhieu(cctbao_id);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetSignType")]
        public async Task<JArray> GetSignType()
        {
            JArray result = await this._invoiceService68.GetSignType();

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetListCertificatesFile68")]
        public async Task<JArray> GetListCertificatesFile68()
        {
            JArray result = await this._invoiceService68.GetListCertificatesFile68(this.UserName, this.Ma_dvcs);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetHoadonChoXuLy")]
        public async Task<JObject> GetHoadonChoXuLy(JObject model)
        {
            JObject result = await this._invoiceService68.GetHoadonChoXuLy(model);
            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetDetailHoadonChoXuLy")]
        public async Task<JArray> GetDetailHoadonChoXuLy(JObject model)
        {
            string data = model["data"]?.ToString();
            JArray result = await this._invoiceService68.GetDetailHoadonChoXuLy(data);
            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetDataDkysdung01")]
        public async Task<JObject> GetDataDkysdung01(int start, int count, string filter)
        {
            string select = "SELECT mau01_id as id,CASE WHEN loai ='1' THEN 'Đăng ký thông tin sử dụng hóa đơn điện tử' ELSE 'Thay đổi thông tin sử dụng hóa đơn điện tử' END AS noi_dung, * FROM #SCHEMA_NAME#.mau01_68 \n"
                + " Where mdvi = '" + this.Ma_dvcs + "'";
            //string select = "SELECT a.mau01_id as id,CASE WHEN loai ='1' THEN 'Đăng ký thông tin sử dụng hóa đơn điện tử' ELSE 'Thay đổi thông tin sử dụng hóa đơn điện tử' END AS noi_dung, * FROM #SCHEMA_NAME#.mau01_68 a" +
            //                 " LEFT JOIN #SCHEMA_NAME#.tonghop_gui_tvan_68 c ON a.mau01_id = c.type_id";

            string ptrang = "   ORDER BY nlap DESC OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, null);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetBangTHDL")]
        public async Task<JObject> GetBangTHDL(int start, int count, string filter)
        {
            string select = $@"SELECT *
                            FROM(SELECT bangtonghopdl_68_id as id,
                                         lkdlieu || (case when lkdlieu = 'N' then ngay || '.' else '' end) || thang || '.' || nam as kdlieu,
                                         bangtonghopdl_68_id, lkdlieu, thang, nam, nglap, ngky, trang_thai, nlap, ldau, sbthdlieu, tgtcthue, tgtthue, tgtttoan, 
                                         bslthu, ngay_ky, tu_ngay, den_ngay, ngay, hddin, lhhoa, tnnt, mst, sdlthu, ket_qua, mtdiepcqt, 
                                         mtdiep_gui, ngay_tbao, ly_do, mdvi, error_status, (Case when ket_qua is null then 3 else ket_qua::numeric end) as ket_qua_order
                                  FROM #SCHEMA_NAME#.bangtonghopdl_68
                                  WHERE mdvi = '" + this.Ma_dvcs + "' ) a ";
            string ptrang = "ORDER BY trang_thai,ket_qua_order DESC, nlap DESC,sbthdlieu DESC OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, null);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/getDatamau04")]
        public async Task<JObject> getDatamau04(int start, int count, string filter)
        {
            //string select = "select * FROM #SCHEMA_NAME#.mau04_68 m  join #SCHEMA_NAME#.mau04_68_chitiet mc on  m.mau04_id = mc.mau04_id ";
            string select = @"SELECT * FROM (SELECT a.mau04_id as id, trang_thai::numeric as ord, *
                                FROM #SCHEMA_NAME#.mau04_68 a
                                    LEFT JOIN (select mau04_id,
                                    string_agg(khieu, ', ') khieu,
                                    string_agg(shdon, ', ') shdon,
                                    (case
                                    when string_agg(hdon_id::text, '') is null or string_agg(hdon_id::text, '') = '' then 0
                                    else 1 end) as     is_hdon78
                                    from #SCHEMA_NAME#.mau04_68_chitiet
                                    group by mau04_id) b
                                on a.mau04_id = b.mau04_id where mdvi = '" + this.Ma_dvcs + "') a ";
            //+ " LEFT JOIN #SCHEMA_NAME#.tonghop_gui_tvan_68 c ON b.mau04_id = c.type_id";
            string ptrang = "ORDER BY ord DESC, ntbao DESC OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, null);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetThongDiepThueTraVe")]
        public async Task<JObject> getThongDiepThueTraVe(int start, int count, string filter)
        {
            string select = "SELECT * FROM #SCHEMA_NAME#.quanly_mathongdiep_cqt WHERE mdvi = '" + this.Ma_dvcs + "' \n";
            string ptrang = "ORDER BY tgian_gui DESC OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, null);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetDataQlymau")]
        public async Task<JObject> GetDataQlymau(int start, int count, string filter, string tlbParam)
        {
            //= "SELECT qlmtke_id as id, * FROM #SCHEMA_NAME#.quanlymau68 ";

            string select = "select * from (select id, qlmtke_id, mmau, tmau, ngtao, ntao, checkmaubangke, code, SUM(tt_sd) tt_sd from ( \n"
              + "  SELECT a.qlmtke_id as id, a.qlmtke_id, a.mmau, a.tmau, a.ngtao, a.ntao, case when a.dulieumaubangke is not null and a.dulieumaubangke != '' then 1 else 0 end as checkmaubangke ,a.code ,c.tt_sd as tt_sd \n"
              + "FROM #SCHEMA_NAME#.quanlymau68 a LEFT JOIN #SCHEMA_NAME#.quanlykyhieu68 b ON"
            + " a.qlmtke_id = b.qlmtke_id LEFT JOIN"
            + " (select count(*) as tt_sd, a.cctbao_id from #SCHEMA_NAME#.hoadon68 a inner join"
            + " #SCHEMA_NAME#.quanlykyhieu68 b ON a.cctbao_id = b.qlkhsdung_id group by a.cctbao_id) c"
            + " ON c.cctbao_id = b.qlkhsdung_id \n"
            + ") a group by id, qlmtke_id, mmau, tmau, ngtao, ntao, checkmaubangke, code ) b ";

            string ptrang = "ORDER BY ntao desc OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, tlbParam);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetDataInvoice")]
        public async Task<JObject> GetDataInvoice(int start, int count, string filter, string tlbparam)
        {
            //string select = "SELECT a.*, b.*, a.hdon_id as id, tnmua || '/' || ten as ten, c.status as statustn, c.message as message FROM #SCHEMA_NAME#.hoadon68 a " +
            //    " INNER JOIN #SCHEMA_NAME#.trangthaihoadon68 b ON a.tthdon = b.mtthdon" +
            //    " LEFT JOIN #SCHEMA_NAME#.tonghop_gui_tvan_68 c ON a.hdon_id = c.type_id";

            string select = "select * from (SELECT a.*, b.*, a.hdon_id as id, (a.shdon ::numeric) as ord, bk.shdon as checksobangke, " +
                            "(case when a.error_status = '1' then 'Có lỗi' else 'Không lỗi' end) as error_status1 FROM #SCHEMA_NAME#.hoadon68 a " + 
                            " INNER JOIN #SCHEMA_NAME#.trangthaihoadon68 b ON a.tthdon = b.mtthdon" + 
                            " LEFT JOIN #SCHEMA_NAME#.bangke_kemtheo bk ON a.hdon_id = bk.hdon_id) a ";

            string ptrang = "ORDER BY a.nlap DESC, ord DESC OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";

            var obj = await _invoiceService68.GetDataNewWith_Bangke(select, ptrang, start, count, filter, tlbparam);

            return obj;
        }


        [HttpGet]
        [Route("Invoice68/GetDataInvoiceThongTin")]
        public async Task<JObject> GetDataInvoiceThongTin(int start, int count, string filter, string tlbparam)
        {
            JObject obj = new JObject();
            try
            {

                string select = @"select sum(case when tthai = 'Chờ cấp mã' and (error_status is null or error_status = '0') then 1 else 0 end)                               as chocapma,
                                   sum(case when (tthai = 'Chờ cấp mã' or tthai = 'CQT không cấp mã') and error_status = '1' then 1 else 0 end) as chocapmaerror,
                                   sum(case when tthai = 'Đã cấp mã' then 1 else 0 end)                                                         as duoccapma,
                                   sum(case when (tthai = 'Chờ ký' or tthai = 'Chờ duyệt' or tthai = 'Chờ sinh số') then 1 else 0 end)          as chuagui,
                                   sum(case
                                           when (((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                  tthdon = 0)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                   tthdon = 2)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                   tthdon = 11)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                   tthdon = 1)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận') and (tthdon = 19 or tthdon = 21 or tthdon = 23))) then tgtttbso_last
                                           else 0 end)                                                                                          as doanhthu,
                                   sum(case when tthai = 'CQT đã nhận' and (error_status is null or error_status = '0') then 1 else 0 end)                                   as khongma,
                                   sum(case when tthai = 'CQT không tiếp nhận HĐ' and error_status = '1' then 1 else 0 end)                                     as khongmaerror,
                                   sum(case
                                           when (((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                  tthdon = 0)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                   tthdon = 2)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                   tthdon = 11)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận' or tthai = 'Chấp nhận TBSS' or tthai = 'Không chấp nhận TBSS' or tthai = 'TBSS sai định dạng' or tthai = 'Chờ phản hồi') and
                                                   tthdon = 1)
                                               or ((tthai = 'Đã cấp mã' or tthai = 'CQT đã nhận') and (tthdon = 19 or tthdon = 21 or tthdon = 23))) then tgtttbso_last
                                           else 0 end)                                                                                          as doanhthunocode
                            from (SELECT a.*, b.*, a.hdon_id as id, (a.shdon ::numeric) as ord, bk.shdon as checksobangke, 
                                (case when a.error_status = '1' then 'Có lỗi' else 'Không lỗi' end) as error_status1 FROM #SCHEMA_NAME#.hoadon68 a
                                            INNER JOIN #SCHEMA_NAME#.trangthaihoadon68 b ON a.tthdon = b.mtthdon
                                            LEFT JOIN #SCHEMA_NAME#.bangke_kemtheo bk ON a.hdon_id = bk.hdon_id) a";

                var sql = await _invoiceService68.GetSqlData(select, filter, tlbparam);

                DataTable doanhthu = await _minvoiceDbContext.GetDataTableAsync(sql);

                obj.Add("chocapma", doanhthu.Rows[0]["chocapma"].ToString());
                obj.Add("chocapmaerror", doanhthu.Rows[0]["chocapmaerror"].ToString());
                obj.Add("duoccapma", doanhthu.Rows[0]["duoccapma"].ToString());
                obj.Add("chuagui", doanhthu.Rows[0]["chuagui"].ToString());
                obj.Add("doanhthu", doanhthu.Rows[0]["doanhthu"].ToString());
                obj.Add("khongma", doanhthu.Rows[0]["khongma"].ToString());
                obj.Add("khongmaerror", doanhthu.Rows[0]["khongmaerror"].ToString());
                obj.Add("doanhthunocode", doanhthu.Rows[0]["doanhthunocode"].ToString());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetDataInvoiceDanhSachChoKy")]
        public async Task<JObject> GetDataInvoiceDanhSachChoKy(int start, int count, string filter, string tlbparam)
        {
            string selectKiHieu = $@"SELECT string_agg(cast(a.qlkhsdung_id as text),''',''')  as id FROM #SCHEMA_NAME#.quanlykyhieu68 a
                                    INNER JOIN #SCHEMA_NAME#.permission_kyhieu68 b ON a.qlkhsdung_id = b.qlkhsdung_id
                                    WHERE b.wb_user_id = (SELECT wb_user_id FROM #SCHEMA_NAME#.wb_user WHERE username='{UserName}')";
            string strKiHieu = "";
            DataTable kihieu = _minvoiceDbContext.GetDataTable(selectKiHieu);
            if (kihieu.Rows.Count > 0 && kihieu.Rows[0][0] != null && !string.IsNullOrEmpty(kihieu.Rows[0][0].ToString()))
            {
                strKiHieu = "{\"columnName\" : \"cctbao_id\", \"columnType\": \"nvarchar\", \"value\": #value }";
                strKiHieu = strKiHieu.Replace("#value", "\"('" + kihieu.Rows[0][0].ToString() + "')\"");

                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.TrimEnd(']');
                    filter += "," + strKiHieu + "]";
                }
            }


            string select = "select * from (SELECT a.*, b.*, a.hdon_id as id, tnmua || '/' || ten as ten, (a.shdon ::numeric) as ord FROM #SCHEMA_NAME#.hoadon68 a " +
                            $" INNER JOIN #SCHEMA_NAME#.trangthaihoadon68 b ON a.tthdon = b.mtthdon where a.mdvi = '{this.Ma_dvcs}') a";

            string ptrang = "ORDER BY a.nlap ASC OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";

            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, tlbparam);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetDataInvoiceDetail")]
        public async Task<JArray> GetDataInvoiceDetail(string id)
        {
            JArray result = await _invoiceService68.GetDataInvoiceDetail(id);

            return result;
        }
        [HttpGet]
        [Route("Invoice68/GetDataInvoicePhi")]
        public async Task<JArray> GetDataInvoicePhi(string id)
        {
            JArray result = await _invoiceService68.GetDataInvoicePhi(id);

            return result;
        }
        [HttpGet]
        [Route("Invoice68/GetDataMau0405Detail")]
        public async Task<JArray> GetDataMau0405Detail(string id)
        {
            JArray result = await _invoiceService68.GetDataMau0405Detail(id);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetDataDangkysudungDetail")]
        public async Task<JArray> GetDataDangkysudungDetail(string id)
        {
            JArray result = await _invoiceService68.GetDataDangkysudungDetail(id);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetDataDangkysudungUyNhiem")]
        public async Task<JArray> GetDataDangkysudungUyNhiem(string id)
        {
            JArray result = await _invoiceService68.GetDataDangkysudungUyNhiem(id);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetDataBangTHDLDetail")]
        public async Task<JArray> GetDataBangTHDLDetail(string id)
        {
            JArray result = await _invoiceService68.GetDataBangTHDLDetail(id);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetResponseMessage")]
        public async Task<JArray> GetResponseMessage(string id)
        {
            JArray result = await _invoiceService68.GetResponseMessage(id);

            return result;
        }


        [HttpGet]
        [Route("Invoice68/GetResponseMessageSCTV")]
        public async Task<JObject> GetResponseMessageSCTV(string id)
        {
            JArray result = await _invoiceService68.GetResponseMessage(id);
            JObject res = FunctionUtil.AddInfoToResult(result);
            return res;
        }

        [HttpGet]
        [Route("Invoice68/GetHistoryInvoice")]
        public async Task<JArray> GetHistoryInvoice(string id)
        {
            JArray result = await _invoiceService68.GetHistoryInvoice(id);

            return result;
        }


        [HttpGet]
        [Route("Invoice68/GetHistoryInvoiceSCTV")]
        public async Task<JObject> GetHistoryInvoiceSCTV(string id)
        {
            JArray result = await _invoiceService68.GetHistoryInvoice(id);
            JObject res = FunctionUtil.AddInfoToResult(result);
            return res;
        }

        [HttpGet]
        [Route("Invoice68/GetInvoiceNotify")]
        public async Task<JArray> GetInvoiceNotify()
        {
            try
            {
                JArray json = new JArray();

                string sql = "SELECT * FROM #SCHEMA_NAME#.rp_list_notify_78('#SCHEMA_NAME#', '" + this.Ma_dvcs + "')";
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text);
                json = JArray.FromObject(dt);

                JArray jsonResult = new JArray();

                if (json.Count > 0)
                {
                    foreach (var item in json)
                    {
                        var dategui = (DateTime)item["tgian_gui"];
                        item["tgian_gui"] = getFormatDate(dategui);
                        jsonResult.Add(item);
                    }
                }
                else
                {
                    dynamic objectEmpty = new JObject();
                    objectEmpty.tgian_gui = null;
                    objectEmpty.contentnotify = "Không có dữ liệu để hiển thị.";
                    jsonResult.Add(objectEmpty);
                }

                return jsonResult;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getFormatDate(DateTime Dateinput)
        {
            System.DateTime createDate = Dateinput;
            System.DateTime now = DateTime.Now;
            System.TimeSpan diff = now.Subtract(createDate);
            string date = "";

            double totalHours = diff.TotalHours;
            if (1 < totalHours && totalHours < 24)
            {
                date = Math.Floor(totalHours) + " giờ trước";
            }
            if (totalHours < 1)
            {
                if (diff.TotalMinutes < 1)
                {
                    date = "vài giây trước";
                }
                else
                {
                    date = Math.Floor(diff.TotalMinutes) + " phút trước";
                }
            }
            if (totalHours > 24 && (Dateinput.Day + 1) == now.Day)
            {
                date = "Hôm qua lúc " + Dateinput.ToString("HH:ss");
            }
            if (totalHours > 24 && (Dateinput.Day + 1) != now.Day)
            {
                date = Dateinput.Day + " Tháng " + Dateinput.Month + " lúc " + Dateinput.ToString("HH:ss");
            }
            return date;
        }

        [HttpPost]
        [Route("Invoice68/deleteMau04")]
        public async Task<JObject> deleteMau04(JObject model)
        {
            JObject result = await this._invoiceService68.deleteMau04(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/deleteMau01")]
        public async Task<JObject> deleteMau01(JObject model)
        {
            JObject result = await this._invoiceService68.deleteMau01(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/deleteMau68")]
        public async Task<JObject> deleteMau68(JObject model)
        {
            JObject result = await this._invoiceService68.deleteMau68(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/quanlymau68")]
        public async Task<HttpResponseMessage> SaveQuanlymau68()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            byte[] bytes = null;
            string fileName = null;
            JObject model = new JObject();

            try
            {

                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }
                        else
                        {
                            model.Add(stream.Headers.ContentDisposition.Name.ToString().Replace("\"", ""), await stream.ReadAsStringAsync());
                        }
                    }
                }

                var json = new JObject();

                if (!fileName.EndsWith("repx"))
                {
                    json.Add("error", "File tải lên không đúng");
                    json.Add("status", "server");

                    return Request.CreateResponse(HttpStatusCode.OK, json); ;
                }

                JObject result = await _invoiceService68.SaveQuanlymau68(model["editmode"].ToString(), model, bytes);

                if (result["error"] != null)
                {
                    json.Add("error", result);
                    json.Add("status", "server");
                }
                else
                {
                    json.Add("ok", true);
                    json.Add("status", "server");
                }

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("Invoice68/DowloadRepx")]
        public HttpResponseMessage DowloadRepx(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                byte[] bytes = _invoiceService68.DowloadRepx(id);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.repx";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice68/quanlykyhieu68")]
        public async Task<JObject> SaveQuanlykyhieu68(JObject model)
        {
            string editmode = model["editmode"].ToString();
            JObject result = await this._invoiceService68.SaveQuanlykyhieu68(editmode, model);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetDataquanlykyhieu68")]
        public async Task<JObject> GetDataquanlykyhieu68(int start, int count, string filter, string tlbparam)
        {
            tlbparam = tlbparam.Replace("code", "a.code");
            string select = @"SELECT qlkhsdung_id as id, qlkhsdung_id, lhdon, hthuc, khdon, khhdon, sdmau, a.code as code, nglap, nlap, 
                                b.qlmtke_id, stt, mmau, tmau, ttsdung, ngtao, ntao, c.tt_sd 
                                FROM #SCHEMA_NAME#.quanlykyhieu68 a 
                                INNER JOIN #SCHEMA_NAME#.quanlymau68 b ON a.qlmtke_id=b.qlmtke_id 
                                LEFT JOIN (select count(*) as tt_sd, a.cctbao_id 
                                                from #SCHEMA_NAME#.hoadon68 a 
                                                inner join #SCHEMA_NAME#.quanlykyhieu68 b ON a.cctbao_id = b.qlkhsdung_id group by a.cctbao_id) c 
                                            ON a.qlkhsdung_id = c.cctbao_id ";

            string ptrang = "ORDER BY nlap desc, lhdon desc OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";

            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, tlbparam);

            return obj;
        }

        [HttpPost]
        [Route("Invoice68/deletequanlykyhieu68")]
        public async Task<JObject> deletequanlykyhieu68(JObject model)
        {
            JObject result = await this._invoiceService68.deletequanlykyhieu68(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/SaveBangTHDL")]
        public async Task<JObject> SaveBangTHDL(JObject model)
        {
            JObject result = await this._invoiceService68.SaveBangTHDL(model, this.UserName);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMauHoadon")]
        public async Task<HttpResponseMessage> inMauHoadon(string id)
        {

            HttpResponseMessage result = null;


            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/App" : "~/Content/App";
                var folder = HttpContext.Current.Server.MapPath(path);

                var xml = File.ReadAllText($"{folder}/SQL/test.xml");
                byte[] bytes = await _invoiceService68.inMauHoadon(id, "PDF", xml);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetDSHoaDonBangKeBanRa")]
        public async Task<JObject> GetDSHoaDonBangKeBanRa(JObject model)
        {
            JObject result = await this._invoiceService68.GetDSHoaDonBangKeBanRa(model);

            return result;
        }
        [HttpPost]
        [Route("Invoice68/GetDSHoaDonBangKeBanRaChiTiet")]
        public async Task<JObject> GetDSHoaDonBangKeBanRaChiTiet(JObject model)
        {
            JObject result = await this._invoiceService68.GetDSHoaDonBangKeBanRaChiTiet(model);

            return result;
        }
        [HttpGet]
        [Route("Invoice68/GetDsBangKeBanRa")]
        public async Task<JObject> GetDsBangKeBanRa(int start, int count, string filter)
        {
            //string select = "SELECT * FROM (SELECT bangkebanra_68_id as id,lkdlieu || (case  when lkdlieu = 'N' then ngay || '.' else '' end) || thang || '.' || nam as kdlieu, * FROM #SCHEMA_NAME#.bangkebanra_68 a)";

            string select = "SELECT * FROM (SELECT bangkebanra_68_id as id,lkdlieu || (case  when lkdlieu = 'N' then ngay || '.' else '' end) || thang || '.' || nam as kdlieu, * FROM #SCHEMA_NAME#.bangkebanra_68 a)";

            //" LEFT JOIN #SCHEMA_NAME#.tonghop_gui_tvan_68 c ON a.hdon_id = c.type_id";
            string ptrang = "ORDER BY nlap DESC OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, null);

            return obj;
        }

        [HttpPost]
        [Route("Invoice68/SaveBangKeBanRa")]
        public async Task<JObject> SaveBangKeBanRa(JObject model)
        {
            JObject result = await this._invoiceService68.SaveBangKeBanRa(model, this.UserName);

            return result;
        }


        [HttpGet]
        [Route("Invoice68/GetDataBangKeBanRaDetail")]
        public async Task<JArray> GetDataBangKeBanRaDetail(string id)
        {
            JArray result = await _invoiceService68.GetDataBangKeBanRaDetail(id);

            return result;
        }


        [HttpPost]
        [Route("Invoice68/SignBangKeBanRa")]
        public async Task<JObject> SignBangKeBanRa(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SignBangKeBanRa(model);
            return res;
        }


        [HttpPost]
        [Route("Invoice68/SaveXmlBangKeBanRa")]
        public async Task<JObject> SaveXmlBangKeBanRa(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveXmlBangKeBanRa(model);
            return res;
        }

        [HttpGet]
        [Route("Invoice68/XmlBangKeBanRa")]
        public async Task<JObject> XmlBangKeBanRa(string id)
        {
            JObject ar = await this._invoiceService68.XmlBangKeBanRa(id);
            return ar;
        }


        [HttpGet]
        [Route("Invoice68/inBangKeBanRa")]
        public async Task<HttpResponseMessage> inBangKeBanRa(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/BKBR_68.repx" : "~/Content/report/BKBR_68.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inBangKeBanRa(id, folder, type);


                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentLength = bytes.Length;

                if (type == "pdf")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "BangkeBanRa.pdf";
                }
                else if (type == "xlsx")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "BangkeBanRa.xlsx";
                }

            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }


        [HttpGet]
        [Route("Invoice68/ExportXMLBangKeBanRa")]
        public async Task<HttpResponseMessage> ExportXMLBangKeBanRa(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLBangKeBanRa(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "BangKeBanRa.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetInfoCompany")]
        public async Task<JObject> GetInfoCompany()
        {
            string branch_code = this.Ma_dvcs;
            JArray lst = await _invoiceService68.GetInfoCompany(branch_code);

            return (JObject)lst[0];
        }


        [HttpGet]
        [Route("Invoice68/ExportXMLBTHDLRep")]
        public async Task<HttpResponseMessage> ExportXMLBTHDLRep(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                string xml = await _invoiceService68.ExportXMLBTHDLRep(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau04.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }


        [HttpGet]
        [Route("Invoice68/inBTHDLRep")]
        public async Task<HttpResponseMessage> inBTHDLRep(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = string.Empty;

                //kiem tra xml bang tong hop xang dau/ khong xang dau de in
                string xml = "";
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id='" + id + "'");
                xml = tblHoadon.Rows[0]["xml_nhan"].ToString();
                if (xml.Contains("<LBTHKXDau>"))
                {
                    path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDL_KQ_68.repx" : "~/Content/report/BTHDL_KQ_68.repx";
                }
                else
                {
                    path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDLXDau_KQ_68.repx" : "~/Content/report/BTHDLXDau_KQ_68.repx";
                }
                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService68.inBTHDLRep(id, folder, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        [HttpGet]
        [Route("Invoice68/GetHistoryTruyenNhan")]
        public async Task<JObject> GetHistoryTruyenNhan(int start, int count, string filter = null, string tggui_tu = null, string tggui_den = null, int check_error = 0)
        {
            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/App" : "~/Content/App";
            var folder = HttpContext.Current.Server.MapPath(path);

            var sql = File.ReadAllText($"{folder}/SQL/getTruyenNhan.sql");

            var obj = await _invoiceService68.GetDataTruyenNhan(sql, start, count, filter, tggui_tu, tggui_den, check_error);

            return obj;
        }
        [HttpGet]
        [Route("Invoice68/GetHistoryTruyenNhan_ChiTiet")]
        public async Task<JObject> GetHistoryTruyenNhan_ChiTiet(string id)
        {
            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/App" : "~/Content/App";
            var folder = HttpContext.Current.Server.MapPath(path);

            var sql = File.ReadAllText($"{folder}/SQL/getTruyenNhan.sql");

            var obj = await _invoiceService68.GetDataTruyenNhan_Chitiet(sql, id);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetHoaDonkyhieu68")]
        public async Task<JObject> GetHoaDonkyhieu68(int start, int count, string filter, string tlbparam = null)
        {
            string select = "SELECT p.*,q.khhdon as khhdon FROM #SCHEMA_NAME#.phanhoadon_78 p join #SCHEMA_NAME#.quanlykyhieu68 q on p.kyhieu_id =q.qlkhsdung_id   ";
            string ptrang = "ORDER BY ngaybatdau desc OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";

            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, tlbparam);

            return obj;
        }

        [HttpPost]
        [Route("Invoice68/hoadonkihieu68")]
        public async Task<JObject> SaveSoHoaDonKiHieu(JObject model)
        {
            //string editmode = model["editmode"].ToString();
            JObject result = await this._invoiceService68.SaveSoHoaDonKiHieu(model);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetKyHieuDonVi68")]
        public async Task<JObject> GetKyHieuDonVi68(int start, int count, string filter, string tlbparam)
        {
            string select = @"select pd.id as phanhoadon_78_donvi_id,pd.phanhoadon_78_id,pd.branch_code as donvisudung,p.*,q.khhdon as khhdon from #SCHEMA_NAME#.phanhoadon_78_donvi pd 

                            join #SCHEMA_NAME#.phanhoadon_78 p on pd.phanhoadon_78_id = p.id join #SCHEMA_NAME#.quanlykyhieu68 q on p.kyhieu_id =q.qlkhsdung_id ";
            string ptrang = "ORDER BY ngaybatdau desc OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";

            var obj = await _invoiceService68.GetData(select, ptrang, start, count, filter, tlbparam);

            return obj;
        }

        [HttpPost]
        [Route("Invoice68/kihieudonvi68")]
        public async Task<JObject> SaveDonvikihieu68(JObject model)
        {
            // string editmode = model["editmode"].ToString();
            JObject result = await this._invoiceService68.PhanKiHieuDonVi(model);

            return result;
        }
        [HttpPost]
        [Route("Invoice68/deletekihieudonvi68")]
        public async Task<JObject> DeleteDonvikihieu68(JObject model)
        {
            // string editmode = model["editmode"].ToString();
            JObject result = await this._invoiceService68.DeletePhanKiHieuDonVi(model);

            return result;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("Invoice68/GenerateThongDiep")]
        public async Task GenerateThongDiep()
        {
            await _invoiceService68.GenerateThongDiep();
        }
        [HttpGet]
        [Route("Invoice68/GetDataInvoiceKhac")]
        public async Task<JArray> GetDataInvoiceKhaci(string id)
        {
            JArray result = await _invoiceService68.GetDataInvoiceKhac(id);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/PrintPDFBangKeBanRa")]
        public async Task<HttpResponseMessage> PrintPDFBangKeBanRa(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/68BangKeBanRa_WEB.repx" : "~/Content/report/68BangKeBanRa_WEB.repx";
                string type = model["type"].ToString();
                path = System.Web.HttpContext.Current.Server.MapPath(path);

                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string ky_hieu = model["ky_hieu"].ToString();
                string lst_branch_code = model["lst_branch_code"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", this.Ma_dvcs);
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));
                parameters.Add("ky_hieu", ky_hieu);
                parameters.Add("lst_branch_code", lst_branch_code);

                string sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bangkebanra_68_new('#SCHEMA_NAME#',@ma_dvcs,@lst_branch_code,@ky_hieu,@tu_ngay,@den_ngay)";
                DataSet ds = await this._minvoiceDbContext.GetDataSetAsyncPrintf(sql, CommandType.Text, parameters);

                byte[] bytes = ReportUtil.PrintReport(ds, path, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "BangKeBanRa." + type;
                if (type == "PDF")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        [HttpPost]
        [Route("Invoice68/PrintPDFBangKeBanRaChiTiet")]
        public async Task<HttpResponseMessage> PrintPDFBangKeBanRaChiTiet(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/68BangKeBanRaChiTiet_WEB.repx" : "~/Content/report/68BangKeBanRaChiTiet_WEB.repx";
                string type = model["type"].ToString();
                path = System.Web.HttpContext.Current.Server.MapPath(path);

                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string ky_hieu = model["ky_hieu"].ToString();
                string lst_branch_code = model["lst_branch_code"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", this.Ma_dvcs);
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));
                parameters.Add("ky_hieu", ky_hieu);
                parameters.Add("lst_branch_code", lst_branch_code);

                string sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bangkebanra_68_hatm('#SCHEMA_NAME#',@ma_dvcs,@lst_branch_code,@ky_hieu,@tu_ngay,@den_ngay)";
                DataSet ds = await this._minvoiceDbContext.GetDataSetAsyncPrintf(sql, CommandType.Text, parameters);

                byte[] bytes = ReportUtil.PrintReport(ds, path, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "BangKeBanRa." + type;
                if (type == "PDF")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        [HttpGet]
        [Route("Invoice68/GetDecimalPlace")]
        public async Task<JArray> GetDecimalPlace(string ma_nt)
        {
            JArray jarr = await this._invoiceService68.GetDecimalPlace(ma_nt);
            return jarr;
        }

        [HttpGet]
        [Route("Invoice68/Amount_ToWord_New")]
        public async Task<JObject> Amount_ToWord_New(string number, int checkReadTV)
        {
            JObject jarr = await this._invoiceService68.Amount_ToWord_New(number, checkReadTV);
            return jarr;
        }

        [HttpGet]
        [Route("Invoice68/SignConfirm")]
        [AllowAnonymous]
        public async Task<JObject> SignConfirm(Guid id)
        {
            return await _invoiceService68.GetXmlData(id);
        }

        [HttpPost]
        [Route("Invoice68/SignConfirm")]
        [AllowAnonymous]
        public async Task<JObject> SignConfirm(JObject model)
        {
            return await _invoiceService68.SignConfirm(model);
        }

        [HttpGet]
        [Route("Invoice68/GetDataInvoiceDetail_BangKe")]
        public async Task<JArray> GetDataInvoiceDetail_BangKe(string id)
        {
            JArray result = await _invoiceService68.GetDataInvoiceDetail_BangKe(id);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetById_Pretreatment")]
        public async Task<JObject> GetById_Pretreatment(Guid id)
        {
            JObject result = await this._invoiceService68.GetInvoiceById_Pretreatment(id);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/SaveHoadon78Pretreatment")]
        public async Task<JObject> SaveHoadon78Pretreatment(JObject model)
        {
            JObject result = await this._invoiceService68.SaveHoadon78Pretreatment(model, this.UserName);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/SaveListHoadon78Pretreatment")]
        public async Task<JArray> SaveListHoadon78Pretreatment(JObject modelList)
        {
            JArray arr_result = new JArray();
            String editMode = modelList["editmode"]?.ToString();
            JArray arr_model = (JArray)modelList["data"];

            foreach (JObject item in arr_model)
            {
                JObject model = new JObject();
                model.Add("editmode", editMode);

                JArray data_model = new JArray();
                data_model.Add(item);

                model.Add("data", data_model);

                JObject result = await this._invoiceService68.SaveHoadon78Pretreatment(model, this.UserName);
                arr_result.Add(result);
            }

            return arr_result;
        }

        [HttpGet]
        [Route("Invoice68/GetById")]
        public async Task<JObject> GetById(Guid id)
        {
            JObject result = await this._invoiceService68.GetInvoiceById(id);
            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetByIdSCTV")]
        public async Task<JObject> GetByIdSCTV(Guid id)
        {
            JObject result = await this._invoiceService68.GetInvoiceById(id);

            result = FunctionUtil.AddInfoToResult(result);
            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetInvoiceFromdateTodate")]
        public async Task<JArray> GetInvoiceFromdateTodate(JObject model)
        {
            JArray result = await _invoiceService68.GetInvoiceFromdateTodate(model);
            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetInvoiceFromdateTodateSCTV")]
        public async Task<JObject> GetInvoiceFromdateTodateSCTV(JObject model)
        {
            JArray result = await _invoiceService68.GetInvoiceFromdateTodate(model);

            JObject res = FunctionUtil.AddInfoToResult(result);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/GetListInvoiceIdFromdateTodate")]
        public async Task<JArray> GetListInvoiceIdFromdateTodate(JObject model)
        {
            JArray result = await _invoiceService68.GetListInvoiceIdFromdateTodate(model);
            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetListInvoiceIdFromdateTodateSCTV")]
        public async Task<JObject> GetListInvoiceIdFromdateTodateSCTV(JObject model)
        {
            JArray result = await _invoiceService68.GetListInvoiceIdFromdateTodate(model);

            JObject res = FunctionUtil.AddInfoToResult(result);

            return res;
        }


        [HttpPost]
        [Route("Invoice68/GetListInvoiceIdFromdateTodate_Pretreatment")]
        public async Task<JArray> GetListInvoiceIdFromdateTodate_Pretreatment(JObject model)
        {
            JArray result = await _invoiceService68.GetListInvoiceIdFromdateTodate_Pretreatment(model);
            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetInvoiceFromdateTodate_Pretreatment")]
        public async Task<JArray> GetInvoiceFromdateTodate_Pretreatment(JObject model)
        {
            JArray result = await _invoiceService68.GetInvoiceFromdateTodate_Pretreatment(model);
            return result;
        }

        [HttpGet]
        [Route("Invoice68/DashboardTiles78")]
        public JObject DashboardTiles78()
        {
            JArray result = this._invoiceService68.DashboardTiles78(this.Ma_dvcs);

            JObject obj = new JObject();
            obj.Add("items", result);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetEmailTemplate78")]
        public async Task<JObject> GetEmailTemplate78(string type, string id)
        {
            Uri uri = this.ActionContext.Request.RequestUri;
            string originalString = uri.Scheme + "://" + uri.Authority;

            return await _invoiceService68.GetEmailTemplate78(type, id, this.Ma_dvcs, originalString);
        }

        [HttpPost]
        [Route("Invoice68/hoadonXoaNhieu")]
        public async Task<JObject> hoadonXoaNhieu(JObject model)
        {

            JObject result = await this._invoiceService68.Xoanhieuhoadon((JArray)model["data"], this.UserName);
            return result;
        }

        [HttpPost]
        [Route("Invoice68/SignListInvoiceCertFile_HSM")]
        public async Task<JObject> SignListInvoiceCertFile_HSM(JObject model)
        {
            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

            var result = await _invoiceService68.SignListInvoiceCertFile_HSM(folder, model);

            return result;
        }

        [HttpGet]
        [Route("Invoice68/ExportXMLBangKe_HD")]
        public async Task<HttpResponseMessage> ExportXMLBangKe_HD(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                //get file name invoice theo Id hóa đơn
                string filename = await _invoiceService68.GetInvoiceFileName(id);

                string xml = await _invoiceService68.ExportXMLBangKe_HD(id);
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = filename + "_BangKe.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice68/huyhoadonSavePretreatment")]
        public async Task<JObject> huyhoadonSavePretreatment(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveHuyhoadonPretreatment(model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/SignDangkysudung_HSM")]
        public async Task<JObject> SignDangkysudung_HSM(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SignDangkysudung_HSM(model);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/DeleteBangKeHoaDon")]
        public async Task<bool> DeleteBangKeHoaDon(string id)
        {
            try
            {
                string sql_ct = $"DELETE FROM #SCHEMA_NAME#.bangke_kemtheo_chitiet WHERE hdon_id = '{id}'";
                string sql_bk = $"DELETE FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id = '{id}'";
                string sql_hdctbke = $"DELETE FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id = '{id}' and ten like N'%Kèm theo bảng kê số%'";
                string sql_hd = $"UPDATE #SCHEMA_NAME#.hoadon68 SET sbke = null, nbke = null where hdon_id='{id}'";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_ct);
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_bk);
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_hdctbke);
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_hd);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        [Route("Invoice68/UploadBangKeTemplate")]
        public async Task<HttpResponseMessage> UploadBangKeTemplate()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            byte[] bytes = null;
            string dmmauhoadon_id = null;
            string fileName = null;

            try
            {

                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }
                        else
                        {
                            dmmauhoadon_id = await stream.ReadAsStringAsync();
                        }
                    }
                }

                var json = new JObject();

                if (!fileName.EndsWith("repx"))
                {
                    json.Add("error", "File tải lên không đúng");
                    json.Add("status", "server");

                    return Request.CreateResponse(HttpStatusCode.OK, json); ;
                }

                string str = System.Text.Encoding.UTF8.GetString(bytes);

                if (CommonManager.CheckForRemoteCommand(str) == true)
                {
                    json.Add("error", "File chứa dữ liệu gây hại!");
                    return Request.CreateResponse(HttpStatusCode.OK, json);

                }

                string result = await _invoiceService68.UploadBangKeTemplate(this.Ma_dvcs, this.UserName, dmmauhoadon_id, bytes);



                if (result.Length > 0)
                {
                    json.Add("error", result);
                    json.Add("status", "server");
                }
                else
                {
                    json.Add("ok", true);
                    json.Add("status", "server");
                }

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("Invoice68/DowloadRepxBangKe")]
        public HttpResponseMessage DowloadRepxBangKe(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                byte[] bytes = _invoiceService68.DowloadRepxBangKe(id);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "BangKeTemplate.repx";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inMauHoadon_QuanLyMau")]
        public async Task<HttpResponseMessage> inMauHoadon_QuanLyMau(string id, bool checkBangKe = false)
        {
            HttpResponseMessage result = null;
            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/App" : "~/Content/App";
                var folder = HttpContext.Current.Server.MapPath(path);

                var xml = File.ReadAllText($"{folder}/SQL/test.xml");
                byte[] bytes = await _invoiceService68.inMauHoadon_QuanLyMau(id, "PDF", xml, checkBangKe);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = checkBangKe == false ? "InvoiceTemplate.pdf" : "BangKeTemplate.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetCTS_HSM_ToKhai")]
        public async Task<JObject> GetCTS_HSM_ToKhai()
        {
            var result = await _invoiceService68.GetCTS_HSM_ToKhai();
            return result;
        }

        [HttpPost]
        [Route("Invoice68/SaveBangTHDLXangDau")]
        public async Task<JObject> SaveBangTHDLXangDau(JObject model)
        {
            JObject result = await this._invoiceService68.SaveBangTHDLXangDau(model, this.UserName);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/GetDetailHoaDonXangDauChoXuLy")]
        public async Task<JArray> GetDetailHoaDonXangDauChoXuLy(JObject model)
        {
            string data = model["data"]?.ToString();
            JArray result = await this._invoiceService68.GetDetailHoaDonXangDauChoXuLy(data);
            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetDataBangTHDLXangDauDetail")]
        public async Task<JArray> GetDataBangTHDLXangDauDetail(string id)
        {
            JArray result = await _invoiceService68.GetDataBangTHDLXangDauDetail(id);

            return result;
        }

        [HttpPost]
        [Route("Invoice68/SignBTHDLXangDau")]
        public async Task<JObject> SignBTHDLXangDau(JObject model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SignBTHDLXangDau(model);
            return res;
        }

        [HttpGet]
        [Route("Invoice68/XmlBTHDLXangDau")]
        public async Task<JObject> XmlBTHDLXangDau(string id)
        {
            JObject ar = await this._invoiceService68.XmlBTHDLXangDau(id);
            return ar;
        }

        [HttpPost]
        [Route("Invoice68/SaveXmlBTHDLXangDau")]
        public async Task<JObject> SaveXmlBTHDLXangDau(JArray model)
        {
            JObject res = new JObject();
            res = await this._invoiceService68.SaveXmlBTHDLXangDau(model);
            return res;
        }

        [HttpGet]
        [Route("Invoice68/inBTHDLieuXangDau")]
        public async Task<HttpResponseMessage> inBTHDLieuXangDau(string id)
        {
            HttpResponseMessage result = null;

            try
            {

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDL_68.repx" : "~/Content/report/BTHDL_68.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inBTHDLieuXangDau(id, folder, "PDF");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau04.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice68/InDanhSachHoaDon")]
        public async Task<HttpResponseMessage> InDanhSachHoaDon(JObject model)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                var type = model["type"].ToString();
                var inChuyenDoi = Convert.ToBoolean(model["inchuyendoi"]);

                byte[] bytes = await _invoiceService68.InDanhSachHoaDon(model, folder, type, inChuyenDoi, false);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice68/TaiDanhSachHoaDon")]
        public async Task<HttpResponseMessage> TaiDanhSachHoaDon(JObject model)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                var type = model["type"]?.ToString();
                var inChuyenDoi = Convert.ToBoolean(model["inchuyendoi"]);

                byte[] bytes = await _invoiceService68.TaiDanhSachHoaDon(model, folder, type, inChuyenDoi, false);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "Invoice.zip";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");

                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.BadRequest);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/uploadCanceledInv")]
        public async Task<JObject> uploadCanceledInv(string id)
        {
            return await _invoiceService68.uploadCanceledInv(id);
        }

        [HttpGet]
        [Route("Invoice68/uploadCanceledInvSCTV")]
        public async Task<JObject> uploadCanceledInvSCTV(string id)
        {
            JObject res = await _invoiceService68.uploadCanceledInv(id);

            res = FunctionUtil.AddInfoToResult(res);
            return res;
        }

        [HttpPost]
        [Route("Invoice68/GetHoadonChoXuLyall")]
        public async Task<JObject> GetHoadonChoXuLyAll(JObject model)
        {
            JObject result = await this._invoiceService68.GetHoadonChoXuLy_LayToanBoHoaDon(model);
            return result;
        }

        [HttpGet]
        [Route("Invoice68/excelBTHDLieu")]
        public async Task<HttpResponseMessage> excelBTHDLieu(string id)
        {
            HttpResponseMessage result = null;

            try
            {

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDL_68.repx" : "~/Content/report/BTHDL_68.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inBTHDLieu(id, folder, "Excel");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau04.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;

            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }


        [HttpGet]
        [Route("Invoice68/excelBTHDLieuXangDau")]
        public async Task<HttpResponseMessage> excelBTHDLieuXangDau(string id)
        {
            HttpResponseMessage result = null;

            try
            {

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/BTHDL_68.repx" : "~/Content/report/BTHDL_68.repx";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inBTHDLieuXangDau(id, folder, "Excel");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "Mau04.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/inHoadonBase64")]
        public async Task<HttpResponseMessage> inHoadonBase64(string id, string type, bool inchuyendoi, bool checkBangKe = false)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService68.inHoadon(id, folder, type, inchuyendoi, checkBangKe);

                string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(base64String);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = base64String.Length;

            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice68/GetInvoiceSeriesByCurrentUser")]
        public async Task<JArray> GetInvoiceSeriesByCurrentUser()
        {
            JArray result = new JArray();
            try
            {
                var sqlCheckRole = $@"select a.wb_role_id from #SCHEMA_NAME#.wb_role a 
                                        join #SCHEMA_NAME#.wb_user b on a.wb_role_id = b.wb_role_id
                                     where a.role_type = 'QT' and b.username = '{UserName}'";
                DataTable rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheckRole);

                if (rsTable.Rows.Count > 0)
                {
                    var sql = $@"select q.khhdon as id, q.khhdon as value
                                from #SCHEMA_NAME#.phanhoadon_78 p
                                         join #SCHEMA_NAME#.phanhoadon_78_donvi p78 on p78.phanhoadon_78_id = p.id
                                         join #SCHEMA_NAME#.quanlykyhieu68 q on p.kyhieu_id = q.qlkhsdung_id 
                                where p78.branch_code in (select code from #SCHEMA_NAME#.wb_branch
                                                        where position('{Ma_dvcs}' || '->' in tree_sort) > 0
                                                          and tree_rank >= (select tree_rank from #SCHEMA_NAME#.wb_branch where code = '{Ma_dvcs}'))";
                    DataTable rs = await _minvoiceDbContext.GetDataTableAsync(sql);
                    result = JArray.FromObject(rs);
                }
                else
                {
                    var sql = $@"select c.khhdon as id, c.khhdon as value
                                from #SCHEMA_NAME#.phanhoadon_78 p
                                         inner join #SCHEMA_NAME#.phanhoadon_78_donvi p78 on p78.phanhoadon_78_id = p.id
                                         join (select q.khhdon, q.qlkhsdung_id
                                               from #SCHEMA_NAME#.permission_kyhieu68 a
                                                        join #SCHEMA_NAME#.wb_user b on a.wb_user_id = b.wb_user_id
                                                        join #SCHEMA_NAME#.quanlykyhieu68 q on a.qlkhsdung_id = q.qlkhsdung_id
                                               where b.username = '{UserName}') c on p.kyhieu_id = c.qlkhsdung_id
                                where p78.branch_code = '{Ma_dvcs}'";
                    DataTable rs = await _minvoiceDbContext.GetDataTableAsync(sql);
                    result = JArray.FromObject(rs);
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }

        [HttpPost]
        [Route("Invoice68/PrintDSHoaDonChiTiet")]
        public async Task<HttpResponseMessage> PrintDSHoaDonChiTiet(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/DSHoaDonChiTiet.repx" : "~/Content/report/DSHoaDonChiTiet.repx";
                string type = model["type"].ToString();
                path = System.Web.HttpContext.Current.Server.MapPath(path);

                string sql =
                    @"select a.mhang, a.thang, a.mdvtinh, a.sluong, a.dgia, a.tlckhau, a.stckhau, a.thtien, c.ttthdon, tnmua, tgtttbso_last, sbmat,
                                       case when a.tsuat = '-1' then 'KCT'
                                            when a.tsuat = '-2' then 'KKKNT' 
                                            else a.tsuat end as tsuat, a.tthue, a.tgtien,
                                       khieu, sdhang, shdon, to_char(nlap, 'DD/MM/YYYY') as nlap, dvtte, tgia, mnmua, b.ten, mst, dchi, htttoan,
                                       mccqthue, shdon::numeric as ord, nlap as ord1
                                from (select hdon_id, ma as mhang, ten as thang, mdvtinh, sluong, dgia, tlckhau, stckhau, thtien, tsuat, tthue, tgtien
                                      from #SCHEMA_NAME#.hoadon68_chitiet) a
                                         right join (select hdon_id, cctbao_id, khieu, sdhang, shdon, nlap, nky, dvtte, tgia, mnmua, tthai, tthdon, tgtttbso_last, sbmat,
                                               (case when ten is not null and ten <> '' then ten else tnhmua end) as ten, mst, dchi, htttoan, tnmua, mccqthue              
                                               from #SCHEMA_NAME#.hoadon68 where cctbao_id = @cctbao_id) b on a.hdon_id = b.hdon_id 
                                         join #SCHEMA_NAME#.trangthaihoadon68 c on b.tthdon = c.mtthdon ";
                
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("cctbao_id", Guid.Parse(model["cctbao_id"].ToString()));
                JArray filter = null;
                if (model["filter"] != null)
                {
                    filter = JArray.FromObject(model["filter"]);
                }
                var order = "order by ord1 desc, ord desc";
                
                DataSet ds = await _invoiceService68.GetDataForPrint(sql, parameters, filter, order);
                
                byte[] bytes = ReportUtil.PrintReport(ds, path, type, true);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "DSHoaDonChiTiet." + type;
                if (type == "PDF")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        [HttpGet]
        [Route("Invoice68/GetDataTraCuu")]
        public async Task<JObject> GetDataTraCuu(int start, int count, string filter)
        {

            string select = "select * from (select *, Case when active =1 then 'Hoạt động' when active =2 then 'Tạm khóa' else 'Chưa kích hoạt' end as active_str  from  #SCHEMA_NAME#.inv_user) a ";

            string ptrang = "ORDER BY a.date_new DESC, code_customer OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";

            var obj = await _invoiceService68.GetDataMaster(select, ptrang, start, count, filter,null);

            return obj;
        }
        [HttpPost]
        [Route("Invoice68/UpdateCreatTraCuu")]
        public async Task<JObject> UpdateCreatTraCuu(JObject input)
        {
            var obj = await _invoiceService68.RegisterUpdateAcount(input);

            return obj;
        }

        [HttpGet]
        [Route("Invoice68/GetDataInvoiceListChoKy")]
        public async Task<JObject> GetDataInvoiceListChoKy()
        {
            var obj = new JObject();
            try
            {
                string selectKiHieu = @"SELECT string_agg(cast(a.qlkhsdung_id as text),''',''')  as id FROM #SCHEMA_NAME#.quanlykyhieu68 a
                                    INNER JOIN #SCHEMA_NAME#.permission_kyhieu68 b ON a.qlkhsdung_id = b.qlkhsdung_id
                                    WHERE b.wb_user_id = (SELECT wb_user_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)";
                Dictionary<string, object> param = new Dictionary<string, object> { { "username", UserName } };
                DataTable kihieu = await _minvoiceDbContext.GetDataTableAsync(selectKiHieu, CommandType.Text, param);
                var cctbaoIds = "";
                if (kihieu.Rows.Count > 0 && kihieu.Rows[0][0] != null && !string.IsNullOrEmpty(kihieu.Rows[0][0].ToString()))
                {
                    cctbaoIds = kihieu.Rows[0][0].ToString();
                }
                else
                {
                    throw new Exception("Người dùng không có quyền xem hóa đơn");
                }

                string select = $@"select * from (SELECT a.*, b.*, a.hdon_id as id,case when ten <> '' and ten is not null then ten else  tnmua end as ten_str, (a.shdon ::numeric) as ord, 
                                                     Case when a.is_hdcma = 0 then 'Không mã' else 'Có mã' end as is_hdcma_str 
                                              FROM #SCHEMA_NAME#.hoadon68 a INNER JOIN #SCHEMA_NAME#.trangthaihoadon68 b ON a.tthdon = b.mtthdon 
                                              WHERE a.mdvi = '{this.Ma_dvcs}' and a.tthai ='Chờ ký' and cctbao_id in ('{cctbaoIds}')) a 
                                              ORDER BY a.nlap ASC, ord ASC, a.tdlap DESC";
                var rsTable = await _minvoiceDbContext.GetDataTableAsync(select);
                if (rsTable.Rows.Count > 0)
                {
                    obj.Add("data", JToken.FromObject(rsTable));
                }
                else
                {
                    obj.Add("data", new JArray());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                obj.Add("error", ex.Message);
            }
            return obj;
        }

    }
}
