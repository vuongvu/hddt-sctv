﻿using InvoiceApi.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace InvoiceApi.Controllers
{
    public class TokenController : BaseApiController
    {
        private readonly ITokenService _tokenService;

        public TokenController(ITokenService tokenService)
        {
            this._tokenService = tokenService;
        }
        [HttpGet]
        [Route("Token/AllCertificatesFile")]
        [AllowAnonymous]
        public async Task<JArray> AllCertificatesFile()
        {
            var tokens = await _tokenService.AllCertificatesFile();
            return tokens;
        }

        [HttpPost]
        [Route("Token/SaveChukysoFile")]
        public async Task<JObject> SaveChukysoFile(JObject model)
        {
            var obj = await _tokenService.SaveChukysoFile(model);

            return obj;
        }

        [HttpPost]
        [Route("Token/SaveTokenFile")]
        public async Task<JObject> SaveTokenFile(JObject model)
        {
            var obj = await _tokenService.SaveTokenFile(model, this.UserName, this.Store_code);

            return obj;
        }


        [HttpGet]
        [Route("Token/SelectTokens")]
        [AllowAnonymous]
        public JArray SelectTokens()
        {
            var tokens = _tokenService.SelectTokens();

            return tokens;
        }

        [HttpGet]
        [Route("Token/AllCertificates")]
        [AllowAnonymous]
        public JArray GetAllCertificates()
        {
            var certs = _tokenService.GetAllCertificates();

            return certs;
        }

        [HttpPost]
        [Route("Token/SaveTokens")]
        public async Task<JObject> SaveTokens(JObject model)
        {
            var obj = await _tokenService.SaveTokens(model);

            return obj;
        }

        [HttpPost]
        [Route("Token/SaveCerts")]
        public async Task<JObject> SaveCerts(JObject model)
        {
            var obj = await _tokenService.SaveCerts(model);

            return obj;
        }

        [HttpPost]
        [Route("Token/SignXml")]
        [AllowAnonymous]
        public JObject SignXml(JObject model)
        {
            var obj = _tokenService.SignXml(model);

            return obj;
        }

        [HttpPost]
        [Route("Token/SignTB01AC")]
        [AllowAnonymous]
        public JObject SignTB01AC(JObject model)
        {
            var obj = _tokenService.SignTB01AC(model);

            return obj;
        }

        [HttpPost]
        [Route("Token/UploadCertFile")]
        public async Task<HttpResponseMessage> UploadCertFile()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            byte[] bytes = null;
            string pass = null;
            string fileName = null;

            try
            {

                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }
                        else
                        {
                            pass = await stream.ReadAsStringAsync();
                        }
                    }
                }



                await _tokenService.UploadCertFileAsync(this.Ma_dvcs, this.UserName, pass, bytes, fileName);

                var json = new JObject();
                json.Add("ok", true);
                json.Add("status", "server");

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        [HttpPost]
        [Route("Token/SaveSimPKI")]
        public async Task<JObject> SaveSimPKI(JObject model)
        {
            var obj = await _tokenService.SaveSimPKI(model);

            return obj;
        }

        [HttpPost]
        [Route("Token/SaveHSM_SCTV")]
        public async Task<JObject> SaveHsmSCTV(JObject model)
        {
            var obj = await _tokenService.SaveHsmSCTV(model);

            return obj;
        }
    }
}