﻿using InvoiceApi.Services;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace InvoiceApi.Controllers
{
    public class TaskController : BaseApiController
    {
        #region Fields

        private readonly ITaskService _taskService;

        #endregion

        #region Ctor

        public TaskController(ITaskService taskService)
        {
            this._taskService = taskService;
        }

        #endregion

        #region Methods

        [HttpPost]
        [Route("Task/CreateTask")]
        [AllowAnonymous]
        public JObject CreateTask(JObject model)
        {
            var result = _taskService.CreateTask(model);

            return result;
        }

        [HttpPost]
        [Route("Task/CreateInvoiceTemp")]
        public async Task<JObject> CreateInvoiceTemp(JObject model)
        {
            var result = await _taskService.CreateInvoiceTemp(model);

            return result;
        }

        [HttpPost]
        [Route("Task/CreateInvoiceAuth")]
        public async Task<JObject> CreateInvoiceAuth(JObject model)
        {
            string batch_num = model["batch_num"].ToString();
            if (CommonManager.CheckForSqlInjection(batch_num))
            {
                return new JObject { { "error", $"Giá trị {batch_num} không hợp lệ !" } };
            }
            var result = await _taskService.CreateInvoiceAuth(model);

            return result;
        }

        [HttpPost]
        [Route("Task/CreateReceipt")]
        public async Task<JObject> CreateReceipt(JObject model)
        {
            var result = await _taskService.CreateReceipt(model);

            return result;
        }

        [HttpPost]
        [Route("Task/SignInvoice")]
        public async Task<JObject> SignInvoice(JObject model)
        {
            string batch_num = model["batch_num"].ToString();
            if (CommonManager.CheckForSqlInjection(batch_num))
            {
                return new JObject { { "error", $"Giá trị {batch_num} không hợp lệ !" } };
            }
            var result = await _taskService.SignInvoice(model);

            return result;
        }

        [HttpPost]
        [Route("Task/SignReceipt")]
        public async Task<JObject> SignReceipt(JObject model)
        {
            string batch_num = model["batch_num"].ToString();
            if (CommonManager.CheckForSqlInjection(batch_num))
            {
                return new JObject { { "error", $"Giá trị {batch_num} không hợp lệ !" } };
            }
            var result = await _taskService.SignReceipt(model);

            return result;
        }

        [HttpPost]
        [Route("Task/SendEmailReceipt")]
        public async Task<JObject> SendEmailReceipt(JObject model)
        {
            var result = await _taskService.SendEmailReceipt(model);

            return result;
        }

        [HttpPost]
        [Route("Task/SendEmailToCustomer")]
        public async Task<JObject> SendEmailToCustomer(JObject model)
        {
            string batch_num = model["batch_num"].ToString();
            if (CommonManager.CheckForSqlInjection(batch_num))
            {
                return new JObject { { "error", $"Giá trị {batch_num} không hợp lệ !" } };
            }
            var result = await _taskService.SendEmailToCustomer(model);

            return result;
        }

        [HttpPost]
        [Route("Task/SendSmsToCustomer")]
        public async Task<JObject> SendSmsToCustomer(JObject model)
        {
            var result = await _taskService.SendSmsToCustomer(model);

            return result;
        }        

        [HttpPost]
        [Route("Task/ImportBatch")]
        public async Task<JObject> ImportBatch(JObject model)
        {
            var result = await _taskService.ImportBatch(model);

            return result;
        }

        [HttpPost]
        [Route("Task/Info")]
        public async Task<JObject> Info(JObject model)
        {
            var result = await _taskService.Info(model);

            return result;
        }

        #endregion
    }
}