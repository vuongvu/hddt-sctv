﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InvoiceApi.Data.Domain;
using InvoiceApi.Services;
using System.Threading.Tasks;

namespace InvoiceApi.Controllers
{
    public class WbWindowController : BaseApiController
    {
        private IWbWindowService _wbWindowService;

        public WbWindowController(IWbWindowService wbWindowService)
        {
            this._wbWindowService = wbWindowService;
        }

        [HttpGet]
        [Route("WbWindow/All")]
        public async Task<JArray> GetAll()
        {
            var tblData = await _wbWindowService.GetAll();
            return JArray.FromObject(tblData);
        }

        [HttpGet]
        [Route("WbWindow/GetWindows")]
        public async Task<JObject> GetWindows(int start,int count,string filter)
        {
            var json = await _wbWindowService.GetWindows(start, count, filter);

            return json;
        }

        [HttpPost]
        [Route("WbWindow/Update")]
        public async Task<JObject> Update(JObject model)
        {
            var json = await _wbWindowService.SaveChange(model);

            return json;
        }

        [HttpPost]
        [Route("WbWindow/Delete")]
        public async Task<JObject> Delete(JObject model)        {

            var json = await _wbWindowService.Delete(model["id"].ToString());

            return json;
        }
        

    }
}