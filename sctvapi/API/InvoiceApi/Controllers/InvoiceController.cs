﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Xml;
using InvoiceApi.Services;
using InvoiceApi.Util;
using System.Threading.Tasks;
using System.Text;
using InvoiceApi.Data;
using PdfSharp.Pdf.IO;

namespace InvoiceApi.Controllers
{
    public class InvoiceController : BaseApiController
    {
        private IInvoiceService _invoiceService;
        private IMinvoiceDbContext _minvoiceDbContext;

        public InvoiceController(IInvoiceService invoiceService, IMinvoiceDbContext minvoiceDbContext)
        {
            this._invoiceService = invoiceService;
            this._minvoiceDbContext = minvoiceDbContext;
        }

        [HttpGet]
        [Route("Invoice/GetSchema")]
        public string GetSchema()
        {
            return _invoiceService.GetSchema();
        }

        [HttpGet]
        [Route("Invoice/ImagePreview")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> ImagePreview(string id)
        {

            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService.PrintInvoiceFromId(id, folder, "Image", "");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "invoice.png";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/PreviewByInvoiceId")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PreviewByInvoiceId(long id)
        {

            JObject obj = await _invoiceService.GetByInvoiceId(id);

            if (obj["error"] != null)
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(obj.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                result.Content.Headers.ContentLength = obj.ToString().Length;

                return result;
            }

            string inv_invoiceauth_id = obj["inv_invoiceauth_id"].ToString();

            return await Preview(inv_invoiceauth_id);
        }

        [HttpGet]
        [Route("Invoice/PreviewByInvoiceIdNoInvoiceNumber")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PreviewByInvoiceIdNoInvoiceNumber(long id)
        {
            HttpResponseMessage result = null;
            JObject obj = await _invoiceService.GetByInvoiceId(id);

            if (obj["error"] != null)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(obj["error"].ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = obj["error"].ToString().Length;

                return result;
            }

            try
            {

                string inv_invoiceauth_id = obj["inv_invoiceauth_id"].ToString();

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService.PrintInvoiceFromId(inv_invoiceauth_id, folder, "PDF", "C");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "invoice.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/PreviewListInvoiceIdNoInvoiceNumber")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PreviewListInvoiceIdNoInvoiceNumber(List<long> id)
        {
            HttpResponseMessage result = null;
            List<JObject> obj = new List<JObject>();
            foreach (var item in id)
            {
                JObject objItem = await _invoiceService.GetByInvoiceId(item);
                if (objItem["error"] != null)
                {
                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(objItem["error"].ToString() +": "+ item);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    result.Content.Headers.ContentLength = objItem["error"].ToString().Length + item.ToString().Length + 3;

                    return result;
                }
                obj.Add(objItem);
            }

            try
            {
                var bytesList = new List<byte[]>();
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                foreach (var item in obj)
                {
                    string inv_invoiceauth_id = item["inv_invoiceauth_id"].ToString();
                    byte[] bytes = await _invoiceService.PrintInvoiceFromId(inv_invoiceauth_id, folder, "PDF", "C");
                    bytesList.Add(bytes);
                }
                var bytesMerge = InvoiceController.MergePdf(bytesList);
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytesMerge);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "invoice.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytesMerge.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/Preview")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Preview(string id)
        {

            HttpResponseMessage result = null;

            try
            {


                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService.PrintInvoiceFromId(id, folder, "PDF", "");
                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "invoice.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/Download")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Download(string id)
        {

            HttpResponseMessage result = null;

            try
            {

                JObject inv = await _invoiceService.GetInvoiceById(Guid.Parse(id));

                string template_code = inv["template_code"].ToString();
                string invoice_series = inv["invoice_series"].ToString();
                string invoice_number = inv["invoice_number"].ToString();

                string fileName = template_code.Replace("/", "_") + "_" + invoice_series.Replace("/", "_") + "_" + invoice_number + ".pdf";

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService.PrintInvoiceFromId(id, folder, "PDF", "");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                result.Content.Headers.ContentDisposition.FileName = fileName;
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/InChuyenDoi")]
        public async Task<HttpResponseMessage> InChuyenDoi(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                string id = model["id"].ToString();

                byte[] bytes = await _invoiceService.InChuyenDoi(id);

                if (bytes == null)
                {
                    throw new Exception("Hóa đơn đã được in chuyển đổi hoặc không tìm thấy hóa đơn");
                }

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "invoice.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                result.Content = new StringContent(ex.Message, Encoding.UTF8, "text/plain");

            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetById")]
        public async Task<JObject> GetById(Guid id)
        {
            JObject result = await this._invoiceService.GetInvoiceById(id);

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetByInvoiceId")]
        public async Task<JObject> GetByInvoiceId(long id)
        {
            JObject result = await this._invoiceService.GetByInvoiceId(id);

            return result;
        }

        [HttpGet]
        [Route("Invoice/InvoiceHistorySendEmail")]
        public async Task<JArray> InvoiceHistorySendEmail(string inv_invoiceauth_id)
        {
            JArray result = await this._invoiceService.InvoiceHistorySendEmail(inv_invoiceauth_id);

            return result;
        }

        [HttpPost]
        [Route("Invoice/Cancel")]
        public async Task<JObject> Cancel(JObject model)
        {
            string id = model["id"].ToString();
            JObject result = await this._invoiceService.CancelInvoice(id);

            return result;
        }

        [HttpPost]
        [Route("Invoice/XoaBoHD")]
        public async Task<JObject> XoaBoHD(JObject model)
        {
            foreach (JProperty prop in model.Properties())
            {
                var value = prop.Value.ToString();
                var isJSON = (value.StartsWith("{") && value.EndsWith("}")) || (value.StartsWith("[") && value.EndsWith("]"));
                if (CommonManager.CheckForSqlInjection(value) && !isJSON)
                {
                    return new JObject { { "error", $"Giá trị {value} không hợp lệ !" } };
                }
            }
            JObject result = await this._invoiceService.XoaBoHD(model);

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetHoaDonChoDuyet")]
        public async Task<JArray> GetHoaDonChoDuyet(string trangthai)
        {

            string trang_thai = "Chờ duyệt";

            if (trangthai != null)
            {
                trang_thai = trangthai;
            }

            JArray result = await this._invoiceService.GetHoaDonChoDuyet(this.Ma_dvcs, trang_thai);

            return result;
        }

        [HttpGet]
        [Route("Invoice/DashboardTiles")]
        public JObject DashboardTiles()
        {
            JArray result = this._invoiceService.DashboardTiles(this.Ma_dvcs);

            JObject obj = new JObject();
            obj.Add("items", result);

            return obj;
        }

        [HttpGet]
        [Route("Invoice/DashboardInvoices")]
        public JArray DashboardInvoices()
        {
            JArray result = this._invoiceService.DashboardInvoices(this.Ma_dvcs);

            return result;
        }

        [HttpGet]
        [Route("Invoice/ExportXml")]
        public async Task<JObject> ExportXml(Guid id)
        {
            JObject result = await this._invoiceService.ExportInvoiceXml(id);

            return result;
        }
        
        [HttpGet]
        [Route("Invoice/ExportXmlPretreatment")]
        public async Task<JObject> ExportXmlPretreatment(Guid id)
        {
            JObject result = await this._invoiceService.ExportInvoiceXmlPretreatment(id);

            return result;
        }

        [HttpPost]
        [Route("Invoice/SaveXml")]
        public async Task<JObject> SaveXml(JObject parameters)
        {
            JObject result = await this._invoiceService.SaveXml(parameters, this.UserName);

            return result;
        }

        [HttpPost]
        [Route("Invoice/SaveXmlPretreatment")]
        public async Task<JObject> SaveXmlPretreatment(JObject parameters)
        {
            JObject result = await this._invoiceService.SaveXmlPretreatment(parameters, this.UserName);

            return result;
        }

        [HttpPost]
        [Route("Invoice/SaveInvoicesXml")]
        public async Task<JObject> SaveInvoicesXml(JObject parameters)
        {
            JObject res = new JObject();

            try
            {
                JArray array = (JArray)parameters["data"];

                for (int i = 0; i < array.Count; i++)
                {
                    JObject obj = (JObject)array[i];
                    JObject result = await this._invoiceService.SaveXml(obj, this.UserName);

                    if (result["error"] != null)
                    {
                        return result;
                    }
                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                res.Add(ex.Message);
            }

            return res;
        }

        [HttpPost]
        [Route("Invoice/LaySoHoaDon")]
        public async Task<JObject> LaySoHoaDon(JObject parameters)
        {
            JObject result = await this._invoiceService.LaySoHoaDon(parameters, this.UserName);

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetByInvoiceNumber")]
        public async Task<JObject> GetByInvoiceNumber(string invoiceCode, string invoiceSeries, string invoiceNumber)
        {
            JObject result = await this._invoiceService.GetInvoiceByInvoiceNumber(invoiceCode, invoiceSeries, invoiceNumber);

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetListCertificates")]
        public async Task<JArray> GetListCertificates()
        {
            JArray result = await this._invoiceService.GetListCertificates(this.UserName, this.Ma_dvcs);

            return result;
        }

        [HttpPost]
        [Route("Invoice/GetThongTinXoaBo")]
        public async Task<JObject> GetThongTinXoaBo(JObject model)
        {
            return await this._invoiceService.GetInvoiceXoaBo(model);

        }

        [HttpPost]
        [Route("Invoice/GetThongTinXoaBoPretreatment")]
        public async Task<JObject> GetThongTinXoaBoPretreatment(JObject model)
        {
            return await this._invoiceService.GetInvoiceXoaBoPretreatment(model);

        }

        [HttpPost]
        [Route("Invoice/SaveInvoiceXoaBo")]
        public async Task<JObject> SaveInvoiceXoaBo(JObject model)
        {
            JObject result = await this._invoiceService.SaveInvoiceXoaBo(model);

            return result;
        }
        
        [HttpPost]
        [Route("Invoice/SaveInvoiceXoaBoPretreatment")]
        public async Task<JObject> SaveInvoiceXoaBoPretreatment(JObject model)
        {
            JObject result = await this._invoiceService.SaveInvoiceXoaBoPretreatment(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/DuyetHoaDon")]
        public async Task<JObject> DuyetHoaDon(JObject model)
        {
            JObject result = await this._invoiceService.DuyetHoaDon(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/TuChoiHoaDon")]
        public async Task<JObject> TuChoiHoaDon(JObject model)
        {
            JObject result = await this._invoiceService.TuChoiHoaDon(model);

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetTemplate")]
        public async Task<HttpResponseMessage> GetTemplate(Guid id)
        {

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                DataTable tblTemplate = await _invoiceService.GetTemplate(id);

                if (tblTemplate.Rows.Count == 0)
                {
                    JObject obj = new JObject();
                    obj.Add("error", "Không tìm thấy mẫu hóa đơn");

                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(obj.ToString());

                    result.Content = new ByteArrayContent(bytes);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    result.Content.Headers.ContentLength = bytes.Length;
                }
                else
                {
                    string report = tblTemplate.Rows[0]["report"].ToString();
                    string hinhthuc = tblTemplate.Rows[0]["invoice_type"].ToString();
                    string ma_loai = tblTemplate.Rows[0]["ma_loai"].ToString();

                    if (report.Length == 0)
                    {
                        string fileName = folder + "INHD_TEMP_" + ma_loai + "_" + hinhthuc + ".repx";

                        if (System.IO.File.Exists(fileName))
                        {
                            report = System.IO.File.ReadAllText(fileName, System.Text.Encoding.UTF8);
                        }
                    }

                    JObject obj = new JObject();
                    obj.Add("ok", true);
                    obj.Add("report", report);

                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(obj.ToString());

                    result.Content = new ByteArrayContent(bytes);
                    result.Content.Headers.ContentLength = bytes.Length;

                }
            }
            catch (Exception ex)
            {
                JObject obj = new JObject();
                obj.Add("error", ex.Message);

                result.Content = new StringContent(obj.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetDataTemplate")]
        public async Task<HttpResponseMessage> GetDataTemplate(Guid id)
        {

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                DataTable tblTemplate = await _invoiceService.GetTemplate(id);

                if (tblTemplate.Rows.Count == 0)
                {
                    JObject obj = new JObject();
                    obj.Add("error", "Không tìm thấy mẫu hóa đơn");

                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(obj.ToString());

                    result.Content = new ByteArrayContent(bytes);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    result.Content.Headers.ContentLength = bytes.Length;
                }
                else
                {
                    string report = tblTemplate.Rows[0]["report"].ToString();
                    string hinhthuc = tblTemplate.Rows[0]["invoice_type"].ToString();
                    string ma_loai = tblTemplate.Rows[0]["ma_loai"].ToString();
                    string mau_so = tblTemplate.Rows[0]["template_code"].ToString();
                    string ky_hieu = tblTemplate.Rows[0]["invoice_series"].ToString();
                    string ten_loai = tblTemplate.Rows[0]["ten_loai"].ToString();
                    string ten_dvcs = tblTemplate.Rows[0]["seller_name"].ToString();
                    string dia_chi = tblTemplate.Rows[0]["seller_address"].ToString();
                    string dien_thoai = tblTemplate.Rows[0]["seller_tel"].ToString();
                    string ms_thue = tblTemplate.Rows[0]["seller_taxcode"].ToString();
                    string fax = tblTemplate.Rows[0]["seller_fax"].ToString();
                    string email = tblTemplate.Rows[0]["seller_email"].ToString();

                    int sodonginmau = tblTemplate.Rows[0]["row_print_template"].ToString().Length > 0 ? Convert.ToInt32(tblTemplate.Rows[0]["row_print_template"].ToString()) : 0;

                    if (report.Length == 0)
                    {
                        string fileName = folder + "INHD_TEMP_" + ma_loai + "_" + hinhthuc + ".repx";

                        if (System.IO.File.Exists(fileName))
                        {
                            report = System.IO.File.ReadAllText(fileName, System.Text.Encoding.UTF8);
                        }
                    }

                    DataSet dsReport = new DataSet();

                    if (hinhthuc == "E")
                    {
                        string dataSourceSchema = ReportUtil.LoadReportFromString(report).DataSourceSchema;

                        DataTable tblXmlData = await _invoiceService.GetXmlDataTemplate();
                        tblXmlData.TableName = "TblXmlData";

                        using (XmlReader reader = XmlReader.Create(new StringReader(dataSourceSchema)))
                        {
                            dsReport.ReadXmlSchema(reader);
                            reader.Close();
                        }

                        if (dsReport.Tables.Contains("InvoiceInformation"))
                        {
                            DataTable tblThongTinHoaDon = dsReport.Tables["InvoiceInformation"];

                            DataRow row = tblThongTinHoaDon.NewRow();

                            row["template_code"] = mau_so;
                            row["invoice_series"] = ky_hieu;
                            row["invoice_number"] = "0000000";
                            row["invoice_name"] = ten_loai;
                            row["seller_name"] = ten_dvcs;
                            row["seller_address"] = dia_chi;
                            row["seller_tel"] = dien_thoai;
                            row["seller_taxcode"] = ms_thue;
                            row["seller_fax"] = fax;
                            row["seller_email"] = email;

                            tblThongTinHoaDon.Rows.Add(row);
                        }

                        if (dsReport.Tables.Contains("CertifiedData"))
                        {
                            DataTable tblCertifiedData = dsReport.Tables["CertifiedData"];

                            DataRow row = tblCertifiedData.NewRow();

                            row["qrCodeData"] = id.ToString() + ";"
                                               + ms_thue + ";"
                                               + mau_so + ";"
                                               + ky_hieu + ";"
                                               + "0000000" + ";"
                                               + String.Format("{0:yyyy-MM-dd}", new DateTime(1900, 1, 1));

                            tblCertifiedData.Rows.Add(row);
                        }

                        if (dsReport.Tables.Contains("InvoiceDetail") && sodonginmau > 0)
                        {
                            DataTable tblChiTiet = dsReport.Tables["InvoiceDetail"];

                            for (int i = 1; i <= sodonginmau; i++)
                            {
                                DataRow row = tblChiTiet.NewRow();
                                tblChiTiet.Rows.Add(row);
                            }
                        }
                    }
                    else
                    {
                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("id", id);

                        DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_inct_invoice_template(@id)", CommandType.Text, parameters);
                        tblData.TableName = "Table";
                        dsReport.Tables.Add(tblData);
                    }

                    MemoryStream ms = new MemoryStream();
                    dsReport.WriteXml(ms, XmlWriteMode.WriteSchema);
                    ms.Position = 0;
                    StreamReader stream = new StreamReader(ms, System.Text.Encoding.UTF8);
                    string xml = stream.ReadToEnd();

                    JObject obj = new JObject();
                    obj.Add("ok", true);
                    obj.Add("xml", xml);

                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(obj.ToString());

                    result.Content = new ByteArrayContent(bytes);
                    result.Content.Headers.ContentLength = bytes.Length;

                }
            }
            catch (Exception ex)
            {
                JObject obj = new JObject();
                obj.Add("error", ex.Message);

                result.Content = new StringContent(obj.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/SaveTemplate")]
        public async Task<JObject> SaveTemplate(JObject model)
        {
            string id = model["id"].ToString();
            string report = model["report"].ToString();

            JObject result = await this._invoiceService.SaveInvoiceTemlate(id, report);

            return result;
        }

        [HttpPost]
        [Route("Invoice/CopyTemplate")]
        public async Task<JObject> CopyTemplate(JObject model)
        {
            JObject result = await this._invoiceService.CopyInvoiceTemlate(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/RegisterInvoiceProcess")]
        public async Task<JObject> RegisterInvoiceProcess(JObject model)
        {
            JObject result = await this._invoiceService.RegisterInvoiceProcess(model, this.UserName);

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetInvoiceTemplate")]
        public async Task<JObject> GetInvoiceTemplate(Guid id)
        {
            DataTable tblInvoiceTemplate = await this._invoiceService.GetInvoiceTemplate(id);

            if (tblInvoiceTemplate.Rows.Count == 0)
            {
                JObject obj = new JObject();
                obj.Add("error", "Không tìm thấy mẫu hóa đơn");

                return obj;
            }
            else
            {
                JArray array = JArray.FromObject(tblInvoiceTemplate);

                return (JObject)array[0];
            }


        }

        [HttpGet]
        [Route("Invoice/GetXmlTemplate")]
        public async Task<JObject> GetXmlTemplate(Guid id)
        {
            return await _invoiceService.GetXmlTemplate(id);
        }

        [HttpGet]
        [Route("Invoice/SignConfirm")]
        [AllowAnonymous]
        public async Task<JObject> SignConfirm(Guid id)
        {
            return await _invoiceService.GetXmlData(id);
        }

        [HttpPost]
        [Route("Invoice/SignConfirm")]
        [AllowAnonymous]
        public async Task<JObject> SignConfirm(JObject model)
        {
            return await _invoiceService.SignConfirm(model);
        }

        [HttpPost]
        [Route("Invoice/SendToCustomer")]
        public async Task<JObject> SendToCustomer(JObject model)
        {
            string invoice_id = model["invoice_id"].ToString();
            if (CommonManager.CheckForSqlInjection(invoice_id))
            {
                return new JObject { { "error", $"Giá trị {invoice_id} không hợp lệ !" } };
            }
            
            return await _invoiceService.SendToCustomer(model);
        }

        [HttpPost]
        [Route("Invoice/DeleteInv")]
        [AllowAnonymous]
        public async Task<JObject> DeleteInv(JObject model)
        {
            return await _invoiceService.DeleteInv(model);
        }
        
        [HttpPost]
        [Route("Invoice/DeleteInvMulti")]
        [AllowAnonymous]
        public async Task<List<JObject>> DeleteInvMulti(List<JObject> models)
        {
            return await _invoiceService.DeleteInvMulti(models);
        }

        [HttpPost]
        [Route("Invoice/ChangeSecurityNumber")]
        public async Task<JObject> ChangeSecurityNumber(JObject model)
        {
            return await _invoiceService.ChangeSecurityNumber(model);
        }

        [HttpPost]
        [Route("Invoice/DeleteReceipt")]
        public async Task<JObject> DeleteReceipt(JObject model)
        {
            return await _invoiceService.DeleteReceipt(model);
        }

        [HttpPost]
        [Route("Invoice/DeleteReceiptMulti")]
        public async Task<JObject> DeleteReceiptMulti(JObject model)
        {
            return await _invoiceService.DeleteReceiptMulti(model);
        }

        [HttpPost]
        [Route("Invoice/UpdateCustomerInfo")]
        [AllowAnonymous]
        public async Task<JObject> UpdateCustomerInfo(JObject model)
        {
            return await _invoiceService.UpdateCustomerInfo(model);
        }

        [HttpPost]
        [Route("Invoice/SendEmailByInvoiceId")]
        public async Task<JObject> SendEmailByInvoiceId(JObject model)
        {
            return await _invoiceService.SendEmailByInvoiceId(model);
        }

        [HttpPost]
        [Route("Invoice/SignXmlTemplate")]
        public async Task<JObject> GetXmlTemplate(JObject parameters)
        {
            string id = parameters["id"].ToString();
            string xml = parameters["xml"].ToString();

            return await _invoiceService.SignXmlTemplate(id, xml);
        }

        [HttpGet]
        [Route("Invoice/PrintInvoiceTemplate")]
        public async Task<HttpResponseMessage> PrintInvoiceTemplate(string id)
        {

            HttpResponseMessage result = null;


            try
            {
                byte[] bytes = await _invoiceService.PrintInvoiceTemplate(id, "PDF");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/PrintInvoiceTemplateChuyenDoi")]
        public async Task<HttpResponseMessage> PrintInvoiceTemplateChuyenDoi(string id)
        {

            HttpResponseMessage result = null;


            try
            {
                byte[] bytes = await _invoiceService.PrintInvoiceTemplate(id, "PDF",true);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        
        [HttpGet]
        [Route("Invoice/PrintInvoiceTemplate")]
        public async Task<HttpResponseMessage> PrintInvoiceTemplate(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                byte[] bytes = await _invoiceService.PrintInvoiceTemplate(id, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/PrintInvoice")]
        public async Task<HttpResponseMessage> PrintInvoice(string id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService.PrintInvoiceFromId(id, folder, type, "");

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/PrintReceiptFromInvoiceId")]
        public async Task<HttpResponseMessage> PrintReceiptFromInvoiceId(long id, string type)
        {
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService.PrintReceiptFromInvoiceId(id, folder, type, false);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "Receipt.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/PrintTempInvoice")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PrintTempInvoice(long id)
        {
            HttpResponseMessage result = null;

            try
            {
                string type = "PDF";

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _invoiceService.PreviewTempInvoice(id, folder, type, false);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/PrintTempInvoices")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PrintTempInvoices(List<long> id)
        {
            HttpResponseMessage result = null;

            try
            {
                string type = "PDF";

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                var listTmp = new List<byte[]>();
                foreach (var item in id)
                {
                    byte[] bytes = await _invoiceService.PreviewTempInvoice(item, folder, type, false);
                    listTmp.Add(bytes);
                }
                var byteOut = MergePdf(listTmp);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(byteOut);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = byteOut.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/PrintListInvoice")]
        public async Task<HttpResponseMessage> PrintListInvoice(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                bool inchuyendoi = false;

                if (model["inchuyendoi"] != null)
                {
                    inchuyendoi = Convert.ToBoolean(model["inchuyendoi"]);
                }

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";
                string type = model["type"].ToString();
                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService.PrintListInvoice(model, folder, inchuyendoi, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentLength = bytes.Length;

                if (type == "Excel")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    result.Content.Headers.ContentDisposition.FileName = "data.xlsx";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else if (type == "Rtf")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    result.Content.Headers.ContentDisposition.FileName = "data.rtf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                else
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "report.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }

            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/PrintListReceipt")]
        public async Task<HttpResponseMessage> PrintListReceipt(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                bool inchuyendoi = false;

                if (model["inchuyendoi"] != null)
                {
                    inchuyendoi = Convert.ToBoolean(model["inchuyendoi"]);
                }

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";
                string type = model["type"].ToString();
                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _invoiceService.PrintListReceipt(model, folder, inchuyendoi, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentLength = bytes.Length;

                if (type == "Excel")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    result.Content.Headers.ContentDisposition.FileName = "data.xlsx";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else if (type == "Rtf")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    result.Content.Headers.ContentDisposition.FileName = "data.rtf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                else
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "report.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }

            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/PrintAdjustmentReport")]
        public async Task<HttpResponseMessage> PrintAdjustmentReport(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                byte[] bytes = await _invoiceService.PrintAdjustmentReport(model);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentLength = bytes.Length;


                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "adjustment_report.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");


            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/ImageTemplate")]
        public async Task<HttpResponseMessage> ImageTemplate(string id)
        {

            HttpResponseMessage result = null;

            try
            {
                byte[] bytes = await _invoiceService.PrintInvoiceTemplate(id, "Image");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.png";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetTB01ACXml")]
        public HttpResponseMessage GetTB01ACXml(string id)
        {

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            string xml = _invoiceService.GetTB01ACXml(id);
            byte[] data = System.Text.Encoding.UTF8.GetBytes(xml);

            result.Content = new ByteArrayContent(data);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
            result.Content.Headers.ContentDisposition.FileName = "TB01AC.xml";
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
            result.Content.Headers.ContentLength = data.Length;

            return result;

        }

        [HttpPost]
        [Route("Invoice/SaveTB01ACXml")]
        public JObject SaveTB01ACXml(JObject model)
        {
            return _invoiceService.SaveTB01ACXml(model);
        }

        [HttpGet]
        [Route("Invoice/ExportZipFile")]
        public async Task<HttpResponseMessage> ExportZipFile(string id)
        {

            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                string fileName = await _invoiceService.GetInvoiceFileName(id);
                byte[] bytes = await _invoiceService.ExportZipFile(id, folder, fileName);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                result.Content.Headers.ContentDisposition.FileName = fileName + ".zip";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("Invoice/Search")]
        public JObject Search(JObject model)
        {
            JObject result = _invoiceService.Search(this.Ma_dvcs, model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/SendInvoiceByEmail")]
        public async Task<JObject> SendInvoiceByEmail(JObject model)
        {
            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);
            JObject result = await _invoiceService.SendInvoiceByEmail(folder, model, this.Ma_dvcs, model["type"].ToString());

            return result;
        }

        [HttpPost]
        [Route("Invoice/AutoSendInvoiceByEmail")]
        public async Task<JObject> AutoSendInvoiceByEmail(JObject model)
        {
            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);
            JObject result = await _invoiceService.AutoSendInvoiceByEmail(folder, model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/SendListInvoices")]
        public async Task<JObject> SendListInvoices(JObject model)
        {
            string originalString = this.ActionContext.Request.RequestUri.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

            var folder = System.Web.HttpContext.Current.Server.MapPath(path);
            JObject result = await _invoiceService.SendListInvoices(folder, model);

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetEmailTemplate")]
        public async Task<JObject> GetEmailTemplate(string type, string id)
        {
            Uri uri = this.ActionContext.Request.RequestUri;
            string originalString = uri.Scheme + "://" + uri.Authority;

            return await _invoiceService.GetEmailTemplate(type, id, this.Ma_dvcs, originalString);
        }

        [HttpGet]
        [Route("Invoice/GetListForSigning")]
        public async Task<JArray> GetListForSigning()
        {
            return await _invoiceService.GetListForSigning(this.Ma_dvcs);
        }

        [HttpPost]
        [Route("Invoice/UploadInvTemplate")]
        public async Task<HttpResponseMessage> UploadInvTemplate()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            byte[] bytes = null;
            string dmmauhoadon_id = null;
            string fileName = null;

            try
            {

                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }
                        else
                        {
                            dmmauhoadon_id = await stream.ReadAsStringAsync();
                        }
                    }
                }

                var json = new JObject();

                if (!fileName.EndsWith("repx"))
                {
                    json.Add("error", "File tải lên không đúng");
                    json.Add("status", "server");

                    return Request.CreateResponse(HttpStatusCode.OK, json); ;
                }

                string str = System.Text.Encoding.UTF8.GetString(bytes);

                if (CommonManager.CheckForRemoteCommand(str) == true)
                {
                    json.Add("error", "File chứa dữ liệu gây hại!");
                    return Request.CreateResponse(HttpStatusCode.OK, json);

                }

                string result = await _invoiceService.UploadInvTemplate(this.Ma_dvcs, this.UserName, dmmauhoadon_id, bytes);



                if (result.Length > 0)
                {
                    json.Add("error", result);
                    json.Add("status", "server");
                }
                else
                {
                    json.Add("ok", true);
                    json.Add("status", "server");
                }

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("Invoice/GetDecimalPlace")]
        public async Task<JArray> GetDecimalPlace(string ma_nt)
        {
            JArray jarr = await this._invoiceService.GetDecimalPlace(ma_nt);
            return jarr;
        }

        [HttpPost]
        [Route("Invoice/GetListInvoiceSeries")]
        public async Task<JArray> GetListInvoiceSeries(JObject model)
        {
            JArray jarr = await this._invoiceService.GetListInvoiceSeries(model);
            return jarr;
        }

        [HttpGet]
        [Route("Invoice/Amount_ToWord")]
        public async Task<JObject> Amount_ToWord(string number, string type_amount)
        {
            JObject jarr = await this._invoiceService.Amount_ToWord(number, type_amount);
            return jarr;
        }

        [HttpPost]
        [Route("Invoice/CreateUser_tracuu")]
        public async Task<JObject> CreateUser_tracuu(JObject model)
        {
            var result = await _invoiceService.CreateUser_tracuu(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/GetUserTracuu")]
        public async Task<JObject> GetUserTracuu(JObject model)
        {
            var result = await _invoiceService.GetUserTracuu(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/ResetPassUserTracuu")]
        public async Task<JObject> ResetPassUserTracuu(JObject model)
        {
            var result = await _invoiceService.ResetPassUserTracuu(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/DeleteUserTracuu")]
        public async Task<JObject> DeleteUserTracuu(JObject model)
        {
            var result = await _invoiceService.DeleteUserTracuu(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/SignInvoiceCertFile")]
        public async Task<JObject> SignInvoiceCertFile(JObject model)
        {
            var result = await _invoiceService.SignInvoiceCertFile(model);
            return result;
        }

        [HttpPost]
        [Route("Invoice/CheckSignByHSM")]
        public async Task<JObject> CheckSignByHSM()
        {
            var result = new JObject
            {
                { "data", await _invoiceService.CheckSignByHSM() }
            };
            return result;
        }

        [HttpPost]
        [Route("Invoice/SignXMLEasyCA")]
        public async Task<JObject> SignXMLEasyCA(JObject model)
        {
            return await _invoiceService.SignXmlEasyCA(model);
        }
        [HttpPost]
        [Route("Invoice/SignListInvoiceCertFile")]
        public async Task<JObject> SignListInvoiceCertFile(JObject model)
        {
            var result = await _invoiceService.SignListInvoiceCertFile(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/DeleteByCertFile")]
        public async Task<JObject> DeleteByCertFile(JObject model)
        {
            var result = await _invoiceService.DeleteByCertFile(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/Create")]
        public async Task<JObject> Create(JObject model)
        {
            var result = await _invoiceService.Create(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/GetXmlSignInvoices")]
        public async Task<string> GetXmlSignInvoices(JObject model)
        {
            string inv_invoicecode_id = model["inv_InvoiceCode_id"].ToString();
            string tu_ngay = model["tu_ngay"].ToString();
            string den_ngay = model["den_ngay"].ToString();
            string data = model["data"].ToString();

            var result = await _invoiceService.GetXmlSignInvoices(inv_invoicecode_id, Convert.ToDateTime(tu_ngay), Convert.ToDateTime(den_ngay), data);

            return result;
        }

        [HttpPost]
        [Route("Invoice/SaveXmlSign")]
        public async Task<JObject> SaveXmlSign(JObject model)
        {
            var result = await _invoiceService.SaveXmlSign(model);

            return result;
        }

        [HttpGet]
        [Route("Invoice/DowloadRepx")]
        public HttpResponseMessage DowloadRepx(string id)
        {
            HttpResponseMessage result = null;

            try
            {
                byte[] bytes = _invoiceService.DowloadRepx(id);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.repx";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpGet]
        [Route("Invoice/GetListCertificatesFile")]
        public async Task<JArray> GetListCertificatesFile()
        {
            JArray result = await this._invoiceService.GetListCertificatesFile(this.UserName, this.Ma_dvcs);

            return result;
        }

        [HttpPost]
        [Route("Invoice/ConfirmTempInvoice")]
        [AllowAnonymous]
        public async Task<JObject> ConfirmTempInvoice(JObject model)
        {
            var result = await _invoiceService.ConfirmTempInvoice(model);

            return result;
        }

        [HttpPost]
        [Route("Invoice/InfoEmailToCustomer")]
        public async Task<JObject> InfoEmailToCustomer(JObject model)
        {
            string invoice_id_ck = model["invoice_id"].ToString();
            if (CommonManager.CheckForSqlInjection(invoice_id_ck))
            {
                return new JObject { { "error", $"Giá trị {invoice_id_ck} không hợp lệ !" } };
            }
            long invoice_id = Convert.ToInt64(invoice_id_ck);

            JObject result = await _invoiceService.InfoEmailToCustomer(invoice_id);

            return result;
        }
        
        private static byte[] MergePdf(List<byte[]> pdfs)
        {
            var lstDocuments = new List<PdfSharp.Pdf.PdfDocument>();
            foreach (var pdf in pdfs)
            {
                lstDocuments.Add(PdfReader.Open(new MemoryStream(pdf), PdfDocumentOpenMode.Import));
            }

            using (PdfSharp.Pdf.PdfDocument outPdf = new PdfSharp.Pdf.PdfDocument())
            {
                for (int i = 1; i <= lstDocuments.Count; i++)
                {
                    foreach (PdfSharp.Pdf.PdfPage page in lstDocuments[i - 1].Pages)
                    {
                        outPdf.AddPage(page);
                    }
                }

                MemoryStream stream = new MemoryStream();
                outPdf.Save(stream, false);
                byte[] bytes = stream.ToArray();

                return bytes;
            }
        }
    }
}