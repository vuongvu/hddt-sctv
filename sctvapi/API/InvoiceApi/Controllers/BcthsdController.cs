﻿using InvoiceApi.Data;
using InvoiceApi.Services;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace InvoiceApi.Controllers
{
    public class BcthsdController : BaseApiController
    {
        private IBcthsdService _bcthsdService;
        private INopDbContext _nopDbContext;
        private readonly IMinvoiceDbContext _minvoiceDbContext;
        public BcthsdController(INopDbContext nopDbContext, IBcthsdService bcthsdService, IMinvoiceDbContext minvoiceDbContext)
        {
            this._nopDbContext = nopDbContext;
            this._bcthsdService = bcthsdService;
            this._minvoiceDbContext = minvoiceDbContext;
        }

        [HttpPost]
        [Route("Bcthsd/bangkebanra")]
        public async Task<JObject> bangkebanra(JObject model)
        {
            var lst = await _bcthsdService.bangkebanra(model);
            return lst;
        }
        [HttpPost]
        [Route("Bcthsd/PrintPDFBangKeBanRa")]
        public async Task<HttpResponseMessage> PrintPDFBangKeBanRa(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                //string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                //var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                //string file = folder + "\\bc_thsdhd.repx";
                //E:\Phanmem02\M - INVOICE\INVOICE\API\InvoiceApi\Content\report
                //   string file = "E:\\Phanmem02\\M-INVOICE\\INVOICE\\API\\InvoiceApi\\Content\report\\bc_thsdhd.repx";
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/BangKeBanRa_WEB.repx" : "~/Content/report/BangKeBanRa_WEB.repx";
                string type = model["type"].ToString();
                path = System.Web.HttpContext.Current.Server.MapPath(path);
                string sql = "#SCHEMA_NAME#.qr_bc_bkbr_tr";
                Dictionary<string, object> param = new Dictionary<string, object>();
             
                param.Add("p_branch_code", this.Ma_dvcs);
                param.Add("p_store_code", "");
                param.Add("p_from_date", (DateTime)model["tu_ngay"]);
                param.Add("p_to_date",(DateTime)model["den_ngay"]);
                param.Add("p_invoice_series", model["ky_hieu"].ToString());
                param.Add("p_lst_branch_code", model["lst_branch_code"].ToString());
                
                //string sql = "EXEC sproc_inct_thsdhd '" + ma_dvcs + "','" + kieu + "','" + nam + "','" + thang + "','" + loai + "'";
                //DataSet ds = DataUtil.GetDataSet(sql, CommandType.StoredProcedure, param);
                DataSet ds = await _minvoiceDbContext.GetDataSetAsyncPrintf(sql, CommandType.StoredProcedure, param);
                byte[] bytes = ReportUtil.PrintReport(ds, path, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "BangKeBanRa." + type;
                if (type == "PDF")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        [HttpGet]
        [Route("Bcthsd/GetInfoDonvi")]
        public async Task<JObject> GetInfoDonvi()
        {
            return await _bcthsdService.GetInfoDonvi();
        }

        [HttpGet]
        [Route("Bcthsd/GetTenFile")]
        public async Task<JObject> GetTenFile()
        {
            return await _bcthsdService.GetTenFile();
        }

        [HttpPost]
        [Route("Bcthsd/Check_xulyHD")]
        public async Task<JObject> Check_xulyHD(JObject model)
        {
            var lst = await _bcthsdService.Check_xulyHD(this.Ma_dvcs,model);
            return lst;
        }

        [HttpPost]
        [Route("Bcthsd/BcTinhhinhsudung")]
        public async Task<JArray> BcTinhhinhsudung(JObject model)
        {
            var lst = await _bcthsdService.BcTinhhinhsudung(this.Ma_dvcs,model);
            return lst;
        }

        [HttpPost]
        [Route("Bcthsd/bcthsd_INSERT")]
        public async Task<JObject> bcthsd_INSERT(JObject model)
        {
            var lst = await _bcthsdService.bcthsd_INSERT(model);
            return lst;
        }

        [HttpPost]
        [Route("Bcthsd/ExportXMLString")]
        public JObject ExportXMLString(JObject model)
        {
            return _bcthsdService.ExportXMLString(this.Ma_dvcs,model);
        }

        [HttpGet]
        [Route("Bcthsd/ExportFileXML")]
        public async Task<HttpResponseMessage> ExportFileXML(string ma_dvcs, string tokhai_quy, string tokhai_thang, string nam, string thang, string tu_ngay, string den_ngay)
        {

            HttpResponseMessage result = null;

            try
            {
                
                string xml = await _bcthsdService.GetStringXML(ma_dvcs, tokhai_quy, tokhai_thang, nam, thang, tu_ngay, den_ngay);

                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml);


                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "invoice.xml";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        [HttpGet]
        [Route("Bcthsd/PrintPDF")]
        public async Task<HttpResponseMessage> PrintPDF(string ma_dvcs, string bc_soluong, string nam, string thang, string tokhai_quy, string tokhai_thang)
        {
            string kieu = "T";
            int loai = 1;
            if (tokhai_quy == "1")
            {
                kieu = "Q";
            }
            if (tokhai_thang == "1")
            {
                kieu = "T";
            }
            if (bc_soluong == "0")
            {
                loai = 1;
            }
            if (bc_soluong == "1")
            { loai = 2; }
            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/bc_thsdhd.repx" : "~/Content/report/bc_thsdhd.repx";

                path = System.Web.HttpContext.Current.Server.MapPath(path);
                string sql = "#SCHEMA_NAME#.qr_inct_thsdhd";
                Dictionary<string, object> param = new Dictionary<string, object>();

                param.Add("p_branch_code", this.Ma_dvcs);
                param.Add("p_store_code", "");
                param.Add("p_kieu", kieu);
                param.Add("p_nam", Convert.ToInt16(nam));
                param.Add("p_thang", Convert.ToInt16(thang));
                param.Add("p_loai", loai);

                DataSet ds = await _minvoiceDbContext.GetDataSetAsyncPrintf(sql, CommandType.StoredProcedure, param);
                //string sql = "EXEC sproc_inct_thsdhd '" + ma_dvcs + "','" + kieu + "','" + nam + "','" + thang + "','" + loai + "'";
                //DataSet ds = DataUtil.GetDataSet(sql, CommandType.Text, null);
                byte[] bytes = ReportUtil.PrintReport(ds, path, "PDF");

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "invoice.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
    }
}