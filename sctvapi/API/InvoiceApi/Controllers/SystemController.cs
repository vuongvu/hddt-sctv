﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using InvoiceApi.Services;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using InvoiceApi.Util;

namespace InvoiceApi.Controllers
{
    public class SystemController : BaseApiController
    {
        private ISystemService _systemService;
        private IRabbitMQService _rabbitMQService;

        public SystemController(ISystemService systemService, IRabbitMQService rabbitMQService)
        {
            this._systemService = systemService;
            this._rabbitMQService = rabbitMQService;
        }

        [HttpGet]
        [Route("System/GetMST")]
        public string GetMST()
        {
            return _systemService.GetMST();
        }

        [HttpGet]
        [Route("System/GetMenuBar")]
        public async Task<JArray> GetMenuBar()
        {
            var lst = await _systemService.GetMenuBarByUser(this.UserName);

            return lst;
        }

        [HttpGet]
        [Route("System/GetMenuBar2")]
        public async Task<JArray> GetMenuBar2()
        {
            var lst = await _systemService.GetMenuBarByUser2(this.UserName);

            return lst;
        }


        [HttpGet]
        [Route("System/GetTypeSQL")]
        public async Task<JArray> GetTypeSQL()
        {
            var lst = await _systemService.GetTypeSQL();

            return lst;
        }

        [HttpGet]
        [Route("System/GetConfigWinByNo")]
        public async Task<HttpResponseMessage> GetConfigWinByNo(string id)
        {
            string result = await _systemService.GetConfigWinByNo(id);
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(result);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(bytes);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.Content.Headers.ContentLength = bytes.Length;

            return response;
        }

        [HttpGet]
        [Route("System/GetDataByWindowNo")]
        public async Task<JObject> GetDataByWindowNo(string window_id, int start, int count, string filter, string infoparam, string tlbparam)
        {
            var obj = await _systemService.GetDataByWindowNo(this.Ma_dvcs, this.UserName, window_id, start, count, filter, infoparam, tlbparam);
            return obj;
        }

        [HttpPost]
        [Route("System/GetDataByWindowNo")]
        public async Task<JObject> GetDataByWindowNo(JObject model)
        {
            string ma_dvcs = this.Ma_dvcs;
            string userName = this.UserName;
            string window_id = model["window_id"].ToString();
            int start = Convert.ToInt32(model["start"].ToString());
            int count = Convert.ToInt32(model["count"].ToString());

            JArray filter = (JArray)model["filter"];
            JObject infoparam = model["infoparam"] as JObject;

            JArray tlbparam = (JArray)model["tlbparam"];

            var obj = await _systemService.GetDataByWindowNo(this.Ma_dvcs, this.UserName, window_id, start, count, filter, infoparam, tlbparam);

            return obj;
        }

        [HttpGet]
        [Route("System/GetDataById")]
        public async Task<JObject> GetDataById(string window_id, string id)
        {
            var obj = await _systemService.GetDataById(window_id, id);

            return obj;
        }

        //[HttpGet]
        //[Route("System/ExecuteQuery")]
        //public async Task<JArray> ExecuteQuery(string sql)
        //{
        //    var lst = await _systemService.ExecuteQueryAsync(sql);

        //    return lst;
        //}

        //[HttpPost]
        //[Route("System/Query")]
        //public async Task<JArray> Query(JObject model)
        //{
        //    string sql = model["sql"].ToString();
        //    var lst = await _systemService.ExecuteQueryAsync(sql);

        //    return lst;
        //}

        [HttpGet]
        [Route("System/GetAllDmDvcs")]
        [AllowAnonymous]
        public async Task<JArray> GetAllDmDvcs()
        {
            //string sql = "SELECT *,code as id,name as value FROM #SCHEMA_NAME#.wb_branch ORDER BY code";

            string sql = "SELECT *,a.code as id,a.name as value, b.name as cqt FROM #SCHEMA_NAME#.wb_branch a LEFT JOIN #SCHEMA_NAME#.sl_tax_authorities_city b on a.provincial_tax_authorities = b.tax_authorities_manage ORDER BY a.code";

            var lst = await _systemService.ExecuteQueryAsync(sql);
            //_rabbitMQService.PublishQueue("create_invoice_queue", "abc");

            return lst;
        }

        [HttpGet]
        [Route("System/GetDmDvcsByUser")]
        [AllowAnonymous]
        public async Task<JArray> GetDmDvcsByUser()
        {
            //string sql = "SELECT *,code as id,name as value FROM #SCHEMA_NAME#.wb_branch ORDER BY code";

            var lst = await _systemService.GetDmDvcsByUser();
            return lst;

        }

        [HttpGet]
        [Route("System/GetInfoCompany")]
        public async Task<JObject> GetInfoCompany()
        {
            //string branch_code = this.Ma_dvcs;
            string branch_code = "VP";
            JArray lst = await _systemService.GetInfoCompany(branch_code);

            return (JObject)lst[0];
        }

        [HttpPost]
        [Route("System/SaveInfoCompany")]
        public async Task<JObject> SaveInfoCompany(JObject model)
        {
            JObject obj = await _systemService.SaveInfoCompany(model);
            return obj;
        }

        [HttpGet]
        [Route("System/GetDataReferencesByRefId")]
        public async Task<JArray> GetDataReferencesByRefId(string refId)
        {
            var lst = await _systemService.GetDataReferencesByRefId(refId, this.Ma_dvcs);
            return lst;
        }

        [HttpGet]
        [Route("System/GetDataReferencesByRefIdSCTV")]
        public async Task<JObject> GetDataReferencesByRefIdSCTV(string refId)
        {
            var lst = await _systemService.GetDataReferencesByRefId(refId, this.Ma_dvcs);
            JObject obj = FunctionUtil.AddInfoToResult(lst);
            return obj;
        }
        [HttpGet]
        [Route("System/GetDataReferencesByRefId")]
        public async Task<JArray> GetDataReferencesByRefId(string refId, string filtervalue)
        {
            var lst = await _systemService.GetDataReferencesByRefId(refId, filtervalue, this.Ma_dvcs);
            return lst;
        }

        [HttpGet]
        [Route("System/GetDataByReferencesId")]
        public async Task<JArray> GetDataByReferencesId(string id)
        {
            var lst = await _systemService.GetDataByReferencesId(id, null, this.Ma_dvcs);
            return lst;
        }

        [HttpGet]
        [Route("System/GetDataByReferencesId")]
        public async Task<JArray> GetDataByReferencesId(string id, string filtervalue)
        {
            var lst = await _systemService.GetDataByReferencesId(id, filtervalue, this.Ma_dvcs);
            return lst;
        }

        [HttpGet]
        [Route("System/GetRecordByReferencesId")]
        public async Task<JArray> GetRecordByReferencesId(string id, string value)
        {
            var lst = await _systemService.GetRecordByReferencesId(id, value);
            return lst;
        }

        [HttpGet]
        [Route("System/GetDataDetailsByTabTable")]
        public async Task<JArray> GetDataDetailsByTabTable(string window_id, string id, string tab_table)
        {
            var lst = await _systemService.GetDataDetailsByTabTable(window_id, Guid.Parse(id), tab_table);
            return lst;
        }

        [HttpGet]
        [Route("System/GetInvoiceDetail")]
        public async Task<JArray> GetInvoiceDetail(string id)
        {
            var lst = await _systemService.GetInvoiceDetail(id);
            return lst;
        }

        [HttpPost]
        [Route("System/ExecuteCommand")]
        public async Task<JArray> ExecuteCommand(JObject model)
        {
            string command = model["command"].ToString();
            JObject parameter = (JObject)model["parameter"];

            var lst = await _systemService.ExecuteCommand(parameter, command);

            return lst;
        }

        [HttpPost]
        [Route("System/PhanQuyen")]
        public async Task<JObject> PhanQuyen(JObject model)
        {
            var obj = await _systemService.PhanQuyen(model);

            return obj;
        }

        [HttpPost]
        [Route("System/CheckBrandValidation")]
        public async Task<bool> CheckBranchValidation(string value)
        {
            var obj = await _systemService.CheckBranchValidation(value);
            return obj;
        }

        [HttpPost]
        [Route("System/RegisterLicenceBranch")]
        public async Task<JObject> RegisterLicenceBranch(JObject model)
        {
            var obj = await _systemService.RegisterLicenceBranch(model);
            return obj;
        }

        [HttpPost]
        [Route("System/Save")]
        public async Task<JObject> Save(JObject model)
        {
            var lst = await _systemService.Save(model, this.UserName);

            return lst;
        }
        [HttpPost]
        [Route("System/SavePretreatment")]
        public async Task<JObject> SavePretreatment(JObject model)
        {
            var lst = await _systemService.SavePretreatment(model, this.UserName);

            return lst;
        }



        [HttpPost]
        [Route("System/SaveAndCreateNumber")]
        public async Task<JObject> SaveAndCreateNumber(JObject model)
        {
            var lst = await _systemService.SaveAndCreateNumber(model, this.UserName);

            return lst;
        }
        [HttpPost]
        [Route("System/SaveAndCreateNumberPretreatment")]
        public async Task<JObject> SaveAndCreateNumberPretreatment(JObject model)
        {
            var lst = await _systemService.SaveAndCreateNumberPretreatment(model, this.UserName);

            return lst;
        }
        [HttpPost]
        [Route("System/SaveSignXMLEasyCA")]
        public async Task<JObject> SaveSignXMLEasyCA(JObject model)
        {
            var lst = await _systemService.SaveSignXMLEasyCA(model, UserName);

            return lst;
        }
        [HttpPost]
        [Route("System/SaveSignInvoiceCertFile")]
        public async Task<JObject> SaveSignInvoiceCertFile(JObject model)
        {
            var lst = await _systemService.SaveSignInvoiceCertFile(model, UserName);
            return lst;
        }

        [HttpPost]
        [Route("System/DeleteMultiChoice")]
        public async Task<JObject> DeleteMultiChoice(JObject model)
        {
            var lst = await _systemService.DeleteMultiChoice(model, this.UserName);

            return lst;
        }

        [HttpPost]
        [Route("System/InsertFieldsByTabId")]
        public async Task<JObject> InsertFieldsByTabId(JObject model)
        {
            var obj = await _systemService.InsertFieldsByTabId(Guid.Parse(model["id"].ToString()));

            return obj;
        }

        [HttpPost]
        [Route("System/AutoCreateTable")]
        public JObject AutoCreateTable(string id)
        {
            var obj = _systemService.AutoCreateTable(Guid.Parse(id));

            return obj;
        }

        [HttpPost]
        [Route("System/AutoCreateCmd")]
        public async Task<JObject> AutoCreateCmd(JObject model)
        {
            var obj = await _systemService.AutoCreateCmd(Guid.Parse(model["id"].ToString()));

            return obj;
        }

        [HttpPost]
        [Route("System/InChungTu")]
        public async Task<HttpResponseMessage> InChungTu(JObject model)
        {

            HttpResponseMessage result = null;

            try
            {

                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";
                string type = model["type"].ToString();
                var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                byte[] bytes = await _systemService.InChungTu(model, folder);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentLength = bytes.Length;

                if (type == "Excel")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    result.Content.Headers.ContentDisposition.FileName = "data.xlsx";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else if (type == "Rtf")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    result.Content.Headers.ContentDisposition.FileName = "data.rtf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                else
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "report.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [Route("System/CreateReport")]
        public async Task<HttpResponseMessage> CreateReport(JObject obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                string type = (string)obj["type"];

                byte[] bytes = await _systemService.CreateReport(obj);

                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.Length;

                if (type == "json")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                }
                else if (type == "PDF")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    response.Content.Headers.ContentDisposition.FileName = "report.pdf";
                }
                else if (type == "xlsx")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    response.Content.Headers.ContentDisposition.FileName = "report.xlsx";
                }
                else if (type == "Rtf")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                    response.Content.Headers.ContentDisposition.FileName = "report.rtf";
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                response.Content.Headers.ContentLength = ex.Message.Length;
            }

            return response;
        }

        [HttpGet]
        [Route("System/DownloadFileExcelTemplate")]
        public async Task<HttpResponseMessage> DownloadFileExcelTemplate(string id)
        {
            HttpResponseMessage response = null;

            try
            {
                byte[] bytes = await _systemService.DownloadFileExcelTemplate(id);

                response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentLength = bytes.Length;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                response.Content.Headers.ContentDisposition.FileName = "template.xlsx";

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                response.Content.Headers.ContentLength = ex.Message.Length;
            }

            return response;
        }

        [HttpPost]
        [Route("System/UploadExcel")]
        public async Task<HttpResponseMessage> UploadExcel()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            byte[] bytes = null;
            string excel_id = null;
            string fileName = null;

            Dictionary<string, object> formData = new Dictionary<string, object>();

            try
            {

                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }
                        else
                        {
                            string dispositionType = stream.Headers.ContentDisposition.DispositionType;

                            if (dispositionType == "form-data")
                            {
                                string name = stream.Headers.ContentDisposition.Name.Replace("\"", "");
                                string value = await stream.ReadAsStringAsync();

                                if (name == "excel_id")
                                {
                                    excel_id = value;
                                }
                                else
                                {
                                    formData.Add(name, value);
                                }

                            }

                        }
                    }
                }

                await _systemService.UploadExcelAsync(this.Ma_dvcs, this.UserName, excel_id, bytes, formData);

                var json = new JObject();
                json.Add("ok", true);
                json.Add("status", "server");

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
            catch (System.Exception e)
            {
                var json = new JObject();
                json.Add("error", e.Message);

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
        }

        [HttpPost]
        [Route("System/UploadExcelTemplate")]
        public async Task<HttpResponseMessage> UploadExcelTemplate()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            byte[] bytes = null;
            string excel_id = null;
            string fileName = null;

            Dictionary<string, object> formData = new Dictionary<string, object>();

            try
            {
                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }
                        else
                        {
                            string dispositionType = stream.Headers.ContentDisposition.DispositionType;

                            if (dispositionType == "form-data")
                            {
                                string name = stream.Headers.ContentDisposition.Name.Replace("\"", "");
                                string value = await stream.ReadAsStringAsync();
                                formData.Add(name, value);
                            }
                        }
                    }
                }

                await _systemService.UploadExcelTemplateAsync(this.Ma_dvcs, this.UserName, bytes, formData);

                var json = new JObject();
                json.Add("ok", true);
                json.Add("status", "server");

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
            catch (System.Exception e)
            {
                var json = new JObject();
                json.Add("error", e.Message);

                return Request.CreateResponse(HttpStatusCode.OK, json);
            }
        }

        [HttpPost]
        [Route("System/UploadExcelListUnit")]
        public async Task<HttpResponseMessage> UploadExcelListCustomer()
        {
            try
            {
                // var filesReadToProvider = await Request.Content.ReadAsByteArrayAsync();
                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();
                byte[] bytes = null;
                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (!stream.IsFormData())
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                        }
                    }
                }

                var result = await _systemService.UploadExcelListUnitAsync(bytes);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                var errorJObject = new JObject();
                errorJObject.Add("error", e.Message);
                return Request.CreateResponse(HttpStatusCode.OK, errorJObject);
            }
        }

        [HttpPost]
        [Route("System/Contract")]
        [AllowAnonymous]
        public async Task<JObject> Contract()
        {
            var obj = await _systemService.SaveContract();
            return obj;
        }

        [HttpPost]
        [Route("System/CreateNewCustomers")]
        public async Task<JObject> CreateNewCustomers(string maSoThue)
        {
            var output = await _systemService.CreateNewCustomers(maSoThue);
            return output;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("System/ServicesIntegratedIntoTheMarketPlace")]
        public async Task<JObject> ServicesIntegratedIntoTheMarketPlace(JObject model)
        {
            var output = await _systemService.ServicesIntegratedIntoTheMarketPlace(model);
            return output;
        }

        [HttpPost]
        [Route("System/CheckInvoiceCancel")]
        public async Task<JObject> CheckInvoiceCancel(List<string> valueList)
        {
            var obj = await _systemService.CheckInvoiceCancel(valueList);
            return obj;
        }

        [HttpGet]
        [Route("System/GetBusinessInfoByTaxCode")]
        public async Task<JObject> GetBusinessInfoByTaxCode(string taxCode)
        {
            var obj = await _systemService.GetBusinessInfoByTaxCode(taxCode);
            if (obj["error"] != null)
            {
                string url = $@"https://hoadondientu.gdt.gov.vn:30000/category/public/dsdkts/{taxCode}/manager";
                var httpClient = new HttpClient();
                HttpResponseMessage responseMessage = await httpClient.GetAsync(url);
                if (responseMessage.StatusCode != HttpStatusCode.OK)
                {
                    return obj;
                }
                var rs = responseMessage.Content.ReadAsStringAsync();
                var resultObject = JObject.Parse(rs.Result.ToString());
                if (string.IsNullOrEmpty(resultObject.GetValue("mst").ToString()))
                {
                    return obj;
                }

                var address = $@"{resultObject["dctsdchi"]}, {resultObject["dctstxa"]}, {resultObject["dctsthuyen"]}, {resultObject["dctstinh"]}";
                _systemService.SaveBusinessInfo(resultObject);
                obj = new JObject();
                obj.Add("business_name", resultObject.GetValue("tennnt"));
                obj.Add("address", address);
                return obj;
            }
            return obj;
        }


        [HttpPost]
        [Route("System/UpdateInfoCompany")]
        public async Task<JObject> UpdateInfoCompany(JObject model)
        {
            JObject obj = await _systemService.UpdateInfoCompany(model);
            return obj;
        }

        [HttpPost]
        [Route("System/UpdateInfoCompanySCTV")]
        public async Task<JObject> UpdateInfoCompanySCTV(JObject model)
        {
            JObject obj = await _systemService.UpdateInfoCompany(model);
            obj = FunctionUtil.AddInfoToResult(obj);
            return obj;
        }

    }
}
