﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InvoiceApi.Data.Domain;
using InvoiceApi.Services;
using System.Threading.Tasks;

namespace InvoiceApi.Controllers
{
    public class WbFieldController : BaseApiController
    {
        private IWbFieldService _wbFieldService;

        public WbFieldController(IWbFieldService wbFieldService)
        {
            this._wbFieldService = wbFieldService;
        }

        [HttpGet]
        [Route("WbField/All")]
        public async Task<JArray> GetAll()
        {
            var tblData = await _wbFieldService.GetAll(); 

            return tblData.ToJArray();
        }

        [HttpGet]
        [Route("WbField/GetFieldsByTabId")]
        public async Task<JArray> GetFieldsByTabId(string id)
        {
            var tblData = await  _wbFieldService.GetFieldsByTabId(id);

            return tblData.ToJArray();
        }

        [HttpGet]
        [Route("WbField/GetFields")]
        public async Task<JObject> GetFields(int start,int count,string filter)
        {
            var json = await _wbFieldService.GetFields(start, count, filter);

            return json;
        }

        [HttpPost]
        [Route("WbField/Update")]
        public async Task<JObject> Update(JObject model)
        {
            var json = await _wbFieldService.SaveChange(model);

            return json;
        }

        [HttpPost]
        [Route("WbField/Delete")]
        public async Task<JObject> Delete(JObject model)
        {
            var json = await _wbFieldService.Delete(model["id"].ToString());

            return json;
        }
        

    }
}