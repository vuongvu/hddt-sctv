﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InvoiceApi.Data.Domain;
using InvoiceApi.Services;
using InvoiceApi.Util;
using System.Threading.Tasks;
using WebApiThrottle;

namespace InvoiceApi.Controllers
{
    public class AccountController : BaseApiController
    {
        private IAccountService _accService;

        public AccountController(IAccountService accService)
        {
            this._accService = accService;
        }

        [HttpPost]
        [Route("Account/Login")]
        [AllowAnonymous]
        [EnableThrottling(PerSecond = 1, PerMinute = 30, PerHour = 100)]
        public async Task<JObject> Login(wb_user account)
        {
            string ipAddress = CommonManager.GetClientIpAddress(this.ActionContext.Request);
            string userAgent = this.ActionContext.Request.Headers.UserAgent.ToString();

            JObject json = await _accService.Login(account.username.ToUpper(), account.password, ipAddress, userAgent, account.ma_dvcs);

            return json;
        }

        [HttpPost]
        [Route("Account/LoginSCTV")]
        [AllowAnonymous]
        [EnableThrottling(PerSecond = 1, PerMinute = 30, PerHour = 100)]
        public async Task<JObject> LoginSCTV(wb_user account)
        {
            string ipAddress = CommonManager.GetClientIpAddress(this.ActionContext.Request);
            string userAgent = this.ActionContext.Request.Headers.UserAgent.ToString();

            JObject json = await _accService.Login(account.username.ToUpper(), account.password, ipAddress, userAgent, account.ma_dvcs);

            json = FunctionUtil.AddInfoToResult(json);
            return json;
        }

        [HttpPost]
        [Route("Account/ChangePass")]
        public async Task<JObject> ChangePass(JObject model)
        {
            JObject json = new JObject();

            string oldpass = model["oldpass"].ToString();
            string newpass = model["newpass"].ToString();
            string confirmpass = model["confirmpass"].ToString();

            if (string.IsNullOrEmpty(oldpass) || string.IsNullOrEmpty(newpass) || string.IsNullOrEmpty(confirmpass))
            {
                json.Add("error", "Bạn chưa nhập đủ thông tin đổi mật khẩu");
                return json;
            }

            if (newpass.Length < 6)
            {
                json.Add("error", "Mật khẩu phải từ 6 ký tự trở lên");
                return json;
            }

            if (newpass != confirmpass)
            {
                json.Add("error", "Mật khẩu mới không giống nhau");
                return json;
            }

            bool oldPassConfirm = await _accService.CheckPassword(this.UserName, oldpass);

            if (oldPassConfirm == false)
            {
                json.Add("error", "Mật khẩu cũ không đúng");
                return json;
            }

            json = await _accService.ChangePass(this.UserName, newpass);

            return json;
        }

        [HttpGet]
        [Route("Account/AllUsers")]
        public async Task<JArray> GetAllUsers()
        {
            return await _accService.GetUsers();
        }

        [HttpGet]
        [Route("Account/AllUsersByBranch")]
        public async Task<JArray> GetUsersByBranch()
        {
            return await _accService.GetUsersByBranch();
        }

        [HttpGet]
        [Route("Account/AllUsersByStore")]
        public async Task<JArray> GetUsersByStore()
        {
            return await _accService.GetUsersByStore();
        }

        [HttpPost]
        [Route("Account/Update")]
        public async Task<JObject> Update(wb_user model)
        {
            var json = await _accService.SaveChange(model);

            return json;
        }

        [HttpPost]
        [Route("Account/Delete")]
        public async Task<JObject> Delete(wb_user model)
        {
            var json = await _accService.Delete(model);

            return json;
        }

        [HttpPost]
        [Route("Account/PhanQuyen")]
        public async Task<JObject> PhanQuyen(JObject model)
        {
            var obj = await _accService.PhanQuyen(model);

            return obj;
        }

        [HttpPost]
        [Route("Account/StoreTemp")]
        public async Task<JObject> StoreTemp(JObject model)
        {
            var obj = await _accService.StoreTemp(model);

            return obj;
        }

        [HttpPost]
        [Route("Account/ResetPass")]
        [AllowAnonymous]
        [EnableThrottling(PerSecond = 1, PerMinute = 30, PerHour = 100)]
        public async Task<JObject> ResetPass(JObject model)
        {
            var obj = await _accService.SendEmailResetPass(model["email"].ToString());

            return obj;
        }

        [HttpGet]
        [Route("Account/GetUserHsmById")]
        public async Task<JArray> GetUserHsmById(string wb_user_id)
        {
            return await _accService.GetUserHsmById(wb_user_id);
        }

        [HttpPost]
        [Route("Account/Update_HSM")]
        public async Task<JObject> Update_HSM(wb_user_hsm_portal model)
        {
            var json = await _accService.Update_HSM(model);

            return json;
        }

        [HttpPost]
        [Route("Account/Delete_HSM")]
        public async Task<JObject> DeleteUserHsmById(JObject obj)
        {
            string wb_user_id = obj["wb_user_id"]?.ToString();

            return await _accService.DeleteUserHsmById(wb_user_id);
        }


        [HttpGet]
        [Route("Account/GetInfoUserLogin")]
        public async Task<string> GetInfoUserLogin()
        {
            return await _accService.getUserInfo();
        }
    }
}