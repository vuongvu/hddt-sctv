﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InvoiceApi.Data.Domain;
using InvoiceApi.Services;
using System.Threading.Tasks;

namespace InvoiceApi.Controllers
{
    public class WbTabController : BaseApiController
    {
        private IWbTabService _wbTabService;

        public WbTabController(IWbTabService wbTabService)
        {
            this._wbTabService = wbTabService;
        }

        [HttpGet]
        [Route("WbTab/All")]
        public async Task<JArray> GetAll()
        {
            var tblData = await _wbTabService.GetAll();

            return tblData.ToJArray();
        }

        [HttpGet]
        [Route("WbTab/GetTabsByWindowId")]
        public async Task<JArray> GetTabsByWindowId(string id)
        {
            var tblData = await _wbTabService.GetTabsByWindowId(id);

            return tblData.ToJArray();
        }

        [HttpGet]
        [Route("WbTab/GetTabs")]
        public async Task<JObject> GetTabs(int start,int count,string filter)
        {
            var json = await _wbTabService.GetTabs(start, count, filter);

            return json;
        }

        [HttpPost]
        [Route("WbTab/Update")]
        public async Task<JObject> Update(JObject model)
        {
            var json = await _wbTabService.SaveChange(model);

            return json;
        }

        [HttpPost]
        [Route("WbTab/Delete")]
        public async Task<JObject> Delete(JObject model)
        {
            var json = await _wbTabService.Delete(model["id"].ToString());

            return json;
        }
        

    }
}