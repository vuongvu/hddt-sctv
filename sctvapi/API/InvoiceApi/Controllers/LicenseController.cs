﻿using InvoiceApi.Services;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace InvoiceApi.Controllers
{
    public class LicenseController : BaseApiController
    {
        private readonly ILicenseService _licenseService;

        public LicenseController(ILicenseService licenseService)
        {
            this._licenseService = licenseService;
        }

        [HttpGet]
        [Route("License/Info")]
        public async Task<JObject> GetLicenseInfo(){
            JObject result = await this._licenseService.GetInfoLicense();
            return result;
        }

        [HttpPost]
        [Route("License/Register")]
        public JObject Register(JObject model)
        {
            JObject result = this._licenseService.RegisterLicense(model);
            return result;
        }

        [HttpGet]
        [Route("License/CheckLicense")]
        public JObject CheckLicense(string id)
        {
            JObject result = this._licenseService.CheckLicense(id);
            return result;
        }

        [HttpGet]
        [Route("License/PrintLicenseInfo")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PrintLicenseInfo()
        {

            HttpResponseMessage result = null;

            try
            {
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report" : "~/Content/report";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _licenseService.PrintLicenseInfo(folder);

                result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                result.Content.Headers.ContentDisposition.FileName = "LicenseInfo.pdf";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
    }
}