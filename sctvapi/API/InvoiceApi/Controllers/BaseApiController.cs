﻿using System.Web.Http;
using InvoiceApi.Attributes;

namespace InvoiceApi.Controllers
{
    [LogActionFilter]
    [RestAuthorizeAttribute]
    public abstract class BaseApiController : ApiController
    {
        public string UserName { get; set; }
        public string Ma_dvcs { get; set; }
        public string Store_code { get; set; }
    }
}