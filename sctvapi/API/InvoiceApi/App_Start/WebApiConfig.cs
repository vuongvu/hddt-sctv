﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using InvoiceApi.Services;
using System.Web.Http.ExceptionHandling;
using InvoiceApi.Util;
using WebApiThrottle;
using System.Web.Hosting;
using System.Xml;
using System.IO;
using System.Text;

namespace InvoiceApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.Services.Replace(typeof(IExceptionLogger), new UnhandledExceptionLogger());

            //config.Filters.Add(new ThrottlingFilter()
            //{
            //    Policy = new ThrottlePolicy(perSecond: 1, perMinute: 20, perHour: 200, perDay: 2000, perWeek: 10000)
            //    {
            //        //scope to IPs
            //        IpThrottling = true,                    

            //        //Endpoint rate limits will be loaded from EnableThrottling attribute
            //        EndpointThrottling = true
            //    }
            //});

            config.MessageHandlers.Add(new ThrottlingHandler()
            {
                Policy = new ThrottlePolicy(perSecond: 5000, perMinute: 5000 * 60, perHour: 5000 * 60 * 60)
                {
                    IpThrottling = true,
                    ClientThrottling = true,
                    EndpointThrottling = true
                }
            });

            // Web API routes
            config.MapHttpAttributeRoutes();            
            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //RegisterAutofacApi();

            //config.Formatters.JsonFormatter.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
        }    
        
    }
}
