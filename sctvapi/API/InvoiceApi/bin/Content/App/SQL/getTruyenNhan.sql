from ((select id as reg_id, tg_vendor_gui as tg_gui,
mngui as mngui_ft,
mltdiep_vendor_gui as mltdiep_gui,
mtdiep_vendor_gui as mtdiep_gui,
xml_vendor_gui as xml_thongdiep,
'' as mtdiep_thamchieu,
'VAN' as mnnhan,
tg_van_confirm as tg_nhan,
vendor as vendor,
mngui as mngui,
1 as stt
from request_data e)
union 
(select id as reg_id, tg_van_confirm as tg_gui,
'VAN' as mngui_ft,
mltdiep_van_confirm as mltdiep_gui,
mtdiep_van_confirm as mtdiep_gui,
xml_van_confirm as xml_thongdiep,
mtdiep_vendor_gui as mtdiep_thamchieu,
mngui as mnnhan,
tg_van_confirm as tg_nhan,
vendor as vendor,
mngui as mngui,
2 as stt
from request_data e)
union
(select id as reg_id, tg_van_gui as tg_gui,
'VAN' as mngui_ft,
mltdiep_van_gui as mltdiep_gui,
mtdiep_van_gui as mtdiep_gui,
xml_van_gui as xml_thongdiep,
'' as mtdiep_thamchieu,
'TCT' as mnnhan,
tg_van_gui as tg_nhan,
vendor as vendor,
mngui as mngui,
3 as stt
from request_data e)
union
(select id as reg_id,tg_tct_confirm as tg_gui,
'TCT' as mngui_ft,
mltdiep_tct_confirm as mltdiep_gui,
mtdiep_tct_confirm as mtdiep_gui,
xml_tct_confirm as xml_thongdiep,
mtdiep_van_gui as mtdiep_thamchieu,
'VAN' as mnnhan,
tg_tct_confirm as tg_nhan,
vendor as vendor,
mngui as mngui,
4 as stt
from request_data e)
union
(select id as reg_id,tg_tct_response as tg_gui,
'TCT' as mngui_ft,
mltdiep_tct_response as mltdiep_gui,
mtdiep_tct_response as mtdiep_gui,
xml_tct_response as xml_thongdiep,
mtdiep_van_gui as mtdiep_thamchieu,
'VAN' as mnnhan,
tg_tct_response as tg_nhan,
vendor as vendor,
mngui as mngui,
5 as stt
from request_data e)
union
(select id as reg_id, tg_tct_tiep_nhan as tg_gui,
'TCT' as mngui_ft,
mltdiep_tct_tiep_nhan as mltdiep_gui,
mtdiep_tct_tiep_nhan as mtdiep_gui,
xml_tct_tiep_nhan as xml_thongdiep,
mtdiep_van_gui as mtdiep_thamchieu,
'VAN' as mnnhan,
tg_tct_tiep_nhan as tg_nhan,
vendor as vendor,
mngui as mngui,
6 as stt
from request_data e)
union
(select id as reg_id, tg_van_send_tct_response as tg_gui,
'VAN' as mngui_ft,
mltdiep_van_send_tct_response as mltdiep_gui,
mtdiep_van_send_tct_response as mtdiep_gui,
xml_van_send_tct_response as xml_thongdiep,
mtdiep_vendor_gui as mtdiep_thamchieu,
mngui as mnnhan,
tg_van_send_tct_response as tg_nhan,
vendor as vendor,
mngui as mngui,
7 as stt
from request_data e)
union
(select id as reg_id, tg_van_send_tct_tiep_nhan as tg_gui,
'VAN' as mngui_ft,
mltdiep_van_send_tct_tiep_nhan as mltdiep_gui,
mtdiep_van_send_tct_tiep_nhan as mtdiep_gui,
xml_van_send_tct_tiep_nhan as xml_thongdiep,
mtdiep_tct_response as mtdiep_thamchieu,
mngui as mnnhan,
tg_van_send_tct_tiep_nhan as tg_nhan,
vendor as vendor,
mngui as mngui,
8 as stt
from request_data e)
union
(select b.id as reg_id, a.created_date as tg_gui,
CASE
  WHEN (a.mtdiep_gui like 'V%') THEN 'VAN'
  ELSE b.mngui
END  as mngui_ft,
a.mltdiep_gui as mltdiep_gui,
a.mtdiep_gui as mtdiep_gui,
a.xml_tdiep_gui as xml_thongdiep,
a.mtdtchieu as mtdiep_thamchieu,
CASE
  WHEN (a.mtdiep_gui like 'V%') THEN 'TCT'
  ELSE 'VAN'
END as mnnhan,
a.created_date as tg_nhan,
b.vendor as vendor,
b.mngui as mngui,9 as stt  from request_data_phkt a join request_data b on a.request_data_id = b.id)
)A