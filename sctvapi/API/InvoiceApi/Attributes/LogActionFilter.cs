﻿using InvoiceApi.Services;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace InvoiceApi.Attributes
{
    public class LogActionFilter : ActionFilterAttribute
    {
        public override async Task OnActionExecutingAsync(HttpActionContext filterContext, CancellationToken cancellationToken)
        {
            try
            {
                var uri = filterContext.Request.RequestUri.AbsolutePath;
                if (checkapi(uri))
                {
                    var requestScope = filterContext.Request.GetDependencyScope();
                    var service = requestScope.GetService(typeof(IWbLogAPI)) as IWbLogAPI;
                    string wb_log_api_id = await service.RegisterDataInputApi(uri, JsonConvert.SerializeObject(filterContext.ActionArguments));
                    filterContext.ActionArguments.Add("wb_log_api_id", wb_log_api_id);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public override async Task OnActionExecutedAsync(HttpActionExecutedContext filterContext, CancellationToken cancellationToken)
        {
            try
            {
                if (filterContext.Response != null && filterContext.ActionContext.ActionArguments["wb_log_api_id"] != null
                    && filterContext.Response.Content.ReadAsStringAsync().Result != null)
                {
                    string wb_log_api_id = filterContext.ActionContext.ActionArguments["wb_log_api_id"].ToString();
                    var requestScope = filterContext.Request.GetDependencyScope();
                    var service = requestScope.GetService(typeof(IWbLogAPI)) as IWbLogAPI;
                    await service.InsertWbLogAPIAsync(filterContext.Response.Content.ReadAsStringAsync().Result, "", wb_log_api_id);
                }
            }
            catch (Exception ex)
            {
            }
        }
        public bool checkapi(string uri)
        {
            bool res = true;
            string[] uri_not_get = new string[]{
                @"System/GetMenuBar",
                @"Invoice68/GetInvoiceNotify",
                @"System/GetDataByReferencesId",
                @"Invoice68/Amount_ToWord",
                @"Invoice68/GetDecimalPlace",
                @"System/CreateReport",
                @"System/GetConfigWinByNo",
                @"System/ExecuteCommand",
                @"System/GetInfoCompany",
                @"Invoice68/GetDataInvoiceThongTin",
                @"Invoice68/GetDataInvoiceDetail",
                @"Invoice68/GetDataInvoiceKhac",
                @"Invoice68/GetDataInvoicePhi",
                @"Account/AllUsersByBranch",
                @"Account/GetInfoUserLogin",
                @"Account/GetUserHsmById",
                @"Account/Login",
                @"Account/LoginSCTV",
                @"Account/LoginSCTV",
                @"Account/Phanquyen",
                @"Invoice68/DashboardTiles78",
                @"Invoice68/DowloadRepx",
                @"Invoice68/ExportXML",
                @"Invoice68/Get",
                @"Invoice68/get",
                @"Invoice68/in",
                @"Invoice68/In",
                @"Invoice68/Print",
                @"Invoice68/quanly",
                @"Invoice/DashboardTiles",
                @"Invoice/Get",
                @"System/Get",
            };
            foreach (var item in uri_not_get)
            {
                if (uri.Contains(item))
                {
                    res = false; break;
                }
            }

            return res;
        }
    }
}