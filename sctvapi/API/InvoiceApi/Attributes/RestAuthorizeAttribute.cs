﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using InvoiceApi.Controllers;
using InvoiceApi.Services;
using InvoiceApi.Util;

namespace InvoiceApi.Attributes
{
    public class RestAuthorizeAttribute : AuthorizeAttribute
    {
        private const string _securityToken = "token";

        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (Authorize(filterContext))
            {
                return;
            }

            HandleUnauthorizedRequest(filterContext);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

        private bool Authorize(HttpActionContext actionContext)
        {
            try
            {

                HttpRequestMessage requestMessage = actionContext.Request;

                string ipAddress = CommonManager.GetClientIpAddress(requestMessage);
                string userAgent = requestMessage.Headers.UserAgent.ToString();

                if (SkipAuthorization(actionContext))
                {
                    return true;
                }

                string token = requestMessage.Headers.Authorization.Parameter;

                return IsTokenValid(token, ipAddress, userAgent, actionContext);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool SkipAuthorization(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                       || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }

        public bool IsTokenValid(string token, string ip, string userAgent, HttpActionContext actionContext)
        {
            bool result = false;

            try
            {

                string[] tokenParts = token.Split(';');
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(tokenParts[0]));

                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 4)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    DateTime timeStamp = new DateTime(ticks);

                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.Now - timeStamp).TotalMinutes) > 120;

                    //!expired
                    if (!expired)
                    {
                        // Get the request lifetime scope so you can resolve services.
                        var requestScope = actionContext.Request.GetDependencyScope();

                        var _accountService = requestScope.GetService(typeof(IAccountService)) as IAccountService;

                        var account = _accountService.GetAccountByUserName(username);

                        // Hash the message with the key to generate a token.
                        string computedToken = _accountService.GenerateToken(username, account.password, ip, userAgent, ticks, tokenParts[1]);

                        // Compare the computed token with the one supplied and ensure they match.
                        result = (tokenParts[0] == computedToken);

                       
                        if (result)
                        {
                            if (actionContext.ControllerContext.Controller is BaseApiController)
                            {
                                BaseApiController baseController = actionContext.ControllerContext.Controller as BaseApiController;
                                baseController.UserName = username;
                                baseController.Ma_dvcs = tokenParts[1];
                                baseController.Store_code = account.store_code;
                            }
                        }

                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }
    }
}