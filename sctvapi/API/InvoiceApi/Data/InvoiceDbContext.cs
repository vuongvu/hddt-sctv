﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using InvoiceApi.Data.Domain;

namespace InvoiceApi.Data
{
    public class InvoiceDbContext : DbContext 
    {
        //public InvoiceDbContext()
        //    : base("DefaultConnection")
        //{
        //    //this.Configuration.LazyLoadingEnabled = false;
        //    //this.Configuration.ProxyCreationEnabled = false;
        //}

        public InvoiceDbContext(string connectionString)
            : base(connectionString)
        {
            //this.Configuration.LazyLoadingEnabled = false;
            //this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<wb_window> WbWindows { get; set; }
        public DbSet<wb_user> WbUsers { get; set; }
        public DbSet<wb_menu> WbMenus { get; set; }
        public DbSet<wb_tab> WbTabs { get; set; }
        public DbSet<wb_field> WbFields { get; set; }
        public DbSet<systemsetting> Systemsettings { get; set; }
        public DbSet<dmdvcs> Dmdvcss { get; set; }
        public DbSet<dvpermission> Dvpermissions { get; set; }
        public DbSet<wb_ctquyen> WbCtquyens { get; set; }
        //public DbSet<wb_log> WbLogs { get; set; }
    }
}