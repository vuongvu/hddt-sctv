﻿using InvoiceApi.Redis;
using InvoiceApi.Services;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InvoiceApi.Data
{
    public class MasterInvoiceDbContext : IMasterInvoiceDbContext
    {
        #region Fields

        private readonly IWebHelper _webHelper;

        private NpgsqlConnection _npgsqlConnection;
        private NpgsqlTransaction _npgsqlTransaction;
        //private IRabbitMQService _rabbitMQService;
        private string schemaName;

        #endregion

        #region Ctor

        public MasterInvoiceDbContext(IWebHelper webHelper)
        {
            this._webHelper = webHelper;
            schemaName = CommonConstants.MASTER_SCHEMA;
        }

        #endregion

        #region Private Method

        private string GetMasterConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MasterConnection"].ConnectionString;
        }


        public string GetSchemaName()
        {
            return this.schemaName;
        }

        #endregion


        #region Public Methods

        public void ExecuteNoneQuery(string sql)
        {
            this.ExecuteNoneQuery(sql, null);
        }

        public void ExecuteNoneQuery(string sql, DataRow row)
        {
            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            string connectionString = this.GetMasterConnectionString();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (row != null)
                    {
                        DataTable table = row.Table;

                        foreach (DataColumn column in table.Columns)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + column.ColumnName;
                            parameter.Value = row[column.ColumnName];

                            if (CommonManager.CheckForSqlInjection(row[column.ColumnName]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(column.ColumnName.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {column.ColumnName} : {row[column.ColumnName]?.ToString()}");
                            }

                            command.Parameters.Add(parameter);
                        }
                    }

                    //command.ExecuteNonQuery();

                    command.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

        }

        public async Task<string> ExecuteNoneQueryAsync(string sql)
        {
            DataRow row = null;
            return await this.ExecuteNoneQueryAsync(sql, CommandType.Text, row);
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, DataRow row)
        {
            return await this.ExecuteNoneQueryAsync(sql, CommandType.Text, row);
        }

        public async Task<string> ExecuteNoneQueryAsyncNotCheckHTML(string sql, DataRow row)
        {
            return await this.ExecuteNoneQueryAsyncNotCheckHTML(sql, CommandType.Text, row);
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, DataRow row)
        {
            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (row != null)
                    {
                        DataTable table = row.Table;

                        foreach (DataColumn column in table.Columns)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + column.ColumnName;
                            parameter.Value = row[column.ColumnName];

                            if (CommonManager.CheckForSqlInjection(row[column.ColumnName]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(column.ColumnName.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {column.ColumnName} : {row[column.ColumnName]?.ToString()}");
                            }

                            command.Parameters.Add(parameter);
                        }
                    }
                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }


            return "";
        }

        public async Task<string> ExecuteNoneQueryAsyncNotCheckHTML(string sql, CommandType commandType, DataRow row)
        {
            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (row != null)
                    {
                        DataTable table = row.Table;

                        foreach (DataColumn column in table.Columns)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + column.ColumnName;
                            parameter.Value = row[column.ColumnName];

                            if (CommonManager.CheckForSqlInjection(row[column.ColumnName]?.ToString(), false))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {column.ColumnName} : {row[column.ColumnName]?.ToString()}");
                            }

                            command.Parameters.Add(parameter);
                        }
                    }
                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, JObject obj)
        {
            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand())
                {
                    command.Connection = conn;
                    command.CommandType = CommandType.Text;

                    if (commandType == CommandType.StoredProcedure)
                    {
                        sql = sql.Replace("#SCHEMA_NAME#.", "");

                        if (CommonManager.CheckForSqlInjection(sql))
                        {

                            throw new Exception($"Dữ liệu không hợp lệ {sql}");
                        }

                        string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                    + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                    + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "'";

                        DataTable tblParameter = new DataTable();

                        using (NpgsqlCommand cmd = new NpgsqlCommand(query, conn))
                        {
                            cmd.CommandType = CommandType.Text;

                            var reader = await cmd.ExecuteReaderAsync();
                            tblParameter.Load(reader);

                            string parameterName = "";

                            if (tblParameter.Rows.Count > 0)
                            {
                                foreach (DataRow row in tblParameter.Rows)
                                {
                                    string proargnames = row["proargnames"].ToString();
                                    string proargtypes = row["proargtypes"].ToString();

                                    parameterName = parameterName + "@" + proargnames + ",";

                                    string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                    NpgsqlParameter parameter = command.CreateParameter();
                                    parameter.ParameterName = "@" + proargnames;
                                    parameter.NpgsqlValue = DBNull.Value;

                                    if (proargnames == "p_schemaname")
                                    {
                                        parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                        parameter.NpgsqlValue = schemaName;
                                    }
                                    else
                                    {
                                        if (obj != null)
                                        {
                                            if (obj[fieldName] != null)
                                            {

                                                if (proargtypes == "uuid")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Guid.Parse(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "timestamp without time zone")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "date")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "integer")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToInt32(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "numeric")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDecimal(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "boolean")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                    bool _value = obj[fieldName].ToString() == "1" || obj[fieldName].ToString().ToLower() == "true" ? true : false;
                                                    parameter.NpgsqlValue = _value;
                                                }
                                                else if (proargtypes == "character varying" || proargtypes == "text")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                    parameter.NpgsqlValue = obj[fieldName].ToString();
                                                }
                                            }
                                        }

                                    }

                                    if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                    {

                                        throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                    }

                                    command.Parameters.Add(parameter);
                                }
                            }

                            if (string.IsNullOrEmpty(parameterName))
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                            }
                            else
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                            }
                        }
                    }
                    else
                    {
                        sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                        command.CommandText = sql;

                        if (obj != null)
                        {
                            foreach (var entry in obj)
                            {
                                string type = entry.Value.Type.ToString();

                                if (type == "Guid")
                                {
                                    command.Parameters.AddWithValue(entry.Key, Guid.Parse(entry.Value.ToString()));
                                }
                                else
                                {
                                    command.Parameters.AddWithValue(entry.Key, entry.Value);
                                }
                            }
                        }
                    }

                    //await command.ExecuteNonQueryAsync();


                    await command.ExecuteNonQueryAsync();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, Dictionary<string, object> parameters)
        {
            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + entry.Key;
                            parameter.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            //if(entry.Key == "sl_invoice_template_id")
                            //{
                            //    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                            //}
                            command.Parameters.Add(parameter);
                        }
                    }

                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public async Task<string> ExecuteNoneQueryAsyncNoneSQLInjection(string sql, Dictionary<string, object> parameters)
        {
            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + entry.Key;
                            parameter.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            command.Parameters.Add(parameter);
                        }
                    }

                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public DataTableExtend GetDataTable(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    //var reader = command.ExecuteReader();
                    //tblData.Load(reader);

                    var reader = command.ExecuteReader();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public DataTableExtend GetDataTable(string sql, CommandType commandType, params object[] parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@P_" + i.ToString();
                        npgsqlParam.Value = parameters[i];

                        if (CommonManager.CheckForSqlInjection(parameters[i]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains("p_" + i.ToString().ToLower())))
                        {

                            throw new Exception($"Dữ liệu không hợp lệ {parameters[i]} : {parameters[i]?.ToString()}");
                        }

                        command.Parameters.Add(npgsqlParam);
                    }


                    var reader = command.ExecuteReader();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }


            return tblData;
        }

        public DataTableExtend GetDataTable(string sql)
        {
            return this.GetDataTable(sql, CommandType.Text, (Dictionary<string, object>)null);
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);


            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    //var reader = await command.ExecuteReaderAsync();
                    //tblData.Load(reader);

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, params object[] parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@P_" + i.ToString();
                        npgsqlParam.Value = parameters[i];

                        if (CommonManager.CheckForSqlInjection(parameters[i]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains("p_" + i.ToString().ToLower())))
                        {


                            throw new Exception($"Dữ liệu không hợp lệ {parameters[i]} : {parameters[i]?.ToString()}");
                        }


                        command.Parameters.Add(npgsqlParam);
                    }

                    //var reader = await command.ExecuteReaderAsync();
                    //tblData.Load(reader);

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql, params object[] parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@P_" + i.ToString();
                        npgsqlParam.Value = parameters[i];

                        if (CommonManager.CheckForSqlInjection(parameters[i]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains("p_" + i.ToString().ToLower())))
                        {

                            throw new Exception($"Dữ liệu không hợp lệ {parameters[i]} : {parameters[i]?.ToString()}");
                        }

                        command.Parameters.Add(npgsqlParam);
                    }

                    //var reader = await command.ExecuteReaderAsync();
                    //tblData.Load(reader);

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql)
        {
            return await this.GetDataTableAsync(sql, CommandType.Text, (Dictionary<string, object>)null);
        }

        public async Task<DataSet> GetDataSetAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "dataSet1";

            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (commandType == CommandType.StoredProcedure)
                    {
                        sql = sql.Replace("#SCHEMA_NAME#.", "");

                        if (CommonManager.CheckForSqlInjection(sql))
                        {
                            throw new Exception($"Dữ liệu không hợp lệ {sql}");
                        }

                        string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                    + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                    + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "' AND p.proargtypes IS NOT NULL";


                        DataTable tblParameter = new DataTable();

                        using (NpgsqlCommand cmd = new NpgsqlCommand(query, conn))
                        {

                            cmd.CommandType = CommandType.Text;

                            var reader = await cmd.ExecuteReaderAsync();
                            tblParameter.Load(reader);

                            string parameterName = "";

                            if (tblParameter.Rows.Count > 0)
                            {
                                foreach (DataRow row in tblParameter.Rows)
                                {
                                    string proargnames = row["proargnames"].ToString();
                                    string proargtypes = row["proargtypes"].ToString();

                                    if (proargtypes.Length == 0)
                                    {
                                        continue;
                                    }

                                    parameterName = parameterName + "@" + proargnames + ",";

                                    string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                    NpgsqlParameter parameter = command.CreateParameter();
                                    parameter.ParameterName = "@" + proargnames;
                                    parameter.NpgsqlValue = DBNull.Value;



                                    if (proargnames == "p_schemaname")
                                    {
                                        parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                        parameter.NpgsqlValue = schemaName;
                                    }
                                    else
                                    {
                                        if (parameters != null)
                                        {
                                            if (parameters.ContainsKey(fieldName))
                                            {

                                                if (proargtypes == "uuid")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Guid.Parse(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "timestamp without time zone")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "date")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "integer")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToInt32(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "boolean")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                    bool _value = parameters[fieldName].ToString() == "1" || parameters[fieldName].ToString().ToLower() == "true" ? true : false;
                                                    parameter.NpgsqlValue = _value;
                                                }
                                                else if (proargtypes == "character varying" || proargtypes == "text")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                    parameter.NpgsqlValue = parameters[fieldName].ToString();
                                                }
                                            }
                                        }

                                    }

                                    if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                    {

                                        throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                    }

                                    command.Parameters.Add(parameter);
                                }
                            }

                            if (string.IsNullOrEmpty(parameterName))
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                            }
                            else
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                            }
                        }
                    }
                    else
                    {
                        sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                        command.CommandText = sql;

                        if (parameters != null)
                        {
                            foreach (var entry in parameters)
                            {

                                if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                                {

                                    throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                                }

                                command.Parameters.AddWithValue(entry.Key, entry.Value == null ? DBNull.Value : entry.Value);
                            }
                        }
                    }

                    var reader1 = await command.ExecuteReaderAsync();
                    DataTable table = new DataTable();
                    table.TableName = "Table";

                    do
                    {
                        table.Load(reader1);

                    } while (!reader1.IsClosed);

                    ds.Tables.Add(table);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }


            return ds;
        }
        public async Task<DataSet> GetDataSetAsyncPrintf(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "dataSet1";

            string connectionString = this.GetMasterConnectionString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = entry.Key;

                            npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }
                    NpgsqlParameter npgsqlParam2 = command.CreateParameter();
                    npgsqlParam2.ParameterName = "p_schemaname";
                    npgsqlParam2.Value = schemaName;
                    npgsqlParam2.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text;
                    command.Parameters.Add(npgsqlParam2);
                    var reader = command.ExecuteReader();
                    DataTable table = new DataTable();
                    table.TableName = "Table";

                    do
                    {
                        table.Load(reader);

                    } while (!reader.IsClosed);

                    ds.Tables.Add(table);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return ds;
        }
        public async Task<string> BeginTransactionAsync()
        {
            string connectionString = this.GetMasterConnectionString();

            _npgsqlConnection = new NpgsqlConnection(connectionString);
            await _npgsqlConnection.OpenAsync();

            _npgsqlTransaction = _npgsqlConnection.BeginTransaction();

            return "";
        }

        public async Task<string> TransactionCommandAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            string schemaName = this.GetSchemaName();

            using (var command = new NpgsqlCommand())
            {
                command.Connection = _npgsqlConnection;
                command.CommandType = CommandType.Text;
                command.Transaction = _npgsqlTransaction;

                if (commandType == CommandType.StoredProcedure)
                {
                    sql = sql.Replace("#SCHEMA_NAME#.", "");

                    if (CommonManager.CheckForSqlInjection(sql))
                    {
                        throw new Exception($"Dữ liệu không hợp lệ {sql}");
                    }

                    string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "'";


                    DataTable tblParameter = new DataTable();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(query, _npgsqlConnection))
                    {

                        cmd.CommandType = CommandType.Text;

                        var reader = await cmd.ExecuteReaderAsync();
                        tblParameter.Load(reader);

                        string parameterName = "";

                        if (tblParameter.Rows.Count > 0)
                        {
                            foreach (DataRow row in tblParameter.Rows)
                            {
                                string proargnames = row["proargnames"].ToString();
                                string proargtypes = row["proargtypes"].ToString();

                                parameterName = parameterName + "@" + proargnames + ",";

                                string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                NpgsqlParameter parameter = command.CreateParameter();
                                parameter.ParameterName = "@" + proargnames;
                                parameter.NpgsqlValue = DBNull.Value;

                                if (proargnames == "p_schemaname")
                                {
                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                    parameter.NpgsqlValue = schemaName;
                                }
                                else
                                {
                                    if (parameters != null)
                                    {
                                        if (parameters.ContainsKey(fieldName))
                                        {

                                            if (proargtypes == "uuid")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Guid.Parse(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "timestamp without time zone")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "date")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "integer")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToInt32(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "boolean")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                bool _value = parameters[fieldName].ToString() == "1" || parameters[fieldName].ToString().ToLower() == "true" ? true : false;
                                                parameter.NpgsqlValue = _value;
                                            }
                                            else if (proargtypes == "character varying" || proargtypes == "text")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                parameter.NpgsqlValue = parameters[fieldName].ToString();
                                            }
                                        }
                                    }

                                }

                                if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                {
                                    throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                }

                                command.Parameters.Add(parameter);
                            }
                        }

                        if (string.IsNullOrEmpty(parameterName))
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                        }
                        else
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                        }
                    }
                }
                else
                {
                    sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                    command.CommandText = sql;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                            }

                            if (entry.Key == "dlxml" || entry.Key == "xml_send" || entry.Key == "xml_rep" || entry.Key == "dlxml_thue")
                            {
                                command.Parameters.Add(new NpgsqlParameter(entry.Key, DbType.Xml)).Value = entry.Value == null ? DBNull.Value : entry.Value;
                            }
                            else
                            {
                                command.Parameters.AddWithValue(entry.Key, entry.Value == null ? DBNull.Value : entry.Value);
                            }

                        }
                    }
                }

                await command.ExecuteNonQueryAsync();
            }

            return "";
        }

        public async Task<string> TransactionCommandAsync(string sql, CommandType commandType, JObject parameters)
        {
            string schemaName = this.GetSchemaName();

            using (var command = new NpgsqlCommand())
            {
                command.Connection = _npgsqlConnection;
                command.CommandType = CommandType.Text;
                command.Transaction = _npgsqlTransaction;

                if (commandType == CommandType.StoredProcedure)
                {
                    sql = sql.Replace("#SCHEMA_NAME#.", "");

                    if (CommonManager.CheckForSqlInjection(sql))
                    {
                        throw new Exception($"Dữ liệu không hợp lệ {sql}");
                    }

                    string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "'";


                    DataTable tblParameter = new DataTable();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(query, _npgsqlConnection))
                    {

                        cmd.CommandType = CommandType.Text;

                        var reader = await cmd.ExecuteReaderAsync();
                        tblParameter.Load(reader);

                        string parameterName = "";

                        if (tblParameter.Rows.Count > 0)
                        {
                            foreach (DataRow row in tblParameter.Rows)
                            {
                                string proargnames = row["proargnames"].ToString();
                                string proargtypes = row["proargtypes"].ToString();

                                parameterName = parameterName + "@" + proargnames + ",";

                                string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                NpgsqlParameter parameter = command.CreateParameter();
                                parameter.ParameterName = "@" + proargnames;
                                parameter.NpgsqlValue = DBNull.Value;

                                if (proargnames == "p_schemaname")
                                {
                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                    parameter.NpgsqlValue = schemaName;
                                }
                                else
                                {
                                    if (parameters != null)
                                    {
                                        if (parameters[fieldName] != null)
                                        {

                                            if (proargtypes == "uuid")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Guid.Parse(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "timestamp without time zone")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "date")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "numeric")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDecimal(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "integer")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToInt32(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "boolean")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                bool _value = parameters[fieldName].ToString() == "1" || parameters[fieldName].ToString().ToLower() == "true" ? true : false;
                                                parameter.NpgsqlValue = _value;
                                            }
                                            else if (proargtypes == "character varying" || proargtypes == "text")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                parameter.NpgsqlValue = parameters[fieldName].ToString();
                                            }
                                        }
                                    }

                                }

                                if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                {
                                    throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                }

                                command.Parameters.Add(parameter);
                            }
                        }

                        if (string.IsNullOrEmpty(parameterName))
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                        }
                        else
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                        }
                    }
                }
                else
                {
                    sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                    command.CommandText = sql;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.AddWithValue(entry.Key, entry.Value.ToString());
                        }
                    }
                }

                await command.ExecuteNonQueryAsync();
            }

            return "";
        }

        public async Task<string> TransactionCommitAsync()
        {
            await _npgsqlTransaction.CommitAsync();

            return "";
        }

        public async Task<DataTableExtend> GetTransactionDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            using (NpgsqlCommand command = new NpgsqlCommand(sql, _npgsqlConnection))
            {
                command.CommandType = commandType;
                command.Transaction = _npgsqlTransaction;

                if (parameters != null)
                {
                    foreach (var entry in parameters)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@" + entry.Key;
                        npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;


                        if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                        {
                            throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                        }

                        command.Parameters.Add(npgsqlParam);
                    }
                }

                var reader = await command.ExecuteReaderAsync();
                tblData.Load(reader);
            }

            return tblData;
        }

        public async Task<string> TransactionRollbackAsync()
        {
            if (_npgsqlTransaction != null)
            {
                await _npgsqlTransaction.RollbackAsync();
            }

            return "";
        }

        public void CloseTransaction()
        {
            if (_npgsqlTransaction != null)
            {
                _npgsqlTransaction.Dispose();
                _npgsqlTransaction = null;
            }

            if (_npgsqlConnection != null)
            {
                _npgsqlConnection.Close();
                _npgsqlConnection.Dispose();

                _npgsqlConnection = null;
            }
        }

        #endregion

    }
}