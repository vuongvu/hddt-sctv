﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using InvoiceApi.Data.Domain;

namespace InvoiceApi.Data
{
    public class TracuuHDDTContext : DbContext
    {
        public TracuuHDDTContext(string dbConnectionString)
            : base(dbConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("reg");
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<inv_admin> Inv_admin { get; set; }
        public DbSet<inv_user> inv_users { get; set; }
    }
}