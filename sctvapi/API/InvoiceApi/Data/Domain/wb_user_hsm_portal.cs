﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InvoiceApi.Data.Domain
{
    public class wb_user_hsm_portal
    {
        [Key]
        [Column("wb_user_hsm_portal_id")]
        public Guid wb_user_hsm_portal_id { get; set; }
        public string username { get; set; }
        public string ma_dvcs { get; set; }
        public string password { get; set; }
        public string password_level2 { get; set; }
        public Guid? wb_user_id { get; set; }
        [NotMapped]
        public string window_type { get; set; }
    }
}