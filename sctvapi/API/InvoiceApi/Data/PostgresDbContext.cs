﻿using InvoiceApi.Data.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InvoiceApi.Data
{
    public class PostgresDbContext : DbContext
    {
        private readonly string schema;

        public PostgresDbContext(string schema, string dbConnectionString)
        : base(dbConnectionString)
        {
            this.schema = schema;
        }

        public DbSet<dmdvcs> Dmdvcss { get; set; }
        public DbSet<wb_user> WbUsers { get; set; }
        public DbSet<dvpermission> Dvpermissions { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.HasDefaultSchema(this.schema);
            base.OnModelCreating(builder);
        }
    }
}