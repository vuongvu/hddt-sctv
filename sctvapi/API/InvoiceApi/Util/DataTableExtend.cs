﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace InvoiceApi.Util
{
    public class DataTableExtend : DataTable
    {
        public DataTableExtend()
        {
        }

        public JArray ToJArray()
        {
            return JArray.FromObject(this);
        }

        public DataRow CreateDataRow(JObject model)
        {
            DataRow row = this.NewRow();

            row.BeginEdit();

            foreach (DataColumn column in this.Columns)
            {
                if (model[column.ColumnName] != null)
                {
                    row[column.ColumnName] = GetValueFromJObject(column, model[column.ColumnName]);
                }
            }

            row.EndEdit();

            return row;
        }

        public DataRow ImportData(DataRow row, JObject model)
        {
            DataRow _row = row;

            if (_row == null)
            {
                _row = this.NewRow();
            }

            _row.BeginEdit();

            foreach (DataColumn column in this.Columns)
            {
                if (model[column.ColumnName] != null)
                {
                    _row[column.ColumnName] = GetValueFromJObject(column, model[column.ColumnName]);
                }
            }

            _row.EndEdit();

            return _row;
        }

        public string GenerateInsertSQL()
        {
            StringBuilder lstColumn=new StringBuilder();
            StringBuilder lstValueParameter=new StringBuilder();

            foreach (DataColumn column in this.Columns)
            {
                lstColumn.Append(column.ColumnName + ",");
                lstValueParameter.Append("@"+column.ColumnName+",");
            }

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("INSERT INTO #SCHEMA_NAME#." + this.TableName + " (" + lstColumn.ToString().Substring(0, lstColumn.Length - 1) + ")");
            builder.AppendLine("VALUES (" + lstValueParameter.ToString().Substring(0, lstValueParameter.Length - 1) + ")");

            return builder.ToString();
        }

        public string GenerateUpdateSQL()
        {
            StringBuilder lstValueParameter = new StringBuilder();

            foreach (DataColumn column in this.Columns)
            {
                lstValueParameter.Append(column.ColumnName+ "=@" + column.ColumnName + ",");
            }

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("UPDATE #SCHEMA_NAME#." + this.TableName + " SET ");
            builder.AppendLine(lstValueParameter.ToString().Substring(0, lstValueParameter.Length - 1));
            builder.AppendLine("WHERE " + this.TableName + "_id=@" + this.TableName + "_id");            

            return builder.ToString();
        }

        public string GenerateDeleteSQL()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("DELETE FROM #SCHEMA_NAME#." + this.TableName );            
            builder.AppendLine("WHERE " + this.TableName + "_id=@" + this.TableName + "_id");

            return builder.ToString();
        }

        private object GetValueFromJObject(DataColumn column, JToken jtoken)
        {
            object value = DBNull.Value;

            string name = column.DataType.Name;
            string tokenValue = jtoken.ToString();

            switch (name)
            {
                case "Guid":

                    if (tokenValue != "")
                    {
                        value = Guid.Parse(tokenValue);
                    }

                    break;
                case "DateTime":
                    
                    if (tokenValue != "")
                    {
                        value = Convert.ToDateTime(tokenValue);
                    }

                    break;
                case "Int32":

                    if (tokenValue != "")
                    {
                        value = Convert.ToInt32(tokenValue);
                    }

                    break;
                default:
                    value = tokenValue;
                    break;
            }

            return value;
        }
    }
}