﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace InvoiceApi.Util
{
    public class ValidInvoice
    {
        public static bool InsertValid(JObject _obj, ref JObject _json)
        {
            bool _result = true;
            if (string.IsNullOrEmpty(_obj["mst"].ToString()))
            {
                _json.Add("error", "Bạn chưa nhập mã số thuế !");
                _result = false;
            }
            return _result;
        }
    }
}