﻿using PkiTokenManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApi.Util
{
    public class Pkcs11SignerProvider : ISignerProvider
    {
        private string _certSerial;
        private string _tokenSerial;
        private string _pass;

        public Pkcs11SignerProvider(string certSerial, string tokenSerial, string pass)
        {
            this._certSerial = certSerial;
            this._tokenSerial = tokenSerial;
            this._pass = pass;
        }

        public byte[] Sign(byte[] data)
        {
            PKI pki = PKI.GetPKI();
            
            byte[] bytes=pki.Sign(_tokenSerial, _certSerial, _pass, data);

            return bytes;
        }
    }
}