﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Util
{
    public interface ISignerProvider
    {
        byte[] Sign(byte[] data);
    }
}
