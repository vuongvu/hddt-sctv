﻿using System.Configuration;

namespace InvoiceApi.Util
{
    public class CommonConstants
    {
        public static readonly string SCHEMA_REG = ConfigurationManager.AppSettings["Schema_reg"];
        public static readonly string SCHEMA_SOURCE = ConfigurationManager.AppSettings["Schema_dbo"];
        public static readonly string MINVOICE = ConfigurationManager.AppSettings["MInvoice"];
        public static readonly string BRANCH_CODE = ConfigurationManager.AppSettings["Branch_Code"].ToString();
        public static readonly string FOLDER_PRINTLIST = ConfigurationManager.AppSettings["FOLDER_PRINTLIST"].ToString();
        public static readonly string MASTER_SCHEMA = ConfigurationManager.AppSettings["MASTER_SCHEMA"].ToString();

        public static readonly string QLDV_CONTRACT = "https://marketplace.mobifone.vn:8443/khdn/rest/mInvoiceService/contacts";
        public static readonly string QLDV_CONTRACT_SUB = "https://marketplace.mobifone.vn:8443/khdn/rest/mInvoiceService/contactSubs";
        public static readonly string QLDV_PASS_API = "marketplacettcntt1234$";

        //Create KH MOIK
        public static readonly string PASS_LOGIN = "123456";
        public static readonly string PASS_HASH = "58VGocVzBCPK8cO14Hj0M/IwVRqgfdzDOBnazaGGFmW7um3j3YNk6Zx0Onu5H9+zqogclyhX9WApQc6b8WwcKCLB/Ocgx+XpghTkpERSsdY=";

        //signHSM
        public static readonly string BASE_HSM_URL = ConfigurationManager.AppSettings["BASE_HSM_URL"];
        public static readonly string SIGN_HSM_URL_32 = ConfigurationManager.AppSettings["SIGN_HSM_URL_32"];
        public static readonly string SIGN_HSM_URL_78 = ConfigurationManager.AppSettings["SIGN_HSM_URL_78"];

        public static readonly string LOGIN = ConfigurationManager.AppSettings["HSM_LOGIN"];
        public static readonly string CTS_HSM = ConfigurationManager.AppSettings["CTS_HSM"];

        //signHSMSCTV
        public static readonly string BASE_HSM_URL_SCTV = ConfigurationManager.AppSettings["BASE_HSM_URL_SCTV"];
        public static readonly string SIGN_HSM_URL_SCTV_TT78 = ConfigurationManager.AppSettings["SIGN_HSM_URL_SCTV_TT78"];
        public static readonly string SIGN_HSM_URL_SCTV_TT32 = ConfigurationManager.AppSettings["SIGN_HSM_URL_SCTV_TT32"];
        public static readonly string CTS_HSM_SCTV = ConfigurationManager.AppSettings["CTS_HSM_SCTV"];

        public static readonly ConnectionStringSettings CONNECT_STRING = ConfigurationManager.ConnectionStrings["InvoiceConnection"];

        public static readonly string[] WHITE_LIST_COLUMN = { "on_click", "body", "data", "xml_data", "xml", "data_xml", "content",
            "response", "icon_css", "xml_send", "xml_rep", "dlxml", "dlxml_thue","dlxml_bangke", "ly_do", "xml_nhan", "xml_cqt", "dulieumau", "xml_tdiep_gui", "signature" };

        public static readonly int TIEP_NHAN_THONG_BAO_UY_NHIEM = 3;
        public static readonly int CHAP_NHAN_UY_NHIEM = 1;
        public static readonly int KHONG_CHAP_NHAN_UY_NHIEM = 2;

        public static readonly string VENDOR = ConfigurationManager.AppSettings["VENDOR"];
        public static readonly string PRIVATE_KEY = ConfigurationManager.AppSettings["PRIVATE_KEY"];
        public static readonly string TVAN_MST = ConfigurationManager.AppSettings["TVAN_MST"];

        public static readonly string TOPIC_VAN_SEND_REPLY_REQUEST_TO_VENDOR = "topic_van_send_reply_request_to_vendor" + "_" + ConfigurationManager.AppSettings["VENDOR"];
        public static readonly string TOPIC_VAN_SEND_REPONSE_TO_VENDOR = "topic_van_send_reponse_to_vendor" + "_" + ConfigurationManager.AppSettings["VENDOR"];
        public static readonly string TOPIC_VENDOR_SEND_TO_VAN = "topic_vendor_send_to_van";

        public static readonly string KAFKA_HOST_SERVER = ConfigurationManager.AppSettings["KAFKA_HOST_SERVER"];
        public static readonly string GROUP_ID = ConfigurationManager.AppSettings["GROUP_ID"];

        public static readonly string QUEUE_VENDOR_SEND_REQUEST_TO_VAN = "vendor_send_request_van";
        public static readonly string QUEUE_VENDOR_RECEIVE_VAN_CONFIRM_TECHNICAL = "vendor_receive_van_confirm_technical";
        public static readonly string QUEUE_VENDOR_RECEIVE_TCT_CONFIRM_TECHNICAL = "vendor_receive_tct_confirm_technical";
        public static readonly string QUEUE_VENDOR_RECEIVE_TCT_RESPONSE = "vendor_receive_tct_response";

        public static readonly string MLTDIEP_TO_KHAI_KHONG_UY_NHIEM = "100";
        public static readonly string MLTDIEP_TO_KHAI_UY_NHIEM = "101";
        public static readonly string MLTDIEP_YEU_CAU_CAP_MA = "200";
        public static readonly string MLTDIEP_GUI_HOA_DON_KHONG_MA = "203";
        public static readonly string MLTDIEP_HOA_DON_SAI_SOT = "300";
        public static readonly string MLTDIEP_BTHDL = "400";

        public static string MSTTCGP_TVAN = "0301463315";
    }
}
