﻿using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace InvoiceApi.Util
{
    public class ExcelWorkbook
    {
        private XSSFWorkbook _xlsWorkbook;
        private ISheet _sheet;


        public ExcelWorkbook()
        {
            _xlsWorkbook = new XSSFWorkbook();
            _sheet = _xlsWorkbook.CreateSheet("Sheet1");
        }

        public ExcelWorkbook(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            _xlsWorkbook = new XSSFWorkbook(ms);
            ms.Close();
        }

        public ISheet GetFirstSheet()
        {
            if (_sheet == null)
            {
                _sheet = _xlsWorkbook.GetSheetAt(0);
            }

            return _sheet;
        }

        public void CreateCell(int row, int cell)
        {
            CreateCell(row, cell, CellType.String);
        }

        public void CreateCell(int row,int cell, CellType type)
        {
            IRow irow= _sheet.GetRow(row);
            irow.CreateCell(cell, type);
        }

        public void CreateRow(int row)
        {
            _sheet.CreateRow(row);
        }

        public void SetRowCellValue(int row,int cell,object value)
        {
            IRow irow = _sheet.GetRow(row);
            ICell icell = irow.Cells[cell];

            if (value is DateTime)
            {
                icell.SetCellValue((DateTime)value);
            }
            else if (value is Decimal)
            {
                icell.SetCellValue((double)value);
            }
            else
            {
                icell.SetCellValue((string)value);
            }
        }

        public byte[] DownloadExel()
        {
            byte[] result = null;

            using(MemoryStream ms=new MemoryStream()){
                _xlsWorkbook.Write(ms);

                result = ms.ToArray();
            }

            return result;
        }

        public object GetCellValueFromExcel(string cellName)
        {
            object result = null;

            ISheet sheet = GetFirstSheet();

            CellReference cellReference = new CellReference(cellName);
            IRow row = sheet.GetRow(cellReference.Row);

            if (row == null) return null;

            ICell cell = row.GetCell(cellReference.Col);

            if (cell != null)
            {
                switch (cell.CellType)
                {

                    case CellType.Boolean:
                        result = cell.BooleanCellValue;
                        break;
                    case CellType.Numeric:

                        if (DateUtil.IsCellDateFormatted(cell))
                        {
                            result = cell.DateCellValue;
                        }
                        else
                        {
                            result = cell.NumericCellValue;
                        }

                        break;
                    case CellType.Formula:
                        IFormulaEvaluator evaluator = _xlsWorkbook.GetCreationHelper().CreateFormulaEvaluator();
                        CellValue cellValue = evaluator.Evaluate(cell);

                        if (cellValue.CellType == CellType.Numeric)
                        {
                            result = cellValue.NumberValue;
                        }
                        else
                        {
                            result = cellValue.StringValue;
                        }

                        break;
                    case CellType.String:
                        result = cell.StringCellValue;
                        break;
                }


            }
            

            return result;
        }
        
        public object GetCellValueFromExcel(int row, int column)
        {
            object result = null;

            ISheet sheet = GetFirstSheet();
            IRow rowValue = sheet.GetRow(row);

            if (rowValue == null) return null;

            ICell cell = rowValue.GetCell(column);

            if (cell != null)
            {
                switch (cell.CellType)
                {

                    case CellType.Boolean:
                        result = cell.BooleanCellValue;
                        break;
                    case CellType.Numeric:

                        if (DateUtil.IsCellDateFormatted(cell))
                        {
                            result = cell.DateCellValue;
                        }
                        else
                        {
                            result = cell.NumericCellValue;
                        }

                        break;
                    case CellType.Formula:
                        IFormulaEvaluator evaluator = _xlsWorkbook.GetCreationHelper().CreateFormulaEvaluator();
                        CellValue cellValue = evaluator.Evaluate(cell);

                        if (cellValue.CellType == CellType.Numeric)
                        {
                            result = cellValue.NumberValue;
                        }
                        else
                        {
                            result = cellValue.StringValue;
                        }

                        break;
                    case CellType.String:
                        result = cell.StringCellValue;
                        break;
                }
            }
            return result;
        }

        public int GetColumnCount()
        {
            return _sheet.GetRow(0).LastCellNum;
        }

        public int GetColumnCountByRow(int row)
        {
            return _sheet.GetRow(row).LastCellNum;
        }

    }
}