﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Web;
using System.Xml;

namespace InvoiceApi.Util
{
    public class PrefixedSignedXML : SignedXml
    {
        public string Prefix { get; set; }

        public PrefixedSignedXML(XmlDocument document)
            : base(document)
        { }

        public PrefixedSignedXML(XmlElement element)
            : base(element)
        { }

        public PrefixedSignedXML()
            : base()
        { }

        internal void ComputeSignature(ISignerProvider signerProvider)
        {
            var methodInfo = typeof(SignedXml).GetMethod("BuildDigestedReferences",
                BindingFlags.Instance | BindingFlags.NonPublic);
            methodInfo.Invoke(this, null);
            SignedInfo.SignatureMethod = XmlDsigRSASHA1Url;
            // See if there is a signature description class defined in the Config file
            SignatureDescription signatureDescription =
                CryptoConfig.CreateFromName(SignedInfo.SignatureMethod) as SignatureDescription;
            if (signatureDescription == null)
                throw new CryptographicException("Cryptography_Xml_SignatureDescriptionNotCreated");

            var hashAlg = signatureDescription.CreateDigest();
            if (hashAlg == null)
                throw new CryptographicException("Cryptography_Xml_CreateHashAlgorithmFailed");

            string _prefix = this.Prefix;
            var hashvalue = this.GetC14NDigest(hashAlg, _prefix);

            m_signature.SignatureValue = signerProvider.Sign(hashvalue);
        }

        //public void ComputeSignature(string prefix)
        //{
        //    this.BuildDigestedReferences();
        //    AsymmetricAlgorithm signingKey = this.SigningKey;
        //    if (signingKey == null)
        //    {
        //        throw new CryptographicException("Cryptography_Xml_LoadKeyFailed");
        //    }
        //    if (this.SignedInfo.SignatureMethod == null)
        //    {
        //        if (!(signingKey is DSA))
        //        {
        //            if (!(signingKey is RSA))
        //            {
        //                throw new CryptographicException("Cryptography_Xml_CreatedKeyFailed");
        //            }
        //            if (this.SignedInfo.SignatureMethod == null)
        //            {
        //                this.SignedInfo.SignatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
        //            }
        //        }
        //        else
        //        {
        //            this.SignedInfo.SignatureMethod = "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
        //        }
        //    }
        //    SignatureDescription description = CryptoConfig.CreateFromName(this.SignedInfo.SignatureMethod) as SignatureDescription;
        //    if (description == null)
        //    {
        //        throw new CryptographicException("Cryptography_Xml_SignatureDescriptionNotCreated");
        //    }
        //    HashAlgorithm hash = description.CreateDigest();
        //    if (hash == null)
        //    {
        //        throw new CryptographicException("Cryptography_Xml_CreateHashAlgorithmFailed");
        //    }
        //    this.GetC14NDigest(hash, prefix);
        //    this.m_signature.SignatureValue = description.CreateFormatter(signingKey).CreateSignature(hash);
        //}

        public XmlElement GetXml(string prefix)
        {
            XmlElement e = this.GetXml();
            SetPrefix(prefix, e);
            return e;
        }

        private void BuildDigestedReferences()
        {
            Type t = typeof(SignedXml);
            MethodInfo m = t.GetMethod("BuildDigestedReferences", BindingFlags.NonPublic | BindingFlags.Instance);
            m.Invoke(this, new object[] { });
        }

        private byte[] GetC14NDigest(HashAlgorithm hash, string prefix)
        {
            XmlDocument document = new XmlDocument();
            document.PreserveWhitespace = true;
            XmlElement e = this.SignedInfo.GetXml();
            document.AppendChild(document.ImportNode(e, true));
            
            Transform canonicalizationMethodObject = this.SignedInfo.CanonicalizationMethodObject;
            SetPrefix(prefix, document.DocumentElement); 
            canonicalizationMethodObject.LoadInput(document);
            return canonicalizationMethodObject.GetDigestedOutput(hash);
        }

        private void SetPrefix(string prefix, XmlNode node)
        {
            foreach (XmlNode n in node.ChildNodes)
                SetPrefix(prefix, n);
            node.Prefix = prefix;
        }
    }
}