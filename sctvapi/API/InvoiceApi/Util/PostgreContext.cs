﻿using Npgsql;

namespace InvoiceApi.Util
{
    public class PostgreContext
    {

        public static NpgsqlConnection GetNpgsqlConnection()
        {
            return new NpgsqlConnection(CommonConstants.CONNECT_STRING?.ToString());
        }
    }
}
