﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InvoiceApi.Util
{
    public class GetCRC
    {
        public static string CalcCRC16(byte[] data)
        {
            ushort crc = 0xFFFF;
            for (int i = 0; i < data.Length; i++)
            {
                crc ^= (ushort)(data[i] << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) > 0)
                        crc = (ushort)((crc << 1) ^ 0x1021);
                    else
                        crc <<= 1;
                }
            }
            return crc.ToString("X4");
        }
        public static string GenerateCRC(byte[] message, int startindex)
        {
            string tcrc16 = "";
            const ushort poly = 0x1021;
            ushort wordcrc = 0xffff;
            ushort msb;
            int i, j;
            byte[] messagebytes = message;
            byte currentbyte;


            i = startindex - 2;
            // loop through each character of buffer updating the CRC 
            while (i < messagebytes.Length - 1)
            {
                i = ++i;
                currentbyte = messagebytes[i];
                wordcrc = (ushort)(wordcrc ^ currentbyte); //'add' the current byte to CRC in one go
                                                           // Now apply the generating polynomial to 8 bits
                for (j = 0; j < 8; ++j)
                {
                    msb = (ushort)(wordcrc & 0x8000);
                    wordcrc = (ushort)(wordcrc << 1); //shift bits left (getting rid of the the overflowed bit if necessary)

                    if (msb != 0)
                    {
                        wordcrc = (ushort)(wordcrc ^ poly); //xor crc
                    }
                }
            }

            wordcrc = (ushort)~wordcrc; //not wordcrc

            //convert CRC to hexadecimal characters
            tcrc16 = wordcrc.ToString("x");
            return tcrc16;
        }

        public static byte[] HexToBytes(string input)
        {
            byte[] result = new byte[input.Length / 2];
            for (int i = 0; i < result.Length; i++)
            {
                try
                {
                    result[i] = Convert.ToByte(input.Substring(2 * i, 2), 16);

                }
                catch (Exception ex)
                {
                }
            }
            return result;
        }

        public static class Crc16
        {
            const ushort polynomial = 0xA001;
            static readonly ushort[] table = new ushort[256];

            public static ushort ComputeChecksum(byte[] bytes)
            {
                ushort crc = 0;
                for (int i = 0; i < bytes.Length; ++i)
                {
                    byte index = (byte)(crc ^ bytes[i]);
                    crc = (ushort)((crc >> 8) ^ table[index]);
                }
                return crc;
            }

            static Crc16()
            {
                ushort value;
                ushort temp;
                for (ushort i = 0; i < table.Length; ++i)
                {
                    value = 0;
                    temp = i;
                    for (byte j = 0; j < 8; ++j)
                    {
                        if (((value ^ temp) & 0x0001) != 0)
                        {
                            value = (ushort)((value >> 1) ^ polynomial);
                        }
                        else
                        {
                            value >>= 1;
                        }
                        temp >>= 1;
                    }
                    table[i] = value;
                }
            }
        }
    }
}