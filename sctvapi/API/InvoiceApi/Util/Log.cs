﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApi.Util
{
    public class Log
    {
        private static readonly ILog _log = LogManager.GetLogger("InvoiceApi");

        public static void Error(object msg, Exception ex)
        {
            _log.Error(msg, ex);
        }

        public static void Error(object msg)
        {
            _log.Error(msg);
        }

        public static void Info(object msg, Exception ex)
        {
            _log.Info(msg, ex);
        }
        public static void Info(object msg)
        {
            _log.Info(msg);
        }

        public static void Warn(object msg, Exception ex)
        {
            _log.Warn(msg, ex);
        }

        public static void Warn(object msg)
        {
            _log.Warn(msg);
        }

        public static void Debug(object msg, Exception ex)
        {
            _log.Debug(msg, ex);
        }

        public static void Debug(object msg)
        {
            _log.Debug(msg);
        }

    }
}