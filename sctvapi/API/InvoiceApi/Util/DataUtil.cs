﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using InvoiceApi.Data;

namespace InvoiceApi.Util
{
    public class DataUtil
    {
        //public static DataTable GetDataTable(string sql, CommandType commandType, Dictionary<string, Object> parameters)
        //{
        //    // creates resulting dataset
        //    var result = new DataTable();

        //    // creates a data access context (DbContext descendant)
        //    using (var context = new InvoiceDbContext())
        //    {
        //        // creates a Command 
        //        var cmd = context.Database.Connection.CreateCommand();
        //        cmd.CommandType = commandType;
        //        cmd.CommandText = sql;

        //        if (parameters != null)
        //        {
        //            // adds all parameters
        //            foreach (var pr in parameters)
        //            {
        //                var p = cmd.CreateParameter();
        //                p.ParameterName = pr.Key;
        //                p.Value = pr.Value;
        //                cmd.Parameters.Add(p);
        //            }
        //        }

        //        try
        //        {
        //            // executes
        //            context.Database.Connection.Open();
        //            var reader = cmd.ExecuteReader();

        //            // loop through all resultsets (considering that it's possible to have more than one)
        //            do
        //            {
        //                // loads the DataTable (schema will be fetch automatically)
        //                //var tb = new DataTable();
        //                result.Load(reader);

        //            } while (!reader.IsClosed);
        //        }
        //        finally
        //        {
        //            // closes the connection
        //            context.Database.Connection.Close();
        //        }
        //    }



        //    // returns the DataSet
        //    return result;
        //}

        //public static DataSet GetDataSet(string sql, CommandType commandType, Dictionary<string, Object> parameters)
        //{
        //    // creates resulting dataset
        //    var result = new DataSet();
        //    result.DataSetName = "DataSet";

        //    string invoiceConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["InvoiceConnection"].ConnectionString;

        //    // creates a data access context (DbContext descendant)
        //    using (var context = new InvoiceDbContext(invoiceConnectionString))
        //    {
        //        // creates a Command 
        //        var cmd = context.Database.Connection.CreateCommand();
        //        cmd.CommandType = commandType;
        //        cmd.CommandText = sql;

        //        if (parameters != null)
        //        {
        //            // adds all parameters
        //            foreach (var pr in parameters)
        //            {
        //                var p = cmd.CreateParameter();
        //                p.ParameterName = pr.Key;
        //                p.Value = pr.Value;
        //                cmd.Parameters.Add(p);
        //            }
        //        }

        //        try
        //        {
        //            // executes
        //            context.Database.Connection.Open();
        //            var reader = cmd.ExecuteReader();

        //            // loop through all resultsets (considering that it's possible to have more than one)
        //            do
        //            {
        //                // loads the DataTable (schema will be fetch automatically)
        //                var tb = new DataTable();
        //                tb.Load(reader);
        //                result.Tables.Add(tb);

        //            } while (!reader.IsClosed);
        //        }
        //        finally
        //        {
        //            // closes the connection
        //            context.Database.Connection.Close();
        //        }
        //    }

        //    if (result.Tables.Count > 0)
        //    {
        //        result.Tables[0].TableName = "DataTable";
        //    }

        //    // returns the DataSet
        //    return result;
        //}
    }
}