﻿using InvoiceApi.Data;
using InvoiceApi.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace InvoiceApi.Util
{
    public class CacheManager
    {
        private string taxCodeSite;
        private RedisCacheManager redisCache;

        public CacheManager(IMinvoiceDbContext minvoiceDbContext)
        {
            this.taxCodeSite = minvoiceDbContext.GetTaxCodeSite();
            this.redisCache = new RedisCacheManager(minvoiceDbContext);
        }

        public void removeUserLogin(string loginName)
        {
            string cacheKey = string.Format(CacheKeyManager.USERNAME_TAXCODE, this.taxCodeSite, loginName);

            this.redisCache.Remove(cacheKey);
        }

        public DataTable GetUserLogin(string loginName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("username", loginName);

            string cacheKey = string.Format(CacheKeyManager.USERNAME_TAXCODE, this.taxCodeSite, loginName);

            String sql = "SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username";

            DataTable wb_user = this.redisCache.GetDataTable(sql, parameters, cacheKey, 60 * 24 * 2);

            return wb_user;
        }

        public DataTable GetParameters(string schemaName, string functionName)
        {

            string cacheKey = string.Format(CacheKeyManager.PARAM_FUNCTION, schemaName, functionName);

            String sql = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                               + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                               + "WHERE  p.proname='" + functionName + "' AND n.nspname = '" + schemaName + "'";

            DataTable tblParameter = this.redisCache.GetDataTable(sql, new Dictionary<string, object>(), cacheKey, 60 * 24 * 2);

            return tblParameter;
        }

        public DataTable GetParametersNotNul(string schemaName, string functionName)
        {

            string cacheKey = string.Format(CacheKeyManager.PARAM_FUNCTION, schemaName, functionName);

            String sql = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                               + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                               + "WHERE  p.proname='" + functionName + "' AND n.nspname = '" + schemaName + "' AND p.proargtypes IS NOT NULL";

            DataTable tblParameter = this.redisCache.GetDataTable(sql, new Dictionary<string, object>(), cacheKey, 60 * 24 * 2);

            return tblParameter;
        }

    }

}