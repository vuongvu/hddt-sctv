﻿
using InvoiceApi.Data;
using InvoiceApi.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InvoiceApi.Services
{
    public class WbLogAPIService : IWbLogAPI
    {
        private IMinvoiceDbContext _minvoiceDbContext;

        public WbLogAPIService(IMinvoiceDbContext minvoiceDbContext)
        {
            _minvoiceDbContext = minvoiceDbContext;
        }

        public async Task<string> RegisterDataInputApi(string apiName, string model)
        {
            Guid wb_log_api_id = Guid.NewGuid();
            try
            {
                String sql = "INSERT INTO #SCHEMA_NAME#.wb_log_api(api, data, wb_log_api_id) VALUES (@apiName, @data, @wb_log_api_id)";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("apiName", apiName);
                parameters.Add("data", model);
                parameters.Add("wb_log_api_id", wb_log_api_id);

                await _minvoiceDbContext.ExecuteNoneQueryAsyncNoneSQLInjection(sql, parameters);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return wb_log_api_id.ToString();
        }

        public async Task InsertWbLogAPIAsync(string response, string status, string wb_log_api_id)
        {
            try
            {
                Guid guid = new Guid(wb_log_api_id);

                string sql = "UPDATE #SCHEMA_NAME#.wb_log_api SET response=@response,status=@status WHERE wb_log_api_id=@wb_log_api_id";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("response", response);
                parameters.Add("status", status);
                parameters.Add("wb_log_api_id", guid);

                await _minvoiceDbContext.ExecuteNoneQueryAsyncNoneSQLInjection(sql, parameters);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }
    }
}