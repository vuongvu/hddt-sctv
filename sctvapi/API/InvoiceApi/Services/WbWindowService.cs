﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InvoiceApi.Data;
using InvoiceApi.Data.Domain;
using InvoiceApi.Models;
using System.Data;
using System.Threading.Tasks;
using System.Text;
using InvoiceApi.Util;

namespace InvoiceApi.Services
{
    public class WbWindowService : IWbWindowService
    {
        #region Fields

        private IMinvoiceDbContext _minvoiceDbContext;

        #endregion

        #region Ctor
        public WbWindowService(IMinvoiceDbContext minvoiceDbContext)
        {

            this._minvoiceDbContext = minvoiceDbContext;
        }

        #endregion

        #region Methods

        public async Task<DataTableExtend> GetAll()
        {
            string sql = "SELECT *,wb_window_id as id FROM #SCHEMA_NAME#.wb_window ORDER BY code";

            DataTableExtend tblData = await _minvoiceDbContext.GetDataTableAsync(sql);

            return tblData;
        }

        public async Task<JObject> GetWindows(int start, int count, string filter)
        {
            JObject json = new JObject();

            try
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("SELECT *,wb_window_id as id FROM #SCHEMA_NAME#.wb_window ");

                string where = "";

                if (filter != null && filter != "null" && filter != " ")
                {
                    JArray array = JArray.Parse(filter);

                    for (int i = 0; i < array.Count; i++)
                    {
                        JObject js = (JObject)array[i];

                        string type = js["columnType"].ToString();

                        if (!string.IsNullOrEmpty(js["value"].ToString()))
                        {
                            where += " AND " + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), type);
                        }
                    }

                    if (!string.IsNullOrEmpty(where))
                        where = " WHERE 1 = 1 " + where;
                }

                string sql = builder.ToString() + where + " ORDER BY code LIMIT " + count + " OFFSET " + start;
                DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql);

                json.Add("data", JArray.FromObject(tblData));
                json.Add("pos", start);

                if (start == 0)
                {
                    sql = "SELECT COUNT(*) as total_count FROM ( " + builder.ToString() + where + ")";
                    tblData = await this._minvoiceDbContext.GetDataTableAsync(sql);

                    json.Add("total_count", tblData.Rows[0]["total_count"].ToString());
                }

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }

        public async Task<JObject> SaveChange(JObject model)
        {
            JObject json = new JObject();

            try
            {
                string id = model["id"] == null ? Guid.NewGuid().ToString() : model["id"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("wb_window_id", Guid.Parse(id));

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_window WHERE wb_window_id=@wb_window_id";

                DataTableExtend tblData = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                tblData.TableName = "wb_window";

                if (tblData.Rows.Count == 0)
                {

                    model["id"] = id;
                    model["wb_window_id"] = id;

                    DataRow row = tblData.CreateDataRow(model);
                    sql = tblData.GenerateInsertSQL();

                    await this._minvoiceDbContext.ExecuteNoneQueryAsyncNotCheckHTML(sql, row);
                }
                else
                {
                    DataRow row = tblData.Rows[0];
                    row = tblData.ImportData(row, model);

                    sql = tblData.GenerateUpdateSQL();

                    await this._minvoiceDbContext.ExecuteNoneQueryAsyncNotCheckHTML(sql, row);
                }



                json.Add("ok", true);
                json.Add("id", id);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<JObject> Delete(string id)
        {
            JObject json = new JObject();

            try
            {
                Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                dicParameter.Add("wb_window_id", Guid.Parse(id));

                string sql = "DELETE FROM #SCHEMA_NAME#.wb_window WHERE wb_window_id=@wb_window_id";
                await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, dicParameter);

                json.Add("ok", true);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        #endregion
    }
}