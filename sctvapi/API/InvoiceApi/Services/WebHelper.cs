﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InvoiceApi.Services
{
    public partial class WebHelper : IWebHelper
    {
        private readonly HttpContextBase _context;

        public WebHelper(HttpContextBase context)
        {
            this._context = context;
        }

        public HttpRequestBase GetRequest()
        {
            return this._context.Request;
        }

        public string GetUser()
        {
            string authorization = this._context.Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorization))
            {
                return null;
            }

            string[] array = authorization.Replace("Bear", "").Trim().Split(';');

            if (array.Length == 0)
            {
                return null;
            }

            string token = array[0];
            string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
            string[] parts = key.Split(new char[] { ':' });

            return parts[1];
        }

        public string GetDvcs()
        {
            string authorization = this._context.Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorization))
            {
                return null;
            }

            string[] array = authorization.Split(';');

            if (array.Length < 2)
            {
                return null;
            }

            string ma_dvcs = array[1];

            return ma_dvcs;
        }

        public string GetLanguage()
        {
            string authorization = this._context.Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorization))
            {
                return null;
            }

            string[] array = authorization.Split(';');

            if (array.Length < 3)
            {
                return null;
            }

            string language = array[2];

            return language;
        }

        public string GetUrlHost()
        {
            //string hostname = this.ServerVariables("HTTP_HOST");

            string hostname = this.GetRequest().Url.Host;

            string scheme = this._context.Request.Url.Scheme;

            if (hostname == "::1")
            {
                hostname = "localhost";
            }

            if (hostname.IndexOf(":") >= 0)
            {
                hostname = hostname.Substring(0, hostname.IndexOf(":"));
            }

            string port = "";

            var urlReferrer = this.GetRequest().UrlReferrer;
            if (urlReferrer != null && urlReferrer.Port != 80)
            {
                port = port + ":" + urlReferrer.Port;
            }
            //
            // if (scheme == "https" && port == ":443")
            // {
            //     port = "";
            // }
            //
            if (port == ":443" || port == ":8443")
            {
                scheme = "https";
                port = "";
            }

            string url = scheme + "://" + hostname + port;

            return url;
        }

        public string GetUrlApi()
        {
            string host = this.GetUrlHost();

            if (this._context.Request.Url.AbsolutePath.StartsWith("/api"))
            {
                host = host + "/api";
            }

            return host;
        }

        private string ServerVariables(string name)
        {
            string result = string.Empty;

            try
            {
                if (this.GetRequest().ServerVariables[name] != null)
                {
                    result = this.GetRequest().ServerVariables[name];
                }
            }
            catch
            {
                result = string.Empty;
            }
            return result;
        }
    }
}