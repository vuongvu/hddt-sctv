﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface ILicenseService
    {
        Task<JObject> GetInfoLicense();
        JObject RegisterLicense(JObject model);
        JObject CheckLicense(string id);

        Task<byte[]> PrintLicenseInfo(string folder);
    }
}
