﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.Xml;
using System.Web;
using InvoiceApi.Data;
using InvoiceApi.Data.Domain;
using InvoiceApi.Models;
using Newtonsoft.Json;
using InvoiceApi.Util;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using System.Web.Script.Serialization;

using System.IO;
using Npgsql;
using System.Configuration;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using DevExpress.XtraPrinting.Native;

namespace InvoiceApi.Services
{
    public class SystemService : ISystemService
    {
        private readonly IMinvoiceDbContext _minvoiceDbContext;
        private readonly IMasterInvoiceDbContext _masterInvoiceDbContext;
        private readonly INopDbContext _nopDbContext;
        private readonly IInvoiceService _invoiceService;
        private readonly IWebHelper _webHelper;
        private readonly IRabbitMQService _rabbitMQService;
        private IEmailService _emailService;
        private readonly string CRD_INV_INVOICEAUTH_INSERT_HSM = "crd_inv_invoiceauth_insert_hsm";

        public SystemService(INopDbContext nopDbContext,
                            IInvoiceService invoiceService,
                            IWebHelper webHelper,
                            IMinvoiceDbContext minvoiceDbContext,
                            IRabbitMQService rabbitMQService,
                            IEmailService emailService,
                            IMasterInvoiceDbContext masterInvoiceDbContext
                            )
        {
            this._nopDbContext = nopDbContext;
            this._invoiceService = invoiceService;
            this._webHelper = webHelper;
            this._minvoiceDbContext = minvoiceDbContext;
            this._rabbitMQService = rabbitMQService;
            this._emailService = emailService;
            this._masterInvoiceDbContext = masterInvoiceDbContext;
        }

        public string GetMST()
        {
            DataTable dt = this._minvoiceDbContext.GetDataTable("SELECT * FROM reg.company WHERE schema_name = '#SCHEMA_NAME#'");
            try
            {
                string mst = dt.Rows[0]["taxcode"].ToString();
                return mst;
            }
            catch (Exception ex)
            {
                return "";
            }

        }
        public JArray ExecuteQuery(string sql)
        {
            DataTable tblData = this._minvoiceDbContext.GetDataTable(sql);
            JArray data = JArray.FromObject(tblData);

            return data;
        }

        public async Task<JArray> ExecuteQueryAsync(string sql)
        {
            DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql);
            JArray data = JArray.FromObject(tblData);

            return data;
        }

        public JArray GetMenuBar()
        {
            JArray data = new JArray();

            var invoiceDb = this._nopDbContext.GetInvoiceDb();
            var parents = invoiceDb.WbMenus.Where(c => c.parentId == null).ToList<wb_menu>();

            foreach (var parent in parents)
            {
                JObject obj = new JObject();
                obj.Add("id", parent.id);
                obj.Add("window_id", parent.window_id);
                obj.Add("value", parent.name);
                obj.Add("icon", parent.icon_css);
                obj.Add("details", parent.details);
                obj.Add("code", parent.code);

                GetChildMenu(parent.id, ref obj);
                data.Add(obj);
            }

            return data;
        }

        public async Task<JArray> GetMenuBarByUser(string username)
        {
            JArray data = new JArray();

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("username", username);

            DataTable tblMenu = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_menubar_by_user('#SCHEMA_NAME#',@username)", CommandType.Text, parameter);

            DataRow[] parentRows = tblMenu.Select("[parent_id] is null", "ord");

            foreach (var row in parentRows)
            {
                JObject obj = new JObject();
                obj.Add("id", row["wb_menu_id"].ToString());
                obj.Add("window_id", row["window_code"].ToString());
                obj.Add("value", row["name"].ToString());
                obj.Add("icon", row["icon_css"].ToString());
                obj.Add("details", row["details"].ToString());
                obj.Add("code", row["code"].ToString());
                obj.Add("url", row["url"].ToString());

                GetChildMenuByUser(tblMenu, row["wb_menu_id"].ToString(), ref obj);
                data.Add(obj);
            }

            return data;
        }

        private void GetChildMenuByUser(DataTable tblMenu, string parentId, ref JObject json)
        {
            DataRow[] rows = tblMenu.Select("[parent_id]='" + parentId + "'", "ord");

            JArray jarray = new JArray();

            foreach (var row in rows)
            {
                JObject obj = new JObject();
                obj.Add("id", row["wb_menu_id"].ToString());
                obj.Add("window_id", row["window_code"].ToString());
                obj.Add("value", row["name"].ToString());
                obj.Add("icon", row["icon_css"].ToString());
                obj.Add("details", row["details"].ToString());
                obj.Add("code", row["code"].ToString());
                obj.Add("url", row["url"].ToString());

                jarray.Add(obj);

                GetChildMenuByUser(tblMenu, row["wb_menu_id"].ToString(), ref obj);
            }

            if (jarray.Count > 0)
            {
                json.Add("submenu", jarray);
            }


        }
        public async Task<JArray> GetMenuBarByUser2(string username)
        {
            JArray data = new JArray();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("username", username);

            DataTable tblMenu = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_menubar_by_user('#SCHEMA_NAME#',@username)", CommandType.Text, parameters);

            DataRow[] parentRows = tblMenu.Select("[parent_id] is null", "ord");

            foreach (var row in parentRows)
            {
                JObject obj = new JObject();
                obj.Add("id", row["wb_menu_id"].ToString());
                obj.Add("open", true);
                obj.Add("window_id", row["window_code"].ToString());
                obj.Add("value", row["name"].ToString());
                obj.Add("icon", row["icon_css"].ToString());
                obj.Add("details", row["details"].ToString());
                obj.Add("code", row["code"].ToString());
                obj.Add("url", row["url"].ToString());

                GetChildMenuByUser2(tblMenu, row["wb_menu_id"].ToString(), ref obj);
                data.Add(obj);
            }

            return data;
        }
        private void GetChildMenuByUser2(DataTable tblMenu, string parentId, ref JObject json)
        {
            DataRow[] rows = tblMenu.Select("[parent_id]='" + parentId + "'", "ord");

            JArray jarray = new JArray();

            foreach (var row in rows)
            {
                JObject obj = new JObject();
                obj.Add("id", row["wb_menu_id"].ToString());
                obj.Add("window_id", row["window_code"].ToString());
                obj.Add("value", row["name"].ToString());
                obj.Add("icon", row["icon_css"].ToString());
                obj.Add("details", row["details"].ToString());
                obj.Add("code", row["code"].ToString());
                obj.Add("url", row["url"].ToString());

                jarray.Add(obj);

                GetChildMenuByUser2(tblMenu, row["wb_menu_id"].ToString(), ref obj);
            }

            if (jarray.Count > 0)
            {
                json.Add("data", jarray);
            }
        }

        private void GetChildMenu(Guid? parentId, ref JObject json)
        {
            var invoiceDb = this._nopDbContext.GetInvoiceDb();
            IList<wb_menu> menus = invoiceDb.WbMenus.Where(c => c.parentId == parentId).ToList<wb_menu>();

            JArray jarray = new JArray();

            foreach (var menu in menus)
            {
                JObject obj = new JObject();
                obj.Add("id", menu.id);
                obj.Add("window_id", menu.window_id);
                obj.Add("value", menu.name);
                obj.Add("icon", menu.icon_css);
                obj.Add("details", menu.details);
                obj.Add("code", menu.code);
                jarray.Add(obj);

                GetChildMenu(menu.id, ref obj);
            }

            if (jarray.Count > 0)
            {
                json.Add("submenu", jarray);
            }


        }


        public async Task<JArray> GetTypeSQL()
        {
            String sql = "SELECT data_type as id,data_type as value,data_type, count(*) as sl FROM information_schema.columns "
                    + "WHERE table_schema = '#SCHEMA_NAME#' GROUP BY data_type ";

            DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql);
            JArray data = JArray.FromObject(tblData);

            return data;
        }


        public async Task<string> GetConfigWinByNo(string windowNo)
        {
            Dictionary<string, object> dicPara = new Dictionary<string, object>();
            dicPara.Add("windowNo", windowNo);

            string sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_configwinbyno('#SCHEMA_NAME#',@windowNo)";
            DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);

            string result = tblData.Rows[0][0].ToString();

            return result;
        }

        public async Task<JObject> GetDataByWindowNo(string ma_dvcs, string userLogin, string window_id, int start, int count, string filter, string infoparam, string tlbparam)
        {
            JObject json = new JObject();

            try
            {
                string store_code = "";

                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("username", userLogin);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicPara);

                if (tblUser.Rows.Count == 0)
                {
                    throw new Exception("Không tìm thấy người sử dụng");
                }

                dicPara.Clear();
                dicPara.Add("window_code", window_id);

                DataTable tblMenu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_menu WHERE window_code=@window_code", CommandType.Text, dicPara);

                if (tblMenu.Rows.Count == 0)
                {
                    throw new Exception("Không tìm thấy chức năng này");
                }

                dicPara.Clear();
                dicPara.Add("username", userLogin);

                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, dicPara);

                if (tblNhomQuyen.Rows.Count == 0)
                {
                    throw new Exception("Không tìm thấy nhóm quyền của người sử dụng");
                }

                string wb_menu_id = tblMenu.Rows[0]["wb_menu_id"].ToString();

                dicPara.Clear();
                dicPara.Add("wb_role_id", Guid.Parse(tblNhomQuyen.Rows[0]["wb_role_id"].ToString()));
                dicPara.Add("wb_menu_id", Guid.Parse(wb_menu_id));

                DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id=@wb_menu_id", CommandType.Text, dicPara);

                if (tblCtQuyen.Rows.Count == 0)
                {
                    throw new Exception("Bạn không được phân quyền chức năng này");
                }

                if (tblUser.Rows.Count > 0)
                {
                    store_code = tblUser.Rows[0]["store_code"].ToString();
                }

                string isviewuser = tblUser.Rows[0]["is_view_user"].ToString();
                string wb_role_id = tblUser.Rows[0]["wb_role_id"].ToString();

                dicPara.Clear();
                dicPara.Add("code", window_id);

                DataTable tblWbWindow = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code", CommandType.Text, dicPara);

                string wb_window_id = tblWbWindow.Rows[0]["wb_window_id"].ToString();
                string voucher_code = tblWbWindow.Rows[0]["voucher_code"].ToString();

                dicPara.Clear();
                dicPara.Add("wb_window_id", Guid.Parse(wb_window_id));

                DataTable tblWbTab = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id LIMIT 1 OFFSET 0", CommandType.Text, dicPara);

                if (infoparam != null && infoparam != "null" && infoparam != " ")
                {
                    JObject js = JObject.Parse(infoparam);
                    string infowwindow_id = js["infowindow_id"].ToString();
                    JObject para = JObject.Parse(js["parameter"].ToString());

                    dicPara.Clear();
                    dicPara.Add("wb_infowindow_id", Guid.Parse(infowwindow_id));

                    var window_info = this._nopDbContext.ExecuteCmd("SELECT * FROM wb_infowindow WHERE wb_infowindow_id=@infowwindow_id", CommandType.Text, dicPara);
                    string sql = window_info.Rows[0]["SQL_CODE"].ToString();

                    string key = "start";
                    JToken value = start;
                    para.Add(key, value);

                    string key1 = "count";
                    JToken value1 = count;
                    para.Add(key1, value1);

                    if (para["ma_dvcs"] != null)
                    {
                        para["ma_dvcs"] = ma_dvcs;
                    }
                    else
                    {
                        para.Add("ma_dvcs", ma_dvcs);
                    }

                    if (para["ma_ct"] != null)
                    {
                        para["ma_ct"] = voucher_code;
                    }
                    else
                    {
                        para.Add("ma_ct", voucher_code);
                    }


                    if (tlbparam != null)
                    {
                        JArray jTlbParam = JArray.Parse(tlbparam);

                        foreach (var item in jTlbParam)
                        {
                            string columnName = item["columnName"].ToString();

                            if (para[columnName] == null)
                            {
                                para.Add(columnName, item["value"]);
                            }
                            else
                            {
                                para[columnName] = item["value"];
                            }
                        }
                    }

                    Dictionary<string, object> parameters = para.ToObject<Dictionary<string, object>>();

                    var lst = _nopDbContext.ExecuteCmd(sql, CommandType.StoredProcedure, parameters);

                    JArray data = JArray.FromObject(lst);
                    json.Add("data", data);
                    json.Add("pos", start);

                    if (start == 0)
                        json.Add("total_count", lst.Rows.Count > 0 ? lst.Rows[0]["total_count"].ToString() : "0");
                    return json;
                }
                else
                {
                    string tab_table = tblWbTab.Rows[0]["table_name"].ToString();
                    string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();
                    string order_by = tblWbTab.Rows[0]["order_by"].ToString();

                    string select = "";
                    string select_count = "SELECT COUNT(*) FROM #SCHEMA_NAME#." + tab_table;
                    string ptrang = "";
                    string txtFilter = "";

                    if (isviewuser != "C")
                    {
                        txtFilter = txtFilter + " AND user_new=N'" + userLogin + "' ";
                    }

                    if (string.IsNullOrEmpty(select_cmd))
                    {
                        select = "SELECT * FROM #SCHEMA_NAME#." + tab_table;

                        if (!string.IsNullOrEmpty(order_by))
                        {
                            ptrang = " ORDER BY " + order_by + " LIMIT " + count + " OFFSET " + start;
                        }
                        else
                            ptrang = "ORDER BY " + tab_table + "_id LIMIT " + count + " OFFSET " + start;
                    }
                    else
                    {
                        select = select_cmd;
                        select = select.Replace("#BRANCH_CODE#", ma_dvcs)
                                    .Replace("#USER_LOGIN#", userLogin)
                                    .Replace("#STORE_CODE", store_code);

                        if (!string.IsNullOrEmpty(order_by))
                        {
                            ptrang = " ORDER BY " + order_by + " LIMIT " + count + " OFFSET " + start;
                        }
                        else
                            ptrang = " LIMIT " + count + " OFFSET " + start;
                    }

                    if (filter != null && filter != "null" && filter != " ")
                    {
                        JArray array = JArray.Parse(filter);
                        JObject js;
                        for (int i = 0; i < array.Count; i++)
                        {
                            js = (JObject)array[i];
                            string type = js["columnType"].ToString();
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                            {
                                if (type == "datetime" || type == "date")
                                {
                                    JObject value = (JObject)js["value"];
                                    JToken jstart = (JToken)value["start"];
                                    JToken jend = (JToken)value["end"];

                                    if (jstart.Type != JTokenType.Null)
                                    {
                                        DateTime startTime = Convert.ToDateTime(jstart.ToString());
                                        startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime)).AddDays(-1);

                                        txtFilter += " AND " + js["columnName"] + ">='" + string.Format("{0:yyyy-MM-dd}", startTime) + "' ";
                                    }

                                    if (jend.Type != JTokenType.Null)
                                    {
                                        DateTime endTime = Convert.ToDateTime(jend.ToString());
                                        endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime)).AddDays(1);

                                        txtFilter += " AND " + js["columnName"] + "<='" + string.Format("{0:yyyy-MM-dd}", endTime) + "' ";
                                    }
                                }
                                else if (type == "uniqueidentifier" || type == "uuid")
                                {
                                    string value = js["value"].ToString();

                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        string tableAlias = js["tableAlias"] == null ? "" : js["tableAlias"].ToString() + ".";
                                        txtFilter += " AND " + tableAlias + js["columnName"] + "='" + value + "' ";
                                    }
                                }
                                else if (type == "bit" || type == "boolean")
                                {
                                    string value = js["value"].ToString();
                                    //value = value.ToLower() == "true" ? "1" : "0";

                                    string tableAlias = js["tableAlias"] == null ? "" : js["tableAlias"].ToString() + ".";
                                    txtFilter += " AND " + tableAlias + js["columnName"] + "=" + value + " ";

                                }
                                else if (type == "int" || type == "decimal" || type == "numeric")
                                {
                                    string value = js["value"].ToString();

                                    string tableAlias = js["tableAlias"] == null ? "" : js["tableAlias"].ToString() + ".";
                                    txtFilter += " AND " + tableAlias + js["columnName"] + "=" + value + " ";

                                }
                                else
                                {
                                    txtFilter += " AND lower(" + js["columnName"] + ")" + Filter.FilterGrid(js["value"].ToString(), type);
                                }
                            }
                        }
                    }

                    if (tlbparam != null && tlbparam != "null" && tlbparam != " ")
                    {
                        JArray array = JArray.Parse(tlbparam);

                        for (int i = 0; i < array.Count; i++)
                        {
                            JObject js = (JObject)array[i];

                            string value = js["value"].ToString();
                            value = value.Replace("'", "''");

                            txtFilter += " AND " + js["columnName"] + "='" + value + "' ";

                        }
                    }

                    if (!string.IsNullOrEmpty(txtFilter))
                    {
                        if (!select.ToUpper().Contains("WHERE"))
                        {
                            txtFilter = " WHERE 1 = 1 " + txtFilter;
                        }

                    }

                    string sql = select + txtFilter + ptrang;
                    var lst = await this._minvoiceDbContext.GetDataTableAsync(sql);

                    JArray data = new JArray();

                    if (lst != null)
                    {
                        data = JArray.FromObject(lst);
                    }

                    json.Add("data", data);
                    json.Add("pos", start);

                    if (start == 0)
                    {
                        string totalSql = select + txtFilter;
                        int idxFrom = totalSql.IndexOf("from", StringComparison.CurrentCultureIgnoreCase);
                        totalSql = "SELECT COUNT(*) " + totalSql.Substring(idxFrom, totalSql.Length - idxFrom);

                        var total = await this._minvoiceDbContext.GetDataTableAsync(totalSql);
                        var total_count = total == null ? 0 : Convert.ToInt32(total.Rows[0][0]);

                        json.Add("total_count", total_count);
                    }
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                json.Add("data", new JArray());
                json.Add("pos", start);

                if (start == 0)
                {
                    json.Add("total_count", 0);
                }
            }
            return json;
        }

        public async Task<JObject> GetDataById(string window_id, string id)
        {
            JObject json = new JObject();

            try
            {

                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("window_id", window_id);

                DataTable tblWbWindow = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@window_id", CommandType.Text, parameters);

                string wb_window_id = tblWbWindow.Rows[0]["wb_window_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_window_id", Guid.Parse(wb_window_id));

                DataTable tblWbTab = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id  ORDER BY ord LIMIT 1 OFFSET 0", CommandType.Text, parameters);

                string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();
                string tab_table = tblWbTab.Rows[0]["table_name"].ToString();

                string query = "";

                if (string.IsNullOrEmpty(select_cmd))
                {
                    query = "SELECT * FROM #SCHEMA_NAME#." + tab_table + " WHERE " + tab_table + "_id='" + id + "'";
                }
                else
                {
                    select_cmd = select_cmd.Replace("#BRANCH_CODE#", _webHelper.GetDvcs());

                    if (CommonManager.CheckForSqlInjection(select_cmd))
                    {
                        throw new Exception($"Dữ liệu không hợp lệ {select_cmd}");
                    }

                    query = "SELECT a.* FROM (" + select_cmd + ") a WHERE a.id='" + id + "'";

                }

                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(query);

                JArray array = JArray.FromObject(tblData);
                json = (JObject)array[0];

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }

        public async Task<JObject> GetDataByWindowNo(string ma_dvcs, string userLogin, string window_id, int start, int count, JArray filter, JObject infoparam, JArray tlbparam)
        {
            JObject json = new JObject();


            try
            {
                string store_code = "";

                Dictionary<string, object> dicParameters = new Dictionary<string, object>();

                dicParameters.Add("username", userLogin);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT store_code FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParameters);

                if (tblUser.Rows.Count > 0)
                {
                    store_code = tblUser.Rows[0]["store_code"].ToString();
                }

                dicParameters.Clear();
                dicParameters.Add("code", window_id);

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code";
                DataTable tblWbWindow = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameters);

                dicParameters.Clear();
                dicParameters.Add("wb_window_id", Guid.Parse(tblWbWindow.Rows[0]["wb_window_id"].ToString()));

                sql = "SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id ORDER BY ord";
                DataTable tblWbTab = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameters);

                if (infoparam != null)
                {
                    JObject js = infoparam;
                    string infowwindow_id = js["infowindow_id"].ToString();
                    JObject para = JObject.Parse(js["parameter"].ToString());

                    dicParameters.Clear();
                    dicParameters.Add("wb_infowindow_id", Guid.Parse(infowwindow_id));

                    var window_info = this._nopDbContext.ExecuteCmd("SELECT * FROM wb_infowindow WHERE wb_infowindow_id=@wb_infowindow_id", CommandType.Text, dicParameters);
                    sql = window_info.Rows[0]["SQL_CODE"].ToString();

                    string key = "start";
                    JToken value = start;
                    para.Add(key, value);

                    string key1 = "count";
                    JToken value1 = count;
                    para.Add(key1, value1);

                    if (para["ma_dvcs"] != null)
                    {
                        para["ma_dvcs"] = ma_dvcs;
                    }
                    else
                    {
                        para.Add("ma_dvcs", ma_dvcs);
                    }


                    if (tlbparam != null)
                    {
                        JArray jTlbParam = tlbparam;

                        foreach (var item in jTlbParam)
                        {
                            string columnName = item["columnName"].ToString();

                            if (para[columnName] == null)
                            {
                                para.Add(columnName, item["value"]);
                            }
                            else
                            {
                                para[columnName] = item["value"];
                            }
                        }



                    }

                    Dictionary<string, object> parameters = para.ToObject<Dictionary<string, object>>();

                    var lst = _nopDbContext.ExecuteCmd(sql, CommandType.StoredProcedure, parameters);

                    JArray data = JArray.FromObject(lst);
                    json.Add("data", data);
                    json.Add("pos", start);

                    if (start == 0)
                        json.Add("total_count", lst.Rows.Count > 0 ? lst.Rows[0]["total_count"].ToString() : "0");
                    return json;
                }
                else
                {
                    string tab_table = tblWbTab.Rows[0]["table_name"].ToString();
                    string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();
                    string order_by = tblWbTab.Rows[0]["order_by"].ToString();

                    string select = "";
                    string select_count = "SELECT COUNT(*) FROM #SCHEMA_NAME#." + tab_table;
                    string ptrang = "";
                    string txtFilter = "";

                    if (string.IsNullOrEmpty(select_cmd))
                    {
                        select = "SELECT * FROM #SCHEMA_NAME#." + tab_table;

                        if (!string.IsNullOrEmpty(order_by))
                        {
                            ptrang = " ORDER BY " + order_by + " LIMIT " + count + " OFFSET " + start;
                        }
                        else
                        {
                            ptrang = "ORDER BY " + tab_table + "_id LIMIT " + count + " OFFSET " + start;
                        }
                    }
                    else
                    {
                        select = select_cmd;

                        select = select.Replace("#BRANCH_CODE#", ma_dvcs)
                                        .Replace("#USER_LOGIN#", userLogin)
                                        .Replace("#STORE_CODE#", store_code);

                        if (!string.IsNullOrEmpty(order_by))
                        {
                            ptrang = " ORDER BY " + order_by + " LIMIT " + count + " OFFSET " + start;
                        }
                        else
                        {
                            ptrang = "ORDER BY " + tab_table + "_id LIMIT " + count + " OFFSET " + start;
                        }
                    }

                    if (filter != null)
                    {
                        JArray array = filter;
                        JObject js;
                        for (int i = 0; i < array.Count; i++)
                        {
                            js = (JObject)array[i];
                            string type = js["columnType"].ToString();
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                            {
                                string columnAlias = js["columnAlias"].ToString() == "" ? js["columnName"].ToString() : js["columnAlias"].ToString();

                                if (type == "datetime" || type == "date")
                                {
                                    JObject value = (JObject)js["value"];
                                    JToken jstart = (JToken)value["start"];
                                    JToken jend = (JToken)value["end"];

                                    if (jstart.Type != JTokenType.Null)
                                    {
                                        DateTime startTime = Convert.ToDateTime(jstart.ToString());
                                        startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime)).AddDays(-1);

                                        txtFilter += " AND " + columnAlias + ">='" + string.Format("{0:yyyy-MM-dd}", startTime) + "' ";
                                    }

                                    if (jend.Type != JTokenType.Null)
                                    {
                                        DateTime endTime = Convert.ToDateTime(jend.ToString());
                                        endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime)).AddDays(1);

                                        txtFilter += " AND " + columnAlias + "<='" + string.Format("{0:yyyy-MM-dd}", endTime) + "' ";
                                    }
                                }
                                else if (type == "uniqueidentifier" || type == "uuid")
                                {
                                    string value = js["value"].ToString();

                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        txtFilter += " AND " + columnAlias + "='" + value + "' ";
                                    }
                                }
                                else if (type == "integer" || type == "numeric")
                                {
                                    string value = js["value"].ToString();

                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        txtFilter += " AND " + columnAlias + "=" + value + " ";
                                    }
                                }
                                else if (type == "bit" || type == "boolean")
                                {
                                    string value = js["value"].ToString();
                                    //value = value.ToLower() == "true" ? "1" : "0";

                                    txtFilter += " AND " + columnAlias + "=" + value + " ";

                                }
                                else if (type == "int" || type == "decimal" || type == "numeric")
                                {
                                    string value = js["value"].ToString();

                                    txtFilter += " AND " + columnAlias + "=" + value + " ";

                                }
                                else
                                {
                                    txtFilter += " AND lower(" + columnAlias + ")" + Filter.FilterGrid(js["value"].ToString(), type);
                                }
                            }
                        }
                    }

                    if (tlbparam != null)
                    {
                        JArray array = tlbparam;

                        for (int i = 0; i < array.Count; i++)
                        {
                            JObject js = (JObject)array[i];

                            string value = js["value"].ToString();
                            value = value.Replace("'", "''");

                            txtFilter += " AND " + js["columnName"] + "='" + value + "' ";

                        }
                    }

                    if (!string.IsNullOrEmpty(txtFilter))
                    {
                        if (!select.ToUpper().Contains("WHERE"))
                        {
                            txtFilter = " WHERE 1 = 1 " + txtFilter;
                        }

                    }

                    sql = select + txtFilter + ptrang;
                    var lst = await this._minvoiceDbContext.GetDataTableAsync(sql);

                    JArray data = new JArray();

                    if (lst != null)
                    {
                        data = JArray.FromObject(lst);
                    }

                    json.Add("data", data);
                    json.Add("pos", start);

                    if (start == 0)
                    {
                        string totalSql = select + txtFilter;
                        int idxFrom = totalSql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase);
                        totalSql = "SELECT COUNT(*) " + totalSql.Substring(idxFrom, totalSql.Length - idxFrom);

                        var total = await this._minvoiceDbContext.GetDataTableAsync(totalSql);
                        var total_count = total == null ? 0 : Convert.ToInt32(total.Rows[0][0]);
                        json.Add("total_count", total_count);
                    }
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                json.Add("data", new JArray());
                json.Add("pos", start);

                if (start == 0)
                {
                    json.Add("total_count", 0);
                }
            }
            return json;
        }

        public async Task<JObject> DeleteMultiChoice(JObject obj, string username)
        {
            JObject json = new JObject();
            string error = "";

            try
            {
                string windowno = obj["windowid"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("username", username);

                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, parameters);

                if (tblNhomQuyen.Rows.Count > 0)
                {
                    parameters.Clear();
                    parameters.Add("wb_role_id", Guid.Parse(tblNhomQuyen.Rows[0]["wb_role_id"].ToString()));
                    parameters.Add("window_code", windowno);
                    DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id IN (SELECT wb_menu_id FROM #SCHEMA_NAME#.wb_menu WHERE window_code=@window_code)", CommandType.Text, parameters);

                    if (tblCtQuyen.Rows.Count == 0)
                    {
                        if (_webHelper.GetUser() != "ADMINISTRATOR")
                        {
                            json.Add("error", "Nhóm chưa được phân quyền");
                            return json;
                        }
                    }
                    else
                    {

                        if (tblCtQuyen.Rows[0]["has_delete"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền xóa");
                            return json;
                        }
                    }

                }

                parameters.Clear();
                parameters.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

                string isedituser = tblUser.Rows[0]["is_edit_user"].ToString();
                string isdeluser = tblUser.Rows[0]["is_del_user"].ToString();
                string store_code = tblUser.Rows[0]["store_code"].ToString();

                parameters.Clear();
                parameters.Add("code", windowno);

                DataTable tblWbWindow = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code", CommandType.Text, parameters);
                string wb_window_id = tblWbWindow.Rows[0]["wb_window_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_window_id", Guid.Parse(wb_window_id));

                DataTable tblWbTab = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id ORDER BY ord LIMIT 1 OFFSET 0", CommandType.Text, parameters);
                string tab_table = tblWbTab.Rows[0]["table_name"].ToString();

                string sql = "#SCHEMA_NAME#." + tblWbTab.Rows[0]["delete_cmd"].ToString();

                JArray array = (JArray)obj["data"];

                await _minvoiceDbContext.BeginTransactionAsync();

                for (int i = 0; i < array.Count; i++)
                {
                    string id = array[i].ToString();

                    DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#." + tab_table + " WHERE " + tab_table + "_id='" + id + "'");

                    string user_new = tblData.Rows[0]["user_new"].ToString();

                    if (username != user_new && isdeluser != "C")
                    {
                        throw new Exception("Bạn không có quyền xóa bản ghi người khác tạo");
                    }

                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, (JObject)JArray.FromObject(tblData)[0]);
                }

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                json.Add("ok", true);

            }
            catch (Exception ex)
            {
                error = ex.Message;
                json.Add("error", error);
            }

            if (error.Length > 0)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> Save(JObject obj, string username)
        {
            JObject json = new JObject();

            SqlConnection conn = null;
            SqlTransaction trans = null;

            bool isError = false;

            try
            {
                json.Add("windowid", obj["windowid"]);

                string windowno = (string)obj["windowid"];
                string editMode = (string)obj["editmode"];
                string branchCode = null;

                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("username", username);

                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, parameters);

                if (tblNhomQuyen.Rows.Count > 0)
                {
                    parameters.Clear();
                    parameters.Add("window_code", windowno);
                    parameters.Add("wb_role_id", Guid.Parse(tblNhomQuyen.Rows[0]["wb_role_id"]?.ToString()));

                    DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id IN (SELECT wb_menu_id FROM #SCHEMA_NAME#.wb_menu WHERE window_code=@window_code)", CommandType.Text, parameters);

                    if (tblCtQuyen.Rows.Count == 0)
                    {
                        if (_webHelper.GetUser() != "ADMINISTRATOR")
                        {
                            json.Add("error", "Nhóm chưa được phân quyền");
                            return json;
                        }
                    }
                    else
                    {

                        if (editMode == "1" && tblCtQuyen.Rows[0]["has_new"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền thêm mới");
                            return json;
                        }

                        if (editMode == "2" && tblCtQuyen.Rows[0]["has_edit"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền sửa");
                            return json;
                        }

                        if (editMode == "3" && tblCtQuyen.Rows[0]["has_delete"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền xóa");
                            return json;
                        }
                    }

                }



                //var invoiceDb = this._nopDbContext.GetInvoiceDb();

                //wb_window window = invoiceDb.WbWindows.Where(c => c.window_id == windowno).FirstOrDefault<wb_window>();
                //wb_tab tab = invoiceDb.WbTabs.Where(c => c.wb_window_id == window.id).OrderBy(x => x.stt).FirstOrDefault<wb_tab>();

                parameters.Clear();
                parameters.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

                string isnewuser = tblUser.Rows[0]["is_new_user"].ToString();
                string isedituser = tblUser.Rows[0]["is_edit_user"].ToString();
                string isdeluser = tblUser.Rows[0]["is_del_user"].ToString();
                string store_code = tblUser.Rows[0]["store_code"].ToString();


                if (editMode == "3" && isdeluser != "C")
                {
                    json.Add("error", "Bạn không có quyền xóa hóa đơn của người dùng khác");
                    return json;
                }

                if (editMode == "1" && isnewuser != "C")
                {
                    json.Add("error", "Bạn không có quyền thêm mới");
                    return json;
                }

                parameters.Clear();
                parameters.Add("code", windowno);

                DataTable tblWbWindow = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code", CommandType.Text, parameters);
                string wb_window_id = tblWbWindow.Rows[0]["wb_window_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_window_id", Guid.Parse(wb_window_id));

                DataTable tblWbTab = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id ORDER BY ord LIMIT 1 OFFSET 0", CommandType.Text, parameters);
                string tab_table = tblWbTab.Rows[0]["table_name"].ToString();

                var id = Guid.NewGuid();

                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    if (editMode == "1")
                    {
                        if (masterObj["branch_code"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["branch_code"].ToString()))
                            {
                                masterObj["branch_code"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                        }
                        branchCode = masterObj["branch_code"].ToString();

                        if (masterObj["user_new"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_new"].ToString()))
                            {
                                masterObj["user_new"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_new", username);
                        }

                        if (masterObj["date_new"] != null)
                        {
                            masterObj["date_new"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_new", DateTime.Now);
                        }

                        if (masterObj[tab_table + "_id"] != null)
                        {

                            if (string.IsNullOrEmpty(masterObj[tab_table + "_id"].ToString()))
                            {
                                masterObj[tab_table + "_id"] = id;
                            }
                            else
                            {

                                id = Guid.Parse(masterObj[tab_table + "_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add(tab_table + "_id", id);
                        }

                    }

                    if (editMode == "2")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isedituser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                                return json;
                            }
                        }


                        if (masterObj["user_edit"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_edit"].ToString()))
                            {
                                masterObj["user_edit"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_edit", username);
                        }

                        if (masterObj["date_edit"] != null)
                        {
                            masterObj["date_edit"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_edit", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj[tab_table + "_id"].ToString());

                    }

                    if (editMode == "3")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isdeluser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                                return json;
                            }
                        }
                    }

                    /* SqlCommand masterCommand = conn.CreateCommand();
                    masterCommand.Transaction = trans;
                    masterCommand.CommandType = CommandType.StoredProcedure;
                    masterCommand.CommandText = editMode == "1" ? tab.insert_cmd : editMode == "2" ? tab.update_cmd : tab.delete_cmd;
                    */
                    string sql = editMode == "1" ? "#SCHEMA_NAME#." + tblWbTab.Rows[0]["insert_cmd"].ToString() : editMode == "2" ? "#SCHEMA_NAME#." + tblWbTab.Rows[0]["update_cmd"].ToString() : "#SCHEMA_NAME#." + tblWbTab.Rows[0]["delete_cmd"].ToString();

                    if (editMode == "1" &&
                        tblWbTab.Rows[0]["insert_cmd"].ToString().Contains("crd_inv_invoiceauth_insert"))
                    {
                        var o = new JObject
                        {
                            {"code", masterObj["invoice_type"]}, {"invoice_series", masterObj["invoice_series"]},  {"inv_notificationdetail_id", masterObj["inv_invoicecode_id"]}
                        };
                        var check = await CheckInvoiceCancel(o);
                        if (!check)
                        {
                            json.Add("error", $"Dải hóa đơn {masterObj["invoice_type"]} đã hủy bỏ!");
                            return json;
                        }

                    }

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }


                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            string tab_id = (string)dtl["tab_id"];
                            string table_name = (string)dtl["tab_table"];

                            parameters.Clear();
                            parameters.Add("code", tab_id);

                            DataTable tblTabDetail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE code=@code", CommandType.Text, parameters);

                            parameters.Clear();
                            parameters.Add("wb_tab_id", Guid.Parse(tblTabDetail.Rows[0]["wb_tab_id"].ToString()));

                            DataTable tblFields = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_field WHERE wb_tab_id=@wb_tab_id", CommandType.Text, parameters);

                            string foreign_key = tblTabDetail.Rows[0]["foreign_key"].ToString();
                            string prefix = tblTabDetail.Rows[0]["prefix"].ToString();

                            var fieldStt_rec0 = tblFields.Select("column_name='row_ord'");

                            JArray obj_details = (JArray)dtl["data"];

                            sql = "DELETE FROM #SCHEMA_NAME#." + table_name + " WHERE " + foreign_key + "='" + masterObj[tab_table + "_id"] + "'";

                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details[foreign_key] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[foreign_key].ToString()))
                                    {
                                        js_details[foreign_key] = masterObj[tab_table + "_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add(foreign_key, masterObj[tab_table + "_id"]);
                                }

                                if (js_details[table_name + "_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[table_name + "_id"].ToString()))
                                    {
                                        js_details[table_name + "_id"] = Guid.NewGuid();
                                    }
                                }
                                else
                                {
                                    js_details.Add(table_name + "_id", Guid.NewGuid());
                                }

                                if (fieldStt_rec0 != null)
                                {
                                    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                }

                                if (js_details["date_new"] != null)
                                {
                                    js_details["date_new"] = DateTime.Now;
                                }
                                else
                                {
                                    js_details.Add("date_new", DateTime.Now);
                                }

                                if (js_details["user_new"] != null)
                                {
                                    js_details["user_new"] = username;
                                }
                                else
                                {
                                    js_details.Add("user_new", username);
                                }

                                if (js_details["branch_code"] != null)
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details["branch_code"] = branch_code;
                                }
                                else
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details.Add("branch_code", branch_code);
                                }


                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#." + tblTabDetail.Rows[0]["insert_cmd"].ToString(), CommandType.StoredProcedure, js_details);
                            }
                        }

                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    //if (tab_table == "inv_invoiceauth" && editMode == "1")
                    //{
                    //    DataTable tblEmailQueue = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_email_queue_id FROM #SCHEMA_NAME#.wb_email_queue WHERE inv_invoiceauth_id='" + id + "' AND is_send=false");

                    //    if (tblEmailQueue.Rows.Count > 0)
                    //    {
                    //        string schemaName = await _minvoiceDbContext.GetSchemaNameAsync();

                    //        JObject rabbitObj = new JObject();
                    //        rabbitObj.Add("SCHEMA_NAME", schemaName);

                    //        JArray jarray = JArray.FromObject(tblEmailQueue);
                    //        rabbitObj.Add("data", jarray);

                    //        _rabbitMQService.PublishQueue("notify_email_signer_queue", rabbitObj.ToString());
                    //    }
                    //}
                }


                if (editMode != "3")
                {
                    string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();

                    if (select_cmd.Length > 0)
                    {
                        if (select_cmd.ToUpper().Contains("WHERE"))
                        {
                            select_cmd = select_cmd + " AND " + tab_table + "_id='" + id + "'";
                        }
                        else
                        {
                            select_cmd = select_cmd + " WHERE " + tab_table + "_id='" + id + "'";
                        }

                        select_cmd = select_cmd.Replace("#BRANCH_CODE#", branchCode != null ? branchCode : _webHelper.GetDvcs())
                                                .Replace("#USER_LOGIN#", _webHelper.GetUser())
                                                .Replace("#STORE_CODE#", store_code);


                        DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);

                        data = JArray.FromObject(tbl);
                        json.Add("data", data[0]);

                    }
                    else
                    {
                        parameters.Clear();
                        parameters.Add("id", id);
                        DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync("SELECT *," + tab_table + "_id as id FROM " + tab_table + " WHERE " + tab_table + "_id=@id", CommandType.Text, parameters);
                        data = JArray.FromObject(tbl);
                        json.Add("data", data[0]);

                    }
                }

                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }


        public async Task<JObject> SaveAndCreateNumber(JObject obj, string username)
        {
            JObject json = new JObject();

            SqlConnection conn = null;
            SqlTransaction trans = null;

            bool isError = false;

            try
            {
                json.Add("windowid", obj["windowid"]);

                string windowno = (string)obj["windowid"];
                string editMode = (string)obj["editmode"];
                var param = new Dictionary<string, object> { { "username", username } };

                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, param);

                if (tblNhomQuyen.Rows.Count > 0)
                {
                    param.Clear();
                    param.Add("wb_role_id", tblNhomQuyen.Rows[0]["wb_role_id"]);
                    param.Add("window_code", windowno);

                    DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id IN (SELECT wb_menu_id FROM #SCHEMA_NAME#.wb_menu WHERE window_code=@window_code)", CommandType.Text, param);

                    if (tblCtQuyen.Rows.Count == 0)
                    {
                        if (_webHelper.GetUser() != "ADMINISTRATOR")
                        {
                            json.Add("error", "Nhóm chưa được phân quyền");
                            return json;
                        }
                    }
                    else
                    {

                        if (editMode == "1" && tblCtQuyen.Rows[0]["has_new"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền thêm mới");
                            return json;
                        }

                        if (editMode == "2" && tblCtQuyen.Rows[0]["has_edit"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền sửa");
                            return json;
                        }

                        if (editMode == "3" && tblCtQuyen.Rows[0]["has_delete"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền xóa");
                            return json;
                        }
                    }

                }



                //var invoiceDb = this._nopDbContext.GetInvoiceDb();

                //wb_window window = invoiceDb.WbWindows.Where(c => c.window_id == windowno).FirstOrDefault<wb_window>();
                //wb_tab tab = invoiceDb.WbTabs.Where(c => c.wb_window_id == window.id).OrderBy(x => x.stt).FirstOrDefault<wb_tab>();
                param.Clear();
                param.Add("username", username);
                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, param);

                string isnewuser = tblUser.Rows[0]["is_new_user"].ToString();
                string isedituser = tblUser.Rows[0]["is_edit_user"].ToString();
                string isdeluser = tblUser.Rows[0]["is_del_user"].ToString();
                string store_code = tblUser.Rows[0]["store_code"].ToString();


                if (editMode == "3" && isdeluser != "C")
                {
                    json.Add("error", "Bạn không có quyền xóa hóa đơn của người dùng khác");
                    return json;
                }

                if (editMode == "1" && isnewuser != "C")
                {
                    json.Add("error", "Bạn không có quyền thêm mới");
                    return json;
                }

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("code", windowno);

                DataTable tblWbWindow = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code", CommandType.Text, dic);
                dic.Clear();
                dic.Add("wb_window_id", tblWbWindow.Rows[0]["wb_window_id"]);
                DataTable tblWbTab = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id ORDER BY ord LIMIT 1 OFFSET 0", CommandType.Text, dic);
                string tab_table = tblWbTab.Rows[0]["table_name"].ToString();


                var id = Guid.NewGuid();

                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    if (editMode == "1")
                    {
                        if (masterObj["branch_code"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["branch_code"].ToString()))
                            {
                                masterObj["branch_code"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                        }

                        if (masterObj["user_new"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_new"].ToString()))
                            {
                                masterObj["user_new"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_new", username);
                        }

                        if (masterObj["date_new"] != null)
                        {
                            masterObj["date_new"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_new", DateTime.Now);
                        }

                        if (masterObj[tab_table + "_id"] != null)
                        {

                            if (string.IsNullOrEmpty(masterObj[tab_table + "_id"].ToString()))
                            {
                                masterObj[tab_table + "_id"] = id;
                            }
                            else
                            {

                                id = Guid.Parse(masterObj[tab_table + "_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add(tab_table + "_id", id);
                        }

                    }

                    if (editMode == "2")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isedituser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                                return json;
                            }
                        }


                        if (masterObj["user_edit"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_edit"].ToString()))
                            {
                                masterObj["user_edit"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_edit", username);
                        }

                        if (masterObj["date_edit"] != null)
                        {
                            masterObj["date_edit"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_edit", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj[tab_table + "_id"].ToString());

                    }

                    if (editMode == "3")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isdeluser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                                return json;
                            }
                        }
                    }

                    /* SqlCommand masterCommand = conn.CreateCommand();
                    masterCommand.Transaction = trans;
                    masterCommand.CommandType = CommandType.StoredProcedure;
                    masterCommand.CommandText = editMode == "1" ? tab.insert_cmd : editMode == "2" ? tab.update_cmd : tab.delete_cmd;
                    */
                    string sql = editMode == "1" ? "#SCHEMA_NAME#." + tblWbTab.Rows[0]["insert_cmd"].ToString() : editMode == "2" ? "#SCHEMA_NAME#." + tblWbTab.Rows[0]["update_cmd"].ToString() : "#SCHEMA_NAME#." + tblWbTab.Rows[0]["delete_cmd"].ToString();

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }


                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            string tab_id = (string)dtl["tab_id"];
                            string table_name = (string)dtl["tab_table"];
                            param = new Dictionary<string, object> { { "code", tab_id } };
                            DataTable tblTabDetail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE code=@code", CommandType.Text, param);
                            param = new Dictionary<string, object> { { "wb_tab_id", tblTabDetail.Rows[0]["wb_tab_id"] } };
                            DataTable tblFields = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_field WHERE wb_tab_id=@wb_tab_id", CommandType.Text, param);

                            string foreign_key = tblTabDetail.Rows[0]["foreign_key"].ToString();
                            string prefix = tblTabDetail.Rows[0]["prefix"].ToString();

                            var fieldStt_rec0 = tblFields.Select("column_name='row_ord'");

                            JArray obj_details = (JArray)dtl["data"];

                            sql = "DELETE FROM #SCHEMA_NAME#." + table_name + " WHERE " + foreign_key + "='" + masterObj[tab_table + "_id"] + "'";

                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details[foreign_key] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[foreign_key].ToString()))
                                    {
                                        js_details[foreign_key] = masterObj[tab_table + "_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add(foreign_key, masterObj[tab_table + "_id"]);
                                }

                                if (js_details[table_name + "_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[table_name + "_id"].ToString()))
                                    {
                                        js_details[table_name + "_id"] = Guid.NewGuid();
                                    }
                                }
                                else
                                {
                                    js_details.Add(table_name + "_id", Guid.NewGuid());
                                }

                                if (fieldStt_rec0 != null)
                                {
                                    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                }

                                if (js_details["date_new"] != null)
                                {
                                    js_details["date_new"] = DateTime.Now;
                                }
                                else
                                {
                                    js_details.Add("date_new", DateTime.Now);
                                }

                                if (js_details["user_new"] != null)
                                {
                                    js_details["user_new"] = username;
                                }
                                else
                                {
                                    js_details.Add("user_new", username);
                                }

                                if (js_details["branch_code"] != null)
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details["branch_code"] = branch_code;
                                }
                                else
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details.Add("branch_code", branch_code);
                                }

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#." + tblTabDetail.Rows[0]["insert_cmd"].ToString(), CommandType.StoredProcedure, js_details);
                            }
                        }

                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }

                string inv_invoiceauth_id = "";
                string security_number = "";

                if (editMode != "3")
                {
                    string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();

                    if (select_cmd.Length > 0)
                    {
                        if (select_cmd.ToUpper().Contains("WHERE"))
                        {
                            select_cmd = select_cmd + " AND " + tab_table + "_id='" + id + "'";
                        }
                        else
                        {
                            select_cmd = select_cmd + " WHERE " + tab_table + "_id='" + id + "'";
                        }

                        select_cmd = select_cmd.Replace("#BRANCH_CODE#", _webHelper.GetDvcs())
                                                .Replace("#USER_LOGIN#", _webHelper.GetUser())
                                                .Replace("#STORE_CODE#", store_code);


                        DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);

                        data = JArray.FromObject(tbl);
                        json.Add("data", data[0]);
                        inv_invoiceauth_id = tbl.Rows[0]["inv_invoiceauth_id"].ToString();
                        security_number = tbl.Rows[0]["security_number"].ToString();

                    }
                    else
                    {
                        DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync("SELECT *," + tab_table + "_id as id FROM " + tab_table + " WHERE " + tab_table + "_id='" + id + "'");
                        data = JArray.FromObject(tbl);
                        json.Add("data", data[0]);
                        inv_invoiceauth_id = tbl.Rows[0]["inv_invoiceauth_id"].ToString();
                        security_number = tbl.Rows[0]["security_number"].ToString();
                    }
                }

                if (security_number == null || "".Equals(security_number))
                {
                    try
                    {
                        string dvcs = _webHelper.GetDvcs();

                        dic.Clear();
                        dic.Add("branch_code", dvcs);

                        DataTable tblInvoiceProcess = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoice_process WHERE branch_code=@branch_code ORDER BY ord", CommandType.Text, dic);
                        int i = 0;

                        for (int j = 0; j < tblInvoiceProcess.Rows.Count; j++)
                        {
                            DataRow row = tblInvoiceProcess.Rows[j];
                            string code = row["code"].ToString();

                            if (code == "02")
                            {
                                i = j;
                                break;
                            }
                        }

                        string status = "";

                        if (tblInvoiceProcess.Rows.Count == 0)
                        {
                            status = "Chờ ký";
                        }
                        else if (tblInvoiceProcess.Rows.Count == 1)
                        {
                            status = tblInvoiceProcess.Rows[0]["name"].ToString();
                        }
                        else
                        {
                            status = tblInvoiceProcess.Rows[i + 1]["name"].ToString();
                        }

                        string sql = "SELECT #SCHEMA_NAME#.qr_get_invoice_number('#SCHEMA_NAME#',@inv_invoiceauth_id,@username,@status)";

                        Dictionary<string, object> parameters = new Dictionary<string, object>();

                        parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                        parameters.Add("username", username);
                        parameters.Add("status", status);

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                        if (editMode != "3")
                        {
                            string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();

                            if (select_cmd.Length > 0)
                            {
                                if (select_cmd.ToUpper().Contains("WHERE"))
                                {
                                    select_cmd = select_cmd + " AND " + tab_table + "_id='" + id + "'";
                                }
                                else
                                {
                                    select_cmd = select_cmd + " WHERE " + tab_table + "_id='" + id + "'";
                                }

                                select_cmd = select_cmd.Replace("#BRANCH_CODE#", _webHelper.GetDvcs())
                                                        .Replace("#USER_LOGIN#", _webHelper.GetUser())
                                                        .Replace("#STORE_CODE#", store_code);

                                DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);

                                data = JArray.FromObject(tbl);
                                json.Remove("data");
                                json.Add("data", data[0]);

                            }
                            else
                            {
                                DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync("SELECT *," + tab_table + "_id as id FROM " + tab_table + " WHERE " + tab_table + "_id='" + id + "'");
                                data = JArray.FromObject(tbl);
                                json.Remove("data");
                                json.Add("data", data[0]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        json.Add("error", ex.Message);
                        return json;
                    }
                }
                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> SaveSignXMLEasyCA(JObject obj, string username)
        {
            JObject json = new JObject();

            SqlConnection conn = null;
            SqlTransaction trans = null;

            bool isError = false;

            try
            {
                json.Add("windowid", obj["windowid"]);


                string windowno = (string)obj["windowid"];
                string editMode = (string)obj["editmode"];

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("username", username);

                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, dic);

                if (tblNhomQuyen.Rows.Count > 0)
                {
                    Dictionary<string, object> parameter1s = new Dictionary<string, object>();
                    parameter1s.Add("window_code", windowno);
                    parameter1s.Add("wb_role_id", Guid.Parse(tblNhomQuyen.Rows[0]["wb_role_id"].ToString()));

                    DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id IN (SELECT wb_menu_id FROM #SCHEMA_NAME#.wb_menu WHERE window_code=@window_code)", CommandType.Text, parameter1s);

                    if (tblCtQuyen.Rows.Count == 0)
                    {
                        if (_webHelper.GetUser() != "ADMINISTRATOR")
                        {
                            json.Add("error", "Nhóm chưa được phân quyền");
                            return json;
                        }
                    }
                    else
                    {

                        if (editMode == "1" && tblCtQuyen.Rows[0]["has_new"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền thêm mới");
                            return json;
                        }

                        if (editMode == "2" && tblCtQuyen.Rows[0]["has_edit"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền sửa");
                            return json;
                        }

                        if (editMode == "3" && tblCtQuyen.Rows[0]["has_delete"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền xóa");
                            return json;
                        }
                    }

                }

                //var invoiceDb = this._nopDbContext.GetInvoiceDb();

                //wb_window window = invoiceDb.WbWindows.Where(c => c.window_id == windowno).FirstOrDefault<wb_window>();
                //wb_tab tab = invoiceDb.WbTabs.Where(c => c.wb_window_id == window.id).OrderBy(x => x.stt).FirstOrDefault<wb_tab>();

                dic.Clear();
                dic.Add("username", username);
                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dic);

                string isnewuser = tblUser.Rows[0]["is_new_user"].ToString();
                string isedituser = tblUser.Rows[0]["is_edit_user"].ToString();
                string isdeluser = tblUser.Rows[0]["is_del_user"].ToString();
                string store_code = tblUser.Rows[0]["store_code"].ToString();


                if (editMode == "3" && isdeluser != "C")
                {
                    json.Add("error", "Bạn không có quyền xóa hóa đơn của người dùng khác");
                    return json;
                }

                if (editMode == "1" && isnewuser != "C")
                {
                    json.Add("error", "Bạn không có quyền thêm mới");
                    return json;
                }

                dic.Clear();
                dic.Add("code", windowno);

                DataTable tblWbWindow = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code", CommandType.Text, dic);
                string wb_window_id = tblWbWindow.Rows[0]["wb_window_id"].ToString();

                dic.Clear();
                dic.Add("wb_window_id", Guid.Parse(wb_window_id));

                DataTable tblWbTab = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id ORDER BY ord LIMIT 1 OFFSET 0", CommandType.Text, dic);
                string tab_table = tblWbTab.Rows[0]["table_name"].ToString();


                var id = Guid.NewGuid();

                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {


                    JObject masterObj = (JObject)data[i];
                    dic = new Dictionary<string, object> { { "transaction_id", masterObj["transaction_id"] } };
                    dic.Add("invoice_series", masterObj["invoice_series"]);
                    dic.Add("invoice_type", masterObj["invoice_type"]);
                    dic.Add("branch_code", masterObj["branch_code"]);

                    var sql_transaction = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE transaction_id=@transaction_id AND invoice_series=@invoice_series AND invoice_type = @invoice_type AND branch_code = @branch_code";

                    DataTable dataTransactionId = await _minvoiceDbContext.GetDataTableAsync(sql_transaction, CommandType.Text, dic);
                    if (dataTransactionId.Rows.Count > 0)
                    {
                        json.Add("error", "Giao dịch đã tồn tại");
                        json.Add("transaction_id", masterObj["transaction_id"]);
                        return json;
                    }

                    if (editMode == "1")
                    {
                        if (masterObj["branch_code"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["branch_code"].ToString()))
                            {
                                masterObj["branch_code"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                        }

                        if (masterObj["user_new"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_new"].ToString()))
                            {
                                masterObj["user_new"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_new", username);
                        }

                        if (masterObj["date_new"] != null)
                        {
                            masterObj["date_new"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_new", DateTime.Now);
                        }

                        if (masterObj[tab_table + "_id"] != null)
                        {

                            if (string.IsNullOrEmpty(masterObj[tab_table + "_id"].ToString()))
                            {
                                masterObj[tab_table + "_id"] = id;
                            }
                            else
                            {

                                id = Guid.Parse(masterObj[tab_table + "_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add(tab_table + "_id", id);
                        }

                    }

                    if (editMode == "2")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isedituser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                                return json;
                            }
                        }


                        if (masterObj["user_edit"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_edit"].ToString()))
                            {
                                masterObj["user_edit"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_edit", username);
                        }

                        if (masterObj["date_edit"] != null)
                        {
                            masterObj["date_edit"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_edit", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj[tab_table + "_id"].ToString());

                    }

                    if (editMode == "3")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isdeluser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                                return json;
                            }
                        }
                    }




                    /* SqlCommand masterCommand = conn.CreateCommand();
                    masterCommand.Transaction = trans;
                    masterCommand.CommandType = CommandType.StoredProcedure;
                    masterCommand.CommandText = editMode == "1" ? tab.insert_cmd : editMode == "2" ? tab.update_cmd : tab.delete_cmd;
                    */
                    string sql = editMode == "1" ? "#SCHEMA_NAME#." + CRD_INV_INVOICEAUTH_INSERT_HSM : editMode == "2" ? "#SCHEMA_NAME#." + tblWbTab.Rows[0]["update_cmd"].ToString() : "#SCHEMA_NAME#." + tblWbTab.Rows[0]["delete_cmd"].ToString();

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }


                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            string tab_id = (string)dtl["tab_id"];
                            string table_name = (string)dtl["tab_table"];
                            dic = new Dictionary<string, object> { { "code", tab_id } };
                            DataTable tblTabDetail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE code=@code", CommandType.Text, dic);

                            dic = new Dictionary<string, object> { { "wb_tab_id", tblTabDetail.Rows[0]["wb_tab_id"] } };
                            DataTable tblFields = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_field WHERE wb_tab_id=@wb_tab_id", CommandType.Text, dic);

                            string foreign_key = tblTabDetail.Rows[0]["foreign_key"].ToString();
                            string prefix = tblTabDetail.Rows[0]["prefix"].ToString();

                            var fieldStt_rec0 = tblFields.Select("column_name='row_ord'");

                            JArray obj_details = (JArray)dtl["data"];

                            dic = new Dictionary<string, object> { { "tab_table_id", masterObj[tab_table + "_id"] } };
                            sql = "DELETE FROM #SCHEMA_NAME#." + table_name + " WHERE " + foreign_key + "=@tab_table_id";

                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dic);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details[foreign_key] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[foreign_key].ToString()))
                                    {
                                        js_details[foreign_key] = masterObj[tab_table + "_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add(foreign_key, masterObj[tab_table + "_id"]);
                                }

                                if (js_details[table_name + "_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[table_name + "_id"].ToString()))
                                    {
                                        js_details[table_name + "_id"] = Guid.NewGuid();
                                    }
                                }
                                else
                                {
                                    js_details.Add(table_name + "_id", Guid.NewGuid());
                                }

                                if (fieldStt_rec0 != null)
                                {
                                    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                }

                                if (js_details["date_new"] != null)
                                {
                                    js_details["date_new"] = DateTime.Now;
                                }
                                else
                                {
                                    js_details.Add("date_new", DateTime.Now);
                                }

                                if (js_details["user_new"] != null)
                                {
                                    js_details["user_new"] = username;
                                }
                                else
                                {
                                    js_details.Add("user_new", username);
                                }

                                if (js_details["branch_code"] != null)
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details["branch_code"] = branch_code;
                                }
                                else
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details.Add("branch_code", branch_code);
                                }

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#." + tblTabDetail.Rows[0]["insert_cmd"].ToString(), CommandType.StoredProcedure, js_details);
                            }
                        }

                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    //---------------------- Sinh số hóa đơn ---------------
                    string dvcs = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                    dic = new Dictionary<string, object> { { "branch_code", dvcs } };

                    DataTable tblInvoiceProcess = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoice_process WHERE branch_code=@branch_code ORDER BY ord", CommandType.Text, dic);
                    int index = 0;

                    for (int j = 0; j < tblInvoiceProcess.Rows.Count; j++)
                    {
                        DataRow row = tblInvoiceProcess.Rows[j];
                        string code = row["code"].ToString();
                        if (code == "02")
                        {
                            index = j;
                            break;
                        }
                    }

                    string status = "";

                    if (tblInvoiceProcess.Rows.Count == 0)
                    {
                        status = "Chờ ký";
                    }
                    else if (tblInvoiceProcess.Rows.Count == 1)
                    {
                        status = tblInvoiceProcess.Rows[0]["name"].ToString();
                    }
                    else
                    {
                        status = tblInvoiceProcess.Rows[i + 1]["name"].ToString();
                    }

                    string sql_get_invoice_number = "SELECT #SCHEMA_NAME#.qr_get_invoice_number('#SCHEMA_NAME#',@inv_invoiceauth_id,@username,@status)";
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        { "inv_invoiceauth_id", Guid.Parse(id.ToString()) },
                        { "username", username },
                        { "status", status }
                    };

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_get_invoice_number, parameters);

                    //---------------------- Sinh số hóa đơn ---------------

                    //---------------------- SignXMLEasyCA ---------------
                    var ma_dvcs = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                    var cer_serialData = await _invoiceService.GetListCertificates(username, ma_dvcs);
                    var cer_serial = cer_serialData[0]["cer_serial"].ToString();
                    var inputKyHoaDon = new JObject()
                    {
                        { "branch_code",  ma_dvcs},
                        { "username", username},
                        { "inv_invoiceauth_id", id},
                        { "cer_serial", cer_serial},

                    };
                    json = await _invoiceService.SignXmlEasyCA(inputKyHoaDon);
                    dic = new Dictionary<string, object> { { "inv_invoiceauth_id", id } };
                    dic.Add("invoice_series", masterObj["invoice_series"]);
                    dic.Add("invoice_type", masterObj["invoice_type"]);
                    dic.Add("branch_code", masterObj["branch_code"]);

                    var sqlInvoiceNumber = "SELECT invoice_number FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id" +
                        " AND invoice_series = @invoice_series AND invoice_type = @invoice_type AND branch_code = @branch_code";

                    DataTable dataInvoiceNumber = await _minvoiceDbContext.GetDataTableAsync(sqlInvoiceNumber, CommandType.Text, dic);
                    if (json["error"] != null)
                    {
                        isError = true;
                        json["error"] = "Hoá đơn đã được tạo và đang ở trạng thái " + status + ". " + json["error"];
                        json.Add("invoice_number", dataInvoiceNumber.Rows[0]["invoice_number"].ToString());
                    }
                    //---------------------- SignXMLEasyCA ---------------

                    //if (tab_table == "inv_invoiceauth" && editMode == "1")
                    //{
                    //    DataTable tblEmailQueue = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_email_queue_id FROM #SCHEMA_NAME#.wb_email_queue WHERE inv_invoiceauth_id='" + id + "' AND is_send=false");
                    //    // LỖI Ở ĐÂY
                    //    if (tblEmailQueue.Rows.Count > 0)
                    //    {
                    //        string schemaName = await _minvoiceDbContext.GetSchemaNameAsync();

                    //        JObject rabbitObj = new JObject();
                    //        rabbitObj.Add("SCHEMA_NAME", schemaName);

                    //        JArray jarray = JArray.FromObject(tblEmailQueue);
                    //        rabbitObj.Add("data", jarray);

                    //        _rabbitMQService.PublishQueue("notify_email_signer_queue", rabbitObj.ToString());
                    //    }
                    //}
                }


                if (editMode != "3")
                {
                    string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();
                    dic = new Dictionary<string, object> { { "id", id } };
                    if (select_cmd.Length > 0)
                    {
                        if (select_cmd.ToUpper().Contains("WHERE"))
                        {
                            select_cmd = select_cmd + " AND " + tab_table + "_id=@id";
                        }
                        else
                        {
                            select_cmd = select_cmd + " WHERE " + tab_table + "_id=@id";
                        }

                        select_cmd = select_cmd.Replace("#BRANCH_CODE#", _webHelper.GetDvcs())
                                                .Replace("#USER_LOGIN#", _webHelper.GetUser())
                                                .Replace("#STORE_CODE#", store_code);


                        DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd, CommandType.Text, dic);

                    }
                    else
                    {
                        dic = new Dictionary<string, object> { { "id", id } };
                        DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync("SELECT *," + tab_table + "_id as id FROM " + tab_table + " WHERE " + tab_table + "_id=@id", CommandType.Text, dic);
                    }
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> SaveSignInvoiceCertFile(JObject obj, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                var dic = new Dictionary<string, object>();
                json.Add("windowid", obj["windowid"]);


                string windowno = (string)obj["windowid"];
                string editMode = (string)obj["editmode"];

                dic.Add("username", username);
                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, dic);

                if (tblNhomQuyen.Rows.Count > 0)
                {

                    dic = new Dictionary<string, object> { { "wb_role_id", Guid.Parse(tblNhomQuyen.Rows[0]["wb_role_id"].ToString()) } };
                    dic.Add("window_code", windowno);
                    DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id IN (SELECT wb_menu_id FROM #SCHEMA_NAME#.wb_menu WHERE window_code=@window_code)", CommandType.Text, dic);

                    if (tblCtQuyen.Rows.Count == 0)
                    {
                        if (_webHelper.GetUser() != "ADMINISTRATOR")
                        {
                            json.Add("error", "Nhóm chưa được phân quyền");
                            return json;
                        }
                    }
                    else
                    {

                        if (editMode == "1" && tblCtQuyen.Rows[0]["has_new"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền thêm mới");
                            return json;
                        }

                        if (editMode == "2" && tblCtQuyen.Rows[0]["has_edit"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền sửa");
                            return json;
                        }

                        if (editMode == "3" && tblCtQuyen.Rows[0]["has_delete"].ToString() != "C")
                        {
                            json.Add("error", "Bạn không có quyền xóa");
                            return json;
                        }
                    }
                }

                dic.Clear();
                dic.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dic);

                string isnewuser = tblUser.Rows[0]["is_new_user"].ToString();
                string isedituser = tblUser.Rows[0]["is_edit_user"].ToString();
                string isdeluser = tblUser.Rows[0]["is_del_user"].ToString();
                string store_code = tblUser.Rows[0]["store_code"].ToString();


                if (editMode == "3" && isdeluser != "C")
                {
                    json.Add("error", "Bạn không có quyền xóa hóa đơn của người dùng khác");
                    return json;
                }

                if (editMode == "1" && isnewuser != "C")
                {
                    json.Add("error", "Bạn không có quyền thêm mới");
                    return json;
                }

                dic = new Dictionary<string, object> { { "code", windowno } };

                DataTable tblWbWindow = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code", CommandType.Text, dic);
                dic = new Dictionary<string, object> { { "wb_window_id", tblWbWindow.Rows[0]["wb_window_id"] } };
                DataTable tblWbTab = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id ORDER BY ord LIMIT 1 OFFSET 0", CommandType.Text, dic);
                string tab_table = tblWbTab.Rows[0]["table_name"].ToString();


                var id = Guid.NewGuid();

                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {


                    JObject masterObj = (JObject)data[i];
                    dic = new Dictionary<string, object> { { "transaction_id", masterObj["transaction_id"] } };
                    dic.Add("invoice_series", masterObj["invoice_series"]);
                    dic.Add("invoice_type", masterObj["invoice_type"]);
                    dic.Add("branch_code", masterObj["branch_code"]);

                    var sql_transaction = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE transaction_id=@transaction_id " +
                        "AND invoice_series=@invoice_series AND invoice_type = @invoice_type AND branch_code = @branch_code";
                    DataTable dataTransactionId = await _minvoiceDbContext.GetDataTableAsync(sql_transaction, CommandType.Text, dic);
                    if (dataTransactionId.Rows.Count > 0)
                    {
                        json.Add("error", "Giao dịch đã tồn tại");
                        json.Add("transaction_id", masterObj["transaction_id"]);
                        return json;
                    }
                    if (editMode == "1")
                    {
                        if (masterObj["branch_code"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["branch_code"].ToString()))
                            {
                                masterObj["branch_code"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                        }

                        if (masterObj["user_new"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_new"].ToString()))
                            {
                                masterObj["user_new"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_new", username);
                        }

                        if (masterObj["date_new"] != null)
                        {
                            masterObj["date_new"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_new", DateTime.Now);
                        }

                        if (masterObj[tab_table + "_id"] != null)
                        {

                            if (string.IsNullOrEmpty(masterObj[tab_table + "_id"].ToString()))
                            {
                                masterObj[tab_table + "_id"] = id;
                            }
                            else
                            {

                                id = Guid.Parse(masterObj[tab_table + "_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add(tab_table + "_id", id);
                        }

                    }

                    if (editMode == "2")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isedituser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                                return json;
                            }
                        }


                        if (masterObj["user_edit"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_edit"].ToString()))
                            {
                                masterObj["user_edit"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_edit", username);
                        }

                        if (masterObj["date_edit"] != null)
                        {
                            masterObj["date_edit"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_edit", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj[tab_table + "_id"].ToString());

                    }

                    if (editMode == "3")
                    {
                        if (masterObj["user_new"] != null)
                        {
                            string user_new = masterObj["user_new"].ToString();

                            if (isdeluser != "C" && user_new != username)
                            {
                                json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                                return json;
                            }
                        }
                    }

                    string sql = editMode == "1" ? "#SCHEMA_NAME#." + CRD_INV_INVOICEAUTH_INSERT_HSM : editMode == "2" ? "#SCHEMA_NAME#." + tblWbTab.Rows[0]["update_cmd"].ToString() : "#SCHEMA_NAME#." + tblWbTab.Rows[0]["delete_cmd"].ToString();

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }


                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            string tab_id = (string)dtl["tab_id"];
                            string table_name = (string)dtl["tab_table"];

                            dic = new Dictionary<string, object> { { "code", tab_id } };
                            DataTable tblTabDetail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE code=@code", CommandType.Text, dic);

                            dic = new Dictionary<string, object> { { "wb_tab_id", tblTabDetail.Rows[0]["wb_tab_id"] } };
                            DataTable tblFields = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_field WHERE wb_tab_id=@wb_tab_id", CommandType.Text, dic);

                            string foreign_key = tblTabDetail.Rows[0]["foreign_key"].ToString();
                            string prefix = tblTabDetail.Rows[0]["prefix"].ToString();

                            var fieldStt_rec0 = tblFields.Select("column_name='row_ord'");

                            JArray obj_details = (JArray)dtl["data"];

                            dic = new Dictionary<string, object> { { "tab_table_id", masterObj[tab_table + "_id"] } };
                            sql = "DELETE FROM #SCHEMA_NAME#." + table_name + " WHERE " + foreign_key + "=@tab_table_id";

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dic);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details[foreign_key] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[foreign_key].ToString()))
                                    {
                                        js_details[foreign_key] = masterObj[tab_table + "_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add(foreign_key, masterObj[tab_table + "_id"]);
                                }

                                if (js_details[table_name + "_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details[table_name + "_id"].ToString()))
                                    {
                                        js_details[table_name + "_id"] = Guid.NewGuid();
                                    }
                                }
                                else
                                {
                                    js_details.Add(table_name + "_id", Guid.NewGuid());
                                }

                                if (fieldStt_rec0 != null)
                                {
                                    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                }

                                if (js_details["date_new"] != null)
                                {
                                    js_details["date_new"] = DateTime.Now;
                                }
                                else
                                {
                                    js_details.Add("date_new", DateTime.Now);
                                }

                                if (js_details["user_new"] != null)
                                {
                                    js_details["user_new"] = username;
                                }
                                else
                                {
                                    js_details.Add("user_new", username);
                                }

                                if (js_details["branch_code"] != null)
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details["branch_code"] = branch_code;
                                }
                                else
                                {
                                    string branch_code = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                                    js_details.Add("branch_code", branch_code);
                                }


                                await _minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#." + tblTabDetail.Rows[0]["insert_cmd"].ToString(), CommandType.StoredProcedure, js_details);
                            }
                        }

                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    //---------------------- Sinh số hóa đơn ---------------
                    string dvcs = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                    dic = new Dictionary<string, object> { { "branch_code", dvcs } };
                    DataTable tblInvoiceProcess = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoice_process WHERE branch_code=@branch_code ORDER BY ord", CommandType.Text, dic);
                    int index = 0;

                    for (int j = 0; j < tblInvoiceProcess.Rows.Count; j++)
                    {
                        DataRow row = tblInvoiceProcess.Rows[j];
                        string code = row["code"].ToString();
                        if (code == "02")
                        {
                            index = j;
                            break;
                        }
                    }

                    string status = "";

                    if (tblInvoiceProcess.Rows.Count == 0)
                    {
                        status = "Chờ ký";
                    }
                    else if (tblInvoiceProcess.Rows.Count == 1)
                    {
                        status = tblInvoiceProcess.Rows[0]["name"].ToString();
                    }
                    else
                    {
                        status = tblInvoiceProcess.Rows[i + 1]["name"].ToString();
                    }

                    string sql_get_invoice_number = "SELECT #SCHEMA_NAME#.qr_get_invoice_number('#SCHEMA_NAME#',@inv_invoiceauth_id,@username,@status)";
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        { "inv_invoiceauth_id", Guid.Parse(id.ToString()) },
                        { "username", username },
                        { "status", status }
                    };

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_get_invoice_number, parameters);

                    //---------------------- Sinh số hóa đơn ---------------

                    //---------------------- SignInvoiceCertFile ---------------
                    var ma_dvcs = masterObj["branch_code"] == null ? _webHelper.GetDvcs() : masterObj["branch_code"].ToString();
                    var cer_serialData = await _invoiceService.GetListCertificates(username, ma_dvcs);
                    var cer_serial = cer_serialData[0]["cer_serial"].ToString();
                    var inputKyHoaDon = new JObject()
                    {
                        { "branch_code",  ma_dvcs},
                        { "username", username},
                        { "inv_invoiceauth_id", id},
                        { "cer_serial", cer_serial},

                    };
                    json = await _invoiceService.SignInvoiceCertFile(inputKyHoaDon);

                    dic = new Dictionary<string, object> { { "inv_invoiceauth_id", id } };
                    dic.Add("invoice_series", masterObj["invoice_series"]);
                    dic.Add("invoice_type", masterObj["invoice_type"]);
                    dic.Add("branch_code", masterObj["branch_code"]);

                    var sqlInvoiceNumber = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id" +
                        " AND invoice_series=@invoice_series AND invoice_type = @invoice_type AND branch_code = @branch_code";

                    DataTable dataInvoiceNumber = await _minvoiceDbContext.GetDataTableAsync(sqlInvoiceNumber, CommandType.Text, dic);
                    if (json["error"] != null)
                    {
                        isError = true;
                        json["error"] = "Hoá đơn đã được tạo và đang ở trạng thái " + status + ". " + json["error"];
                        json.Add("invoice_number", dataInvoiceNumber.Rows[0]["invoice_number"].ToString());
                    }
                    //---------------------- SignInvoiceCertFile ---------------

                    //if (tab_table == "inv_invoiceauth" && editMode == "1")
                    //{
                    //    DataTable tblEmailQueue = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_email_queue_id FROM #SCHEMA_NAME#.wb_email_queue WHERE inv_invoiceauth_id='" + id + "' AND is_send=false");
                    //    // LỖI Ở ĐÂY
                    //    if (tblEmailQueue.Rows.Count > 0)
                    //    {
                    //        string schemaName = await _minvoiceDbContext.GetSchemaNameAsync();

                    //        JObject rabbitObj = new JObject();
                    //        rabbitObj.Add("SCHEMA_NAME", schemaName);

                    //        JArray jarray = JArray.FromObject(tblEmailQueue);
                    //        rabbitObj.Add("data", jarray);

                    //        _rabbitMQService.PublishQueue("notify_email_signer_queue", rabbitObj.ToString());
                    //    }
                    //}
                }


                if (editMode != "3")
                {
                    string select_cmd = tblWbTab.Rows[0]["select_cmd"].ToString();
                    dic = new Dictionary<string, object> { { "id", id } };
                    if (select_cmd.Length > 0)
                    {
                        if (select_cmd.ToUpper().Contains("WHERE"))
                        {
                            select_cmd = select_cmd + " AND " + tab_table + "_id=@id";
                        }
                        else
                        {
                            select_cmd = select_cmd + " WHERE " + tab_table + "_id=@id";
                        }

                        select_cmd = select_cmd.Replace("#BRANCH_CODE#", _webHelper.GetDvcs())
                                                .Replace("#USER_LOGIN#", _webHelper.GetUser())
                                                .Replace("#STORE_CODE#", store_code);


                        DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(select_cmd, CommandType.Text, dic);

                    }
                    else
                    {
                        dic = new Dictionary<string, object> { { "id", id } };
                        DataTable tbl = await _minvoiceDbContext.GetDataTableAsync("SELECT *," + tab_table + "_id as id FROM " + tab_table + " WHERE " + tab_table + "_id=@id", CommandType.Text, dic);
                    }
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JArray> GetDataReferencesByRefId(string no, string ma_dvcs)
        {
            JArray data;

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("code", no);

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_references WHERE code=@code";
                DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tbl.Rows[0]["type"].ToString() == "List")
                {
                    string[] arr = tbl.Rows[0]["list_value"].ToString().Split(',');
                    data = new JArray();
                    foreach (var s in arr)
                    {
                        data.Add(s);
                    }
                }
                else
                {
                    string user = _webHelper.GetUser();

                    parameters.Clear();
                    parameters.Add("username", user);

                    DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

                    string store_code = "";

                    if (tblUser.Rows.Count > 0)
                    {
                        store_code = tblUser.Rows[0]["store_code"].ToString();
                    }

                    sql = tbl.Rows[0]["sql"].ToString()
                                            .Replace("#BRANCH_CODE#", ma_dvcs)
                                            .Replace("#USER_LOGIN#", user)
                                            .Replace("#STORE_CODE#", store_code);

                    var lst = await this._minvoiceDbContext.GetDataTableAsync(sql);
                    data = JArray.FromObject(lst);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return data;
        }


        public async Task<JArray> GetDataReferencesByRefId(string no, string filtervalue, string ma_dvcs)
        {
            JArray data;

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("code", no);

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_references WHERE code=@code";
                DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tbl.Rows[0]["type"].ToString() == "List")
                {
                    string[] arr = tbl.Rows[0]["list_value"].ToString().Split(',');
                    data = new JArray();
                    foreach (var s in arr)
                    {
                        data.Add(s);
                    }
                }
                else
                {
                    string user = _webHelper.GetUser();

                    parameters.Clear();
                    parameters.Add("username", user);

                    DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

                    string store_code = "";

                    if (tblUser.Rows.Count > 0)
                    {
                        store_code = tblUser.Rows[0]["store_code"].ToString();
                    }

                    sql = tbl.Rows[0]["sql"].ToString()
                                            .Replace("#BRANCH_CODE#", ma_dvcs)
                                            .Replace("#USER_LOGIN#", user)
                                            .Replace("#STORE_CODE#", store_code);

                    string filter_value = tbl.Rows[0]["filter_value"].ToString();
                    string order_by = tbl.Rows[0]["order_by"].ToString();

                    if (!string.IsNullOrEmpty(filter_value) && !string.IsNullOrEmpty(filtervalue))
                    {
                        filtervalue = filtervalue.Replace("'", "''");
                        filter_value = filter_value.Replace("#FILTER_VALUE#", filtervalue);

                        int idx_where = sql.ToUpper().IndexOf("WHERE");

                        if (idx_where >= 0)
                        {
                            if (!filter_value.ToUpper().StartsWith("AND"))
                            {
                                sql = sql + " AND " + filter_value;
                            }
                            else
                            {
                                sql = sql + filter_value;
                            }
                        }
                        else
                        {
                            sql = sql + " WHERE " + filter_value;
                        }
                    }

                    if (!string.IsNullOrEmpty(order_by))
                    {
                        sql = sql + " ORDER BY " + order_by;
                    }

                    var lst = await this._minvoiceDbContext.GetDataTableAsync(sql);
                    data = JArray.FromObject(lst);

                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return data;
        }


        public async Task<JObject> InsertFieldsByTabId(Guid id)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("id", id);

                string sql = "SELECT #SCHEMA_NAME#.qr_auto_create_fields('#SCHEMA_NAME#',@id)";
                await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }


        public async Task<JArray> GetDataByReferencesId(string no, string filtervalue, string ma_dvcs)
        {
            JArray data;

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("wb_references_id", Guid.Parse(no));

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_references WHERE wb_references_id=@wb_references_id";
                DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tbl.Rows[0]["type"].ToString() == "List")
                {
                    string[] arr = tbl.Rows[0]["list_value"].ToString().Split(',');
                    data = new JArray();
                    foreach (var s in arr)
                    {
                        data.Add(s);
                    }
                }
                else
                {
                    string user = _webHelper.GetUser();

                    parameters.Clear();
                    parameters.Add("username", user);

                    DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

                    string store_code = "";

                    if (tblUser.Rows.Count > 0)
                    {
                        store_code = tblUser.Rows[0]["store_code"].ToString();
                    }

                    sql = tbl.Rows[0]["sql"].ToString()
                                    .Replace("#BRANCH_CODE#", ma_dvcs)
                                    .Replace("#USER_LOGIN#", user)
                                    .Replace("#STORE_CODE#", store_code);

                    string filter_value = tbl.Rows[0]["filter_value"].ToString();
                    string order_by = tbl.Rows[0]["order_by"].ToString();

                    if (!string.IsNullOrEmpty(filter_value) && !string.IsNullOrEmpty(filtervalue))
                    {
                        filtervalue = filtervalue.Replace("'", "''");
                        filter_value = filter_value.Replace("#FILTER_VALUE#", filtervalue);

                        int idx_where = sql.ToUpper().IndexOf("WHERE");

                        if (idx_where >= 0)
                        {
                            if (!filter_value.ToUpper().StartsWith("AND"))
                            {
                                sql = sql + " AND " + filter_value;
                            }
                            else
                            {
                                sql = sql + filter_value;
                            }
                        }
                        else
                        {
                            sql = sql + " WHERE " + filter_value;
                        }
                    }

                    if (!string.IsNullOrEmpty(order_by))
                    {
                        sql = sql + " ORDER BY " + order_by;
                    }

                    var lst = await this._minvoiceDbContext.GetDataTableAsync(sql);
                    data = JArray.FromObject(lst);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return data;
        }

        public async Task<JArray> GetRecordByReferencesId(string id, string value)
        {
            JArray data;

            try
            {
                string ma_dvcs = _webHelper.GetDvcs();

                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("wb_references_id", Guid.Parse(id));

                DataTable tbl = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_references WHERE wb_references_id=@wb_references_id", CommandType.Text, parameters);

                string value_field = tbl.Rows[0]["value_field"].ToString();

                if (tbl.Rows[0]["type"].ToString() == "List")
                {
                    string[] arr = tbl.Rows[0]["list_value"].ToString().Split(',');
                    data = new JArray();
                    foreach (var s in arr)
                    {
                        data.Add(s);
                    }
                }
                else
                {
                    string user = _webHelper.GetUser();

                    parameters.Clear();
                    parameters.Add("username", user);

                    DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

                    string store_code = "";

                    if (tblUser.Rows.Count > 0)
                    {
                        store_code = tblUser.Rows[0]["store_code"].ToString();
                    }

                    string sql = tbl.Rows[0]["sql"].ToString()
                                    .Replace("#MA_DVCS#", ma_dvcs)
                                    .Replace("#BRANCH_CODE#", ma_dvcs)
                                    .Replace("#STORE_CODE#", store_code)
                                    .Replace("#USER_LOGIN#", user);

                    string order_by = tbl.Rows[0]["order_by"].ToString();

                    int idx_where = sql.ToUpper().IndexOf("WHERE");

                    if (idx_where >= 0)
                    {
                        if (string.IsNullOrEmpty(value))
                        {
                            sql = sql + " AND " + value_field + " IS NULL ";
                        }
                        else
                        {
                            sql = sql + " AND " + value_field + "='" + value + "' ";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(value))
                        {
                            sql = sql + " WHERE " + value_field + " IS NULL ";
                        }
                        else
                        {
                            sql = sql + " WHERE " + value_field + "='" + value + "' ";
                        }
                    }

                    if (!string.IsNullOrEmpty(order_by))
                    {
                        sql = sql + " ORDER BY " + order_by;
                    }

                    var lst = await _minvoiceDbContext.GetDataTableAsync(sql);
                    data = JArray.FromObject(lst);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return data;
        }

        public async Task<JArray> GetDataDetailsByTabTable(string window_id, Guid id, string tab_table)
        {
            JArray data = new JArray();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("code", window_id);

                DataTable tblWindow = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_window WHERE code=@code", CommandType.Text, parameters);
                string wb_window_id = tblWindow.Rows[0]["wb_window_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_window_id", Guid.Parse(wb_window_id));
                parameters.Add("table_name", tab_table);

                DataTable tblTab = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id=@wb_window_id AND table_name=@table_name", CommandType.Text, parameters);

                string foreign_key = tblTab.Rows[0]["foreign_key"].ToString();
                string select_cmd = tblTab.Rows[0]["select_cmd"].ToString();

                string sql = "";
                string dk = " WHERE " + foreign_key + "='" + id + "'";


                string order = "";

                if (string.IsNullOrEmpty(select_cmd))
                {
                    sql = "SELECT * FROM " + tab_table + dk;
                }
                else
                {
                    if (select_cmd.ToLower().Contains("order by"))
                    {
                        int index = select_cmd.IndexOf("ORDER");
                        sql = select_cmd.Substring(0, index);
                        order = select_cmd.Substring(index);
                        sql = sql + dk + " " + order;
                    }
                    else
                        sql = select_cmd + dk;
                }


                var lst = await this._minvoiceDbContext.GetDataTableAsync(sql);
                var json = JsonConvert.SerializeObject(lst, new JsonSerializerSettings() { DateFormatString = "yyyy-MM-dd hh:mm:ss" });


                data = JArray.Parse(json);
            }
            catch (Exception ex)
            {
                return null;
            }
            return data;
        }

        public async Task<JArray> ExecuteCommand(JObject obj, string command)
        {
            string username = _webHelper.GetUser();
            string ma_dvcs = _webHelper.GetDvcs();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("code", command);

            DataTable tblCommand = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_command WHERE code=@code", CommandType.Text, parameters);

            if (tblCommand.Rows.Count > 0)
            {
                parameters.Clear();
                parameters.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_user_id,branch_code,store_code FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);
                string store_code = tblUser.Rows[0]["store_code"].ToString();

                string sql = tblCommand.Rows[0]["sql"].ToString();

                sql = sql.Replace("#MA_DVCS#", "'" + ma_dvcs + "'");
                sql = sql.Replace("#BRANCH_CODE#", "'" + ma_dvcs + "'");
                sql = sql.Replace("#STORE_CODE#", "'" + store_code + "'");
                sql = sql.Replace("#USER_LOGIN#", "'" + username + "'");

                parameters.Clear();

                foreach (var x in obj)
                {
                    //sql = sql.Replace("#" + x.Key + "#", x.Value.ToString());

                    sql = sql.Replace("#" + x.Key + "#", "@" + x.Key);

                    if (!parameters.ContainsKey(x.Key))
                    {
                        string _value = x.Value.ToString();
                        Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

                        if (isGuid.IsMatch(_value))
                        {
                            parameters.Add(x.Key, Guid.Parse(_value));
                        }
                        else
                        {
                            parameters.Add(x.Key, _value);
                        }

                    }
                }

                DataTable tblResult = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                JArray data = JArray.FromObject(tblResult);

                return data;
            }

            return null;
        }

        public async Task<JObject> PhanQuyen(JObject model)
        {
            JObject result = new JObject();

            try
            {
                string _user = _webHelper.GetUser();

                DataTable tblMenu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_menu WHERE window_code='WIN00005'");

                if (tblMenu.Rows.Count == 0)
                {
                    result.Add("error", "Không tìm thấy chức năng phân quyền");
                    return result;
                }

                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("username", _user);

                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, dicPara);

                if (tblNhomQuyen.Rows.Count == 0)
                {
                    result.Add("error", "Không tìm thấy nhóm quyền của người sử dụng");
                    return result;
                }

                string wb_menu_id = tblMenu.Rows[0]["wb_menu_id"].ToString();

                dicPara.Clear();
                dicPara.Add("wb_role_id", Guid.Parse(tblNhomQuyen.Rows[0]["wb_role_id"].ToString()));
                dicPara.Add("wb_menu_id", Guid.Parse(wb_menu_id));

                DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id=@wb_menu_id", CommandType.Text, dicPara);

                if (tblCtQuyen.Rows.Count == 0)
                {
                    result.Add("error", "Bạn không được phân quyền chức năng này");
                    return result;
                }

                JObject dic = new JObject();
                dic.Add("wb_role_id", Guid.Parse(model["wb_role_id"].ToString()));

                string sql = "DELETE FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, CommandType.Text, dic);

                JArray jCtquyen = (JArray)model["wb_ctquyen"];

                foreach (var obj in jCtquyen)
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("wb_detailrole_id", Guid.NewGuid());
                    parameters.Add("wb_role_id", Guid.Parse(obj["wb_role_id"].ToString()));
                    parameters.Add("wb_menu_id", Guid.Parse(obj["wb_menu_id"].ToString()));
                    parameters.Add("has_new", obj["them"].ToString());
                    parameters.Add("has_edit", obj["sua"].ToString());
                    parameters.Add("has_delete", obj["xoa"].ToString());

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_detailrole (wb_detailrole_id,wb_role_id,wb_menu_id,has_new,has_edit,has_delete) \n"
                        + "VALUES (@wb_detailrole_id,@wb_role_id,@wb_menu_id,@has_new,@has_edit,@has_delete)";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                }

                sql = "DELETE FROM #SCHEMA_NAME#.wb_detailrole_report WHERE wb_role_id=@wb_role_id";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, CommandType.Text, dic);

                jCtquyen = (JArray)model["wb_ctquyenrp"];

                foreach (var obj in jCtquyen)
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("wb_detailrole_report_id", Guid.NewGuid());
                    parameters.Add("wb_role_id", Guid.Parse(obj["wb_role_id"].ToString()));
                    parameters.Add("wb_report_id", Guid.Parse(obj["wb_report_id"].ToString()));

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_detailrole_report (wb_detailrole_report_id,wb_role_id,wb_report_id) \n"
                        + "VALUES (@wb_detailrole_report_id,@wb_role_id,@wb_report_id)";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);


                }

            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);

            }

            return result;
        }

        public async Task<JObject> RegisterLicenceBranch(JObject model)
        {
            JObject result = new JObject();
            string editMode = (string)model["editmode"];
            string pl_licence_branch_id = (string)model["pl_licence_branch_id"];
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                if (editMode == "1")
                {
                    parameters.Add("pl_licence_branch_id", Guid.NewGuid());
                }
                else if (editMode == "2")
                {
                    parameters.Add("pl_licence_branch_id", Guid.Parse(pl_licence_branch_id));

                }
                parameters.Add("branch_code", model["branch_code"].ToString());

                Regex regex = new Regex(@"^[0-9]+$");
                if (regex.IsMatch(model["quantity"].ToString()))
                {
                    parameters.Add("quantity", Int32.Parse(model["quantity"].ToString()));
                }
                else
                {
                    result.Add("error", "Số lượng không đúng định dạng là một số dương"); ;
                    return result;
                }

                var date = DateTime.Parse(model["createdondate"].ToString());

                parameters.Add("createdondate", date);

                // check số lượng hóa đơn của khách hàng < tổng hóa đơn của Bao Tiêu 

                // lấy tổng hóa đơn của Bao Tiêu
                string sql = "SELECT COALESCE(SUM(a.quantity_addition), 0) quantity_addition FROM #SCHEMA_NAME#.pl_licenseinfo a WHERE a.branch_code = 'VP'";
                DataTable tblLiInfo = await this._minvoiceDbContext.GetDataTableAsync(sql);
                var row = tblLiInfo.Rows[0];
                var quantity_addition = Int32.Parse(row["quantity_addition"].ToString());

                // lấy tổng số hóa đơn của đơn vị khách hàng để so sánh

                sql = "SELECT COALESCE(SUM(a.quantity), 0) FROM #SCHEMA_NAME#.pl_licence_branch a WHERE a.pl_licence_branch_id <> @pl_licence_branch_id";

                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("pl_licence_branch_id", pl_licence_branch_id == null ? Guid.NewGuid() : Guid.Parse(pl_licence_branch_id));
                dics.Add("branch_code", model["branch_code"].ToString());

                DataTable tblLiBr = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dics);
                row = tblLiBr.Rows[0];
                var quantity = Int32.Parse(row[0].ToString());
                //var subQuantity = quantity_addition - quantity; // số lượng chênh lệch
                quantity += Int32.Parse(model["quantity"].ToString());

                if (quantity <= quantity_addition)
                {
                    // thêm mới
                    if (editMode == "1")
                    {
                        // insert vào DB
                        sql = "INSERT INTO #SCHEMA_NAME#.pl_licence_branch (pl_licence_branch_id, branch_code, quantity, createdondate ) \n"
                                + "VALUES (@pl_licence_branch_id,@branch_code,@quantity, @createdondate)";

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                        DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_licence_branch", CommandType.Text);
                        JArray data = new JArray();
                        data = JArray.FromObject(tbl);
                        result.Add("data", data[0]);
                    }
                    else if (editMode == "2")
                    {
                        // cập nhật vào DB
                        // kiểm tra ngày tạo có phải ngày trong tháng không?
                        sql = "SELECT a.createdondate FROM #SCHEMA_NAME#.pl_licence_branch a WHERE a.pl_licence_branch_id=@pl_licence_branch_id";
                        DataTable tblLibItem = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                        row = tblLibItem.Rows[0];
                        //var itemDate = DateTime.Parse(tblLibItem.Rows[0].ToString());

                        var itemDate = DateTime.Parse(row["createdondate"].ToString());
                        var itemMonth = itemDate.Month;
                        var itemYear = itemDate.Year;

                        var currentDate = DateTime.Now;
                        var currentMonth = currentDate.Month;
                        var currentYear = currentDate.Year;

                        if (currentYear == itemYear && currentMonth == itemMonth)
                        {
                            // kiểm tra cho cập nhật bản ghi gần nhất
                            sql = "SELECT a.createdondate FROM #SCHEMA_NAME#.pl_licence_branch a WHERE a.branch_code=@branch_code ORDER BY a.createdondate DESC";
                            DataTable tblLib = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                            row = tblLib.Rows[0];
                            // bản ghi gần nhất
                            var itemTopDate = DateTime.Parse(row["createdondate"].ToString());
                            //var itemTopMonth = itemTopDate.Month;
                            //var itemTopYear = itemTopDate.Year;

                            if (itemDate == itemTopDate)
                            {
                                // kiểm tra số lượng hóa đơn cập nhật phải lớn hơn số lượng hóa đơn đã phát hành

                                // tính số lượng hóa đơn đã phát hành của khách hàng

                                sql = "SELECT COALESCE(SUM(a.quantity), 0) FROM #SCHEMA_NAME#.inv_notificationdetail a WHERE a.branch_code IN (SELECT b.code FROM #SCHEMA_NAME#.wb_branch b WHERE b.tree_sort like '%'||@branch_code||'->%')";
                                DataTable tblNoti = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                                row = tblNoti.Rows[0];
                                var quantity_notification = Int32.Parse(row[0].ToString());
                                //var quantityUpdate = Int32.Parse(model["quantity"].ToString());

                                if (quantity < quantity_notification)
                                {
                                    sql = "SELECT COALESCE(SUM(a.quantity), 0) FROM #SCHEMA_NAME#.pl_licence_branch a WHERE a.pl_licence_branch_id <> @pl_licence_branch_id AND a.branch_code = @branch_code";

                                    tblLiBr = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dics);
                                    row = tblLiBr.Rows[0];
                                    quantity = Int32.Parse(row[0].ToString());
                                    var subQuantiy = quantity_notification - quantity;
                                    var str = "Số lượng cập nhật phải lớn hơn hoặc bằng ";
                                    str += subQuantiy.ToString();

                                    result.Add("error", str);
                                }
                                else
                                {
                                    sql = "UPDATE #SCHEMA_NAME#.pl_licence_branch a SET quantity=@quantity WHERE a.pl_licence_branch_id=@pl_licence_branch_id";
                                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                                    DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_licence_branch a WHERE a.pl_licence_branch_id=@pl_licence_branch_id", CommandType.Text, parameters);
                                    JArray data = new JArray();
                                    data = JArray.FromObject(tbl);
                                    result.Add("data", data[0]);
                                }
                            }
                            else
                            {
                                result.Add("error", "Không được sửa bản ghi này");
                            }
                        }
                        else
                        {
                            result.Add("error", "Không được sửa bản ghi này");
                        }
                    }
                }
                else
                {
                    result.Add("error", "Số lượng đăng ký đã vượt quá số lượng Tổng hóa đơn");
                }

                result.Add("ok", "true");
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);
            }
            return result;
        }
        public JObject AutoCreateTable(Guid id)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("vc_tab_id", id.ToString());

                string result = this._nopDbContext.ExecuteStoreProcedure("sproc_sys_autocreatetable", parameters);

                if (result != null)
                {
                    obj.Add("error", result);
                }

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> AutoCreateCmd(Guid id)
        {
            JObject obj = new JObject();

            try
            {

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("id", id);

                string sql = "SELECT #SCHEMA_NAME#.qr_auto_create_crd('#SCHEMA_NAME#',@id)";
                await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, dicParam);

                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }


        public async Task<byte[]> InChungTu(JObject model, string folder)
        {
            byte[] data = null;

            string wb_mauin_id = model["wb_mauin_id"].ToString();
            JObject parameter = (JObject)model["parameter"];
            string type = model["type"].ToString();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("wb_print_template_id", Guid.Parse(wb_mauin_id));

            DataTable tblMauIn = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_print_template WHERE wb_print_template_id=@wb_print_template_id", CommandType.Text, parameters);

            if (tblMauIn.Rows.Count == 0)
            {
                throw new Exception("Mẫu in không tồn tại");
            }
            bool inthutu = false;
            if (model["inthutu"] != null && Convert.ToBoolean(model["inthutu"].ToString()))
                inthutu = true;
            if (tblMauIn.Columns.Contains("is_receipt"))
            {
                string is_receipt = tblMauIn.Rows[0]["is_receipt"].ToString();

                if (is_receipt == "C")
                {
                    data = await _invoiceService.PrintReceiptFromId(parameter["id"].ToString(), folder, type, false, inthutu);
                    return data;
                }

            }

            if (Convert.ToBoolean(Convert.ToInt16(tblMauIn.Rows[0]["is_invoice"].ToString())) == true)
            {
                data = await _invoiceService.PrintInvoiceFromId(parameter["id"].ToString(), folder, type, "", inthutu);
            }
            else
            {
                string sql_code = tblMauIn.Rows[0]["sql_code"].ToString();
                //Dictionary<string, object> dic = parameter["id"].ToObject<Dictionary<string, object>>();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("p_id", (Guid)parameter["id"]);
                var dataSet = await this._minvoiceDbContext.GetDataSetAsyncPrintf(sql_code, CommandType.StoredProcedure, dic);
                string reportPath = folder + "\\" + tblMauIn.Rows[0]["report_file"].ToString();

                data = ReportUtil.PrintReport(dataSet, reportPath, type);

            }

            return data;
        }

        public async Task<JObject> SaveInfoCompany(JObject model)
        {
            JObject obj = new JObject();

            try
            {
                string branch_code = model["code"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("city", model["city"].ToString());
                parameters.Add("district", model["district"].ToString());
                parameters.Add("begin_date", model["begin_date"].ToString());
                parameters.Add("manage_unit_name", model["manage_unit_name"].ToString());
                parameters.Add("manage_taxcode", model["manage_taxcode"].ToString());
                parameters.Add("bank_account", model["bank_account"].ToString());
                parameters.Add("bank_name", model["bank_name"].ToString());
                parameters.Add("manage_email", model["manage_email"].ToString());
                parameters.Add("tel", model["tel"].ToString());
                parameters.Add("fax", model["fax"].ToString());
                parameters.Add("provincial_tax_authorities", model["provincial_tax_authorities"].ToString());
                parameters.Add("manage_tax_authorities", model["manage_tax_authorities"].ToString());
                parameters.Add("code", model["code"].ToString());

                string sql = "UPDATE #SCHEMA_NAME#.wb_branch SET city=@city,district=@district,begin_date=@begin_date,"
                            + "manage_unit_name=@manage_unit_name,manage_taxcode=@manage_taxcode,bank_account=@bank_account,"
                            + "bank_name=@bank_name,manage_email=@manage_email,tel=@tel,fax=@fax,"
                            + "provincial_tax_authorities=@provincial_tax_authorities,manage_tax_authorities=@manage_tax_authorities "
                            + "WHERE code=@code";

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);


                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JArray> GetInfoCompany(string branch_code)
        {
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("code", branch_code);

            string sql = "SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@code";

            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameter);

            return JArray.FromObject(tblData);
        }

        public async Task<byte[]> CreateReport(JObject model)
        {
            byte[] result = null;

            JObject parameter = (JObject)model["parameters"];

            string report_id = (string)model["report_id"];
            string type = (string)model["type"];
            string infowindow_id = (string)model["infowindow_id"];

            Dictionary<string, object> dicParameter = new Dictionary<string, object>();
            dicParameter.Add("wb_infowindow_id", Guid.Parse(infowindow_id));

            DataTable tblInfoWindow = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_infowindow WHERE wb_infowindow_id=@wb_infowindow_id", CommandType.Text, dicParameter);
            string sql_code = tblInfoWindow.Rows[0]["sql_code"].ToString();

            dicParameter.Clear();
            dicParameter.Add("wb_report_id", Guid.Parse(report_id));

            DataTable tblReport = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_report WHERE wb_report_id=@wb_report_id", CommandType.Text, dicParameter);
            string report_file = tblReport.Rows[0]["report_file"].ToString();

            DataSet ds = await _minvoiceDbContext.GetDataSetAsync(sql_code, CommandType.StoredProcedure, parameter.ToObject<Dictionary<string, object>>());

            if (type == "json")
            {
                var json = JsonConvert.SerializeObject(ds.Tables[0], new JsonSerializerSettings() { DateFormatString = "yyyy-MM-dd hh:mm:ss" });
                result = System.Text.Encoding.UTF8.GetBytes(json);
            }
            else
            {
                report_file = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Report/") + "\\" + report_file;
                result = ReportUtil.PrintReport(ds, report_file, type);
            }

            return result;

        }

        public async Task<byte[]> DownloadFileExcelTemplate(string excel_id)
        {

            try
            {
                // Kiểm tra file mẫu đã tồn tại trong db hay chưa nếu tồn tại thì lấy ra
                String sqlCheckFileExist = "SELECT file_template FROM #SCHEMA_NAME#.wb_excel WHERE code = @code";
                Dictionary<string, object> parameterCheckExist = new Dictionary<string, object>();
                parameterCheckExist.Add("code", excel_id);
                DataTable dataTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheckFileExist, CommandType.Text, parameterCheckExist);
                if (dataTable.Rows[0]["file_template"] != DBNull.Value)
                {
                    ExcelWorkbook xlsxTemplate = new ExcelWorkbook((byte[])dataTable.Rows[0]["file_template"]);
                    return xlsxTemplate.DownloadExel();
                }
                // Nếu file mẫu chưa có thì tự sinh
                Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                dicParameter.Add("code", excel_id);

                string sql = "SELECT b.* FROM #SCHEMA_NAME#.wb_excel a INNER JOIN #SCHEMA_NAME#.wb_excelcolumn b ON a.wb_excel_id=b.wb_excel_id WHERE a.code=@code ORDER BY b.ord";
                DataTable tblExcel = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                ExcelWorkbook xlsx = new ExcelWorkbook();

                xlsx.CreateRow(0);

                int col = 0;

                for (int i = 0; i < tblExcel.Rows.Count; i++)
                {
                    DataRow row = tblExcel.Rows[i];

                    string excel_col = row["excel_col"].ToString();

                    if (!string.IsNullOrEmpty(excel_col))
                    {
                        string field = row["field"].ToString();
                        string caption = row["caption"].ToString();
                        string not_empty = row["not_empty"].ToString();

                        string title = string.IsNullOrEmpty(caption) == true ? field : caption;

                        if (not_empty == "C")
                        {
                            title = title + "(*)";
                        }

                        xlsx.CreateCell(0, col);
                        xlsx.SetRowCellValue(0, col, title);

                        col++;
                    }

                }

                byte[] result = xlsx.DownloadExel();

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<string> UploadExcelAsync(string ma_dvcs, string user_login, string excel_id, byte[] bytes, Dictionary<string, object> formData)
        {
            Dictionary<string, object> parameter1s = new Dictionary<string, object>();
            parameter1s.Add("code", excel_id);

            string sql = "SELECT a.sql_code,a.sql_code2,a.row_begin,a.wb_window_id,a.sql_check_exists,b.* FROM #SCHEMA_NAME#.wb_excel a "
                        + "INNER JOIN #SCHEMA_NAME#.wb_excelcolumn b ON a.wb_excel_id=b.wb_excel_id "
                        + "WHERE a.code=@code ORDER BY b.ord";

            DataTable tblExcel = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameter1s);

            string sql_code = tblExcel.Rows[0]["sql_code"].ToString();
            string sql_code2 = tblExcel.Rows[0]["sql_code2"].ToString();
            int row_begin = Convert.ToInt32(tblExcel.Rows[0]["row_begin"]);

            string wb_window_id = tblExcel.Rows[0]["wb_window_id"].ToString();
            string sql_check_exists = tblExcel.Rows[0]["sql_check_exists"].ToString();
            string insert_cmd = string.Empty;
            string update_cmd = string.Empty;
            if (!wb_window_id.Equals(string.Empty))
            {
                sql = $"SELECT * FROM #SCHEMA_NAME#.wb_tab WHERE wb_window_id='{wb_window_id}' ORDER BY ord LIMIT 1 OFFSET 0";
                DataTable tblTab = await _minvoiceDbContext.GetDataTableAsync(sql);
                if (tblTab.Rows.Count > 0)
                {
                    insert_cmd = tblTab.Rows[0]["insert_cmd"].ToString();
                    update_cmd = tblTab.Rows[0]["update_cmd"].ToString();
                }
            }
            ExcelWorkbook xlsx = new ExcelWorkbook(bytes);

            int last_row = xlsx.GetFirstSheet().LastRowNum + 1;


            if (string.IsNullOrEmpty(sql_code2))
            {

                for (int i = row_begin; i <= last_row; i++)
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object>();

                    string _sql_check_exists = sql_check_exists;

                    foreach (DataRow row in tblExcel.Rows)
                    {
                        string excel_col = row["excel_col"].ToString();
                        string defaultvalue = row["default_value"].ToString();
                        string field = row["field"].ToString();
                        string type = row["type"].ToString();
                        string sqlvalue = row["sql_value"].ToString();

                        object value = DBNull.Value;

                        if (defaultvalue.Length > 0)
                        {
                            if (defaultvalue == "MA_DVCS")
                            {
                                value = ma_dvcs;
                            }
                            else if (defaultvalue == "USER_LOGIN")
                            {
                                value = user_login;
                            }
                            else if (defaultvalue == "CURRENT_DATE")
                            {
                                value = DateTime.Now.Date;
                            }
                            else if (defaultvalue == "CURRENT_DATETIME")
                            {
                                value = DateTime.Now;
                            }
                            else if (defaultvalue == "NEWID")
                            {
                                value = Guid.NewGuid();
                            }
                        }
                        else
                        {
                            string cellAddress = string.Format(excel_col + "{0}", i);
                            value = xlsx.GetCellValueFromExcel(cellAddress) == null ? DBNull.Value : xlsx.GetCellValueFromExcel(cellAddress);

                            if (sqlvalue.Length > 0)
                            {
                                sqlvalue = sqlvalue.Replace("#BRANCH_CODE#", ma_dvcs)
                                                    .Replace("#USER_LOGIN#", user_login);

                                if (value != null)
                                {
                                    sqlvalue = sqlvalue.Replace("#" + field + "#", value.ToString());
                                }

                                DataTable tblResult = await _nopDbContext.ExecuteCmdAsync(sqlvalue);

                                if (tblResult.Rows.Count > 0)
                                {
                                    value = tblResult.Rows[0][0];
                                }
                            }
                        }

                        if (_sql_check_exists.Length > 0 && value != null)
                        {
                            _sql_check_exists = _sql_check_exists.Replace("#" + field + "#", value.ToString());
                        }

                        parameters.Add(field, value);

                    }

                    if (sql_code.Length > 0)
                    {
                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_code, parameters);
                    }
                    else
                    {
                        if (_sql_check_exists.Length > 0)
                        {
                            DataTable tblExists = await _minvoiceDbContext.GetDataTableAsync(_sql_check_exists);

                            if (tblExists.Rows.Count > 0)
                            {
                                Dictionary<string, object> paramUpdate = new Dictionary<string, object>();

                                DataRow row = tblExists.Rows[0];

                                foreach (DataColumn column in tblExists.Columns)
                                {
                                    paramUpdate.Add(column.ColumnName, row[column.ColumnName]);
                                }

                                foreach (DataRow rowExcel in tblExcel.Rows)
                                {
                                    string excel_col = rowExcel["excel_col"].ToString();
                                    string field = rowExcel["field"].ToString();

                                    if (excel_col.Length > 0)
                                    {
                                        paramUpdate[field] = parameters[field];
                                    }
                                }

                                await _minvoiceDbContext.ExecuteNoneQueryAsync(update_cmd, paramUpdate);
                            }
                            else
                            {
                                await _minvoiceDbContext.ExecuteNoneQueryAsync(insert_cmd, parameters);
                            }
                        }
                        else
                        {
                            await _minvoiceDbContext.ExecuteNoneQueryAsync(insert_cmd, parameters);
                        }
                    }
                }
            }
            else
            {
                int columnCount = xlsx.GetColumnCountByRow(row_begin - 1);
                int numberOfRecords = tblExcel.Select("excel_col<>''").Length;

                if (columnCount != numberOfRecords)
                {
                    throw new Exception("File Excel là " + columnCount + " cột khác với file mẫu có " + numberOfRecords + " cột ");
                }

                string data_xml = "<data-set>";

                for (int i = row_begin; i <= last_row; i++)
                {
                    string record_xml = "<record>";

                    foreach (DataRow row in tblExcel.Rows)
                    {
                        string excel_col = row["excel_col"].ToString();
                        string defaultvalue = row["default_value"].ToString();
                        string field = row["field"].ToString();
                        string type = row["type"].ToString();
                        string sqlvalue = row["sql_value"].ToString();
                        string not_empty = row["not_empty"].ToString();

                        object value = DBNull.Value;

                        if (defaultvalue.Length > 0)
                        {
                            if (defaultvalue == "BRANCH_CODE")
                            {
                                value = ma_dvcs;
                            }
                            else if (defaultvalue == "USER_LOGIN")
                            {
                                value = user_login;
                            }
                            else if (defaultvalue == "CURRENT_DATE")
                            {
                                value = DateTime.Now.Date;
                            }
                            else if (defaultvalue == "CURRENT_DATETIME")
                            {
                                value = DateTime.Now;
                            }
                            else if (defaultvalue == "NEWID")
                            {
                                value = Guid.NewGuid();
                            }
                        }
                        else
                        {
                            string cellAddress = string.Format(excel_col + "{0}", i);
                            value = xlsx.GetCellValueFromExcel(cellAddress) == null ? DBNull.Value : xlsx.GetCellValueFromExcel(cellAddress);

                            if (value.ToString() == "" && not_empty == "C")
                            {
                                throw new Exception("Dòng " + i + " cột " + excel_col + " không được để trống");
                            }

                            if (sqlvalue.Length > 0)
                            {
                                sqlvalue = sqlvalue.Replace("#BRANCH_CODE#", ma_dvcs)
                                                    .Replace("#USER_LOGIN#", user_login);

                                if (value != null)
                                {
                                    sqlvalue = sqlvalue.Replace("#" + field + "#", value.ToString());
                                }

                                DataTable tblResult = await _minvoiceDbContext.GetDataTableAsync(sqlvalue);

                                if (tblResult.Rows.Count > 0)
                                {
                                    value = tblResult.Rows[0][0];
                                }
                            }


                        }

                        string element = "<" + field + ">{0}</" + field + ">";

                        if (type == "datetime" || type == "date")
                        {
                            if (value is string)
                            {
                                string[] parts = value.ToString().Split('/');

                                int day = Convert.ToInt32(parts[0]);
                                int month = Convert.ToInt32(parts[1]);
                                int year = Convert.ToInt32(parts[2]);

                                DateTime dt = new DateTime(year, month, day);
                                value = string.Format("{0:yyyy-MM-dd}", dt);
                            }
                            else
                            {

                                DateTime dt = Convert.ToDateTime(value);

                                value = string.Format("{0:yyyy-MM-dd}", dt);
                            }

                        }

                        if (value != DBNull.Value)
                        {
                            element = string.Format(element, value);
                            record_xml = record_xml + element;
                        }

                    }

                    record_xml = record_xml + "</record>";
                    data_xml = data_xml + record_xml;
                }

                data_xml = data_xml + "</data-set>";

                JObject parameters = new JObject();
                parameters.Add("branch_code", _webHelper.GetDvcs());
                parameters.Add("user_login", _webHelper.GetUser());
                parameters.Add("data_xml", data_xml);

                if (formData.Count > 0)
                {
                    foreach (KeyValuePair<string, object> entry in formData)
                    {
                        parameters.Add(entry.Key, entry.Value.ToString());
                    }
                }

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql_code2, CommandType.StoredProcedure, parameters);
            }
            return "";

        }
        public async Task<string> UploadExcelTemplateAsync(string ma_dvcs, string user_login, byte[] bytes,
            Dictionary<string, object> formData)
        {
            try
            {
                Dictionary<string, object> parameter = new Dictionary<string, object>();
                string sql =
                    "UPDATE #SCHEMA_NAME#.wb_excel SET file_template = @data " +
                    "WHERE wb_excel_id = cast(@wb_excel_id as uuid) ";
                parameter.Add("data", bytes);
                parameter.Add("wb_excel_id", formData["wb_excel_id"]);
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameter);
                return "";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public async Task<JArray> GetInvoiceDetail(string id)
        {
            JArray data = new JArray();
            try
            {
                Dictionary<string, object> dicParameters = new Dictionary<string, object>();
                dicParameters.Add("inv_invoiceauth_id", Guid.Parse(id));

                DataTable tblWindow = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoiceauthdetail_id as id, * FROM #SCHEMA_NAME#.inv_invoiceauthdetail WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParameters);
                data = JArray.FromObject(tblWindow);
            }
            catch (Exception ex)
            {
                return null;
            }
            return data;
        }

        public async Task<JObject> SaveContract()
        {
            var obj = new JObject();
            try
            {

                var client = new HttpClient();
                string contractUrl = CommonConstants.QLDV_CONTRACT;
                string contractSubUrl = CommonConstants.QLDV_CONTRACT_SUB;
                const string contractPar = "?timeStamp=2020120412000&userSys=ttcntt_minvoice&token=fdca517d4056e015e72160a72b27b99583abd49fd52cf814f73cd8e60ccd5bd7";
                client.BaseAddress = new Uri(contractUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(contractPar).Result;
                var serializer = new JavaScriptSerializer();

                await _minvoiceDbContext.BeginTransactionAsync();

                //Insert data contract
                var sql =
                    @"INSERT INTO reg.contract(contract_id, contract_no, contract_date, admin_name, admin_position, admin_tel, admin_email,
                                start_date, end_date, payment_type, prepaid_info, prepaid_money, am_id, am_name, agency_id, agency_name, region_id, 
                                region_name, user_new, date_new,date_edit, taxcode, time_stamp, customer_id, customer_name, customer_email, customer_tel, customer_address, license_no)
                              VALUES(@contract_id, @contract_no, @contract_date, @admin_name, @admin_position, @admin_tel, @admin_email, @start_date, @end_date, @payment_type, 
                                     @prepaid_info, @prepaid_money, @am_id, @am_name, @agency_id, @agency_name, @region_id, @region_name, @user_new, @date_new,@date_edit, @taxcode, 
                                     @time_stamp, @customer_id, @customer_name, @customer_email, @customer_tel, @customer_address, @license_no); ";
                var date = new DateTime();
                int i = 0;
                long l = 0;
                Dictionary<string, object> parameters;
                dynamic item, objects;
                if (response.IsSuccessStatusCode)
                {
                    item = serializer.Deserialize<object>(await response.Content.ReadAsStringAsync());
                    objects = serializer.Deserialize<List<object>>(item["datas"]);


                    foreach (var d in objects)
                    {
                        parameters = new Dictionary<string, object>();

                        if (!d.ContainsKey("contractId"))
                            continue;
                        parameters.Add("contract_id", d["contractId"].ToString());
                        parameters.Add("contract_no", d.ContainsKey("contractNo") ? d["contractNo"].ToString() : DBNull.Value);
                        if (d.ContainsKey("contractDate") && DateTime.TryParse(d["contractDate"].ToString(), out date))
                            parameters.Add("contract_date", date);
                        else
                            parameters.Add("contract_date", DBNull.Value);
                        parameters.Add("admin_name", d.ContainsKey("adminName") ? d["adminName"].ToString() : DBNull.Value);
                        parameters.Add("admin_position", d.ContainsKey("adminPosition") ? d["adminPosition"].ToString() : DBNull.Value);
                        parameters.Add("admin_tel", d.ContainsKey("adminTel") ? d["adminTel"].ToString() : DBNull.Value);
                        parameters.Add("admin_email", d.ContainsKey("adminEmail") ? d["adminEmail"].ToString() : DBNull.Value);
                        if (d.ContainsKey("startDate") && DateTime.TryParse(d["startDate"].ToString(), out date))
                            parameters.Add("start_date", date);
                        else
                            parameters.Add("start_date", DBNull.Value);
                        if (d.ContainsKey("endDate") && DateTime.TryParse(d["endDate"].ToString(), out date))
                            parameters.Add("end_date", date);
                        else
                            parameters.Add("end_date", DBNull.Value);
                        parameters.Add("payment_type", d.ContainsKey("paymentType") ? d["paymentType"].ToString() : DBNull.Value);
                        if (d.ContainsKey("prepaidInfo") && int.TryParse(d["prepaidInfo"].ToString(), out i))
                            parameters.Add("prepaid_info", i);
                        else
                            parameters.Add("prepaid_info", DBNull.Value);

                        if (d.ContainsKey("prepaidMoney") && long.TryParse(d["prepaidMoney"].ToString(), out l))
                            parameters.Add("prepaid_money", l);
                        else
                            parameters.Add("prepaid_money", DBNull.Value);

                        parameters.Add("am_id", d.ContainsKey("amId") ? d["amId"].ToString() : DBNull.Value);
                        parameters.Add("am_name", d.ContainsKey("amName") ? d["amName"].ToString() : DBNull.Value);
                        parameters.Add("agency_id", d.ContainsKey("agencyId") ? d["agencyId"].ToString() : DBNull.Value);
                        parameters.Add("agency_name", d.ContainsKey("agencyName") ? d["agencyName"].ToString() : DBNull.Value);
                        parameters.Add("region_id", d.ContainsKey("regionId") ? d["regionId"].ToString() : DBNull.Value);
                        parameters.Add("region_name", d.ContainsKey("regionName") ? d["regionName"].ToString() : DBNull.Value);
                        parameters.Add("user_new", d.ContainsKey("userNew") ? d["userNew"].ToString() : DBNull.Value);
                        if (d.ContainsKey("dateNew") && DateTime.TryParse(d["dateNew"].ToString(), out date))
                            parameters.Add("date_new", date);
                        else
                            parameters.Add("date_new", DBNull.Value);
                        if (d.ContainsKey("dateEdit") && DateTime.TryParse(d["dateEdit"].ToString(), out date))
                            parameters.Add("date_edit", date);
                        else
                            parameters.Add("date_edit", DBNull.Value);
                        parameters.Add("taxcode", d.ContainsKey("taxcode") ? d["taxcode"].ToString() : DBNull.Value);
                        parameters.Add("time_stamp", d.ContainsKey("timeStamp") ? d["timeStamp"].ToString() : DBNull.Value);
                        parameters.Add("customer_id", d.ContainsKey("customerId") ? d["customerId"].ToString() : DBNull.Value);
                        parameters.Add("customer_name", d.ContainsKey("customerName") ? d["customerName"].ToString() : DBNull.Value);
                        parameters.Add("customer_email", d.ContainsKey("customerEmail") ? d["customerEmail"].ToString() : DBNull.Value);
                        parameters.Add("customer_tel", d.ContainsKey("customerTel") ? d["customerTel"].ToString() : DBNull.Value);
                        parameters.Add("customer_address", d.ContainsKey("customerAddress") ? d["customerAddress"].ToString() : DBNull.Value);
                        parameters.Add("license_no", d.ContainsKey("license") ? d["license"].ToString() : DBNull.Value);
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);
                        //await _minvoiceDbContext.ExecuteNoneQueryAsync(sql,  parameters);
                    }
                }
                client.Dispose();
                //Insert data contract sub
                client = new HttpClient { BaseAddress = new Uri(contractSubUrl) };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = client.GetAsync(contractPar).Result;

                if (response.IsSuccessStatusCode)
                {
                    sql = @"INSERT INTO reg.contract_sub(
                        contract_id, date_new, package_code, package_quantity, note)
                        VALUES(@contract_id, @date_new, @package_code, @package_quantity, @note)";
                    item = serializer.Deserialize<object>(await response.Content.ReadAsStringAsync());
                    objects = serializer.Deserialize<List<object>>(item["datas"]);
                    foreach (var d in objects)
                    {
                        parameters = new Dictionary<string, object>();
                        if (!d.ContainsKey("contractId"))
                            continue;
                        parameters.Add("contract_id", d["contractId"].ToString());
                        if (d.ContainsKey("dateNew") && DateTime.TryParse(d["dateNew"].ToString(), out date))
                            parameters.Add("date_new", date);
                        else
                            parameters.Add("date_new", DBNull.Value);
                        parameters.Add("package_code", d.ContainsKey("packageCode") ? d["packageCode"].ToString() : DBNull.Value);

                        if (d.ContainsKey("packageQuantity") && int.TryParse(d["packageQuantity"].ToString(), out i))
                            parameters.Add("package_quantity", i);
                        else
                            parameters.Add("package_quantity", DBNull.Value);
                        parameters.Add("note", d.ContainsKey("note") ? d["note"].ToString() : DBNull.Value);
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);
                        //await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    }
                }
                client.Dispose();
                await _minvoiceDbContext.TransactionCommitAsync();
                obj.Add("success", "Insert dữ liệu thành công!");
            }
            catch (Exception e)
            {
                obj.Add("error", e.Message);
                await _minvoiceDbContext.TransactionRollbackAsync();
            }
            finally
            {
                _minvoiceDbContext.CloseTransaction();
            }
            return obj;
        }

        public async Task<JObject> CreateNewCustomers(string maSoThue)
        {
            JObject json = new JObject();
            var conn = PostgreContext.GetNpgsqlConnection();
            try
            {
                if (string.IsNullOrWhiteSpace(maSoThue))
                {
                    json.Add("error", "Bạn chưa nhập thông tin khách hàng");
                    return json;
                }
                conn.Open();
                string originalString = _webHelper.GetRequest().Url.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/App" : "~/Content/App";
                var folder = HttpContext.Current.Server.MapPath(path);

                var script = File.ReadAllText($"{folder}/SQL/clone_schema.sql");
                script = script.Replace("#schema#", CommonConstants.SCHEMA_REG);
                await _minvoiceDbContext.ExecuteNoneQueryAsync(script);

                // TẠO DATABASE CLONE
                string sqlClone = $"SELECT * FROM {CommonConstants.SCHEMA_REG}.clone_schema('{CommonConstants.SCHEMA_SOURCE}','m{maSoThue}');";// $"SELECT {CommonConstants.schemaSaveFunction}.clone_schema('dbo','{txtName.Text}', {chboxCloneData.Checked},false);";
                NpgsqlDataAdapter s = new NpgsqlDataAdapter(sqlClone, conn);
                var dt = new DataTable();
                s.Fill(dt);
                var value = dt.Rows.Count > 0 && bool.Parse(dt.Rows[0]["o_value"].ToString());
                if (value)
                {
                    // LƯU THÔNG TIN COMPANY
                    string sql = $"INSERT INTO {CommonConstants.SCHEMA_REG}.company VALUES ('{maSoThue}', '{CommonConstants.MINVOICE}', 'm{maSoThue}')";
                    NpgsqlCommand command = new NpgsqlCommand(sql, conn);
                    command.ExecuteNonQuery();
                    json.Add("success", "Tạo khách hàng thành công !");
                }

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return json;
        }

        public async Task<JObject> ServicesIntegratedIntoTheMarketPlace(JObject model)
        {
            var output = new JObject();
            //int createType = 1;
            //int updateType = 2;
            //int updateTypeWithPackage = 6;
            try
            {
                if (model == null || model.Count <= 0) throw new Exception("Không có dữ liệu");
                var key = CommonConstants.QLDV_PASS_API;
                var rawDataToken = model["timeStamp"].ToString() + model["customerId"].ToString() + model["customerName"].ToString() + key;
                var token = FunctionUtil.ComputeSha256Hash(rawDataToken);
                if (model["token"].ToString() != token)
                {
                    throw new Exception("Sai thông tin Token! Vui lòng kiểm tra lại");
                }

                int type = int.Parse(model["type"].ToString());

                var taxCodeOrigin = model["taxCode"].ToString();
                var tax_code = new string(model["taxCode"].ToString().Where(p => char.IsDigit(p)).ToArray());
                model["taxCode"] = tax_code;
                var parameters = new Dictionary<string, object>();
                string sql = "";
                var data = new DataTable();

                string schemaUrl = await _minvoiceDbContext.GetConnectionAsyncCache(tax_code);

                if (string.IsNullOrWhiteSpace(schemaUrl))
                {
                    sql = $"select a.id, a.schema_url, count(a.id) from {CommonConstants.MASTER_SCHEMA}.db_config a \n" +
                        $"  LEFT JOIN {CommonConstants.MASTER_SCHEMA}.tenant_config b ON a.id = b.db_config_id \n" +
                        $"  group by a.id order by count(1)";
                    DataTable dataCompany = await _masterInvoiceDbContext.GetDataTableAsync(sql);

                    string minDbConnection = "";
                    Guid idDbTenant = Guid.NewGuid();
                    if (dataCompany.Rows.Count > 0)
                    {
                        minDbConnection = dataCompany.Rows[0]["schema_url"].ToString();
                        idDbTenant = Guid.Parse(dataCompany.Rows[0]["id"].ToString());
                    }
                    else
                    {
                        throw new Exception("Kiểm tra Thông tin Database!");
                    }

                    sql = $"INSERT INTO {CommonConstants.MASTER_SCHEMA}.tenant_config(id, tax_code, db_config_id, schema_url)  \n" +
                        $"  VALUES ('{Guid.NewGuid()}', '{tax_code}', '{idDbTenant}', '{minDbConnection}')";
                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql);

                }

                _minvoiceDbContext.SetSiteHddt(tax_code);
                await _minvoiceDbContext.BeginTransactionAsyncNotPool();

                switch (type)
                {
                    case 1:

                        // LƯU THÔNG TIN COMPANY
                        //string sql = $"INSERT INTO {CommonConstants.schemaReg}.company VALUES ('{txtTaxCode.Text}', '{CommonConstants.minvoice}', 'm{txtName.Text}')";
                        sql = $"SELECT * FROM {CommonConstants.SCHEMA_REG}.company WHERE taxcode = '{tax_code}'";
                        DataTable dataCompany = await _minvoiceDbContext.GetDataTableAsync(sql);
                        if (dataCompany.Rows.Count > 0) throw new Exception("Đăng kí dịch vụ mới thất bại. Khách hàng đã tồn tại!");



                        // CREATE SCHEMA
                        sql = $"SELECT {CommonConstants.SCHEMA_REG}.clone_schema('{CommonConstants.SCHEMA_SOURCE}','m{tax_code}', true, false);";
                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, new Dictionary<string, object>());
                        // CREATE COMPANY
                        sql = $"INSERT INTO {CommonConstants.SCHEMA_REG}.company VALUES ('{tax_code}', '{CommonConstants.MINVOICE}', 'm{tax_code}', true)";
                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, new Dictionary<string, object>());

                        // TẠO BRANCH MỚI
                        var customer_name = model["customerName"]?.ToString();
                        var customer_address = model["customerAddress"]?.ToString();
                        var wb_branch_id = Guid.Parse("2aefd9dc-cd42-4741-9d78-44b5990ac2d0");
                        var wb_user_id = Guid.Parse("38e27b13-484b-460d-a64d-88548968d2ae");


                        parameters.Clear();

                        // THÔNG TIN USERNAME
                        var password = CommonConstants.PASS_HASH;
                        var password_login = CommonConstants.PASS_LOGIN;
                        var wb_role_id = Guid.Parse("588bb696-7934-44b4-aedc-61acacb0aae4");
                        var customer_email = model["customerEmail"]?.ToString();

                        parameters.Add("wb_user_id", wb_user_id);
                        parameters.Add("username", tax_code);
                        parameters.Add("password", password);
                        parameters.Add("wb_role_id", wb_role_id);
                        parameters.Add("email", customer_email);
                        parameters.Add("fullname", customer_name);
                        parameters.Add("branch_code", CommonConstants.BRANCH_CODE);

                        sql = $"INSERT INTO m{tax_code}.wb_user(wb_user_id,username, password, wb_role_id,email, is_view_user, is_edit_user, is_del_user, is_sign_invoice, is_lock, fullname, branch_code, is_new_user) \n"
                                  + "VALUES (@wb_user_id,@username,@password,@wb_role_id,@email,'C','C','C','C','false',@fullname,@branch_code,'C')";
                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, parameters);

                        // THÔNG TIN WB_BRANCH_PERMISSION
                        parameters.Clear();
                        var wb_branch_permission_id = Guid.Parse("af379b19-b759-4ce5-9468-02fe454eccfc");
                        parameters.Add("wb_branch_permission_id", wb_branch_permission_id);
                        parameters.Add("wb_user_id", wb_user_id);
                        parameters.Add("wb_branch_id", wb_branch_id);

                        sql = $"INSERT INTO m{tax_code}.wb_branch_permission(wb_branch_permission_id, wb_user_id, wb_branch_id) VALUES (@wb_branch_permission_id,@wb_user_id,@wb_branch_id)";
                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, parameters);

                        // DỮ LIỆU BẢNG CONTRACT
                        //var prepaid_info = string.IsNullOrEmpty(model["prepaidInfo"]?.ToString()) ? (int?)null : int.Parse(model["prepaidInfo"]?.ToString());
                        //var prepaid_money = string.IsNullOrEmpty(model["prepaidMoney"]?.ToString()) ? (long?)null : int.Parse(model["prepaidMoney"]?.ToString());

                        parameters.Clear();
                        parameters.Add("contract_id", model["contractID"]?.ToString());
                        parameters.Add("contract_no", model["contractNo"]?.ToString());
                        try
                        {
                            parameters.Add("contract_date", DateTime.Parse(model["contractDate"]?.ToString()));
                        }
                        catch (Exception ex)
                        {
                            parameters.Add("contract_date", DBNull.Value);
                        }

                        parameters.Add("admin_name", model["adminName"]?.ToString());
                        parameters.Add("admin_position", model["adminPosition"]?.ToString());
                        parameters.Add("admin_tel", model["adminTel"]?.ToString());
                        parameters.Add("admin_email", model["adminEmail"]?.ToString());

                        parameters.Add("start_date", DateTime.Parse(model["startDate"]?.ToString()));
                        parameters.Add("end_date", DateTime.Parse(model["endDate"]?.ToString()));

                        parameters.Add("payment_type", model["paymentType"]?.ToString());

                        parameters.Add("prepaid_info", model["prepaidInfo"]?.ToString() == null ? "" : model["prepaidInfo"].ToString());

                        int pre;
                        if (string.IsNullOrWhiteSpace(model["prepaidMoney"]?.ToString()) || !int.TryParse(model["prepaidMoney"]?.ToString(), out pre))
                            parameters.Add("prepaid_money", DBNull.Value);
                        else
                            parameters.Add("prepaid_money", pre);

                        parameters.Add("am_id", model["aMID"]?.ToString());
                        parameters.Add("am_name", model["aMName"]?.ToString());
                        parameters.Add("agency_id", model["agencyID"]?.ToString());
                        parameters.Add("agency_name", model["agencyName"]?.ToString());
                        parameters.Add("region_id", model["regionID"]?.ToString());
                        parameters.Add("region_name", model["regionName"]?.ToString());

                        parameters.Add("user_new", tax_code);
                        parameters.Add("date_new", DateTime.Now);

                        parameters.Add("taxcode", tax_code);
                        parameters.Add("time_stamp", model["timeStamp"]?.ToString());

                        parameters.Add("customer_id", model["customerId"]?.ToString());
                        parameters.Add("customer_name", model["customerName"]?.ToString());
                        parameters.Add("customer_email", model["customerEmail"]?.ToString());
                        parameters.Add("customer_tel", model["customerTel"]?.ToString());
                        parameters.Add("customer_address", model["customerAddress"]?.ToString());
                        parameters.Add("license_no", model["licenseNo"]?.ToString());

                        sql = $"INSERT INTO {CommonConstants.SCHEMA_REG}.contract(customer_id, customer_name, customer_email, customer_tel, customer_address, time_stamp, license_no, contract_id,contract_no, contract_date, admin_name,admin_position, admin_tel, admin_email, start_date, end_date, payment_type, prepaid_info, prepaid_money, am_id, am_name, agency_id, agency_name, region_id, region_name, user_new, date_new, taxcode) \n"
                                      + "VALUES (@customer_id, @customer_name, @customer_email, @customer_tel, @customer_address, @time_stamp, @license_no, @contract_id,@contract_no,@contract_date,@admin_name,@admin_position, @admin_tel, @admin_email, @start_date, @end_date, @payment_type, @prepaid_info, @prepaid_money, @am_id, @am_name, @agency_id, @agency_name, @region_id, @region_name, @user_new, @date_new, @taxcode)";

                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, parameters);

                        parameters = new Dictionary<string, object>();
                        parameters.Add("name", customer_name);
                        parameters.Add("tax_code", taxCodeOrigin);
                        parameters.Add("customer_email", customer_address);

                        sql = $@"UPDATE m{tax_code}.wb_branch SET name=@name, tax_code=@tax_code, address=@customer_email WHERE code='VP'";

                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, parameters);

                        sql = $@"UPDATE m{tax_code}.pl_tempemail as t1 SET alias = t2.name from m{tax_code}.wb_branch as t2 WHERE t1.branch_code = t2.code";

                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, new Dictionary<string, object>());

                        string isBaoTieu;

                        try
                        {
                            isBaoTieu = model["isBaoTieu"].ToString();
                        }
                        catch (Exception ex)
                        {
                            isBaoTieu = "false";
                        }

                        if (isBaoTieu.Equals("true"))
                        {
                            sql = $"UPDATE {CommonConstants.SCHEMA_REG}.company set is_baotieu=true WHERE taxcode = '{tax_code}'";
                            await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, new Dictionary<string, object>());
                            sql = $"SELECT FROM {CommonConstants.SCHEMA_REG}.createbaotieu('m{tax_code}')";
                            await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, new Dictionary<string, object>());
                        }

                        parameters.Clear();

                        // GỬI MAIL
                        DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM {CommonConstants.SCHEMA_SOURCE}.wb_tempemail WHERE email_type='CREATE_ACCOUNT_LOGIN' AND yesno='C' LIMIT 1");
                        try
                        {
                            if (tblEmail.Rows.Count > 0)
                            {
                                string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                                if (smtp_address.Length > 0)
                                {
                                    int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                                    bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                                    this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                                }

                                string subject = tblEmail.Rows[0]["subject"].ToString();
                                string tax_code2 = tax_code.Replace("-", "");
                                string domain = tax_code2 + ".mobifoneinvoice.vn";
                                string email = model["customerEmail"]?.ToString();
                                string body = tblEmail.Rows[0]["body"].ToString()
                                                .Replace("#username#", tax_code)
                                                .Replace("#password#", password_login)
                                                .Replace("#url_host#", domain);

                                await _emailService.SendAsync(email, subject, body);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                        }

                        output.Add("message", "Tạo mới thông tin dịch vụ thành công");
                        break;
                    // CẬP NHẬT THÔNG TIN HỢP ĐỒNG
                    case 2:
                        sql = $"SELECT COUNT(1) AS numb FROM {CommonConstants.SCHEMA_REG}.contract WHERE contract_id = '{model["contractID"]}'";
                        data = await _minvoiceDbContext.GetDataTableAsync(sql);
                        if (data.Rows[0]["numb"].ToString() == "0") throw new Exception($"Không tìm thấy thông tin hợp đồng với mã {model["contractID"]}");

                        // CẬP NHẬT DỮ LIỆU BẢNG CONTRACT
                        parameters.Clear();

                        parameters.Add("contract_id", model["contractID"]?.ToString());

                        parameters.Add("admin_name", model["adminName"]?.ToString());
                        parameters.Add("admin_position", model["adminPosition"]?.ToString());
                        parameters.Add("admin_tel", model["adminTel"]?.ToString());
                        parameters.Add("admin_email", model["adminEmail"]?.ToString());

                        parameters.Add("end_date", DateTime.Parse(model["endDate"]?.ToString()));

                        parameters.Add("payment_type", model["paymentType"]?.ToString());
                        parameters.Add("prepaid_info", model["prepaidInfo"]?.ToString() == null ? "" : model["prepaidInfo"].ToString());

                        int pre_update;
                        if (string.IsNullOrWhiteSpace(model["prepaidMoney"]?.ToString()) || !int.TryParse(model["prepaidMoney"]?.ToString(), out pre_update))
                            parameters.Add("prepaid_money", DBNull.Value);
                        else
                            parameters.Add("prepaid_money", pre_update);

                        parameters.Add("am_id", model["aMID"]?.ToString());
                        parameters.Add("am_name", model["aMName"]?.ToString());
                        parameters.Add("agency_id", model["agencyID"]?.ToString());
                        parameters.Add("agency_name", model["agencyName"]?.ToString());
                        parameters.Add("region_id", model["regionID"]?.ToString());
                        parameters.Add("region_name", model["regionName"]?.ToString());

                        parameters.Add("user_edit", tax_code);
                        parameters.Add("date_edit", DateTime.Now);

                        parameters.Add("time_stamp", model["timeStamp"]?.ToString());

                        parameters.Add("customer_name", model["customerName"]?.ToString());
                        parameters.Add("customer_email", model["customerEmail"]?.ToString());
                        parameters.Add("customer_tel", model["customerTel"]?.ToString());
                        parameters.Add("customer_address", model["customerAddress"]?.ToString());
                        parameters.Add("license_no", model["licenseNo"]?.ToString());

                        sql = $"UPDATE {CommonConstants.SCHEMA_REG}.contract SET customer_name = @customer_name, customer_email = @customer_email, customer_tel = @customer_tel, customer_address = @customer_address, time_stamp = @time_stamp, license_no = @license_no, admin_name = @admin_name, admin_position = @admin_position, admin_tel = @admin_tel, admin_email = @admin_email, end_date = @end_date, payment_type = @payment_type, prepaid_info = @prepaid_info, prepaid_money = @prepaid_money, am_id = @am_id, am_name = @am_name, agency_id = @agency_id, agency_name = @agency_name, region_id =  @region_id, region_name = @region_name, user_edit = @user_edit, date_edit = @date_edit WHERE contract_id = @contract_id";
                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, parameters);
                        output.Add("message", "Cập nhật thông tin dịch vụ thành công");
                        break;
                    // GIA HẠN GÓI
                    case 6:
                        sql = $"SELECT COUNT(1) AS numb FROM {CommonConstants.SCHEMA_REG}.contract WHERE contract_id = '{model["contractID"]}'";
                        data = await _minvoiceDbContext.GetDataTableAsync(sql);
                        if (data.Rows[0]["numb"].ToString() == "0") throw new Exception($"Không tìm thấy thông tin hợp đồng với mã {model["contractID"]}");
                        // CẬP NHẬT
                        parameters.Clear();
                        parameters.Add("contract_id", model["contractID"]?.ToString());

                        parameters.Add("admin_name", model["adminName"]?.ToString());
                        parameters.Add("admin_position", model["adminPosition"]?.ToString());
                        parameters.Add("admin_tel", model["adminTel"]?.ToString());
                        parameters.Add("admin_email", model["adminEmail"]?.ToString());

                        parameters.Add("end_date", DateTime.Parse(model["endDate"]?.ToString()));

                        parameters.Add("payment_type", model["paymentType"]?.ToString());

                        parameters.Add("prepaid_info", model["prepaidInfo"]?.ToString() == null ? "" : model["prepaidInfo"].ToString());

                        int pre_update_giahan;
                        if (string.IsNullOrWhiteSpace(model["prepaidMoney"]?.ToString()) || !int.TryParse(model["prepaidMoney"]?.ToString(), out pre_update_giahan))
                            parameters.Add("prepaid_money", DBNull.Value);
                        else
                            parameters.Add("prepaid_money", pre_update_giahan);

                        parameters.Add("am_id", model["aMID"]?.ToString());
                        parameters.Add("am_name", model["aMName"]?.ToString());
                        parameters.Add("agency_id", model["agencyID"]?.ToString());
                        parameters.Add("agency_name", model["agencyName"]?.ToString());
                        parameters.Add("region_id", model["regionID"]?.ToString());
                        parameters.Add("region_name", model["regionName"]?.ToString());

                        parameters.Add("user_edit", tax_code);
                        parameters.Add("date_edit", DateTime.Now);

                        parameters.Add("taxcode", tax_code);
                        parameters.Add("time_stamp", model["timeStamp"]?.ToString());

                        parameters.Add("customer_name", model["customerName"]?.ToString());
                        parameters.Add("customer_email", model["customerEmail"]?.ToString());
                        parameters.Add("customer_tel", model["customerTel"]?.ToString());
                        parameters.Add("customer_address", model["customerAddress"]?.ToString());
                        parameters.Add("license_no", model["licenseNo"]?.ToString());

                        sql = $"UPDATE {CommonConstants.SCHEMA_REG}.contract SET customer_name = @customer_name, customer_email = @customer_email, customer_tel = @customer_tel, customer_address = @customer_address, time_stamp = @time_stamp, license_no = @license_no, admin_name = @admin_name, admin_position = @admin_position, admin_tel = @admin_tel, admin_email = @admin_email, end_date = @end_date, payment_type = @payment_type, prepaid_info = @prepaid_info, prepaid_money = @prepaid_money, am_id = @am_id, am_name = @am_name, agency_id = @agency_id, agency_name = @agency_name, region_id =  @region_id, region_name = @region_name, user_edit = @user_edit, date_edit = @date_edit WHERE contract_id = @contract_id";
                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, parameters);

                        // GIA HẠN
                        parameters.Clear();
                        var packages = model["listPackage"];

                        if (packages != null && packages.HasValues)
                        {
                            foreach (var package in packages)
                            {
                                var action = int.Parse(package["action"].ToString());
                                // GET DỮ LIỆU BẢNG CONFIG_PACKAGE
                                if (action == 1)
                                {
                                    var package_quantity = 0;
                                    var so_luong_dang_ky = 0;
                                    sql = $"SELECT quantity FROM {CommonConstants.SCHEMA_REG}.config_package WHERE package_code = '{package["packageCode"]}'";
                                    data = await _minvoiceDbContext.GetDataTableAsync(sql);
                                    package_quantity = data.Rows.Count > 0 ? int.Parse(data.Rows[0]["quantity"].ToString()) : package_quantity;

                                    parameters.Add("contract_id", model["contractID"]?.ToString());

                                    parameters.Add("note", package["note"].ToString());

                                    parameters.Add("package_code", package["packageCode"].ToString());
                                    if (string.IsNullOrWhiteSpace(package["quantityPackage"].ToString()) || !int.TryParse(package["quantityPackage"].ToString(), out pre_update_giahan))
                                        parameters.Add("package_quantity", DBNull.Value);
                                    else
                                    {
                                        so_luong_dang_ky = pre_update_giahan * package_quantity;
                                        parameters.Add("package_quantity", so_luong_dang_ky);
                                    }

                                    parameters.Add("user_new", tax_code);
                                    parameters.Add("date_new", DateTime.Now);
                                    if (action == 1)
                                    {
                                        sql = $"INSERT INTO {CommonConstants.SCHEMA_REG}.contract_sub(contract_id, package_code, note, package_quantity, user_new, date_new) \n"
                                                  + "VALUES (@contract_id, @package_code, @note, @package_quantity, @user_new, @date_new)";
                                        await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, parameters);

                                    }
                                    // LICENSE NO 
                                    string certFile = "~/App_Data/MobiFone.pfx";
                                    certFile = HttpContext.Current.Server.MapPath(certFile);
                                    X509Certificate2 cert = new X509Certificate2(certFile, "Mbf@2021");
                                    RSACryptoServiceProvider rsaCrypto = (RSACryptoServiceProvider)cert.PrivateKey;

                                    string xml = $"<ThongTinBanQuyen><MaSoThue>{tax_code}</MaSoThue><SoLuongHD>{so_luong_dang_ky}</SoLuongHD></ThongTinBanQuyen>";

                                    byte[] signBytes = rsaCrypto.SignData(System.Text.Encoding.UTF8.GetBytes(xml),
                                        new SHA1CryptoServiceProvider());
                                    string signature = Convert.ToBase64String(signBytes);

                                    string keyParam = signature.Replace("-", "").Replace("+", "").Replace("/", "").Replace("=", "")
                                        .ToUpper();

                                    string keyBanquyen = $"{keyParam.Substring(5, 4)}-{keyParam.Substring(10, 4)}-{keyParam.Substring(15, 4)}";
                                    string xmlSign = $"<MobiFone>{xml}<Signature>{signature}</Signature><Key_banquyen>{keyBanquyen}</Key_banquyen></MobiFone>";


                                    sql = $"INSERT INTO m{tax_code.Replace("-", "")}.pl_licenseinfo (pl_licenseinfo_id, branch_code, xml_license, quantity_addition, date_reg, key_license) VALUES(uuid_generate_v4(), '{CommonConstants.BRANCH_CODE}', '{xmlSign}', {so_luong_dang_ky}, '{DateTime.Now:yyyy-MM-dd})', '{keyBanquyen}')";

                                    await _minvoiceDbContext.TransactionCommandAsyncNotPool(sql, CommandType.Text, new Dictionary<string, object>());
                                }
                            }
                        }

                        output.Add("message", "Gia hạn thông tin dịch vụ thành công");
                        break;

                    default:
                        break;
                }

                await _minvoiceDbContext.TransactionCommitAsyncNotPool();
                _minvoiceDbContext.CloseTransactionNotPool();
                output.Add("status", 1);
                output.Add("data", string.Empty);
            }
            catch (Exception exception)
            {
                await _minvoiceDbContext.TransactionRollbackAsyncNotPool();
                _minvoiceDbContext.CloseTransactionNotPool();
                output.Add("status", 2);
                output.Add("message", exception.Message);
            }
            return output;
        }

        public async Task<JObject> SavePretreatment(JObject obj, string username)
        {
            var json = new JObject();

            var data = (JArray)obj["data"];

            foreach (var d in data)
            {
                if (d["inv_invoicecode_id"] == null)
                {
                    json.Add("error", "inv_invoicecode_id chưa được nhập");
                    return json;
                }
                var invInvoiceCodeId = d["inv_invoicecode_id"].ToString();
                var dic = new Dictionary<string, object> { { "invInvoiceCodeId", Guid.Parse(invInvoiceCodeId) } };
                var sql =
                    $@"SELECT a.code,a.name,a.inv_notificationdetail_id as inv_invoicecode_id, a.template_code, a.invoice_series, a.quantity, 
                               a.from_number, a.to_number, a.begin_date, a.branch_code, a.current_number
                            FROM #SCHEMA_NAME#.inv_notificationdetail a 
                            INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id
                            INNER JOIN #SCHEMA_NAME#.inv_notification_permission c ON a.inv_notificationdetail_id=c.inv_notificationdetail_id
                            INNER JOIN #SCHEMA_NAME#.wb_user d ON c.wb_user_id=d.wb_user_id
                            INNER JOIN #SCHEMA_NAME#.inv_notification_branch AS e ON a.inv_notificationdetail_id = e.inv_notificationdetail_id
                            WHERE a.inv_notificationdetail_id = @invInvoiceCodeId
                            LIMIT 1;";


                var dtPar = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dic);
                if (dtPar.Rows.Count == 0)
                {
                    json.Add("error", $"inv_invoicecode_id : '{invInvoiceCodeId}' không tồn tại !");
                    return json;
                }

                var row = dtPar.Rows[0];
                d["invoice_series"] = row["invoice_series"].ToString();
                d["template_code"] = row["template_code"].ToString();
                d["invoice_type"] = row["code"].ToString();
                d["branch_code"] = row["branch_code"].ToString();
                d["invoice_name"] = row["name"].ToString();
            }

            obj["data"] = data;
            json = await Save(obj, username);
            return json;
        }

        public async Task<JObject> SaveAndCreateNumberPretreatment(JObject obj, string username)
        {
            var json = new JObject();

            var data = (JArray)obj["data"];

            foreach (var d in data)
            {
                if (d["inv_invoicecode_id"] == null)
                {
                    json.Add("error", "inv_invoicecode_id chưa được nhập");
                    return json;
                }
                var invInvoiceCodeId = d["inv_invoicecode_id"].ToString();
                var dic = new Dictionary<string, object> { { "invInvoiceCodeId", Guid.Parse(invInvoiceCodeId) } };
                var sql =
                    $@"SELECT a.code,a.name,a.inv_notificationdetail_id as inv_invoicecode_id, a.template_code, a.invoice_series, a.quantity, 
                               a.from_number, a.to_number, a.begin_date, a.branch_code, a.current_number
                            FROM #SCHEMA_NAME#.inv_notificationdetail a 
                            INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id
                            INNER JOIN #SCHEMA_NAME#.inv_notification_permission c ON a.inv_notificationdetail_id=c.inv_notificationdetail_id
                            INNER JOIN #SCHEMA_NAME#.wb_user d ON c.wb_user_id=d.wb_user_id
                            INNER JOIN #SCHEMA_NAME#.inv_notification_branch AS e ON a.inv_notificationdetail_id = e.inv_notificationdetail_id
                            WHERE a.inv_notificationdetail_id = @invInvoiceCodeId
                            LIMIT 1;";

                var dtPar = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dic);
                if (dtPar.Rows.Count == 0)
                {
                    json.Add("error", $"inv_invoicecode_id : '{invInvoiceCodeId}' không tồn tại !");
                    return json;
                }

                var row = dtPar.Rows[0];
                d["invoice_series"] = row["invoice_series"].ToString();
                d["template_code"] = row["template_code"].ToString();
                d["invoice_type"] = row["code"].ToString();
                d["branch_code"] = row["branch_code"].ToString();
                d["invoice_name"] = row["name"].ToString();
            }

            obj["data"] = data;
            json = await SaveAndCreateNumber(obj, username);
            return json;
        }

        public async Task<bool> CheckInvoiceCancel(JObject obj)
        {
            //var json = new JObject();
            try
            {
                //
                var dic = new Dictionary<string, object> { { "code", obj["code"].ToString() } };
                dic.Add("invoice_series", obj["invoice_series"].ToString());
                dic.Add("inv_notificationdetail_id", Guid.Parse(obj["inv_notificationdetail_id"].ToString()));
                var sql = $@"SELECT  m.hours,m.minutes,m.date_cancel,d.invoice_series,d.code 
                                FROM #SCHEMA_NAME#.inv_notification_cancel m
                                INNER JOIN #SCHEMA_NAME#.inv_notification_canceldetail d ON m.inv_notification_cancel_id = d.inv_notification_cancel_id
                                WHERE d.code = @code AND d.invoice_series = @invoice_series AND d.inv_notificationdetail_id = @inv_notificationdetail_id";
                DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dic);
                if (tblData.Rows.Count == 0)
                {
                    //json.Add("success","");
                    return true;

                }

                DateTime d;
                int h, m;
                var time = DateTime.Now;
                foreach (DataRow row in tblData.Rows)
                {
                    DateTime.TryParse(row["date_cancel"].ToString(), out d);
                    int.TryParse(row["hours"].ToString(), out h);
                    int.TryParse(row["minutes"].ToString(), out m);

                    if (new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, 0, 0) <
                        new DateTime(d.Year, d.Month, d.Day, h, m, 0, 0)) continue;
                    //json.Add("error", $"Dải hóa đơn {invoiceService} đã hủy bỏ!");
                    return false;
                }
            }
            catch (Exception e)
            {
                //json.Add("error" , e.Message);
                return false;
            }
            return true;
        }

        public async Task<JObject> CheckInvoiceCancel(List<string> valueList)
        {
            var json = new JObject();
            try
            {
                foreach (var sql in valueList.Select(item => $@"SELECT a.* FROM #SCHEMA_NAME#.inv_notificationdetail a 
                                   WHERE a.inv_notificationdetail_id IN (SELECT inv_invoicecode_id FROM #SCHEMA_NAME#.inv_invoiceauth 
									  WHERE inv_invoiceauth_id ='{item}')
									  LIMIT 1;"))
                {
                    var tblData = await _minvoiceDbContext.GetDataTableAsync(sql);
                    if (tblData.Rows.Count == 0)
                        continue;
                    var row = tblData.Rows[0];
                    var obj = new JObject
                    {
                        {"code", row["code"].ToString()}, {"invoice_series", row["invoice_series"].ToString() }, {"inv_notificationdetail_id", row["inv_notificationdetail_id"].ToString()}
                    };
                    var check = await CheckInvoiceCancel(obj);
                    if (check) continue;
                    json.Add("error", $"Dải hóa đơn {row["invoice_series"]} đã hủy bỏ!");
                    return json;
                }

                json.Add("success", "");
                return json;

            }
            catch (Exception e)
            {
                json.Add("error", e.Message);
            }
            return json;
        }

        public async Task<bool> CheckBranchValidation(string value)
        {
            try
            {
                string branchCode = _webHelper.GetDvcs();

                if (!branchCode.Equals("VP"))
                {
                    return false;
                }

                // check điều kiện đơn vị hiện tại có level = 2
                var dic = new Dictionary<string, object> { { "value", value } };
                var sql = "SELECT  m.* FROM #SCHEMA_NAME#.wb_branch m WHERE m.code = @value AND m.tree_rank = 2 and m.tree_sort = 'VP->'||@value||'->' ";
                DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dic);
                //var tblData = await _minvoiceDbContext.GetDataTableAsync(sql);
                if (tblData.Rows.Count == 0)
                    return false;
                else
                {
                    // check điều kiện đơn vị mẹ là khách hàng Bao Tiêu
                    sql = "SELECT b.is_baotieu FROM reg.company b WHERE b.schema_name='#SCHEMA_NAME#'";
                    tblData = await _minvoiceDbContext.GetDataTableAsync(sql);
                    var row = tblData.Rows[0];
                    var rowValue = row["is_baotieu"].ToString();

                    if (tblData.Rows.Count == 1 && rowValue == "True")
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public async Task<JArray> GetDmDvcsByUser()
        {
            JArray data = new JArray();
            try
            {
                Dictionary<string, object> dicParameters = new Dictionary<string, object>();
                dicParameters.Add("code", _webHelper.GetDvcs());

                string sql = "SELECT *,a.code as id,a.name as value, b.name as cqt FROM #SCHEMA_NAME#.wb_branch a LEFT JOIN \n"
               + "#SCHEMA_NAME#.sl_tax_authorities_city b on a.provincial_tax_authorities = b.tax_authorities_manage Where a.code=@code ORDER BY a.code \n";

                DataTable tblWindow = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameters);
                data = JArray.FromObject(tblWindow);
            }
            catch (Exception ex)
            {
                return null;
            }
            return data;
        }

        public async Task<JObject> UploadExcelListUnitAsync(byte[] filesReadToProvider)
        {
            try
            {
                ExcelWorkbook xlsx = new ExcelWorkbook(filesReadToProvider);
                int lastRow = xlsx.GetFirstSheet().LastRowNum;
                int beginRow = 2;
                if (lastRow < beginRow)
                {
                    throw new Exception("File mẫu không đúng!");
                }
                await _minvoiceDbContext.BeginTransactionAsync();
                for (int i = beginRow; i <= lastRow; i++)
                {
                    String sql = "";
                    var parameters = new Dictionary<string, object>();
                    var tax_code = xlsx.GetCellValueFromExcel(i, 0) != null
                        ? xlsx.GetCellValueFromExcel(i, 0).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột A không được để trống");

                    // Validate khách hàng đã tồn tại
                    sql = $"SELECT * FROM {CommonConstants.SCHEMA_REG}.company WHERE taxcode = '{tax_code}'";
                    DataTable dataCompany = await _minvoiceDbContext.GetDataTableAsync(sql);
                    if (dataCompany.Rows.Count > 0)
                        throw new Exception("Đăng kí dịch vụ mới thất bại. Khách hàng đã tồn tại!");

                    // CREATE SCHEMA
                    sql =
                        $"SELECT {CommonConstants.SCHEMA_REG}.clone_schema('{CommonConstants.SCHEMA_SOURCE}','m{tax_code}', true, false);";
                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text,
                        new Dictionary<string, object>());
                    // CREATE COMPANY
                    sql =
                        $"INSERT INTO {CommonConstants.SCHEMA_REG}.company VALUES ('{tax_code}', '{CommonConstants.MINVOICE}', 'm{tax_code}', true)";
                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text,
                        new Dictionary<string, object>());

                    // TẠO BRANCH MỚI
                    var customer_name = xlsx.GetCellValueFromExcel(i, 1) != null
                        ? xlsx.GetCellValueFromExcel(i, 1).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột B không được để trống");
                    var customer_address = xlsx.GetCellValueFromExcel(i, 2) != null
                        ? xlsx.GetCellValueFromExcel(i, 2).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột C không được để trống");
                    var wb_branch_id = Guid.Parse("2aefd9dc-cd42-4741-9d78-44b5990ac2d0");
                    var wb_user_id = Guid.Parse("38e27b13-484b-460d-a64d-88548968d2ae");


                    parameters.Clear();

                    // THÔNG TIN USERNAME
                    var password = CommonConstants.PASS_HASH;
                    var wb_role_id = Guid.Parse("588bb696-7934-44b4-aedc-61acacb0aae4");
                    var customer_email = xlsx.GetCellValueFromExcel(i, 3) != null
                        ? xlsx.GetCellValueFromExcel(i, 3).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột D không được để trống");

                    parameters.Add("wb_user_id", wb_user_id);
                    parameters.Add("username", tax_code);
                    parameters.Add("password", password);
                    parameters.Add("wb_role_id", wb_role_id);
                    parameters.Add("email", customer_email);
                    parameters.Add("fullname", customer_name);
                    parameters.Add("branch_code", CommonConstants.BRANCH_CODE);

                    sql =
                        $"INSERT INTO m{tax_code}.wb_user(wb_user_id,username, password, wb_role_id,email, is_view_user, is_edit_user, is_del_user, is_sign_invoice, is_lock, fullname, branch_code, is_new_user) \n"
                        + "VALUES (@wb_user_id,@username,@password,@wb_role_id,@email,'C','C','C','C','false',@fullname,@branch_code,'C')";
                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                    // THÔNG TIN WB_BRANCH_PERMISSION
                    parameters.Clear();
                    var wb_branch_permission_id = Guid.Parse("af379b19-b759-4ce5-9468-02fe454eccfc");
                    parameters.Add("wb_branch_permission_id", wb_branch_permission_id);
                    parameters.Add("wb_user_id", wb_user_id);
                    parameters.Add("wb_branch_id", wb_branch_id);

                    sql =
                        $"INSERT INTO m{tax_code}.wb_branch_permission(wb_branch_permission_id, wb_user_id, wb_branch_id) VALUES (@wb_branch_permission_id,@wb_user_id,@wb_branch_id)";
                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                    // DỮ LIỆU BẢNG CONTRACT
                    //var prepaid_info = string.IsNullOrEmpty(model["prepaidInfo"]?.ToString()) ? (int?)null : int.Parse(model["prepaidInfo"]?.ToString());
                    //var prepaid_money = string.IsNullOrEmpty(model["prepaidMoney"]?.ToString()) ? (long?)null : int.Parse(model["prepaidMoney"]?.ToString());

                    parameters.Clear();

                    var contract_id = xlsx.GetCellValueFromExcel(i, 4) != null
                        ? xlsx.GetCellValueFromExcel(i, 4)?.ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột E không được để trống"); ;
                    parameters.Add("contract_id", contract_id);

                    parameters.Add("contract_no", xlsx.GetCellValueFromExcel(i, 5)?.ToString());

                    var contract_date = xlsx.GetCellValueFromExcel(i, 6) != null
                        ? xlsx.GetCellValueFromExcel(i, 6)
                        : DBNull.Value;
                    parameters.Add("contract_date", contract_date);

                    var admin_name = xlsx.GetCellValueFromExcel(i, 7) != null
                        ? xlsx.GetCellValueFromExcel(i, 7).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột H không được để trống");
                    parameters.Add("admin_name", admin_name);

                    parameters.Add("admin_position", xlsx.GetCellValueFromExcel(i, 8)?.ToString());
                    parameters.Add("admin_tel", xlsx.GetCellValueFromExcel(i, 9)?.ToString());
                    parameters.Add("admin_email", xlsx.GetCellValueFromExcel(i, 10)?.ToString());

                    var start_date = xlsx.GetCellValueFromExcel(i, 11) != null
                        ? xlsx.GetCellValueFromExcel(i, 11)
                        : DBNull.Value;
                    parameters.Add("start_date", start_date);

                    var end_date = xlsx.GetCellValueFromExcel(i, 12) != null
                        ? xlsx.GetCellValueFromExcel(i, 12)
                        : DBNull.Value;
                    parameters.Add("end_date", end_date);

                    parameters.Add("payment_type", xlsx.GetCellValueFromExcel(i, 13)?.ToString());

                    parameters.Add("prepaid_info",
                        xlsx.GetCellValueFromExcel(i, 14)?.ToString() == null
                            ? ""
                            : xlsx.GetCellValueFromExcel(i, 14)?.ToString());

                    int pre;
                    if (string.IsNullOrWhiteSpace(xlsx.GetCellValueFromExcel(i, 15)?.ToString()) ||
                        !int.TryParse(xlsx.GetCellValueFromExcel(i, 15)?.ToString(), out pre))
                    {
                        parameters.Add("prepaid_money", DBNull.Value);
                    }
                    else
                    {
                        parameters.Add("prepaid_money", pre);
                    }

                    parameters.Add("am_id", xlsx.GetCellValueFromExcel(i, 16)?.ToString());
                    parameters.Add("am_name", xlsx.GetCellValueFromExcel(i, 17)?.ToString());
                    parameters.Add("agency_id", xlsx.GetCellValueFromExcel(i, 18)?.ToString());
                    parameters.Add("agency_name", xlsx.GetCellValueFromExcel(i, 19)?.ToString());
                    parameters.Add("region_id", xlsx.GetCellValueFromExcel(i, 20)?.ToString());
                    parameters.Add("region_name", xlsx.GetCellValueFromExcel(i, 21)?.ToString());

                    parameters.Add("user_new", tax_code);
                    parameters.Add("date_new", DateTime.Now);

                    parameters.Add("taxcode", tax_code);
                    parameters.Add("time_stamp", xlsx.GetCellValueFromExcel(i, 22)?.ToString());

                    parameters.Add("customer_id", xlsx.GetCellValueFromExcel(i, 23)?.ToString());
                    parameters.Add("customer_name", customer_name);
                    parameters.Add("customer_email", customer_email);
                    parameters.Add("customer_tel", xlsx.GetCellValueFromExcel(i, 24)?.ToString());
                    parameters.Add("customer_address", customer_address);
                    parameters.Add("license_no", xlsx.GetCellValueFromExcel(i, 25)?.ToString());

                    sql =
                        $"INSERT INTO {CommonConstants.SCHEMA_REG}.contract(customer_id, customer_name, customer_email, customer_tel, customer_address, time_stamp, license_no, contract_id,contract_no, contract_date, admin_name,admin_position, admin_tel, admin_email, start_date, end_date, payment_type, prepaid_info, prepaid_money, am_id, am_name, agency_id, agency_name, region_id, region_name, user_new, date_new, taxcode) \n"
                        + "VALUES (@customer_id, @customer_name, @customer_email, @customer_tel, @customer_address, @time_stamp, @license_no, @contract_id,@contract_no,@contract_date,@admin_name,@admin_position, @admin_tel, @admin_email, @start_date, @end_date, @payment_type, @prepaid_info, @prepaid_money, @am_id, @am_name, @agency_id, @agency_name, @region_id, @region_name, @user_new, @date_new, @taxcode)";

                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                    parameters.Clear();

                    parameters.Add("loai", 1);

                    var mau01_id = Guid.NewGuid();
                    parameters.Add("mau01_id", mau01_id);

                    var tnnt = xlsx.GetCellValueFromExcel(i, 26) != null
                        ? xlsx.GetCellValueFromExcel(i, 26).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AA không được để trống");
                    parameters.Add("tnnt", tnnt);

                    var mst = xlsx.GetCellValueFromExcel(i, 27) != null
                        ? xlsx.GetCellValueFromExcel(i, 27).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AB không được để trống");
                    parameters.Add("mst", mst);

                    var cqtqly = xlsx.GetCellValueFromExcel(i, 28) != null
                        ? xlsx.GetCellValueFromExcel(i, 28).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AC không được để trống");
                    parameters.Add("cqtqly", cqtqly);

                    var mcqtqly = xlsx.GetCellValueFromExcel(i, 29) != null
                        ? xlsx.GetCellValueFromExcel(i, 29).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AD không được để trống");
                    parameters.Add("mcqtqly", mcqtqly);

                    var ddanh = xlsx.GetCellValueFromExcel(i, 30) != null
                        ? xlsx.GetCellValueFromExcel(i, 30).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AE không được để trống");
                    parameters.Add("ddanh", ddanh);

                    var nlhe = xlsx.GetCellValueFromExcel(i, 31) != null
                        ? xlsx.GetCellValueFromExcel(i, 31).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AF không được để trống");
                    parameters.Add("nlhe", nlhe);

                    var dtlhe = xlsx.GetCellValueFromExcel(i, 32) != null
                        ? xlsx.GetCellValueFromExcel(i, 32).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AG không được để trống");
                    parameters.Add("dtlhe", dtlhe);

                    var dclhe = xlsx.GetCellValueFromExcel(i, 33) != null
                        ? xlsx.GetCellValueFromExcel(i, 33).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AH không được để trống");
                    parameters.Add("dclhe", dclhe);

                    var dctdtu = xlsx.GetCellValueFromExcel(i, 34) != null
                        ? xlsx.GetCellValueFromExcel(i, 34).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AI không được để trống");
                    parameters.Add("dctdtu", dctdtu);

                    var adhdcma = xlsx.GetCellValueFromExcel(i, 35) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 35).ToString())
                        : 0;
                    parameters.Add("adhdcma", adhdcma);

                    var adhdkma = xlsx.GetCellValueFromExcel(i, 36) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 36).ToString())
                        : 0;
                    parameters.Add("adhdkma", adhdkma);

                    var htgdlhddtcma = xlsx.GetCellValueFromExcel(i, 37) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 37).ToString())
                        : 0;
                    parameters.Add("htgdlhddtcma", htgdlhddtcma);

                    var htgdlhddtcmkkhan = xlsx.GetCellValueFromExcel(i, 38) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 38).ToString())
                        : 0;
                    parameters.Add("htgdlhddtcmkkhan", htgdlhddtcmkkhan);

                    var htgdlhddtcmkcncao = xlsx.GetCellValueFromExcel(i, 39) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 39).ToString())
                        : 0;
                    parameters.Add("htgdlhddtcmkcncao", htgdlhddtcmkcncao);

                    var htgdlhddtkma = xlsx.GetCellValueFromExcel(i, 40) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 40).ToString())
                        : 0;
                    parameters.Add("htgdlhddtkma", htgdlhddtkma);

                    var htgdlhddtkmttiep = xlsx.GetCellValueFromExcel(i, 41) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 41).ToString())
                        : 0;
                    parameters.Add("htgdlhddtkmttiep", htgdlhddtkmttiep);

                    var htgdlhddtkmtchuc = xlsx.GetCellValueFromExcel(i, 42) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 42).ToString())
                        : 0;
                    parameters.Add("htgdlhddtkmtchuc", htgdlhddtkmtchuc);

                    var ptcdlhdndthdon = xlsx.GetCellValueFromExcel(i, 43) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 43).ToString())
                        : 0;
                    parameters.Add("ptcdlhdndthdon", ptcdlhdndthdon);

                    var ptcdlhdbthop = xlsx.GetCellValueFromExcel(i, 44) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 44).ToString())
                        : 0;
                    parameters.Add("ptcdlhdbthop", ptcdlhdbthop);

                    var hdgtgt = xlsx.GetCellValueFromExcel(i, 45) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 45).ToString())
                        : 0;
                    parameters.Add("hdgtgt", hdgtgt);

                    var hdbhang = xlsx.GetCellValueFromExcel(i, 46) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 46).ToString())
                        : 0;
                    parameters.Add("hdbhang", hdbhang);

                    var hdbtscong = xlsx.GetCellValueFromExcel(i, 47) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 47).ToString())
                        : 0;
                    parameters.Add("hdbtscong", hdbtscong);

                    var hdbhdtqgia = xlsx.GetCellValueFromExcel(i, 48) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 48).ToString())
                        : 0;
                    parameters.Add("hdbhdtqgia", hdbhdtqgia);

                    var hdkhac = xlsx.GetCellValueFromExcel(i, 49) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 49).ToString())
                        : 0;
                    parameters.Add("hdkhac", hdkhac);

                    var qlnhdon = xlsx.GetCellValueFromExcel(i, 50) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 50).ToString())
                        : 0;
                    parameters.Add("qlnhdon", qlnhdon);

                    parameters.Add("ldkunhiem", DBNull.Value);

                    parameters.Add("nlap", DateTime.Now);

                    parameters.Add("mdvi", CommonConstants.BRANCH_CODE);

                    sql = $@"INSERT INTO m{tax_code}.mau01_68(mau01_id, loai, tnnt, mst, cqtqly, mcqtqly, ddanh, 
                                nlhe, dtlhe, dclhe, dctdtu, adhdcma, adhdkma, htgdlhddtcma, htgdlhddtcmkkhan, 
                                htgdlhddtcmkcncao, htgdlhddtkma, htgdlhddtkmttiep, htgdlhddtkmtchuc, ptcdlhdndthdon, 
                                ptcdlhdbthop, hdgtgt, hdbhang, hdbtscong, hdbhdtqgia, hdkhac, qlnhdon, ldkunhiem, nlap, 
                                mdvi)
                            VALUES(@mau01_id, @loai, @tnnt, @mst, @cqtqly, @cqtqly, @ddanh, @nlhe, @dtlhe, @dclhe, 
                                @dctdtu, @adhdcma, @adhdkma, @htgdlhddtcma, @htgdlhddtcmkkhan, @htgdlhddtcmkcncao, 
                                @htgdlhddtkma, @htgdlhddtkmttiep, @htgdlhddtkmtchuc, @ptcdlhdndthdon, @ptcdlhdbthop, 
                                @hdgtgt, @hdbhang, @hdbtscong, @hdbhdtqgia, @hdkhac, @qlnhdon, @ldkunhiem, @nlap, @mdvi)";

                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                    parameters.Clear();

                    parameters.Add("mau01_id", mau01_id);

                    var mau01_chitiet_id = Guid.NewGuid();
                    parameters.Add("mau01_chitiet_id", mau01_chitiet_id);

                    var ttchuc = xlsx.GetCellValueFromExcel(i, 51) != null
                        ? xlsx.GetCellValueFromExcel(i, 51).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột AZ không được để trống");
                    parameters.Add("ttchuc", ttchuc);

                    var seri = xlsx.GetCellValueFromExcel(i, 52) != null
                        ? xlsx.GetCellValueFromExcel(i, 52).ToString()
                        : throw new Exception("Dòng " + (i + 1) + " cột BA không được để trống");
                    parameters.Add("seri", seri);

                    var tngay = xlsx.GetCellValueFromExcel(i, 53) != null
                        ? DateTime.Parse(xlsx.GetCellValueFromExcel(i, 53).ToString())
                        : throw new Exception("Dòng " + (i + 1) + " cột BB không được để trống");
                    parameters.Add("tngay", tngay);

                    var dngay = xlsx.GetCellValueFromExcel(i, 54) != null
                        ? DateTime.Parse(xlsx.GetCellValueFromExcel(i, 54).ToString())
                        : throw new Exception("Dòng " + (i + 1) + " cột BC không được để trống");
                    parameters.Add("dngay", dngay);

                    var hthuc = xlsx.GetCellValueFromExcel(i, 55) != null
                        ? int.Parse(xlsx.GetCellValueFromExcel(i, 55).ToString())
                        : throw new Exception("Dòng " + (i + 1) + " cột BD không được để trống");
                    parameters.Add("hthuc", hthuc);

                    sql = $@"INSERT INTO m{tax_code}.mau01_68_chitiet(mau01_id, mau01_chitiet_id, ttchuc, seri, tngay, 
                                              dngay, hthuc) 
                                    VALUES (@mau01_id, @mau01_chitiet_id, @ttchuc, @seri, @tngay, @dngay, @hthuc)";

                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);
                }

                await _minvoiceDbContext.TransactionCommitAsync();
                var result = new JObject();
                result.Add("ok", true);
                result.Add("status", "server");
                return result;
            }
            catch (Exception ex)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                Console.WriteLine(ex);
                throw;
            }
            finally
            {
                _minvoiceDbContext.CloseTransaction();
            }
        }

        public async Task<JObject> GetBusinessInfoByTaxCode(string taxCode)
        {
            try
            {
                var result = new JObject();
                if (string.IsNullOrEmpty(taxCode))
                {
                    result.Add("error", $"Mã số thuế không hợp lệ!");
                    return result;
                }
                string sql = $@"SELECT business_id, tax_code, business_name, trading_name, issue_date, active_status, 
                                    address, tax_authority, phone, business_owner
                                FROM reg.business 
                                WHERE tax_code='{taxCode}'";
                DataTable resultTable = await _minvoiceDbContext.GetDataTableAsync(sql);
                if (resultTable.Rows.Count == 0)
                {
                    result.Add("error", $"Chưa có thông tin doanh nghiệp!");
                    return result;
                }
                var obj = (JArray)JObject.FromObject(resultTable.Rows[0]).GetValue("Table");
                result = (JObject)obj[0];
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var result = new JObject("error", ex.Message);
                return result;
            }
        }

        public void SaveBusinessInfo(JObject resultObject)
        {
            try
            {
                string taxCode = $@"{resultObject["mst"]}";
                var businessName = $@"{resultObject["tennnt"]}";
                var activeStatus = $@"{resultObject["tthai"]}";
                var address = $@"{resultObject["dctsdchi"]}, {resultObject["dctstxa"]}, {resultObject["dctsthuyen"]}, {resultObject["dctstinh"]}";
                var taxAuthority = $@"{resultObject["tencqt"]}";

                var sql =
                    $@"INSERT INTO reg.business(tax_code, business_name, active_status, address, tax_authority) 
                            VALUES ('{taxCode}', '{businessName}', '{activeStatus}', '{address}', '{taxAuthority}')";
                _minvoiceDbContext.ExecuteNoneQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<JObject> UpdateInfoCompany(JObject model)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("name", model["name"]?.ToString());
                parameters.Add("address", model["address"]?.ToString());
                parameters.Add("tel", model["tel"]?.ToString());
                parameters.Add("fax", model["fax"]?.ToString());
                parameters.Add("email", model["email"]?.ToString());
                parameters.Add("manager", model["manager"]?.ToString());
                parameters.Add("bank_account", model["bank_account"]?.ToString());
                parameters.Add("bank_name", model["bank_name"]?.ToString());
                parameters.Add("district", model["district"]?.ToString());
                parameters.Add("city", model["city"]?.ToString());
                parameters.Add("tax_code", model["tax_code"]?.ToString());

                string sql = "UPDATE #SCHEMA_NAME#.wb_branch SET \n" +
                    "   name= @name, \n" +
                    "   address= @address , \n" +
                    "   tel= @tel, \n" +
                    "   fax= @fax, \n" +
                    "   email= @email , \n" +
                    "   manager= @manager, \n" +
                    "   bank_account= @bank_account, \n" +
                    "   bank_name= @bank_name , \n" +
                    "   district= @district, \n" +
                    "   city= @city \n" +
                    "   WHERE tax_code=@tax_code";

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);


                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

    }
}
