﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceApi.Data;

namespace InvoiceApi.Services
{
    public partial interface INopDbContext
    {
        //string GetCurrentRequest();
        InvoiceDbContext GetInvoiceDb();
        PostgresDbContext GetPostgresDb();
        string ExecuteStoreProcedure(string sql, Dictionary<string, string> parameters);
        DataTable ExecuteCmd(string sql);
        DataTable ExecuteCmd(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataTable> ExecuteCmdAsync(string sql);
        Task<string> ExecuteStoreProcedureAsync(string sql,Dictionary<string, object> parameters);
        DataTable GetStoreProcedureParameters(string storeProcedure);
        DataSet GetDataSet(string sql, Dictionary<string, string> parameters);

        void ExecuteNoneQuery(string sql);
        void ExecuteNoneQuery(string sql, Dictionary<string, object> parameters);
        Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
    }
}
