﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using InvoiceApi.Data;
using InvoiceApi.Data.Domain;
using VACOMLIB;
using InvoiceApi.Util;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public class AccountService : IAccountService
    {
        #region Fields

        private IMinvoiceDbContext _minvoiceDbContext;
        private INopDbContext _nopDbContext;
        private ILogService _logService;
        private IEmailService _emailService;
        private IWebHelper _webHelper;

        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh";

        #endregion

        #region Ctor

        public AccountService(INopDbContext nopDbContext,
                        IEmailService emailService,
                        ILogService logService,
                        IWebHelper webHelper,
                        IMinvoiceDbContext minvoiceDbContext
            )
        {
            this._nopDbContext = nopDbContext;
            this._emailService = emailService;
            this._logService = logService;
            this._webHelper = webHelper;
            this._minvoiceDbContext = minvoiceDbContext;
        }

        #endregion

        #region Methods
        public wb_user GetAccountByUserName(string loginName)
        {
            CacheManager cacheManager = new CacheManager(_minvoiceDbContext);
            DataTable tblWbUser = cacheManager.GetUserLogin(loginName);

            if (tblWbUser.Rows.Count == 0) return null;

            wb_user user = new wb_user();
            user.username = tblWbUser.Rows[0]["username"].ToString();
            user.password = tblWbUser.Rows[0]["password"].ToString();
            user.id = Guid.Parse(tblWbUser.Rows[0]["wb_user_id"].ToString());
            user.store_code = tblWbUser.Rows[0]["store_code"].ToString();
            return user;
        }

        public async Task<JObject> Login(string username, string password, string ipAddress, string userAgent, string ma_dvcs)
        {

            JObject json = new JObject();
            try
            {
                int? attempLogin = null;
                DateTime? timeLock = null;

                if (password.Length < 6)
                {
                    json.Add("error", "Mật khẩu phải từ 6 ký tự trở lên");
                    return json;
                }

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("username", username);

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username";
                DataTable tblWbUser = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblWbUser.Rows.Count == 0)
                {
                    json.Add("error", "Tên đăng nhập hoặc mật khẩu không đúng ");
                }
                else
                {
                    if (string.IsNullOrEmpty(ma_dvcs))
                    {
                        ma_dvcs = tblWbUser.Rows[0]["branch_code"].ToString();
                    }

                    parameters.Clear();
                    parameters.Add("code", ma_dvcs);

                    DataTable tblDvcs = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@code", CommandType.Text, parameters);

                    if (tblDvcs.Rows.Count == 0)
                    {
                        json.Add("error", string.Format("Không tìm thấy đơn vị đăng nhập: {0}", ma_dvcs));
                        return json;
                    }

                    string wb_branch_id = tblDvcs.Rows[0]["wb_branch_id"].ToString();
                    string wb_user_id = tblWbUser.Rows[0]["wb_user_id"].ToString();

                    parameters.Clear();
                    parameters.Add("wb_user_id", Guid.Parse(wb_user_id));
                    parameters.Add("wb_branch_id", Guid.Parse(wb_branch_id));

                    DataTable tblBranchPermission = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch_permission WHERE wb_user_id=@wb_user_id AND wb_branch_id=@wb_branch_id", CommandType.Text, parameters);

                    if (tblBranchPermission.Rows.Count == 0)
                    {
                        json.Add("error", "Bạn không có quyền đăng nhập đơn vị này ");
                        return json;
                    }

                    bool isLock = tblWbUser.Rows[0]["is_lock"] == DBNull.Value ? false : Convert.ToBoolean(tblWbUser.Rows[0]["is_lock"]);

                    if (isLock)
                    {
                        timeLock = Convert.ToDateTime(tblWbUser.Rows[0]["time_lock"]);

                        if (DateTime.Now < timeLock)
                        {
                            json.Add("error", "Tài khoản đang tạm thời bị khóa");
                            return json;
                        }
                        else
                        {
                            isLock = false;
                            timeLock = null;
                            attempLogin = null;
                        }
                    }

                    string pass = tblWbUser.Rows[0]["password"].ToString();

                    Encrypt crypt = new Encrypt(pass);

                    if (crypt.CheckPassword(password))
                    {
                        string token = GenerateToken(username, pass, ipAddress, userAgent, DateTime.Now.Ticks, ma_dvcs);

                        isLock = false;
                        timeLock = null;
                        attempLogin = null;

                        parameters.Clear();
                        parameters.Add("username", username);

                        sql = "UPDATE #SCHEMA_NAME#.wb_user SET is_lock=false,time_lock=null,attemp_login=null WHERE username=@username";
                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                        json.Add("token", token);
                        json.Add("ma_dvcs", tblWbUser.Rows[0]["branch_code"].ToString());
                        json.Add("wb_user_id", wb_user_id);
                    }
                    else
                    {
                        attempLogin = tblWbUser.Rows[0]["attemp_login"] == DBNull.Value ? 0 : Convert.ToInt32(tblWbUser.Rows[0]["attemp_login"]);
                        attempLogin = attempLogin + 1;

                        if (attempLogin > 10)
                        {
                            isLock = true;
                            timeLock = DateTime.Now.AddMinutes(5);
                        }

                        parameters = new Dictionary<string, object>();
                        parameters.Add("is_lock", isLock);
                        parameters.Add("time_lock", timeLock);
                        parameters.Add("attemp_login", attempLogin);
                        parameters.Add("username", username);

                        sql = "UPDATE #SCHEMA_NAME#.wb_user SET is_lock=@is_lock,time_lock=@time_lock,attemp_login=@attemp_login WHERE username=@username";
                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                        json.Add("error", "Tên hoặc mật khẩu không đúng");
                    }
                }
            }
            catch (Exception e)
            {
                json.Add("error", e.Message);
                return json;
            }

            return json;
        }

        public async Task<bool> CheckPassword(string username, string password)
        {

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("username", username);

            DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

            if (tblUser.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                string pass = tblUser.Rows[0]["password"].ToString();

                Encrypt crypt = new Encrypt(pass);

                if (crypt.CheckPassword(password))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }


        }

        public async Task<JObject> ChangePass(string username, string password)
        {
            JObject json = new JObject();

            try
            {
                Encrypt crypt = new Encrypt(password);
                string hashPassword = crypt.CreateHashedPassword(password, null);

                string sql = "UPDATE #SCHEMA_NAME#.wb_user SET password=@password WHERE username=@username";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("password", hashPassword);
                parameters.Add("username", username);

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                CacheManager cacheManager = new CacheManager(_minvoiceDbContext);
                cacheManager.removeUserLogin(username);

                json.Add("ok", true);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }


            return json;
        }

        public string GenerateToken(string username, string password, string ip, string userAgent, long ticks, string ma_svcs)
        {
            string hash = string.Join(":", new string[] { username, "", ticks.ToString(), ma_svcs });
            string hashLeft = "";
            string hashRight = "";

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));

                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username, ticks.ToString(), ma_svcs });
            }

            string token = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));

            return token;
        }

        private string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

                return Convert.ToBase64String(hmac.Hash);
            }
        }

        public async Task<JArray> GetUsers()
        {
            /*string userLogin = _webHelper.GetUser();

            var invoiceDb = this._nopDbContext.GetInvoiceDb();

            wb_user user = invoiceDb.WbUsers.Where(c => c.username == userLogin).FirstOrDefault<wb_user>();
            wb_menu menu = invoiceDb.WbMenus.Where(c => c.code == "account").FirstOrDefault<wb_menu>();

            if (menu == null)
            {
                return null;
            }

            wb_ctquyen ctquyen = invoiceDb.WbCtquyens.Where(c => c.wb_nhomquyen_id == user.wb_nhomquyen_id 
                            && c.wb_menu_id == menu.id).FirstOrDefault<wb_ctquyen>();

            if (ctquyen == null)
            {
                return null;
            }*/
            string sql = "SELECT *,wb_user_id as id FROM #SCHEMA_NAME#.wb_user ORDER BY username";
            DataTable tblWbUser = await this._minvoiceDbContext.GetDataTableAsync(sql);

            foreach (DataRow row in tblWbUser.Rows)
            {
                row["password"] = DBNull.Value;
            }

            return JArray.FromObject(tblWbUser);
        }

        public async Task<JArray> GetUsersByBranch()
        {
            string userLogin = _webHelper.GetUser();

            Dictionary<string, object> dicPara = new Dictionary<string, object>();
            dicPara.Add("username", userLogin);

            DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicPara);

            if (tblUser.Rows.Count == 0)
            {
                throw new Exception("Không tìm thấy người sử dụng");
            }

            DataTable tblMenu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_menu WHERE code='account'");

            if (tblMenu.Rows.Count == 0)
            {
                throw new Exception("Không tìm thấy chức năng tài khoản");
            }

            DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, dicPara);

            if (tblNhomQuyen.Rows.Count == 0)
            {
                throw new Exception("Không tìm thấy nhóm quyền của người sử dụng");
            }

            string wb_menu_id = tblMenu.Rows[0]["wb_menu_id"].ToString();

            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("wb_role_id", Guid.Parse(tblNhomQuyen.Rows[0]["wb_role_id"].ToString()));
            parameters.Add("wb_menu_id", Guid.Parse(wb_menu_id));

            DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id=@wb_menu_id", CommandType.Text, parameters);

            if (tblCtQuyen.Rows.Count == 0)
            {
                throw new Exception("Bạn không được phân quyền chức năng tài khoản");
            }

            string branch_code = tblUser.Rows[0]["branch_code"].ToString();
            string sql = "SELECT a.*,a.wb_user_id as id FROM #SCHEMA_NAME#.wb_user a \n"
                       + "INNER JOIN #SCHEMA_NAME#.wb_branch b ON a.branch_code=b.code \n"
                       + "WHERE POSITION('" + branch_code + "->' IN b.tree_sort)>0 ";


            tblUser = await _minvoiceDbContext.GetDataTableAsync(sql);

            return JArray.FromObject(tblUser);
        }

        public async Task<JArray> GetUsersByStore()
        {
            string userLogin = _webHelper.GetUser();

            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("username", userLogin);

            DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

            if (tblUser.Rows.Count == 0)
            {
                return new JArray();
            }

            string branch_code = tblUser.Rows[0]["branch_code"].ToString();
            string store_code = tblUser.Rows[0]["store_code"].ToString();

            parameters.Clear();
            parameters.Add("branch_code", branch_code);
            parameters.Add("store_code", store_code);

            string sql = "SELECT *,wb_user_id as id FROM #SCHEMA_NAME#.wb_user WHERE branch_code=@branch_code AND store_code=@store_code ORDER BY username";

            tblUser = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

            return JArray.FromObject(tblUser);
        }

        public async Task<JObject> SaveChange(wb_user model)
        {
            JObject json = new JObject();
            string pass = "";

            try
            {
                string _user = _webHelper.GetUser();

                /*wb_user userLogin = invoiceDb.WbUsers.Where(c => c.username == _user).FirstOrDefault<wb_user>();

                wb_menu menu = invoiceDb.WbMenus.Where(c => c.code == "account").FirstOrDefault<wb_menu>();

                if (menu == null)
                {
                    json.Add("error", "Không tìm thấy chức năng tài khoản");
                    return json;
                }

                wb_ctquyen ctquyen = invoiceDb.WbCtquyens.Where(c => c.wb_nhomquyen_id == userLogin.wb_nhomquyen_id
                            && c.wb_menu_id == menu.id).FirstOrDefault<wb_ctquyen>();

                if (ctquyen == null)
                {
                    json.Add("error", "Bạn không được phân quyền chức năng này");
                    return json;
                }

                var acc = invoiceDb.WbUsers.Where(c => c.username == model.username && c.id != model.id).FirstOrDefault<wb_user>();

                if (acc != null)
                {
                    json.Add("error", "Tên truy cập đã có");
                    return json;
                }

                acc = invoiceDb.WbUsers.Where(c => c.email == model.email && c.id != model.id).FirstOrDefault<wb_user>();

                if (acc != null)
                {
                    json.Add("error", "Email này đã có");
                    return json;
                }*/

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("email", model.email);
                parameters.Add("wb_user_id", model.id);

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_user WHERE email=@email AND wb_user_id<>@wb_user_id";

                DataTable tblWbUser = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblWbUser.Rows.Count > 0)
                {
                    json.Add("error", "Email này đã có");
                    return json;
                }

                parameters.Clear();
                parameters.Add("username", model.username);
                parameters.Add("wb_user_id", model.id);

                sql = "SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username AND wb_user_id<>@wb_user_id";

                tblWbUser = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblWbUser.Rows.Count > 0)
                {
                    json.Add("error", "Tên truy cập đã có");
                    return json;
                }

                parameters.Clear();
                parameters.Add("username", _user);

                DataTable tblUserLogin = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);
                string wb_role_id = tblUserLogin.Rows[0]["wb_role_id"].ToString();

                string window_type = model.window_type;

                parameters.Clear();
                parameters.Add("code", window_type);

                DataTable tblMenu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM  #SCHEMA_NAME#.wb_menu WHERE code=@code", CommandType.Text, parameters);

                if (tblMenu.Rows.Count == 0)
                {
                    json.Add("error", "Không tìm thấy chức năng này");
                    return json;
                }

                string wb_menu_id = tblMenu.Rows[0]["wb_menu_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_role_id", Guid.Parse(wb_role_id));
                parameters.Add("wb_menu_id", Guid.Parse(wb_menu_id));

                DataTable tblDetailRole = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM  #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id=@wb_menu_id", CommandType.Text, parameters);

                if (tblDetailRole.Rows.Count == 0)
                {
                    json.Add("error", "Bạn không được phân quyền chức năng này");
                    return json;
                }

                Guid id = Guid.NewGuid();

                if (model.id.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    string _them = tblDetailRole.Rows[0]["has_new"].ToString();

                    if (_them != "C")
                    {
                        json.Add("error", "Bạn không có quyền thêm mới");
                        return json;
                    }

                    Encrypt crypt = new Encrypt(pass);
                    pass = CommonManager.RandomPassword(8);

                    string branch_code = model.branch_code;

                    if (window_type != "account")
                    {
                        branch_code = tblUserLogin.Rows[0]["branch_code"].ToString();

                        if (window_type == "accountstore")
                        {
                            model.store_code = tblUserLogin.Rows[0]["store_code"].ToString();
                        }

                    }

                    parameters = new Dictionary<string, object>();

                    parameters.Add("wb_user_id", id);
                    parameters.Add("username", model.username);
                    parameters.Add("fullname", model.fullname);
                    parameters.Add("password", crypt.CreateHashedPassword(pass, null));
                    parameters.Add("email", model.email);
                    parameters.Add("explanation", model.dien_giai);
                    parameters.Add("wb_role_id", model.wb_role_id);
                    parameters.Add("is_view_user", "C");
                    parameters.Add("is_edit_user", "C");
                    parameters.Add("is_del_user", "C");
                    parameters.Add("is_new_user", "C");
                    parameters.Add("is_sign_invoice", "C");
                    parameters.Add("branch_code", branch_code);
                    parameters.Add("store_code", model.store_code);

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_user(wb_user_id,username,password,email,explanation,wb_role_id,is_view_user,is_edit_user,is_del_user,is_sign_invoice,fullname,branch_code,store_code,is_new_user) \n"
                                + "VALUES (@wb_user_id,@username,@password,@email,@explanation,@wb_role_id,@is_view_user,@is_edit_user,@is_del_user,@is_sign_invoice,@fullname,@branch_code,@store_code,@is_new_user)";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                    parameters.Clear();
                    parameters.Add("code", branch_code);

                    DataTable tblDvcs = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@code", CommandType.Text, parameters);

                    parameters.Clear();

                    parameters.Add("wb_user_id", id);
                    parameters.Add("wb_branch_permission_id", Guid.NewGuid());
                    parameters.Add("wb_branch_id", Guid.Parse(tblDvcs.Rows[0]["wb_branch_id"].ToString()));

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_branch_permission(wb_branch_permission_id, wb_user_id, wb_branch_id) \n"
                        + "VALUES (@wb_branch_permission_id, @wb_user_id, @wb_branch_id)";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                }
                else
                {
                    /*string sua = ctquyen.sua == null ? "" : ctquyen.sua;

                    if (sua != "C")
                    {
                        json.Add("error", "Bạn không có quyền sửa");
                        return json;
                    }

                    var window = invoiceDb.WbUsers.Find(model.id);
                    window.username = model.username;
                    window.wb_nhomquyen_id = model.wb_nhomquyen_id;
                    window.dien_giai = model.dien_giai;
                    window.email = model.email;
                    window.isdeluser = model.isdeluser;
                    window.isedituser = model.isedituser;
                    window.issigninvoice = model.issigninvoice;
                    window.isviewuser = model.isviewuser;

                    invoiceDb.Entry(window).State = System.Data.Entity.EntityState.Modified;*/

                    string _sua = tblDetailRole.Rows[0]["has_edit"].ToString();

                    if (_sua != "C")
                    {
                        json.Add("error", "Bạn không có quyền sửa");
                        return json;
                    }

                    id = model.id;
                    string branch_code = model.branch_code;
                    string store_code = model.store_code;

                    if (window_type != "account")
                    {
                        branch_code = tblUserLogin.Rows[0]["branch_code"].ToString();

                        if (window_type == "accountstore")
                        {
                            store_code = tblUserLogin.Rows[0]["store_code"].ToString();
                        }

                    }

                    parameters.Clear();
                    parameters.Add("wb_user_id", model.id);

                    sql = "SELECT * FROM #SCHEMA_NAME#.wb_user WHERE wb_user_id=@wb_user_id";
                    tblWbUser = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                    DataRow row = tblWbUser.Rows[0];

                    if (_webHelper.GetUser() == row["username"].ToString() && row["wb_role_id"].ToString() != model.wb_role_id.ToString())
                    {
                        throw new Exception("Bạn không có quyền chỉnh sửa quyền của mình!");
                    }

                    row.BeginEdit();
                    row["username"] = model.username;
                    row["fullname"] = model.fullname;
                    row["email"] = model.email;
                    row["wb_role_id"] = model.wb_role_id;
                    row["explanation"] = model.dien_giai;
                    row["store_code"] = store_code;
                    row["branch_code"] = branch_code;
                    row["wb_user_id"] = model.id;
                    row.EndEdit();

                    sql = "UPDATE #SCHEMA_NAME#.wb_user SET store_code=@store_code,username=@username,email=@email,wb_role_id=@wb_role_id,explanation=@explanation,"
                         + "is_view_user=@is_view_user,is_edit_user=@is_edit_user,is_sign_invoice=@is_sign_invoice,is_del_user=@is_del_user,fullname=@fullname,branch_code=@branch_code \n"
                         + "WHERE wb_user_id=@wb_user_id";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, row);


                }

                //invoiceDb.SaveChanges();

                if (pass.Length > 0)
                {
                    DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='CREATE_ACCOUNT_LOGIN' AND yesno='C' LIMIT 1");

                    if (tblEmail.Rows.Count > 0)
                    {
                        string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                        if (smtp_address.Length > 0)
                        {
                            int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                            bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                            this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                        }

                        string subject = tblEmail.Rows[0]["subject"].ToString();
                        string body = tblEmail.Rows[0]["body"].ToString()
                                        .Replace("#username#", model.username)
                                        .Replace("#password#", pass)
                                        .Replace("#url_host#", _webHelper.GetUrlHost());

                        this._emailService.Send(model.email, subject, body);
                    }

                }

                parameters.Clear();
                parameters.Add("wb_user_id", id);

                string _sql = "SELECT wb_user_id as id, wb_user_id, username, wb_role_id, explanation, email, is_view_user, is_edit_user, is_del_user, is_sign_invoice, fullname, branch_code, store_code, is_new_user FROM #SCHEMA_NAME#.wb_user "
                        + "WHERE wb_user_id=@wb_user_id";

                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(_sql, CommandType.Text, parameters);
                JArray _array = JArray.FromObject(tblData);

                if (_array.Count > 0)
                {
                    json.Add("item", _array[0]);
                }

                json.Add("ok", true);
                json.Add("id", id);

                CacheManager cacheManager = new CacheManager(_minvoiceDbContext);
                cacheManager.removeUserLogin(model.username);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }


        public async Task<JObject> Delete(wb_user model)
        {
            JObject json = new JObject();
            var invoiceDb = this._nopDbContext.GetInvoiceDb();

            try
            {
                string user = _webHelper.GetUser();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("username", user);

                DataTable tblUserLogin = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);
                string wb_role_id = tblUserLogin.Rows[0]["wb_role_id"].ToString();

                string window_type = model.window_type;

                parameters.Clear();
                parameters.Add("code", window_type);

                DataTable tblMenu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM  #SCHEMA_NAME#.wb_menu WHERE code=@code", CommandType.Text, parameters);

                if (tblMenu.Rows.Count == 0)
                {
                    json.Add("error", "Không tìm thấy chức năng này");
                    return json;
                }

                string wb_menu_id = tblMenu.Rows[0]["wb_menu_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_role_id", Guid.Parse(wb_role_id));
                parameters.Add("wb_menu_id", Guid.Parse(wb_menu_id));

                DataTable tblDetailRole = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM  #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id=@wb_menu_id", CommandType.Text, parameters);

                if (tblDetailRole.Rows.Count == 0)
                {
                    json.Add("error", "Bạn không được phân quyền chức năng này");
                    return json;
                }

                string xoa = tblDetailRole.Rows[0]["has_delete"].ToString();

                if (xoa != "C")
                {
                    json.Add("error", "Bạn không có quyền xóa!");
                    return json;
                }

                string id = model.id.ToString();

                parameters.Clear();
                parameters.Add("wb_user_id", Guid.Parse(id));

                string sqlUser = "SELECT * FROM #SCHEMA_NAME#.wb_user WHERE wb_user_id=@wb_user_id ";

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync(sqlUser, CommandType.Text, parameters);

                string userDelete = tblUser.Rows[0]["username"].ToString();

                if (_webHelper.GetUser() == userDelete)
                {
                    throw new Exception("Bạn không có quyền xóa chính tài khoản của mình!");
                }

                parameters.Clear();
                parameters.Add("wb_user_id", Guid.Parse(id));

                string sql = "DELETE FROM #SCHEMA_NAME#.wb_user WHERE wb_user_id=@wb_user_id ";
                await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                json.Add("ok", true);

                CacheManager cacheManager = new CacheManager(_minvoiceDbContext);
                cacheManager.removeUserLogin(model.username);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<JObject> PhanQuyen(JObject model)
        {
            JObject result = new JObject();

            try
            {

                string _user = _webHelper.GetUser();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("username", _user);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);

                DataTable tblMenu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_menu WHERE code='account'");

                if (tblMenu.Rows.Count == 0)
                {
                    result.Add("error", "Không tìm thấy chức năng tài khoản");
                    return result;
                }

                DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=@username)", CommandType.Text, parameters);

                if (tblNhomQuyen.Rows.Count == 0)
                {
                    result.Add("error", "Không tìm thấy nhóm quyền của người sử dụng");
                    return result;
                }

                string wb_menu_id = tblMenu.Rows[0]["wb_menu_id"].ToString();
                string wb_role_id = tblNhomQuyen.Rows[0]["wb_role_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_menu_id", Guid.Parse(wb_menu_id));
                parameters.Add("wb_role_id", Guid.Parse(wb_role_id));

                DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id=@wb_menu_id", CommandType.Text, parameters);

                if (tblCtQuyen.Rows.Count == 0)
                {
                    result.Add("error", "Bạn không được phân quyền chức năng này");
                    return result;
                }

                /*wb_ctquyen ctquyen = invoiceDb.WbCtquyens.Where(c => c.wb_nhomquyen_id == userLogin.wb_nhomquyen_id
                            && c.wb_menu_id == menu.id).FirstOrDefault<wb_ctquyen>();

                if (ctquyen == null)
                {
                    result.Add("error", "Bạn không được phân quyền chức năng này");
                    return result;
                }

                string sua = ctquyen.sua == null ? "" : ctquyen.sua;

                if (sua != "C")
                {
                    result.Add("error", "Bạn không có quyền sửa");
                    return result;
                }*/

                string wb_user_id = model["wb_user_id"].ToString();
                string branch_code = tblUser.Rows[0]["branch_code"].ToString();

                Dictionary<string, object> paraDelUser = new Dictionary<string, object>();
                paraDelUser.Add("wb_user_id", Guid.Parse(wb_user_id));

                string sql = "DELETE FROM #SCHEMA_NAME#.pl_certificate_permission WHERE wb_user_id=@wb_user_id";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDelUser);

                JArray jCtquyen = (JArray)model["ckspermission"];

                foreach (JObject obj in jCtquyen)
                {
                    if (obj["chon1"].ToString() == "C")
                    {
                        Dictionary<string, object> paraDetail = new Dictionary<string, object>();

                        paraDetail.Add("pl_certificate_permission_id", Guid.NewGuid());
                        paraDetail.Add("pl_certificate_id", Guid.Parse(obj["pl_certificate_id"].ToString()));
                        paraDetail.Add("wb_user_id", Guid.Parse(wb_user_id));
                        paraDetail.Add("branch_code", branch_code);

                        sql = "INSERT INTO #SCHEMA_NAME#.pl_certificate_permission (pl_certificate_permission_id,pl_certificate_id,wb_user_id,branch_code) \n"
                            + "VALUES (@pl_certificate_permission_id,@pl_certificate_id,@wb_user_id,@branch_code)";

                        await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDetail);
                    }
                }

                sql = "DELETE FROM #SCHEMA_NAME#.inv_notification_permission WHERE wb_user_id=@wb_user_id";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDelUser);

                jCtquyen = (JArray)model["invtemppermission"];

                foreach (JObject obj in jCtquyen)
                {
                    if (obj["chon1"].ToString() == "C")
                    {
                        Dictionary<string, object> paraDetail = new Dictionary<string, object>();

                        paraDetail.Add("inv_notification_permission_id", Guid.NewGuid());
                        paraDetail.Add("inv_notificationdetail_id", Guid.Parse(obj["ctthongbao_id"].ToString()));
                        paraDetail.Add("wb_user_id", Guid.Parse(wb_user_id));

                        sql = "INSERT INTO #SCHEMA_NAME#.inv_notification_permission (inv_notification_permission_id,inv_notificationdetail_id,wb_user_id) \n"
                            + "VALUES (@inv_notification_permission_id,@inv_notificationdetail_id,@wb_user_id)";

                        await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDetail);
                    }
                }

                if (model["dvpermission"] != null)
                {
                    sql = "DELETE FROM #SCHEMA_NAME#.wb_branch_permission WHERE wb_user_id=@wb_user_id";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDelUser);

                    jCtquyen = (JArray)model["dvpermission"];

                    foreach (JObject obj in jCtquyen)
                    {
                        if (obj["chon1"].ToString() == "C")
                        {
                            Dictionary<string, object> paraDetail = new Dictionary<string, object>();

                            paraDetail.Add("wb_branch_permission_id", Guid.NewGuid());
                            paraDetail.Add("wb_branch_id", Guid.Parse(obj["ctthongbao_id"].ToString()));
                            paraDetail.Add("wb_user_id", Guid.Parse(wb_user_id));

                            sql = "INSERT INTO #SCHEMA_NAME#.wb_branch_permission (wb_branch_permission_id,wb_branch_id,wb_user_id) \n"
                                + "VALUES (@wb_branch_permission_id,@wb_branch_id,@wb_user_id)";

                            await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDetail);
                        }
                    }
                }

                if (model["dtMauHoaDon68"] != null)
                {
                    sql = "DELETE FROM #SCHEMA_NAME#.permission_kyhieu68 WHERE wb_user_id=@wb_user_id";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDelUser);

                    jCtquyen = (JArray)model["dtMauHoaDon68"];

                    foreach (JObject obj in jCtquyen)
                    {
                        if (obj["chon1"].ToString() == "C")
                        {
                            Dictionary<string, object> paraDetail = new Dictionary<string, object>();

                            paraDetail.Add("permission_id", Guid.NewGuid());
                            paraDetail.Add("qlkhsdung_id", Guid.Parse(obj["qlkhsdung_id"].ToString()));
                            paraDetail.Add("wb_user_id", Guid.Parse(wb_user_id));

                            sql = "INSERT INTO #SCHEMA_NAME#.permission_kyhieu68 (permission_id,qlkhsdung_id,wb_user_id) \n"
                                + "VALUES (@permission_id,@qlkhsdung_id,@wb_user_id)";

                            await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraDetail);
                        }
                    }
                }

                JObject inv = (JObject)model["invoicepermission"];

                sql = "UPDATE #SCHEMA_NAME#.wb_user SET is_sign_invoice=@is_sign_invoice,is_sign_invoice_hsm=@is_sign_invoice_hsm,is_view_user=@is_view_user,is_edit_user=@is_edit_user,is_del_user=@is_del_user,is_new_user=@is_new_user WHERE wb_user_id=@wb_user_id";

                Dictionary<string, object> paraUser = new Dictionary<string, object>();
                paraUser.Add("is_sign_invoice", inv["issigninvoice"].ToString());
                paraUser.Add("is_sign_invoice_hsm", inv["issigninvoicehsm"].ToString());
                paraUser.Add("is_view_user", inv["isviewuser"].ToString());
                paraUser.Add("is_edit_user", inv["isedituser"].ToString());
                paraUser.Add("is_del_user", inv["isdeluser"].ToString());
                paraUser.Add("is_new_user", inv["isnewuser"].ToString());
                paraUser.Add("wb_user_id", Guid.Parse(wb_user_id));

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paraUser);
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);

            }

            return result;
        }

        public async Task<JObject> StoreTemp(JObject model)
        {
            JObject result = new JObject();

            try
            {
                string sl_store_id = model["sl_store_id"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("sl_store_id", Guid.Parse(sl_store_id));

                string sql = "DELETE FROM #SCHEMA_NAME#.sl_store_template WHERE sl_store_id=@sl_store_id";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                JArray jCtquyen = (JArray)model["data"];

                foreach (JObject obj in jCtquyen)
                {
                    if (obj["chon"].ToString() == "C")
                    {
                        parameters.Clear();
                        parameters.Add("sl_store_template_id", Guid.NewGuid());
                        parameters.Add("sl_store_id", Guid.Parse(sl_store_id));
                        parameters.Add("inv_notificationdetail_id", Guid.Parse(obj["inv_notificationdetail_id"].ToString()));

                        sql = "INSERT INTO #SCHEMA_NAME#.sl_store_template (sl_store_template_id,sl_store_id,inv_notificationdetail_id) \n"
                            + "VALUES (@sl_store_template_id,@sl_store_id,@inv_notificationdetail_id)";

                        await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);

            }

            return result;
        }

        public async Task<JObject> SendEmailResetPass(string email)
        {
            JObject json = new JObject();

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("email", email);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE email=@email LIMIT 1", CommandType.Text, parameters);

                if (tblUser.Rows.Count == 0)
                {
                    json.Add("error", "Email chưa được đăng ký");
                    return json;
                }

                string pass = CommonManager.RandomPassword(8);
                Encrypt crypt = new Encrypt(pass);

                string username = tblUser.Rows[0]["username"].ToString();
                string password = crypt.CreateHashedPassword(pass, null);

                string sql = "UPDATE #SCHEMA_NAME#.wb_user SET password=@password, is_lock=false WHERE username=@username";

                parameters = new Dictionary<string, object>();
                parameters.Add("password", password);
                parameters.Add("username", username);

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='RESET_PASSWORD' AND yesno='C' LIMIT 1");

                if (tblEmail.Rows.Count > 0)
                {
                    string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                    if (smtp_address.Length > 0)
                    {
                        int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                        bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                        this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                    }

                    string subject = tblEmail.Rows[0]["subject"].ToString();
                    string body = tblEmail.Rows[0]["body"].ToString()
                                    .Replace("#username#", username)
                                    .Replace("#password#", pass)
                                    .Replace("#url_host#", _webHelper.GetUrlHost());

                    this._emailService.Send(email, subject, body);
                }
                else
                {
                    json.Add("error", "Chưa có mẫu email reset mật khẩu");
                    return json;
                }

                CacheManager cacheManager = new CacheManager(_minvoiceDbContext);
                cacheManager.removeUserLogin(username);

                json.Add("ok", true);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<JArray> GetUserHsmById(string wb_user_id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Clear();
            parameters.Add("wb_user_id", Guid.Parse(wb_user_id));

            string sql = "SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id";

            DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

            foreach (DataRow row in tblUser.Rows)
            {
                var password_Encode = row["password"].ToString();
                var password2_Encode = row["password_level2"].ToString();
                var password_Decode = Encoding.UTF8.GetString(Convert.FromBase64String(password_Encode));
                var password2_Decode = Encoding.UTF8.GetString(Convert.FromBase64String(password2_Encode));
                row["password"] = password_Decode;
                row["password_level2"] = password2_Decode;
            }

            return JArray.FromObject(tblUser);
        }

        public async Task<JObject> Update_HSM(wb_user_hsm_portal model)
        {
            JObject json = new JObject();
            string pass = "";
            string pass_level2 = "";
            //string sql = "";

            try
            {
                string _user = _webHelper.GetUser();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                string sql = "";

                parameters.Clear();
                parameters.Add("username", _user);

                DataTable tblUserLogin = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, parameters);
                string wb_role_id = tblUserLogin.Rows[0]["wb_role_id"].ToString();

                string window_type = model.window_type;

                parameters.Clear();
                parameters.Add("code", window_type);

                DataTable tblMenu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM  #SCHEMA_NAME#.wb_menu WHERE code=@code", CommandType.Text, parameters);

                if (tblMenu.Rows.Count == 0)
                {
                    json.Add("error", "Không tìm thấy chức năng này");
                    return json;
                }

                string wb_menu_id = tblMenu.Rows[0]["wb_menu_id"].ToString();

                parameters.Clear();
                parameters.Add("wb_role_id", Guid.Parse(wb_role_id));
                parameters.Add("wb_menu_id", Guid.Parse(wb_menu_id));

                DataTable tblDetailRole = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM  #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id=@wb_role_id AND wb_menu_id=@wb_menu_id", CommandType.Text, parameters);

                if (tblDetailRole.Rows.Count == 0)
                {
                    json.Add("error", "Bạn không được phân quyền chức năng này");
                    return json;
                }


                if (model.wb_user_hsm_portal_id.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    Guid id = Guid.NewGuid();
                    string _them = tblDetailRole.Rows[0]["has_new"].ToString();
                    //string _them = "C";
                    if (_them != "C")
                    {
                        json.Add("error", "Bạn không có quyền thêm mới");
                        return json;
                    }

                    pass = model.password;
                    pass_level2 = model.password_level2;

                    string pwd_crypt = Convert.ToBase64String(Encoding.UTF8.GetBytes(pass));
                    string pwd2_crypt = Convert.ToBase64String(Encoding.UTF8.GetBytes(pass_level2));

                    parameters = new Dictionary<string, object>();

                    parameters.Add("wb_user_hsm_portal_id", id);
                    parameters.Add("username", model.username);
                    parameters.Add("ma_dvcs", model.ma_dvcs);
                    parameters.Add("password", pwd_crypt);
                    parameters.Add("password_level2", pwd2_crypt);
                    parameters.Add("wb_user_id", model.wb_user_id);

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_user_hsm_portal_78(wb_user_hsm_portal_id,username,ma_dvcs,password,password_level2,wb_user_id) \n"
                                + "VALUES (@wb_user_hsm_portal_id,@username,@ma_dvcs,@password,@password_level2,@wb_user_id)";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                }
                else
                {
                    string _sua = tblDetailRole.Rows[0]["has_edit"].ToString();
                    //string _sua = "C";
                    if (_sua != "C")
                    {
                        json.Add("error", "Bạn không có quyền sửa");
                        return json;
                    }

                    parameters.Clear();
                    parameters.Add("wb_user_hsm_portal_id", model.wb_user_hsm_portal_id);

                    sql = "SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_hsm_portal_id=@wb_user_hsm_portal_id";
                    DataTable tblWbUser = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                    DataRow row = tblWbUser.Rows[0];

                    row.BeginEdit();

                    row["username"] = model.username;
                    row["ma_dvcs"] = model.ma_dvcs;
                    row["password"] = Convert.ToBase64String(Encoding.UTF8.GetBytes(model.password));
                    row["password_level2"] = Convert.ToBase64String(Encoding.UTF8.GetBytes(model.password_level2));
                    row["wb_user_hsm_portal_id"] = model.wb_user_hsm_portal_id;
                    row.EndEdit();

                    sql = "UPDATE #SCHEMA_NAME#.wb_user_hsm_portal_78 SET username=@username,ma_dvcs=@ma_dvcs,password=@password,password_level2=@password_level2 WHERE wb_user_hsm_portal_id=@wb_user_hsm_portal_id";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, row);

                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<JObject> DeleteUserHsmById(string wb_user_id)
        {
            JObject json = new JObject();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Clear();
                parameters.Add("wb_user_id", Guid.Parse(wb_user_id));

                string sql = "DELETE FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id";

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<string> getUserInfo()
        {
            string _user = _webHelper.GetUser();
            var dvcs = _webHelper.GetDvcs();
            string sql = $"SELECT q.is_sign_invoice_hsm FROM #SCHEMA_NAME#.wb_user q where q.username ='{_user}' and q.branch_code = '{dvcs}'";
            DataTable json = await _minvoiceDbContext.GetDataTableAsync(sql);
            return json.Rows[0]["is_sign_invoice_hsm"].ToString();
        }

        #endregion
    }
}