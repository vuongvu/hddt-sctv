﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceApi.Data.Domain;

namespace InvoiceApi.Services
{
    public partial interface IAccountService
    {
        Task<JArray> GetUsers();
        Task<JObject> SaveChange(wb_user model);
        Task<JObject> Delete(wb_user model);

        wb_user GetAccountByUserName(string loginName);
        string GenerateToken(string username, string password, string ip, string userAgent, long ticks, string ma_svcs);
        Task<JObject> Login(string username, string password, string ipAddress, string userAgent,string ma_dvcs);
        Task<bool> CheckPassword(string username, string password);
        Task<JObject> ChangePass(string username, string password);
        Task<JObject> PhanQuyen(JObject model);
        Task<JObject> StoreTemp(JObject model);
        Task<JObject> SendEmailResetPass(string email);

        Task<JArray> GetUsersByBranch();
        Task<JArray> GetUsersByStore();
        Task<JArray> GetUserHsmById(string wb_user_id);
        Task<JObject> Update_HSM(wb_user_hsm_portal model);
        Task<string> getUserInfo();
        Task<JObject> DeleteUserHsmById(string wb_user_id);

    }
}
