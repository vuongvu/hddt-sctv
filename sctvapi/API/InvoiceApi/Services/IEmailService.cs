﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface IEmailService
    {
        void Send(string to, string subject, string body);
        Task SendAsync(string to, string subject, string body);
        void Send(string from, string pass, string to, string subject, string body);
        void Send(string from, string pass, string to, string subject, string body, string bcc);
        void Attach(string fileName, Stream stream, string contentType);
        void SetEmailServer(string smtpAddress, int smtpPort, bool enableSSL);
        void ClearAttach();

        string GetFrom();

        Task SendAsync(string from, string pass, string to, string subject, string body, string bcc, string alias);
    }
}
