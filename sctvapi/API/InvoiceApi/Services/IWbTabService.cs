﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceApi.Data.Domain;
using InvoiceApi.Util;

namespace InvoiceApi.Services
{
    public partial interface IWbTabService
    {
        Task<DataTableExtend> GetAll();
        Task<DataTableExtend> GetTabsByWindowId(string id);
        Task<JObject> GetTabs(int start, int count, string filter);
        Task<JObject> SaveChange(JObject model);
        Task<JObject> Delete(string id);
    }
}
