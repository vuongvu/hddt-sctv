﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface IInvoiceService
    {
        Task<byte[]> PrintInvoiceFromId(string id, string folder, string type, string no_preview, bool inThuTu = false);
        Task<byte[]> PrintInvoiceFromId(string id, string folder, string type, bool inchuyendoi, string no_preview, bool inThuTu = false);
        Task<byte[]> PrintListInvoice(JObject model, string folder, bool inchuyendoi, string type);
        Task<byte[]> PrintAdjustmentReport(JObject model);
        Task<byte[]> InChuyenDoi(string id);
        Task<byte[]> ExportZipFile(string id, string pathReport, string fileName);
        Task<string> GetInvoiceFileName(string id);
        Task<JObject> XoaBoHD(JObject model);
        Task<JObject> GetInvoiceById(Guid id);
        JObject Search(string ma_dvcs, JObject model);
        Task<JObject> GetInvoiceByInvoiceNumber(string invoiceCode, string invoiceSeries, string invoiceNumber);

        Task<JObject> GetInvoiceXoaBo(JObject parameters);
        Task<JObject> GetInvoiceXoaBoPretreatment(JObject parameters);
        Task<JObject> SaveInvoiceXoaBo(JObject masterObj);
        Task<JObject> SaveInvoiceXoaBoPretreatment(JObject masterObj);
        Task<JObject> DuyetHoaDon(JObject model);
        Task<JObject> TuChoiHoaDon(JObject model);
        Task<JObject> ExportInvoiceXml(Guid id);
        Task<JObject> ExportInvoiceXmlPretreatment(Guid id);
        Task<JObject> SaveXml(JObject parameters, string ma_dvcs);
        Task<JObject> SaveXmlPretreatment(JObject parameters, string ma_dvcs);
        Task<JObject> SignConfirm(JObject parameters);
        Task<JObject> CancelInvoice(string id);
        Task<JObject> RegisterInvoiceProcess(JObject model, string username);
        Task<JArray> GetListCertificates(string userName, string ma_dvcs);
        Task<JObject> LaySoHoaDon(JObject model, string username);
        Task<JArray> GetHoaDonChoDuyet(string ma_dvcs, string trang_thai);
        JArray DashboardTiles(string ma_dvcs);
        JArray DashboardInvoices(string ma_dvcs);

        Task<DataTable> GetTemplate(Guid id);
        Task<DataTable> GetXmlDataTemplate();
        Task<DataTable> GetInvoiceTemplate(Guid id);

        Task<JObject> SaveInvoiceTemlate(string dmmauhoadon_id, string report);
        Task<JObject> CopyInvoiceTemlate(JObject model);
        Task<JObject> GetXmlTemplate(Guid id);
        Task<JObject> GetXmlData(Guid id);
        Task<JObject> SignXmlTemplate(string id, string xml);

        Task<byte[]> PrintInvoiceTemplate(string id, string type, bool inchuyendoi =false);

        string GetTB01ACXml(string id);
        JObject SaveTB01ACXml(JObject model);

        Task<JObject> SendInvoiceByEmail(string folder, JObject model, string ma_dvcs, string type);
        Task<JObject> GetEmailTemplate(string type, string id, string ma_dvcs, string originalString);

        Task<JArray> GetListForSigning(string ma_dvcs);

        Task<string> UploadInvTemplate(string ma_dvcs, string user_login, string excel_id, byte[] bytes);
        Task<JArray> GetDecimalPlace(string ma_nt);
        Task<JObject> Amount_ToWord(string number, string type_amount);
        Task<JObject> CreateUser_tracuu(JObject model);
        Task<JObject> GetUserTracuu(JObject model);
        Task<JObject> ResetPassUserTracuu(JObject model);
        Task<JObject> DeleteUserTracuu(JObject model);
        string GetSchema();
        byte[] DowloadRepx(string id);
        Task<JArray> GetListCertificatesFile(string userName, string ma_dvcs);

        Task<JObject> SignInvoiceCertFile(JObject model);
        Task<JObject> SignXmlEasyCA(JObject model);
        Task<bool> CheckSignByHSM();
        Task<JObject> Create(JObject model);
        Task<JObject> DeleteByCertFile(JObject model);
        Task<JArray> InvoiceHistorySendEmail(string id);
        Task<JObject> AutoSendInvoiceByEmail(string folder, JObject model);
        Task<JObject> SendListInvoices(string folder, JObject model);
        Task<JObject> SignListInvoiceCertFile(JObject model);
        Task<string> GetXmlSignInvoices(string inv_InvoiceCode_id, DateTime tu_ngay, DateTime den_ngay, string inv_invoiceAuth_id);
        Task<JObject> SaveXmlSign(JObject parameters);
        Task<JArray> GetListInvoiceSeries(JObject model);

        Task<JObject> ConfirmTempInvoice(JObject model);
        Task<byte[]> PreviewTempInvoice(long id, string folder, string type, bool inchuyendoi);

        Task<JObject> UpdateCustomerInfo(JObject model);
        Task<JObject> GetByInvoiceId(long invoice_id);
        Task<JObject> SendEmailByInvoiceId(JObject model);
        Task<JObject> InfoEmailToCustomer(long invoice_id);

        Task<byte[]> PrintReceiptFromInvoiceId(long id, string folder, string type, bool inchuyendoi);
        Task<byte[]> PrintListReceipt(JObject model, string folder, bool inchuyendoi, string type);

        Task<JObject> SendToCustomer(JObject model);
        Task<JObject> DeleteReceipt(JObject model);

        Task<JObject> DeleteReceiptMulti(JObject model);

        Task<byte[]> PrintReceiptFromId(string id, string folder, string type, bool inchuyendoi,bool inThuTu = false);
        Task<JObject> ChangeSecurityNumber(JObject model);
        Task<JObject> DeleteInv(JObject model);

        Task<List<JObject>> DeleteInvMulti(List<JObject> models);

        Task<string> GenerateSecurity(int size, int type);
    }
}
