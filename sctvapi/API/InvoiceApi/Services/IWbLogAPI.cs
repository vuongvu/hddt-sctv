﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface IWbLogAPI
    {
        Task<string> RegisterDataInputApi(string apiName, string model);
        Task InsertWbLogAPIAsync(string response, string status, string wb_log_api_id);
    }
}
