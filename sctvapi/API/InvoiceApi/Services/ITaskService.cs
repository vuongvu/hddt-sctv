﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface ITaskService
    {
        Task<JObject> ImportBatch(JObject model);
        Task<JObject> CreateInvoiceTemp(JObject model);
        Task<JObject> CreateInvoiceAuth(JObject model);
        Task<JObject> SignInvoice(JObject model);
        Task<JObject> SendEmailToCustomer(JObject model);
        Task<JObject> SendSmsToCustomer(JObject model);
        Task<JObject> Info(JObject model);

        Task<JObject> CreateReceipt(JObject model);
        Task<JObject> SignReceipt(JObject model);
        Task<JObject> SendEmailReceipt(JObject model);

        JObject CreateTask(JObject model);
    }
}
