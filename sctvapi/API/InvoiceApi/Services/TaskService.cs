﻿using InvoiceApi.Data;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InvoiceApi.Services
{
    public partial class TaskService : ITaskService
    {
        #region Fields

        private readonly IMinvoiceDbContext _db;
        private readonly IRabbitMQService _rabbitmqService;

        #endregion

        #region Ctor

        public TaskService(IMinvoiceDbContext db, IRabbitMQService rabbitmqService)
        {
            this._db = db;
            this._rabbitmqService = rabbitmqService;
        }

        #endregion

        #region Methods

        public async Task<JObject> ImportBatch(JObject model)
        {
            JObject res = new JObject();
            string taskName = "create_batch_invoice_queue";

            try
            {
                string schemaName = await _db.GetSchemaNameAsync();
                model.Add("SCHEMA_NAME", schemaName);

                _rabbitmqService.PublishQueue(taskName, model.ToString());

                res.Add("ok", true);
                res.Add("task_create", true);
                res.Add("task_name", taskName);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> CreateInvoiceTemp(JObject model)
        {
            JObject res = new JObject();

            try
            {
                long batch_num = Convert.ToInt64(model["batch_num"]);

                Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                dicParameter.Add("batch_num", batch_num);

                string sql = "SELECT * FROM #SCHEMA_NAME#.inv_batch WHERE batch_num=@batch_num";
                DataTable tblBatch = await _db.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                if (tblBatch.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy lô " + batch_num);
                    return res;
                }

                string batch_status = tblBatch.Rows[0]["batch_status"].ToString();

                if (batch_status == "OK")
                {
                    res.Add("error", "Lô " + batch_num + " đã tạo xong hóa đơn chờ duyệt");
                    return res;
                }

                bool is_running = Convert.ToBoolean(tblBatch.Rows[0]["is_running"]);

                if (is_running)
                {
                    res.Add("error", "Lô " + batch_num + " đang chạy");
                    return res;
                }

                string schemaName = await _db.GetSchemaNameAsync();

                JObject rabbitObj = new JObject();
                rabbitObj.Add("SCHEMA_NAME", schemaName);
                rabbitObj.Add("batch_num", batch_num);

                string taskName = "create_invoice_temp_queue";
                _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());

                res.Add("ok", true);
                res.Add("task", "create");
                res.Add("task_name", taskName);

            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> CreateInvoiceAuth(JObject model)
        {
            JObject res = new JObject();

            string batch_num = model["batch_num"].ToString();

            string schemaName = await _db.GetSchemaNameAsync();

            Dictionary<string, object> dicParameter = new Dictionary<string, object>();
            dicParameter.Add("batch_num", Convert.ToInt64(batch_num));

            DataTable tblData = await _db.GetDataTableAsync("SELECT DISTINCT branch_code FROM #SCHEMA_NAME#.inv_invoice_temp WHERE batch_num=@batch_num  AND is_export=false", CommandType.Text, dicParameter);

            if (tblData.Rows.Count == 0)
            {
                res.Add("error", "Đã hết hóa đơn cần tạo");
                return res;
            }

            string taskName = "crd_invoice_queue";

            foreach (DataRow row in tblData.Rows)
            {
                JObject rabbitObj = new JObject();
                rabbitObj.Add("SCHEMA_NAME", schemaName);
                rabbitObj.Add("batch_num", batch_num);
                rabbitObj.Add("branch_code", row["branch_code"].ToString());

                _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());
            }

            res.Add("ok", true);
            res.Add("task", "create");
            res.Add("task_name", taskName);

            return res;
        }

        public async Task<JObject> CreateReceipt(JObject model)
        {
            JObject res = new JObject();

            string batch_num = model["batch_num"].ToString();

            string schemaName = await _db.GetSchemaNameAsync();

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("batch_num", Convert.ToInt64(batch_num));

            DataTable tblData = await _db.GetDataTableAsync("SELECT DISTINCT branch_code FROM #SCHEMA_NAME#.inv_batchdetail WHERE batch_num=@batch_num AND invoice_number IS NULL", CommandType.Text, parameter);

            if (tblData.Rows.Count == 0)
            {
                res.Add("error", "Đã hết phiếu thu cần tạo");
                return res;
            }

            string taskName = "crd_receipt_queue";

            foreach (DataRow row in tblData.Rows)
            {
                JObject rabbitObj = new JObject();
                rabbitObj.Add("SCHEMA_NAME", schemaName);
                rabbitObj.Add("batch_num", batch_num);
                rabbitObj.Add("branch_code", row["branch_code"].ToString());

                _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());
            }

            res.Add("ok", true);
            res.Add("task", "create");
            res.Add("task_name", taskName);

            return res;
        }

        public async Task<JObject> SignInvoice(JObject model)
        {
            JObject res = new JObject();

            string batch_num = model["batch_num"].ToString();

            string schemaName = await _db.GetSchemaNameAsync();

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("batch_num", Convert.ToInt64(batch_num));

            DataTable tblData = await _db.GetDataTableAsync("SELECT DISTINCT branch_code FROM #SCHEMA_NAME#.inv_invoiceauth WHERE batch_num=@batch_num AND status='Chờ ký'", CommandType.Text, parameter);

            if (tblData.Rows.Count == 0)
            {
                res.Add("error", "Đã hết hóa đơn cần ký");
                return res;
            }

            string taskName = "sign_invoice_queue";

            foreach (DataRow row in tblData.Rows)
            {
                JObject rabbitObj = new JObject();
                rabbitObj.Add("SCHEMA_NAME", schemaName);
                rabbitObj.Add("batch_num", batch_num);
                rabbitObj.Add("branch_code", row["branch_code"].ToString());

                _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());
            }

            res.Add("ok", true);
            res.Add("task", "create");
            res.Add("task_name", taskName);

            return res;
        }

        public async Task<JObject> SignReceipt(JObject model)
        {
            JObject res = new JObject();

            string batch_num = model["batch_num"].ToString();

            string schemaName = await _db.GetSchemaNameAsync();

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("batch_num", Convert.ToInt64(batch_num));

            DataTable tblData = await _db.GetDataTableAsync("SELECT DISTINCT branch_code FROM #SCHEMA_NAME#.inv_receipt WHERE batch_num=@batch_num AND status='Chờ ký'", CommandType.Text, parameter);

            if (tblData.Rows.Count == 0)
            {
                res.Add("error", "Đã hết phiếu thu cần ký");
                return res;
            }

            string taskName = "sign_receipt_queue";

            foreach (DataRow row in tblData.Rows)
            {
                JObject rabbitObj = new JObject();
                rabbitObj.Add("SCHEMA_NAME", schemaName);
                rabbitObj.Add("batch_num", batch_num);
                rabbitObj.Add("branch_code", row["branch_code"].ToString());

                _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());
            }

            res.Add("ok", true);
            res.Add("task", "create");
            res.Add("task_name", taskName);

            return res;
        }

        public async Task<JObject> SendEmailReceipt(JObject model)
        {
            JObject res = new JObject();

            string batch_num = model["batch_num"].ToString();

            string schemaName = await _db.GetSchemaNameAsync();

            string taskName = "receipt_emailsms_queue";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("batch_num", Convert.ToInt64(batch_num));

            DataTable tblData = await _db.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_branch WHERE batch_num=@batch_num", CommandType.Text, parameter);

            foreach (DataRow row in tblData.Rows)
            {
                JObject rabbitObj = new JObject();
                rabbitObj.Add("SCHEMA_NAME", schemaName);
                rabbitObj.Add("batch_num", batch_num);
                rabbitObj.Add("branch_code", row["branch_code"].ToString());

                _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());
            }

            res.Add("ok", true);
            res.Add("task", "create");
            res.Add("task_name", taskName);

            return res;
        }

        public async Task<JObject> SendEmailToCustomer(JObject model)
        {
            JObject res = new JObject();
            string taskName = "email_inv_customer_queue";

            try
            {
                string batch_num = model["batch_num"].ToString();

                string schemaName = await _db.GetSchemaNameAsync();
                model.Add("SCHEMA_NAME", schemaName);

                int pageSize = 500;

                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("batch_num", Convert.ToInt64(batch_num));

                string sql = "SELECT COUNT(*) FROM #SCHEMA_NAME#.inv_invoiceauth WHERE batch_num=@batch_num";
                DataTable tblData = await _db.GetDataTableAsync(sql, CommandType.Text, parameter);

                int totalRows = Convert.ToInt32(tblData.Rows[0][0]);
                int totalPage = (int)Math.Ceiling((double)totalRows / pageSize);

                for (int i = 1; i <= totalPage; i++)
                {
                    int offset = pageSize * (i - 1);

                    parameter.Clear();
                    parameter.Add("batch_num", Convert.ToInt64(batch_num));
                    parameter.Add("pageSize", pageSize);
                    parameter.Add("offset", offset);

                    sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE batch_num=@batch_num  LIMIT @pageSize OFFSET @offset";
                    tblData = await _db.GetDataTableAsync(sql, CommandType.Text, parameter);

                    JObject rabbitObj = new JObject();
                    rabbitObj.Add("SCHEMA_NAME", schemaName);
                    rabbitObj.Add("invs", JArray.FromObject(tblData));
                    rabbitObj.Add("batch_num", batch_num);

                    _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());

                }

                res.Add("ok", true);
                res.Add("task_create", true);
                res.Add("task_name", taskName);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> SendSmsToCustomer(JObject model)
        {
            JObject res = new JObject();
            string taskName = "sms_inv_customer_queue";

            try
            {
                string batch_num = model["batch_num"].ToString();

                string schemaName = await _db.GetSchemaNameAsync();
                model.Add("SCHEMA_NAME", schemaName);

                int pageSize = 500;

                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("batch_num", Convert.ToInt64(batch_num));

                string sql = "SELECT COUNT(*) FROM #SCHEMA_NAME#.inv_invoiceauth WHERE batch_num=@batch_num";
                DataTable tblData = await _db.GetDataTableAsync(sql, CommandType.Text, parameter);

                int totalRows = Convert.ToInt32(tblData.Rows[0][0]);
                int totalPage = (int)Math.Ceiling((double)totalRows / pageSize);

                for (int i = 1; i <= totalPage; i++)
                {
                    int offset = pageSize * (i - 1);

                    parameter.Clear();
                    parameter.Add("batch_num", Convert.ToInt64(batch_num));
                    parameter.Add("pageSize", pageSize);
                    parameter.Add("offset", offset);

                    sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE batch_num=@batch_num LIMIT @pageSize OFFSET @offset ";
                    tblData = await _db.GetDataTableAsync(sql, CommandType.Text, parameter);

                    JObject rabbitObj = new JObject();
                    rabbitObj.Add("SCHEMA_NAME", schemaName);
                    rabbitObj.Add("invs", JArray.FromObject(tblData));
                    rabbitObj.Add("batch_num", batch_num);

                    _rabbitmqService.PublishQueue(taskName, rabbitObj.ToString());

                }

                res.Add("ok", true);
                res.Add("task_create", true);
                res.Add("task_name", taskName);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> Info(JObject model)
        {
            JObject res = new JObject();

            long batch_num = Convert.ToInt64(model["batch_num"].ToString());
            int task_type = Convert.ToInt32(model["task_type"].ToString());

            //if (task_type < 0 || task_type > 2)
            //{
            //    res.Add("error", "Loại task không đúng chỉ được là 0,1,2 ");
            //    return res;
            //}
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("batch_num", batch_num);

            string sql = "SELECT * FROM #SCHEMA_NAME#.inv_batch WHERE batch_num=@batch_num";
            DataTable tblBatch = await _db.GetDataTableAsync(sql, CommandType.Text, parameter);

            if (tblBatch.Rows.Count == 0)
            {
                res.Add("error", "Không tìm thấy Batch " + batch_num);
                return res;
            }

            if (task_type == 0)
            {
                res.Add("start_time", tblBatch.Rows[0]["start_time"].ToString());
                res.Add("finish_time", tblBatch.Rows[0]["finish_time"].ToString());
                res.Add("batch_status", tblBatch.Rows[0]["batch_status"].ToString());
                res.Add("batch_info", tblBatch.Rows[0]["batch_info"].ToString());

                parameter.Clear();
                parameter.Add("batch_num", batch_num);

                DataTable tblTempInvoice = await _db.GetDataTableAsync("SELECT COUNT(*) as SL FROM #SCHEMA_NAME#.inv_invoice_temp WHERE batch_num=@batch_num", CommandType.Text, parameter);

                if (tblTempInvoice.Rows.Count == 0)
                {
                    res.Add("number_processed", 0);
                }
                else
                {
                    res.Add("number_processed", Convert.ToInt32(tblTempInvoice.Rows[0]["SL"].ToString()));
                }

            }
            else if (task_type == 1)
            {

                res.Add("start_time", tblBatch.Rows[0]["timestart_invoice"].ToString());
                res.Add("finish_time", tblBatch.Rows[0]["timefinish_invoice"].ToString());
                res.Add("batch_status", tblBatch.Rows[0]["status_invoice"].ToString());
                res.Add("batch_info", tblBatch.Rows[0]["created_info"].ToString());

                parameter.Clear();
                parameter.Add("batch_num", batch_num);

                DataTable tblTempInvoice = await _db.GetDataTableAsync("SELECT COUNT(*) as SL FROM #SCHEMA_NAME#.inv_invoiceauth WHERE batch_num=@batch_num", CommandType.Text, parameter);

                if (tblTempInvoice.Rows.Count == 0)
                {
                    res.Add("number_processed", 0);
                }
                else
                {
                    res.Add("number_processed", Convert.ToInt32(tblTempInvoice.Rows[0]["SL"].ToString()));
                }
            }
            else if (task_type == 2)
            {
                res.Add("start_time", tblBatch.Rows[0]["timestart_sign"].ToString());
                res.Add("finish_time", tblBatch.Rows[0]["timefinish_sign"].ToString());
                res.Add("batch_info", tblBatch.Rows[0]["sign_info"].ToString());
                res.Add("batch_status", tblBatch.Rows[0]["status_sign"].ToString());
                res.Add("is_end_sign", tblBatch.Rows[0]["is_end_sign"].ToString());

                parameter.Clear();
                parameter.Add("batch_num", batch_num);

                DataTable tblTempInvoice = await _db.GetDataTableAsync("SELECT COUNT(*) as SL FROM #SCHEMA_NAME#.inv_invoiceauth WHERE status='Đã ký' AND batch_num=@batch_num", CommandType.Text, parameter);

                if (tblTempInvoice.Rows.Count == 0)
                {
                    res.Add("number_processed", 0);
                }
                else
                {
                    res.Add("number_processed", Convert.ToInt32(tblTempInvoice.Rows[0]["SL"].ToString()));
                }
            }
            else if (task_type == 3)
            {
                res.Add("start_time", tblBatch.Rows[0]["timestart_receipt"].ToString());
                res.Add("finish_time", tblBatch.Rows[0]["timefinish_receipt"].ToString());
                res.Add("batch_status", tblBatch.Rows[0]["status_receipt"].ToString());
                res.Add("batch_info", tblBatch.Rows[0]["created_info"].ToString());

                parameter.Clear();
                parameter.Add("batch_num", batch_num);

                DataTable tblTempInvoice = await _db.GetDataTableAsync("SELECT COUNT(*) as SL FROM #SCHEMA_NAME#.inv_receipt WHERE batch_num=@batch_num", CommandType.Text, parameter);

                if (tblTempInvoice.Rows.Count == 0)
                {
                    res.Add("number_processed", 0);
                }
                else
                {
                    res.Add("number_processed", Convert.ToInt32(tblTempInvoice.Rows[0]["SL"].ToString()));
                }
            }
            else if (task_type == 4)
            {
                res.Add("start_time", tblBatch.Rows[0]["timestart_sign"].ToString());
                res.Add("finish_time", tblBatch.Rows[0]["timefinish_sign"].ToString());
                res.Add("batch_info", tblBatch.Rows[0]["sign_info"].ToString());
                res.Add("batch_status", tblBatch.Rows[0]["status_sign"].ToString());
                res.Add("is_end_sign", tblBatch.Rows[0]["is_end_sign"].ToString());

                parameter.Clear();
                parameter.Add("batch_num", batch_num);

                DataTable tblTempInvoice = await _db.GetDataTableAsync("SELECT COUNT(*) as SL FROM #SCHEMA_NAME#.inv_receipt WHERE batch_num=@batch_num AND status='Đã ký'", CommandType.Text, parameter);

                if (tblTempInvoice.Rows.Count == 0)
                {
                    res.Add("number_processed", 0);
                }
                else
                {
                    res.Add("number_processed", Convert.ToInt32(tblTempInvoice.Rows[0]["SL"].ToString()));
                }
            }
            else if (task_type == 5)
            {
                res.Add("start_time", tblBatch.Rows[0]["timestart_email"].ToString());
                res.Add("finish_time", tblBatch.Rows[0]["timefinish_email"].ToString());
                res.Add("batch_info", tblBatch.Rows[0]["email_info"].ToString());
                res.Add("batch_status", tblBatch.Rows[0]["status_email"].ToString());

                string type = tblBatch.Rows[0]["type"].ToString();

                parameter.Clear();
                parameter.Add("batch_num", batch_num);

                if (type == "3")
                {
                    sql = "SELECT COUNT(*) as SL FROM #SCHEMA_NAME#.inv_batchdetail WHERE batch_num=@batch_num AND is_send_email=true";
                }
                else
                {
                    sql = "SELECT COUNT(distinct a.inv_invoiceauth_id) as SL FROM #SCHEMA_NAME#.inv_invoiceauth a "
                               + "INNER JOIN #SCHEMA_NAME#.inv_invoice_temp b ON a.invoice_id=b.invoice_id "
                               + "INNER JOIN #SCHEMA_NAME#.wb_email_queue c ON a.inv_invoiceauth_id=c.inv_invoiceauth_id "
                               + "WHERE b.batch_num=@batch_num AND c.is_send=true";
                }

                DataTable tblTempInvoice = await _db.GetDataTableAsync(sql, CommandType.Text, parameter);

                if (tblTempInvoice.Rows.Count == 0)
                {
                    res.Add("number_processed", 0);
                }
                else
                {
                    res.Add("number_processed", Convert.ToInt32(tblTempInvoice.Rows[0]["SL"].ToString()));
                }
            }
            else if (task_type == 6)
            {
                res.Add("start_time", tblBatch.Rows[0]["timestart_email"].ToString());
                res.Add("finish_time", tblBatch.Rows[0]["timefinish_email"].ToString());
                res.Add("batch_info", tblBatch.Rows[0]["email_info"].ToString());
                res.Add("batch_status", tblBatch.Rows[0]["status_email"].ToString());

                parameter.Clear();
                parameter.Add("batch_num", batch_num);

                sql = "SELECT COUNT(distinct a.inv_receipt_id) as SL FROM #SCHEMA_NAME#.inv_receipt a "
                                    + "INNER JOIN #SCHEMA_NAME#.inv_batchdetail b ON a.invoice_id=b.invoice_id "
                                    + "INNER JOIN #SCHEMA_NAME#.wb_email_queue c ON a.inv_receipt_id=c.inv_invoiceauth_id "
                                    + "WHERE b.batch_num=@batch_num AND c.is_send=true";

                DataTable tblTempInvoice = await _db.GetDataTableAsync(sql, CommandType.Text, parameter);

                if (tblTempInvoice.Rows.Count == 0)
                {
                    res.Add("number_processed", 0);
                }
                else
                {
                    res.Add("number_processed", Convert.ToInt32(tblTempInvoice.Rows[0]["SL"].ToString()));
                }
            }

            return res;

        }

        public JObject CreateTask(JObject model)
        {
            JObject res = new JObject();

            try
            {
                string task_name = model["task_name"].ToString();
                _rabbitmqService.PublishQueue(task_name, model["msg"].ToString());

                res.Add("ok", true);
                res.Add("task_create", true);
                res.Add("task_name", task_name);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;

        }

        #endregion

    }
}