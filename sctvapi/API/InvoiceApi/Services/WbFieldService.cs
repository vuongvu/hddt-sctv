﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InvoiceApi.Data;
using InvoiceApi.Data.Domain;
using InvoiceApi.Models;
using System.Threading.Tasks;
using InvoiceApi.Util;
using System.Text;
using System.Data;

namespace InvoiceApi.Services
{
    public class WbFieldService : IWbFieldService
    {
        #region Fields

        private IMinvoiceDbContext _minvoiceDbContext;

        #endregion

        #region Ctor

        public WbFieldService(IMinvoiceDbContext minvoiceDbContext)
        {
            this._minvoiceDbContext = minvoiceDbContext;
        }

        #endregion

        #region Methods
        public async Task<DataTableExtend> GetAll()
        {
            string sql = "SELECT *,wb_field_id as id FROM #SCHEMA_NAME#.wb_field ORDER BY wb_tab_id,ord";
            DataTableExtend tblData = await this._minvoiceDbContext.GetDataTableAsync(sql);

            return tblData;
        }

        public async Task<JObject> GetFields(int start, int count, string filter)
        {
            JObject json = new JObject();
            try
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("SELECT *,wb_field_id as id FROM #SCHEMA_NAME#.wb_field ");

                string where = "";

                if (filter != null && filter != "null" && filter != " ")
                {
                    JArray array = JArray.Parse(filter);

                    for (int i = 0; i < array.Count; i++)
                    {
                        JObject js = (JObject)array[i];
                        string type = js["columnType"].ToString();

                        if (!string.IsNullOrEmpty(js["value"].ToString()))
                        {
                            where += " AND " + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), type);
                        }
                    }

                    if (!string.IsNullOrEmpty(where))
                    {
                        where = " WHERE 1 = 1 " + where;
                    }
                }

                string sql = builder.ToString() + where + " ORDER BY code LIMIT " + count + " OFFSET " + start;
                DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql);

                json.Add("data", JArray.FromObject(tblData));
                json.Add("pos", start);

                if (start == 0)
                {
                    sql = "SELECT COUNT(*) as total_count FROM ( " + builder.ToString() + where + ")";
                    tblData = await this._minvoiceDbContext.GetDataTableAsync(sql);

                    json.Add("total_count", tblData.Rows[0]["total_count"].ToString());
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }

        public async Task<JObject> SaveChange(JObject model)
        {
            JObject json = new JObject();

            try
            {
                Dictionary<string, object> dicParameter = new Dictionary<string, object>();



                string id = model["id"] == null ? Guid.NewGuid().ToString() : model["id"].ToString();

                dicParameter.Add("wb_field_id", Guid.Parse(id));

                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_field WHERE wb_field_id=@wb_field_id";

                DataTableExtend tblData = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);
                tblData.TableName = "wb_field";

                if (tblData.Rows.Count == 0)
                {

                    model["id"] = id;
                    model["wb_field_id"] = id;

                    DataRow row = tblData.CreateDataRow(model);
                    sql = tblData.GenerateInsertSQL();

                    await this._minvoiceDbContext.ExecuteNoneQueryAsyncNotCheckHTML(sql, row);
                }
                else
                {
                    DataRow row = tblData.Rows[0];
                    row = tblData.ImportData(row, model);

                    sql = tblData.GenerateUpdateSQL();

                    await this._minvoiceDbContext.ExecuteNoneQueryAsyncNotCheckHTML(sql, row);
                }



                json.Add("ok", true);
                json.Add("id", id);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<JObject> Delete(string id)
        {
            JObject json = new JObject();

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("wb_field_id", Guid.Parse(id));

                string sql = "DELETE FROM #SCHEMA_NAME#.wb_field WHERE wb_field_id=@wb_field_id";
                await this._minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                json.Add("ok", true);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }


        public async Task<DataTableExtend> GetFieldsByTabId(string id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("wb_tab_id", Guid.Parse(id));

            string sql = "SELECT *,wb_field_id as id FROM #SCHEMA_NAME#.wb_field WHERE wb_tab_id=@wb_tab_id ORDER BY ord";
            DataTableExtend tblData = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

            return tblData;
        }

        #endregion
    }
}