﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface ITokenService
    {
        JArray SelectTokens();
        Task<JObject> SaveTokens(JObject model);
        JArray GetAllCertificates();
        Task<JObject> SaveCerts(JObject model);
        JObject SignXml(JObject model);
        JObject SignTB01AC(JObject model);
        Task<JObject> SaveTokenFile(JObject obj, string username, string store_code);
        Task<JArray> AllCertificatesFile();
        Task<JObject> SaveChukysoFile(JObject model);
        Task<string> UploadCertFileAsync(string ma_dvcs, string username, string pass, byte[] bytes, string fileName);
        Task<JObject> SaveSimPKI(JObject model);
        Task<JObject> SaveHsmSCTV(JObject model);
    }
}
