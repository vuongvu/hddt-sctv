﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceApi.Data.Domain;
using InvoiceApi.Util;

namespace InvoiceApi.Services
{
    public partial interface IWbFieldService
    {
        Task<DataTableExtend> GetAll();
        Task<DataTableExtend> GetFieldsByTabId(string id);
        Task<JObject> GetFields(int start, int count, string filter);
        Task<JObject> SaveChange(JObject model);
        Task<JObject> Delete(string id);
    }
}
