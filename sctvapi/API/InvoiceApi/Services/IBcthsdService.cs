﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface IBcthsdService
    {
        Task<JObject> GetInfoDonvi();
        Task<JObject> GetTenFile();
        Task<JObject> Check_xulyHD(string ma_dvcs,JObject model);
        Task<JArray> BcTinhhinhsudung(string ma_dvcs,JObject model);
        Task<JObject> bcthsd_INSERT(JObject model);
        JObject ExportXMLString(string ma_dvcs,JObject model);
        Task<string> GetStringXML(string ma_dvcs, string tokhai_quy, string tokhai_thang, string nam, string thang, string tu_ngay, string den_ngay);
        Task<JObject> bangkebanra(JObject model);
    }
}
