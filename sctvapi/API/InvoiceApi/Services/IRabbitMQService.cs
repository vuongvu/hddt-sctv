﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface IRabbitMQService
    {
        void PublishQueue(string queueName,string msg);
        void PublishTaskQueue(string queueName, string msg);
    }
}
