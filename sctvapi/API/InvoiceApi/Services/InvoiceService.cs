﻿using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.BarCode;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraReports.UI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using InvoiceApi.Util;
using InvoiceApi.Data.Domain;
using ICSharpCode.SharpZipLib.Zip;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using HtmlAgilityPack;
using InvoiceApi.Data;
using System.Security.Cryptography.X509Certificates;
using System.Collections;
using log4net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Configuration;
using Syncfusion.Pdf;
using System.Security.Cryptography.Xml;
using Newtonsoft.Json;
using InvoiceApi.Models;

namespace InvoiceApi.Services
{
    public class InvoiceService : IInvoiceService
    {
        #region Fields

        private IMinvoiceDbContext _minvoiceDbContext;
        private IMasterInvoiceDbContext _masterInvoiceDbContext;
        private INopDbContext _nopDbContext;
        private IEmailService _emailService;
        private ICacheManager _cacheManager;
        private ILogService _logService;
        private IWebHelper _webHelper;
        private IRabbitMQService _rabbitmqService;
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        #endregion

        public InvoiceService(
                                INopDbContext nopDbContext,
                                IEmailService emailService,
                                ICacheManager cacheManager,
                                ILogService logService,
                                IWebHelper webHelper,
                                IMinvoiceDbContext minvoiceDbContext,
                                IRabbitMQService rabbitmqService,
                                IMasterInvoiceDbContext masterInvoiceDbContext
            )
        {
            this._nopDbContext = nopDbContext;
            this._emailService = emailService;
            this._cacheManager = cacheManager;
            this._logService = logService;
            this._webHelper = webHelper;
            this._minvoiceDbContext = minvoiceDbContext;
            this._rabbitmqService = rabbitmqService;
            this._masterInvoiceDbContext = masterInvoiceDbContext;
        }
        public async Task<JArray> GetListCertificatesFile(string userName, string ma_dvcs)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("username", userName);
            parameters.Add("ma_dvcs", ma_dvcs);
            //string sql = "SELECT a.*,d.pass,d.path,a.so_serial as id FROM #SCHEMA_NAME#.pl_certificate a "
            //            + "INNER JOIN #SCHEMA_NAME#.ckspermission b ON a.chukyso_id=b.chukyso_id "
            //            + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id "
            //            + "INNER JOIN #SCHEMA_NAME#.wb_tokenfile d ON a.ma_dvcs=d.ma_dvcs AND a.so_serial=d.serialNumber "
            //            + "WHERE c.username=@username AND a.ma_dvcs=@ma_dvcs";

            string sql = "SELECT a.pl_certificate_id,a.issuer,a.signer,a.subject_name,a.begin_date,a.end_date,a.cert_type,d.pass,a.cer_serial as id,a.cer_serial,a.cer_serial as so_serial \n"
                     + "   FROM #SCHEMA_NAME#.pl_certificate a "
                     + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id "
                     + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id "
                     + "LEFT JOIN #SCHEMA_NAME#.wb_token d ON a.branch_code=d.branch_code AND a.token_serial=d.serial_number "
                     + "WHERE c.username=@username AND a.branch_code=@ma_dvcs";

            DataTable tblCerts = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

            JArray data = JArray.FromObject(tblCerts);

            return data;
        }

        public async Task<byte[]> PrintInvoiceFromId(string id, string folder, string type, string no_preview, bool inThuTu = false)
        {
            byte[] results = await PrintInvoiceFromId(id, folder, type, false, no_preview, inThuTu);
            return results;
        }

        public async Task<byte[]> PrintListReceipt(JObject model, string folder, bool inchuyendoi, string type)
        {
            JArray array = (JArray)model["invs"];

            XtraReport totalReport = new XtraReport();
            totalReport.PrintingSystem.ContinuousPageNumbering = false;

            for (int _inv = 0; _inv < array.Count; _inv++)
            {

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("invoice_id", Convert.ToInt64(array[_inv].ToString()));

                DataTable tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_receipt WHERE invoice_id=@invoice_id", CommandType.Text, parameters);
                string inv_InvoiceAuth_id = tblInv_InvoiceAuth.Rows[0]["inv_receipt_id"].ToString();

                parameters.Clear();
                parameters.Add("inv_receipt_id", Guid.Parse(inv_InvoiceAuth_id));

                DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_receiptxmldata WHERE inv_receipt_id=@inv_receipt_id", CommandType.Text, parameters);

                string xml = "";

                if (tblInvoiceXmlData.Rows.Count > 0)
                {
                    xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                }
                else
                {
                    parameters.Clear();
                    parameters.Add("inv_InvoiceAuth_id", Guid.Parse(inv_InvoiceAuth_id));

                    DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlreceipt('#SCHEMA_NAME#',@inv_InvoiceAuth_id)", CommandType.Text, parameters);
                    xml = tblData.Rows[0][0].ToString();
                }

                string fileReceipt = folder + "\\Receipt.repx";
                XtraReport report = XtraReport.FromFile(fileReceipt, true);


                report.Name = "XtraReport1";
                report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                {
                    ds.ReadXmlSchema(xmlReader);
                    xmlReader.Close();
                }

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                {
                    ds.ReadXml(xmlReader);
                    xmlReader.Close();
                }

                if (ds.Tables.Contains("TblXmlData"))
                {
                    ds.Tables.Remove("TblXmlData");
                }


                DataTable tblXmlData = new DataTable();
                tblXmlData.TableName = "TblXmlData";
                tblXmlData.Columns.Add("data");

                DataRow r = tblXmlData.NewRow();
                r["data"] = xml;
                tblXmlData.Rows.Add(r);
                ds.Tables.Add(tblXmlData);

                string datamember = report.DataMember;

                if (datamember.Length > 0)
                {
                    if (ds.Tables.Contains(datamember))
                    {
                        DataTable tblChiTiet = ds.Tables[datamember];

                        int rowcount = ds.Tables[datamember].Rows.Count;

                    }
                }

                if (inchuyendoi)
                {
                    var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                    if (tblInChuyenDoi != null)
                    {
                        tblInChuyenDoi.Visible = true;
                    }

                    if (report.Parameters["MSG_HD_TITLE"] != null)
                    {
                        report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                    }

                    if (report.Parameters["NGUOI_IN_CDOI"] != null)
                    {
                        report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                        report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                    }

                    if (report.Parameters["NGAY_IN_CDOI"] != null)
                    {
                        report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                        report.Parameters["NGAY_IN_CDOI"].Visible = true;
                    }
                }

                report.DataSource = ds;
                report.CreateDocument();

                if (tblInv_InvoiceAuth.Rows[0]["status"].ToString() == "Xóa bỏ")
                {

                    Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                    int pageCount = report.Pages.Count;


                    for (int i = 0; i < pageCount; i++)
                    {
                        Page page = report.Pages[i];
                        PageWatermark pmk = new PageWatermark();
                        pmk.Image = bmp;
                        page.AssignWatermark(pmk);
                    }

                }

                for (int _pageIndex = 0; _pageIndex < report.Pages.Count; _pageIndex++)
                {

                    Page page = report.Pages[_pageIndex];

                    if (report.Watermark != null)
                    {
                        if (report.Watermark.Image != null)
                        {
                            Image img = report.Watermark.Image;
                            Bitmap bmp = new Bitmap(img);

                            PageWatermark pmk = new PageWatermark();

                            pmk.Image = bmp;
                            pmk.ImageAlign = report.Watermark.ImageAlign;
                            pmk.ImageTiling = report.Watermark.ImageTiling;
                            pmk.ImageTransparency = report.Watermark.ImageTransparency;
                            pmk.ImageViewMode = report.Watermark.ImageViewMode;
                            pmk.ShowBehind = report.Watermark.ShowBehind;

                            page.AssignWatermark(pmk);

                        }
                    }



                    totalReport.Pages.Add(page);


                }
            }

            MemoryStream ms = new MemoryStream();

            if (type == "Image")
            {
                var options = new ImageExportOptions(ImageFormat.Png)
                {
                    ExportMode = ImageExportMode.SingleFilePageByPage,
                };
                totalReport.ExportToImage(ms, options);
            }
            else if (type == "Excel")
            {
                totalReport.ExportToXlsx(ms);
            }
            else if (type == "Rtf")
            {
                totalReport.ExportToRtf(ms);
            }
            else
            {
                totalReport.ExportToPdf(ms);
            }

            byte[] bytes = ms.ToArray();
            ms.Close();

            return bytes;
        }

        public async Task<byte[]> PrintListInvoice(JObject model, string folder, bool inchuyendoi, string type)
        {
            JArray array = (JArray)model["invs"];

            string invReport = "";
            string hang_nghin = "";
            string thap_phan = "";

            bool isInvoiceId = false;

            if (model["isInvoiceId"] != null)
            {
                isInvoiceId = Convert.ToBoolean(model["isInvoiceId"]);
            }

            DataTable tblCtthongbao = null;
            XtraReport totalReport = null;

            byte[] byteOut = null;

            String folderPrint = CommonConstants.FOLDER_PRINTLIST + "/" + CommonManager.GetTimestamp(DateTime.Now);
            CommonManager.createFolder(folderPrint);

            //var listTmp = new List<byte[]>();

            for (int _inv = 0; _inv < array.Count; _inv++)
            {
                try
                {
                    totalReport = new XtraReport();
                    Guid inv_InvoiceAuth_id = Guid.NewGuid();
                    DataTable tblInv_InvoiceAuth = null;

                    if (isInvoiceId)
                    {
                        tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@P_0", Convert.ToInt64(array[_inv].ToString()));

                        if (tblInv_InvoiceAuth.Rows.Count == 0)
                        {
                            continue;
                        }
                        else
                        {
                            inv_InvoiceAuth_id = Guid.Parse(tblInv_InvoiceAuth.Rows[0]["inv_InvoiceAuth_id"].ToString());
                        }
                    }
                    else
                    {
                        inv_InvoiceAuth_id = Guid.Parse(array[_inv].ToString());

                        tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@P_0", inv_InvoiceAuth_id);
                    }

                    string invoice_status = tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString();
                    string adjustmentType = tblInv_InvoiceAuth.Rows[0]["adjustment_type"].ToString();

                    if (invoice_status == "7" || invoice_status == "13" || adjustmentType == "7")
                    {
                        continue;
                    }

                    DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@P_0", inv_InvoiceAuth_id);

                    string xml = "";
                    string msg_tb = "";

                    if (tblInvoiceXmlData.Rows.Count > 0)
                    {
                        xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                    }
                    else
                    {
                        DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@P_0,'C')", inv_InvoiceAuth_id);
                        xml = tblData.Rows[0][0].ToString();
                    }
                    // QuyenNH modified
                    xml = await ExportInvoiceXml(xml, inv_InvoiceAuth_id);

                    xml = FunctionUtil.ReverseXml(xml);
                    string inv_InvoiceCode_id = tblInv_InvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();
                    int trang_thai_hd = Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["invoice_status"]);
                    string inv_originalId = tblInv_InvoiceAuth.Rows[0]["original_invoice_id"].ToString();

                    Dictionary<string, object> dicParam = new Dictionary<string, object>();

                    if (tblCtthongbao == null)
                    {
                        dicParam.Clear();
                        dicParam.Add("inv_notificationdetail_id", Guid.Parse(inv_InvoiceCode_id));

                        tblCtthongbao = await _minvoiceDbContext.GetDataTableAsync("SELECT a.*,b.thousand_point as hang_nghin,b.decimal_point as thap_phan "
                            + "FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id "
                            + "WHERE a.inv_notificationdetail_id=@inv_notificationdetail_id", CommandType.Text, dicParam);

                        hang_nghin = tblCtthongbao.Rows[0]["hang_nghin"].ToString();
                        thap_phan = tblCtthongbao.Rows[0]["thap_phan"].ToString();
                    }

                    //string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", tblCtthongbao.Rows[0]["inv_notificationdetail_id"]);

                    //XtraReport report = _cacheManager.Get<XtraReport>(cacheReportKey);
                    XtraReport report = new XtraReport();
                    //if (report == null)
                    //{


                    if (invReport.Length == 0)
                    {
                        dicParam.Clear();
                        dicParam.Add("sl_invoice_template_id", Guid.Parse(tblCtthongbao.Rows[0]["sl_invoice_template_id"].ToString()));

                        DataTable tblDmmauhd = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.sl_invoice_template "
                            + "WHERE sl_invoice_template_id=@sl_invoice_template_id", CommandType.Text, dicParam);
                        invReport = tblDmmauhd.Rows[0]["report"].ToString();
                    }

                    if (invReport.Length > 0)
                    {
                        report = ReportUtil.LoadReportFromString(invReport);
                        // _cacheManager.Set(cacheReportKey, report, 30);
                    }
                    else
                    {
                        throw new Exception("Không tải được mẫu hóa đơn");
                    }

                    //   }


                    report.Name = "XtraReport1";
                    report.ScriptReferencesString = "AccountSignature.dll";

                    DataSet ds = new DataSet();

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                    {
                        ds.ReadXmlSchema(xmlReader);
                        xmlReader.Close();
                    }

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                    {
                        ds.ReadXml(xmlReader);
                        xmlReader.Close();
                    }

                    if (ds.Tables.Contains("TblXmlData"))
                    {
                        ds.Tables.Remove("TblXmlData");
                    }


                    DataTable tblXmlData = new DataTable();
                    tblXmlData.TableName = "TblXmlData";
                    tblXmlData.Columns.Add("data");

                    DataRow r = tblXmlData.NewRow();
                    r["data"] = xml;
                    tblXmlData.Rows.Add(r);
                    ds.Tables.Add(tblXmlData);

                    string datamember = report.DataMember;

                    if (datamember.Length > 0)
                    {
                        if (ds.Tables.Contains(datamember))
                        {
                            DataTable tblChiTiet = ds.Tables[datamember];

                            int rowcount = ds.Tables[datamember].Rows.Count;

                        }
                    }


                    if (trang_thai_hd == 11 || trang_thai_hd == 13 || trang_thai_hd == 17)
                    {
                        dicParam.Clear();
                        dicParam.Add("original_invoice_id", inv_InvoiceAuth_id);

                        string sql = "SELECT STRING_AGG ( a.invoice_number || ' ngày ' || to_char(a.invoice_issued_date,'DD/MM/YYYY') || ' mẫu số ' || a.template_code || ' ký hiệu ' || a.invoice_series ,','  ORDER BY a.invoice_issued_date desc,a.invoice_number desc) msg FROM #SCHEMA_NAME#.inv_invoiceauth a "
                                + "WHERE a.original_invoice_id=@original_invoice_id GROUP BY a.original_invoice_id";

                        DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                        if (tblInv.Rows.Count > 0)
                        {
                            string msg = tblInv.Rows[0]["msg"].ToString();

                            if (msg.Length > 0)
                            {
                                dicParam.Clear();
                                dicParam.Add("code", trang_thai_hd);

                                DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=@code", CommandType.Text, dicParam);
                                string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();

                                msg_tb = "Hóa đơn " + loai + " bởi hóa đơn số: " + msg;

                            }

                        }
                    }

                    if (Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]) == 7)
                    {
                        msg_tb = "";
                    }

                    if (report.Parameters["MSG_TB"] != null)
                    {
                        report.Parameters["MSG_TB"].Value = msg_tb;
                    }

                    var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

                    if (lblHoaDonMau != null)
                    {
                        lblHoaDonMau.Visible = false;
                    }

                    if (inchuyendoi)
                    {
                        var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                        if (tblInChuyenDoi != null)
                        {
                            tblInChuyenDoi.Visible = true;
                        }

                        if (report.Parameters["MSG_HD_TITLE"] != null)
                        {
                            report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                        }

                        if (report.Parameters["NGUOI_IN_CDOI"] != null)
                        {
                            report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                            report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                        }

                        if (report.Parameters["NGAY_IN_CDOI"] != null)
                        {
                            report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                            report.Parameters["NGAY_IN_CDOI"].Visible = true;
                        }
                    }

                    report.DataSource = ds;

                    try
                    {
                        bool giamthuebanhang20 = tblInv_InvoiceAuth.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblInv_InvoiceAuth.Rows[0]["giamthuebanhang20"].ToString());
                        string dvtte = tblInv_InvoiceAuth.Rows[0]["currency_code"]?.ToString();

                        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                        string tongTienGiam = double.Parse(DoubleStrToString(tblInv_InvoiceAuth.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                        tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");

                        string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";
                        if (giamthuebanhang20)
                        {
                            var dt = ds.Tables["invoiceinformation"];
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Columns.Contains("amount_by_word"))
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        var docTien = (dt.Rows[i]["amount_by_word"]?.ToString()).Trim();
                                        docTien = String.IsNullOrEmpty(docTien) ? "Không đồng." : docTien;
                                        ds.Tables["invoiceinformation"].Rows[i]["amount_by_word"] = docTien + " " + Environment.NewLine + thueGTGTBanHang;
                                        report.DataSource = ds;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Log.Error(ex);
                    }

                    var t = Task.Run(() =>
                    {
                        var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        newCulture.NumberFormat.NumberDecimalSeparator = thap_phan;
                        newCulture.NumberFormat.NumberGroupSeparator = hang_nghin;

                        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
                        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;

                        report.CreateDocument();

                    });

                    t.Wait();


                    if (tblInv_InvoiceAuth.Columns.Contains("list_number"))
                    {
                        if (tblInv_InvoiceAuth.Rows[0]["list_number"].ToString().Length > 0)
                        {
                            string fileName = folder + "\\BangKeDinhKem.repx";

                            XtraReport rpBangKe = null;

                            if (!File.Exists(fileName))
                            {
                                rpBangKe = new XtraReport();
                                rpBangKe.SaveLayout(fileName);
                            }
                            else
                            {
                                rpBangKe = XtraReport.FromFile(fileName, true);
                            }

                            rpBangKe.ScriptReferencesString = "AccountSignature.dll";
                            rpBangKe.Name = "rpBangKe";
                            rpBangKe.DisplayName = "BangKeDinhKem.repx";

                            rpBangKe.DataSource = report.DataSource;

                            rpBangKe.CreateDocument();
                            report.Pages.AddRange(rpBangKe.Pages);
                        }

                    }

                    if (tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString() == "7")
                    {

                        Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        int pageCount = report.Pages.Count;


                        for (int i = 0; i < pageCount; i++)
                        {
                            Page page = report.Pages[i];
                            PageWatermark pmk = new PageWatermark();
                            pmk.Image = bmp;
                            page.AssignWatermark(pmk);
                        }

                        string fileName = folder + "\\BienBanXoaBo.repx";
                        XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                        rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                        rpBienBan.Name = "rpBienBan";
                        rpBienBan.DisplayName = "BienBanXoaBo.repx";

                        rpBienBan.DataSource = report.DataSource;
                        rpBienBan.DataMember = report.DataMember;

                        rpBienBan.CreateDocument();

                        rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                        report.PrintingSystem.ContinuousPageNumbering = false;

                        report.Pages.AddRange(rpBienBan.Pages);

                        int idx = pageCount;
                        pageCount = report.Pages.Count;

                        for (int i = idx; i < pageCount; i++)
                        {
                            PageWatermark pmk = new PageWatermark();
                            pmk.ShowBehind = false;
                            report.Pages[i].AssignWatermark(pmk);
                        }

                    }

                    if (trang_thai_hd == 19 || trang_thai_hd == 21 || trang_thai_hd == 5)
                    {

                        string rp_file = trang_thai_hd == 19 || trang_thai_hd == 21 ? "INCT_BBDC_GT.repx" : "INCT_BBDC_DD.repx";
                        string rp_code = trang_thai_hd == 19 || trang_thai_hd == 21 ? "#SCHEMA_NAME#.qr_inct_hd_dieuchinhgt" : "#SCHEMA_NAME#.qr_inct_hd_dieuchinhdg";

                        string fileName = folder + "\\" + rp_file;
                        XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                        rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                        rpBienBan.Name = "rpBienBanDC";
                        rpBienBan.DisplayName = rp_file;

                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("p_branch_code", _webHelper.GetDvcs());
                        //parameters.Add("p_schemaname", "#SCHEMA_NAME#");
                        parameters.Add("p_document_id", inv_InvoiceAuth_id);

                        DataSet dataSource = await this._minvoiceDbContext.GetDataSetAsyncPrintf(rp_code, CommandType.StoredProcedure, parameters);

                        rpBienBan.DataSource = dataSource;
                        rpBienBan.DataMember = dataSource.Tables[0].TableName;

                        rpBienBan.CreateDocument();

                        rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                        report.PrintingSystem.ContinuousPageNumbering = false;

                        report.Pages.AddRange(rpBienBan.Pages);

                        Page page = report.Pages[report.Pages.Count - 1];
                        page.AssignWatermark(new PageWatermark());

                    }

                    if (trang_thai_hd == 3)
                    {

                        string rp_file = "INCT_BBDTH.repx";
                        string rp_code = "#SCHEMA_NAME#.qr_inct_bbth";

                        string fileName = folder + "\\" + rp_file;
                        XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                        rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                        rpBienBan.Name = "rpBienBanThuHoi";
                        rpBienBan.DisplayName = rp_file;

                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("p_branch_code", _webHelper.GetDvcs());
                        //parameters.Add("p_schemaname", "#SCHEMA_NAME#");
                        parameters.Add("p_document_id", inv_InvoiceAuth_id);

                        DataSet dataSource = await this._minvoiceDbContext.GetDataSetAsyncPrintf(rp_code, CommandType.StoredProcedure, parameters);

                        rpBienBan.DataSource = dataSource;
                        rpBienBan.DataMember = dataSource.Tables[0].TableName;

                        rpBienBan.CreateDocument();

                        rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                        report.PrintingSystem.ContinuousPageNumbering = false;

                        report.Pages.AddRange(rpBienBan.Pages);

                        Page page = report.Pages[report.Pages.Count - 1];
                        page.AssignWatermark(new PageWatermark());

                    }

                    if (trang_thai_hd == 13 || trang_thai_hd == 17)
                    {
                        Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        int pageCount = report.Pages.Count;

                        for (int i = 0; i < pageCount; i++)
                        {
                            PageWatermark pmk = new PageWatermark();
                            pmk.Image = bmp;
                            report.Pages[i].AssignWatermark(pmk);
                        }
                    }

                    totalReport.PrintingSystem.ContinuousPageNumbering = false;


                    for (int _pageIndex = 0; _pageIndex < report.Pages.Count; _pageIndex++)
                    {

                        Page page = report.Pages[_pageIndex];

                        if (report.Watermark != null)
                        {
                            if (report.Watermark.Image != null)
                            {
                                Image img = report.Watermark.Image;
                                Bitmap bmp = new Bitmap(img);

                                PageWatermark pmk = new PageWatermark();

                                pmk.Image = bmp;
                                pmk.ImageAlign = report.Watermark.ImageAlign;
                                pmk.ImageTiling = report.Watermark.ImageTiling;
                                pmk.ImageTransparency = report.Watermark.ImageTransparency;
                                pmk.ImageViewMode = report.Watermark.ImageViewMode;
                                pmk.ShowBehind = report.Watermark.ShowBehind;

                                page.AssignWatermark(pmk);

                            }
                        }

                        totalReport.Pages.Add(page);

                    }

                    MemoryStream ms = new MemoryStream();

                    if (type == "Image")
                    {
                        var options = new ImageExportOptions(ImageFormat.Png)
                        {
                            ExportMode = ImageExportMode.SingleFilePageByPage,
                        };
                        totalReport.ExportToImage(ms, options);
                    }
                    else if (type == "Excel")
                    {
                        totalReport.ExportToXlsx(ms);
                    }
                    else if (type == "Rtf")
                    {
                        totalReport.ExportToRtf(ms);
                    }
                    else
                    {
                        totalReport.ExportToPdf(ms);
                    }

                    //byte[] bytes = ms.ToArray();
                    //listTmp.Add(bytes);

                    CommonManager.createFileRandomName(folderPrint, ms.ToArray());

                    ms.Close();

                }
                catch (Exception ex)
                {
                    _log.Error(ex.Message);
                }

            }

            try
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(folderPrint);

                //Get the PDF files in folder path into FileInfo
                FileInfo[] files = directoryInfo.GetFiles("*.pdf");

                //Create a new PDF document 
                PdfDocument document = new PdfDocument();

                //Set enable memory optimization as true 
                document.EnableMemoryOptimization = true;

                foreach (FileInfo file in files)
                {
                    //Load the PDF document 
                    FileStream fileStream = new FileStream(file.FullName, FileMode.Open);
                    Syncfusion.Pdf.Parsing.PdfLoadedDocument loadedDocument = new Syncfusion.Pdf.Parsing.PdfLoadedDocument(fileStream);

                    //Merge PDF file
                    PdfDocumentBase.Merge(document, loadedDocument);

                    //Close the existing PDF document 
                    loadedDocument.Close(true);
                }


                String fileNameFinal = CommonManager.GetTimestamp(DateTime.Now);

                //Creates a new Memory stream
                //MemoryStream stream = new MemoryStream();

                //Save the PDF document
                document.Save(folderPrint + "/" + fileNameFinal + ".pdf");

                //Close the instance of PdfDocument
                document.Close(true);

                //byteOut = stream.ToArray();

                byteOut = System.IO.File.ReadAllBytes(folderPrint + "/" + fileNameFinal + ".pdf");

                //stream.Close();

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }


            //byte[] byteOut = FunctionUtil.MergePdf(listTmp);

            return byteOut;
        }

        public async Task<string> ExportInvoiceXml(string xml, Guid inv_InvoiceAuth_id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            XmlElement root = xmldoc.DocumentElement;

            // kiểm tra có phải hóa đơn mBill không?
            if (root.FirstChild.Name.ToString() == "inv:invoiceData")
            {
                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id,'C')", CommandType.Text, parameters);
                string xml_template = tblData.Rows[0][0].ToString();

                // đọc dữ liệu từ function invoicexml_data get thông tin signature của hóa đơn mBill
                XmlNodeList nodeList = xmldoc.GetElementsByTagName("Signature");

                string signature = string.Empty;
                foreach (XmlNode node in nodeList)
                {
                    // Signature mBill
                    signature = node.InnerXml;
                }

                // gán Signature mBill vào xml_template
                XmlDocument xmldoc_template = new XmlDocument();
                xmldoc_template.LoadXml(xml_template);
                xmldoc_template["HoaDonDienTu"]["Signatures"].AppendChild(xmldoc_template.CreateElement("Signature"));

                xmldoc_template.SelectSingleNode("HoaDonDienTu/Signatures/Signature").InnerXml = signature;

                // lấy số bảo mật mBill
                XmlNodeList nodeList_mBill = xmldoc.GetElementsByTagName("inv:containerNumber");
                string sobaomat = string.Empty;
                foreach (XmlNode node in nodeList_mBill)
                {
                    // security_number mBill
                    sobaomat = node.InnerXml;
                }
                xml = CommonManager.ExportSaveInvoiceXml(xmldoc_template.OuterXml, sobaomat);
            }
            return xml;
        }

        public async Task<byte[]> PrintInvoiceFromId(string id, string folder, string type, bool inchuyendoi, string no_preview, bool inThuTu = false)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                Guid inv_InvoiceAuth_id = Guid.Parse(id);

                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);


                DataTable tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);
                DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);

                if (tblInvoiceXmlData.Rows.Count > 0)
                {
                    xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                }
                else
                {
                    parameters.Clear();
                    parameters.Add("id", inv_InvoiceAuth_id);

                    DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@id,'C')", CommandType.Text, parameters);
                    xml = tblData.Rows[0][0].ToString();
                }

                if (inThuTu)
                {
                    parameters.Clear();
                    parameters.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);
                    //parameters.Add("id", inv_InvoiceAuth_id);
                    DataTable dtInvocieDetails = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlinvoice_detail_vn('#SCHEMA_NAME#',@inv_invoiceauth_id)", CommandType.Text, parameters);
                    var xmlDeltal = dtInvocieDetails.Rows[0][0].ToString();
                    XDocument doc = XDocument.Parse(xml);
                    XElement docDetail = XElement.Parse(xmlDeltal);
                    List<XElement> lstNode = doc.Descendants("ChiTietHoaDon").ToList();
                    for (int i = 0; i < lstNode.Count; i++)
                    {
                        lstNode[i].ReplaceWith(docDetail);
                    }
                    xml = doc.ToString();
                }
                // QuyenNH modified

                xml = await ExportInvoiceXml(xml, inv_InvoiceAuth_id);

                xml = FunctionUtil.ReverseXml(xml);
                string inv_InvoiceCode_id = tblInv_InvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();
                int trang_thai_hd = Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["invoice_status"]);
                string inv_originalId = tblInv_InvoiceAuth.Rows[0]["original_invoice_id"].ToString();

                parameters.Clear();
                parameters.Add("inv_notificationdetail_id", Guid.Parse(inv_InvoiceCode_id));

                DataTable tblCtthongbao = await _minvoiceDbContext.GetDataTableAsync("SELECT a.*,b.thousand_point as hang_nghin,b.decimal_point as thap_phan FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id WHERE a.inv_notificationdetail_id=@inv_notificationdetail_id", CommandType.Text, parameters);

                string hang_nghin = tblCtthongbao.Rows[0]["hang_nghin"].ToString();
                string thap_phan = tblCtthongbao.Rows[0]["thap_phan"].ToString();

                string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", tblCtthongbao.Rows[0]["inv_notificationdetail_id"]);

                //XtraReport report = _cacheManager.Get<XtraReport>(cacheReportKey);
                XtraReport report = new XtraReport();
                //if (report == null)
                //{
                parameters.Clear();
                parameters.Add("sl_invoice_template_id", Guid.Parse(tblCtthongbao.Rows[0]["sl_invoice_template_id"].ToString()));
                DataTable tblDmmauhd = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id", CommandType.Text, parameters);
                string invReport = tblDmmauhd.Rows[0]["report"].ToString();

                if (invReport.Length > 0)
                {
                    report = ReportUtil.LoadReportFromString(invReport);
                    // _cacheManager.Set(cacheReportKey, report, 30);
                }
                else
                {
                    throw new Exception("Không tải được mẫu hóa đơn");
                }

                //   }


                report.Name = "XtraReport1";
                report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                {
                    ds.ReadXmlSchema(xmlReader);
                    xmlReader.Close();
                }

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                {
                    ds.ReadXml(xmlReader);
                    xmlReader.Close();
                }

                if (ds.Tables.Contains("TblXmlData"))
                {
                    ds.Tables.Remove("TblXmlData");
                }


                DataTable tblXmlData = new DataTable();
                tblXmlData.TableName = "TblXmlData";
                tblXmlData.Columns.Add("data");

                DataRow r = tblXmlData.NewRow();
                r["data"] = xml;
                tblXmlData.Rows.Add(r);
                ds.Tables.Add(tblXmlData);

                string datamember = report.DataMember;

                if (datamember.Length > 0)
                {
                    if (ds.Tables.Contains(datamember))
                    {
                        DataTable tblChiTiet = ds.Tables[datamember];

                        int rowcount = ds.Tables[datamember].Rows.Count;

                    }
                }

                if (trang_thai_hd == 11 || trang_thai_hd == 13 || trang_thai_hd == 17)
                {
                    parameters.Clear();
                    parameters.Add("original_invoice_id", inv_InvoiceAuth_id);

                    string sql = "SELECT STRING_AGG ( a.invoice_number || ' ngày ' || to_char(a.invoice_issued_date,'DD/MM/YYYY') || ' mẫu số ' || a.template_code || ' ký hiệu ' || a.invoice_series ,','  ORDER BY a.invoice_issued_date desc,a.invoice_number desc) msg FROM #SCHEMA_NAME#.inv_invoiceauth a "
                            + "WHERE a.original_invoice_id=@original_invoice_id GROUP BY a.original_invoice_id";

                    DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                    if (tblInv.Rows.Count > 0)
                    {
                        string msg = tblInv.Rows[0]["msg"].ToString();

                        if (msg.Length > 0)
                        {
                            parameters.Clear();
                            parameters.Add("code", trang_thai_hd);
                            DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=@code", CommandType.Text, parameters);
                            string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();


                            msg_tb = "Hóa đơn " + loai + " bởi hóa đơn số: " + msg;

                        }

                    }
                }

                if (Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]) == 7)
                {
                    msg_tb = "";
                }

                if (report.Parameters["MSG_TB"] != null)
                {
                    report.Parameters["MSG_TB"].Value = msg_tb;
                }

                var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

                if (lblHoaDonMau != null)
                {
                    lblHoaDonMau.Visible = false;
                }

                if (inchuyendoi)
                {
                    var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                    if (tblInChuyenDoi != null)
                    {
                        tblInChuyenDoi.Visible = true;
                    }

                    if (report.Parameters["MSG_HD_TITLE"] != null)
                    {
                        report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                    }

                    if (report.Parameters["NGUOI_IN_CDOI"] != null)
                    {
                        report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                        report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                    }

                    if (report.Parameters["NGAY_IN_CDOI"] != null)
                    {
                        report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                        report.Parameters["NGAY_IN_CDOI"].Visible = true;
                    }
                }

                if (no_preview.Length > 0)
                {
                    if (report.Parameters["MSG_NO_PREVIEW"] != null)
                    {
                        report.Parameters["MSG_NO_PREVIEW"].Value = no_preview;
                    }
                }

                report.DataSource = ds;

                try
                {
                    bool giamthuebanhang20 = tblInv_InvoiceAuth.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblInv_InvoiceAuth.Rows[0]["giamthuebanhang20"].ToString());
                    string dvtte = tblInv_InvoiceAuth.Rows[0]["currency_code"]?.ToString();

                    CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                    string tongTienGiam = double.Parse(DoubleStrToString(tblInv_InvoiceAuth.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                    tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");

                    string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";
                    if (giamthuebanhang20)
                    {
                        var dt = ds.Tables["invoiceinformation"];
                        if (dt.Rows.Count > 0)
                        {
                            if (dt.Columns.Contains("amount_by_word"))
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    var docTien = (dt.Rows[i]["amount_by_word"]?.ToString()).Trim();
                                    docTien = String.IsNullOrEmpty(docTien) ? "Không đồng." : docTien;
                                    ds.Tables["invoiceinformation"].Rows[i]["amount_by_word"] = docTien + " " + Environment.NewLine + thueGTGTBanHang;
                                    report.DataSource = ds;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Log.Error(ex);
                }

                var t = Task.Run(() =>
                {
                    var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    newCulture.NumberFormat.NumberDecimalSeparator = thap_phan;
                    newCulture.NumberFormat.NumberGroupSeparator = hang_nghin;

                    System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;

                    report.CreateDocument();

                });

                t.Wait();

                //DataTable tblLicenseInfo = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.sl_licenseinfo WHERE branch_code=N'" + tblInv_InvoiceAuth.Rows[0]["branch_code"] + "' AND key_license IS NOT NULL AND license_xml_info IS NOT NULL");

                //if (tblLicenseInfo.Rows.Count == 0)
                //{
                //    Bitmap bmp = ReportUtil.DrawStringDemo(report);
                //    int pageCount = report.Pages.Count;

                //    for (int i = 0; i < pageCount; i++)
                //    {
                //        PageWatermark pmk = new PageWatermark();
                //        pmk.Image = bmp;
                //        report.Pages[i].AssignWatermark(pmk);
                //    }
                //}


                if (tblInv_InvoiceAuth.Columns.Contains("list_number"))
                {
                    if (tblInv_InvoiceAuth.Rows[0]["list_number"].ToString().Length > 0)
                    {
                        string fileName = folder + "\\BangKeDinhKem.repx";

                        XtraReport rpBangKe = null;

                        if (!File.Exists(fileName))
                        {
                            rpBangKe = new XtraReport();
                            rpBangKe.SaveLayout(fileName);
                        }
                        else
                        {
                            rpBangKe = XtraReport.FromFile(fileName, true);
                        }

                        rpBangKe.ScriptReferencesString = "AccountSignature.dll";
                        rpBangKe.Name = "rpBangKe";
                        rpBangKe.DisplayName = "BangKeDinhKem.repx";

                        rpBangKe.DataSource = report.DataSource;

                        rpBangKe.CreateDocument();
                        report.Pages.AddRange(rpBangKe.Pages);
                    }

                }

                if (tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString() == "7")
                {

                    Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                    int pageCount = report.Pages.Count;


                    for (int i = 0; i < pageCount; i++)
                    {
                        Page page = report.Pages[i];
                        PageWatermark pmk = new PageWatermark();
                        pmk.Image = bmp;
                        page.AssignWatermark(pmk);
                    }

                    string fileName = folder + "\\BienBanXoaBo.repx";
                    XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                    rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                    rpBienBan.Name = "rpBienBan";
                    rpBienBan.DisplayName = "BienBanXoaBo.repx";

                    rpBienBan.DataSource = report.DataSource;
                    rpBienBan.DataMember = report.DataMember;

                    rpBienBan.CreateDocument();

                    rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                    report.PrintingSystem.ContinuousPageNumbering = false;

                    report.Pages.AddRange(rpBienBan.Pages);

                    int idx = pageCount;
                    pageCount = report.Pages.Count;

                    for (int i = idx; i < pageCount; i++)
                    {
                        PageWatermark pmk = new PageWatermark();
                        pmk.ShowBehind = false;
                        report.Pages[i].AssignWatermark(pmk);
                    }

                }

                if (trang_thai_hd == 19 || trang_thai_hd == 21 || trang_thai_hd == 5)
                {

                    string rp_file = trang_thai_hd == 19 || trang_thai_hd == 21 ? "INCT_BBDC_GT.repx" : "INCT_BBDC_DD.repx";
                    string rp_code = trang_thai_hd == 19 || trang_thai_hd == 21 ? "#SCHEMA_NAME#.qr_inct_hd_dieuchinhgt" : "#SCHEMA_NAME#.qr_inct_hd_dieuchinhdg";

                    string fileName = folder + "\\" + rp_file;
                    XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                    rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                    rpBienBan.Name = "rpBienBanDC";
                    rpBienBan.DisplayName = rp_file;

                    parameters.Clear();
                    parameters.Add("p_branch_code", _webHelper.GetDvcs());
                    //parameters.Add("p_schemaname", "#SCHEMA_NAME#");
                    parameters.Add("p_document_id", Guid.Parse(id));

                    DataSet dataSource = await this._minvoiceDbContext.GetDataSetAsyncPrintf(rp_code, CommandType.StoredProcedure, parameters);

                    rpBienBan.DataSource = dataSource;
                    rpBienBan.DataMember = dataSource.Tables[0].TableName;

                    rpBienBan.CreateDocument();

                    rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                    report.PrintingSystem.ContinuousPageNumbering = false;

                    report.Pages.AddRange(rpBienBan.Pages);

                    Page page = report.Pages[report.Pages.Count - 1];
                    page.AssignWatermark(new PageWatermark());

                }

                if (trang_thai_hd == 3)
                {

                    string rp_file = "INCT_BBDTH.repx";
                    string rp_code = "#SCHEMA_NAME#.qr_inct_bbth";

                    string fileName = folder + "\\" + rp_file;
                    XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                    rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                    rpBienBan.Name = "rpBienBanThuHoi";
                    rpBienBan.DisplayName = rp_file;

                    parameters.Clear();
                    parameters.Add("p_branch_code", _webHelper.GetDvcs());
                    //parameters.Add("p_schemaname", "#SCHEMA_NAME#");
                    parameters.Add("p_document_id", Guid.Parse(id));

                    DataSet dataSource = await this._minvoiceDbContext.GetDataSetAsyncPrintf(rp_code, CommandType.StoredProcedure, parameters);

                    rpBienBan.DataSource = dataSource;
                    rpBienBan.DataMember = dataSource.Tables[0].TableName;

                    rpBienBan.CreateDocument();

                    rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                    report.PrintingSystem.ContinuousPageNumbering = false;

                    report.Pages.AddRange(rpBienBan.Pages);

                    Page page = report.Pages[report.Pages.Count - 1];
                    page.AssignWatermark(new PageWatermark());

                }

                if (trang_thai_hd == 13 || trang_thai_hd == 17)
                {
                    Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                    int pageCount = report.Pages.Count;

                    for (int i = 0; i < pageCount; i++)
                    {
                        PageWatermark pmk = new PageWatermark();
                        pmk.Image = bmp;
                        report.Pages[i].AssignWatermark(pmk);
                    }
                }

                MemoryStream ms = new MemoryStream();

                if (type == "Html")
                {
                    report.ExportOptions.Html.EmbedImagesInHTML = true;
                    report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                    report.ExportOptions.Html.Title = "Hóa đơn điện tử";
                    report.ExportToHtml(ms);

                    string html = Encoding.UTF8.GetString(ms.ToArray());

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(html);


                    string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                    string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                    var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            string eventMouseDown = node.Attributes["onmousedown"].Value;

                            if (eventMouseDown.Contains("showCert('seller')"))
                            {
                                node.SetAttributeValue("id", "certSeller");
                            }
                            if (eventMouseDown.Contains("showCert('buyer')"))
                            {
                                node.SetAttributeValue("id", "certBuyer");
                            }
                            if (eventMouseDown.Contains("showCert('vacom')"))
                            {
                                node.SetAttributeValue("id", "certVacom");
                            }
                            if (eventMouseDown.Contains("showCert('minvoice')"))
                            {
                                node.SetAttributeValue("id", "certMinvoice");
                            }
                        }
                    }

                    HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                    HtmlNode xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("id", "xmlData");
                    xmlNode.SetAttributeValue("type", "text/xmldata");

                    xmlNode.AppendChild(doc.CreateTextNode(xml));
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("type", "text/javascript");

                    xmlNode.InnerHtml = "$(function () { "
                                       + "  var url = 'http://localhost:19898/signalr'; "
                                       + "  var connection = $.hubConnection(url, {  "
                                       + "     useDefaultPath: false "
                                       + "  });"
                                       + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                       + " invoiceHubProxy.on('resCommand', function (result) { "
                                       + " }); "
                                       + " connection.start().done(function () { "
                                       + "      console.log('Connect to the server successful');"
                                       + "      $('#certSeller').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'seller' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certVacom').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'vacom' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certBuyer').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'buyer' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certMinvoice').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'minvoice' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "})"
                                       + ".fail(function () { "
                                       + "     alert('failed in connecting to the signalr server'); "
                                       + "});"
                                       + "});";

                    head.AppendChild(xmlNode);

                    if (report.Watermark != null)
                    {
                        if (report.Watermark.Image != null)
                        {
                            ImageConverter _imageConverter = new ImageConverter();
                            byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                            string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                            HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                            string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                            string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                            HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                            style.AppendChild(textNode);

                            HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                            HtmlNodeCollection pageNodes = body.SelectNodes("div");

                            foreach (var pageNode in pageNodes)
                            {
                                pageNode.Attributes.Add("class", "waterMark");

                                string divStyle = pageNode.Attributes["style"].Value;
                                divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                pageNode.Attributes["style"].Value = divStyle;
                            }
                        }
                    }

                    ms.SetLength(0);
                    doc.Save(ms);

                    doc = null;
                }
                else if (type == "Image")
                {
                    var options = new ImageExportOptions(ImageFormat.Png)
                    {
                        ExportMode = ImageExportMode.SingleFilePageByPage,
                    };
                    report.ExportToImage(ms, options);
                }
                else if (type == "Excel")
                {
                    report.ExportToXlsx(ms);
                }
                else if (type == "Rtf")
                {
                    report.ExportToRtf(ms);
                }
                else
                {
                    report.ExportToPdf(ms);
                }

                bytes = ms.ToArray();
                ms.Close();

                if (bytes == null)
                {
                    throw new Exception("null");

                }

            }
            catch (Exception ex)
            {
                _log.Error($"PrintInvoiceFromId : {ex}");
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> PrintReceiptFromInvoiceId(long id, string folder, string type, bool inchuyendoi)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("invoice_id", id);

            DataTable tblReceipt = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_receipt_id FROM #SCHEMA_NAME#.inv_receipt WHERE invoice_id=@invoice_id", CommandType.Text, parameters);

            if (tblReceipt.Rows.Count == 0)
            {
                throw new Exception("Không tìm thấy phiếu thu");
            }

            string inv_receipt_id = tblReceipt.Rows[0]["inv_receipt_id"].ToString();

            return await PrintReceiptFromId(inv_receipt_id, folder, type, inchuyendoi);
        }

        public async Task<byte[]> PrintReceiptFromId(string id, string folder, string type, bool inchuyendoi, bool inThuTu = false)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                Guid inv_InvoiceAuth_id = Guid.Parse(id);

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("inv_receipt_id", inv_InvoiceAuth_id);

                DataTable tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_receipt WHERE inv_receipt_id=@inv_receipt_id", CommandType.Text, parameters);
                DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_receiptxmldata WHERE inv_receipt_id=@inv_receipt_id", CommandType.Text, parameters);

                if (tblInvoiceXmlData.Rows.Count > 0)
                {
                    xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                }
                else
                {
                    parameters.Clear();
                    parameters.Add("id", Guid.Parse(id));
                    DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlreceipt('#SCHEMA_NAME#',@id)", CommandType.Text, parameters);
                    xml = tblData.Rows[0][0].ToString();
                }
                if (inThuTu)
                {
                    parameters.Clear();
                    parameters.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);
                    //parameters.Add("id", inv_InvoiceAuth_id);
                    DataTable dtInvocieDetails = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlinvoice_detail_vn('#SCHEMA_NAME#',@inv_invoiceauth_id)", CommandType.Text, parameters);
                    var xmlDeltal = dtInvocieDetails.Rows[0][0].ToString();
                    XDocument doc = XDocument.Parse(xml);
                    XElement docDetail = XElement.Parse(xmlDeltal);
                    List<XElement> lstNode = doc.Descendants("ChiTietHoaDon").ToList();
                    for (int i = 0; i < lstNode.Count; i++)
                    {
                        lstNode[i].ReplaceWith(docDetail);
                    }
                    xml = doc.ToString();
                }
                string fileReceipt = folder + "\\Receipt.repx";
                XtraReport report = XtraReport.FromFile(fileReceipt, true);


                report.Name = "XtraReport1";
                report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                {
                    ds.ReadXmlSchema(xmlReader);
                    xmlReader.Close();
                }

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                {
                    ds.ReadXml(xmlReader);
                    xmlReader.Close();
                }

                if (ds.Tables.Contains("TblXmlData"))
                {
                    ds.Tables.Remove("TblXmlData");
                }


                DataTable tblXmlData = new DataTable();
                tblXmlData.TableName = "TblXmlData";
                tblXmlData.Columns.Add("data");

                DataRow r = tblXmlData.NewRow();
                r["data"] = xml;
                tblXmlData.Rows.Add(r);
                ds.Tables.Add(tblXmlData);

                string datamember = report.DataMember;

                if (datamember.Length > 0)
                {
                    if (ds.Tables.Contains(datamember))
                    {
                        DataTable tblChiTiet = ds.Tables[datamember];

                        int rowcount = ds.Tables[datamember].Rows.Count;

                    }
                }

                if (inchuyendoi)
                {
                    var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                    if (tblInChuyenDoi != null)
                    {
                        tblInChuyenDoi.Visible = true;
                    }

                    if (report.Parameters["MSG_HD_TITLE"] != null)
                    {
                        report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                    }

                    if (report.Parameters["NGUOI_IN_CDOI"] != null)
                    {
                        report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                        report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                    }

                    if (report.Parameters["NGAY_IN_CDOI"] != null)
                    {
                        report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                        report.Parameters["NGAY_IN_CDOI"].Visible = true;
                    }
                }

                report.DataSource = ds;
                report.CreateDocument();

                if (tblInv_InvoiceAuth.Rows[0]["status"].ToString() == "Xóa bỏ")
                {

                    Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                    int pageCount = report.Pages.Count;


                    for (int i = 0; i < pageCount; i++)
                    {
                        Page page = report.Pages[i];
                        PageWatermark pmk = new PageWatermark();
                        pmk.Image = bmp;
                        page.AssignWatermark(pmk);
                    }

                }

                MemoryStream ms = new MemoryStream();

                if (type == "Html")
                {
                    report.ExportOptions.Html.EmbedImagesInHTML = true;
                    report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                    report.ExportOptions.Html.Title = "Hóa đơn điện tử";
                    report.ExportToHtml(ms);

                    string html = Encoding.UTF8.GetString(ms.ToArray());

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(html);


                    string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                    string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                    var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            string eventMouseDown = node.Attributes["onmousedown"].Value;

                            if (eventMouseDown.Contains("showCert('seller')"))
                            {
                                node.SetAttributeValue("id", "certSeller");
                            }
                            if (eventMouseDown.Contains("showCert('buyer')"))
                            {
                                node.SetAttributeValue("id", "certBuyer");
                            }
                            if (eventMouseDown.Contains("showCert('vacom')"))
                            {
                                node.SetAttributeValue("id", "certVacom");
                            }
                            if (eventMouseDown.Contains("showCert('minvoice')"))
                            {
                                node.SetAttributeValue("id", "certMinvoice");
                            }
                        }
                    }

                    HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                    HtmlNode xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("id", "xmlData");
                    xmlNode.SetAttributeValue("type", "text/xmldata");

                    xmlNode.AppendChild(doc.CreateTextNode(xml));
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("type", "text/javascript");

                    xmlNode.InnerHtml = "$(function () { "
                                       + "  var url = 'http://localhost:19898/signalr'; "
                                       + "  var connection = $.hubConnection(url, {  "
                                       + "     useDefaultPath: false "
                                       + "  });"
                                       + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                       + " invoiceHubProxy.on('resCommand', function (result) { "
                                       + " }); "
                                       + " connection.start().done(function () { "
                                       + "      console.log('Connect to the server successful');"
                                       + "      $('#certSeller').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'seller' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certVacom').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'vacom' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certBuyer').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'buyer' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certMinvoice').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'minvoice' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "})"
                                       + ".fail(function () { "
                                       + "     alert('failed in connecting to the signalr server'); "
                                       + "});"
                                       + "});";

                    head.AppendChild(xmlNode);

                    if (report.Watermark != null)
                    {
                        if (report.Watermark.Image != null)
                        {
                            ImageConverter _imageConverter = new ImageConverter();
                            byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                            string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                            HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                            string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                            string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                            HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                            style.AppendChild(textNode);

                            HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                            HtmlNodeCollection pageNodes = body.SelectNodes("div");

                            foreach (var pageNode in pageNodes)
                            {
                                pageNode.Attributes.Add("class", "waterMark");

                                string divStyle = pageNode.Attributes["style"].Value;
                                divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                pageNode.Attributes["style"].Value = divStyle;
                            }
                        }
                    }

                    ms.SetLength(0);
                    doc.Save(ms);

                    doc = null;
                }
                else if (type == "Image")
                {
                    var options = new ImageExportOptions(ImageFormat.Png)
                    {
                        ExportMode = ImageExportMode.SingleFilePageByPage,
                    };
                    report.ExportToImage(ms, options);
                }
                else if (type == "Excel")
                {
                    report.ExportToXlsx(ms);
                }
                else if (type == "Rtf")
                {
                    report.ExportToRtf(ms);
                }
                else
                {
                    report.ExportToPdf(ms);
                }

                bytes = ms.ToArray();
                ms.Close();

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                _log.Error($"PrintReceiptFromId : {ex}");
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> InChuyenDoi(string id)
        {
            byte[] result = null;

            try
            {

                string sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET print_convert=true,person_convert=@person_convert,date_convert=@date_convert WHERE inv_invoiceauth_id=@inv_invoiceauth_id";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("person_convert", _webHelper.GetUser());
                parameters.Add("date_convert", DateTime.Now);
                parameters.Add("inv_invoiceauth_id", Guid.Parse(id));

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                string originalString = _webHelper.GetRequest().Url.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                result = await PrintInvoiceFromId(id, folder, "PDF", true, "");

            }
            catch (Exception ex)
            {
                _log.Error($"InChuyenDoi : {ex}");
                throw new Exception(ex.Message);
            }

            return result;
        }

        public async Task<JObject> GetInvoiceById(Guid id)
        {
            JObject obj = new JObject();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("inv_invoiceauth_id", id);

            DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);

            if (tblInv_InvoiceAuth.Rows.Count == 0)
            {
                obj.Add("error", "Không tìm thấy hóa đơn");
                return obj;
            }

            DataTable tblInv_InvoiceAuthDetail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauthdetail WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);

            JArray jarray = JArray.FromObject(tblInv_InvoiceAuth);

            obj = (JObject)jarray[0];

            jarray = JArray.FromObject(tblInv_InvoiceAuthDetail);
            obj.Add("inv_InvoiceAuthDetails", jarray);


            return obj;

        }

        public async Task<JObject> GetByInvoiceId(long invoice_id)
        {
            JObject obj = new JObject();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("invoice_id", invoice_id);

            DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id", CommandType.Text, parameters);

            if (tblInv_InvoiceAuth.Rows.Count == 0)
            {
                obj.Add("error", "Không tìm thấy hóa đơn");
                return obj;
            }

            string inv_invoiceauth_id = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

            parameters.Clear();
            parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
            DataTable tblInv_InvoiceAuthDetail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauthdetail WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);

            JArray jarray = JArray.FromObject(tblInv_InvoiceAuth);

            obj = (JObject)jarray[0];

            jarray = JArray.FromObject(tblInv_InvoiceAuthDetail);
            obj.Add("inv_invoiceauthdetails", jarray);


            return obj;

        }

        public async Task<JObject> GetInvoiceXoaBo(JObject parameters)
        {
            Guid id = Guid.Parse(parameters["id"].ToString());

            Guid inv_InvoiceAuth_id = Guid.NewGuid();


            Dictionary<string, object> dicPara = new Dictionary<string, object>();
            dicPara.Add("id", id);


            DataTable tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_InvoiceAuth WHERE inv_InvoiceAuth_id=@id", CommandType.Text, dicPara);
            DataTable tblInv_InvoiceAuthDetail = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_InvoiceAuthDetail WHERE inv_InvoiceAuth_id=@id", CommandType.Text, dicPara);

            string inv_invoiceNumber = tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString();
            string inv_originalInvoiceId = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

            DateTime ngayvb = Convert.ToDateTime(parameters["ngayvb"].ToString());
            DateTime inv_invoiceIssuedDate = Convert.ToDateTime(tblInv_InvoiceAuth.Rows[0]["invoice_issued_date"]);

            if (ngayvb < inv_invoiceIssuedDate)
            {
                JObject obj = new JObject();
                obj.Add("error", "Ngày văn bản không được nhỏ hơn ngày hóa đơn");
                return obj;
            }

            foreach (DataRow row in tblInv_InvoiceAuth.Rows)
            {
                row.BeginEdit();
                row["inv_invoiceauth_id"] = inv_InvoiceAuth_id;
                row["additional_reference_des"] = parameters["sovb"].ToString();
                row["additional_reference_date"] = ngayvb;
                row["additional_reference_note"] = parameters["ghi_chu"].ToString();
                row["adjustment_type"] = 7;
                row["status"] = "Đã ký";
                //row["so_hd_dc"] = inv_invoiceNumber;
                //row["inv_originalInvoiceId"] = inv_originalInvoiceId;
                row["original_invoice_id"] = inv_originalInvoiceId;
                //row["inv_additionalReferenceDes"] = parameters["sovb"].ToString();
                //row["inv_additionalReferenceDate"] = ngayvb;
                row["invoice_status"] = 7;
                row["signer"] = DBNull.Value;
                row["print_convert"] = false;
                row["date_convert"] = DBNull.Value;
                row["person_convert"] = DBNull.Value;
                row["invoice_number1"] = row["invoice_number"].ToString() + "7";
                row.EndEdit();
            }

            foreach (DataRow row in tblInv_InvoiceAuthDetail.Rows)
            {
                row.BeginEdit();
                row["inv_invoiceauth_id"] = inv_InvoiceAuth_id;
                row["inv_invoiceauthdetail_id"] = Guid.NewGuid();
                row.EndEdit();
            }

            dicPara.Clear();
            dicPara.Add("idString", Guid.Parse(id.ToString()));

            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@idString)", CommandType.Text, dicPara);
            string exportXml = tblData.Rows[0][0].ToString();

            // QuyenNH modified
            exportXml = await ExportInvoiceXml(exportXml, inv_InvoiceAuth_id);

            exportXml = FunctionUtil.ReverseXml(exportXml);
            // string exportXml = this._nopDbContext.ExecuteCmd("EXECUTE sproc_export_XmlInvoice '" + id + "'").Rows[0][0].ToString();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(exportXml);

            doc.GetElementsByTagName("sellerapprecord_id")[0].InnerText = inv_InvoiceAuth_id.ToString();
            doc.GetElementsByTagName("invoice_status")[0].InnerText = "7";
            doc.GetElementsByTagName("invoice_number")[0].InnerText = inv_invoiceNumber;

            if (doc.GetElementsByTagName("additional_reference_des").Count == 0)
            {
                XmlElement _el = doc.CreateElement("additional_reference_des");
                _el.InnerText = parameters["sovb"].ToString();
                doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(_el);
            }
            else
            {
                doc.GetElementsByTagName("additional_reference_des")[0].InnerText = parameters["sovb"].ToString();
            }

            if (doc.GetElementsByTagName("additional_reference_note").Count == 0)
            {
                XmlElement _el = doc.CreateElement("additional_reference_note");
                _el.InnerText = parameters["ghi_chu"].ToString();
                doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(_el);
            }
            else
            {
                doc.GetElementsByTagName("additional_reference_note")[0].InnerText = parameters["ghi_chu"].ToString();
            }

            if (doc.GetElementsByTagName("additional_reference_date").Count == 0)
            {
                XmlElement _el = doc.CreateElement("additional_reference_date");
                _el.InnerText = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", ngayvb);
                doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(_el);
            }
            else
            {
                doc.GetElementsByTagName("additional_reference_date")[0].InnerText = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", ngayvb);
            }

            XmlElement el = doc.CreateElement("NoiDungThoaThuan");
            el.InnerText = parameters["ghi_chu"].ToString();
            doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(el);

            el = doc.CreateElement("ThongTinThoaThuan");
            el.InnerText = parameters["sovb"].ToString();
            doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(el);

            el = doc.CreateElement("NgayThoaThuan");
            el.InnerText = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", ngayvb);
            doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(el);


            JArray jarray = JArray.FromObject(tblInv_InvoiceAuth);

            JObject json = (JObject)jarray[0];
            json.Add("inv_InvoiceAuthDetails", JArray.FromObject(tblInv_InvoiceAuthDetail));
            json.Add("InvoiceXmlData", doc.OuterXml);

            if (json["inv_invoiceauth_id"] == null)
            {
                json.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);
            }

            return json;

        }

        public async Task<JObject> GetInvoiceXoaBoPretreatment(JObject parameters)
        {
            try
            {
                if (parameters["inv_invoiceauth_id"] == null)
                {
                    JObject obj = new JObject();
                    obj.Add("error", "Id hóa đơn không được để trống!");
                    return obj;
                }

                if (parameters["sovb"] == null)
                {
                    JObject obj = new JObject();
                    obj.Add("error", "Số văn bản không được để trống");
                    return obj;
                }

                if (parameters["ngayvb"] == null)
                {
                    JObject obj = new JObject();
                    obj.Add("error", "Ngày văn bản không được để trống");
                    return obj;
                }

                Guid id = Guid.Parse(parameters["inv_invoiceauth_id"].ToString());

                Guid inv_InvoiceAuth_id = Guid.NewGuid();

                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("id", id);

                DataTable tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_InvoiceAuth WHERE inv_InvoiceAuth_id=@id", CommandType.Text, dicPara);
                DataTable tblInv_InvoiceAuthDetail = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_InvoiceAuthDetail WHERE inv_InvoiceAuth_id=@id", CommandType.Text, dicPara);

                if (tblInv_InvoiceAuth.Rows.Count == 0)
                {
                    JObject obj = new JObject();
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                string inv_invoiceNumber = tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString();
                string inv_originalInvoiceId = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

                DateTime ngayvb = Convert.ToDateTime(parameters["ngayvb"].ToString());
                DateTime inv_invoiceIssuedDate = Convert.ToDateTime(tblInv_InvoiceAuth.Rows[0]["invoice_issued_date"]);

                if (ngayvb < inv_invoiceIssuedDate)
                {
                    JObject obj = new JObject();
                    obj.Add("error", "Ngày văn bản không được nhỏ hơn ngày hóa đơn");
                    return obj;
                }

                foreach (DataRow row in tblInv_InvoiceAuth.Rows)
                {
                    row.BeginEdit();
                    row["inv_invoiceauth_id"] = inv_InvoiceAuth_id;
                    row["additional_reference_des"] = parameters["sovb"].ToString();
                    row["additional_reference_date"] = ngayvb;
                    row["additional_reference_note"] = parameters["ghi_chu"]?.ToString();
                    row["adjustment_type"] = 7;
                    row["status"] = "Đã ký";
                    //row["so_hd_dc"] = inv_invoiceNumber;
                    //row["inv_originalInvoiceId"] = inv_originalInvoiceId;
                    row["original_invoice_id"] = inv_originalInvoiceId;
                    //row["inv_additionalReferenceDes"] = parameters["sovb"].ToString();
                    //row["inv_additionalReferenceDate"] = ngayvb;
                    row["invoice_status"] = 7;
                    row["signer"] = DBNull.Value;
                    row["print_convert"] = false;
                    row["date_convert"] = DBNull.Value;
                    row["person_convert"] = DBNull.Value;
                    row["invoice_number1"] = row["invoice_number"].ToString() + "7";
                    row.EndEdit();
                }

                foreach (DataRow row in tblInv_InvoiceAuthDetail.Rows)
                {
                    row.BeginEdit();
                    row["inv_invoiceauth_id"] = inv_InvoiceAuth_id;
                    row["inv_invoiceauthdetail_id"] = Guid.NewGuid();
                    row.EndEdit();
                }

                dicPara.Clear();
                dicPara.Add("idString", Guid.Parse(id.ToString()));

                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@idString)", CommandType.Text, dicPara);
                string exportXml = tblData.Rows[0][0].ToString();

                // QuyenNH modified
                exportXml = await ExportInvoiceXml(exportXml, inv_InvoiceAuth_id);

                exportXml = FunctionUtil.ReverseXml(exportXml);
                // string exportXml = this._nopDbContext.ExecuteCmd("EXECUTE sproc_export_XmlInvoice '" + id + "'").Rows[0][0].ToString();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(exportXml);

                doc.GetElementsByTagName("sellerapprecord_id")[0].InnerText = inv_InvoiceAuth_id.ToString();
                doc.GetElementsByTagName("invoice_status")[0].InnerText = "7";
                doc.GetElementsByTagName("invoice_number")[0].InnerText = inv_invoiceNumber;

                if (doc.GetElementsByTagName("additional_reference_des").Count == 0)
                {
                    XmlElement _el = doc.CreateElement("additional_reference_des");
                    _el.InnerText = parameters["sovb"].ToString();
                    doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(_el);
                }
                else
                {
                    doc.GetElementsByTagName("additional_reference_des")[0].InnerText = parameters["sovb"].ToString();
                }

                if (doc.GetElementsByTagName("additional_reference_note").Count == 0)
                {
                    XmlElement _el = doc.CreateElement("additional_reference_note");
                    _el.InnerText = parameters["ghi_chu"].ToString();
                    doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(_el);
                }
                else
                {
                    doc.GetElementsByTagName("additional_reference_note")[0].InnerText = parameters["ghi_chu"].ToString();
                }

                if (doc.GetElementsByTagName("additional_reference_date").Count == 0)
                {
                    XmlElement _el = doc.CreateElement("additional_reference_date");
                    _el.InnerText = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", ngayvb);
                    doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(_el);
                }
                else
                {
                    doc.GetElementsByTagName("additional_reference_date")[0].InnerText = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", ngayvb);
                }

                XmlElement el = doc.CreateElement("NoiDungThoaThuan");
                el.InnerText = parameters["ghi_chu"].ToString();
                doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(el);

                el = doc.CreateElement("ThongTinThoaThuan");
                el.InnerText = parameters["sovb"].ToString();
                doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(el);

                el = doc.CreateElement("NgayThoaThuan");
                el.InnerText = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", ngayvb);
                doc.GetElementsByTagName("invoiceinformation")[0].AppendChild(el);


                JArray jarray = JArray.FromObject(tblInv_InvoiceAuth);

                JObject json = new JObject();

                json.Add("data", Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((JObject)jarray[0]))));
                json.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);
                json.Add("original_invoice_id", inv_originalInvoiceId);
                json.Add("inv_InvoiceAuthDetails", JArray.FromObject(tblInv_InvoiceAuthDetail));
                json.Add("InvoiceXmlData", Convert.ToBase64String(Encoding.UTF8.GetBytes(doc.OuterXml)));

                if (json["inv_invoiceauth_id"] == null)
                {
                    json.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);
                }
                return json;
            }
            catch (Exception ex)
            {
                JObject json = new JObject();
                json.Add("error", ex.Message);
                return json;
            }

        }

        public async Task<JObject> SaveInvoiceXoaBo(JObject masterObj)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                //var invoiceDb = this._nopDbContext.GetInvoiceDb();

                if (masterObj["adjustment_type"] != null)
                {
                    masterObj["adjustment_type"] = 7;
                }
                else
                {
                    masterObj.Add("adjustment_type", 7);
                }

                string serialNumber = masterObj["serialNumber"].ToString();
                string nguoiky = "";

                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("cer_serial", serialNumber);

                DataTable tblChuKySo = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE cer_serial=@cer_serial", CommandType.Text, dicPara);

                if (tblChuKySo.Rows.Count > 0)
                {
                    nguoiky = tblChuKySo.Rows[0]["signer"].ToString();
                }

                string sql = "#SCHEMA_NAME#.crd_inv_invoiceauth_insert";
                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                JArray details = null;
                if (masterObj["inv_InvoiceAuthDetails"] != null)
                {
                    if (masterObj.SelectToken("inv_InvoiceAuthDetails") is JArray)
                    {
                        details = (JArray)masterObj["inv_InvoiceAuthDetails"];
                    }
                }

                if (details != null)
                {
                    foreach (var dtl in details)
                    {
                        await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_inv_invoiceauthdetail_insert", CommandType.StoredProcedure, (JObject)dtl);
                    }
                }

                string xml = masterObj["InvoiceXmlData"].ToString();
                xml = FunctionUtil.ConvertXml(xml);
                string inv_invoiceauth_id = masterObj["inv_invoiceauth_id"].ToString();

                string sobaomat = await GenerateSecurity(6, 1);
                xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                dicPara.Clear();
                dicPara.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                dicPara.Add("xml", xml);
                dicPara.Add("nguoiky", nguoiky);
                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //json.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return json;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                string sqlsave = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id,@xml::xml,@nguoiky)";
                await _minvoiceDbContext.TransactionCommandAsync(sqlsave, CommandType.Text, dicPara);

                //string sqlupdate = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET status='Xóa bỏ',invoice_status=13,integrated_number='" + masterObj["invoice_number"] + "',original_invoice_id='" + masterObj["inv_invoiceauth_id"] + "' WHERE inv_invoiceauth_id='" + masterObj["original_invoice_id"] + "'";
                // await _minvoiceDbContext.TransactionCommandAsync(sqlupdate,CommandType.Text,(JObject)null);

                // save xml to postgresql 
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();


                json.Add("ok", true);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                isError = true;
            }
            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;

        }

        public async Task<JObject> SaveInvoiceXoaBoPretreatment(JObject masterObj)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                string nguoiky = _webHelper.GetUser();

                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("username", nguoiky);
                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicPara);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    json.Add("error", "Bạn không có quyền ký hóa đơn");
                    return json;
                }

                dicPara.Clear();
                dicPara.Add("inv_invoiceauth_id", Guid.Parse(masterObj["original_invoice_id"].ToString()));

                DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT status, invoice_status, invoice_issued_date,inv_invoiceauth_id,invoice_number,branch_code,buyer_email,customer_code,template_code,invoice_series FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicPara);

                if (tblInv_InvoiceAuth.Rows.Count == 0)
                {
                    json.Add("error", "Không tồn tại hóa đơn");
                    return json;
                }
                else
                {
                    if (tblInv_InvoiceAuth.Rows[0]["status"].ToString() != "Đã ký")
                    {
                        json.Add("error", "Hóa đơn gốc chưa được ký không thể xóa bỏ!");
                        return json;
                    }

                    if (tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString() == "7")
                    {
                        json.Add("error", "Không được xóa bỏ hóa đơn xóa bỏ!");
                        return json;
                    }

                    if (tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString() == "11")
                    {
                        json.Add("error", "Không xóa bỏ được hóa đơn Bị điều chỉnh!");
                        return json;
                    }
                }

                string branch_code = tblInv_InvoiceAuth.Rows[0]["branch_code"].ToString();
                string buyer_email = tblInv_InvoiceAuth.Rows[0]["buyer_email"].ToString();

                dicPara.Clear();
                dicPara.Add("branch_code", branch_code);

                DataTable tblBranch = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@branch_code ORDER BY code", CommandType.Text, dicPara);

                string subjectName = String.Empty;

                if (tblBranch.Rows.Count == 0)
                {
                    json.Add("error", "Không tồn tại mã đơn vị");
                    return json;
                }
                else
                {
                    subjectName = tblBranch.Rows[0]["tax_code"].ToString();
                }

                string xml = masterObj["InvoiceXmlData"].ToString();

                xml = Encoding.UTF8.GetString(Convert.FromBase64String(xml));

                //Validate chữ ký
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(xml);

                XmlNodeList nodeListNguoiBan = xmlDoc.GetElementsByTagName("MaSoThueNguoiBan");

                if (nodeListNguoiBan.Count > 0)
                {
                    XmlNode nodeNguoiBan = nodeListNguoiBan[0];
                    if (!subjectName.Contains(nodeNguoiBan.InnerText))
                    {
                        json.Add("error", "Mã số thuế người bán với chứng thư số không khớp nhau");
                        return json;
                    };
                }

                XmlNodeList nodeListSignature = xmlDoc.GetElementsByTagName("Signature");

                // Kiểm tra tính hợp lệ của chữ ký.
                if (nodeListSignature.Count <= 0)
                {
                    json.Add("error", "Không tìm thấy chữ ký trong dữ liệu");
                    return json;
                }
                else
                {
                    foreach (XmlElement element in nodeListSignature)
                    {
                        if (element.Attributes["Id"].Value == "seller")
                        {
                            string subjectname = element.GetElementsByTagName("X509SubjectName")[0].InnerText;

                            Dictionary<string, object> par = new Dictionary<string, object>();
                            par.Add("branch_code", branch_code);
                            par.Add("subject_name", subjectname);

                            DataTable checkCKS = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code and subject_name=@subject_name and current_timestamp <= end_date", CommandType.Text, par);

                            if (checkCKS.Rows.Count == 0)
                            {
                                json.Add("error", "Chữ ký số không tồn tại trên hệ thống");
                                return json;
                            }
                        }

                        try
                        {
                            // Create a new SignedXml object and pass it
                            // the XML document class.
                            SignedXml signedXml = new SignedXml(xmlDoc);

                            // Load the signature node.
                            signedXml.LoadXml(element);

                            // Check the signature and return the result.
                            if (!signedXml.CheckSignature())
                            {
                                json.Add("error", "Chữ ký số đã bị thay đổi");
                                return json;
                            }
                        }
                        catch (Exception ex)
                        {
                            json.Add("error", "Chữ ký số không hợp lệ");
                            return json;
                        }

                    }
                }

                string data = Encoding.UTF8.GetString(Convert.FromBase64String(masterObj["data"].ToString()));

                JObject dataInvoice = JsonConvert.DeserializeObject<JObject>(data);

                string sql = "#SCHEMA_NAME#.crd_inv_invoiceauth_insert";
                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, dataInvoice);

                JArray details = null;
                if (masterObj["inv_InvoiceAuthDetails"] != null)
                {
                    if (masterObj.SelectToken("inv_InvoiceAuthDetails") is JArray)
                    {
                        details = (JArray)masterObj["inv_InvoiceAuthDetails"];
                    }
                }

                if (details != null)
                {
                    foreach (var dtl in details)
                    {
                        await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_inv_invoiceauthdetail_insert", CommandType.StoredProcedure, (JObject)dtl);
                    }
                }

                xml = FunctionUtil.ConvertXml(xml);
                string inv_invoiceauth_id = masterObj["inv_invoiceauth_id"].ToString();

                string sobaomat = await GenerateSecurity(6, 1);
                xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                dicPara.Clear();
                dicPara.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                dicPara.Add("xml", xml);
                dicPara.Add("nguoiky", nguoiky);
                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //json.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return json;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                string sqlsave = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id,@xml::xml,@nguoiky)";
                await _minvoiceDbContext.TransactionCommandAsync(sqlsave, CommandType.Text, dicPara);

                //string sqlupdate = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET status='Xóa bỏ',invoice_status=13,integrated_number='" + masterObj["invoice_number"] + "',original_invoice_id='" + masterObj["inv_invoiceauth_id"] + "' WHERE inv_invoiceauth_id='" + masterObj["original_invoice_id"] + "'";
                // await _minvoiceDbContext.TransactionCommandAsync(sqlupdate,CommandType.Text,(JObject)null);

                // save xml to postgresql 
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                json.Add("ok", true);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                isError = true;
            }
            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;

        }

        public async Task<JArray> GetListCertificates(string userName, string ma_dvcs)
        {
            Dictionary<string, object> dicPara = new Dictionary<string, object>();
            dicPara.Add("username", userName);
            dicPara.Add("branch_code", ma_dvcs);

            string sql = "SELECT a.pl_certificate_id,a.issuer,a.signer,a.subject_name,a.begin_date,a.end_date, (CASE WHEN a.cert_type = 'SIM' THEN 'FILE' ELSE a.cert_type END) as cert_type,d.pass,a.cer_serial as id,a.cer_serial,a.cer_serial as so_serial FROM #SCHEMA_NAME#.pl_certificate a "
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id "
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id "
                        + "LEFT JOIN #SCHEMA_NAME#.wb_token d ON a.branch_code=d.branch_code AND a.token_serial=d.serial_number "
                        + "WHERE c.username=@username AND a.branch_code=@branch_code";
            DataTable tblCerts = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);

            JArray data = JArray.FromObject(tblCerts);

            return data;
        }


        public async Task<JObject> GetInvoiceByInvoiceNumber(string invoiceCode, string invoiceSeries, string invoiceNumber)
        {
            Dictionary<string, object> dicPara = new Dictionary<string, object>();
            dicPara.Add("template_code", invoiceCode);
            dicPara.Add("invoice_series", invoiceSeries);
            dicPara.Add("invoice_number", invoiceNumber);

            JObject obj = new JObject();

            DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE template_code=@template_code AND invoice_series=@invoice_series AND invoice_number=@invoice_number", CommandType.Text, dicPara);

            if (tblInv_InvoiceAuth.Rows.Count == 0)
            {
                obj.Add("error", "Không tìm thấy hóa đơn");
                return obj;
            }

            string id = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

            dicPara.Clear();
            dicPara.Add("inv_invoiceauth_id", Guid.Parse(id));

            DataTable tblInv_InvoiceAuthDetail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM inv_invoiceauthdetail WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicPara);

            JArray jarray = JArray.FromObject(tblInv_InvoiceAuth);

            obj = (JObject)jarray[0];

            jarray = JArray.FromObject(tblInv_InvoiceAuthDetail);
            obj.Add("inv_InvoiceAuthDetails", jarray);


            return obj;
        }

        public async Task<JObject> ExportInvoiceXml(Guid id)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("inv_invoiceauth_id", id);

                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicPara);

                if (tblInvoices.Rows.Count == 0)
                {
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                dicPara.Clear();
                dicPara.Add("id", id);

                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@id)", CommandType.Text, dicPara);
                string exportXml = tblData.Rows[0][0].ToString();
                obj.Add("inv_InvoiceAuth_id", id);
                obj.Add("InvoiceXmlData", exportXml);
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> ExportInvoiceXmlPretreatment(Guid id)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("inv_invoiceauth_id", id);

                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicPara);

                if (tblInvoices.Rows.Count == 0)
                {
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                dicPara.Clear();
                dicPara.Add("id", id);

                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@id)", CommandType.Text, dicPara);
                string exportXml = tblData.Rows[0][0].ToString();
                obj.Add("inv_InvoiceAuth_id", id);
                obj.Add("InvoiceXmlData", Convert.ToBase64String(Encoding.UTF8.GetBytes(exportXml)));
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> SaveXml(JObject parameters, string username)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("username", username);
                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicPara);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    obj.Add("error", "Bạn không có quyền ký hóa đơn");
                    return obj;
                }

                dicPara.Clear();
                dicPara.Add("inv_invoiceauth_id", Guid.Parse(parameters["inv_InvoiceAuth_id"].ToString()));

                DataTable check_Invoice = await _minvoiceDbContext.GetDataTableAsync("SELECT status FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicPara);

                if (check_Invoice.Rows.Count == 0)
                {
                    obj.Add("error", "Không tồn tại hóa đơn");
                    return obj;
                }
                else
                {
                    if (check_Invoice.Rows[0]["status"].ToString() != "Chờ ký")
                    {
                        obj.Add("error", "Hóa đơn không ở trạng thái chờ ký");
                        return obj;
                    }
                }

                string xml = parameters["InvoiceXmlData"].ToString();
                xml = FunctionUtil.ConvertXml(xml);
                //xml = xml.Replace("'", "''");
                //xml = CommonManager.SignByMInvoice(xml, "Signatures");

                string sobaomat = await GenerateSecurity(6, 1);
                xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                dicPara.Clear();
                dicPara.Add("inv_InvoiceAuth_id", Guid.Parse(parameters["inv_InvoiceAuth_id"].ToString()));
                dicPara.Add("xml", xml);
                dicPara.Add("fullname", tblUser.Rows[0]["fullname"].ToString());
                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //obj.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return obj;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                string sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_InvoiceAuth_id,@xml::xml,@fullname)";

                //Dictionary<string, object> dic = new Dictionary<string, object>();
                //dic.Add("p_xml_data", xml);
                //dic.Add("p_signer", tblUser.Rows[0]["fullname"].ToString());
                //dic.Add("p_inv_invoiceauth_id", parameters["inv_InvoiceAuth_id"].ToString());

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dicPara);

                dicPara.Clear();
                dicPara.Add("inv_invoiceauth_id", Guid.Parse(parameters["inv_InvoiceAuth_id"].ToString()));

                DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT invoice_issued_date,inv_invoiceauth_id,invoice_number,branch_code,buyer_email,customer_code,template_code,invoice_series FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicPara);

                string branch_code = tblInv_InvoiceAuth.Rows[0]["branch_code"].ToString();
                string buyer_email = tblInv_InvoiceAuth.Rows[0]["buyer_email"].ToString();
                string inv_invoiceauth_id = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

                dicPara.Clear();
                dicPara.Add("branch_code", branch_code);

                sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);

                if (tblSetting.Rows.Count > 0)
                {
                    string value = tblSetting.Rows[0]["value"].ToString();

                    if (value == "C")
                    {
                        if (buyer_email.Length > 0)
                        {
                            JObject item = new JObject();

                            obj.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            obj.Add("nguoinhan", buyer_email);
                            obj.Add("tieude", "Thông báo xuất hóa đơn điện tử");


                            string originalString = _webHelper.GetRequest().Url.OriginalString;
                            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                            await this.AutoSendInvoiceByEmail(folder, obj);

                            //obj.Add("API_INV_URL", _webHelper.GetUrlApi());
                            //_rabbitmqService.PublishQueue("email_inv_customer_queue", obj.ToString());

                        }


                    }
                }

                //sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_SMS') AND (branch_code='" + branch_code + "') ORDER BY code";
                //tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql);

                //if (tblSetting.Rows.Count > 0)
                //{
                //    string value = tblSetting.Rows[0]["value"].ToString();

                //    if (value == "C")
                //    {
                //        string customer_code = tblInv_InvoiceAuth.Rows[0]["customer_code"].ToString();

                //        if (customer_code.Length > 0)
                //        {
                //            DataTable tblCustomer = await _minvoiceDbContext.GetDataTableAsync("SELECT mobile FROM #SCHEMA_NAME#.pl_customer WHERE branch_code='" + branch_code + "' AND code='" + customer_code + "'");

                //            if (tblCustomer.Rows.Count > 0)
                //            {
                //                string mobile = tblCustomer.Rows[0]["mobile"].ToString();

                //                if (mobile.Length > 0)
                //                {
                //                    string id = Guid.NewGuid().ToString();
                //                    string template_code = tblInv_InvoiceAuth.Rows[0]["template_code"].ToString();
                //                    string invoice_series = tblInv_InvoiceAuth.Rows[0]["invoice_series"].ToString();
                //                    string invoice_number = tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString();

                //                    sql = "INSERT INTO #SCHEMA_NAME#.wb_sms_queue(wb_sms_queue_id,mobile,subject,body,date_new,inv_invoiceauth_id,template_code,invoice_number,invoice_series,is_send) \n"
                //                        + "VALUES ('" + id + "','" + mobile + "','test','test',current_timestamp,'" + inv_invoiceauth_id + "','" + template_code + "','" + invoice_number + "','" + invoice_series + "',false)";

                //                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql);

                //                    string schemaName = await _minvoiceDbContext.GetSchemaNameAsync();

                //                    JObject rb = new JObject();
                //                    rb.Add("wb_sms_queue_id", id);
                //                    rb.Add("SCHEMA_NAME", schemaName);

                //                    _rabbitmqService.PublishQueue("sms_inv_customer_queue", rb.ToString());
                //                }
                //            }
                //        }

                //    }
                //}

                obj.Add("ok", true);
                obj.Add("inv_InvoiceAuth_id", parameters["inv_InvoiceAuth_id"].ToString());
                obj.Add("inv_invoiceIssuedDate", String.Format("{0:yyyy-MM-dd}", tblInv_InvoiceAuth.Rows[0]["invoice_issued_date"]));
                obj.Add("inv_invoiceNumber", tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString());
                obj.Add("trang_thai", "Đã ký");

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> SaveXmlPretreatment(JObject parameters, string username)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("username", username);
                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicPara);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    obj.Add("error", "Bạn không có quyền ký hóa đơn");
                    return obj;
                }

                dicPara.Clear();
                dicPara.Add("inv_invoiceauth_id", Guid.Parse(parameters["inv_InvoiceAuth_id"].ToString()));

                DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT status, invoice_issued_date,inv_invoiceauth_id,invoice_number,branch_code,buyer_email,customer_code,template_code,invoice_series FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicPara);

                if (tblInv_InvoiceAuth.Rows.Count == 0)
                {
                    obj.Add("error", "Không tồn tại hóa đơn");
                    return obj;
                }
                else
                {
                    if (tblInv_InvoiceAuth.Rows[0]["status"].ToString() != "Chờ ký")
                    {
                        obj.Add("error", "Hóa đơn không ở trạng thái chờ ký");
                        return obj;
                    }
                }

                string branch_code = tblInv_InvoiceAuth.Rows[0]["branch_code"].ToString();
                string buyer_email = tblInv_InvoiceAuth.Rows[0]["buyer_email"].ToString();
                string inv_invoiceauth_id = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

                dicPara.Clear();
                dicPara.Add("branch_code", branch_code);

                DataTable tblBranch = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@branch_code ORDER BY code", CommandType.Text, dicPara);

                string subjectName = String.Empty;

                if (tblBranch.Rows.Count == 0)
                {
                    obj.Add("error", "Không tồn tại mã đơn vị");
                    return obj;
                }
                else
                {
                    subjectName = tblBranch.Rows[0]["tax_code"].ToString();
                }

                string xml = parameters["InvoiceXmlData"].ToString();

                xml = Encoding.UTF8.GetString(Convert.FromBase64String(xml));

                //Validate chữ ký
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(xml);

                XmlNodeList nodeListNguoiBan = xmlDoc.GetElementsByTagName("MaSoThueNguoiBan");

                if (nodeListNguoiBan.Count > 0)
                {
                    XmlNode nodeNguoiBan = nodeListNguoiBan[0];
                    if (!subjectName.Contains(nodeNguoiBan.InnerText))
                    {
                        obj.Add("error", "Mã số thuế người bán với chứng thư số không khớp nhau");
                        return obj;
                    };
                }

                XmlNodeList nodeListSignature = xmlDoc.GetElementsByTagName("Signature");

                // Kiểm tra tính hợp lệ của chữ ký.
                if (nodeListSignature.Count <= 0)
                {
                    obj.Add("error", "Không tìm thấy chữ ký trong dữ liệu");
                    return obj;
                }
                else
                {
                    foreach (XmlElement element in nodeListSignature)
                    {
                        if (element.Attributes["Id"].Value == "seller")
                        {
                            string subjectname = element.GetElementsByTagName("X509SubjectName")[0].InnerText;

                            Dictionary<string, object> par = new Dictionary<string, object>();
                            par.Add("branch_code", branch_code);
                            par.Add("subject_name", subjectname);

                            DataTable checkCKS = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code and subject_name=@subject_name and current_timestamp <= end_date", CommandType.Text, par);

                            if (checkCKS.Rows.Count == 0)
                            {
                                obj.Add("error", "Chữ ký số không tồn tại trên hệ thống");
                                return obj;
                            }
                        }

                        try
                        {
                            // Create a new SignedXml object and pass it
                            // the XML document class.
                            SignedXml signedXml = new SignedXml(xmlDoc);

                            // Load the signature node.
                            signedXml.LoadXml(element);

                            // Check the signature and return the result.
                            if (!signedXml.CheckSignature())
                            {
                                obj.Add("error", "Chữ ký số đã bị thay đổi");
                                return obj;
                            }
                        }
                        catch (Exception ex)
                        {
                            obj.Add("error", "Chữ ký số không hợp lệ");
                            return obj;
                        }

                    }
                }

                xml = FunctionUtil.ConvertXml(xml);
                //xml = xml.Replace("'", "''");
                //xml = CommonManager.SignByMInvoice(xml, "Signatures");

                string sobaomat = await GenerateSecurity(6, 1);
                xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                dicPara.Clear();
                dicPara.Add("inv_InvoiceAuth_id", Guid.Parse(parameters["inv_InvoiceAuth_id"].ToString()));
                dicPara.Add("xml", xml);
                dicPara.Add("fullname", tblUser.Rows[0]["fullname"].ToString());
                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //obj.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return obj;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }
                string sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_InvoiceAuth_id,@xml::xml,@fullname)";

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dicPara);

                dicPara.Clear();
                dicPara.Add("branch_code", branch_code);

                sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);

                if (tblSetting.Rows.Count > 0)
                {
                    string value = tblSetting.Rows[0]["value"].ToString();

                    if (value == "C")
                    {
                        if (buyer_email.Length > 0)
                        {
                            JObject item = new JObject();

                            obj.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            obj.Add("nguoinhan", buyer_email);
                            obj.Add("tieude", "Thông báo xuất hóa đơn điện tử");

                            string originalString = _webHelper.GetRequest().Url.OriginalString;
                            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                            await this.AutoSendInvoiceByEmail(folder, obj);

                        }

                    }
                }

                obj.Add("ok", true);
                obj.Add("inv_InvoiceAuth_id", parameters["inv_InvoiceAuth_id"].ToString());
                obj.Add("inv_invoiceIssuedDate", String.Format("{0:yyyy-MM-dd}", tblInv_InvoiceAuth.Rows[0]["invoice_issued_date"]));
                obj.Add("inv_invoiceNumber", tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString());
                obj.Add("trang_thai", "Đã ký");

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JArray> GetHoaDonChoDuyet(string ma_dvcs, string trang_thai)
        {
            ////Dictionary<string, object> parameters = new Dictionary<string, object>();
            ////parameters.Add("branch_code", ma_dvcs);
            ////parameters.Add("status", trang_thai);

            ////string sql = "SELECT *,CASE WHEN buyer_legal_name='' OR buyer_legal_name IS NULL THEN buyer_display_name ELSE buyer_legal_name end as ten_dt,TO_CHAR(invoice_issued_date,'YYYY-MM-DD') as ngay_hd"
            ////    + " FROM #SCHEMA_NAME#.inv_invoiceauth WHERE branch_code=@branch_code AND status=@status"
            ////    + " ORDER BY invoice_issued_date,template_code,invoice_series,invoice_number,order_number";
            ////DataTable tblHoaDon = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
            ////JArray data = JArray.FromObject(tblHoaDon);

            //Dictionary<string, object> parameters = new Dictionary<string, object>();
            //parameters.Add("mdvi", ma_dvcs);
            //parameters.Add("tthai", trang_thai);

            ////string sql = "SELECT *,CASE WHEN buyer_legal_name='' OR buyer_legal_name IS NULL THEN buyer_display_name ELSE buyer_legal_name end as ten_dt,TO_CHAR(invoice_issued_date,'YYYY-MM-DD') as ngay_hd"
            ////    + " FROM #SCHEMA_NAME#.inv_invoiceauth WHERE branch_code=@branch_code AND status=@status"
            ////    + " ORDER BY invoice_issued_date,template_code,invoice_series,invoice_number,order_number";
            //string sql = "SELECT hdon_id as id,* FROM #SCHEMA_NAME#.hoadon68 WHERE mdvi=@mdvi AND tthai=@tthai"
            //    + " ORDER BY tdlap desc";
            //DataTable tblHoaDon = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
            //JArray data = JArray.FromObject(tblHoaDon);


            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("branch_code", ma_dvcs);
            parameters.Add("status", trang_thai);

            string sql = "SELECT *,CASE WHEN buyer_legal_name='' OR buyer_legal_name IS NULL THEN buyer_display_name ELSE buyer_legal_name end as ten_dt,TO_CHAR(invoice_issued_date,'YYYY-MM-DD') as ngay_hd"
                + " FROM #SCHEMA_NAME#.inv_invoiceauth WHERE branch_code=@branch_code AND status=@status"
                + " ORDER BY invoice_issued_date,template_code,invoice_series,invoice_number,order_number";
            DataTable tblHoaDon = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
            JArray data = JArray.FromObject(tblHoaDon);

            return data;

        }

        public async Task<JObject> DuyetHoaDon(JObject model)
        {
            Guid inv_InvoiceAuth_id = Guid.Parse(model["inv_InvoiceAuth_id"].ToString());

            JObject obj = new JObject();

            try
            {
                string dvcs = _webHelper.GetDvcs();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("branch_code", dvcs);

                DataTable tblInvoiceProcess = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoice_process WHERE branch_code=@branch_code ORDER BY ord", CommandType.Text, parameters);
                int i = 0;

                for (int j = 0; j < tblInvoiceProcess.Rows.Count; j++)
                {
                    DataRow row = tblInvoiceProcess.Rows[j];
                    string code = row["code"].ToString();

                    if (code == "01")
                    {
                        i = j;
                        break;
                    }
                }

                string status = "";

                if (tblInvoiceProcess.Rows.Count == 0)
                {
                    status = "Chờ ký";
                }
                else if (tblInvoiceProcess.Rows.Count == 1)
                {
                    status = tblInvoiceProcess.Rows[0]["name"].ToString();
                }
                else
                {
                    status = tblInvoiceProcess.Rows[i + 1]["name"].ToString();
                }

                parameters.Clear();
                parameters.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);

                DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoiceauth_id,to_char(invoice_issued_date,'YYYY-MM-DD') as invoice_issued_date,inv_invoicecode_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);
                string inv_invoicecode_id = tblInvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();
                string invoice_issued_date = tblInvoiceAuth.Rows[0]["invoice_issued_date"].ToString();

                parameters.Clear();
                parameters.Add("inv_invoicecode_id", Guid.Parse(inv_invoicecode_id));
                parameters.Add("invoice_issued_date", DateTime.Parse((invoice_issued_date)));

                string sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE status='Chờ duyệt' AND inv_invoicecode_id=@inv_invoicecode_id AND invoice_issued_date< @invoice_issued_date LIMIT 1";
                tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblInvoiceAuth.Rows.Count > 0)
                {
                    obj.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                    return obj;
                }

                parameters.Clear();

                parameters.Add("status", status);
                parameters.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);

                sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET status=@status  WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;

        }

        public async Task<JObject> TuChoiHoaDon(JObject model)
        {
            Guid inv_InvoiceAuth_id = Guid.Parse(model["inv_InvoiceAuth_id"].ToString());

            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("inv_invoiceauth_id", inv_InvoiceAuth_id);

                string sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET status='Từ chối' WHERE inv_invoiceauth_id=@inv_invoiceauth_id ";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;

        }

        public async Task<JObject> LaySoHoaDon(JObject model, string username)
        {

            JObject obj = new JObject();

            try
            {

                string dvcs = _webHelper.GetDvcs();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("branch_code", dvcs);

                DataTable tblInvoiceProcess = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoice_process WHERE branch_code=@branch_code ORDER BY ord", CommandType.Text, parameters);
                int i = 0;

                for (int j = 0; j < tblInvoiceProcess.Rows.Count; j++)
                {
                    DataRow row = tblInvoiceProcess.Rows[j];
                    string code = row["code"].ToString();

                    if (code == "02")
                    {
                        i = j;
                        break;
                    }
                }

                string status = "";

                if (tblInvoiceProcess.Rows.Count == 0)
                {
                    status = "Chờ ký";
                }
                else if (tblInvoiceProcess.Rows.Count == 1)
                {
                    status = tblInvoiceProcess.Rows[0]["name"].ToString();
                }
                else
                {
                    status = tblInvoiceProcess.Rows[i + 1]["name"].ToString();
                }

                JArray array = (JArray)model["data"];

                string sql = "SELECT #SCHEMA_NAME#.qr_get_invoice_number('#SCHEMA_NAME#',@inv_invoiceauth_id,@username,@status)";

                for (int k = 0; k < array.Count; k++)
                {
                    JObject item = (JObject)array[k];
                    string inv_invoiceauth_id = item["inv_invoiceauth_id"].ToString();

                    string sqlCheckSo = "SELECT invoice_number FROM #SCHEMA_NAME#.inv_invoiceauth where inv_invoiceauth_id = @inv_invoiceauth_id";

                    parameters.Clear();
                    parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                    DataTable tblCheckSo = await _minvoiceDbContext.GetDataTableAsync(sqlCheckSo, CommandType.Text, parameters);

                    if (tblCheckSo.Rows.Count > 0)
                    {
                        bool checkSinhSo = false;
                        foreach (DataRow row in tblCheckSo.Rows)
                        {
                            if (!String.IsNullOrWhiteSpace(row["invoice_number"].ToString()))
                            {
                                checkSinhSo = true;
                                break;
                            }
                        }

                        if (checkSinhSo == true)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }


                    parameters.Clear();
                    parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                    parameters.Add("username", username);
                    parameters.Add("status", status);

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                    //DataTable tblEmailQueue = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_email_queue_id FROM #SCHEMA_NAME#.wb_email_queue WHERE inv_invoiceauth_id='" + inv_invoiceauth_id + "' AND is_send=false");

                    //if (tblEmailQueue.Rows.Count > 0)
                    //{
                    //    string schemaName = await _minvoiceDbContext.GetSchemaNameAsync();

                    //    JObject rabbitObj = new JObject();
                    //    rabbitObj.Add("SCHEMA_NAME", schemaName);

                    //    JArray jarray = JArray.FromObject(tblEmailQueue);
                    //    rabbitObj.Add("data", jarray);

                    //    _rabbitmqService.PublishQueue("notify_email_signer_queue", rabbitObj.ToString());
                    //}
                }

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;

        }

        public async Task<DataTable> GetTemplate(Guid id)
        {

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("sl_invoice_template_id", id);

            string sql = "SELECT a.*,b.code as ma_loai,b.name as ten_loai,c.name as seller_name,c.address as seller_address,"
                    + "c.tax_code as seller_taxcode,c.tel as seller_tel,c.fax as seller_fax,c.email as seller_email FROM #SCHEMA_NAME#.sl_invoice_template a "
                    + "INNER JOIN #SCHEMA_NAME#.sl_invoice_type b ON a.sl_invoice_type_id=b.sl_invoice_type_id "
                    + "INNER JOIN #SCHEMA_NAME#.wb_branch c ON a.branch_code=c.code WHERE a.sl_invoice_template_id=@sl_invoice_template_id";

            return await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

        }

        public async Task<DataTable> GetInvoiceTemplate(Guid id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("sl_invoice_template_id", id);

            string sql = "SELECT sl_invoice_template_id,template_code,invoice_series,invoice_type,template_status,sl_invoice_type_id,signed FROM #SCHEMA_NAME#.sl_invoice_template "
                    + "WHERE sl_invoice_template_id=@sl_invoice_template_id";

            return await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

        }

        public async Task<DataTable> GetXmlDataTemplate()
        {
            string sql = "SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE 1=0";

            return await this._minvoiceDbContext.GetDataTableAsync(sql);
        }

        public async Task<JObject> SaveInvoiceTemlate(string dmmauhoadon_id, string report)
        {
            JObject obj = new JObject();

            try
            {

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("report", report);
                parameters.Add("sl_invoice_template_id", Guid.Parse(dmmauhoadon_id));

                string sql = "UPDATE #SCHEMA_NAME#.sl_invoice_template SET template_status='Đã tạo mẫu',report=@report WHERE sl_invoice_template_id=@sl_invoice_template_id";

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", dmmauhoadon_id);
                _cacheManager.Remove(cacheReportKey);
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> RegisterInvoiceProcess(JObject model, string username)
        {
            JObject obj = new JObject();
            string error = "";

            try
            {
                string dvcs = model["branch_code"] != null ? model["branch_code"].ToString() : _webHelper.GetDvcs();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("branch_code", dvcs);

                string sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE status NOT IN ('Đã ký','Chờ người mua ký','Người mua đã ký','Xóa bỏ') AND branch_code=@branch_code LIMIT 1";
                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblData.Rows.Count > 0)
                {
                    obj.Add("error", "Bạn phải xử lý hết hóa đơn trước khi đăng ký mới quy trình");
                    return obj;
                }

                sql = "SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai IN ('Chờ duyệt','Chờ sinh số','Chờ ký') AND mdvi=@branch_code LIMIT 1";
                tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                parameters.Clear();
                parameters.Add("branch_code", dvcs);

                if (tblData.Rows.Count > 0)
                {
                    obj.Add("error", "Bạn phải xử lý hết hóa đơn 78 trước khi đăng ký mới quy trình");
                    return obj;
                }

                sql = "SELECT code,value FROM #SCHEMA_NAME#.wb_setting WHERE branch_code=@branch_code AND code='SIGN_CREATE_NUMBER'";
                tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblData.Rows.Count == 0)
                {
                    obj.Add("error", "Không có qui tắc sinh số: SIGN_CREATE_NUMBER trong khai báo tham số hệ thống");
                    return obj;
                }

                string value = tblData.Rows[0]["value"].ToString();
                bool isChoSinhSo = false;

                JArray array = (JArray)model["data"];

                for (int i = 0; i < array.Count; i++)
                {
                    JObject item = (JObject)array[i];
                    string name = item["name"].ToString();

                    if (name == "Chờ sinh số")
                    {
                        isChoSinhSo = true;
                        break;
                    }
                }

                if (isChoSinhSo && value == "K")
                {
                    obj.Add("error", "Chờ sinh số không thể cấu hình cùng với qui tắc sinh số sau");
                    return obj;
                }

                if (array.Count > 0)
                {
                    JObject jObj = new JObject();
                    jObj.Add("is_select", true);
                    jObj.Add("ord", 3);
                    jObj.Add("code", "03");
                    jObj.Add("name", "Chờ ký");

                    array.Add(jObj);
                }


                parameters.Clear();
                parameters.Add("branch_code", dvcs);

                sql = "DELETE FROM #SCHEMA_NAME#.inv_invoice_process WHERE branch_code=@branch_code";

                await _minvoiceDbContext.BeginTransactionAsync();
                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                sql = "INSERT INTO #SCHEMA_NAME#.inv_invoice_process(inv_invoice_process_id,ord,code,name,user_new,date_new,branch_code,is_select) \n"
                     + "VALUES(@inv_invoice_process_id,@ord,@code,@name,@user_new,@date_new,@branch_code,@is_select)";

                for (int i = 0; i < array.Count; i++)
                {
                    JObject item = (JObject)array[i];
                    bool chon = Convert.ToBoolean(item["is_select"]);

                    if (chon)
                    {
                        parameters.Clear();
                        parameters.Add("inv_invoice_process_id", Guid.NewGuid());
                        parameters.Add("ord", Convert.ToInt32(item["ord"]));
                        parameters.Add("code", Convert.ToString(item["code"]));
                        parameters.Add("name", Convert.ToString(item["name"]));
                        parameters.Add("user_new", username);
                        parameters.Add("date_new", DateTime.Now);
                        parameters.Add("branch_code", dvcs);
                        parameters.Add("is_select", true);

                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);
                    }
                }

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                error = ex.Message;
                obj.Add("error", error);
            }

            if (error.Length > 0)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return obj;
        }

        public async Task<JObject> CopyInvoiceTemlate(JObject model)
        {
            JObject obj = new JObject();

            try
            {
                string id = model["id"].ToString();
                string invoice_series = model["invoice_series"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("sl_invoice_template_id", Guid.Parse(id));

                string sql = "SELECT * FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id";
                DataTable tblInvoiceTemplate = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                sql = "INSERT INTO #SCHEMA_NAME#.sl_invoice_template(sl_invoice_template_id, name, sl_invoice_type_id, invoice_type, template_code, invoice_series, copy_number, row_print_template, copy1, copy2, copy3, copy4, copy5, copy6, copy7, copy8, copy9, template_status, template_xml, signed, user_new, date_new, user_edit, date_edit, branch_code, report) \n"
                   + "VALUES (@sl_invoice_template_id, @name, @sl_invoice_type_id, @invoice_type, @template_code, @invoice_series, @copy_number, @row_print_template, @copy1, @copy2, @copy3, @copy4, @copy5, @copy6, @copy7, @copy8, @copy9, @template_status, @template_xml, @signed, @user_new, @date_new, @user_edit, @date_edit, @branch_code, @report)";

                DataRow row = tblInvoiceTemplate.Rows[0];
                row.BeginEdit();
                row["sl_invoice_template_id"] = Guid.NewGuid();
                row["signed"] = false;
                row["date_new"] = DateTime.Now;
                row["user_new"] = _webHelper.GetUser();
                row["template_xml"] = DBNull.Value;
                row["invoice_series"] = invoice_series;
                row.EndEdit();

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, row);

                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<byte[]> PrintInvoiceTemplate(string id, string type, bool inChuyenDoi = false)
        {
            byte[] bytes = null;
            string xml = "";

            Guid dmmauhd_id = Guid.Parse(id);

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("sl_invoice_template_id", Guid.Parse(id));

            string sql = "SELECT a.*,b.code as ma_loai,b.name as ten_loai,c.name as seller_name,c.address as seller_address,"
                    + "c.tax_code as seller_taxcode,c.tel as seller_tel,c.fax as seller_fax,c.email as seller_email,c.bank_account as seller_bank_account,c.bank_name as seller_bank_name FROM #SCHEMA_NAME#.sl_invoice_template a "
                    + "INNER JOIN #SCHEMA_NAME#.sl_invoice_type b ON a.sl_invoice_type_id=b.sl_invoice_type_id "
                    + "INNER JOIN #SCHEMA_NAME#.wb_branch c ON a.branch_code=c.code WHERE a.sl_invoice_template_id=@sl_invoice_template_id";

            DataTable tblMauHoaDon = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
            DataRow row = tblMauHoaDon.Rows[0];

            string branch_code = row["branch_code"].ToString();

            string name = "INHD_" + row["template_code"].ToString().Replace("/", "") + "_" + row["invoice_series"].ToString().Replace("/", "") + ".repx";


            XtraReport rp = ReportUtil.LoadReportFromString(row["report"].ToString());

            DataSet dsReport = new DataSet();

            if (row["invoice_type"].ToString() == "E")
            {
                DataTable tblXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE 1=0");
                tblXmlData.TableName = "TblXmlData";

                using (XmlReader reader = XmlReader.Create(new StringReader(rp.DataSourceSchema)))
                {
                    dsReport.ReadXmlSchema(reader);
                    reader.Close();
                }

                dsReport.Tables.Add(tblXmlData);

                xml = row["template_xml"].ToString();

                if (string.IsNullOrEmpty(xml) == false)
                {

                    XmlReaderSettings settings = new XmlReaderSettings();
                    XmlSchema schema = XmlSchema.Read(new StringReader(rp.DataSourceSchema.ToString()), validationXml);

                    using (XmlReader reader = XmlReader.Create(new StringReader(xml.ToString()), settings))
                    {
                        dsReport.ReadXml(reader);
                        reader.Close();
                    }

                    DataRow r = tblXmlData.NewRow();
                    r["inv_invoiceauth_id"] = row["sl_invoice_template_id"];
                    r["data"] = row["template_xml"].ToString();
                    tblXmlData.Rows.Add(r);
                }
                else
                {
                    if (dsReport.Tables.Contains("invoiceinformation"))
                    {
                        DataTable tblThongTinHoaDon = dsReport.Tables["invoiceinformation"];

                        DataRow r = tblThongTinHoaDon.NewRow();

                        r["template_code"] = row["template_code"];
                        r["invoice_series"] = row["invoice_series"];
                        r["invoice_number"] = "0000000";
                        r["invoice_name"] = row["ten_loai"];
                        r["seller_name"] = row["seller_name"];
                        r["seller_address"] = row["seller_address"];
                        r["seller_tel"] = row["seller_tel"];
                        r["seller_taxcode"] = row["seller_taxcode"];
                        r["seller_bank_account"] = row["seller_bank_account"];
                        r["seller_bank_name"] = row["seller_bank_name"];

                        tblThongTinHoaDon.Rows.Add(r);
                    }

                    if (dsReport.Tables.Contains("Certified_data"))
                    {
                        DataTable tblCertifiedData = dsReport.Tables["Certified_data"];

                        DataRow r = tblCertifiedData.NewRow();

                        r["qrCodeData"] = row["sl_invoice_template_id"] + ";"
                                           + row["seller_taxcode"] + ";"
                                           + row["template_code"] + ";"
                                           + row["invoice_series"] + ";"
                                           + "0000000" + ";"
                                           + String.Format("{0:yyyy-MM-dd}", new DateTime(1900, 1, 1));

                        tblCertifiedData.Rows.Add(r);
                    }

                    int sodonginmau = row["row_print_template"].ToString().Length > 0 ? Convert.ToInt32(row["row_print_template"]) : 0;

                    if (dsReport.Tables.Contains("invoicedetail") && sodonginmau > 0)
                    {
                        DataTable tblChiTiet = dsReport.Tables["invoicedetail"];

                        for (int i = 1; i <= sodonginmau; i++)
                        {
                            DataRow r = tblChiTiet.NewRow();
                            tblChiTiet.Rows.Add(r);
                        }
                    }
                }

                rp.DataSource = dsReport;
            }
            else
            {
                parameters.Clear();
                parameters.Add("sl_invoice_template_id", Guid.Parse(row["sl_invoice_template_id"].ToString()));

                DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_inct_invoice_template(@sl_invoice_template_id)", CommandType.Text, parameters);
                tblData.TableName = "Table";
                dsReport.Tables.Add(tblData);

                rp.DataSource = dsReport;
                rp.DataMember = dsReport.Tables[0].TableName;
            }

            var barCodes = rp.AllControls<XRBarCode>();

            if (barCodes != null)
            {
                foreach (XRBarCode barcode in barCodes)
                {
                    if (barcode.Symbology.Name.ToUpper() == "QRCODE")
                    {
                        QRCodeGenerator codeGen = (QRCodeGenerator)barcode.Symbology;
                        codeGen.CompactionMode = QRCodeCompactionMode.Byte;
                        codeGen.ErrorCorrectionLevel = QRCodeErrorCorrectionLevel.L;
                        codeGen.Version = QRCodeVersion.AutoVersion;

                    }
                }
            }

            var lblHoaDonMau = rp.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

            if (lblHoaDonMau != null)
            {
                lblHoaDonMau.Visible = true;
            }

            parameters.Clear();
            parameters.Add("branch_code", branch_code);

            DataTable tblSystemSetting = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE report='C' AND branch_code=@branch_code", CommandType.Text, parameters);

            foreach (DataRow r in tblSystemSetting.Rows)
            {
                string key = r["code"].ToString();
                string giatri = r["value"].ToString();

                if (rp.Parameters[key] == null)
                {
                    DevExpress.XtraReports.Parameters.Parameter param = new DevExpress.XtraReports.Parameters.Parameter();
                    param.Name = r["code"].ToString();
                    param.Value = r["value"].ToString();
                    rp.Parameters.Add(param);
                }
                else
                {
                    rp.Parameters[key].Value = giatri;
                }
            }

            if (rp.Parameters["MSG_TB"] == null)
            {
                DevExpress.XtraReports.Parameters.Parameter prMsg_tb = new DevExpress.XtraReports.Parameters.Parameter();
                prMsg_tb.Name = "MSG_TB";
                rp.Parameters.Add(prMsg_tb);
            }

            rp.ScriptReferencesString = "AccountSignature.dll";
            rp.Name = "XtraReport1";
            rp.DisplayName = name;
            if (inChuyenDoi)
            {
                var tblInChuyenDoi = rp.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                if (tblInChuyenDoi != null)
                {
                    tblInChuyenDoi.Visible = true;
                }

                if (rp.Parameters["MSG_HD_TITLE"] != null)
                {
                    rp.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                }

                if (rp.Parameters["NGUOI_IN_CDOI"] != null)
                {
                    rp.Parameters["NGUOI_IN_CDOI"].Value = ".................";
                    rp.Parameters["NGUOI_IN_CDOI"].Visible = true;
                }

                if (rp.Parameters["NGAY_IN_CDOI"] != null)
                {
                    rp.Parameters["NGAY_IN_CDOI"].Value = "../../....";
                    rp.Parameters["NGAY_IN_CDOI"].Visible = true;
                }
            }
            rp.CreateDocument();

            if (rp.Pages.Count >= 2)
            {
                var pageInfo = rp.Bands[BandKind.PageFooter].FindControl("pageInfo", true);

                if (pageInfo != null)
                {
                    pageInfo.Visible = true;
                    rp.CreateDocument();
                }
            }

            MemoryStream ms = new MemoryStream();


            if (type == "Html")
            {
                rp.ExportOptions.Html.EmbedImagesInHTML = true;
                rp.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                rp.ExportOptions.Html.Title = "Mẫu hóa đơn";
                rp.ExportToHtml(ms);

                string html = Encoding.UTF8.GetString(ms.ToArray());

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);


                string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                if (nodes != null)
                {
                    foreach (HtmlNode node in nodes)
                    {
                        string eventMouseDown = node.Attributes["onmousedown"].Value;

                        if (eventMouseDown.Contains("showCert('seller')"))
                        {
                            node.SetAttributeValue("id", "certSeller");
                        }
                        if (eventMouseDown.Contains("showCert('buyer')"))
                        {
                            node.SetAttributeValue("id", "certBuyer");
                        }
                        if (eventMouseDown.Contains("showCert('vacom')"))
                        {
                            node.SetAttributeValue("id", "certVacom");
                        }
                        if (eventMouseDown.Contains("showCert('minvoice')"))
                        {
                            node.SetAttributeValue("id", "certMinvoice");
                        }
                    }
                }

                HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                HtmlNode xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("id", "xmlData");
                xmlNode.SetAttributeValue("type", "text/xmldata");

                xmlNode.AppendChild(doc.CreateTextNode(xml));
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("type", "text/javascript");

                xmlNode.InnerHtml = "$(function () { "
                                   + "  var url = 'http://localhost:19898/signalr'; "
                                   + "  var connection = $.hubConnection(url, {  "
                                   + "     useDefaultPath: false "
                                   + "  });"
                                   + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                   + " invoiceHubProxy.on('resCommand', function (result) { "
                                   + " }); "
                                   + " connection.start().done(function () { "
                                   + "      console.log('Connect to the server successful');"
                                   + "      $('#certSeller').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'seller' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certVacom').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'vacom' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certBuyer').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'buyer' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certMinvoice').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'minvoice' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "})"
                                   + ".fail(function () { "
                                   + "     alert('failed in connecting to the signalr server'); "
                                   + "});"
                                   + "});";

                head.AppendChild(xmlNode);

                if (rp.Watermark != null)
                {
                    if (rp.Watermark.Image != null)
                    {
                        ImageConverter _imageConverter = new ImageConverter();
                        byte[] img = (byte[])_imageConverter.ConvertTo(rp.Watermark.Image, typeof(byte[]));

                        string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                        HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                        string strechMode = rp.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                        string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                        HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                        style.AppendChild(textNode);

                        HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                        HtmlNodeCollection pageNodes = body.SelectNodes("div");

                        foreach (var pageNode in pageNodes)
                        {
                            pageNode.Attributes.Add("class", "waterMark");

                            string divStyle = pageNode.Attributes["style"].Value;
                            divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                            pageNode.Attributes["style"].Value = divStyle;
                        }
                    }
                }

                ms.SetLength(0);
                doc.Save(ms);

                doc = null;
            }
            else if (type == "Image")
            {
                var options = new ImageExportOptions(ImageFormat.Png)
                {
                    ExportMode = ImageExportMode.SingleFilePageByPage,
                };

                rp.ExportToImage(ms, options);
            }
            else
            {
                rp.ExportToPdf(ms);
            }


            bytes = ms.ToArray();


            return bytes;

        }


        public async Task<JObject> GetXmlTemplate(Guid id)
        {
            JObject json = new JObject();

            try
            {

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("id", id);

                string sql = "SELECT * FROM #SCHEMA_NAME#.qr_export_xmltemplate('#SCHEMA_NAME#',@id)";

                DataTable tblResult = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                if (tblResult.Rows.Count > 0)
                {
                    string xml = tblResult.Rows[0][0].ToString();

                    json.Add("id", id);
                    json.Add("xml", xml);
                }
                else
                {
                    json.Add("error", "Không tải được file XML");
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;

        }

        public async Task<JObject> GetXmlData(Guid id)
        {
            JObject json = new JObject();

            try
            {
                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("inv_invoiceauth_id", id);

                DataTable tblResult = await _minvoiceDbContext.GetDataTableAsync("SELECT data FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);

                if (tblResult.Rows.Count > 0)
                {
                    string xml = tblResult.Rows[0][0].ToString();

                    json.Add("id", id);
                    json.Add("xml", xml);
                }
                else
                {
                    json.Add("error", "Hóa đơn chưa có file Xml");
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<JObject> SignXmlTemplate(string id, string xml)
        {
            JObject obj = new JObject();

            try
            {
                //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                var invoiceDb = this._nopDbContext.GetInvoiceDb();

                string sql = "UPDATE #SCHEMA_NAME#.sl_invoice_template SET signed=true,template_xml=CAST(@xml as xml) WHERE sl_invoice_template_id=@sl_invoice_template_id";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("xml", xml);
                parameters.Add("sl_invoice_template_id", Guid.Parse(id));

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public static ValidationEventHandler validationXml { get; set; }


        public string GetTB01ACXml(string id)
        {
            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("dpthongbao_id", Guid.Parse(id));

            DataTable tblMauHoaDon = this._nopDbContext.ExecuteCmd("SELECT xml_data FROM dpthongbao WHERE dpthongbao_id=@dpthongbao_id", CommandType.Text, dicParam);
            string xml = tblMauHoaDon.Rows[0]["xml_data"].ToString();

            if (xml.Length > 0)
            {
                xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xml;
                return xml;
            }

            dicParam.Clear();
            dicParam.Add("id", Guid.Parse(id));

            DataTable tblResult = this._nopDbContext.ExecuteCmd("EXECUTE [sproc_tbphathanh_sendData] @id", CommandType.Text, dicParam);

            xml = tblResult.Rows[0][0].ToString();

            return xml;
        }

        public JObject SaveTB01ACXml(JObject model)
        {
            JObject json = new JObject();

            try
            {
                string id = model["id"].ToString();
                string xml = model["xml"].ToString();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                foreach (XmlNode node in doc)
                {
                    if (node.NodeType == XmlNodeType.XmlDeclaration)
                    {
                        doc.RemoveChild(node);
                        break;
                    }
                }

                string sql = "UPDATE dpthongbao SET trang_thai=N'Đã ký',xml_data=@xml_data WHERE dpthongbao_id=@dpthongbao_id";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("xml_data", doc.OuterXml);
                parameters.Add("dpthongbao_id", id);

                _nopDbContext.ExecuteNoneQuery(sql, parameters);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<string> GetInvoiceFileName(string id)
        {
            string fileName = "";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("inv_invoiceauth_id", Guid.Parse(id));

            DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);

            string mau_hd = tblInv_InvoiceAuth.Rows[0]["template_code"].ToString();
            string so_serial = tblInv_InvoiceAuth.Rows[0]["invoice_series"].ToString();
            string so_hd = tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString();
            string inv_InvoiceCode_id = tblInv_InvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();

            fileName = "invoice_" + mau_hd.Replace("/", "") + "_" + so_serial.Replace("/", "").Trim() + "_" + so_hd;

            return fileName;
        }

        public async Task<byte[]> ExportZipFile(string id, string pathReport, string fileName)
        {
            byte[] result = null;

            Guid inv_InvoiceAuth_id = Guid.Parse(id);

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("inv_invoiceauth_id", Guid.Parse(id));

            DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);

            string mau_hd = tblInv_InvoiceAuth.Rows[0]["template_code"].ToString();
            string so_serial = tblInv_InvoiceAuth.Rows[0]["invoice_series"].ToString();
            string so_hd = tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString();
            string inv_InvoiceCode_id = tblInv_InvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();

            if (fileName.Length == 0)
            {
                fileName = "invoice_" + mau_hd.Replace("/", "") + "_" + so_serial.Replace("/", "").Trim() + "_" + so_hd;
            }

            DataTable tblInvoiceXmlData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, parameters);


            if (tblInvoiceXmlData.Rows.Count == 0)
            {
                return null;
            }

            parameters.Clear();
            parameters.Add("inv_notificationdetail_id", Guid.Parse(inv_InvoiceCode_id));


            DataTable tblCtthongbao = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_notificationdetail WHERE inv_notificationdetail_id=@inv_notificationdetail_id", CommandType.Text, parameters);

            parameters.Clear();
            parameters.Add("sl_invoice_template_id", Guid.Parse(tblCtthongbao.Rows[0]["sl_invoice_template_id"].ToString()));

            DataTable tblMauHoaDon = await _minvoiceDbContext.GetDataTableAsync("SELECT report FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id", CommandType.Text, parameters);

            string xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + xml;

            byte[] dataPdf = await this.PrintInvoiceFromId(id, pathReport, "PDF", "");

            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

            ZipEntry newEntry = new ZipEntry("invoice.xml");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            byte[] bytes = Encoding.UTF8.GetBytes(xml);

            MemoryStream inStream = new MemoryStream(bytes);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            newEntry = new ZipEntry(fileName + ".pdf");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            inStream = new MemoryStream(dataPdf);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            inStream = new MemoryStream();
            using (StreamWriter sw = new StreamWriter(inStream))
            {
                sw.Write(tblMauHoaDon.Rows[0]["report"].ToString());
                sw.Flush();

                newEntry = new ZipEntry("invoice.repx");
                newEntry.DateTime = DateTime.Now;
                newEntry.IsUnicodeText = true;
                zipStream.PutNextEntry(newEntry);

                inStream.WriteTo(zipStream);
                inStream.Close();
                zipStream.CloseEntry();

                sw.Close();
            }

            zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;


            result = outputMemStream.ToArray();

            outputMemStream.Close();

            return result;
        }


        public JObject Search(string ma_dvcs, JObject model)
        {
            int start = Convert.ToInt32(model["start"].ToString());
            int count = Convert.ToInt32(model["count"].ToString());

            bool _continue = false;

            if (!string.IsNullOrEmpty(model["continue"].ToString()))
            {
                _continue = model["continue"].ToString() == "true" ? true : false;
            }

            JObject filter = (JObject)model["filter"];

            string where = "";

            if (filter["typingFilter"] != null)
            {
                string typingFilter = filter["typingFilter"].ToString();
                where = where + " AND (inv_buyerLegalName like N'%" + typingFilter + "%' OR inv_buyerAddressLine like N'%" + typingFilter + "%' "
                        + "OR inv_buyerTaxCode like N'%" + typingFilter + "%' "
                        + "OR inv_buyerDisplayName like N'%" + typingFilter + "%' "
                        + "OR inv_invoiceSeries like N'%" + typingFilter + "%' "
                        + "OR mau_hd like N'%" + typingFilter + "%' "
                        + "OR inv_invoiceNumber like N'%" + typingFilter + "%' "
                        + ")";
            }

            if (filter["paramFilter"].ToString() != "{}")
            {
                JObject paramFilter = (JObject)filter["paramFilter"];

                if (paramFilter["inv_invoiceType"].ToString() != "")
                {
                    where = where + "AND inv_invoiceType='" + paramFilter["inv_invoiceType"].ToString() + "' ";
                }

                if (paramFilter["mau_hd"].ToString() != "")
                {
                    where = where + "AND mau_hd='" + paramFilter["mau_hd"].ToString() + "' ";
                }

                if (paramFilter["inv_invoiceSeries"].ToString() != "")
                {
                    where = where + "AND inv_invoiceSeries='" + paramFilter["inv_invoiceSeries"].ToString() + "' ";
                }

                if (paramFilter["inv_invoiceNumber"].ToString() != "")
                {
                    where = where + "AND inv_invoiceNumber='" + paramFilter["inv_invoiceNumber"].ToString() + "' ";
                }

                if (paramFilter["user_new"].ToString() != "")
                {
                    where = where + "AND user_new='" + paramFilter["user_new"].ToString() + "' ";
                }

                if (paramFilter["tu_ngay"].ToString() != "")
                {
                    DateTime tu_ngay = Convert.ToDateTime(paramFilter["tu_ngay"].ToString());
                    where = where + "AND inv_invoiceIssuedDate>='" + string.Format("{0:yyyy-MM-dd}", tu_ngay) + "' ";
                }

                if (paramFilter["den_ngay"].ToString() != "")
                {
                    DateTime den_ngay = Convert.ToDateTime(paramFilter["den_ngay"].ToString());
                    where = where + "AND inv_invoiceIssuedDate<='" + string.Format("{0:yyyy-MM-dd}", den_ngay) + "' ";
                }

                if (paramFilter["trang_thai"].ToString() != "")
                {
                    where = where + "AND trang_thai='" + paramFilter["trang_thai"].ToString() + "' ";
                }

                if (paramFilter["trang_thai_hd"].ToString() != "")
                {
                    where = where + "AND trang_thai_hd=" + paramFilter["trang_thai_hd"].ToString() + " ";
                }
            }

            if (where.Length == 0)
            {
                where = "WHERE 1=0 ";
            }
            else
            {
                where = "WHERE trang_thai<>N'Xóa bỏ' AND ma_dvcs=N'" + ma_dvcs + "' " + where;
            }

            string sql = "SELECT * FROM inv_InvoiceAuth " + where + " ORDER BY ma_dvcs,mau_hd,inv_invoiceSeries,inv_invoiceIssuedDate,inv_invoiceNumber " + " OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";

            DataTable tblData = this._nopDbContext.ExecuteCmd(sql);

            JObject result = new JObject();
            result.Add("data", JArray.FromObject(tblData));
            result.Add("pos", start);

            if (_continue == false)
            {
                DataTable tblTotal = this._nopDbContext.ExecuteCmd("SELECT COUNT(*) FROM inv_InvoiceAuth " + where);
                result.Add("total_count", Convert.ToInt32(tblTotal.Rows[0][0]));
            }

            return result;

        }


        public async Task<JObject> SendInvoiceByEmail(string folder, JObject model, string ma_dvcs, string type)
        {
            JObject result = new JObject();

            MemoryStream msZip = null;
            MemoryStream msPdf = null;

            string sql = "";

            try
            {
                string nguoinhan = model["nguoinhan"].ToString();
                string tieude = model["tieude"].ToString();
                string noidung = model["noidung"].ToString();
                string inv_InvoiceAuth_id = model["inv_invoiceauth_id"].ToString();
                string nguoi_gui = model["nguoi_gui"].ToString();
                string mat_khau = model["mat_khau"].ToString();
                string bcc = model["bcc"].ToString();
                string alias = model["alias"]?.ToString();

                _emailService.ClearAttach();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();

                if (type == "Gửi hóa đơn")
                {
                    string fileName = await GetInvoiceFileName(inv_InvoiceAuth_id);

                    byte[] dataZip = await ExportZipFile(inv_InvoiceAuth_id, folder, fileName);
                    msZip = new MemoryStream(dataZip);
                    _emailService.Attach(fileName + ".zip", msZip, "application/zip");

                    byte[] dataPdf = await PrintInvoiceFromId(inv_InvoiceAuth_id, folder, "PDF", "");
                    msPdf = new MemoryStream(dataPdf);
                    _emailService.Attach(fileName + ".pdf", msPdf, "application/pdf");
                }
                else if (type == "Ký xác nhận")
                {
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));

                    sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET status=N'Chờ người mua ký' WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dicParam);
                }


                dicParam.Clear();
                dicParam.Add("branch_code", ma_dvcs);
                dicParam.Add("tempemail_type", type);

                DataTable tblTempEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND branch_code=@branch_code AND tempemail_type=@tempemail_type LIMIT 1 OFFSET 0", CommandType.Text, dicParam);

                if (tblTempEmail.Rows.Count > 0)
                {
                    string smtpAddress = tblTempEmail.Rows[0]["smtp_address"].ToString();
                    int smtpPort = Convert.ToInt32(tblTempEmail.Rows[0]["smtp_port"]);
                    bool enableSSL = Convert.ToBoolean(tblTempEmail.Rows[0]["enable_ssl"]);

                    if (smtpAddress.Length > 0)
                    {
                        _emailService.SetEmailServer(smtpAddress, smtpPort, enableSSL);
                    }
                }

                string[] parts = nguoinhan.Replace(",", ";").Split(';');

                foreach (var part in parts)
                {
                    await _emailService.SendAsync(nguoi_gui, mat_khau, part, tieude, noidung, bcc, alias);

                    dicParam.Clear();
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));

                    DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT branch_code FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);

                    string branch_code = tblInvoiceAuth.Rows[0]["branch_code"].ToString();

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email(wb_log_email_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                         + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("branch_code", branch_code);
                    parameters.Add("send_date", DateTime.Now);
                    parameters.Add("from", nguoi_gui);
                    parameters.Add("to", part);
                    parameters.Add("subject", tieude);
                    parameters.Add("content", noidung);
                    parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));
                    parameters.Add("user_send", _webHelper.GetUser());
                    parameters.Add("send_type", type);

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                }


                result.Add("ok", true);

            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);
            }
            finally
            {
                if (msZip != null)
                {
                    msZip.Close();
                }

                if (msPdf != null)
                {
                    msPdf.Close();
                }
            }

            return result;
        }

        public async Task<JArray> InvoiceHistorySendEmail(string id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("inv_invoiceauth_id", Guid.Parse(id));

            string sql = "SELECT a.inv_invoiceauth_id,a.invoice_number,a.invoice_series,a.template_code,b.user_send,b.send_date,b.to as send_to,b.send_type FROM #SCHEMA_NAME#.inv_invoiceauth a INNER JOIN #SCHEMA_NAME#.wb_log_email b ON a.inv_invoiceauth_id=b.inv_invoiceauth_id \n"
                    + " WHERE a.inv_invoiceauth_id=@inv_invoiceauth_id"
                    + " UNION "
                    + " SELECT a.hdon_id,a.shdon,RIGHT(a.khieu, 6),LEFT(a.khieu, 1),b.user_send,b.send_date,b.to as send_to,b.send_type FROM #SCHEMA_NAME#.hoadon68 a INNER JOIN #SCHEMA_NAME#.wb_log_email_68 b ON a.hdon_id=b.inv_invoiceauth_id"
                    + "  WHERE a.hdon_id=@inv_invoiceauth_id";

            DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

            return JArray.FromObject(tblInvoiceAuth);
        }

        public string DoubleStrToString(string strdec)
        {
            try
            {
                if (strdec.Contains("."))
                {
                    string result = strdec.TrimEnd('0').TrimEnd('.');
                    if (result.Length > 0 && result[result.Length - 1] == '.') return result.Substring(0, result.Length - 1);
                    return strdec.Contains(".") ? strdec.TrimEnd('0').TrimEnd('.') : strdec;
                }
                else
                {
                    return strdec;
                }
            }
            catch
            {
                return strdec;
            }
        }

        public async Task<JObject> GetEmailTemplate(string type, string id, string ma_dvcs, string originalString)
        {
            JObject result = new JObject();
            try
            {
                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT branch_code,customer_code,buyer_legal_name,buyer_display_name,invoice_number, total_amount FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@P_0", Guid.Parse(id));
                string customer_code = tblInvoice.Rows[0]["customer_code"].ToString();
                string branch_code = tblInvoice.Rows[0]["branch_code"].ToString();

                DataTable tblTemp = await _minvoiceDbContext.GetDataTableAsync("SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type=@P_0 AND branch_code=@P_1 LIMIT 1 OFFSET 0", type, ma_dvcs);

                string pass1 = tblTemp.Rows[0]["pass"].ToString();
                string alias = tblTemp.Rows[0]["alias"].ToString();

                JArray array = JArray.FromObject(tblTemp);

                result = (JObject)array[0];
                result["pass"] = pass1;
                result["alias"] = alias;

                string mdvi = tblInvoice.Rows[0]["branch_code"]?.ToString();
                DataTable tblBranch = await _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code = '{mdvi}'");

                DataTable tblEmailBody = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@P_0)", Guid.Parse(id));
                DataTable tblEmailSubject = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_subject_email_invoice('#SCHEMA_NAME#',@P_0)", Guid.Parse(id));

                string noi_dung = result["noi_dung"].ToString();
                string tieude = result["subject"].ToString();

                if (type == "Ký xác nhận")
                {
                    var hostEmail = _webHelper.GetUrlHost();
                    string url = originalString + "/Content/App/SignInvoice/SignInvoiceApplication.application?type=07&id=" + id;
                    noi_dung = noi_dung.Replace("{URL_CONFIRM}", url + "type=32&mst=" + hostEmail);
                    noi_dung = noi_dung.Replace("{ID_INV}", id);
                }
                string total_amount = DoubleStrToString(tblInvoice.Rows[0]["total_amount"].ToString());
                CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
                string total_amount_convert = double.Parse(total_amount).ToString("#,###", cul.NumberFormat);
                noi_dung = noi_dung.Replace("#total_amount#", total_amount_convert);

                DataRow row = tblEmailBody.Rows[0];

                foreach (DataColumn column in tblEmailBody.Columns)
                {
                    if (column.ColumnName == "url_confirm")
                    {
                        var hostEmail = _webHelper.GetUrlHost();
                        string value = row[column.ColumnName].ToString();
                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value + "&type=32&mst=" + hostEmail);
                    }
                    else
                    {
                        string value = row[column.ColumnName].ToString();
                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                    }
                }

                DataRow row_sub = tblEmailSubject.Rows[0];
                foreach (DataColumn column in tblEmailSubject.Columns)
                {
                    string value = row_sub[column.ColumnName].ToString();
                    tieude = tieude.Replace("#" + column.ColumnName + "#", value);
                }

                //string tendvi = tblInvoice.Rows[0]["buyer_legal_name"]?.ToString();

                //tieude = tieude.Replace("#invoice_number#", tblInvoice.Rows[0]["invoice_number"].ToString());
                //tieude = tieude.Replace("#seller_legal_name#", tblBranch.Rows[0]["name"].ToString());
                //tieude = tieude.Replace("#buyer_name#", tendvi != null ? tendvi : tblInvoice.Rows[0]["buyer_display_name"].ToString());

                DataTable tblHisEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT COUNT(*) FROM #SCHEMA_NAME#.wb_log_email WHERE inv_invoiceauth_id=@P_0", Guid.Parse(id));
                int his_mail = Convert.ToInt32(tblHisEmail.Rows[0][0]);

                result["subject"] = tieude;
                result["noi_dung"] = noi_dung;
                result["body"] = noi_dung;

                result.Add("his_email", his_mail);

                DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT a.email,b.email as email1 FROM #SCHEMA_NAME#.pl_customer a LEFT JOIN #SCHEMA_NAME#.pl_customer_email b ON a.pl_customer_id=b.pl_customer_id WHERE a.code=@P_0 AND a.branch_code=@P_1", customer_code, branch_code);

                array = new JArray();

                if (tblEmail.Rows.Count > 0)
                {
                    string email = tblEmail.Rows[0]["email"].ToString();

                    if (email.Length > 0)
                    {
                        JObject obj = new JObject();
                        obj.Add("id", email);
                        obj.Add("value", email);

                        array.Add(obj);
                    }

                    for (int i = 0; i < tblEmail.Rows.Count; i++)
                    {
                        DataRow r = tblEmail.Rows[i];
                        string email1 = r["email1"].ToString();

                        if (email1.Length > 0 && email1 != email)
                        {
                            JObject obj = new JObject();
                            obj.Add("id", email1);
                            obj.Add("value", email1);

                            array.Add(obj);
                        }
                    }
                }

                result.Add("lst_email", array);

                return result;
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);
                return result;
            }

        }

        public async Task<JArray> GetListForSigning(string ma_dvcs)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("branch_code", ma_dvcs);

            string sql = "SELECT *,cast(false as boolean) as chon FROM inv_invoiceauth WHERE invoice_status=N'Chờ ký' AND branch_code=@branch_code ORDER BY template_code,invoice_series,invoice_number";

            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

            return JArray.FromObject(tblData);
        }


        public async Task<JObject> SignConfirm(JObject parameters)
        {
            JObject obj = new JObject();
            string error = "";
            bool isOpen = false;

            try
            {

                string xml = parameters["InvoiceXmlData"].ToString();
                xml = FunctionUtil.ReverseXml(xml);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                string id = doc.GetElementsByTagName("sellerapprecord_id")[0].InnerText;


                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                parameters.Add("inv_invoiceauth_id", Guid.Parse(id));

                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);

                if (tblInvoice.Rows.Count == 0)
                {
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                string trang_thai = tblInvoice.Rows[0]["status"].ToString();

                //if (trang_thai != "Chờ người mua ký")
                //{
                //    obj.Add("error", "Hóa đơn không ở trạng thái chờ người mua ký");
                //    return obj;
                //}

                int idx = xml.IndexOf("Id=\"buyer\"");

                if (idx < 0)
                {
                    obj.Add("error", "Người mua chưa ký");
                    return obj;
                }

                XmlNode node = doc.GetElementsByTagName("certified_data")[0];

                XmlNodeList nodeList = doc.GetElementsByTagName("Signature");

                foreach (XmlElement element in nodeList)
                {
                    if (element.Attributes["Id"].Value == "buyer")
                    {
                        string subjectname = element.GetElementsByTagName("X509SubjectName")[0].InnerText;
                        string[] name = subjectname.Split(',');

                        for (int i = 0; i < name.Length; i++)
                        {
                            if (name[i].Trim().StartsWith("O=") || name[i].Trim().StartsWith("CN="))
                            {
                                XmlElement elem = doc.CreateElement("NguoiMuaKy");
                                elem.InnerText = "Được ký bởi " + name[i].Trim().Replace("O=", "").Replace("CN=", "");
                                node.AppendChild(elem);

                                elem = doc.CreateElement("NgayNguoiMuaKy");
                                elem.InnerText = string.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
                                node.AppendChild(elem);

                                break;
                            }
                        }

                    }

                }

                await _minvoiceDbContext.BeginTransactionAsync();
                isOpen = true;

                string sql = "UPDATE #SCHEMA_NAME#.inv_invoicexmldata SET data=CAST(@data as xml) WHERE inv_invoiceauth_id=@inv_invoiceauth_id";

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", Guid.Parse(id));
                dicParam.Add("data", FunctionUtil.ConvertXml(doc.OuterXml));

                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dicParam);

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", Guid.Parse(id));

                sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET status='Người mua đã ký' WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dicParam);

                await _minvoiceDbContext.TransactionCommitAsync();
            }
            catch (Exception ex)
            {
                error = ex.Message;
                obj.Add("error", ex.Message);
            }

            if (isOpen)
            {
                if (error.Length > 0)
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                }

                _minvoiceDbContext.CloseTransaction();
            }

            return obj;
        }

        public JArray DashboardTiles(string ma_dvcs)
        {

            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("ma_dvcs", ma_dvcs);
            dicParam.Add("user", _webHelper.GetUser());

            string sql = "SELECT * FROM #SCHEMA_NAME#.qr_report_dashboard_tiles('#SCHEMA_NAME#',@ma_dvcs , @user)";
            DataTable tblData = _minvoiceDbContext.GetDataTable(sql, CommandType.Text, dicParam);
            JArray array = JArray.FromObject(tblData);

            return array;
        }

        public JArray DashboardInvoices(string ma_dvcs)
        {
            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("ma_dvcs", ma_dvcs);

            DataTable tblData = _nopDbContext.ExecuteCmd("EXECUTE sproc_bc_dashboard_invoices @ma_dvcs", CommandType.Text, dicParam);
            JArray array = JArray.FromObject(tblData);

            return array;
        }


        public async Task<string> UploadInvTemplate(string ma_dvcs, string user_login, string dmmauhoadon_id, byte[] bytes)
        {
            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("sl_invoice_template_id", Guid.Parse(dmmauhoadon_id));

            DataTable tblMauHoaDon = await _minvoiceDbContext.GetDataTableAsync("SELECT signed FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id", CommandType.Text, dicParam);
            bool da_ky = Convert.ToBoolean(tblMauHoaDon.Rows[0]["signed"]);

            if (da_ky == true)
            {
                return "Mẫu hóa đơn đã được ký bạn không thể tải lên";
            }

            string report = System.Text.Encoding.UTF8.GetString(bytes);
            string sql = "UPDATE #SCHEMA_NAME#.sl_invoice_template SET template_status='Đã tạo mẫu',report=@report WHERE sl_invoice_template_id=@sl_invoice_template_id";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("report", report);
            parameters.Add("sl_invoice_template_id", Guid.Parse(dmmauhoadon_id));

            await _minvoiceDbContext.ExecuteNoneQueryAsyncNoneSQLInjection(sql, parameters);

            string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", dmmauhoadon_id);
            _cacheManager.Remove(cacheReportKey);

            return "";
        }

        public async Task<JObject> CancelInvoice(string id)
        {
            JObject obj = new JObject();

            try
            {

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("inv_invoiceauth_id", Guid.Parse(id));

                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoiceauth_id,adjustment_type,invoice_status FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id ", CommandType.Text, parameters);

                if (tblInvoice.Rows.Count == 0)
                {
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                if (tblInvoice.Rows[0]["invoice_status"].ToString() != "Chờ ký" && tblInvoice.Rows[0]["invoice_status"].ToString() != "Chờ duyệt")
                {
                    obj.Add("error", "Hóa đơn đã được ký không thể hủy");
                    return obj;
                }

                parameters.Clear();
                parameters.Add("inv_invoiceauth_id", Guid.Parse(id));
                string sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET adjustment_type=15 WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                obj.Add("ok", true);
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> SendListInvoices(string folder, JObject model1)
        {
            JObject res = new JObject();

            JArray invs = (JArray)model1["invs"];


            try
            {
                for (int i = 0; i < invs.Count; i++)
                {
                    string inv_invoiceauth_id = invs[i].ToString();

                    Dictionary<string, object> dicParam = new Dictionary<string, object>();
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                    DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT buyer_email,status FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);

                    string status = tblInvoice.Rows[0]["status"].ToString();

                    if (status == "Đã ký" || status == "Chờ người mua ký" || status == "Người mua đã ký" || status == "Xóa bỏ")
                    {
                        string nguoinhan = tblInvoice.Rows[0]["buyer_email"].ToString();

                        if (nguoinhan.Length == 0)
                        {
                            continue;
                        }

                        //string tieude = model["tieude"].ToString();
                        string type = "Gửi hóa đơn";

                        var emailTemplate = await this.GetEmailTemplate(type, inv_invoiceauth_id, _webHelper.GetDvcs(), "");

                        JObject model = new JObject();
                        model.Add("noidung", emailTemplate["body"].ToString());
                        model.Add("nguoi_gui", emailTemplate["sender"].ToString());
                        model.Add("mat_khau", emailTemplate["pass"].ToString());
                        model.Add("nguoinhan", nguoinhan);
                        model.Add("tieude", emailTemplate["subject"].ToString());
                        model.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                        model.Add("bcc", "");

                        JObject result = await this.SendInvoiceByEmail(folder, model, _webHelper.GetDvcs(), type);

                        if (result["error"] != null)
                        {
                            return result;
                        }
                    }

                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> AutoSendInvoiceByEmail(string folder, JObject model)
        {
            JObject res = new JObject();

            string inv_invoiceauth_id = model["inv_invoiceauth_id"].ToString();
            string nguoinhan = model["nguoinhan"].ToString();
            string tieude = model["tieude"].ToString();
            string type = "Gửi hóa đơn";

            try
            {
                var emailTemplate = await this.GetEmailTemplate(type, inv_invoiceauth_id, _webHelper.GetDvcs(), "");

                model.Add("noidung", emailTemplate["body"].ToString());
                model.Add("nguoi_gui", emailTemplate["sender"].ToString());
                model.Add("mat_khau", emailTemplate["pass"].ToString());
                model.Add("bcc", "");
                model["tieude"] = emailTemplate["subject"].ToString();
                model.Add("alias", emailTemplate["alias"].ToString());

                await this.SendInvoiceByEmail(folder, model, _webHelper.GetDvcs(), type);

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> SendToCustomer(JObject model)
        {
            JObject res = new JObject();

            try
            {
                long invoice_id = Convert.ToInt64(model["invoice_id"].ToString());


                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("invoice_id", invoice_id);

                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id", CommandType.Text, dicParam);

                if (tblInvoices.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                string inv_invoiceauth_id = tblInvoices.Rows[0]["inv_invoiceauth_id"].ToString();
                string branch_code = tblInvoices.Rows[0]["branch_code"].ToString();

                string buyer_email = model["buyer_email"].ToString();
                string buyer_tel = model["buyer_tel"].ToString();

                if (buyer_email.Length > 0)
                {

                    dicParam.Clear();
                    dicParam.Add("branch_code", branch_code);

                    string sql = "SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type='Gửi hóa đơn' AND branch_code=@branch_code LIMIT 1 OFFSET 0";
                    DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);


                    dicParam.Clear();
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                    sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id)";
                    DataTable tblEmailBody = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                    DataRow row = tblEmailBody.Rows[0];

                    string noi_dung = tblTemp.Rows[0]["noi_dung"].ToString();
                    string subject = tblTemp.Rows[0]["subject"].ToString();

                    foreach (DataColumn column in tblEmailBody.Columns)
                    {
                        string value = "";

                        if (row[column.ColumnName] != DBNull.Value)
                        {
                            string dataType = column.DataType.Name.ToLower();

                            if (dataType.Contains("datetime"))
                            {
                                DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                value = string.Format("{0:dd/MM/yyyy}", dateTime);
                            }
                            else
                            {
                                value = row[column.ColumnName].ToString();
                            }
                        }

                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                        subject = subject.Replace("#" + column.ColumnName + "#", value);
                    }

                    await _emailService.SendAsync(buyer_email, subject, noi_dung);

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email(wb_log_email_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                         + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("branch_code", branch_code);
                    parameters.Add("send_date", DateTime.Now);
                    parameters.Add("from", _emailService.GetFrom());
                    parameters.Add("to", buyer_email);
                    parameters.Add("subject", subject);
                    parameters.Add("content", noi_dung);
                    parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                    parameters.Add("user_send", _webHelper.GetUser());
                    parameters.Add("send_type", "Gửi hóa đơn");

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                }

                if (buyer_tel.Length > 0)
                {
                    dicParam.Clear();
                    dicParam.Add("branch_code", branch_code);
                    string sql = "SELECT * FROM #SCHEMA_NAME#.pl_tempsms WHERE yesno='C' AND tempsms_type='Gửi hóa đơn' AND branch_code=@branch_code LIMIT 1 OFFSET 0";
                    DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                    if (tblTemp.Rows.Count > 0)
                    {
                        string noi_dung = tblTemp.Rows[0]["body"].ToString();
                        string subject = tblTemp.Rows[0]["subject"].ToString();

                        dicParam.Clear();
                        dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                        sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id)";
                        DataTable tblEmailBody = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                        DataRow row = tblEmailBody.Rows[0];

                        foreach (DataColumn column in tblEmailBody.Columns)
                        {
                            string value = "";

                            if (row[column.ColumnName] != DBNull.Value)
                            {
                                string dataType = column.DataType.Name.ToLower();

                                if (dataType.Contains("datetime"))
                                {
                                    DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                    value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                }
                                else
                                {
                                    value = row[column.ColumnName].ToString();
                                }
                            }

                            noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                            subject = subject.Replace("#" + column.ColumnName + "#", value);
                        }

                        sql = "INSERT INTO #SCHEMA_NAME#.wb_sms_queue(wb_sms_queue_id,mobile,subject,body,date_new,inv_invoiceauth_id,template_code,invoice_number,invoice_series,is_send,send_error,send_status) \n"
                            + "VALUES(@wb_sms_queue_id,@mobile,@subject,@body,@date_new,@inv_invoiceauth_id,@template_code,@invoice_number,@invoice_series,@is_send,@send_error,@send_status)";

                        Dictionary<string, object> dic = new Dictionary<string, object>();

                        dic.Add("wb_sms_queue_id", Guid.NewGuid());
                        dic.Add("mobile", buyer_tel);
                        dic.Add("subject", subject);
                        dic.Add("body", noi_dung);
                        dic.Add("date_new", DateTime.Now);
                        dic.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                        dic.Add("template_code", tblInvoices.Rows[0]["template_code"].ToString());
                        dic.Add("invoice_series", tblInvoices.Rows[0]["invoice_series"].ToString());
                        dic.Add("invoice_number", tblInvoices.Rows[0]["invoice_number"].ToString());
                        dic.Add("is_send", false);
                        //dic.Add("send_date", DateTime.Now);
                        dic.Add("send_error", "");
                        dic.Add("send_status", "");

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dic);
                    }
                }

                res.Add("ok", true);


            }
            catch (Exception ex)
            {
                res.Add("ok", false);
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JArray> GetDecimalPlace(string ma_nt)
        {
            //Int32 decimal_place = 2;
            JArray jarr = new JArray();
            try
            {
                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("code", ma_nt);

                DataTable dmnt = await this._minvoiceDbContext.GetDataTableAsync("SELECT quantity_decimal as quantity, price_decimal as unit_price, amount_without_vat_decimal as total_amount_without_vat, total_amount_decimal as total_amount FROM #SCHEMA_NAME#.sl_currency WHERE code = @code", CommandType.Text, dicParam);

                jarr = JArray.FromObject(dmnt);
                //decimal_place = Convert.ToInt32(dmnt.Rows[0]["decimal_place"]);
            }
            catch (Exception ex)
            {

            }
            return jarr;
        }

        public async Task<JObject> Amount_ToWord(string number, string type_amount)
        {
            JObject json = new JObject();
            try
            {
                if (type_amount == "USD")
                {
                    json.Add("data", FunctionUtil.ConvertUpper(FunctionUtil.USD_Amount(number)));
                }
                else
                {
                    json.Add("data", FunctionUtil.ConvertUpper(FunctionUtil.VND_Amount2(number)));
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }

        public async Task<JObject> GetUserTracuu(JObject model)
        {
            JObject json = new JObject();
            //TracuuHDDTContext tracuu = new TracuuHDDTContext();
            try
            {

                string mst = model["mst"].ToString();
                string ma_dt = model["code"].ToString();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("tax_code", mst);
                dicParam.Add("code_customer", ma_dt);

                DataTable tblUser = await _masterInvoiceDbContext.GetDataTableAsync($"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.inv_user WHERE tax_code=@tax_code AND code_customer=@code_customer", CommandType.Text, dicParam);

                if (tblUser.Rows.Count > 0)
                {
                    JArray array = JArray.FromObject(tblUser);
                    json = (JObject)array[0];
                }
                else
                {
                    json.Add("error", "Khách hàng " + ma_dt + " chưa có user tra cứu ! Bạn vui lòng nhập thông tin rồi ấn Lưu.");
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }
        public async Task<JObject> ResetPassUserTracuu(JObject model)
        {
            JObject json = new JObject();
            //TracuuHDDTContext tracuu = new TracuuHDDTContext();
            try
            {
                string mst = model["mst"].ToString();
                string ma_dt = model["ma_dt"].ToString();

                string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.inv_user WHERE tax_code=@tax_code AND code_customer=@code_customer ";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("tax_code", mst);
                parameters.Add("code_customer", ma_dt);

                DataTable tblUser = await _masterInvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);


                if (tblUser.Rows.Count == 0)
                {
                    json.Add("error", "Khách hàng này chưa tạo tài khoản !");
                    return json;
                    //json = JObject.FromObject(us);
                }
                else
                {
                    PassCommand pwcmd = new PassCommand("minvoice");

                    sql = $"UPDATE {CommonConstants.MASTER_SCHEMA}.inv_user SET password=@password WHERE inv_user_id=@inv_user_id ";

                    parameters.Clear();
                    parameters.Add("password", pwcmd.CreateHashedPassword("minvoice", null));
                    parameters.Add("inv_user_id", Guid.Parse(tblUser.Rows[0]["inv_user_id"].ToString()));

                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                    //us.password = pwcmd.CreateHashedPassword("minvoice", null);
                    //tracuu.Entry(us).State = System.Data.Entity.EntityState.Modified;
                }
                //tracuu.SaveChanges();

                json.Add("ok", "Reset pasword thành công !");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }
        public async Task<JObject> DeleteUserTracuu(JObject model)
        {
            JObject json = new JObject();
            //TracuuHDDTContext tracuu = new TracuuHDDTContext();
            try
            {
                string mst = model["mst"].ToString();
                string ma_dt = model["ma_dt"].ToString();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("tax_code", mst);
                dicParam.Add("code_customer", ma_dt);

                string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.inv_user WHERE tax_code=@tax_code AND code_customer=@code_customer";
                DataTable tblUser = await _masterInvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                if (tblUser.Rows.Count == 0)
                {
                    json.Add("error", "Khách hàng này chưa tạo tài khoản tra cứu !");
                    return json;
                }
                else
                {

                    dicParam.Clear();
                    dicParam.Add("inv_user_id", Guid.Parse(tblUser.Rows[0]["inv_user_id"].ToString()));
                    sql = $"DELETE FROM {CommonConstants.MASTER_SCHEMA}.inv_user WHERE inv_user_id=@inv_user_id";
                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql, dicParam);

                }

                json.Add("ok", "Xóa thành công !");

            }
            catch (Exception ex)
            {

                json.Add("error", ex.Message);
            }

            return json;
        }
        public async Task<JObject> CreateUser_tracuu(JObject model)
        {

            JObject json = new JObject();
            //TracuuHDDTContext tracuu = new TracuuHDDTContext();
            try
            {
                string username = model["username"].ToString();
                string mst = model["mst"].ToString();
                string ma_dt = model["ma_dt"].ToString();
                string email = model["email"].ToString();
                string schema_name = model["schema_name"].ToString();
                // thêm mới user tra cứu 

                //check user name xem đã tồn tại chưa ???

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("tax_code", mst);
                dicParam.Add("code_customer", ma_dt);

                DataTable tblUser = await _masterInvoiceDbContext.GetDataTableAsync($"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.inv_user WHERE tax_code=@tax_code AND code_customer=@code_customer", CommandType.Text, dicParam);

                if (tblUser.Rows.Count == 0)
                {
                    //if(us_check.us)
                    string pas = model["password"].ToString();
                    PassCommand pwcmd = new PassCommand(pas);

                    string sql = $"INSERT INTO {CommonConstants.MASTER_SCHEMA}.inv_user(inv_user_id, tax_code, code_customer, username, password, email, date_new, schema_name, schema_url) \n"
                            + "VALUES (@inv_user_id, @tax_code, @code_customer, @username, @password, @email, @date_new, @schema_name, @schema_url)";

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("inv_user_id", Guid.NewGuid());
                    parameters.Add("tax_code", mst);
                    parameters.Add("code_customer", ma_dt);
                    parameters.Add("username", username);
                    parameters.Add("password", pwcmd.CreateHashedPassword(pas, null));
                    parameters.Add("email", email);
                    parameters.Add("date_new", DateTime.Now);
                    parameters.Add("schema_name", schema_name);
                    parameters.Add("schema_url", await _minvoiceDbContext.GetConnectionAsyncCache(_minvoiceDbContext.GetTaxCodeSite()));

                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                }
                else
                { // sửa user tra cứu
                    string pas = model["password"].ToString();
                    PassCommand pwcmd = new PassCommand(pas);

                    string sql = $"UPDATE {CommonConstants.MASTER_SCHEMA}.inv_user SET username=@username,password=@password,email=@email,date_edit=@date_edit,schema_name=@schema_name \n"
                                + "WHERE inv_user_id=@inv_user_id";

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("inv_user_id", Guid.Parse(tblUser.Rows[0]["inv_user_id"].ToString()));
                    parameters.Add("username", username);
                    parameters.Add("password", pwcmd.CreateHashedPassword(pas, null));
                    parameters.Add("email", email);
                    parameters.Add("date_edit", DateTime.Now);
                    parameters.Add("schema_name", schema_name);

                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                }

                if (string.IsNullOrEmpty(email))
                {
                    json.Add("ok", "Tạo tài khoản tra cứu thành công ! Khách hàng không có email không thể gửi email.");
                }
                else
                {
                    DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='CREATE_CUSTOMER_LOGIN' AND yesno='C' LIMIT 1");

                    if (tblEmail.Rows.Count > 0)
                    {
                        string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                        if (smtp_address.Length > 0)
                        {
                            int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                            bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                            this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                        }

                        string subject = tblEmail.Rows[0]["subject"].ToString();
                        string body = tblEmail.Rows[0]["body"].ToString()
                                        .Replace("#username#", username)
                                        .Replace("#password#", model["password"].ToString())
                                        .Replace("#taxcode#", mst)
                                        .Replace("#url_host#", _webHelper.GetUrlHost());

                        this._emailService.Send(email, subject, body);

                        json.Add("ok", "Tạo tài khoản tra cứu thành công ! Gửi email cho khách thành công !");
                    }
                    else
                    {
                        json.Add("ok", "Tạo tài khoản tra cứu thành công !");
                    }

                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;
        }

        public async Task<JObject> SignInvoiceCertFile(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {

                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParam);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                Guid inv_invoiceauth_id = Guid.Parse(model["inv_invoiceauth_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username=@username";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                dicParam.Clear();
                dicParam.Add("username", username);

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }

                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);


                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoicecode_id, invoice_series, invoice_number FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);
                string inv_invoicecode_id = tblInvoice.Rows[0]["inv_invoicecode_id"].ToString();
                string invoice_series = tblInvoice.Rows[0]["invoice_series"]?.ToString();
                string invoice_number = tblInvoice.Rows[0]["invoice_number"]?.ToString();

                await _minvoiceDbContext.BeginTransactionAsync();

                dicParam.Clear();
                dicParam.Add("inv_notificationdetail_id", Guid.Parse(inv_invoicecode_id));

                sql = "SELECT * FROM #SCHEMA_NAME#.inv_notificationdetail WHERE inv_notificationdetail_id=@inv_notificationdetail_id FOR UPDATE";
                await _minvoiceDbContext.GetTransactionDataTableAsync(sql, CommandType.Text, dicParam);

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);

                DataTable tblData = await _minvoiceDbContext.GetTransactionDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id)", CommandType.Text, dicParam);

                string xml = tblData.Rows[0][0].ToString();

                //xml = xml.Replace("'", "''");

                if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                {
                    xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "Signatures", "xml", xml, tblCertificate.Rows[0]["cer_serial"].ToString(), true);
                }
                else
                {
                    xml = CommonManager.SignXml("#data", "seller", xml, certFile, "Signatures");
                }

                //xml = CommonManager.SignByMInvoice(xml, "Signatures");

                string sobaomat = await GenerateSecurity(6, 1);
                xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                dicParam.Add("xml", xml);
                dicParam.Add("fullname", tblUser.Rows[0]["fullname"].ToString());


                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id,@xml::xml,@fullname)";

                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dicParam);

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);

                sql = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                JArray array = JArray.FromObject(tblInvoices);

                res.Add("ok", true);
                res.Add("inv", (JObject)array[0]);
                res.Add("inv_InvoiceAuth_id", inv_invoiceauth_id);
                res.Add("inv_invoiceIssuedDate", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["invoice_issued_date"]));
                res.Add("inv_invoiceNumber", tblInvoices.Rows[0]["invoice_number"].ToString());
                res.Add("trang_thai", "Đã ký");

                dicParam.Clear();
                dicParam.Add("branch_code", branch_code);

                sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                if (tblSetting.Rows.Count > 0)
                {
                    string value = tblSetting.Rows[0]["value"].ToString();

                    if (value == "C")
                    {
                        string buyer_email = tblInvoices.Rows[0]["buyer_email"].ToString();

                        if (buyer_email.Length > 0)
                        {
                            JObject obj = new JObject();
                            obj.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            obj.Add("nguoinhan", buyer_email);
                            obj.Add("tieude", "Thông báo xuất hóa đơn điện tử");

                            string originalString = _webHelper.GetRequest().Url.OriginalString;
                            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                            var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                            await this.AutoSendInvoiceByEmail(folder, obj);
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex)
                {
                    _log.Error("SignInvoiceCertFile" + ex);
                }
            }

            return res;
        }
        public async Task<JObject> SignXmlEasyCA(JObject model)
        {
            JObject res = new JObject();
            string error = "";
            try
            {
                //---------------------- SignInvoiceCertFile ---------------
                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParam);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                Guid inv_invoiceauth_id = Guid.Parse(model["inv_invoiceauth_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);

                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoicecode_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id ", CommandType.Text, dicParam);
                string inv_invoicecode_id = tblInvoice.Rows[0]["inv_invoicecode_id"].ToString();

                await _minvoiceDbContext.BeginTransactionAsync();

                dicParam.Clear();
                dicParam.Add("inv_invoicecode_id", Guid.Parse(inv_invoicecode_id));

                var sql = "SELECT * FROM #SCHEMA_NAME#.inv_notificationdetail WHERE inv_notificationdetail_id=@inv_invoicecode_id FOR UPDATE";
                await _minvoiceDbContext.GetTransactionDataTableAsync(sql, CommandType.Text, dicParam);

                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);

                DataTable tblData = await _minvoiceDbContext.GetTransactionDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id )", CommandType.Text, dicParam);

                string xml = tblData.Rows[0][0].ToString();

                //xml = xml.Replace("'", "''");

                //------------------ End SignInvoiceCertFile ---------------------

                var invoiceDb = _nopDbContext.GetInvoiceDb();
                string ma_dvcs = _webHelper.GetDvcs();

                //string xml = model["InvoiceXmlData"].ToString();
                XmlDocument docXml = new XmlDocument();
                docXml.LoadXml(xml);
                var xmlHashSHA1 = docXml.SelectSingleNode("/HoaDonDienTu/HoaDon[@id='einvoice']");
                string xmlHash = xmlHashSHA1.InnerText;
                byte[] xmlBytes = System.Text.Encoding.UTF8.GetBytes(xmlHash);
                byte[] hash = null;
                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    hash = sha1.ComputeHash(xmlBytes);
                }
                string content = System.Convert.ToBase64String(hash);
                string _EasyCaURLAuth, _EasyCaURLSign, userName, passWord, key, pin, serial;
                var checkUrlDemo = await CheckURLSignByHSM();
                if (checkUrlDemo)
                {
                    _EasyCaURLAuth = ConfigurationManager.AppSettings["URL_AUTH_HSM_DEMO"];
                    _EasyCaURLSign = ConfigurationManager.AppSettings["URL_SIGN_HSM_DEMO"];
                    userName = ConfigurationManager.AppSettings["USERNAME_HSM_DEMO"];
                    passWord = ConfigurationManager.AppSettings["PASSWORD_HSM_DEMO"];
                    key = ConfigurationManager.AppSettings["KEY_HSM_DEMO"];
                    pin = ConfigurationManager.AppSettings["PIN_HSM_DEMO"];
                    serial = ConfigurationManager.AppSettings["SERIAL_HSM_DEMO"];
                }
                else
                {
                    _EasyCaURLAuth = ConfigurationManager.AppSettings["URL_AUTH_HSM"];
                    _EasyCaURLSign = ConfigurationManager.AppSettings["URL_SIGN_HSM"];
                    userName = ConfigurationManager.AppSettings["USERNAME_HSM"];
                    passWord = ConfigurationManager.AppSettings["PASSWORD_HSM"];
                    key = ConfigurationManager.AppSettings["KEY_HSM"];
                    pin = ConfigurationManager.AppSettings["PIN_HSM"];
                    serial = ConfigurationManager.AppSettings["SERIAL_HSM"];
                }


                JObject data = new JObject
                    {
                        {"username", userName}, // model["username"].ToString()
                        {"password", passWord} //model["password"].ToString()
                    };

                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3;
                string tokenResponse = client.PostAsync(_EasyCaURLAuth, new StringContent(data.ToString(), Encoding.UTF8, "application/json")).Result.Content.ReadAsStringAsync().Result;
                string token;
                JObject tokenObject = JObject.Parse(tokenResponse);
                if (tokenObject["id_token"] != null)
                {
                    token = tokenObject["id_token"].ToString();
                }
                else
                {
                    res.Add("error", tokenObject["title"].ToString() + " - " + tokenObject["path"].ToString() + " - " + tokenObject["message"].ToString());
                    return res;
                }

                JObject request = new JObject
                {
                    {
                        "elements", new JArray
                        {
                            new JObject
                            {
                                {"content", content},
                                {"key",  key}, //model["id"].ToString()
                                {"signDate", "" }, //DateTime.Now.ToString("yyyy-MM-dd")
                                {"signer", _webHelper.GetUser() } //_webHelper.GetUser()
                            }
                        }
                    },
                    {
                        "optional", new JObject
                        {
                            {"hashAlgorithm", "sha1"},
                            {"returnInputData" , true },
                            {"signAlgorithm",  "rsa" }
                        }
                    },
                    {
                        "tokenInfo", new JObject
                        {
                            {"pin" , pin}, //model["pin"].ToString()
                            {"serial" , serial} //model["so_serial"].ToString() //5404fffeb7033fb316d672201b80d398
                        }
                    }
                };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                string response = client.PostAsync(_EasyCaURLSign, new StringContent(request.ToString(), Encoding.UTF8, "application/json")).Result.Content.ReadAsStringAsync().Result;

                JObject result = JObject.Parse(response);
                if (result["data"] != null)
                {
                    string status = result["status"].ToString();
                    if (status.Equals("0"))
                    {
                        string dataResult = result["data"].ToString();
                        if (!string.IsNullOrEmpty(dataResult))
                        {
                            JArray signResult = (JArray)result["data"]["signResult"];
                            string certificate = result["data"]["certificate"].ToString();

                            string base64Signature = signResult[0]["base64Signature"].ToString();

                            XDocument doc = XDocument.Parse(xml);

                            // sql = $"SELECT TOP 1 ten_dvcs FROM  dbo.dmdvcs WHERE ma_dvcs ='{_webHelper.GetDvcs()}'";
                            //DataTable table = _nopDbContext.ExecuteCmd(sql);
                            X509Certificate2 cert = new X509Certificate2(Convert.FromBase64String(certificate));

                            string tenDonVi = "";// table.Rows[0]["ten_dvcs"].ToString();
                            //xml = CommonManager.CreateSignature(doc, cert.Subject, base64Signature, certificate, tenDonVi, xmlHash);
                            //xml = CommonManager.SignXml("#data", "seller", xml, certFile, "Signatures");
                            xml = CommonManager.CreateSignature("#data", "seller", xml, cert.Subject, base64Signature, certificate, tenDonVi, content, "Signatures");

                            //res.Add("InvoiceXmlData", xmlSign);

                            //---------------- SignInvoiceCertFile -----------------

                            string sobaomat = await GenerateSecurity(6, 1);
                            xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                            dicParam.Clear();
                            dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            dicParam.Add("xml", xml);
                            dicParam.Add("fullName", tblUser.Rows[0]["fullname"].ToString());


                            if (CommonManager.CheckThoiGian(xml) == false)
                            {
                                //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                                //return res;
                                throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            }

                            sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id,@xml::xml',@fullName)";

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dicParam);

                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();

                            dicParam.Clear();
                            dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);

                            sql = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id ";
                            DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                            JArray array = JArray.FromObject(tblInvoices);

                            res.Add("ok", true);
                            res.Add("inv", (JObject)array[0]);
                            res.Add("inv_InvoiceAuth_id", inv_invoiceauth_id);
                            res.Add("inv_invoiceIssuedDate", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["invoice_issued_date"]));
                            res.Add("inv_invoiceNumber", tblInvoices.Rows[0]["invoice_number"].ToString());
                            res.Add("trang_thai", "Đã ký");

                            dicParam.Clear();
                            dicParam.Add("branch_code", branch_code);

                            sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                            DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                            if (tblSetting.Rows.Count > 0)
                            {
                                string value = tblSetting.Rows[0]["value"].ToString();

                                if (value == "C")
                                {
                                    string buyer_email = tblInvoices.Rows[0]["buyer_email"].ToString();

                                    if (buyer_email.Length > 0)
                                    {
                                        JObject obj = new JObject();
                                        obj.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                                        obj.Add("nguoinhan", buyer_email);
                                        obj.Add("tieude", "Thông báo xuất hóa đơn điện tử");

                                        string originalString = _webHelper.GetRequest().Url.OriginalString;
                                        string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                                        var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                                        await this.AutoSendInvoiceByEmail(folder, obj);
                                    }


                                }
                            }
                            //--------------- END SignInvoiceCertFile ---------------------
                        }
                    }
                    else
                    {
                        res.Add("error", result["status"].ToString() + " - " + result["msg"].ToString());
                    }
                }
                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception e)
            {
                error = e.Message;
                _log.Error("SignXmlEasyCA" + e);
            }
            if (error.Length > 0)
            {
                res.Add("error", error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex)
                {
                    _log.Error("SignXmlEasyCA" + ex);
                }
            }

            return res;
        }
        private async Task<bool> CheckURLSignByHSM()
        {
            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("branch_code", _webHelper.GetDvcs());

            var sql = "SELECT value FROM #SCHEMA_NAME#.wb_setting WHERE branch_code=@branch_code AND code='SIGN_BY_HSM_DEMO'";
            var tblSetingHsm = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);
            if (tblSetingHsm.Rows.Count > 0)
            {
                return tblSetingHsm.Rows[0]["value"].ToString().ToUpper().Equals("C");
            }
            else
                return false;
        }
        public async Task<bool> CheckSignByHSM()
        {
            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("branch_code", _webHelper.GetDvcs());

            var sql = "SELECT value FROM #SCHEMA_NAME#.wb_setting WHERE branch_code=@branch_code AND code='SIGN_BY_HSM'";
            var tblSetingHsm = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);
            if (tblSetingHsm.Rows.Count > 0)
            {
                return tblSetingHsm.Rows[0]["value"].ToString().ToUpper().Equals("C");
            }
            else
                return false;
        }

        public async Task<JObject> SignListInvoiceCertFile(JObject model)
        {
            JObject res = new JObject();
            string error = "";
            bool isOpenConnection = false;

            try
            {

                string username = _webHelper.GetUser();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("username", username);


                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParam);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                string cert_serial = model["so_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username=@username"
                        + " AND a.cer_serial=@cer_serial"
                        + " ORDER BY end_date desc LIMIT 1";

                dicParam.Clear();
                dicParam.Add("username", username);
                dicParam.Add("cer_serial", cert_serial);

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                }

                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                JArray invs = (JArray)model["invs"];

                for (int i = 0; i < invs.Count; i++)
                {
                    string inv_invoiceauth_id = invs[i].ToString();

                    dicParam.Clear();
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                    DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoicecode_id, invoice_series, invoice_number FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);
                    string inv_invoicecode_id = tblInvoice.Rows[0]["inv_invoicecode_id"].ToString();
                    string invoice_series = tblInvoice.Rows[0]["invoice_series"]?.ToString();
                    string invoice_number = tblInvoice.Rows[0]["invoice_number"]?.ToString();

                    dicParam.Clear();
                    dicParam.Add("inv_notificationdetail_id", Guid.Parse(inv_invoicecode_id));

                    await _minvoiceDbContext.BeginTransactionAsync();
                    isOpenConnection = true;

                    sql = "SELECT * FROM #SCHEMA_NAME#.inv_notificationdetail WHERE inv_notificationdetail_id=@inv_notificationdetail_id FOR UPDATE";
                    await _minvoiceDbContext.GetTransactionDataTableAsync(sql, CommandType.Text, dicParam);

                    dicParam.Clear();
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                    DataTable tblData = await _minvoiceDbContext.GetTransactionDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id)", CommandType.Text, dicParam);
                    string xml = tblData.Rows[0][0].ToString();

                    if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                    {
                        xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "Signatures", "xml", xml, tblCertificate.Rows[0]["cer_serial"].ToString(), true);
                    }
                    else
                    {
                        xml = CommonManager.SignXml("#data", "seller", xml, certFile, "Signatures");
                    }

                    //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                    //xml = xml.Replace("'", "''");

                    string sobaomat = await GenerateSecurity(6, 1);
                    xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                    dicParam.Clear();
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                    dicParam.Add("xml", xml);
                    dicParam.Add("fullname", tblUser.Rows[0]["fullname"].ToString());

                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }

                    sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id,@xml::xml,@fullname)";
                    // JObject parameters = null;
                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dicParam);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    isOpenConnection = false;

                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                res.Add("error", error);
            }

            if (isOpenConnection)
            {
                try
                {
                    if (error.Length > 0)
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                    }

                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }

            }

            return res;
        }

        public async Task<string> GetXmlSignInvoices(string inv_InvoiceCode_id, DateTime tu_ngay, DateTime den_ngay, string inv_invoiceAuth_id)
        {

            byte[] data = System.Convert.FromBase64String(inv_invoiceAuth_id);
            string base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);

            string xml = "";

            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("tu_ngay", tu_ngay);
            dicParam.Add("den_ngay", den_ngay);
            dicParam.Add("base64Decoded", base64Decoded);
            dicParam.Add("inv_invoicecode_id", Guid.Parse(inv_InvoiceCode_id));

            string sql = "SELECT '<DanhSach>'|| string_agg(a.detail_xml,',') || '</DanhSach>' FROM ( \n"
                            + " SELECT inv_invoiceauth_id,CAST(#SCHEMA_NAME#.qr_export_xmlinvoice_vn('#SCHEMA_NAME#',inv_invoiceauth_id) as character varying) as detail_xml \n"
                            + " FROM #SCHEMA_NAME#.inv_invoiceauth WHERE status='Chờ ký' AND invoice_issued_date>=@tu_ngay"
                                    + "' AND invoice_issued_date<=@den_ngay AND inv_invoiceauth_id IN (SELECT unnest(string_to_array(@base64Decoded, ',')::uuid[])) \n"
                                    + " AND inv_invoicecode_id=@inv_invoicecode_id  \n"
                        + ") a";


            DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

            if (tblInvoices.Rows.Count > 0)
            {
                xml = tblInvoices.Rows[0][0].ToString();
            }

            return xml;
        }

        public async Task<JArray> GetListInvoiceSeries(JObject model)
        {
            string username = model["username"].ToString();
            string main = model["main"].ToString();

            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("username", username);

            DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParam);

            if (tblUser.Rows.Count == 0)
            {
                return new JArray();
            }

            string branch_code = tblUser.Rows[0]["branch_code"].ToString();
            string store_code = tblUser.Rows[0]["store_code"].ToString();

            dicParam.Clear();
            dicParam.Add("code", branch_code);

            DataTable tblBranch = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@code", CommandType.Text, dicParam);
            string wb_branch_id = tblBranch.Rows[0]["wb_branch_id"].ToString();

            dicParam.Clear();
            dicParam.Add("wb_branch_id", Guid.Parse(wb_branch_id));
            dicParam.Add("username", username);
            dicParam.Add("main", Convert.ToInt32(main));

            string sql = "SELECT a.* FROM #SCHEMA_NAME#.inv_notificationdetail a \n"
                        + "INNER JOIN #SCHEMA_NAME#.inv_notification_permission b ON a.inv_notificationdetail_id=b.inv_notificationdetail_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON  b.wb_user_id=c.wb_user_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.inv_notification_branch d ON a.inv_notificationdetail_id=d.inv_notificationdetail_id AND d.wb_branch_id=@wb_branch_id"
                        + "WHERE  c.username=@username  AND COALESCE(a.is_end,false)=false AND d.ord=@main  ORDER BY a.begin_date";

            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);
            JArray array = JArray.FromObject(tblData);

            return array;

        }

        public async Task<byte[]> PrintAdjustmentReport(JObject model)
        {
            try
            {
                string inv_invoiceauth_id = model["inv_invoiceauth_id"].ToString();
                string ad_number = model["ad_number"].ToString();
                DateTime issued_date = Convert.ToDateTime(model["issued_date"].ToString());
                string reason = model["reason"].ToString();
                string ad_before = model["ad_before"].ToString();
                string ad_after = model["ad_after"].ToString();

                string sql = "SELECT inv_invoiceauth_id,invoice_number,invoice_series,invoice_issued_date,inv_invoicecode_id FROM #SCHEMA_NAME#.inv_invoiceauth \n"
                                + "WHERE inv_invoiceauth_id=@P_0";

                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, Guid.Parse(inv_invoiceauth_id));

                if (tblData.Rows.Count == 0)
                {
                    throw new Exception("Không tìm thấy hóa đơn");
                }

                Guid inv_invoicecode_id = Guid.Parse(tblData.Rows[0]["inv_invoicecode_id"].ToString());
                string invoice_number = tblData.Rows[0]["invoice_number"].ToString();

                sql = "SELECT * FROM #SCHEMA_NAME#.inv_adjustment WHERE inv_invoicecode_id=@P_0 AND invoice_number=@P_1";
                tblData = await _minvoiceDbContext.GetDataTableAsync(sql, inv_invoicecode_id, invoice_number);

                Guid id = Guid.NewGuid();

                string dvcs = _webHelper.GetDvcs();
                string user_login = _webHelper.GetUser();
                sql = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@P_0";
                var inv_invoiceauth_id_parse = Guid.Parse(inv_invoiceauth_id);
                var tblInvoiceAuthData = await _minvoiceDbContext.GetDataTableAsync(sql, inv_invoiceauth_id_parse);

                if (tblInvoiceAuthData.Rows.Count > 0 & tblInvoiceAuthData.Rows[0]["status"].ToString() == "Đã ký")
                {
                    if (tblData.Rows.Count == 0)
                    {
                        sql = "INSERT INTO #SCHEMA_NAME#.inv_adjustment(inv_adjustment_id, ad_number, issued_date, invoice_number,reason, ad_before, ad_after, branch_code, user_new, date_new, inv_invoicecode_id, inv_invoiceauth_id) \n"
                            + "VALUES (@inv_adjustment_id, @ad_number, @issued_date, @invoice_number,@reason, @ad_before, @ad_after, @branch_code, @user_new, @date_new, @inv_invoicecode_id,@inv_invoiceauth_id)";

                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("inv_adjustment_id", id);
                        parameters.Add("ad_number", ad_number);
                        parameters.Add("issued_date", issued_date);
                        parameters.Add("invoice_number", invoice_number);
                        parameters.Add("reason", reason);
                        parameters.Add("ad_before", ad_before);
                        parameters.Add("ad_after", ad_after);
                        parameters.Add("branch_code", dvcs);
                        parameters.Add("user_new", user_login);
                        parameters.Add("date_new", DateTime.Now);
                        parameters.Add("inv_invoicecode_id", inv_invoicecode_id);
                        parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    }
                    else
                    {
                        DataRow row = tblData.Rows[0];
                        id = Guid.Parse(row["inv_adjustment_id"].ToString());

                        row.BeginEdit();
                        row["inv_invoiceauth_id"] = Guid.Parse(inv_invoiceauth_id);
                        row["ad_number"] = ad_number;
                        row["issued_date"] = issued_date;
                        row["invoice_number"] = invoice_number;
                        row["inv_invoicecode_id"] = inv_invoicecode_id;
                        row["reason"] = reason;
                        row["ad_before"] = ad_before;
                        row["ad_after"] = ad_after;
                        row["user_edit"] = user_login;
                        row["date_edit"] = DateTime.Now;
                        row["inv_adjustment_id"] = id;

                        sql = "UPDATE #SCHEMA_NAME#.inv_adjustment SET ad_number=@ad_number,issued_date=@issued_date,invoice_number=@invoice_number \n"
                                + ",reason=@reason,ad_before=@ad_before,ad_after=@ad_after,inv_invoicecode_id=@inv_invoicecode_id,user_edit=@user_edit,date_edit=@date_edit,inv_invoiceauth_id=@inv_invoiceauth_id \n"
                                + "WHERE inv_adjustment_id=@inv_adjustment_id";

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, row);
                    }
                    //CẬP NHẬT TRẠNG THÁI HOÁ ĐƠN BỊ ĐIỀU CHỈNH

                    if (tblInvoiceAuthData.Rows.Count > 0)
                    {
                        DataRow row_invoice_status = tblInvoiceAuthData.Rows[0];
                        row_invoice_status["invoice_status"] = 11;
                        row_invoice_status["inv_invoiceauth_id"] = inv_invoiceauth_id_parse;
                        sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET invoice_status = @invoice_status WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, row_invoice_status);
                    }
                    //END
                }

                sql = "SELECT * FROM #SCHEMA_NAME#.qr_inct_adjustment('#SCHEMA_NAME#',@P_0)";
                tblData = await _minvoiceDbContext.GetDataTableAsync(sql, id);

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                //IN HOÁ ĐƠN
                string xml = "";
                DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT data FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);

                if (tblInvoiceXmlData.Rows.Count > 0)
                {
                    xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                }

                // QuyenNH modified
                xml = await ExportInvoiceXml(xml, Guid.Parse(inv_invoiceauth_id));

                xml = FunctionUtil.ReverseXml(xml);
                XtraReport report = new XtraReport();
                // LẤY THÔNG TIN sl_invoice_template_id
                dicParam.Clear();
                dicParam.Add("inv_notificationdetail_id", inv_invoicecode_id);

                DataTable tblNTThongBao = await _minvoiceDbContext.GetDataTableAsync("SELECT a.sl_invoice_template_id,b.thousand_point as hang_nghin,b.decimal_point as thap_phan FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id WHERE a.inv_notificationdetail_id=@inv_notificationdetail_id", CommandType.Text, dicParam);

                string hang_nghin = tblNTThongBao.Rows[0]["hang_nghin"].ToString();
                string thap_phan = tblNTThongBao.Rows[0]["thap_phan"].ToString();

                dicParam.Clear();
                dicParam.Add("sl_invoice_template_id", Guid.Parse(tblNTThongBao.Rows[0]["sl_invoice_template_id"].ToString()));

                DataTable tblDMMauHoaDon = await _minvoiceDbContext.GetDataTableAsync("SELECT report FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id ", CommandType.Text, dicParam);
                string invReport = tblDMMauHoaDon.Rows[0]["report"].ToString();
                if (invReport.Length > 0)
                {
                    report = ReportUtil.LoadReportFromString(invReport);
                }
                else
                {
                    throw new Exception("Không tải được mẫu hóa đơn");
                }
                report.Name = "XtraReport1";
                report.ScriptReferencesString = "AccountSignature.dll";

                // DATA
                DataSet ds = new DataSet();
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                {
                    ds.ReadXmlSchema(xmlReader);
                    xmlReader.Close();
                }
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                {
                    ds.ReadXml(xmlReader);
                    xmlReader.Close();
                }
                if (ds.Tables.Contains("TblXmlData"))
                {
                    ds.Tables.Remove("TblXmlData");
                }

                DataTable tblXmlData = new DataTable
                {
                    TableName = "TblXmlData"
                };
                tblXmlData.Columns.Add("data");

                DataRow rowXmlData = tblXmlData.NewRow();
                rowXmlData["data"] = xml;
                tblXmlData.Rows.Add(rowXmlData);

                ds.Tables.Add(tblXmlData);
                report.DataSource = ds;
                var t = Task.Run(() =>
                {
                    var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    newCulture.NumberFormat.NumberDecimalSeparator = thap_phan;
                    newCulture.NumberFormat.NumberGroupSeparator = hang_nghin;

                    System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;

                    report.CreateDocument();
                });
                t.Wait();

                // BIÊN BẢN

                int pageCount = report.Pages.Count;
                string reportPath = ReportUtil.MapPath("~/Content/report/DieuChinhDinhDanh.repx");


                XtraReport rpBienBan = XtraReport.FromFile(reportPath, true);
                //byte[] bytes = ReportUtil.PrintReport(dataSet, reportPath, "Pdf");

                rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                rpBienBan.Name = "rpDieuChinhDinhDanh";
                rpBienBan.DisplayName = "DieuChinhDinhDanh.repx";

                rpBienBan.DataSource = tblData;
                DataSet dataSet = new DataSet();
                dataSet.Tables.Add(tblData);
                if (dataSet.Tables.Count > 0)
                {
                    rpBienBan.DataMember = dataSet.Tables[0].TableName;
                }

                rpBienBan.CreateDocument();

                rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                report.PrintingSystem.ContinuousPageNumbering = false;

                report.Pages.AddRange(rpBienBan.Pages);

                int idx = pageCount;
                pageCount = report.Pages.Count;

                for (int i = idx; i < pageCount; i++)
                {
                    PageWatermark pmk = new PageWatermark
                    {
                        ShowBehind = false
                    };
                    report.Pages[i].AssignWatermark(pmk);
                }


                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);

                byte[] bytes = ms.ToArray();

                ms.Close();

                if (bytes == null)
                {
                    throw new Exception("null");
                }

                return bytes;
                //END
            }
            catch (Exception ex)
            {
                _log.Error($"PrintInvoiceFromId : {ex}");
                throw new Exception(ex.Message);
            }

        }

        public async Task<JObject> Create(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                string username = model["username"].ToString();

                string template_code = model["template_code"] == null ? "" : model["template_code"].ToString();
                string invoice_series = model["invoice_series"] == null ? "" : model["invoice_series"].ToString();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParam);

                if (tblUser.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy tên truy cập");
                    return res;
                }

                string branch_code = tblUser.Rows[0]["branch_code"].ToString();
                string store_code = tblUser.Rows[0]["store_code"].ToString();

                dicParam.Clear();
                dicParam.Add("code", branch_code);

                DataTable tblBranch = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@code", CommandType.Text, dicParam);
                string wb_branch_id = tblBranch.Rows[0]["wb_branch_id"].ToString();

                dicParam.Clear();
                dicParam.Add("wb_branch_id", Guid.Parse(wb_branch_id));
                dicParam.Add("username", username);

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.inv_notificationdetail a \n"
                        + "INNER JOIN #SCHEMA_NAME#.inv_notification_permission b ON a.inv_notificationdetail_id=b.inv_notificationdetail_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON  b.wb_user_id=c.wb_user_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.inv_notification_branch d ON a.inv_notificationdetail_id=d.inv_notificationdetail_id AND d.wb_branch_id=@wb_branch_id"
                        + "WHERE  c.username=@username  AND COALESCE(a.is_end,false)=false AND d.ord IS NOT NULL AND d.ord<>3 ";

                //if (branch_code.Length > 0)
                //{
                //    sql = sql + " AND a.branch_code='" + branch_code + "' ";
                //}

                if (template_code.Length > 0)
                {
                    dicParam.Add("template_code", template_code);
                    sql += " AND a.template_code=@template_code ";
                }

                if (invoice_series.Length > 0)
                {
                    dicParam.Add("invoice_series", invoice_series);
                    sql += " AND a.invoice_series=@invoice_series";
                }

                sql = sql + " ORDER BY d.ord,a.begin_date";

                DataTable tblNotificationDetail = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParam);

                if (tblNotificationDetail.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy mẫu số ký hiệu hoặc người sử dụng chưa được phân quyền hoặc đã hết số hóa đơn");
                    return res;
                }

                Guid inv_invoicecode_id = Guid.Parse(tblNotificationDetail.Rows[0]["inv_notificationdetail_id"].ToString());

                template_code = tblNotificationDetail.Rows[0]["template_code"].ToString();
                invoice_series = tblNotificationDetail.Rows[0]["invoice_series"].ToString();

                string invoice_type = tblNotificationDetail.Rows[0]["code"].ToString();
                string invoice_name = tblNotificationDetail.Rows[0]["name"].ToString();

                dicParam.Clear();
                dicParam.Add("branch_code", branch_code);

                DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync("SELECT value FROM #SCHEMA_NAME#.wb_setting WHERE code='QUY_TRINH_HD' AND branch_code=@branch_code", CommandType.Text, dicParam);
                string status = tblSetting.Rows[0][0].ToString();

                Guid inv_invoiceauth_id = Guid.NewGuid();

                JObject inv = (JObject)model["inv"];

                string original_invoice_id = "";
                string additional_reference_des = "";
                string additional_reference_date = "";
                string additional_reference_note = "";
                string adjustment_number = "";
                string integrated_number = "";

                if (inv["adjust_invoice"] != null)
                {
                    JObject adInvoice = (JObject)inv["adjust_invoice"];

                    if (adInvoice["template_code"].ToString() != template_code || adInvoice["invoice_series"].ToString() != template_code)
                    {
                        res.Add("error", "Số hóa đơn điều chỉnh không cùng mẫu số ký hiệu với dải được chọn");
                        return res;
                    }

                    dicParam.Clear();
                    dicParam.Add("template_code", template_code);
                    dicParam.Add("invoice_series", invoice_series);
                    dicParam.Add("invoice_number", adInvoice["invoice_number"].ToString());

                    DataTable tbladInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM inv_invoiceauth WHERE template_code=@template_code  AND invoice_series=@invoice_series  AND invoice_number=@invoice_number", CommandType.Text, dicParam);

                    original_invoice_id = tbladInvoice.Rows[0]["inv_invoiceauth_id"].ToString();
                    additional_reference_des = adInvoice["additional_reference_des"].ToString();
                    additional_reference_note = adInvoice["additional_reference_note"].ToString();
                    additional_reference_date = adInvoice["additional_reference_date"].ToString();
                    adjustment_number = adInvoice["adjustment_number"].ToString();
                    integrated_number = adInvoice["invoice_number"].ToString();
                }

                if (inv["inv_invoiceauth_id"] != null)
                {
                    inv_invoiceauth_id = Guid.Parse(inv["inv_invoiceauth_id"].ToString());
                }
                else
                {
                    inv.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                }

                if (store_code.Length > 0)
                {
                    if (inv["store_code"] != null)
                    {
                        if (inv["store_code"].ToString() == "")
                        {
                            inv["store_code"] = store_code;
                        }
                    }
                    else
                    {
                        inv.Add("store_code", store_code);
                    }

                }

                if (inv["adjustment_type"] == null)
                {
                    inv.Add("adjustment_type", 1);
                }

                if (inv["invoice_status"] == null)
                {
                    inv.Add("invoice_status", Convert.ToInt32(inv["adjustment_type"]));
                }

                if (inv["branch_code"] != null)
                {
                    inv["branch_code"] = branch_code;
                }
                else
                {
                    inv.Add("branch_code", branch_code);
                }


                inv.Add("invoice_type", invoice_type);
                inv.Add("invoice_name", invoice_name);
                inv.Add("inv_invoicecode_id", inv_invoicecode_id);
                inv.Add("invoice_series", invoice_series);
                inv.Add("template_code", template_code);

                if (inv["currency_code"] == null)
                {
                    inv.Add("currency_code", "VND");
                }

                if (inv["exchange_rate"] == null)
                {
                    inv.Add("exchange_rate", 1);
                }

                if (inv["status"] == null)
                {
                    inv.Add("status", status);
                }

                inv.Add("additional_reference_des", additional_reference_des);
                inv.Add("additional_reference_note", additional_reference_note);

                if (additional_reference_note.Length > 0 && inv["invoice_note"] == null)
                {
                    inv.Add("invoice_note", additional_reference_note);
                }

                if (additional_reference_date.Length > 0)
                {
                    inv.Add("additional_reference_date", Convert.ToDateTime(additional_reference_date));
                }

                if (original_invoice_id.Length > 0)
                {
                    inv.Add("original_invoice_id", Guid.Parse(original_invoice_id));
                }

                if (adjustment_number.Length > 0)
                {
                    inv.Add("adjustment_number", adjustment_number);
                }

                if (integrated_number.Length > 0)
                {
                    inv.Add("integrated_number", integrated_number);
                }

                await _minvoiceDbContext.BeginTransactionAsync();
                await _minvoiceDbContext.ExecuteNoneQueryAsync("crd_inv_invoiceauth_insert", CommandType.StoredProcedure, inv);

                JArray details = (JArray)inv["details"];

                for (int i = 0; i < details.Count; i++)
                {
                    JObject obj = (JObject)details[i];

                    if (obj["inv_invoiceauthdetail_id"] == null)
                    {
                        obj.Add("inv_invoiceauthdetail_id", Guid.NewGuid());
                    }

                    if (obj["inv_invoiceauth_id"] == null)
                    {
                        obj.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                    }
                    else
                    {
                        obj["inv_invoiceauth_id"] = inv_invoiceauth_id;
                    }

                    if (obj["row_ord"] == null)
                    {
                        obj.Add("row_ord", i.ToString().PadLeft(10, '0'));
                    }
                    else
                    {
                        obj["row_ord"] = obj["row_ord"].ToString().PadLeft(10, '0');
                    }


                    await _minvoiceDbContext.ExecuteNoneQueryAsync("crd_inv_invoiceauthdetail_insert", CommandType.StoredProcedure, obj);
                }

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();


                dicParam.Clear();
                dicParam.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoiceauth_id,invoice_number,invoice_series,template_code,invoice_issued_date FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id", CommandType.Text, dicParam);

                if (tblInvoice.Rows.Count > 0)
                {
                    inv = new JObject();
                    inv.Add("inv_invoiceauth_id", tblInvoice.Rows[0]["inv_invoiceauth_id"].ToString());
                    inv.Add("invoice_number", tblInvoice.Rows[0]["invoice_number"].ToString());
                    inv.Add("invoice_series", tblInvoice.Rows[0]["invoice_series"].ToString());
                    inv.Add("template_code", tblInvoice.Rows[0]["template_code"].ToString());
                    inv.Add("invoice_issued_date", string.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(tblInvoice.Rows[0]["invoice_issued_date"].ToString())));

                    res.Add("ok", true);
                    res.Add("inv", inv);
                }
                else
                {
                    res.Add("error", "Không tạo được hóa đơn");
                }


            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async Task<JObject> XoaBoHD(JObject model)
        {
            JObject res = new JObject();

            try
            {
                string branch_code = model["branch_code"] == null ? "" : model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                if (branch_code.Length == 0)
                {
                    branch_code = tblUser.Rows[0]["branch_code"].ToString();
                }


                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }

                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                string template_code = model["template_code"].ToString();
                string invoice_series = model["invoice_series"].ToString();
                string invoice_number = model["invoice_number"].ToString();
                string inv_invoiceauth_id01 = model["inv_invoiceauth_id"] == null ? "" : model["inv_invoiceauth_id"].ToString();


                Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                dicParameter.Add("username", username);

                dicParameter.Clear();
                dicParameter.Add("template_code", template_code);
                dicParameter.Add("invoice_series", invoice_series);
                dicParameter.Add("invoice_number", invoice_number);

                sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE template_code=@template_code  AND invoice_series=@invoice_series  AND invoice_number=@invoice_number";

                if (branch_code.Length > 0)
                {
                    dicParameter.Add("branch_code", branch_code);
                    sql += " AND branch_code=@branch_code";
                }

                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                if (tblInvoice.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                Guid inv_invoiceauth_id = Guid.Parse(tblInvoice.Rows[0]["inv_invoiceauth_id"].ToString());

                if (model["id"] == null)
                {
                    model.Add("id", inv_invoiceauth_id);
                }

                JObject delInv = await GetInvoiceXoaBo(model);

                if (delInv["error"] != null)
                {
                    res.Add("error", delInv["error"].ToString());
                    return res;
                }

                string xml = delInv["InvoiceXmlData"].ToString();
                string _delInv_id = delInv["inv_invoiceauth_id"].ToString();

                if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                {
                    xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "Signatures", "xml", xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                }
                else
                {
                    xml = CommonManager.SignXml("#data", "seller", xml, certFile, "Signatures");
                }

                //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                delInv.Add("serialNumber", "");
                delInv["InvoiceXmlData"] = xml;

                delInv = await SaveInvoiceXoaBo(delInv);

                if (delInv["error"] != null)
                {
                    res.Add("error", delInv["error"].ToString());
                }
                else
                {
                    res.Add("ok", true);
                    res.Add("inv_invoiceauth_id", _delInv_id);
                }

            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> SaveXmlSign(JObject parameters)
        {
            JObject obj = new JObject();
            bool isOpenConnection = false;
            string error = "";

            try
            {
                string username = _webHelper.GetUser();

                Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                dicParameter.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParameter);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    obj.Add("error", "Bạn không có quyền ký hóa đơn");
                    return obj;
                }

                string certSerial = parameters["cerSerial"].ToString();

                dicParameter.Clear();
                dicParameter.Add("cer_serial", certSerial);

                DataTable tblCert = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE cer_serial=@cer_serial", CommandType.Text, dicParameter);

                string nguoiky = tblCert.Rows[0]["signer"].ToString();

                JArray array = (JArray)parameters["data"];

                for (int i = 0; i < array.Count; i++)
                {
                    JObject jobject = (JObject)array[i];

                    string inv_invoiceauth_id = jobject["inv_InvoiceAuth_id"].ToString();

                    string xml = jobject["InvoiceXmlData"].ToString();
                    xml = FunctionUtil.ConvertXml(xml);
                    //xml = xml.Replace("'", "''");
                    await _minvoiceDbContext.BeginTransactionAsync();
                    isOpenConnection = true;

                    dicParameter.Clear();
                    dicParameter.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                    string sql = "SELECT inv_invoicecode_id,branch_code,buyer_email FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                    DataTable tblInvoice = await _minvoiceDbContext.GetTransactionDataTableAsync(sql, CommandType.Text, dicParameter);

                    string inv_invoicecode_id = tblInvoice.Rows[0]["inv_invoicecode_id"].ToString();
                    string branch_code = tblInvoice.Rows[0]["branch_code"].ToString();
                    string buyer_email = tblInvoice.Rows[0]["buyer_email"].ToString();

                    dicParameter.Clear();
                    dicParameter.Add("inv_notificationdetail_id", Guid.Parse(inv_invoicecode_id));

                    sql = "SELECT * FROM #SCHEMA_NAME#.inv_notificationdetail WHERE inv_notificationdetail_id=@inv_notificationdetail_id FOR UPDATE";
                    await _minvoiceDbContext.GetTransactionDataTableAsync(sql, CommandType.Text, dicParameter);

                    string sobaomat = await GenerateSecurity(6, 1);
                    xml = CommonManager.ExportSaveInvoiceXml(xml, sobaomat);

                    dicParameter.Clear();
                    dicParameter.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                    dicParameter.Add("xml", xml);
                    dicParameter.Add("nguoiky", nguoiky);


                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //obj.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return obj;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }

                    sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice_vn('#SCHEMA_NAME#',@inv_invoiceauth_id,@xml::xml,@nguoiky)";

                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dicParameter);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    isOpenConnection = false;

                    dicParameter.Clear();
                    dicParameter.Add("branch_code", branch_code);

                    sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                    DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                    if (tblSetting.Rows.Count > 0)
                    {
                        string value = tblSetting.Rows[0]["value"].ToString();

                        if (value == "C")
                        {

                            if (buyer_email.Length > 0)
                            {
                                JObject item = new JObject();
                                obj.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                                obj.Add("nguoinhan", buyer_email);
                                obj.Add("tieude", "Thông báo xuất hóa đơn điện tử");

                                string originalString = _webHelper.GetRequest().Url.OriginalString;
                                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                                await this.AutoSendInvoiceByEmail(folder, obj);
                            }


                        }
                    }
                }

                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                error = ex.Message;
                obj.Add("error", ex.Message);
            }

            if (isOpenConnection)
            {
                try
                {
                    if (error.Length > 0)
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                    }

                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return obj;
        }

        public async Task<JObject> DeleteByCertFile(JObject model)
        {
            JObject res = new JObject();

            try
            {
                string branch_code = model["branch_code"] == null ? "" : model["branch_code"].ToString();
                string username = model["username"].ToString();

                Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                dicParameter.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParameter);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                if (branch_code.Length == 0)
                {
                    branch_code = tblUser.Rows[0]["branch_code"].ToString();
                }


                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                dicParameter.Clear();
                dicParameter.Add("username", username);

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username=@username";

                if (branch_code.Length > 0)
                {
                    dicParameter.Add("branch_code", branch_code);
                    sql += " AND a.branch_code=@branch_code ";
                }

                if (cert_serial.Length > 0)
                {
                    dicParameter.Add("cert_serial", cert_serial);
                    sql += " AND a.cer_serial=@cert_serial";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }

                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                string template_code = model["template_code"].ToString();
                string invoice_series = model["invoice_series"].ToString();
                string invoice_number = model["invoice_number"].ToString();

                dicParameter.Clear();
                dicParameter.Add("template_code", template_code);
                dicParameter.Add("invoice_series", invoice_series);
                dicParameter.Add("invoice_number", invoice_number);

                sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE template_code=@template_code  AND invoice_series=@invoice_series  AND invoice_number=@invoice_number";

                if (branch_code.Length > 0)
                {
                    dicParameter.Add("branch_code", branch_code);
                    sql += " AND branch_code=@branch_code";
                }

                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                if (tblInvoice.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                Guid inv_invoiceauth_id = Guid.Parse(tblInvoice.Rows[0]["inv_invoiceauth_id"].ToString());

                if (model["id"] == null)
                {
                    model.Add("id", inv_invoiceauth_id);
                }

                JObject delInv = await GetInvoiceXoaBo(model);

                if (delInv["error"] != null)
                {
                    res.Add("error", delInv["error"].ToString());
                    return res;
                }

                string xml = delInv["InvoiceXmlData"].ToString();

                if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                {
                    xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "Signatures", "xml", xml, tblCertificate.Rows[0]["cer_serial"].ToString(), true);
                }
                else
                {
                    xml = CommonManager.SignXml("#data", "seller", xml, certFile, "Signatures");
                }

                //xml = CommonManager.SignByMInvoice(xml, "Signatures");

                delInv.Add("serialNumber", cert_serial);
                delInv["InvoiceXmlData"] = xml;

                delInv = await SaveInvoiceXoaBo(delInv);

                if (delInv["error"] != null)
                {
                    res.Add("error", delInv["error"].ToString());
                }
                else
                {
                    res.Add("ok", true);
                }

            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public string GetSchema()
        {
            return _minvoiceDbContext.GetSchemaName();
        }

        public byte[] DowloadRepx(string id)
        {
            byte[] s = null;

            Dictionary<string, object> dicParameter = new Dictionary<string, object>();
            dicParameter.Add("sl_invoice_template_id", Guid.Parse(id));

            DataTable dt = this._minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id =@sl_invoice_template_id", CommandType.Text, dicParameter);
            string dtt = dt.Rows[0]["report"].ToString();

            s = System.Text.Encoding.UTF8.GetBytes(dtt);
            return s;
        }

        public async Task<JObject> ConfirmTempInvoice(JObject model)
        {
            JArray invs = (JArray)model["invs"];
            JObject res = new JObject();

            try
            {
                for (int i = 0; i < invs.Count; i++)
                {
                    JObject item = (JObject)invs[i];

                    long invoice_id = Convert.ToInt64(item["invoice_id"].ToString());
                    string is_accept = Convert.ToBoolean(item["is_accept"].ToString()) == true ? "true" : "false";

                    string sql = "UPDATE #SCHEMA_NAME#.inv_invoice_temp SET is_accept=@is_accept WHERE invoice_id=@invoice_id";

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("is_accept", is_accept.Equals("true") ? true : false);
                    parameters.Add("invoice_id", invoice_id);

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                res.Add("ok", false);
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<byte[]> PreviewTempInvoice(long id, string folder, string type, bool inchuyendoi)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                dicParameter.Add("invoice_id", id);

                DataTable tblInvoiceTemp = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoice_temp_id FROM #SCHEMA_NAME#.inv_invoice_temp WHERE invoice_id=@invoice_id", CommandType.Text, dicParameter);

                if (tblInvoiceTemp.Rows.Count == 0)
                {
                    throw new Exception("Không tồn tại hóa đơn.");
                }

                Guid inv_InvoiceAuth_id = Guid.Parse(tblInvoiceTemp.Rows[0]["inv_invoice_temp_id"].ToString());

                dicParameter.Clear();
                dicParameter.Add("inv_InvoiceAuth_id", inv_InvoiceAuth_id);

                DataTable tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync("SELECT *,adjustment_type as invoice_status FROM #SCHEMA_NAME#.inv_invoice_temp WHERE inv_invoice_temp_id=@inv_InvoiceAuth_id", CommandType.Text, dicParameter);
                DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_InvoiceAuth_id", CommandType.Text, dicParameter);

                if (tblInvoiceXmlData.Rows.Count > 0)
                {
                    xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                }
                else
                {
                    dicParameter.Clear();
                    dicParameter.Add("inv_InvoiceAuth_id", inv_InvoiceAuth_id);

                    DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("SELECT #SCHEMA_NAME#.qr_export_xmlinvoice_temp('#SCHEMA_NAME#',@inv_InvoiceAuth_id)", CommandType.Text, dicParameter);
                    xml = tblData.Rows[0][0].ToString();
                }

                // QuyenNH modified
                xml = await ExportInvoiceXml(xml, inv_InvoiceAuth_id);

                xml = FunctionUtil.ReverseXml(xml);
                string branch_code = tblInv_InvoiceAuth.Rows[0]["branch_code"].ToString();

                dicParameter.Clear();
                dicParameter.Add("branch_code", branch_code);

                DataTable tblNotificationDetail = await this._minvoiceDbContext.GetDataTableAsync("SELECT inv_notificationdetail_id FROM #SCHEMA_NAME#.inv_notificationdetail WHERE branch_code=@branch_code  AND is_end=false ORDER BY begin_date,invoice_series", CommandType.Text, dicParameter);

                string inv_InvoiceCode_id = tblNotificationDetail.Rows[0]["inv_notificationdetail_id"].ToString();

                int trang_thai_hd = Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]);
                string inv_originalId = tblInv_InvoiceAuth.Rows[0]["original_invoice_id"].ToString();

                dicParameter.Clear();
                dicParameter.Add("inv_notificationdetail_id", Guid.Parse(inv_InvoiceCode_id));

                DataTable tblCtthongbao = await _minvoiceDbContext.GetDataTableAsync("SELECT a.*,b.thousand_point as hang_nghin,b.decimal_point as thap_phan FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id WHERE a.inv_notificationdetail_id=@inv_notificationdetail_id", CommandType.Text, dicParameter);

                string hang_nghin = tblCtthongbao.Rows[0]["hang_nghin"].ToString();
                string thap_phan = tblCtthongbao.Rows[0]["thap_phan"].ToString();

                string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", tblCtthongbao.Rows[0]["inv_notificationdetail_id"]);

                //XtraReport report = _cacheManager.Get<XtraReport>(cacheReportKey);
                XtraReport report = new XtraReport();
                //if (report == null)
                //{
                dicParameter.Clear();
                dicParameter.Add("sl_invoice_template_id", Guid.Parse(tblCtthongbao.Rows[0]["sl_invoice_template_id"].ToString()));

                DataTable tblDmmauhd = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id", CommandType.Text, dicParameter);
                string invReport = tblDmmauhd.Rows[0]["report"].ToString();

                if (invReport.Length > 0)
                {
                    report = ReportUtil.LoadReportFromString(invReport);
                    // _cacheManager.Set(cacheReportKey, report, 30);
                }
                else
                {
                    throw new Exception("Không tải được mẫu hóa đơn");
                }

                //   }


                report.Name = "XtraReport1";
                report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                {
                    ds.ReadXmlSchema(xmlReader);
                    xmlReader.Close();
                }

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                {
                    ds.ReadXml(xmlReader);
                    xmlReader.Close();
                }

                if (ds.Tables.Contains("TblXmlData"))
                {
                    ds.Tables.Remove("TblXmlData");
                }


                DataTable tblXmlData = new DataTable();
                tblXmlData.TableName = "TblXmlData";
                tblXmlData.Columns.Add("data");

                DataRow r = tblXmlData.NewRow();
                r["data"] = xml;
                tblXmlData.Rows.Add(r);
                ds.Tables.Add(tblXmlData);

                string datamember = report.DataMember;

                if (datamember.Length > 0)
                {
                    if (ds.Tables.Contains(datamember))
                    {
                        DataTable tblChiTiet = ds.Tables[datamember];

                        int rowcount = ds.Tables[datamember].Rows.Count;

                    }
                }

                if (trang_thai_hd == 11 || trang_thai_hd == 13 || trang_thai_hd == 17)
                {

                    dicParameter.Clear();
                    dicParameter.Add("inv_InvoiceAuth_id", inv_InvoiceAuth_id);

                    string sql = "SELECT STRING_AGG ( a.invoice_number || ' ngày ' || to_char(a.invoice_issued_date,'DD/MM/YYYY') || ' mẫu số ' || a.template_code || ' ký hiệu ' || a.invoice_series ,','  ORDER BY a.invoice_issued_date desc,a.invoice_number desc) msg FROM #SCHEMA_NAME#.inv_invoiceauth a "
                            + "WHERE a.original_invoice_id=@inv_InvoiceAuth_id GROUP BY a.original_invoice_id";

                    DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                    if (tblInv.Rows.Count > 0)
                    {
                        string msg = tblInv.Rows[0]["msg"].ToString();

                        if (msg.Length > 0)
                        {
                            dicParameter.Clear();
                            dicParameter.Add("code", trang_thai_hd);
                            DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=@code", CommandType.Text, dicParameter);
                            string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();


                            msg_tb = "Hóa đơn " + loai + " bởi hóa đơn số: " + msg;

                        }

                    }
                }

                if (Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]) == 7)
                {
                    msg_tb = "";
                }

                if (report.Parameters["MSG_TB"] != null)
                {
                    report.Parameters["MSG_TB"].Value = msg_tb;
                }

                var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

                if (lblHoaDonMau != null)
                {
                    lblHoaDonMau.Visible = false;
                }

                if (inchuyendoi)
                {
                    var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                    if (tblInChuyenDoi != null)
                    {
                        tblInChuyenDoi.Visible = true;
                    }

                    if (report.Parameters["MSG_HD_TITLE"] != null)
                    {
                        report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                    }

                    if (report.Parameters["NGUOI_IN_CDOI"] != null)
                    {
                        report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                        report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                    }

                    if (report.Parameters["NGAY_IN_CDOI"] != null)
                    {
                        report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                        report.Parameters["NGAY_IN_CDOI"].Visible = true;
                    }
                }

                report.DataSource = ds;

                var t = Task.Run(() =>
                {
                    var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    newCulture.NumberFormat.NumberDecimalSeparator = thap_phan;
                    newCulture.NumberFormat.NumberGroupSeparator = hang_nghin;

                    System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;

                    report.CreateDocument();

                });

                t.Wait();


                if (tblInv_InvoiceAuth.Columns.Contains("list_number"))
                {
                    if (tblInv_InvoiceAuth.Rows[0]["list_number"].ToString().Length > 0)
                    {
                        string fileName = folder + "\\BangKeDinhKem.repx";

                        XtraReport rpBangKe = null;

                        if (!File.Exists(fileName))
                        {
                            rpBangKe = new XtraReport();
                            rpBangKe.SaveLayout(fileName);
                        }
                        else
                        {
                            rpBangKe = XtraReport.FromFile(fileName, true);
                        }

                        rpBangKe.ScriptReferencesString = "AccountSignature.dll";
                        rpBangKe.Name = "rpBangKe";
                        rpBangKe.DisplayName = "BangKeDinhKem.repx";

                        rpBangKe.DataSource = report.DataSource;

                        rpBangKe.CreateDocument();
                        report.Pages.AddRange(rpBangKe.Pages);
                    }

                }

                if (tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString() == "7")
                {

                    Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                    int pageCount = report.Pages.Count;


                    for (int i = 0; i < pageCount; i++)
                    {
                        Page page = report.Pages[i];
                        PageWatermark pmk = new PageWatermark();
                        pmk.Image = bmp;
                        page.AssignWatermark(pmk);
                    }

                    string fileName = folder + "\\BienBanXoaBo.repx";
                    XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                    rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                    rpBienBan.Name = "rpBienBan";
                    rpBienBan.DisplayName = "BienBanXoaBo.repx";

                    rpBienBan.DataSource = report.DataSource;
                    rpBienBan.DataMember = report.DataMember;

                    rpBienBan.CreateDocument();

                    rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                    report.PrintingSystem.ContinuousPageNumbering = false;

                    report.Pages.AddRange(rpBienBan.Pages);

                    int idx = pageCount;
                    pageCount = report.Pages.Count;

                    for (int i = idx; i < pageCount; i++)
                    {
                        PageWatermark pmk = new PageWatermark();
                        pmk.ShowBehind = false;
                        report.Pages[i].AssignWatermark(pmk);
                    }

                }

                if (trang_thai_hd == 19 || trang_thai_hd == 21 || trang_thai_hd == 5)
                {

                    string rp_file = trang_thai_hd == 19 || trang_thai_hd == 21 ? "INCT_BBDC_GT.repx" : "INCT_BBDC_DD.repx";
                    string rp_code = trang_thai_hd == 19 || trang_thai_hd == 21 ? "#SCHEMA_NAME#.qr_inct_hd_dieuchinhgt" : "#SCHEMA_NAME#.qr_inct_hd_dieuchinhdg";

                    string fileName = folder + "\\" + rp_file;
                    XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                    rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                    rpBienBan.Name = "rpBienBanDC";
                    rpBienBan.DisplayName = rp_file;

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("p_branch_code", _webHelper.GetDvcs());
                    //parameters.Add("p_schemaname", "#SCHEMA_NAME#");
                    parameters.Add("p_document_id", inv_InvoiceAuth_id);

                    DataSet dataSource = await this._minvoiceDbContext.GetDataSetAsyncPrintf(rp_code, CommandType.StoredProcedure, parameters);

                    rpBienBan.DataSource = dataSource;
                    rpBienBan.DataMember = dataSource.Tables[0].TableName;

                    rpBienBan.CreateDocument();

                    rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                    report.PrintingSystem.ContinuousPageNumbering = false;

                    report.Pages.AddRange(rpBienBan.Pages);

                    Page page = report.Pages[report.Pages.Count - 1];
                    page.AssignWatermark(new PageWatermark());

                }

                if (trang_thai_hd == 13 || trang_thai_hd == 17)
                {
                    Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                    int pageCount = report.Pages.Count;

                    for (int i = 0; i < pageCount; i++)
                    {
                        PageWatermark pmk = new PageWatermark();
                        pmk.Image = bmp;
                        report.Pages[i].AssignWatermark(pmk);
                    }
                }

                MemoryStream ms = new MemoryStream();

                if (type == "Html")
                {
                    report.ExportOptions.Html.EmbedImagesInHTML = true;
                    report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                    report.ExportOptions.Html.Title = "Hóa đơn điện tử";
                    report.ExportToHtml(ms);

                    string html = Encoding.UTF8.GetString(ms.ToArray());

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(html);


                    string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                    string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                    var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            string eventMouseDown = node.Attributes["onmousedown"].Value;

                            if (eventMouseDown.Contains("showCert('seller')"))
                            {
                                node.SetAttributeValue("id", "certSeller");
                            }
                            if (eventMouseDown.Contains("showCert('buyer')"))
                            {
                                node.SetAttributeValue("id", "certBuyer");
                            }
                            if (eventMouseDown.Contains("showCert('vacom')"))
                            {
                                node.SetAttributeValue("id", "certVacom");
                            }
                            if (eventMouseDown.Contains("showCert('minvoice')"))
                            {
                                node.SetAttributeValue("id", "certMinvoice");
                            }
                        }
                    }

                    HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                    HtmlNode xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("id", "xmlData");
                    xmlNode.SetAttributeValue("type", "text/xmldata");

                    xmlNode.AppendChild(doc.CreateTextNode(xml));
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("type", "text/javascript");

                    xmlNode.InnerHtml = "$(function () { "
                                       + "  var url = 'http://localhost:19898/signalr'; "
                                       + "  var connection = $.hubConnection(url, {  "
                                       + "     useDefaultPath: false "
                                       + "  });"
                                       + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                       + " invoiceHubProxy.on('resCommand', function (result) { "
                                       + " }); "
                                       + " connection.start().done(function () { "
                                       + "      console.log('Connect to the server successful');"
                                       + "      $('#certSeller').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'seller' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certVacom').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'vacom' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certBuyer').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'buyer' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certMinvoice').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'minvoice' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "})"
                                       + ".fail(function () { "
                                       + "     alert('failed in connecting to the signalr server'); "
                                       + "});"
                                       + "});";

                    head.AppendChild(xmlNode);

                    if (report.Watermark != null)
                    {
                        if (report.Watermark.Image != null)
                        {
                            ImageConverter _imageConverter = new ImageConverter();
                            byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                            string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                            HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                            string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                            string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                            HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                            style.AppendChild(textNode);

                            HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                            HtmlNodeCollection pageNodes = body.SelectNodes("div");

                            foreach (var pageNode in pageNodes)
                            {
                                pageNode.Attributes.Add("class", "waterMark");

                                string divStyle = pageNode.Attributes["style"].Value;
                                divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                pageNode.Attributes["style"].Value = divStyle;
                            }
                        }
                    }

                    ms.SetLength(0);
                    doc.Save(ms);

                    doc = null;
                }
                else if (type == "Image")
                {
                    var options = new ImageExportOptions(ImageFormat.Png)
                    {
                        ExportMode = ImageExportMode.SingleFilePageByPage,
                    };
                    report.ExportToImage(ms, options);
                }
                else if (type == "Excel")
                {
                    report.ExportToXlsx(ms);
                }
                else if (type == "Rtf")
                {
                    report.ExportToRtf(ms);
                }
                else
                {
                    report.ExportToPdf(ms);
                }

                bytes = ms.ToArray();
                ms.Close();

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<JObject> UpdateCustomerInfo(JObject model)
        {
            JObject res = new JObject();

            try
            {
                long invoice_id = Convert.ToInt64(model["invoice_id"].ToString());

                string buyer_legal_name = model["buyer_legal_name"].ToString();
                string buyer_taxcode = model["buyer_taxcode"].ToString();
                string buyer_email = model["buyer_email"].ToString();
                string buyer_address_line = model["buyer_address_line"].ToString();
                string buyer_display_name = model["buyer_display_name"].ToString();
                string customer_code = model["customer_code"].ToString();
                string sub_no = model["sub_no"].ToString();

                string sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET buyer_legal_name=@buyer_legal_name,buyer_taxcode=@buyer_taxcode,"
                    + "buyer_email=@buyer_email,buyer_address_line=@buyer_address_line,buyer_display_name=@buyer_display_name,"
                    + "customer_code=@customer_code,sub_no=@sub_no WHERE invoice_id=@invoice_id AND status='Chờ ký' ";

                Dictionary<string, object> dic = new Dictionary<string, object>();

                dic.Add("buyer_legal_name", buyer_legal_name);
                dic.Add("buyer_taxcode", buyer_taxcode);
                dic.Add("buyer_email", buyer_email);
                dic.Add("buyer_address_line", buyer_address_line);
                dic.Add("buyer_display_name", buyer_display_name);
                dic.Add("customer_code", customer_code);
                dic.Add("sub_no", sub_no);
                dic.Add("invoice_id", invoice_id);

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dic);

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                res.Add("ok", false);
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> SendEmailByInvoiceId(JObject model)
        {
            JObject res = new JObject();

            try
            {
                long invoice_id = Convert.ToInt64(model["invoice_id"].ToString());

                Dictionary<string, object> dicParameter = new Dictionary<string, object>();

                dicParameter.Add("invoice_id", invoice_id);
                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT inv_invoiceauth_id,buyer_email FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id", CommandType.Text, dicParameter);

                if (tblInvoices.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                string buyer_email = tblInvoices.Rows[0]["buyer_email"].ToString();
                string inv_invoiceauth_id = tblInvoices.Rows[0]["inv_invoiceauth_id"].ToString();

                if (buyer_email.Length > 0)
                {
                    JObject obj = new JObject();
                    obj.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                    obj.Add("nguoinhan", buyer_email);
                    obj.Add("tieude", "Thông báo xuất hóa đơn điện tử");

                    string originalString = _webHelper.GetRequest().Url.OriginalString;
                    string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                    var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                    res = await this.AutoSendInvoiceByEmail(folder, obj);
                }
                else
                {
                    res.Add("ok", false);
                    res.Add("error", "Hóa đơn không có thông tin email");
                }
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> InfoEmailToCustomer(long invoice_id)
        {
            JObject res = new JObject();

            try
            {
                Dictionary<string, object> dicParameter = new Dictionary<string, object>();

                dicParameter.Add("invoice_id", invoice_id);

                string sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id  LIMIT 1";
                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                if (tblData.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                string inv_invoiceauth_id = tblData.Rows[0]["inv_invoiceauth_id"].ToString();

                JArray array = new JArray();

                dicParameter.Clear();
                dicParameter.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id.ToString()));

                sql = "SELECT * FROM #SCHEMA_NAME#.wb_log_email WHERE inv_invoiceauth_id=@inv_invoiceauth_id";
                tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                foreach (DataRow row in tblData.Rows)
                {
                    string to = row["to"].ToString();
                    DateTime send_date = Convert.ToDateTime(row["send_date"]);

                    JObject obj = new JObject();
                    obj.Add("to", to);
                    obj.Add("send_date", send_date);

                    array.Add(obj);
                }

                dicParameter.Clear();
                dicParameter.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id.ToString()));

                sql = "SELECT * FROM #SCHEMA_NAME#.wb_email_queue WHERE inv_invoiceauth_id=@inv_invoiceauth_id AND is_send=true AND send_status='OK' ";
                tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                foreach (DataRow row in tblData.Rows)
                {
                    string to = row["to"].ToString();
                    DateTime send_date = Convert.ToDateTime(row["send_date"]);

                    JObject obj = new JObject();
                    obj.Add("to", to);
                    obj.Add("send_date", send_date);

                    array.Add(obj);
                }

                res.Add("ok", true);
                res.Add("lst_date", array);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> DeleteReceiptMulti(JObject model)
        {
            JObject res = new JObject();

            try
            {
                String invoice_ids = model["invoice_ids"].ToString();
                String[] list_invoice_id = invoice_ids.Split(',').ToArray();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("invoice_ids", invoice_ids);

                DataTable tblTotalInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT COUNT(*) TOTAL FROM #SCHEMA_NAME#.inv_receipt WHERE invoice_id IN (SELECT unnest(string_to_array(@invoice_ids, ',')::bigint[])) ", CommandType.Text, parameters);

                if (tblTotalInvoices.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Không tìm thấy phiếu thu");
                    return res;
                }
                else
                {
                    if (Convert.ToInt64(tblTotalInvoices.Rows[0]["TOTAL"].ToString()) != list_invoice_id.Length)
                    {
                        res.Add("ok", false);
                        res.Add("error", "Danh sách phiếu thu không hợp lệ");
                        return res;
                    }
                }

                foreach (String invoice_id_str in list_invoice_id)
                {
                    long invoice_id = Convert.ToInt64(invoice_id_str);

                    parameters.Clear();
                    parameters.Add("invoice_id", invoice_id);

                    DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_receipt WHERE invoice_id=@invoice_id ", CommandType.Text, parameters);

                    if (tblInvoices.Rows.Count == 0)
                    {
                        res.Add("ok", false);
                        res.Add("error", "Không tìm thấy phiếu thu");
                        return res;
                    }

                    string inv_invoiceauth_id = tblInvoices.Rows[0]["inv_receipt_id"].ToString();
                    string branch_code = tblInvoices.Rows[0]["branch_code"].ToString();

                    parameters.Clear();
                    parameters.Add("inv_receipt_id", Guid.Parse(inv_invoiceauth_id));

                    string sql = "UPDATE #SCHEMA_NAME#.inv_receipt SET status='Xóa bỏ' WHERE inv_receipt_id=@inv_receipt_id ";

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);


                    string buyer_email = model["buyer_email"].ToString();
                    string buyer_tel = model["buyer_tel"].ToString();

                    if (buyer_email.Length > 0)
                    {

                        parameters.Clear();
                        parameters.Add("branch_code", branch_code);

                        sql = "SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type='Xóa bỏ phiếu thu' AND branch_code=@branch_code  LIMIT 1 OFFSET 0";
                        DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                        if (tblTemp.Rows.Count > 0)
                        {
                            string noi_dung = tblTemp.Rows[0]["noi_dung"].ToString();
                            string subject = tblTemp.Rows[0]["subject"].ToString();

                            DataRow row = tblInvoices.Rows[0];

                            foreach (DataColumn column in tblInvoices.Columns)
                            {
                                string value = "";

                                if (row[column.ColumnName] != DBNull.Value)
                                {
                                    string dataType = column.DataType.Name.ToLower();

                                    if (dataType.Contains("datetime"))
                                    {
                                        DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                        value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                    }
                                    else
                                    {
                                        value = row[column.ColumnName].ToString();
                                    }
                                }

                                noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                                subject = subject.Replace("#" + column.ColumnName + "#", value);
                            }

                            await _emailService.SendAsync(buyer_email, subject, noi_dung);

                            sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email(wb_log_email_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                                 + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                            parameters.Clear();
                            parameters.Add("branch_code", branch_code);
                            parameters.Add("send_date", DateTime.Now);
                            parameters.Add("from", _emailService.GetFrom());
                            parameters.Add("to", buyer_email);
                            parameters.Add("subject", subject);
                            parameters.Add("content", noi_dung);
                            parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                            parameters.Add("user_send", _webHelper.GetUser());
                            parameters.Add("send_type", "Xóa bỏ phiếu thu");

                            await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                        }


                    }

                    if (buyer_tel.Length > 0)
                    {

                        parameters.Clear();
                        parameters.Add("branch_code", branch_code);

                        sql = "SELECT * FROM #SCHEMA_NAME#.pl_tempsms WHERE yesno='C' AND tempsms_type='Xóa bỏ phiếu thu' AND branch_code=@branch_code  LIMIT 1 OFFSET 0";
                        DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                        if (tblTemp.Rows.Count > 0)
                        {
                            string noi_dung = tblTemp.Rows[0]["body"].ToString();
                            string subject = tblTemp.Rows[0]["subject"].ToString();

                            DataRow row = tblInvoices.Rows[0];

                            foreach (DataColumn column in tblInvoices.Columns)
                            {
                                string value = "";

                                if (row[column.ColumnName] != DBNull.Value)
                                {
                                    string dataType = column.DataType.Name.ToLower();

                                    if (dataType.Contains("datetime"))
                                    {
                                        DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                        value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                    }
                                    else
                                    {
                                        value = row[column.ColumnName].ToString();
                                    }
                                }

                                noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                                subject = subject.Replace("#" + column.ColumnName + "#", value);
                            }

                            sql = "INSERT INTO #SCHEMA_NAME#.wb_sms_queue(wb_sms_queue_id,mobile,subject,body,date_new,inv_invoiceauth_id,template_code,invoice_number,invoice_series,is_send,send_error,send_status) \n"
                                + "VALUES(@wb_sms_queue_id,@mobile,@subject,@body,@date_new,@inv_invoiceauth_id,@template_code,@invoice_number,@invoice_series,@is_send,@send_error,@send_status)";

                            Dictionary<string, object> dic = new Dictionary<string, object>();

                            dic.Add("wb_sms_queue_id", Guid.NewGuid());
                            dic.Add("mobile", buyer_tel);
                            dic.Add("subject", subject);
                            dic.Add("body", noi_dung);
                            dic.Add("date_new", DateTime.Now);
                            dic.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                            dic.Add("template_code", "");
                            dic.Add("invoice_series", "");
                            dic.Add("invoice_number", tblInvoices.Rows[0]["receipt_number"].ToString());
                            dic.Add("is_send", false);
                            //dic.Add("send_date", DateTime.Now);
                            dic.Add("send_error", "");
                            dic.Add("send_status", "");

                            await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dic);
                        }
                    }

                }

                res.Add("ok", true);


            }
            catch (Exception ex)
            {
                res.Add("ok", false);
                res.Add("error", ex.Message);
            }

            return res;
        }
        public async Task<JObject> DeleteReceipt(JObject model)
        {
            JObject res = new JObject();

            try
            {
                long invoice_id = Convert.ToInt64(model["invoice_id"].ToString());

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("invoice_id", invoice_id);

                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_receipt WHERE invoice_id=@invoice_id ", CommandType.Text, parameters);

                if (tblInvoices.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Không tìm thấy phiếu thu");
                    return res;
                }

                string inv_invoiceauth_id = tblInvoices.Rows[0]["inv_receipt_id"].ToString();
                string branch_code = tblInvoices.Rows[0]["branch_code"].ToString();

                string sql = "UPDATE #SCHEMA_NAME#.inv_receipt SET status='Xóa bỏ' WHERE inv_receipt_id=@inv_receipt_id ";
                parameters.Clear();
                parameters.Add("inv_receipt_id", Guid.Parse(inv_invoiceauth_id));

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                string buyer_email = model["buyer_email"].ToString();
                string buyer_tel = model["buyer_tel"].ToString();


                if (buyer_email.Length > 0)
                {
                    sql = "SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type='Xóa bỏ phiếu thu' AND branch_code=@branch_code LIMIT 1 OFFSET 0";

                    parameters.Clear();
                    parameters.Add("branch_code", branch_code);

                    DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);


                    string noi_dung = tblTemp.Rows[0]["noi_dung"].ToString();
                    string subject = tblTemp.Rows[0]["subject"].ToString();

                    DataRow row = tblInvoices.Rows[0];

                    foreach (DataColumn column in tblInvoices.Columns)
                    {
                        string value = "";

                        if (row[column.ColumnName] != DBNull.Value)
                        {
                            string dataType = column.DataType.Name.ToLower();

                            if (dataType.Contains("datetime"))
                            {
                                DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                value = string.Format("{0:dd/MM/yyyy}", dateTime);
                            }
                            else
                            {
                                value = row[column.ColumnName].ToString();
                            }
                        }

                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                        subject = subject.Replace("#" + column.ColumnName + "#", value);
                    }

                    await _emailService.SendAsync(buyer_email, subject, noi_dung);

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email(wb_log_email_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                         + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                    parameters.Clear();
                    parameters.Add("branch_code", branch_code);
                    parameters.Add("send_date", DateTime.Now);
                    parameters.Add("from", _emailService.GetFrom());
                    parameters.Add("to", buyer_email);
                    parameters.Add("subject", subject);
                    parameters.Add("content", noi_dung);
                    parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                    parameters.Add("user_send", _webHelper.GetUser());
                    parameters.Add("send_type", "Xóa bỏ phiếu thu");

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                }

                if (buyer_tel.Length > 0)
                {
                    parameters.Clear();
                    parameters.Add("branch_code", branch_code);

                    sql = "SELECT * FROM #SCHEMA_NAME#.pl_tempsms WHERE yesno='C' AND tempsms_type='Xóa bỏ phiếu thu' AND branch_code=@branch_code  LIMIT 1 OFFSET 0";
                    DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                    if (tblTemp.Rows.Count > 0)
                    {
                        string noi_dung = tblTemp.Rows[0]["body"].ToString();
                        string subject = tblTemp.Rows[0]["subject"].ToString();

                        DataRow row = tblInvoices.Rows[0];

                        foreach (DataColumn column in tblInvoices.Columns)
                        {
                            string value = "";

                            if (row[column.ColumnName] != DBNull.Value)
                            {
                                string dataType = column.DataType.Name.ToLower();

                                if (dataType.Contains("datetime"))
                                {
                                    DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                    value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                }
                                else
                                {
                                    value = row[column.ColumnName].ToString();
                                }
                            }

                            noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                            subject = subject.Replace("#" + column.ColumnName + "#", value);
                        }

                        sql = "INSERT INTO #SCHEMA_NAME#.wb_sms_queue(wb_sms_queue_id,mobile,subject,body,date_new,inv_invoiceauth_id,template_code,invoice_number,invoice_series,is_send,send_error,send_status) \n"
                            + "VALUES(@wb_sms_queue_id,@mobile,@subject,@body,@date_new,@inv_invoiceauth_id,@template_code,@invoice_number,@invoice_series,@is_send,@send_error,@send_status)";

                        Dictionary<string, object> dic = new Dictionary<string, object>();

                        dic.Add("wb_sms_queue_id", Guid.NewGuid());
                        dic.Add("mobile", buyer_tel);
                        dic.Add("subject", subject);
                        dic.Add("body", noi_dung);
                        dic.Add("date_new", DateTime.Now);
                        dic.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                        dic.Add("template_code", "");
                        dic.Add("invoice_series", "");
                        dic.Add("invoice_number", tblInvoices.Rows[0]["receipt_number"].ToString());
                        dic.Add("is_send", false);
                        //dic.Add("send_date", DateTime.Now);
                        dic.Add("send_error", "");
                        dic.Add("send_status", "");

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dic);
                    }
                }

                res.Add("ok", true);


            }
            catch (Exception ex)
            {
                res.Add("ok", false);
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<JObject> ChangeSecurityNumber(JObject model)
        {
            JObject res = new JObject();
            bool isOpen = false;

            try
            {
                long invoice_id = Convert.ToInt64(model["invoice_id"].ToString());

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("invoice_id", invoice_id);

                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id ", CommandType.Text, parameters);


                if (tblInvoices.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                string inv_invoiceauth_id = tblInvoices.Rows[0]["inv_invoiceauth_id"].ToString();
                string branch_code = tblInvoices.Rows[0]["branch_code"].ToString();

                parameters.Clear();
                parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                DataTable tblInvoiceXmlData = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_invoiceauth_id ", CommandType.Text, parameters);

                if (tblInvoiceXmlData.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Hóa đơn chưa ký");
                    return res;
                }

                string xml = tblInvoiceXmlData.Rows[0]["data"].ToString();


                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = true;

                doc.LoadXml(xml);

                //byte[] bytes = Guid.NewGuid().ToByteArray();
                //string base64Str = Convert.ToBase64String(bytes);

                string security_number_old = doc.GetElementsByTagName("security_number")[0].InnerText;

                string security_number = Guid.NewGuid().ToString().Substring(0, 6);
                doc.GetElementsByTagName("security_number")[0].InnerText = security_number;

                xml = doc.OuterXml;

                await _minvoiceDbContext.BeginTransactionAsync();

                isOpen = true;

                string sql = "UPDATE #SCHEMA_NAME#.inv_invoicexmldata SET data=CAST(@data as xml) WHERE inv_invoiceauth_id=@inv_invoiceauth_id ";

                JObject parameter = new JObject();
                parameter.Add("data", xml);
                parameter.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameter);

                sql = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET security_number=@security_number WHERE inv_invoiceauth_id=@inv_invoiceauth_id ";

                parameters.Clear();
                parameters.Add("security_number", security_number);
                parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);


                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                isOpen = false;

                string buyer_email = model["buyer_email"].ToString();
                string buyer_tel = model["buyer_tel"].ToString();

                if (buyer_email.Length > 0)
                {
                    sql = "SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type='Đổi mã bảo mật' AND branch_code=@branch_code LIMIT 1 OFFSET 0";

                    parameters.Clear();
                    parameters.Add("branch_code", branch_code);

                    DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                    if (tblTemp.Rows.Count > 0)
                    {
                        sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id )";

                        parameters.Clear();
                        parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                        DataTable tblEmailBody = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                        DataRow row = tblEmailBody.Rows[0];

                        string noi_dung = tblTemp.Rows[0]["noi_dung"].ToString();
                        string subject = tblTemp.Rows[0]["subject"].ToString();

                        foreach (DataColumn column in tblEmailBody.Columns)
                        {
                            string value = "";

                            if (row[column.ColumnName] != DBNull.Value)
                            {
                                string dataType = column.DataType.Name.ToLower();

                                if (dataType.Contains("datetime"))
                                {
                                    DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                    value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                }
                                else
                                {
                                    value = row[column.ColumnName].ToString();
                                }
                            }

                            noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                            subject = subject.Replace("#" + column.ColumnName + "#", value);
                        }

                        await _emailService.SendAsync(buyer_email, subject, noi_dung);

                        sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email(wb_log_email_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                             + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                        parameters.Clear();
                        parameters.Add("branch_code", branch_code);
                        parameters.Add("send_date", DateTime.Now);
                        parameters.Add("from", _emailService.GetFrom());
                        parameters.Add("to", buyer_email);
                        parameters.Add("subject", subject);
                        parameters.Add("content", noi_dung);
                        parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                        parameters.Add("user_send", _webHelper.GetUser());
                        parameters.Add("send_type", "Gửi hóa đơn");

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    }
                }

                if (buyer_tel.Length > 0)
                {
                    parameters.Clear();
                    parameters.Add("branch_code", branch_code);

                    sql = "SELECT * FROM #SCHEMA_NAME#.pl_tempsms WHERE yesno='C' AND tempsms_type='Đổi mã bảo mật' AND branch_code=@branch_code LIMIT 1 OFFSET 0";
                    DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                    if (tblTemp.Rows.Count > 0)
                    {
                        string noi_dung = tblTemp.Rows[0]["body"].ToString();
                        string subject = tblTemp.Rows[0]["subject"].ToString();

                        parameters.Clear();
                        parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));

                        sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id)";
                        DataTable tblEmailBody = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                        DataRow row = tblEmailBody.Rows[0];

                        foreach (DataColumn column in tblEmailBody.Columns)
                        {
                            string value = "";

                            if (row[column.ColumnName] != DBNull.Value)
                            {
                                string dataType = column.DataType.Name.ToLower();

                                if (dataType.Contains("datetime"))
                                {
                                    DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                    value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                }
                                else
                                {
                                    value = row[column.ColumnName].ToString();
                                }
                            }

                            noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                            subject = subject.Replace("#" + column.ColumnName + "#", value);
                        }

                        noi_dung = noi_dung.Replace("#security_number_old#", security_number_old);
                        subject = subject.Replace("#security_number_old#", security_number_old);

                        sql = "INSERT INTO #SCHEMA_NAME#.wb_sms_queue(wb_sms_queue_id,mobile,subject,body,date_new,inv_invoiceauth_id,template_code,invoice_number,invoice_series,is_send,send_error,send_status) \n"
                            + "VALUES(@wb_sms_queue_id,@mobile,@subject,@body,@date_new,@inv_invoiceauth_id,@template_code,@invoice_number,@invoice_series,@is_send,@send_error,@send_status)";

                        Dictionary<string, object> dic = new Dictionary<string, object>();

                        dic.Add("wb_sms_queue_id", Guid.NewGuid());
                        dic.Add("mobile", buyer_tel);
                        dic.Add("subject", subject);
                        dic.Add("body", noi_dung);
                        dic.Add("date_new", DateTime.Now);
                        dic.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                        dic.Add("template_code", tblInvoices.Rows[0]["template_code"].ToString());
                        dic.Add("invoice_series", tblInvoices.Rows[0]["invoice_series"].ToString());
                        dic.Add("invoice_number", tblInvoices.Rows[0]["invoice_number"].ToString());
                        dic.Add("is_send", false);
                        //dic.Add("send_date", DateTime.Now);
                        dic.Add("send_error", "");
                        dic.Add("send_status", "");

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dic);
                    }
                }

                res.Add("ok", true);
                res.Add("security_number", security_number);
            }
            catch (Exception ex)
            {
                res.Add("ok", false);
                res.Add("error", ex.Message);
            }

            if (isOpen)
            {
                if (res["error"] != null)
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                }

                _minvoiceDbContext.CloseTransaction();
            }

            return res;
        }

        public async Task<JObject> DeleteInv(JObject model)
        {
            JObject res = new JObject();

            try
            {
                long invoice_id = Convert.ToInt64(model["invoice_id"].ToString());

                Dictionary<string, object> dicParameter = new Dictionary<string, object>();

                dicParameter.Add("invoice_id", invoice_id);


                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id", CommandType.Text, dicParameter);

                if (tblInvoices.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                //string inv_invoiceauth_id = tblInvoices.Rows[0]["inv_invoiceauth_id"].ToString();
                string branch_code = tblInvoices.Rows[0]["branch_code"].ToString();

                string buyer_email = model["buyer_email"].ToString();
                string buyer_tel = model["buyer_tel"].ToString();

                dicParameter.Clear();
                dicParameter.Add("branch_code", branch_code);

                string sql = "SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code  AND begin_date<=current_timestamp AND end_date>=current_timestamp";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("ok", false);
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }

                string pass = tblCertificate.Rows[0]["pass"].ToString();
                string cert_serial = tblCertificate.Rows[0]["cer_serial"].ToString();
                byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];

                X509Certificate2 certFile = new X509Certificate2(bytes, pass);

                string template_code = tblInvoices.Rows[0]["template_code"].ToString();
                string invoice_series = tblInvoices.Rows[0]["invoice_series"].ToString();
                string invoice_number = tblInvoices.Rows[0]["invoice_number"].ToString();

                dicParameter.Clear();
                dicParameter.Add("template_code", template_code);
                dicParameter.Add("invoice_series", invoice_series);
                dicParameter.Add("invoice_number", invoice_number);

                sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE template_code=@template_code  AND invoice_series=@invoice_series  AND invoice_number=@invoice_number";

                if (branch_code.Length > 0)
                {
                    dicParameter.Add("branch_code", branch_code);
                    sql += " AND branch_code=@branch_code ";
                }

                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                if (tblInvoice.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy hóa đơn");
                    return res;
                }

                Guid inv_invoiceauth_id = Guid.Parse(tblInvoice.Rows[0]["inv_invoiceauth_id"].ToString());

                if (model["id"] == null)
                {
                    model.Add("id", inv_invoiceauth_id);
                }

                JObject delInv = await GetInvoiceXoaBo(model);

                if (delInv["error"] != null)
                {
                    res.Add("error", delInv["error"].ToString());
                    return res;
                }

                string xml = delInv["InvoiceXmlData"].ToString();

                xml = CommonManager.SignXml("#data", "seller", xml, certFile, "Signatures");
                //xml = CommonManager.SignByMInvoice(xml, "Signatures");

                delInv.Add("serialNumber", cert_serial);
                delInv["InvoiceXmlData"] = xml;

                delInv = await SaveInvoiceXoaBo(delInv);

                if (delInv["error"] != null)
                {
                    res.Add("error", delInv["error"].ToString());
                }
                else
                {


                    if (buyer_email.Length > 0)
                    {
                        dicParameter.Clear();
                        dicParameter.Add("branch_code", branch_code);
                        sql = "SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type='Xóa bỏ hóa đơn' AND branch_code=@branch_code  LIMIT 1 OFFSET 0";
                        DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                        if (tblTemp.Rows.Count > 0)
                        {
                            dicParameter.Clear();
                            dicParameter.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id)";
                            DataTable tblEmailBody = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                            DataRow row = tblEmailBody.Rows[0];

                            string noi_dung = tblTemp.Rows[0]["noi_dung"].ToString();
                            string subject = tblTemp.Rows[0]["subject"].ToString();

                            foreach (DataColumn column in tblEmailBody.Columns)
                            {
                                string value = "";

                                if (row[column.ColumnName] != DBNull.Value)
                                {
                                    string dataType = column.DataType.Name.ToLower();

                                    if (dataType.Contains("datetime"))
                                    {
                                        DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                        value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                    }
                                    else
                                    {
                                        value = row[column.ColumnName].ToString();
                                    }
                                }

                                noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                                subject = subject.Replace("#" + column.ColumnName + "#", value);
                            }

                            await _emailService.SendAsync(buyer_email, subject, noi_dung);

                            sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email(wb_log_email_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                                 + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                            Dictionary<string, object> parameters = new Dictionary<string, object>();
                            parameters.Add("branch_code", branch_code);
                            parameters.Add("send_date", DateTime.Now);
                            parameters.Add("from", _emailService.GetFrom());
                            parameters.Add("to", buyer_email);
                            parameters.Add("subject", subject);
                            parameters.Add("content", noi_dung);
                            parameters.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            parameters.Add("user_send", _webHelper.GetUser());
                            parameters.Add("send_type", "Xóa bỏ hóa đơn");

                            await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                        }
                    }

                    if (buyer_tel.Length > 0)
                    {
                        dicParameter.Clear();
                        dicParameter.Add("branch_code", branch_code);

                        sql = "SELECT * FROM #SCHEMA_NAME#.pl_tempsms WHERE yesno='C' AND tempsms_type='Xóa bỏ hóa đơn' AND branch_code=@branch_code LIMIT 1 OFFSET 0";
                        DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                        if (tblTemp.Rows.Count > 0)
                        {
                            string noi_dung = tblTemp.Rows[0]["body"].ToString();
                            string subject = tblTemp.Rows[0]["subject"].ToString();

                            dicParameter.Clear();
                            dicParameter.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id)";
                            DataTable tblEmailBody = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                            DataRow row = tblEmailBody.Rows[0];

                            foreach (DataColumn column in tblEmailBody.Columns)
                            {
                                string value = "";

                                if (row[column.ColumnName] != DBNull.Value)
                                {
                                    string dataType = column.DataType.Name.ToLower();

                                    if (dataType.Contains("datetime"))
                                    {
                                        DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                        value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                    }
                                    else
                                    {
                                        value = row[column.ColumnName].ToString();
                                    }
                                }

                                noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                                subject = subject.Replace("#" + column.ColumnName + "#", value);
                            }

                            sql = "INSERT INTO #SCHEMA_NAME#.wb_sms_queue(wb_sms_queue_id,mobile,subject,body,date_new,inv_invoiceauth_id,template_code,invoice_number,invoice_series,is_send,send_error,send_status) \n"
                                + "VALUES(@wb_sms_queue_id,@mobile,@subject,@body,@date_new,@inv_invoiceauth_id,@template_code,@invoice_number,@invoice_series,@is_send,@send_error,@send_status)";

                            Dictionary<string, object> dic = new Dictionary<string, object>();

                            dic.Add("wb_sms_queue_id", Guid.NewGuid());
                            dic.Add("mobile", buyer_tel);
                            dic.Add("subject", subject);
                            dic.Add("body", noi_dung);
                            dic.Add("date_new", DateTime.Now);
                            dic.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                            dic.Add("template_code", tblInvoices.Rows[0]["template_code"].ToString());
                            dic.Add("invoice_series", tblInvoices.Rows[0]["invoice_series"].ToString());
                            dic.Add("invoice_number", tblInvoices.Rows[0]["invoice_number"].ToString());
                            dic.Add("is_send", false);
                            //dic.Add("send_date", DateTime.Now);
                            dic.Add("send_error", "");
                            dic.Add("send_status", "");

                            await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dic);
                        }
                    }

                    res.Add("ok", true);
                }

            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }

            return res;
        }

        public async Task<List<JObject>> DeleteInvMulti(List<JObject> models)
        {
            List<JObject> resList = new List<JObject>();
            JObject res;

            foreach (JObject model in models)
            {

                long invoice_id = -1;
                try
                {
                    invoice_id = Convert.ToInt64(model["invoice_id"].ToString());
                }
                catch (Exception ex)
                {
                    invoice_id = -1;
                }

                try
                {

                    Dictionary<string, object> dicParameter = new Dictionary<string, object>();

                    dicParameter.Add("invoice_id", invoice_id);

                    DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id", CommandType.Text, dicParameter);

                    if (tblInvoices.Rows.Count == 0)
                    {
                        res = new JObject();
                        res.Add("invoice_id", invoice_id);
                        res.Add("ok", false);
                        res.Add("error", "Không tìm thấy hóa đơn");
                        resList.Add(res);
                        continue;
                    }

                    dicParameter.Clear();
                    dicParameter.Add("invoice_id", invoice_id);

                    DataTable tblInvoiceExist = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_id=@invoice_id and status='Xóa bỏ'", CommandType.Text, dicParameter);

                    if (tblInvoiceExist.Rows.Count > 0)
                    {
                        res = new JObject();
                        res.Add("invoice_id", invoice_id);
                        res.Add("ok", false);
                        res.Add("error", "Hóa đơn đã ở trạng thái xóa bỏ.");
                        resList.Add(res);
                        continue;
                    }

                    //string inv_invoiceauth_id = tblInvoices.Rows[0]["inv_invoiceauth_id"].ToString();
                    string branch_code = tblInvoices.Rows[0]["branch_code"].ToString();

                    string buyer_email = model["buyer_email"].ToString();
                    string buyer_tel = model["buyer_tel"].ToString();

                    dicParameter.Clear();
                    dicParameter.Add("branch_code", branch_code);

                    string sql = "SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code  AND begin_date<=current_timestamp AND end_date>=current_timestamp";

                    DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                    if (tblCertificate.Rows.Count == 0)
                    {
                        res = new JObject();
                        res.Add("invoice_id", invoice_id);
                        res.Add("ok", false);
                        res.Add("error", "Không tìm thấy chứng thư số");
                        resList.Add(res);
                        continue;
                    }

                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    string cert_serial = tblCertificate.Rows[0]["cer_serial"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];

                    X509Certificate2 certFile = new X509Certificate2(bytes, pass);

                    string template_code = tblInvoices.Rows[0]["template_code"].ToString();
                    string invoice_series = tblInvoices.Rows[0]["invoice_series"].ToString();
                    string invoice_number = tblInvoices.Rows[0]["invoice_number"].ToString();

                    dicParameter.Clear();
                    dicParameter.Add("template_code", template_code);
                    dicParameter.Add("invoice_series", invoice_series);
                    dicParameter.Add("invoice_number", invoice_number);

                    sql = "SELECT inv_invoiceauth_id FROM #SCHEMA_NAME#.inv_invoiceauth WHERE template_code=@template_code  AND invoice_series=@invoice_series  AND invoice_number=@invoice_number";

                    if (branch_code.Length > 0)
                    {
                        dicParameter.Add("branch_code", branch_code);
                        sql += " AND branch_code=@branch_code";
                    }

                    DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                    if (tblInvoice.Rows.Count == 0)
                    {
                        res = new JObject();
                        res.Add("invoice_id", invoice_id);
                        res.Add("ok", false);
                        res.Add("error", "Không tìm thấy hóa đơn");
                        resList.Add(res);
                        continue;
                    }

                    Guid inv_invoiceauth_id = Guid.Parse(tblInvoice.Rows[0]["inv_invoiceauth_id"].ToString());

                    if (model["id"] == null)
                    {
                        model.Add("id", inv_invoiceauth_id);
                    }

                    JObject delInv = await GetInvoiceXoaBo(model);

                    if (delInv["error"] != null)
                    {
                        res = new JObject();
                        res.Add("invoice_id", invoice_id);
                        res.Add("ok", false);
                        res.Add("error", delInv["error"].ToString());
                        resList.Add(res);
                        continue;
                    }

                    string xml = delInv["InvoiceXmlData"].ToString();

                    xml = CommonManager.SignXml("#data", "seller", xml, certFile, "Signatures");

                    //xml = CommonManager.SignByMInvoice(xml, "Signatures");

                    delInv.Add("serialNumber", cert_serial);
                    delInv["InvoiceXmlData"] = xml;

                    delInv = await SaveInvoiceXoaBo(delInv);

                    if (delInv["error"] != null)
                    {
                        res = new JObject();
                        res.Add("invoice_id", invoice_id);
                        res.Add("ok", false);
                        res.Add("error", delInv["error"].ToString());
                        resList.Add(res);
                        continue;
                    }
                    else
                    {

                        if (buyer_email.Length > 0)
                        {
                            dicParameter.Clear();
                            dicParameter.Add("branch_code", branch_code);
                            sql = "SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type='Xóa bỏ hóa đơn' AND branch_code=@branch_code  LIMIT 1 OFFSET 0";
                            DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                            if (tblTemp.Rows.Count > 0)
                            {
                                dicParameter.Clear();
                                dicParameter.Add("inv_invoiceauth_id", inv_invoiceauth_id);

                                sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id)";
                                DataTable tblEmailBody = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                                DataRow row = tblEmailBody.Rows[0];

                                string noi_dung = tblTemp.Rows[0]["noi_dung"].ToString();
                                string subject = tblTemp.Rows[0]["subject"].ToString();

                                foreach (DataColumn column in tblEmailBody.Columns)
                                {
                                    string value = "";

                                    if (row[column.ColumnName] != DBNull.Value)
                                    {
                                        string dataType = column.DataType.Name.ToLower();

                                        if (dataType.Contains("datetime"))
                                        {
                                            DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                            value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                        }
                                        else
                                        {
                                            value = row[column.ColumnName].ToString();
                                        }
                                    }

                                    noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                                    subject = subject.Replace("#" + column.ColumnName + "#", value);
                                }

                                await _emailService.SendAsync(buyer_email, subject, noi_dung);

                                sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email(wb_log_email_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                                     + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                                Dictionary<string, object> parameters = new Dictionary<string, object>();
                                parameters.Add("branch_code", branch_code);
                                parameters.Add("send_date", DateTime.Now);
                                parameters.Add("from", _emailService.GetFrom());
                                parameters.Add("to", buyer_email);
                                parameters.Add("subject", subject);
                                parameters.Add("content", noi_dung);
                                parameters.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                                parameters.Add("user_send", _webHelper.GetUser());
                                parameters.Add("send_type", "Xóa bỏ hóa đơn");

                                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                            }
                        }

                        if (buyer_tel.Length > 0)
                        {
                            dicParameter.Clear();
                            dicParameter.Add("branch_code", branch_code);
                            sql = "SELECT * FROM #SCHEMA_NAME#.pl_tempsms WHERE yesno='C' AND tempsms_type='Xóa bỏ hóa đơn' AND branch_code=@branch_code LIMIT 1 OFFSET 0";
                            DataTable tblTemp = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                            if (tblTemp.Rows.Count > 0)
                            {
                                string noi_dung = tblTemp.Rows[0]["body"].ToString();
                                string subject = tblTemp.Rows[0]["subject"].ToString();

                                dicParameter.Clear();
                                dicParameter.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                                sql = "SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice('#SCHEMA_NAME#',@inv_invoiceauth_id)";
                                DataTable tblEmailBody = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                                DataRow row = tblEmailBody.Rows[0];

                                foreach (DataColumn column in tblEmailBody.Columns)
                                {
                                    string value = "";

                                    if (row[column.ColumnName] != DBNull.Value)
                                    {
                                        string dataType = column.DataType.Name.ToLower();

                                        if (dataType.Contains("datetime"))
                                        {
                                            DateTime dateTime = Convert.ToDateTime(row[column.ColumnName].ToString());
                                            value = string.Format("{0:dd/MM/yyyy}", dateTime);
                                        }
                                        else
                                        {
                                            value = row[column.ColumnName].ToString();
                                        }
                                    }

                                    noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                                    subject = subject.Replace("#" + column.ColumnName + "#", value);
                                }

                                sql = "INSERT INTO #SCHEMA_NAME#.wb_sms_queue(wb_sms_queue_id,mobile,subject,body,date_new,inv_invoiceauth_id,template_code,invoice_number,invoice_series,is_send,send_error,send_status) \n"
                                    + "VALUES(@wb_sms_queue_id,@mobile,@subject,@body,@date_new,@inv_invoiceauth_id,@template_code,@invoice_number,@invoice_series,@is_send,@send_error,@send_status)";

                                Dictionary<string, object> dic = new Dictionary<string, object>();

                                dic.Add("wb_sms_queue_id", Guid.NewGuid());
                                dic.Add("mobile", buyer_tel);
                                dic.Add("subject", subject);
                                dic.Add("body", noi_dung);
                                dic.Add("date_new", DateTime.Now);
                                dic.Add("inv_invoiceauth_id", inv_invoiceauth_id);
                                dic.Add("template_code", tblInvoices.Rows[0]["template_code"].ToString());
                                dic.Add("invoice_series", tblInvoices.Rows[0]["invoice_series"].ToString());
                                dic.Add("invoice_number", tblInvoices.Rows[0]["invoice_number"].ToString());
                                dic.Add("is_send", false);
                                //dic.Add("send_date", DateTime.Now);
                                dic.Add("send_error", "");
                                dic.Add("send_status", "");

                                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, dic);
                            }
                        }

                        res = new JObject();
                        res.Add("ok", true);
                        res.Add("invoice_id", invoice_id);
                        resList.Add(res);
                        continue;
                    }

                }
                catch (Exception ex)
                {
                    res = new JObject();
                    res.Add("invoice_id", invoice_id);
                    res.Add("ok", false);
                    res.Add("error", ex.Message);
                    resList.Add(res);
                    continue;

                }
            }

            return resList;
        }


        /**
         * Size: Kích thước security_number
         * Type: 1: hóa đơn; 2: phiếu thu
         */
        public async Task<string> GenerateSecurity(int size, int type)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            string securityNumber = "";
            bool isUsed = true;


            Dictionary<string, object> parameters = new Dictionary<string, object>();

            String sql = "";

            if (type == 1)
            {
                sql = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE security_number=@security_number";
            }
            else
            {
                sql = "SELECT * FROM #SCHEMA_NAME#.inv_receipt WHERE security_number=@security_number";
            }

            while (isUsed == true)
            {
                byte[] data = new byte[4 * size];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }
                StringBuilder result = new StringBuilder(size);
                for (int i = 0; i < size; i++)
                {
                    var rnd = BitConverter.ToUInt16(data, i * 4);
                    var idx = rnd % chars.Length;
                    result.Append(chars[idx]);
                }

                securityNumber = result.ToString();
                parameters.Clear();
                parameters.Add("security_number", securityNumber);

                DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblData.Rows.Count == 0)
                {
                    isUsed = false;
                }
            }

            return securityNumber;

        }
        public async Task<string> getTenDv()
        {
            string sql = "SELECT name FROM #SCHEMA_NAME#.wb_branch WHERE code ='" + _webHelper.GetDvcs() + "'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql);
            return dt.Rows[0][0].ToString();
        }

    }
}