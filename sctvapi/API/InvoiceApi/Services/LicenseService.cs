﻿using InvoiceApi.Data;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace InvoiceApi.Services
{
    public class LicenseService : ILicenseService
    {
        #region Fields

        private readonly IWebHelper _webHelper;
        private readonly INopDbContext _nopDbContext;
        private readonly IMinvoiceDbContext _minvoiceDbContext;

        #endregion

        public LicenseService(IWebHelper webHelper, INopDbContext nopDbContext, IMinvoiceDbContext minvoiceDbContext)
        {
            this._webHelper = webHelper;
            this._nopDbContext = nopDbContext;
            this._minvoiceDbContext = minvoiceDbContext;
        }

        public async Task<JObject> GetInfoLicense()
        {
            JObject result = new JObject();

            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("ma_dvcs", _webHelper.GetDvcs());

            string sql = "SELECT * FROM LicenseInfo WHERE ma_dvcs=@ma_dvcs AND key_license IS NOT NULL AND LicenseXmlInfo IS NOT NULL ORDER BY ngay_dk desc";
            DataTable tblLicenseInfo = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

            if (tblLicenseInfo.Rows.Count > 0)
            {
                string licenseXmlInfo = tblLicenseInfo.Rows[0]["LicenseXmlInfo"].ToString();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(licenseXmlInfo);

                string key_banquyen = doc.GetElementsByTagName("Key_banquyen")[0].InnerText;
                string sl_hd = doc.GetElementsByTagName("SoLuongHD")[0].InnerText;
                string tendonvi = doc.GetElementsByTagName("TenDonVi")[0].InnerText;
                string maSoThue = doc.GetElementsByTagName("MaSoThue")[0].InnerText;

                result.Add("ma_dvcs", _webHelper.GetDvcs());
                result.Add("ten_dvcs", tendonvi);
                result.Add("ms_thue", maSoThue);
                result.Add("sl_hd", sl_hd);
                result.Add("key_license", key_banquyen);

            }
            else
            {
                parameters.Clear();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());

                DataTable tblDmdvcs = await _minvoiceDbContext.GetDataTableAsync("SELECT code as ma_dvcs,name as ten_dvcs,tax_code as ms_thue FROM #SCHEMA_NAME#.wb_branch WHERE code=@ma_dvcs", CommandType.Text, parameters);

                result.Add("ma_dvcs", tblDmdvcs.Rows[0]["ma_dvcs"].ToString());
                result.Add("ten_dvcs", tblDmdvcs.Rows[0]["ten_dvcs"].ToString());
                result.Add("ms_thue", tblDmdvcs.Rows[0]["ms_thue"].ToString());
            }

            return result;
        }


        public JObject RegisterLicense(JObject model)
        {
            JObject obj = new JObject();

            try
            {

                string id = "";

                string ms_thue = model["ms_thue"].ToString();
                string ten_dvcs = model["ten_dvcs"].ToString();
                string sl_dk_them = model["sl_dk_them"].ToString();

                Dictionary<string, object> dicPara = new Dictionary<string, object>();

                dicPara.Add("ma_dvcs", _webHelper.GetDvcs());

                DataTable tblLicense = this._nopDbContext.ExecuteCmd("SELECT TOP 1 * FROM LicenseInfo WHERE ma_dvcs=@ma_dvcs AND (key_license IS NULL OR LicenseXmlInfo IS NULL) ORDER BY ngay_dk desc", CommandType.Text, dicPara);

                if (tblLicense.Rows.Count == 0)
                {
                    id = Guid.NewGuid().ToString();

                    dicPara.Clear();
                    dicPara.Add("license_id", id);
                    dicPara.Add("ngay_dk", DateTime.Now);
                    dicPara.Add("sl_dk_them", sl_dk_them);
                    dicPara.Add("ma_dvcs", _webHelper.GetDvcs());

                    string sql = "INSERT INTO LicenseInfo(sl_dk_them,ngay_dk,ma_dvcs,license_id) VALUES (@sl_dk_them,@ngay_dk,@ma_dvcs,@license_id)";

                    this._nopDbContext.ExecuteNoneQuery(sql, dicPara);
                }
                else
                {
                    if (sl_dk_them != tblLicense.Rows[0]["sl_dk_them"].ToString())
                    {
                        obj.Add("error", "Số lượng đăng ký thêm khác với số lượng đăng ký thêm gần nhất là " + tblLicense.Rows[0]["sl_dk_them"].ToString());
                        return obj;
                    }

                    id = tblLicense.Rows[0]["license_id"].ToString();
                }

                JObject json = new JObject();
                json.Add("id", id);
                json.Add("ms_thue", ms_thue);
                json.Add("ten_dv", ten_dvcs);
                json.Add("sl_hd", sl_dk_them);

                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                string url = ConfigurationManager.AppSettings["LICENSE_HOST"] + "/License/InsertLicense";
                JObject result = JObject.Parse(client.UploadString(url, json.ToString()));

                if (result["error"] != null)
                {
                    obj.Add("error", "Remote Host: " + result["error"]);
                    return obj;
                }

                obj.Add("ok", true);
                obj.Add("id", id);
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public JObject CheckLicense(string id)
        {
            JObject obj = new JObject();

            try
            {
                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                dicPara.Add("id", Guid.Parse(id));
                DataTable tblLicenseInfo = this._nopDbContext.ExecuteCmd("SELECT * FROM LicenseInfo WHERE license_id=@id  AND key_license IS NOT NULL AND LicenseXmlInfo IS NOT NULL", CommandType.Text, dicPara);

                if (tblLicenseInfo.Rows.Count > 0)
                {
                    string licenseXmlInfo = tblLicenseInfo.Rows[0]["LicenseXmlInfo"].ToString();

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(licenseXmlInfo);

                    string key_banquyen = doc.GetElementsByTagName("Key_banquyen")[0].InnerText;
                    string sl_hd = doc.GetElementsByTagName("SoLuongHD")[0].InnerText;

                    obj.Add("ok", true);
                    obj.Add("key_license", key_banquyen);
                    obj.Add("sl_hd", sl_hd);

                    return obj;
                }

                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;
                client.Headers.Add("Content-Type", "application/json");

                string url = ConfigurationManager.AppSettings["LICENSE_HOST"] + "/License/CheckLicense";

                JObject parameter = new JObject();
                parameter.Add("licenseinfo_id", id);

                JObject json = JObject.Parse(client.UploadString(url, parameter.ToString()));

                if (json["xml"] != null)
                {
                    string xml = json["xml"].ToString();

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);

                    string key_banquyen = doc.GetElementsByTagName("Key_banquyen")[0].InnerText;
                    string sl_hd = doc.GetElementsByTagName("SoLuongHD")[0].InnerText;

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("LicenseXmlInfo", xml);
                    parameters.Add("key_license", key_banquyen);
                    parameter.Add("license_id", id);

                    string sql = "UPDATE LicenseInfo SET LicenseXmlInfo=@LicenseXmlInfo,key_license=@key_license WHERE license_id=@id";

                    this._nopDbContext.ExecuteNoneQuery(sql, parameters);

                    obj.Add("ok", true);
                    obj.Add("key_license", key_banquyen);
                    obj.Add("sl_hd", sl_hd);

                }
                else
                {
                    obj.Add("error", "Bản quyền chưa được duyệt");
                }


            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }


        public async Task<byte[]> PrintLicenseInfo(string folder)
        {
            string fileReport = folder + "\\LicenseInfo.repx";

            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("ma_dvcs", _webHelper.GetDvcs());

            DataTable tblData = await this._minvoiceDbContext.GetDataTableAsync("select * from qr_inct_license (@ma_dvcs)", CommandType.Text, dicParam);
            tblData.TableName = "Table1";

            DataSet dataSet = new DataSet();
            dataSet.DataSetName = "DataSet";
            dataSet.Tables.Add(tblData);

            byte[] bytes = ReportUtil.PrintReport(dataSet, fileReport, "PDF");

            return bytes;
        }
    }
}