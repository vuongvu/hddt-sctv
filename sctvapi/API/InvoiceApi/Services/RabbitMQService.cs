﻿using InvoiceApi.Data;
using InvoiceApi.Util;
using log4net;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace InvoiceApi.Services
{
    public class RabbitMQService : IRabbitMQService, IDisposable
    {
        private string _queue_vendor_receive_van_confirm_technical = CommonConstants.QUEUE_VENDOR_RECEIVE_VAN_CONFIRM_TECHNICAL;
        private string _queue_vendor_receive_tct_confirm_technical = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_CONFIRM_TECHNICAL;
        private string _queue_vendor_receive_tct_response = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE;

        private IConnection _connection;
        private readonly IMinvoiceDbContext _minvoiceDbContext;
        private readonly IWebHelper _webHelper;

        public RabbitMQService(IMinvoiceDbContext minvoiceDbContext, IWebHelper webHelper)
        {
            this._minvoiceDbContext = minvoiceDbContext;
            this._webHelper = webHelper;
            Init();
        }


        public void Init()
        {
            //Connect();
        }

        public void Connect()
        {
            try
            {
                //var factory = new ConnectionFactory()
                //{
                //    HostName = ConfigurationManager.AppSettings["RABBITMQ_HOST"],
                //    UserName = ConfigurationManager.AppSettings["RABBITMQ_USER"],
                //    Password = ConfigurationManager.AppSettings["RABBITMQ_PASS"]
                //};

                //factory.AutomaticRecoveryEnabled = true;
                //_connection = factory.CreateConnection();

                if (IsOpen() == false)
                {
                    ConnectionFactory factory = new ConnectionFactory();
                    factory.UserName = ConfigurationManager.AppSettings["RABBITMQ_USER"];
                    factory.Password = ConfigurationManager.AppSettings["RABBITMQ_PASS"];
                    factory.VirtualHost = "/";
                    //factory.Protocol = Protocols.FromEnvironment();
                    factory.HostName = ConfigurationManager.AppSettings["RABBITMQ_HOST"];
                    factory.Port = AmqpTcpEndpoint.UseDefaultPort;
                    _connection = factory.CreateConnection();
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public bool IsOpen()
        {
            if (_connection == null) return false;

            return _connection.IsOpen;
        }

        public IConnection GetConnection()
        {
            return _connection;
        }

        public void PublishQueue(string queueName, string msg)
        {
            try
            {
                Connect();
                using (var channel = _connection.CreateModel())
                {
                    channel.ExchangeDeclare(queueName, "direct");

                    IDictionary<string, object> args = new Dictionary<string, object>();
                    args.Add("x-dead-letter-exchange", queueName);
                    args.Add("x-queue-type", "quorum");

                    channel.QueueDeclare(queue: queueName,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: args);

                    var body = Encoding.UTF8.GetBytes(msg);

                    var properties = channel.CreateBasicProperties();
                    properties.Persistent = true;

                    channel.BasicPublish(exchange: "",
                                         routingKey: queueName,
                                         basicProperties: properties,
                                         body: body);

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        public void PublishTaskQueue(string queueName, string msg)
        {
            try
            {
                Connect();
                using (var channel = _connection.CreateModel())
                {
                    IDictionary<string, object> args = new Dictionary<string, object>();
                    args.Add("x-queue-type", "quorum");

                    channel.QueueDeclare(queue: queueName,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: args);

                    var body = Encoding.UTF8.GetBytes(msg);

                    var properties = channel.CreateBasicProperties();
                    properties.Persistent = true;

                    channel.BasicPublish(exchange: "",
                                         routingKey: queueName,
                                         basicProperties: properties,
                                         body: body);

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_connection != null)
                {
                    if (_connection.IsOpen)
                    {
                        _connection.Close();
                    }

                    _connection.Dispose();
                }
            }

        }

    }
}