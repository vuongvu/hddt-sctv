﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using InvoiceApi.Data;
using InvoiceApi.Data.Domain;
using System.Threading.Tasks;
using System.Data.SqlClient;
using InvoiceApi.Util;
using Npgsql;
using System.Configuration;

namespace InvoiceApi.Services
{
    public class NopDbContext : INopDbContext
    {
        private readonly IWebHelper _webHelper;
        private InvoiceDbContext invoiceDbContext;
        private PostgresDbContext postgresDbContext;

        public NopDbContext(IWebHelper webHelper)
        {
            this._webHelper = webHelper;

            string connectionString = this.GetConnectionTenantString();

            postgresDbContext = new PostgresDbContext("public", connectionString);

            invoiceDbContext = new InvoiceDbContext(connectionString);

            //if (System.Configuration.ConfigurationManager.ConnectionStrings["InvoiceConnection"] != null)
            //{
            //    string invoiceConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["InvoiceConnection"].ConnectionString;
            //    invoiceDbContext = new InvoiceDbContext(invoiceConnectionString);
            //}
            //else
            //{
            //    string host = this._webHelper.GetRequest().Url.Host;

            //    string[] paths = host.Split('.');

            //    string mst = paths[0];

            //    TracuuHDDTContext tracuuDB = new TracuuHDDTContext(connectionString);

            //    if (mst == "localhost")
            //    {
            //        mst = "0102236276";
            //    }

            //    var inv_admin = tracuuDB.Inv_admin.Where(c => c.MST == mst || c.alias == mst).FirstOrDefault<inv_admin>();

            //    if (inv_admin == null)
            //    {
            //        throw new Exception("Không có thông tin kết nối");

            //    }
            //    else
            //    {
            //        invoiceDbContext = new InvoiceDbContext(inv_admin.ConnectString);
            //    }
            //}

        }

        //public string GetCurrentRequest()
        //{
        //    return this._context.Request.Url.OriginalString;
        //}

        public InvoiceDbContext GetInvoiceDb()
        {
            return this.invoiceDbContext;
        }

        public PostgresDbContext GetPostgresDb()
        {
            return this.postgresDbContext;
        }

        public DataTable GetStoreProcedureParameters(string storeProcedure)
        {

            if (CommonManager.CheckForSqlInjection(storeProcedure))
            {
                throw new Exception($"Dữ liệu không hợp lệ {storeProcedure}");
            }

            DataTable tblParameters = this.ExecuteCmd("SELECT p.*,t.[name] AS [type] FROM sys.procedures sp " +
                                    "JOIN sys.parameters p  ON sp.object_id = p.object_id " +
                                    "JOIN sys.types t  ON p.user_type_id = t.user_type_id " +
                                    "WHERE sp.name = '" + storeProcedure + "' and t.name<>'sysname'");

            return tblParameters;
        }

        public string ExecuteStoreProcedure(string sql, Dictionary<string, string> parameters)
        {
            DbConnection connection = null;

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = sql;


                if (CommonManager.CheckForSqlInjection(sql))
                {
                    throw new Exception($"Dữ liệu không hợp lệ {sql}");
                }


                DataTable tblParameters = this.ExecuteCmd("SELECT p.*,t.[name] AS [Type] FROM sys.procedures sp " +
                                    "JOIN sys.parameters p  ON sp.object_id = p.object_id " +
                                    "JOIN sys.types t  ON p.user_type_id = t.user_type_id " +
                                    "WHERE sp.name = '" + sql + "' and t.name<>'sysname'");

                for (int i = 0; i < tblParameters.Rows.Count; i++)
                {
                    DataRow row = tblParameters.Rows[i];
                    var para = parameters.Where(c => c.Key == row["name"].ToString().Substring(1)).FirstOrDefault();

                    var parameter = command.CreateParameter();
                    parameter.ParameterName = row["name"].ToString();
                    parameter.Value = para.Value;

                    command.Parameters.Add(parameter);
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return null;
        }

        public DataSet GetDataSet(string sql, Dictionary<string, string> parameters)
        {
            DbConnection connection = null;

            DataSet ds = new DataSet();
            ds.DataSetName = "dataSet1";

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = sql;


                if (CommonManager.CheckForSqlInjection(sql))
                {
                    throw new Exception($"Dữ liệu không hợp lệ {sql}");
                }

                DataTable tblParameters = this.ExecuteCmd("SELECT p.*,t.[name] AS [Type] FROM sys.procedures sp " +
                                    "JOIN sys.parameters p  ON sp.object_id = p.object_id " +
                                    "JOIN sys.types t  ON p.user_type_id = t.user_type_id " +
                                    "WHERE sp.name = '" + sql + "' and t.name<>'sysname'");

                for (int i = 0; i < tblParameters.Rows.Count; i++)
                {
                    DataRow row = tblParameters.Rows[i];
                    var para = parameters.Where(c => c.Key == row["name"].ToString().Substring(1)).FirstOrDefault();

                    var parameter = command.CreateParameter();
                    parameter.ParameterName = row["name"].ToString();

                    if (para.Value == null)
                    {
                        parameter.Value = DBNull.Value;
                    }
                    else
                    {
                        parameter.Value = para.Value;
                    }

                    command.Parameters.Add(parameter);
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                var reader = command.ExecuteReader();
                DataTable table = new DataTable();
                table.TableName = "Table";

                do
                {
                    table.Load(reader);

                } while (!reader.IsClosed);

                ds.Tables.Add(table);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return ds;
        }

        public DataTable ExecuteCmd(string sql)
        {
            DbConnection connection = null;

            var table = new DataTable();

            try
            {
                var invoiceDb = this.postgresDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandText = sql;

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                var reader = command.ExecuteReader();

                do
                {
                    table.Load(reader);

                } while (!reader.IsClosed);



            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return table;

        }

        public async Task<DataTable> ExecuteCmdAsync(string sql)
        {
            DbConnection connection = null;

            var table = new DataTable();

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandText = sql;

                if (connection.State == ConnectionState.Closed)
                {
                    await connection.OpenAsync();
                }

                var reader = command.ExecuteReader();

                do
                {
                    await Task.Run(() => { table.Load(reader); });

                } while (!reader.IsClosed);

                return table;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }


        public async Task<string> ExecuteStoreProcedureAsync(string sql, Dictionary<string, object> parameters)
        {
            DbConnection connection = null;

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = sql;

                if (CommonManager.CheckForSqlInjection(sql))
                {
                    throw new Exception($"Dữ liệu không hợp lệ {sql}");
                }

                DataTable tblParameters = await this.ExecuteCmdAsync("SELECT p.*,t.[name] AS [Type] FROM sys.procedures sp " +
                                    "JOIN sys.parameters p  ON sp.object_id = p.object_id " +
                                    "JOIN sys.types t  ON p.user_type_id = t.user_type_id " +
                                    "WHERE sp.name = '" + sql + "' and t.name<>'sysname'");

                for (int i = 0; i < tblParameters.Rows.Count; i++)
                {
                    DataRow row = tblParameters.Rows[i];
                    var para = parameters.Where(c => c.Key == row["name"].ToString().Substring(1)).FirstOrDefault();

                    var parameter = command.CreateParameter();
                    parameter.ParameterName = row["name"].ToString();
                    parameter.Value = para.Value;

                    command.Parameters.Add(parameter);
                }

                if (connection.State == ConnectionState.Closed)
                {
                    await connection.OpenAsync();
                }

                await command.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return null;
        }

        public void ExecuteNoneQuery(string sql)
        {
            DbConnection connection = null;

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandText = sql;

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                command.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }



        }

        public void ExecuteNoneQuery(string sql, Dictionary<string, object> parameters)
        {
            DbConnection connection = null;

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandText = sql;

                if (parameters != null)
                {
                    foreach (KeyValuePair<string, object> entry in parameters)
                    {
                        var parameter = command.CreateParameter();
                        parameter.ParameterName = entry.Key;
                        parameter.Value = entry.Value;

                        command.Parameters.Add(parameter);
                    }
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                command.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }



        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DbConnection connection = null;

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandText = sql;
                command.CommandType = commandType;

                if (parameters != null)
                {
                    foreach (KeyValuePair<string, object> entry in parameters)
                    {
                        var parameter = command.CreateParameter();
                        parameter.ParameterName = entry.Key;
                        parameter.Value = entry.Value;

                        command.Parameters.Add(parameter);
                    }
                }

                if (connection.State == ConnectionState.Closed)
                {
                    await connection.OpenAsync();
                }

                await command.ExecuteNonQueryAsync();


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return "";

        }


        public DataTable ExecuteCmd(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DbConnection connection = null;

            var table = new DataTable();

            try
            {
                var invoiceDb = this.invoiceDbContext;

                connection = invoiceDb.Database.Connection;
                var command = connection.CreateCommand();

                command.CommandText = sql;
                command.CommandType = commandType;

                if (parameters != null)
                {
                    foreach (KeyValuePair<string, object> entry in parameters)
                    {
                        var parameter = command.CreateParameter();
                        parameter.ParameterName = entry.Key;
                        parameter.Value = entry.Value;

                        command.Parameters.Add(parameter);
                    }
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                var reader = command.ExecuteReader();

                do
                {
                    table.Load(reader);

                } while (!reader.IsClosed);



            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return table;
        }


        private string GetConnectionTenantString()
        {
            return GetConnectionCache(this.GetTaxCodeSite());
        }

        private async Task<string> GetConnectionTenantAsyncString()
        {
            return await GetConnectionAsyncCache(this.GetTaxCodeSite());
        }

        public string GetTaxCodeSite()
        {
            return "dbo_uat";
            //return "dbo_sctv";
            string host = this._webHelper.GetRequest().Url.Host;

            string[] paths = host.Split('.');

            string taxCode = paths[0];

            return taxCode;

        }

        private string GetMasterConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MasterConnection"].ConnectionString;
        }

        public async Task<string> GetConnectionAsyncCache(string taxCode)
        {
            string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.tenant_config WHERE tax_code=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            DataTable tblData = new DataTable();

            string connectionString = this.GetMasterConnectionString();

            string connectionTenant = null;

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value;

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            if (tblData.Rows.Count > 0)
            {
                connectionTenant = tblData.Rows[0]["schema_url"].ToString();
            }

            return connectionTenant;
        }


        public string GetConnectionCache(string taxCode)
        {
            string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.tenant_config WHERE tax_code=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            DataTable tblData = new DataTable();

            string connectionString = this.GetMasterConnectionString();

            string connectionTenant = null;

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    var reader = command.ExecuteReader();
                    tblData.Load(reader);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            if (tblData.Rows.Count > 0)
            {
                connectionTenant = tblData.Rows[0]["schema_url"].ToString();
            }

            return connectionTenant;
        }




    }
}