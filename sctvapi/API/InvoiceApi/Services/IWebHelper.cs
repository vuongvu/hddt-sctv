﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace InvoiceApi.Services
{
    public partial interface IWebHelper
    {
        string GetUser();
        string GetDvcs();
        string GetLanguage();
        HttpRequestBase GetRequest();
        string GetUrlHost();
        string GetUrlApi();
    }
}
