﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceApi.Data.Domain;
using System.Data;
using InvoiceApi.Util;

namespace InvoiceApi.Services
{
    public partial interface IWbWindowService
    {
        Task<DataTableExtend> GetAll();
        Task<JObject> GetWindows(int start,int count,string filter);
        Task<JObject> SaveChange(JObject model);
        Task<JObject> Delete(string id);
    }
}
