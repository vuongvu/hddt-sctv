﻿using InvoiceApi.Data;
using InvoiceApi.Models;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using PkiTokenManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using DevExpress.XtraExport;
using NPOI.HPSF;

namespace InvoiceApi.Services
{
    public class TokenService : ITokenService
    {
        #region Fields

        private readonly IMinvoiceDbContext _minvoiceDbContext;
        private readonly INopDbContext _nopDbContext;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public TokenService(INopDbContext nopDbContext, IWebHelper webHelper, IMinvoiceDbContext minvoiceDbContext)
        {
            this._nopDbContext = nopDbContext;
            this._webHelper = webHelper;
            this._minvoiceDbContext = minvoiceDbContext;
        }

        #endregion

        #region Methods
        public async Task<JObject> SaveChukysoFile(JObject model)
        {
            JObject obj = new JObject();
            try
            {
                JArray jarray = (JArray)model["certs"];

                foreach (var json in jarray)
                {
                    if (json["chon"] != null)
                    {
                        bool chon = json["chon"].ToObject<Boolean>();

                        if (chon == true)
                        {
                            string path = json["file_path"].ToString();

                            X509Certificate2 cert = new X509Certificate2(HttpContext.Current.Server.MapPath(json["file_path"].ToString()), json["pass"].ToString());
                            string serial = json["serial_number"].ToString();
                            string issuer = cert.Issuer.ToString();
                            string subjectname = cert.SubjectName.Name.ToString();
                            string ngaybatdau = cert.NotBefore.ToString("yyyy-MM-dd");
                            string ngayketthuc = cert.NotAfter.ToString("yyyy-MM-dd");
                            string tokenserial = cert.SerialNumber.ToString();

                            Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                            dicParameter.Add("branch_code", _webHelper.GetDvcs());
                            dicParameter.Add("cer_serial", serial);

                            string sql1 = "SELECT count(*) FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code AND cer_serial=@cer_serial";
                            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql1, CommandType.Text, dicParameter);
                            if (Convert.ToInt16(dt.Rows[0][0]) > 0)
                            {
                                continue;
                            }
                            else
                            {
                                string sql = "INSERT INTO #SCHEMA_NAME#.pl_certificate(pl_certificate_id,branch_code,cer_serial,issuer,subject_name,begin_date,end_date,token_serial) VALUES(@pl_certificate_id,@branch_code,@cer_serial,@issuer,@subject_name,@begin_date,@end_date,@token_serial)";

                                Dictionary<string, object> parameters = new Dictionary<string, object>();

                                parameters.Add("pl_certificate_id", Guid.NewGuid());
                                parameters.Add("branch_code", _webHelper.GetDvcs());
                                parameters.Add("cer_serial", serial);
                                parameters.Add("issuer", issuer);
                                parameters.Add("subject_name", subjectname);
                                parameters.Add("begin_date", cert.NotBefore);
                                parameters.Add("end_date", cert.NotAfter);
                                parameters.Add("token_serial", serial);

                                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                            }
                        }
                    }
                }

                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }
            finally
            {
            }

            return obj;
        }

        public async Task<JArray> AllCertificatesFile()
        {
            JArray jarray = new JArray();
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tokenfile");
            jarray = JArray.FromObject(dt);
            return jarray;
        }

        public async Task<JObject> SaveTokenFile(JObject obj, string username, string store_code)
        {
            JObject json = new JObject();
            bool isStartTransaction = false;

            try
            {
                json.Add("windowid", obj["windowid"]);

                string windowno = (string)obj["windowid"];
                string editMode = (string)obj["editmode"];

                /* DataTable tblNhomQuyen = _nopDbContext.ExecuteCmd("SELECT wb_nhomquyen_id FROM wb_nhomquyen WHERE wb_nhomquyen_id IN (SELECT wb_nhomquyen_id FROM wb_user WHERE username=N'" + username + "')");

                 if (tblNhomQuyen.Rows.Count > 0)
                 {
                     DataTable tblCtQuyen = _nopDbContext.ExecuteCmd("SELECT * FROM wb_ctquyen WHERE wb_nhomquyen_id='" + tblNhomQuyen.Rows[0]["wb_nhomquyen_id"] + "' AND wb_menu_id IN (SELECT wb_menu_id FROM wb_menu WHERE window_id='" + windowno + "')");

                     if (tblCtQuyen.Rows.Count == 0)
                     {
                         if (_webHelper.GetUser() != "ADMINISTRATOR")
                         {
                             json.Add("error", "Nhóm chưa được phân quyền");
                             return json;
                         }
                     }
                     else
                     {

                         if (editMode == "1" && tblCtQuyen.Rows[0]["them"].ToString() != "C")
                         {
                             json.Add("error", "Bạn không có quyền thêm mới");
                             return json;
                         }

                         if (editMode == "2" && tblCtQuyen.Rows[0]["sua"].ToString() != "C")
                         {
                             json.Add("error", "Bạn không có quyền sửa");
                             return json;
                         }

                         if (editMode == "3" && tblCtQuyen.Rows[0]["xoa"].ToString() != "C")
                         {
                             json.Add("error", "Bạn không có quyền xóa");
                             return json;
                         }
                     }

                 }*/

                /*DataTable tblUser = _nopDbContext.ExecuteCmd("SELECT * FROM wb_user WHERE username=N'" + username + "'");
                string isedituser = tblUser.Rows[0]["isedituser"].ToString();
                string isdeluser = tblUser.Rows[0]["isdeluser"].ToString();

                if (editMode == "2" && isedituser != "C")
                {
                    json.Add("error", "Bạn không có quyền sửa hóa đơn của người dùng khác");
                    return json;
                }

                if (editMode == "3" && isdeluser != "C")
                {
                    json.Add("error", "Bạn không có quyền xóa hóa đơn của người dùng khác");
                    return json;
                }*/

                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];
                    // Lấy serial Token
                    string certFile = masterObj["file_path"].ToString();
                    certFile = HttpContext.Current.Server.MapPath(certFile);

                    string pass = masterObj["pass"].ToString();
                    //byte[] certBytes = GetResourceBytes("InvoiceApi.RsaKey.Minvoice.pfx");
                    X509Certificate2 cert = new X509Certificate2(certFile, pass);

                    string serial = cert.SerialNumber;

                    if (editMode == "1")
                    {
                        if (masterObj["branch_code"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["branch_code"].ToString()))
                            {
                                masterObj["branch_code"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                        }

                        if (masterObj["user_new"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_new"].ToString()))
                            {
                                masterObj["user_new"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_new", username);
                        }

                        if (masterObj["date_new"] != null)
                        {
                            masterObj["date_new"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_new", DateTime.Now);
                        }

                        if (masterObj["store_code"] != null)
                        {
                            masterObj["store_code"] = store_code;
                        }
                        else
                        {
                            masterObj.Add("store_code", store_code);
                        }
                        if (masterObj["serial_number"] != null)
                        {
                            masterObj["serial_number"] = serial;
                        }
                        else
                        {
                            masterObj.Add("serial_number", serial);
                        }
                        if (masterObj["serial_label"] != null)
                        {
                            masterObj["serial_label"] = serial;
                        }
                        else
                        {
                            masterObj.Add("serial_label", serial);
                        }
                        if (masterObj["wb_tokenfile_id"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["wb_tokenfile_id"].ToString()))
                            {
                                masterObj["wb_tokenfile_id"] = id;
                            }
                            else
                            {
                                id = Guid.Parse(masterObj["wb_tokenfile_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add("wb_tokenfile_id", id);
                        }
                    }

                    if (editMode == "2")
                    {
                        if (masterObj["branch_code"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["branch_code"].ToString()))
                            {
                                masterObj["branch_code"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                        }

                        if (masterObj["user_edit"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["user_edit"].ToString()))
                            {
                                masterObj["user_edit"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("user_edit", username);
                        }

                        if (masterObj["date_edit"] != null)
                        {
                            masterObj["date_edit"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("date_edit", DateTime.Now);
                        }
                        if (masterObj["store_code"] != null)
                        {
                            masterObj["store_code"] = store_code;
                        }
                        else
                        {
                            masterObj.Add("store_code", store_code);
                        }
                        if (masterObj["serial_number"] != null)
                        {
                            masterObj["serial_number"] = serial;
                        }
                        else
                        {
                            masterObj.Add("serial_number", serial);
                        }
                        if (masterObj["serial_label"] != null)
                        {
                            masterObj["serial_label"] = serial;
                        }
                        else
                        {
                            masterObj.Add("serial_label", serial);
                        }
                        id = Guid.Parse(masterObj["wb_tokenfile_id"].ToString());
                    }

                    string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_wb_tokenfile_insert" : editMode == "2" ? "#SCHEMA_NAME#.crd_wb_tokenfile_update" : "#SCHEMA_NAME#.crd_wb_tokenfile_delete";

                    await _minvoiceDbContext.BeginTransactionAsync();
                    isStartTransaction = true;

                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);


                    await _minvoiceDbContext.TransactionCommitAsync();

                    //Dictionary<string, object> parameters = new Dictionary<string, object>();
                    //parameters.Add("serial_number", serial);
                    //parameters.Add("serial_label", serial);
                    //parameters.Add("wb_tokenfile_id", id);

                    //string sqlUpdate = "UPDATE #SCHEMA_NAME#.wb_tokenfile SET serial_number=@serial_number,serial_label=@serial_label WHERE wb_tokenfile_id=@wb_tokenfile_id";
                    //await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdate, parameters);


                }

                json.Add("ok", "true");
                if (editMode != "3")
                {
                    Dictionary<string, object> dicParam = new Dictionary<string, object>();
                    dicParam.Add("wb_tokenfile_id", id);

                    DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync("SELECT *,wb_tokenfile_id as id FROM #SCHEMA_NAME#.wb_tokenfile WHERE wb_tokenfile_id=@wb_tokenfile_id", CommandType.Text, dicParam);
                    data = JArray.FromObject(tbl);
                    json.Add("data", data[0]);
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);

            }

            if (isStartTransaction)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
            }

            _minvoiceDbContext.CloseTransaction();

            return json;
        }
        public JArray SelectTokens()
        {
            JArray jarray = loadTokens();

            return jarray;
        }

        private JArray loadTokens()
        {
            JArray jarray = new JArray();

            PKI pki = PKI.GetPKI();

            if (pki == null)
            {
                throw new Exception("PKI NOT INIT");
            }

            int total = pki.GetListTokens().Count;

            for (int i = 0; i < total; i++)
            {
                var token = pki.GetListTokens()[i];

                string serialNumber = token.GetTokenSerialNumer().Replace("\0", "");

                JObject obj = new JObject();
                obj.Add("chon", false);
                obj.Add("serialNumber", serialNumber);
                obj.Add("tokenLabel", token.GetTokenLabel());

                jarray.Add(obj);
            }

            return jarray;
        }


        public JArray GetAllCertificates()
        {
            JArray jarray = new JArray();

            PKI pki = PKI.GetPKI();

            var tokens = pki.GetListTokens();

            foreach (var token in tokens)
            {
                List<X509Certificate2> lst = token.GetAllCertificates();

                foreach (var cert in lst)
                {
                    string tokenSerialNumber = token.GetTokenSerialNumer();

                    JObject obj = new JObject();
                    obj.Add("so_serial", cert.SerialNumber);
                    obj.Add("issuer", cert.Issuer);
                    obj.Add("subjectname", cert.Subject);
                    obj.Add("tokenSerial", token.GetTokenSerialNumer().Replace("\0", ""));
                    obj.Add("ngaybatdau", string.Format("{0:yyyy-MM-dd HH:mm:ss}", cert.NotBefore));
                    obj.Add("ngayketthuc", string.Format("{0:yyyy-MM-dd HH:mm:ss}", cert.NotAfter));

                    jarray.Add(obj);
                }

            }

            return jarray;
        }


        public async Task<JObject> SaveTokens(JObject model)
        {
            JObject obj = new JObject();

            try
            {
                string ma_dvcs = _webHelper.GetDvcs();
                JArray jarray = (JArray)model["tokens"];

                foreach (var json in jarray)
                {
                    bool chon = json["chon"].ToObject<Boolean>();

                    if (chon == true)
                    {
                        Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                        dicParameter.Add("branch_code", ma_dvcs);
                        dicParameter.Add("serial_number", json["serialNumber"].ToString());

                        string sql = "SELECT * FROM #SCHEMA_NAME#.wb_token WHERE branch_code=@branch_code AND serial_number=@serial_number";

                        DataTable tblWbToken = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                        if (tblWbToken.Rows.Count == 0)
                        {
                            sql = "INSERT INTO #SCHEMA_NAME#.wb_token(wb_token_id, serial_number, serial_label, pass, branch_code) \n"
                                + "VALUES (@wb_token_id, @serial_number, @serial_label, @pass, @branch_code)";

                            Dictionary<string, object> parameters = new Dictionary<string, object>();

                            parameters.Add("wb_token_id", Guid.NewGuid());
                            parameters.Add("serial_number", json["serialNumber"].ToString());
                            parameters.Add("serial_label", json["serialLabel"].ToString());
                            parameters.Add("pass", json["pass"].ToString());
                            parameters.Add("branch_code", ma_dvcs);

                            await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> SaveCerts(JObject model)
        {
            JObject obj = new JObject();

            try
            {
                string ma_dvcs = _webHelper.GetDvcs();

                JArray jarray = (JArray)model["certs"];

                foreach (var json in jarray)
                {
                    if (json["chon"] != null)
                    {
                        bool chon = json["chon"].ToObject<Boolean>();

                        if (chon == true)
                        {
                            Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                            dicParameter.Add("branch_code", ma_dvcs);
                            dicParameter.Add("cer_serial", json["so_serial"].ToString());

                            string sql = "SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code AND cer_serial=@cer_serial";
                            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                            if (tblData.Rows.Count == 0)
                            {
                                sql = "INSERT INTO #SCHEMA_NAME#.pl_certificate (pl_certificate_id,branch_code,cer_serial,issuer,subject_name,token_serial,begin_date,end_date) \n"
                                      + "VALUES (@pl_certificate_id,@branch_code,@cer_serial,@issuer,@subject_name,@token_serial,@begin_date,@end_date)";

                                Dictionary<string, object> paramerters = new Dictionary<string, object>();

                                paramerters.Add("pl_certificate_id", Guid.NewGuid());
                                paramerters.Add("branch_code", ma_dvcs);
                                paramerters.Add("cer_serial", json["so_serial"].ToString());
                                paramerters.Add("issuer", json["issuer"].ToString());
                                paramerters.Add("subject_name", json["subjectname"].ToString());
                                paramerters.Add("token_serial", json["tokenSerial"].ToString());
                                paramerters.Add("begin_date", Convert.ToDateTime(json["ngaybatdau"].ToString()));
                                paramerters.Add("end_date", Convert.ToDateTime(json["ngayketthuc"].ToString()));

                                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paramerters);
                            }
                        }
                    }
                }

                obj.Add("ok", true);

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public JObject SignXml(JObject model)
        {
            JObject result = new JObject();

            try
            {
                string xml = model["xml"].ToString();
                string inv_InvoiceAuth_id = model["inv_InvoiceAuth_id"].ToString();
                string refUri = model["refUri"].ToString();
                string certSerial = model["certSerial"].ToString();
                string tokenSerial = model["tokenSerial"].ToString();
                string signId = model["signId"].ToString();
                string signTag = model["signTag"].ToString();
                string pass = model["pass"].ToString();

                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = true;
                doc.LoadXml(xml);

                PKI pki = PKI.GetPKI();

                X509Certificate2 cert = pki.GetCertificate2(tokenSerial, certSerial);

                if (cert == null)
                {
                    result.Add("error", "Không tìm thấy chứng thư số");
                    return result;
                }

                ISignerProvider prd = new Pkcs11SignerProvider(certSerial, tokenSerial, pass);

                Pkcs11SignedXml signedXml = new Pkcs11SignedXml(doc);
                signedXml.Signature.Id = signId;

                KeyInfo keyInfo = new KeyInfo();

                RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

                KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                keyInfoData.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyInfoData);

                signedXml.KeyInfo = keyInfo;

                Reference reference = new Reference();
                reference.Uri = refUri;

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();

                reference.AddTransform(env);

                XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                reference.AddTransform(env1);

                signedXml.AddReference(reference);

                signedXml.ComputeSignature(prd);

                XmlElement xmlDigitalSignature = signedXml.GetXml();

                XmlNodeList nodeList = doc.GetElementsByTagName(signTag);

                foreach (XmlNode node in nodeList)
                {

                    node.AppendChild(xmlDigitalSignature);
                }

                xml = doc.OuterXml;

                result.Add("xml", xml);
                result.Add("inv_InvoiceAuth_id", inv_InvoiceAuth_id);
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);
            }

            return result;
        }

        public JObject SignTB01AC(JObject model)
        {
            JObject result = new JObject();

            try
            {
                string xml = model["xml"].ToString();
                string ns = model["ns"].ToString();
                string refUri = model["refUri"].ToString();
                string certSerial = model["certSerial"].ToString();
                string tokenSerial = model["tokenSerial"].ToString();
                string signId = model["signId"].ToString();
                string signTag = model["signTag"].ToString();
                string pass = model["pass"].ToString();

                xml = SignToKhai(certSerial, tokenSerial, pass, xml, signTag, refUri, ns);

                model["xml"] = xml;
                result = model;
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);
            }

            return result;
        }

        public async Task<string> UploadCertFileAsync(string ma_dvcs, string username, string pass, byte[] bytes, string fileName)
        {

            X509Certificate2 cert = new X509Certificate2(bytes, pass, X509KeyStorageFlags.MachineKeySet
                             | X509KeyStorageFlags.PersistKeySet
                             | X509KeyStorageFlags.Exportable);

            string certSerial = cert.SerialNumber;

            Dictionary<string, object> dicParameter = new Dictionary<string, object>();
            dicParameter.Add("branch_code", ma_dvcs);
            dicParameter.Add("cer_serial", certSerial);

            string sql = "SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code AND cer_serial=@cer_serial";
            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

            if (tblData.Rows.Count == 0)
            {
                sql = "INSERT INTO #SCHEMA_NAME#.pl_certificate (pl_certificate_id,branch_code,cer_serial,issuer,subject_name,token_serial,begin_date,end_date,file_name,cert_data,cert_type,pass) \n"
                      + "VALUES (@pl_certificate_id,@branch_code,@cer_serial,@issuer,@subject_name,@token_serial,@begin_date,@end_date,@file_name,@cert_data,@cert_type,@pass)";

                Dictionary<string, object> paramerters = new Dictionary<string, object>();

                paramerters.Add("pl_certificate_id", Guid.NewGuid());
                paramerters.Add("branch_code", ma_dvcs);
                paramerters.Add("cer_serial", certSerial);
                paramerters.Add("issuer", cert.IssuerName.Name);
                paramerters.Add("subject_name", cert.SubjectName.Name);
                paramerters.Add("token_serial", "");
                paramerters.Add("begin_date", cert.NotBefore);
                paramerters.Add("end_date", cert.NotAfter);
                paramerters.Add("file_name", fileName);
                paramerters.Add("cert_data", bytes);
                paramerters.Add("cert_type", "FILE");
                paramerters.Add("pass", pass);

                await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paramerters);
            }

            return "";
        }

        private string SignToKhai(string certSerial, string tokenSerial, string pass, string xml, string tag, string data, string ns)
        {

            XmlDocument document = new XmlDocument();

            document.PreserveWhitespace = true;
            document.LoadXml(xml);

            PKI pki = PKI.GetPKI();

            X509Certificate2 cert = pki.GetCertificate2(tokenSerial, certSerial);

            if (cert == null)
            {
                throw new Exception("Không tìm thấy chứng thư số");
            }


            ISignerProvider prd = new Pkcs11SignerProvider(certSerial, tokenSerial, pass);

            PrefixedSignedXML signedXml = new PrefixedSignedXML(document);
            signedXml.Prefix = ns;

            signedXml.Signature.Id = "seller";
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NWithCommentsTransformUrl;

            KeyInfo keyInfo = new KeyInfo();

            RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
            keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

            KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
            keyInfoData.AddSubjectName(cert.SubjectName.Name);
            keyInfo.AddClause(keyInfoData);

            signedXml.KeyInfo = keyInfo;

            Reference reference = new Reference();
            reference.Uri = data;

            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();

            reference.AddTransform(env);

            signedXml.AddReference(reference);
            signedXml.ComputeSignature(prd);

            XmlElement xmlDigitalSignature = signedXml.GetXml(ns);


            XmlNodeList nodeList = document.GetElementsByTagName(tag);

            foreach (XmlNode node in nodeList)
            {

                node.AppendChild(xmlDigitalSignature);
            }

            return document.OuterXml;

        }

        #endregion
        public async Task<JObject> SaveSimPKI(JObject model)
        {
            JObject obj = new JObject();
            try
            {
                string ma_dvcs = _webHelper.GetDvcs();

                string phone = model["phone"].ToString();
                if (Regex.IsMatch(phone, @"^\d+$"))
                {
                    if (phone[0] == '0') phone = "84" + phone.Substring(1);
                    else if (phone.Substring(0, 2) != "84") phone = "84" + phone;
                    ResultCheckSimPKI resultCheck;

                    Dictionary<string, object> dicParameter = new Dictionary<string, object>();
                    dicParameter.Add("branch_code", ma_dvcs);
                    dicParameter.Add("cer_serial", phone);

                    string sql = "SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code AND token_serial=@cer_serial";
                    DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicParameter);

                    if (tblData.Rows.Count == 0)
                    {
                        resultCheck = CommonManager.CheckSimPKI(phone);
                        if (resultCheck.Status == "ok")
                        {
                            sql = "INSERT INTO #SCHEMA_NAME#.pl_certificate (pl_certificate_id,branch_code,cer_serial,issuer,subject_name,token_serial,begin_date,end_date,cert_type) \n"
                                    + "VALUES (@pl_certificate_id,@branch_code,@cer_serial,@issuer,@subject_name,@token_serial,CAST(@begin_date as timestamp),CAST(@end_date as timestamp),@cert_type)";

                            Dictionary<string, object> paramerters = new Dictionary<string, object>();

                            paramerters.Add("pl_certificate_id", Guid.NewGuid());
                            paramerters.Add("branch_code", ma_dvcs);
                            paramerters.Add("cer_serial", resultCheck.serial);
                            paramerters.Add("issuer", resultCheck.issuer);
                            paramerters.Add("subject_name", resultCheck.X509SubjectName);
                            paramerters.Add("token_serial", phone);
                            paramerters.Add("begin_date", resultCheck.dtStart);
                            paramerters.Add("end_date", resultCheck.dtEnd);
                            paramerters.Add("cert_type", "SIM");
                            await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, paramerters);
                            obj.Add("ok", true);
                        }
                        else
                        {
                            obj.Add("error", resultCheck.StrError);
                        }
                    }

                    else
                    {
                        obj.Add("error", "Đã tồn tại số điện thoại lưu trong hệ thống");
                    }
                }
                else
                {
                    obj.Add("error", "Bạn nhập sai định dạng");
                }
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return obj;
        }

        public async Task<JObject> SaveHsmSCTV(JObject model)
        {
            JObject result = new JObject();
            try
            {
                var certSerial = model["certSerial"].ToString();
                //Kiểm tra tồn tại hsm
                var branchCode = _webHelper.GetDvcs();
                var sql = $"SELECT cer_serial FROM #SCHEMA_NAME#.pl_certificate WHERE cer_serial = '{certSerial}' AND branch_code = '{branchCode}'";
                DataTable rsTable = await _minvoiceDbContext.GetDataTableAsync(sql);
                if (rsTable.Rows.Count > 0)
                {
                    result.Add("error", "Cert serial đã tồn tại");
                    return result;
                }

                //Lấy và lưu thông tin cert hsm
                var baseUrl = CommonConstants.BASE_HSM_URL;
                var apiUrl = CommonConstants.CTS_HSM;
                JObject request = new JObject();
                request.Add("SeriaNumber", certSerial);
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(baseUrl);
                HttpResponseMessage response = await client.PostAsJsonAsync(apiUrl, request);
                string content = await response.Content.ReadAsStringAsync();

                JObject res = JObject.Parse(content);

                if ("false".Equals(res["status"]?.ToString().ToLower()))
                {
                    throw new Exception(result["message"]?.ToString());
                }

                var sqlUpdate =
                    $@"INSERT INTO  #SCHEMA_NAME#.pl_certificate (pl_certificate_id, cer_serial, issuer, subject_name, begin_date, end_date, branch_code, cert_type)
                     VALUES ('{Guid.NewGuid()}', '{certSerial}', '{res["issuer"]}', '{res["subjectname"]}', '{res["ngaybatdau"]}', '{res["ngayketthuc"]}', '{_webHelper.GetDvcs()}', 'HSM_SCTV')";
                _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
                result.Add("status", "ok");

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                result.Add("error", ex.Message);
                return result;
            }
        }
    }
}