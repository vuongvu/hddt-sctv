﻿using InvoiceApi.Data;
using InvoiceApi.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InvoiceApi.Services
{
    public class BcthsdService : IBcthsdService
    {
        private INopDbContext _nopDbContext;
        private IInvoiceService _invoiceService;
        private readonly IMinvoiceDbContext _minvoiceDbContext;
        private readonly IWebHelper _webHelper;

        public BcthsdService(INopDbContext nopDbContext, IInvoiceService invoiceService, IMinvoiceDbContext minvoiceDbContext, IWebHelper webHelper)
        {
            this._nopDbContext = nopDbContext;
            this._invoiceService = invoiceService;
            this._minvoiceDbContext = minvoiceDbContext;
            this._webHelper = webHelper;
        }

        public async Task<JObject> GetInfoDonvi()
        {
            JObject json = new JObject();

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("code", _webHelper.GetDvcs());

                DataTable dvcs = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code =@code", CommandType.Text, parameters);

                parameters.Clear();
                parameters.Add("username", _webHelper.GetUser());

                DataTable user = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username =@username", CommandType.Text, parameters);

                JArray dmdvcs = JArray.FromObject(dvcs);
                JArray username = JArray.FromObject(user);
                string nguoi_dai_dien = null;
                string ten_nguoi_sd = null;

                json.Add("nguoi_dai_dien", String.IsNullOrEmpty(dmdvcs[0]["manage_unit_name"].ToString()) ? nguoi_dai_dien : dmdvcs[0]["manage_unit_name"].ToString());
                json.Add("nguoi_lap", String.IsNullOrEmpty(username[0]["fullname"].ToString()) ? ten_nguoi_sd : username[0]["fullname"].ToString());

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }
        public async Task<JObject> GetTenFile()
        {
            JObject json = new JObject();

            string ma_dvcs = _webHelper.GetDvcs();

            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("code", ma_dvcs);

            DataTable dt = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code =@code", CommandType.Text, parameters);
            //var dt = invoiceDB.Dmdvcss.Where(c => c.ma_dvcs == ma_dvcs).FirstOrDefault<dmdvcs>();
            string ma_cqthuecaptinh = dt.Rows[0]["provincial_tax_authorities"].ToString();

            parameters.Clear();
            parameters.Add("tax_authorities_manage", ma_cqthuecaptinh);

            DataTable mathue = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.sl_tax_authorities_city WHERE tax_authorities_manage=@tax_authorities_manage", CommandType.Text, parameters);
            JArray msthuecapquanly = JArray.FromObject(mathue);
            string ma = msthuecapquanly[0]["code"].ToString();
            //JObject jj = JObject.FromObject(dt);
            string ms_thue = dt.Rows[0]["tax_code"].ToString();
            string mst = "";
            if (ms_thue.Length == 10)
            {
                mst = ms_thue + "000";
            }
            else
            {
                if (ms_thue.Length == 14)
                {
                    string[] a = ms_thue.Split('-');
                    mst = a[0] + a[1];
                }
                else
                {
                    mst = ms_thue;
                }
            }
            string tenfile = ma + "-" + mst + "-BC26_AC";
            json.Add("name", tenfile);
            return json;
        }
        public async Task<JObject> Check_xulyHD(string ma_dvcs, JObject model)
        {
            JObject json = new JObject();

            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("branch_code", ma_dvcs);
            parameters.Add("invoice_issued_date_before", DateTime.Parse(model["tu_ngay"].ToString()));
            parameters.Add("invoice_issued_date_after", DateTime.Parse(model["den_ngay"].ToString()));

            string sql = "SELECT COUNT(*) FROM #SCHEMA_NAME#.inv_invoiceauth WHERE branch_code=@branch_code AND (status='Chờ ký' OR status='Chờ duyệt' OR status='Chờ sinh số') AND invoice_status<>7 AND invoice_status<>15 AND invoice_issued_date>=@invoice_issued_date_before AND invoice_issued_date<=@invoice_issued_date_after";
            DataTable dt = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
            int k = Convert.ToInt16(dt.Rows[0][0]);

            if (k != 0)
            {
                json.Add("error", "Bạn có " + k + " hóa đơn đang chờ xử lý !");
            }
            else
            {
                parameters.Clear();
                parameters.Add("branch_code", ma_dvcs);

                string sql2 = "SELECT name as TEN_DVCS, tax_code as MS_THUE FROM #SCHEMA_NAME#.wb_branch WHERE code =@branch_code";
                DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(sql2, CommandType.Text, parameters);
                JArray arr = JArray.FromObject(tbl);
                json = (JObject)arr[0];
            }
            return json;
        }

        public async Task<JArray> BcTinhhinhsudung(string ma_dvcs, JObject model)
        {
            JObject obj = new JObject();
            string kieu = "";
            int loai = 1;
            try
            {
                if (model["tokhai_thang"].ToString() == "1")
                {
                    kieu = "T";
                }
                else
                {
                    kieu = "Q";
                }
                if (model["bc_soluong"].ToString() == "0")
                {
                    loai = 1;
                }
                else { loai = 2; }

                string lst_branch_code = model["lst_branch_code"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", ma_dvcs);
                parameters.Add("kieu", kieu);
                parameters.Add("nam", Convert.ToInt32(model["nam"].ToString()));
                parameters.Add("thang", Convert.ToInt32(model["thang"].ToString()));
                parameters.Add("tu_ngay", DateTime.Parse(model["tu_ngay"].ToString()));
                parameters.Add("den_ngay", DateTime.Parse(model["den_ngay"].ToString()));
                parameters.Add("loai", loai);
                parameters.Add("lst_branch_code", lst_branch_code);

                string sql = "SELECT * FROM #SCHEMA_NAME#.qr_bc_thsdhd('#SCHEMA_NAME#',@ma_dvcs,'',@kieu,@nam,@thang,@tu_ngay::date,@den_ngay::date,@loai,@lst_branch_code)";
                DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray data = JArray.FromObject(tbl);

                return data;

            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
            }

            return null;
        }

        public async Task<JObject> bcthsd_INSERT(JObject model)
        {
            JObject json = new JObject();
            bool isStartTransacion = false;

            try
            {

                JArray details = (JArray)model["data"];

                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("report_type", details[0]["kieu"].ToString());
                parameters.Add("branch_code", details[0]["ma_dvcs"].ToString());
                parameters.Add("year", Convert.ToInt32(details[0]["nam"].ToString()));
                parameters.Add("thang", Convert.ToInt32(details[0]["thang"].ToString()));
                parameters.Add("type", Convert.ToInt32(details[0]["loai"].ToString()));

                string delete = "DELETE FROM #SCHEMA_NAME#.inv_invoice_report WHERE report_type=@report_type AND branch_code=@branch_code AND year=@year AND thang=@thang AND type=@type";
                await _minvoiceDbContext.ExecuteNoneQueryAsync(delete, parameters);

                foreach (JObject js_details in details)
                {

                    JObject j = FunctionUtil.Bathsd(js_details);
                    string sql = "#SCHEMA_NAME#.crd_inv_invoice_report_insert";

                    await _minvoiceDbContext.BeginTransactionAsync();
                    isStartTransacion = true;

                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, js_details);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }

                json.Add("ok", "Ghi dữ liệu thành công !");
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            if (isStartTransacion)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
            }

            _minvoiceDbContext.CloseTransaction();

            return json;
        }

        public JObject ExportXMLString(string ma_dvcs, JObject model)
        {
            string kieu = "T";
            if (model["tokhai_quy"].ToString() == "1")
            {
                kieu = "Q";
            }
            if (model["tokhai_thang"].ToString() == "1")
            {
                kieu = "T";
            }
            JObject json = new JObject();



            try
            {
                DataTable tblResult = this._nopDbContext.ExecuteCmd("EXECUTE sproc_bcthsdhd_sendData N'" + ma_dvcs + "','" + kieu + "','" + model["nam"].ToString() + "','" + model["thang"].ToString() + "','" + model["tu_ngay"].ToString() + "','" + model["den_ngay"].ToString() + "'");

                if (tblResult.Rows.Count > 0)
                {
                    string xml = tblResult.Rows[0][0].ToString();
                    json.Add("xml", xml);
                    // byte [] a= System.Text.UTF8Encoding.UTF8.GetBytes(json["xml"].ToString());
                }
                else
                {
                    json.Add("error", "Không tải được file XML");
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return json;

        }

        public async Task<string> GetStringXML(string ma_dvcs, string tokhai_quy, string tokhai_thang, string nam, string thang, string tu_ngay, string den_ngay)
        {
            string kieu = "T";

            string xml = "";

            if (tokhai_quy.ToString() == "1")
            {
                kieu = "Q";
            }
            if (tokhai_thang.ToString() == "1")
            {
                kieu = "T";
            }
            JObject json = new JObject();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("ma_dvcs", _webHelper.GetDvcs());
            parameters.Add("kieu", kieu);
            parameters.Add("nam", Convert.ToInt32(nam));
            parameters.Add("thang", Convert.ToInt32(thang));
            parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
            parameters.Add("den_ngay", DateTime.Parse(den_ngay));

            try
            {
                string sql = "SELECT * FROM #SCHEMA_NAME#.qr_bcthsdhd_senddata('#SCHEMA_NAME#',@ma_dvcs,'',@kieu,@nam,@thang,@tu_ngay::date,@den_ngay::date)";
                DataTable tblResult = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                if (tblResult.Rows.Count > 0)
                {
                    xml = tblResult.Rows[0][0].ToString();
                    json.Add("xml", xml);
                    // byte [] a= System.Text.UTF8Encoding.UTF8.GetBytes(json["xml"].ToString());
                }
                else
                {
                    json.Add("error", "Không tải được file XML");
                    xml = "";
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }

            return xml;

        }
        public async Task<JObject> bangkebanra(JObject model)
        {
            JObject json = new JObject();
            try
            {
                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string ky_hieu = model["ky_hieu"].ToString();
                string lst_branch_code = model["lst_branch_code"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));
                parameters.Add("ky_hieu", ky_hieu);
                parameters.Add("lst_branch_code", lst_branch_code);

                //var invoiceDb = this._nopDbContext.GetInvoiceDb();
                string sql = "SELECT * FROM #SCHEMA_NAME#.qr_bc_bkbr_tr('#SCHEMA_NAME#',@ma_dvcs,null,@tu_ngay,@den_ngay, @ky_hieu,@lst_branch_code)";
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray jarr = JArray.FromObject(dt);
                json.Add("data", jarr);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }
    }
}