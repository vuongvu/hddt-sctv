﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApi.Services
{
    public partial interface ISystemService
    {
        string GetMST();
        JArray GetMenuBar();
        Task<JArray> GetMenuBarByUser(string username);

        JArray ExecuteQuery(string sql);
        Task<JArray> ExecuteQueryAsync(string sql);

        Task<JArray> GetTypeSQL();
        Task<string> GetConfigWinByNo(string windowNo);
        Task<JObject> GetDataByWindowNo(string ma_dvcs, string userLogin, string window_id, int start, int count, string filter, string infoparam, string tlbparam);
        Task<JObject> GetDataByWindowNo(string ma_dvcs, string userLogin, string window_id, int start, int count, JArray filter, JObject infoparam, JArray tlbparam);
        Task<JObject> GetDataById(string window_id, string id);

        Task<JArray> GetDataReferencesByRefId(string no, string ma_dvcs);
        Task<JArray> GetDataReferencesByRefId(string no, string filtervalue, string ma_dvcs);
        Task<JArray> GetRecordByReferencesId(string id, string value);
        Task<JArray> GetDataByReferencesId(string no, string filtervalue, string ma_dvcs);
        Task<JArray> GetDataDetailsByTabTable(string window_id, Guid id, string tab_table);

        Task<JArray> ExecuteCommand(JObject obj, string command);

        Task<JObject> Save(JObject obj, string username);

        Task<JObject> SaveAndCreateNumber(JObject obj, string username);
        Task<JObject> SaveSignXMLEasyCA(JObject obj, string username);
        Task<JObject> SaveSignInvoiceCertFile(JObject obj, string username);
        Task<JObject> DeleteMultiChoice(JObject obj, string username);
        Task<JObject> InsertFieldsByTabId(Guid id);
        Task<JObject> PhanQuyen(JObject model);

        Task<bool> CheckBranchValidation(string value);
        Task<JObject> RegisterLicenceBranch(JObject model);

        JObject AutoCreateTable(Guid id);
        Task<JObject> AutoCreateCmd(Guid id);

        Task<byte[]> InChungTu(JObject model, string folder);
        Task<byte[]> CreateReport(JObject model);
        Task<byte[]> DownloadFileExcelTemplate(string excel_id);

        Task<JObject> SaveInfoCompany(JObject model);
        Task<JArray> GetInfoCompany(string branch_code);

        Task<string> UploadExcelAsync(string ma_dvcs, string user_login, string excel_id, byte[] bytes, Dictionary<string, object> formData);

        Task<string> UploadExcelTemplateAsync(string ma_dvcs, string user_login, byte[] bytes, Dictionary<string, object> formData);

        Task<JArray> GetMenuBarByUser2(string username);
        Task<JArray> GetInvoiceDetail(string id);

        Task<JObject> SaveContract();


        Task<JObject> CreateNewCustomers(string maSoThue);
        Task<JObject> ServicesIntegratedIntoTheMarketPlace(JObject model);

        Task<JObject> SavePretreatment(JObject obj, string username);
        Task<JObject> SaveAndCreateNumberPretreatment(JObject obj, string username);

        Task<bool> CheckInvoiceCancel(JObject obj);
        Task<JObject> CheckInvoiceCancel(List<string> valueList);

        Task<JArray> GetDmDvcsByUser();

        Task<JObject> UploadExcelListUnitAsync(byte[] filesReadToProvider);
        Task<JObject> GetBusinessInfoByTaxCode(string taxCode);
        void SaveBusinessInfo(JObject resultObject);

        Task<JObject> UpdateInfoCompany(JObject model);
    }
}
