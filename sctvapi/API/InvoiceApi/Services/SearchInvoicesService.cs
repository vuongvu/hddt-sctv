﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace InvoiceApi.Services
{
    public class SearchInvoicesService : ISearchInvoicesService
    {
        private readonly INopDbContext _nopDbContext;

        public SearchInvoicesService(INopDbContext nopDbContext)
        {
            this._nopDbContext = nopDbContext;
        }

        public JArray FindByCustomer(JObject model)
        {
            string sql = "SELECT * FROM inv_InvoiceAuth WHERE 1=1 ";

            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("ma_dvcs", model["ma_dvcs"].ToString());
            dicParam.Add("ma_dt", model["ma_dt"].ToString());

            sql = sql + " AND ma_dvcs=@ma_dvcs ";
            sql = sql + " AND ma_dt=@ma_dt ";

            if (model["sobaomat"] != null)
            {
                dicParam.Add("sobaomat", model["sobaomat"].ToString());
                sql = sql + " AND sobaomat=@sobaomat ";
            }

            if (model["tu_ngay"] != null)
            {
                dicParam.Add("inv_invoiceIssuedDate_before", DateTime.Parse(model["tu_ngay"].ToString()));
                sql = sql + " AND inv_invoiceIssuedDate>=@inv_invoiceIssuedDate_before ";
            }

            if (model["den_ngay"] != null)
            {
                dicParam.Add("inv_invoiceIssuedDate_after", DateTime.Parse(model["den_ngay"].ToString()));
                sql = sql + " AND inv_invoiceIssuedDate<=@inv_invoiceIssuedDate_after ";
            }

            DataTable tblInvoices = this._nopDbContext.ExecuteCmd(sql, CommandType.Text, dicParam);

            JArray result = JArray.FromObject(tblInvoices);

            return result;

        }
    }
}