﻿using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.BarCode;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraReports.UI;
using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Zip;
using InvoiceApi.Data;
using InvoiceApi.Models;
using InvoiceApi.Services;
using InvoiceApi.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Syncfusion.Pdf;
using System.IO.Compression;
using System.Configuration;

namespace InvoiceApi.Services68
{
    public class InvoiceService68 : IInvoiceService68
    {
        private string _queue_vendor_request_van = CommonConstants.QUEUE_VENDOR_SEND_REQUEST_TO_VAN;

        private readonly IMinvoiceDbContext _minvoiceDbContext;
        private readonly IMasterInvoiceDbContext _masterInvoiceDbContext;
        private readonly INopDbContext _nopDbContext;
        private readonly IInvoiceService _invoiceService;
        private readonly IWebHelper _webHelper;
        private readonly IRabbitMQService _rabbitMQService;
        private IEmailService _emailService;
        //private readonly IKafkaService _kafkaService;

        public InvoiceService68(INopDbContext nopDbContext,
                            IInvoiceService invoiceService,
                            IWebHelper webHelper,
                            IMinvoiceDbContext minvoiceDbContext,
                            IRabbitMQService rabbitMQService,
                            IEmailService emailService,
                            IMasterInvoiceDbContext masterInvoiceDbContext
                            )
        {
            this._nopDbContext = nopDbContext;
            this._invoiceService = invoiceService;
            this._webHelper = webHelper;
            this._minvoiceDbContext = minvoiceDbContext;
            this._rabbitMQService = rabbitMQService;
            this._emailService = emailService;
            _masterInvoiceDbContext = masterInvoiceDbContext;
        }

        public async Task<JArray> GetSignType()
        {
            string qry = $"SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE code='KYTAPTRUNG' AND branch_code='{_webHelper.GetDvcs()}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            JArray arr = JArray.FromObject(dt);
            return arr;
        }

        public async Task<string> ThongDiepGui(string cmd, string xml)
        {
            string vendor = CommonConstants.VENDOR;
            string host = _webHelper.GetRequest().Url.Host;
            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string privateKey = CommonConstants.PRIVATE_KEY;

            string hashMessage = cmd + ":" + vendor + ":" + host + ":" + timestamp + ":" + privateKey;

            var token = FunctionUtil.ComputeSha256Hash(FunctionUtil.ComputeSha256Hash(xml) + ":" + hashMessage);

            string originalString = _webHelper.GetRequest().Url.OriginalString;
            string path = originalString.StartsWith("/api") ? "~/api/Content/XML/XMLThongDiep.xml" : "~/Content/XML/XMLThongDiep.xml";

            var fileName = System.Web.HttpContext.Current.Server.MapPath(path);

            string thongDiepXml = "";

            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var sr = new StreamReader(stream, Encoding.UTF8))
                {
                    thongDiepXml = sr.ReadToEnd();
                }
            }

            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = false;

            doc.LoadXml(thongDiepXml);

            doc.GetElementsByTagName("MLTDiep")[0].InnerText = cmd;
            doc.GetElementsByTagName("Vendor")[0].InnerText = vendor;
            doc.GetElementsByTagName("Site")[0].InnerText = host;
            doc.GetElementsByTagName("Timestamp")[0].InnerText = timestamp;
            doc.GetElementsByTagName("Token")[0].InnerText = token;
            doc.GetElementsByTagName("DLTDiep")[0].InnerText = xml;

            return doc.OuterXml;

        }

        public async Task<JObject> GetEmailTemplate(string type, string id, string ma_dvcs, string originalString)
        {
            JObject result = new JObject();
            try
            {
                DataTable hoadon68 = await _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='{id}'");
                if (hoadon68.Rows.Count == 0)
                {
                    throw new Exception("Hóa đơn không tồn tại");
                }

                if (string.IsNullOrEmpty(hoadon68.Rows[0]["nky"].ToString()))
                {
                    throw new Exception("Hóa đơn chưa được ký");
                }
                string customer_code = hoadon68.Rows[0]["mnmua"].ToString();
                string branch_code = hoadon68.Rows[0]["mdvi"].ToString();

                DataTable tblTemp = await _minvoiceDbContext.GetDataTableAsync("SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type=@P_0 AND branch_code=@P_1 LIMIT 1 OFFSET 0", type, ma_dvcs);

                string pass1 = tblTemp.Rows[0]["pass"].ToString();
                string alias = tblTemp.Rows[0]["alias"].ToString();

                JArray array = JArray.FromObject(tblTemp);

                result = (JObject)array[0];
                result["pass"] = pass1;
                result["alias"] = alias;

                DataTable tblEmailBody = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice_68('#SCHEMA_NAME#',@P_0)", Guid.Parse(id));
                DataTable tblEmailSubject = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_subject_email_invoice_68('#SCHEMA_NAME#',@P_0)", Guid.Parse(id));

                // get thông tin đơn vị

                string mdvi = hoadon68.Rows[0]["mdvi"]?.ToString();
                string tendvi = hoadon68.Rows[0]["ten"]?.ToString();

                DataTable tblBranch = await _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code = '{mdvi}'");

                string noi_dung = result["noi_dung"].ToString();
                string tieude = result["subject"].ToString();

                if (type == "Ký xác nhận")
                {
                    var hostEmail = _webHelper.GetUrlHost();
                    string url = originalString + "/Content/App/SignInvoice/SignInvoiceApplication.application?type=07&id=" + id;
                    noi_dung = noi_dung.Replace("{URL_CONFIRM}", url + "type=78&mst=" + hostEmail);
                    noi_dung = noi_dung.Replace("{ID_INV}", id);
                }

                string total_amount = DoubleStrToString(hoadon68.Rows[0]["tgtttbso_last"].ToString());
                CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
                string total_amount_convert = double.Parse(total_amount).ToString("#,###", cul.NumberFormat);
                noi_dung = noi_dung.Replace("#total_amount#", total_amount_convert);

                DataRow row = tblEmailBody.Rows[0];

                foreach (DataColumn column in tblEmailBody.Columns)
                {
                    if (column.ColumnName == "url_confirm")
                    {
                        var hostEmail = _webHelper.GetUrlHost();
                        string value = row[column.ColumnName].ToString();
                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value + "&type=78&mst=" + hostEmail);
                    }
                    else
                    {
                        string value = row[column.ColumnName].ToString();
                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                    }
                }

                DataRow row_sub = tblEmailSubject.Rows[0];
                foreach (DataColumn column in tblEmailSubject.Columns)
                {
                    string value = row_sub[column.ColumnName].ToString();
                    tieude = tieude.Replace("#" + column.ColumnName + "#", value);
                }

                //DataTable hoadon68 = await _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='{id}'");
                DataTable tblHisEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT COUNT(*) FROM #SCHEMA_NAME#.wb_log_email_68 WHERE inv_invoiceauth_id=@P_0", Guid.Parse(id));

                if (tblHisEmail.Rows.Count > 0)
                {
                    int his_mail = Convert.ToInt32(tblHisEmail.Rows[0][0]);
                    result.Add("his_email", his_mail);
                }
                else
                {
                    result.Add("his_email", 0);
                }

                string host = this._webHelper.GetRequest().Url.Host;
                string[] paths = host.Split('.');
                string schemaName = paths[0];

                //DateTime dt = Convert.ToDateTime(hoadon68.Rows[0]["nlap"].ToString());

                //tieude = tieude.Replace("#invoice_number#", hoadon68.Rows[0]["shdon"].ToString());
                //tieude = tieude.Replace("#seller_legal_name#", tblBranch.Rows[0]["name"].ToString());
                //tieude = tieude.Replace("#buyer_name#", tendvi != null ? tendvi : hoadon68.Rows[0]["tnmua"].ToString());

                //noi_dung = noi_dung.Replace("#template_code#", hoadon68.Rows[0]["khieu"].ToString().Substring(0, 1));
                //noi_dung = noi_dung.Replace("#invoice_series#", hoadon68.Rows[0]["khieu"].ToString().Substring(1));
                //noi_dung = noi_dung.Replace("#invoice_issued_date#", dt.ToString("dd-MM-yyyy"));
                //noi_dung = noi_dung.Replace("#invoice_number#", hoadon68.Rows[0]["shdon"].ToString());
                //noi_dung = noi_dung.Replace("#buyer_legal_name#", hoadon68.Rows[0]["tnmua"].ToString());
                //noi_dung = noi_dung.Replace("#security_number#", hoadon68.Rows[0]["sbmat"].ToString());
                //noi_dung = noi_dung.Replace("#total_amount#", total_amount_convert);

                //noi_dung = noi_dung.Replace("#seller_legal_name#", tblBranch.Rows[0]["name"].ToString());
                //noi_dung = noi_dung.Replace("#seller_taxcode#", tblBranch.Rows[0]["tax_code"].ToString());
                //noi_dung = noi_dung.Replace("#seller_tel#", tblBranch.Rows[0]["tel"].ToString());
                //noi_dung = noi_dung.Replace("#schema_name#", schemaName);

                result["subject"] = tieude;
                result["noi_dung"] = noi_dung;
                result["body"] = noi_dung;

                DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT a.email,b.email as email1 FROM #SCHEMA_NAME#.pl_customer a LEFT JOIN #SCHEMA_NAME#.pl_customer_email b ON a.pl_customer_id=b.pl_customer_id WHERE a.code=@P_0 AND a.branch_code=@P_1", customer_code, branch_code);

                array = new JArray();

                if (tblEmail.Rows.Count > 0)
                {
                    string email = tblEmail.Rows[0]["email"].ToString();

                    if (email.Length > 0)
                    {
                        JObject obj = new JObject();
                        obj.Add("id", email);
                        obj.Add("value", email);

                        array.Add(obj);
                    }

                    for (int i = 0; i < tblEmail.Rows.Count; i++)
                    {
                        DataRow r = tblEmail.Rows[i];
                        string email1 = r["email1"].ToString();

                        if (email1.Length > 0 && email1 != email)
                        {
                            JObject obj = new JObject();
                            obj.Add("id", email1);
                            obj.Add("value", email1);

                            array.Add(obj);
                        }
                    }
                }

                result.Add("hd68", 1);
                result.Add("lst_email", array);
            }
            catch (Exception e)
            {
                result.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return result;
            //return result;

        }

        public async Task<byte[]> ExportZipFile(string id, byte[] dataPdf, byte[] dataXml, string filename)
        {
            byte[] result = null;

            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

            ZipEntry newEntry = new ZipEntry(filename + ".xml");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            MemoryStream inStream = new MemoryStream(dataXml);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            newEntry = new ZipEntry(filename + ".pdf");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            inStream = new MemoryStream(dataPdf);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;


            result = outputMemStream.ToArray();

            outputMemStream.Close();

            return result;
        }

        public async Task<byte[]> ExportZipFile_BangKe(string id, byte[] dataPdf, byte[] dataXml, byte[] dataXml_bangke, string filename)
        {
            byte[] result = null;

            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

            ZipEntry newEntry = new ZipEntry(filename + ".xml");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            MemoryStream inStream = new MemoryStream(dataXml);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            newEntry = new ZipEntry(filename + "_bangke.xml");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            MemoryStream inStream_bangke = new MemoryStream(dataXml_bangke);
            inStream_bangke.WriteTo(zipStream);
            inStream_bangke.Close();
            zipStream.CloseEntry();

            newEntry = new ZipEntry(filename + ".pdf");
            newEntry.DateTime = DateTime.Now;
            newEntry.IsUnicodeText = true;

            zipStream.PutNextEntry(newEntry);

            inStream = new MemoryStream(dataPdf);
            inStream.WriteTo(zipStream);
            inStream.Close();
            zipStream.CloseEntry();

            zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;


            result = outputMemStream.ToArray();

            outputMemStream.Close();

            return result;
        }

        public async Task<JObject> SendInvoiceByEmail(string folder, JObject model, string ma_dvcs, string type)
        {
            JObject result = new JObject();

            MemoryStream msZip = null;
            MemoryStream msPdf = null;
            MemoryStream msXml = null;

            string sql = "";

            try
            {
                string nguoinhan = model["nguoinhan"].ToString();
                string tieude = model["tieude"].ToString();
                string noidung = model["noidung"].ToString();
                string inv_InvoiceAuth_id = model["id"].ToString();
                string nguoi_gui = model["nguoi_gui"].ToString();
                string mat_khau = model["mat_khau"].ToString();
                string bcc = model["bcc"].ToString();
                string alias = model["alias"].ToString();

                _emailService.ClearAttach();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();

                if (type == "Gửi hóa đơn")
                {
                    string fileName = await GetInvoiceFileName(inv_InvoiceAuth_id);

                    byte[] dataPdf = await inHoadon(inv_InvoiceAuth_id, folder, "PDF", false);
                    msPdf = new MemoryStream(dataPdf);

                    //get xml hóa đơn
                    string xml = await ExportXMLHoadon(inv_InvoiceAuth_id);
                    byte[] dataXml = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                    msXml = new MemoryStream(dataXml);

                    //get xml bảng kê hoá đơn 
                    MemoryStream msXml_bangke = null;
                    byte[] dataXml_bangke = null;
                    string xml_bangke = await ExportXMLBangKe_HD(inv_InvoiceAuth_id);

                    if (!string.IsNullOrEmpty(xml_bangke))
                    {
                        dataXml_bangke = System.Text.UTF8Encoding.UTF8.GetBytes(xml_bangke);
                        msXml_bangke = new MemoryStream(dataXml_bangke);
                    }

                    byte[] dataZip = string.IsNullOrEmpty(xml_bangke) ? await ExportZipFile(inv_InvoiceAuth_id, dataPdf, dataXml, fileName) : await ExportZipFile_BangKe(inv_InvoiceAuth_id, dataPdf, dataXml, dataXml_bangke, fileName);
                    msZip = new MemoryStream(dataZip);
                    _emailService.Attach(fileName + ".zip", msZip, "application/zip");
                }

                dicParam.Clear();
                dicParam.Add("branch_code", ma_dvcs);
                dicParam.Add("tempemail_type", type);

                DataTable tblTempEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND branch_code=@branch_code AND tempemail_type=@tempemail_type LIMIT 1 OFFSET 0", CommandType.Text, dicParam);

                if (tblTempEmail.Rows.Count > 0)
                {
                    string smtpAddress = tblTempEmail.Rows[0]["smtp_address"].ToString();
                    int smtpPort = Convert.ToInt32(tblTempEmail.Rows[0]["smtp_port"]);
                    bool enableSSL = Convert.ToBoolean(tblTempEmail.Rows[0]["enable_ssl"]);

                    if (smtpAddress.Length > 0)
                    {
                        _emailService.SetEmailServer(smtpAddress, smtpPort, enableSSL);
                    }
                }

                string[] parts = nguoinhan.Replace(",", ";").Split(';');

                foreach (var part in parts)
                {
                    await _emailService.SendAsync(nguoi_gui, mat_khau, part, tieude, noidung, bcc, alias);

                    dicParam.Clear();
                    dicParam.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));

                    string sqlSelect_hoadon = $"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@inv_invoiceauth_id";
                    DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, dicParam);

                    string branch_code = tblInvoiceAuth.Rows[0]["mdvi"].ToString();

                    sql = "INSERT INTO #SCHEMA_NAME#.wb_log_email_68(wb_log_email_68_id, branch_code, send_date,\"from\", \"to\", subject, content, inv_invoiceauth_id, user_send,send_type) \n"
                         + "VALUES (public.uuid_generate_v4(),@branch_code,@send_date,@from,@to,@subject,@content,@inv_invoiceauth_id,@user_send,@send_type)";

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("branch_code", branch_code);
                    parameters.Add("send_date", DateTime.Now);
                    parameters.Add("from", nguoi_gui);
                    parameters.Add("to", part);
                    parameters.Add("subject", tieude);
                    parameters.Add("content", noidung);
                    parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));
                    parameters.Add("user_send", _webHelper.GetUser());
                    parameters.Add("send_type", type);

                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                    // update vào bảng hóa đơn đã gửi Email
                    string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET issendmail = true WHERE hdon_id = '" + inv_InvoiceAuth_id + "'";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdate);
                }
                result.Add("ok", true);
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            finally
            {
                if (msZip != null)
                {
                    msZip.Close();
                }

                if (msPdf != null)
                {
                    msPdf.Close();
                }
            }

            return result;
        }

        public async Task<JObject> AutoSendInvoiceByEmail(string folder, JObject model)
        {
            JObject res = new JObject();

            try
            {
                if (!model.ContainsKey("id") || string.IsNullOrEmpty(model["id"].ToString()))
                {
                    throw new Exception("id không được bỏ trống");
                }

                if (Guid.TryParse(model["id"].ToString(), out Guid mau04Id) == false)
                {
                    throw new Exception("id không đúng định dạng");
                }

                string inv_invoiceauth_id = model["id"].ToString();
                // string nguoinhan = model["nguoinhan"].ToString();
                // string tieude = model["tieude"].ToString();
                string type = "Gửi hóa đơn";

                string originalString = _webHelper.GetRequest().Url.OriginalString;
                var emailTemplate = await this.GetEmailTemplate(type, inv_invoiceauth_id, _webHelper.GetDvcs(), originalString);

                if (emailTemplate.ContainsKey("error"))
                {
                    throw new Exception(emailTemplate["error"]?.ToString());
                }

                model.Add("noidung", emailTemplate["body"].ToString());
                model.Add("nguoi_gui", emailTemplate["sender"].ToString());
                model.Add("mat_khau", emailTemplate["pass"].ToString());
                model.Add("bcc", "");
                model["tieude"] = emailTemplate["subject"].ToString();
                model.Add("alias", emailTemplate["alias"].ToString());

                //model.Add("tieude", emailTemplate["subject"].ToString());
                await this.SendInvoiceByEmail(folder, model, _webHelper.GetDvcs(), type);
                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return res;

        }
        public async Task<byte[]> inHoadon(string id, string folder, string type, bool inchuyendoi, bool checkBangKe = false)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                Guid hdon_id = Guid.Parse(id);

                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'");
                DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id='" + hdon_id + "'");

                if (tblInvoiceXmlData.Rows.Count > 0)
                {
                    if (checkBangKe == true)
                    {
                        if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_bangke"].ToString()))
                        {
                            xml = tblInvoiceXmlData.Rows[0]["dlxml_bangke"].ToString();
                        }
                        else
                        {
                            JObject jsXml = await XmlInvoice_BangKe(hdon_id.ToString());
                            xml = jsXml["xml"].ToString();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString()))
                        {
                            xml = tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString();
                            //XDocument doc = XDocument.Parse(xml);

                            //xml = doc.Descendants("HDon").First().ToString();

                            XmlDocument document = new XmlDocument();
                            document.LoadXml(xml);
                            XmlNode nodeHDon = document.SelectSingleNode("/TDiep/DLieu/HDon");
                            xml = nodeHDon.OuterXml;
                        }
                        else if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString()))
                        {
                            xml = tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString();
                        }
                        else
                        {
                            xml = tblInvoiceXmlData.Rows[0]["dlxml"].ToString();
                        }
                    }
                }
                else
                {
                    if (checkBangKe == true)
                    {
                        JObject jsXml = await XmlInvoice_BangKe(hdon_id.ToString());
                        xml = jsXml["xml"].ToString();
                    }
                    else
                    {
                        JObject jsXml = await XmlInvoice(hdon_id.ToString());
                        xml = jsXml["xml"].ToString();
                    }
                }

                string sqlDetails = $"SELECT  * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' order by Cast (stt as numeric)";
                DataTable detail = await _minvoiceDbContext.GetDataTableAsync(sqlDetails);

                string[] notGet = new string[] { "kmai", "stt", "ma", "ten", "mdvtinh", "sluong", "dgia", "tlckhau", "stckhau", "thtien", "tsuat", "tsuatstr", "hdon_id", "cthdon_id" };

                string cctbao_id = tblHoadon.Rows[0]["cctbao_id"].ToString();
                int trang_thai_hd = Convert.ToInt32(tblHoadon.Rows[0]["tthdon"]);
                string sbmat = tblHoadon.Rows[0]["sbmat"].ToString();


                string SqlDongMau = $"Select sdmau from #SCHEMA_NAME#.quanlykyhieu68 where qlkhsdung_id = '{cctbao_id}'";
                DataTable dongMau = await _minvoiceDbContext.GetDataTableAsync(SqlDongMau);
                int soDongMau = Convert.ToInt32(dongMau.Rows[0][0].ToString());

                xml = FunctionUtil.AddXml(xml, detail, notGet, "HHDVu", cctbao_id, soDongMau);


                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(xml);

                //XmlNodeList xmlNode = doc.GetElementsByTagName("HHDVu");
                //XmlNode parent = xmlNode[0].ParentNode;
                //for (int i = xmlNode.Count; i < 8; i++)
                //{
                //    XmlElement xmlkey = doc.CreateElement("HHDVu");
                //    string blankValue = "";
                //    xmlkey.InnerText = blankValue;
                //    parent.AppendChild(xmlkey);
                //}

                //string inv_originalId = tblInv_InvoiceAuth.Rows[0]["original_invoice_id"].ToString();

                //DataTable tblCtthongbao = await _minvoiceDbContext.GetDataTableAsync("SELECT a.*,b.thousand_point as hang_nghin,b.decimal_point as thap_phan FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id WHERE a.inv_notificationdetail_id='" + inv_InvoiceCode_id + "'");

                //string hang_nghin = tblCtthongbao.Rows[0]["hang_nghin"].ToString();
                //string thap_phan = tblCtthongbao.Rows[0]["thap_phan"].ToString();

                //string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", tblCtthongbao.Rows[0]["inv_notificationdetail_id"]);

                //XtraReport report = _cacheManager.Get<XtraReport>(cacheReportKey);
                XtraReport report = new XtraReport();
                //if (report == null)
                //{
                DataTable tblKyhieu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + cctbao_id + "'");
                DataTable tblDmmauhd = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + tblKyhieu.Rows[0]["qlmtke_id"] + "'");
                string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

                if (checkBangKe == true)
                {
                    invReport = tblDmmauhd.Rows[0]["dulieumaubangke"].ToString();
                }
                if (invReport.Length > 0)
                {
                    report = ReportUtil.LoadReportFromString(invReport);
                    // _cacheManager.Set(cacheReportKey, report, 30);
                }
                else
                {
                    throw new Exception("Không tải được mẫu hóa đơn");
                }

                //   }


                report.Name = "XtraReport1";
                report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                {
                    ds.ReadXmlSchema(xmlReader);
                    xmlReader.Close();
                }

                using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                {
                    ds.ReadXml(xmlReader);
                    xmlReader.Close();
                }

                if (ds.Tables.Contains("TblXmlData"))
                {
                    ds.Tables.Remove("TblXmlData");
                }


                DataTable tblXmlData = new DataTable();
                tblXmlData.TableName = "TblXmlData";
                tblXmlData.Columns.Add("data");

                DataRow r = tblXmlData.NewRow();
                r["data"] = xml;
                tblXmlData.Rows.Add(r);
                ds.Tables.Add(tblXmlData);

                string datamember = report.DataMember;

                if (datamember.Length > 0)
                {
                    if (ds.Tables.Contains(datamember))
                    {
                        DataTable tblChiTiet = ds.Tables[datamember];
                        int rowcount = ds.Tables[datamember].Rows.Count;
                    }
                }

                if (tblHoadon.Rows[0]["hdon68_id_lk"] != null &&
                    !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString()))
                {
                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "3")
                    {
                        Guid hoadonIdLK =
                            tblHoadon.Rows[0]["hdon68_id_lk"] != null &&
                            !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                     + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql);

                        if (tblInv.Rows.Count > 0)
                        {
                            //    string msg = tblInv.Rows[0]["msg"].ToString();

                            //    if (msg.Length > 0)
                            //    {
                            //        DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=" + trang_thai_hd);
                            //        string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();

                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                                                          " năm " + nam;

                        }
                        //}
                    }

                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "2")
                    {
                        Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                            ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                            : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                     + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql);

                        if (tblInv.Rows.Count > 0)
                        {
                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                     " năm " + nam;
                        }
                    }

                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "19" ||
                        tblHoadon.Rows[0]["tthdon_original"].ToString() == "21" ||
                        tblHoadon.Rows[0]["tthdon_original"].ToString() == "23")
                    {
                        Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                            ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                            : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                     + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql);

                        if (tblInv.Rows.Count > 0)
                        {
                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                     " năm " + nam;
                        }
                    }
                }

                else
                {
                    if (trang_thai_hd == 5 || trang_thai_hd == 19 || trang_thai_hd == 21 || trang_thai_hd == 23)
                    {
                        string kyHieuMauSoLQ = tblHoadon.Rows[0]["khmshdclquan"].ToString();
                        string kyHieuHoaDonLQ = tblHoadon.Rows[0]["khhdclquan"].ToString();
                        string soHoaDonLQ = tblHoadon.Rows[0]["shdclquan"].ToString();
                        DateTime ngayLap = DateTime.Parse(tblHoadon.Rows[0]["nlhdclquan"].ToString());
                        string ngay = ngayLap.Day.ToString();
                        string thang = ngayLap.Month.ToString();
                        string nam = ngayLap.Year.ToString();
                        msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " + kyHieuMauSoLQ + " ký hiệu " +
                                 kyHieuHoaDonLQ
                                 + " số " + soHoaDonLQ + " ngày " + ngay + " tháng " + thang + " năm " + nam;
                    }

                    if (trang_thai_hd == 2)
                    {
                        string kyHieuMauSoLQ = tblHoadon.Rows[0]["khmshdclquan"].ToString();
                        string kyHieuHoaDonLQ = tblHoadon.Rows[0]["khhdclquan"].ToString();
                        string soHoaDonLQ = tblHoadon.Rows[0]["shdclquan"].ToString();
                        DateTime ngayLap = DateTime.Parse(tblHoadon.Rows[0]["nlhdclquan"].ToString());
                        string ngay = ngayLap.Day.ToString();
                        string thang = ngayLap.Month.ToString();
                        string nam = ngayLap.Year.ToString();
                        msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " + kyHieuMauSoLQ + " ký hiệu " +
                                 kyHieuHoaDonLQ
                                 + " số " + soHoaDonLQ + " ngày " + ngay + " tháng " + thang + " năm " + nam;
                    }
                }



                //if (Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]) == 7)
                //{
                //    msg_tb = "";
                //}
                if (tblHoadon.Rows[0]["dvtte"] != null && tblHoadon.Rows[0]["dvtte"].ToString() == "USD" && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgia"].ToString()))
                {
                    decimal tienthanhtoan = 0;
                    if (tblHoadon.Rows[0]["tgtttbso_last"] != null && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgtttbso_last"].ToString()))
                    {
                        tienthanhtoan = Convert.ToDecimal(tblHoadon.Rows[0]["tgtttbso_last"].ToString());
                    }
                    decimal tigia = Convert.ToDecimal(tblHoadon.Rows[0]["tgia"].ToString());
                    decimal tienquydoi = tienthanhtoan * tigia;
                    if (report.Parameters["QUYDOI"] != null)
                    {
                        report.Parameters["QUYDOI"].Value = tienquydoi.ToString();
                    }
                }
                if (report.Parameters["MSG_TB"] != null)
                {
                    report.Parameters["MSG_TB"].Value = msg_tb;
                }

                if (report.Parameters["MCCQT"] != null)
                {
                    report.Parameters["MCCQT"].Value = tblHoadon.Rows[0]["mccqthue"].ToString();
                }

                // số bảo mật
                if (report.Parameters["SoBaoMat"] != null)
                {
                    report.Parameters["SoBaoMat"].Value = sbmat;
                }

                //check in bảng kê
                if (checkBangKe == true)
                {
                    if (report.Parameters["CheckBangKe"] != null)
                    {
                        report.Parameters["CheckBangKe"].Value = 1;
                    }
                }
                else
                {
                    if (report.Parameters["CheckBangKe"] != null)
                    {
                        report.Parameters["CheckBangKe"].Value = 0;
                    }

                }

                try
                {

                    if (report.Parameters["SoThapPhan"] != null)
                    {
                        string sql = "SELECT * FROM #SCHEMA_NAME#.sl_currency a "
                          + "WHERE a.code='" + tblHoadon.Rows[0]["dvtte"].ToString() + "' limit 1";

                        DataTable currency = await _minvoiceDbContext.GetDataTableAsync(sql);
                        if (currency.Rows.Count > 0)
                        {
                            report.Parameters["SoThapPhan"].Value = currency.Rows[0]["hoadon123"].ToString();
                        }
                    }
                }
                catch
                {

                }
                //var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

                //if (lblHoaDonMau != null)
                //{
                //    lblHoaDonMau.Visible = false;
                //}

                if (inchuyendoi)
                {
                    var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                    if (tblInChuyenDoi != null)
                    {
                        tblInChuyenDoi.Visible = true;
                    }

                    if (report.Parameters["MSG_HD_TITLE"] != null)
                    {
                        report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                    }

                    if (report.Parameters["NGUOI_IN_CDOI"] != null)
                    {
                        report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                        report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                    }

                    if (report.Parameters["NGAY_IN_CDOI"] != null)
                    {
                        report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                        report.Parameters["NGAY_IN_CDOI"].Visible = true;
                    }
                }

                report.DataSource = ds;

                try
                {
                    bool giamthuebanhang20 = tblHoadon.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblHoadon.Rows[0]["giamthuebanhang20"].ToString());
                    string dvtte = tblHoadon.Rows[0]["dvtte"]?.ToString();

                    CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                    string tongTienGiam = double.Parse(DoubleStrToString(tblHoadon.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                    tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");
                    string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";

                    if (giamthuebanhang20)
                    {
                        var dt = ds.Tables["TToan"];
                        if (dt.Rows.Count > 0)
                        {
                            if (dt.Columns.Contains("TgTTTBChu"))
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    var docTien = (dt.Rows[i]["TgTTTBChu"]?.ToString()).Trim();

                                    ds.Tables["TToan"].Rows[i]["TgTTTBChu"] = docTien + ". " + Environment.NewLine + thueGTGTBanHang;
                                    report.DataSource = ds;

                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Log.Error(ex);
                }

                #region Them Hien Thi Gia khung hinh
                try
                {
                    //if (tblHoadon.Rows[0]["error_status"].ToString() == "1")
                    //{
                    //    int pageCount = report.Pages.Count;
                    //    Watermark pmk = report.Watermark;
                    //    pmk.Text = tblHoadon.Rows[0]["is_hdcma"].ToString() == "1" ? "CQT không cấp mã" : (tblHoadon.Rows[0]["is_hdcma"].ToString() == "0" ? "CQT không tiếp nhận HĐ" : "");
                    //    pmk.TextDirection = DirectionMode.ForwardDiagonal;
                    //    pmk.Font = new Font("Times New Roman", 50);
                    //    pmk.TextTransparency = 150;
                    //    pmk.PageRange = "1-" + pageCount;
                    //}
                    //else
                    //{
                    if (tblHoadon.Rows[0]["tthdon"].ToString() == "3" || tblHoadon.Rows[0]["tthdon"].ToString() == "17")
                    {
                        // Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        // report.Watermark.Image = bmp;
                        Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        int pageCount = report.Pages.Count;

                        Watermark pmk = report.Watermark;
                        pmk.Image = bmp;
                        pmk.PageRange = "1-" + pageCount;

                    }
                    //}
                }
                catch (Exception ex)
                {
                }
                #endregion

                report.CreateDocument();
               
                MemoryStream ms = new MemoryStream();
                #region HTML
                if (type == "Html")
                {
                    report.ExportOptions.Html.EmbedImagesInHTML = true;
                    report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                    report.ExportOptions.Html.Title = "Hóa đơn điện tử";
                    report.ExportToHtml(ms);

                    string html = Encoding.UTF8.GetString(ms.ToArray());

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(html);


                    string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                    string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                    var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            string eventMouseDown = node.Attributes["onmousedown"].Value;

                            if (eventMouseDown.Contains("showCert('seller')"))
                            {
                                node.SetAttributeValue("id", "certSeller");
                            }
                            if (eventMouseDown.Contains("showCert('buyer')"))
                            {
                                node.SetAttributeValue("id", "certBuyer");
                            }
                            if (eventMouseDown.Contains("showCert('vacom')"))
                            {
                                node.SetAttributeValue("id", "certVacom");
                            }
                            if (eventMouseDown.Contains("showCert('minvoice')"))
                            {
                                node.SetAttributeValue("id", "certMinvoice");
                            }
                        }
                    }

                    HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                    HtmlNode xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("id", "xmlData");
                    xmlNode.SetAttributeValue("type", "text/xmldata");

                    xmlNode.AppendChild(doc.CreateTextNode(xml));
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                    head.AppendChild(xmlNode);

                    xmlNode = doc.CreateElement("script");
                    xmlNode.SetAttributeValue("type", "text/javascript");

                    xmlNode.InnerHtml = "$(function () { "
                                       + "  var url = 'http://localhost:19898/signalr'; "
                                       + "  var connection = $.hubConnection(url, {  "
                                       + "     useDefaultPath: false "
                                       + "  });"
                                       + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                       + " invoiceHubProxy.on('resCommand', function (result) { "
                                       + " }); "
                                       + " connection.start().done(function () { "
                                       + "      console.log('Connect to the server successful');"
                                       + "      $('#certSeller').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'seller' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certVacom').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'vacom' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certBuyer').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'buyer' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "      $('#certMinvoice').click(function () { "
                                       + "         var arg = { "
                                       + "              xml: document.getElementById('xmlData').innerHTML, "
                                       + "              id: 'minvoice' "
                                       + "         }; "
                                       + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                       + "         }); "
                                       + "})"
                                       + ".fail(function () { "
                                       + "     alert('failed in connecting to the signalr server'); "
                                       + "});"
                                       + "});";

                    head.AppendChild(xmlNode);

                    if (report.Watermark != null)
                    {
                        if (report.Watermark.Image != null)
                        {
                            ImageConverter _imageConverter = new ImageConverter();
                            byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                            string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                            HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                            string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                            string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                            HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                            style.AppendChild(textNode);

                            HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                            HtmlNodeCollection pageNodes = body.SelectNodes("div");

                            foreach (var pageNode in pageNodes)
                            {
                                pageNode.Attributes.Add("class", "waterMark");

                                string divStyle = pageNode.Attributes["style"].Value;
                                divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                pageNode.Attributes["style"].Value = divStyle;
                            }
                        }
                    }

                    ms.SetLength(0);
                    doc.Save(ms);

                    doc = null;
                }
                #endregion
                else if (type == "Image")
                {
                    var options = new ImageExportOptions(ImageFormat.Png)
                    {
                        ExportMode = ImageExportMode.SingleFilePageByPage,
                    };
                    report.ExportToImage(ms, options);
                }
                else if (type == "Excel")
                {
                    report.ExportToXlsx(ms);
                }
                else if (type == "Rtf")
                {
                    report.ExportToRtf(ms);
                }
                else
                {
                    report.ExportToPdf(ms);
                }

                bytes = ms.ToArray();
                ms.Close();

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> inMau01(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id='" + id + "'");

                //if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_send"].ToString()))
                //{
                //    xml = tblHoadon.Rows[0]["xml_send"].ToString();
                //}
                //else
                //{
                xml = await XmlDangkysudungIn(tblHoadon.Rows[0]["mau01_id"].ToString());
                //}

                //XtraReport report = XtraReport.FromFile(folder, true);

                //report.Name = "XtraReport1";
                //report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();
                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, null);

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> inMau02(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id='" + id + "'");

                if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_rep"].ToString()))
                {
                    xml = tblHoadon.Rows[0]["xml_rep"].ToString();
                }
                else
                {
                    throw new Exception("Chưa có dữ liệu chấp nhận");
                }

                //XtraReport report = XtraReport.FromFile(folder, true);

                //report.Name = "XtraReport1";
                //report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();
                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, null);

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> inMau03(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id='" + id + "'");

                if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_tiepnhan"].ToString()))
                {
                    xml = tblHoadon.Rows[0]["xml_tiepnhan"].ToString();
                }
                else
                {
                    throw new Exception("Chưa có dữ liệu tiếp nhận");
                }

                XmlDocument docXml = new XmlDocument();
                docXml.LoadXml(xml);

                XmlNode dltao = docXml.GetElementsByTagName("DLTBao")[0];
                XmlElement tcqt = docXml.CreateElement("TCQT");
                tcqt.InnerText = tblHoadon.Rows[0]["cqtqly"].ToString();
                XmlElement mta = docXml.CreateElement("MTa");
                mta.InnerText = tblHoadon.Rows[0]["lydo_tiepnhan"].ToString();
                XmlElement hthuc = docXml.CreateElement("HThuc");
                hthuc.InnerText = tblHoadon.Rows[0]["loai"].ToString();
                dltao.AppendChild(tcqt);
                dltao.AppendChild(mta);
                dltao.AppendChild(hthuc);

                //XtraReport report = XtraReport.FromFile(folder, true);

                //report.Name = "XtraReport1";
                //report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();
                StringReader theReader = new StringReader(docXml.OuterXml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, null);

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> inMau04(string id, string folder, string type, string loai)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id='" + id + "'");

                if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_send"].ToString()))
                {
                    xml = tblHoadon.Rows[0]["xml_send"].ToString();
                }
                else
                {
                    xml = await XmlHuyhoadonMau04(tblHoadon.Rows[0]["mau04_id"].ToString());
                }

                //XtraReport report = XtraReport.FromFile(folder, true);

                //report.Name = "XtraReport1";
                //report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();
                if (loai == "5")
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);
                    XmlElement el = (XmlElement)doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/DSCKS");
                    if (el != null)
                    {
                        el.ParentNode.RemoveChild(el);
                        StringReader theReader = new StringReader(doc.OuterXml);
                        ds.ReadXml(theReader);
                    }
                }
                else
                {
                    StringReader theReader = new StringReader(xml);

                    ds.ReadXml(theReader);
                }

                bytes = ReportUtil.PrintReport(ds, folder, type, null);

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> inMau04Rep(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id='" + id + "'");
                if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_nhan"].ToString()))
                {
                    xml = tblHoadon.Rows[0]["xml_nhan"].ToString();
                }
                else
                {
                    throw new Exception("Chưa có dữ liệu tiếp nhận");
                }

                //if (xml.Contains("<MLTDiep>204</MLTDiep>"))
                //{
                //    throw new Exception("Thông điệp 204 không có mẫu in");
                //    bytes = null;

                //}

                DataSet ds = new DataSet();

                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                //bytes = ReportUtil.PrintReport(ds, folder, type, null);
                bytes = ReportUtil.PrintReport2(ds, folder, type, null);

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> inBTHDLieu(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id='" + id + "'");

                //if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_send"].ToString()))
                //{
                //    xml = tblHoadon.Rows[0]["xml_send"].ToString();
                //}
                //else
                //{
                int so = string.IsNullOrEmpty(tblHoadon.Rows[0]["sbthdlieu"].ToString()) ? 0 : Convert.ToInt32(tblHoadon.Rows[0]["sbthdlieu"].ToString());
                JObject x = await XmlBangTHDLIn(tblHoadon.Rows[0]["bangtonghopdl_68_id"].ToString(), so);
                xml = x["xml"].ToString();
                //}

                //XtraReport report = XtraReport.FromFile(folder, true);

                //report.Name = "XtraReport1";
                //report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();
                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, "Dlieu");

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<byte[]> inMaThongDiepCQT(string xml, string folder, string type)
        {
            byte[] bytes = null;

            try
            {
                DataSet ds = new DataSet();
                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, "Dlieu");

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }
        public async Task<bool> checkQuyen(string _editmode, string _username, Action<JObject> _json)
        {
            bool _resul = true;
            JObject obj = new JObject();
            DataTable tblNhomQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT wb_role_id FROM #SCHEMA_NAME#.wb_role WHERE wb_role_id IN (SELECT wb_role_id FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + _username + "')");

            if (tblNhomQuyen.Rows.Count > 0)
            {
                Dictionary<string, object> parameter1s = new Dictionary<string, object>();
                parameter1s.Add("window_code", "aaaaaaaaaa");
                DataTable tblCtQuyen = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_detailrole WHERE wb_role_id='" + tblNhomQuyen.Rows[0]["wb_role_id"] + "' AND wb_menu_id IN (SELECT wb_menu_id FROM #SCHEMA_NAME#.wb_menu WHERE window_code=@window_code)", CommandType.Text, parameter1s);

                if (tblCtQuyen.Rows.Count == 0)
                {
                    if (_webHelper.GetUser() != "ADMINISTRATOR")
                    {
                        obj.Add("error", "Nhóm chưa được phân quyền");
                        _json.Invoke(obj);
                        _resul = false;
                        return _resul;
                    }
                }
                else
                {
                    if (_editmode == "1" && tblCtQuyen.Rows[0]["has_new"].ToString() != "C")
                    {
                        obj.Add("error", "Bạn không có quyền thêm mới");
                        _json.Invoke(obj);
                        _resul = false;
                        return _resul;
                    }

                    if (_editmode == "2" && tblCtQuyen.Rows[0]["has_edit"].ToString() != "C")
                    {
                        obj.Add("error", "Bạn không có quyền sửa");
                        _json.Invoke(obj);
                        _resul = false;
                        return _resul;
                    }

                    if (_editmode == "3" && tblCtQuyen.Rows[0]["has_delete"].ToString() != "C")
                    {
                        obj.Add("error", "Bạn không có quyền xóa");
                        _json.Invoke(obj);
                        _resul = false;
                        return _resul;
                    }
                }
            }

            DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username='" + _username + "'");

            string isnewuser = tblUser.Rows[0]["is_new_user"].ToString();
            string isedituser = tblUser.Rows[0]["is_edit_user"].ToString();
            string isdeluser = tblUser.Rows[0]["is_del_user"].ToString();
            string store_code = tblUser.Rows[0]["store_code"].ToString();

            if (_editmode == "3" && isdeluser != "C")
            {
                obj.Add("error", "Bạn không có quyền xóa hóa đơn của người dùng khác");
                _json.Invoke(obj);
                _resul = false;
                return _resul;
            }

            if (_editmode == "1" && isnewuser != "C")
            {
                obj.Add("error", "Bạn không có quyền thêm mới");
                _json.Invoke(obj);
                _resul = false;
                return _resul;
            }
            return _resul;
        }

        public async Task<JArray> getKyhieu(string cctbao_id)
        {
            JArray _res = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id = '" + cctbao_id + "'";
            _res = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return _res;
        }


        public async Task<JObject> SaveHoadon(JObject obj, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];
                    #region Check

                    Dictionary<string, object> obj_check = new Dictionary<string, object>();

                    var sql_check = @"select * from #SCHEMA_NAME#.permission_kyhieu68 a 
                                join #SCHEMA_NAME#.quanlykyhieu68 b on a.qlkhsdung_id = b.qlkhsdung_id 
                                join #SCHEMA_NAME#.wb_user c on a.wb_user_id =c.wb_user_id 
                                where c.username =   @username and b.qlkhsdung_id = @qlkhsdung_id";
                    obj_check.Add("username", username);
                    obj_check.Add("qlkhsdung_id", Guid.Parse(masterObj["cctbao_id"].ToString()));
                    DataTable data_check = await _minvoiceDbContext.GetDataTableAsync(sql_check, CommandType.Text, obj_check);

                    if (data_check.Rows.Count == 0)
                    {
                        throw new Exception("Người dùng không có quyền sử dụng kí hiệu này");
                    }

                    sql_check = $@"select * from #SCHEMA_NAME#.phanhoadon_78_donvi a 
                                            join #SCHEMA_NAME#.phanhoadon_78 b on a.phanhoadon_78_id = b.id 
                                            join #SCHEMA_NAME#.quanlykyhieu68 c on b.kyhieu_id =c.qlkhsdung_id 
                                            where a.branch_code =@branch_code and c.qlkhsdung_id = @qlkhsdung_id";
                    obj_check = new Dictionary<string, object>();
                    obj_check.Add("branch_code", _webHelper.GetDvcs());
                    obj_check.Add("qlkhsdung_id", Guid.Parse(masterObj["cctbao_id"].ToString()));
                    data_check = await _minvoiceDbContext.GetDataTableAsync(sql_check, CommandType.Text, obj_check);

                    if (data_check.Rows.Count == 0)
                    {
                        throw new Exception("Đơn vị không được phân quyền cho kí hiệu này");
                    }
                    #endregion
                    //if(!ValidInvoice.InsertValid(masterObj, ref json))
                    //{
                    //    return json;
                    //}
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1" || editMode == "4")
                    {
                        //if (masterObj["tthdon"] != null)
                        //{
                        //    masterObj["tthdon"] = 0;
                        //}
                        //else
                        //{
                        //    masterObj.Add("tthdon", 0);
                        //}

                        if (masterObj["tthdon"] == null)
                        {
                            masterObj.Add("tthdon", 0);
                        }

                        if (masterObj["tthdon_old"] == null)
                        {
                            masterObj.Add("tthdon_old", masterObj["tthdon"]);
                        }
                        else
                        {
                            masterObj["tthdon_old"] = masterObj["tthdon"];
                        }

                        if (masterObj["tthdon_original"] == null)
                        {
                            masterObj.Add("tthdon_original", masterObj["tthdon"]);
                        }
                        else
                        {
                            masterObj["tthdon_original"] = masterObj["tthdon"];
                        }

                        if (masterObj["shdon"] != null)
                        {
                            masterObj["shdon"] = null;
                        }
                        else
                        {
                            masterObj.Add("shdon", null);
                        }

                        if (masterObj["tthai"] != null)
                        {
                            masterObj["tthai"] = "Chờ ký";
                        }
                        else
                        {
                            masterObj.Add("tthai", "Chờ ký");
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                        if (masterObj["nglap"] != null)
                        {
                            masterObj["nglap"] = username;
                        }
                        else
                        {
                            masterObj.Add("nglap", username);
                        }

                        //if (masterObj["nlap"] != null)
                        //{
                        //    masterObj["nlap"] = DateTime.Now;
                        //}
                        //else
                        //{
                        //    masterObj.Add("nlap", DateTime.Now);
                        //}
                        if (masterObj["nlap"] == null)
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (editMode == "1")
                        {
                            if (masterObj["hdon_id"] != null)
                            {
                                if (string.IsNullOrEmpty(masterObj["hdon_id"].ToString()))
                                {
                                    masterObj["hdon_id"] = id;
                                }
                                else
                                {
                                    id = Guid.Parse(masterObj["hdon_id"].ToString());
                                }
                            }
                            else
                            {
                                masterObj.Add("hdon_id", id);
                            }

                            if ((masterObj["hdon_id_old"] == null || (masterObj["hdon_id_old"] != null && string.IsNullOrEmpty(masterObj["hdon_id_old"].ToString())))
                                && (masterObj["hdon68_id_lk"] == null || (masterObj["hdon68_id_lk"] != null && !string.IsNullOrEmpty(masterObj["hdon68_id_lk"].ToString())))
                                && (masterObj["tthdon"] == null || (masterObj["tthdon"] != null && !"2".Equals(masterObj["tthdon"].ToString())
                                && !"19".Equals(masterObj["tthdon"].ToString()) && !"21".Equals(masterObj["tthdon"].ToString()) && !"23".Equals(masterObj["tthdon"].ToString()))
                                ))
                            {
                                masterObj.Remove("lhdclquan");
                                masterObj.Remove("khmshdclquan");
                                masterObj.Remove("khhdclquan");
                                masterObj.Remove("shdclquan");
                                masterObj.Remove("nlhdclquan");
                            }

                        }

                        if (masterObj["tgtttbchu"] == null)
                        {
                            JObject rsObj = new JObject();
                            rsObj = await Amount_ToWord(masterObj["tgtttbso_last"].ToString(), masterObj["dvtte"].ToString());
                            masterObj.Add("tgtttbchu", rsObj["data"].ToString());
                        }
                    }

                    if (editMode == "2" || editMode == "5")
                    {
                        //if (masterObj["nglap"] != null)
                        //{
                        //    string user_new = masterObj["hdon_id"].ToString();

                        //    if (isedituser != "C" && user_new != username)
                        //    {
                        //        json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                        //        return json;
                        //    }
                        //}

                        if (masterObj["ngsua"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["ngsua"].ToString()))
                            {
                                masterObj["ngsua"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("ngsua", username);
                        }

                        if (masterObj["nsua"] != null)
                        {
                            masterObj["nsua"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nsua", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj["hdon_id"].ToString());

                        if (masterObj["tgtttbchu"] == null)
                        {
                            JObject rsObj = new JObject();
                            rsObj = await Amount_ToWord(masterObj["tgtttbso_last"].ToString(), masterObj["dvtte"].ToString());
                            masterObj.Add("tgtttbchu", rsObj["data"].ToString());
                        }

                    }
                    string hdon_id = "";
                    string tthai = "";
                    string tthdon = "";
                    if (editMode == "1" || editMode == "2")
                    {
                        string tthdonNew = masterObj["tthdon"]?.ToString();
                        // check validate điều chỉnh hóa đơn, thay thế hóa đơn
                        if (tthdonNew != null && (tthdonNew == "19" || tthdonNew == "21" || tthdonNew == "23" || tthdonNew == "2"))
                        {
                            if ((masterObj["hdon_id_old"] != null && !string.IsNullOrEmpty(masterObj["hdon_id_old"].ToString())) || (masterObj["hdon68_id_lk"] != null && !string.IsNullOrEmpty(masterObj["hdon68_id_lk"].ToString())))
                            {
                                hdon_id = "";
                                if (masterObj["hdon_id_old"] != null && !string.IsNullOrEmpty(masterObj["hdon_id_old"].ToString()))
                                {
                                    hdon_id = masterObj["hdon_id_old"].ToString();
                                }
                                else
                                {
                                    hdon_id = masterObj["hdon68_id_lk"].ToString();
                                }

                                if (string.IsNullOrEmpty(hdon_id))
                                {
                                    json.Add("error", "Không tồn tại hóa đơn bị điều chỉnh!");
                                    return json;
                                }

                                string sql1 = $"SELECT tthai, tthdon FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='{hdon_id}'";
                                DataTable dt1 = await this._minvoiceDbContext.GetDataTableAsync(sql1);
                                if (dt1.Rows.Count > 0)
                                {
                                    tthai = dt1.Rows[0]["tthai"]?.ToString();
                                    tthdon = dt1.Rows[0]["tthdon"]?.ToString();
                                }

                                if (editMode == "1")
                                {
                                    if (tthdonNew == "19" || tthdonNew == "21" || tthdonNew == "23")
                                    {
                                        if ((tthai != null && (tthai == "Đã ký" || tthai == "Đã cấp mã" || tthai == "CQT đã nhận" || tthai == "Chấp nhận TBSS" || tthai == "Không chấp nhận TBSS" || tthai == "TBSS sai định dạng" || tthai == "Chờ phản hồi") && (tthdon == "0" || tthdon == "1"))
                                            || (tthai != null && (tthai == "Đã ký" || tthai == "Đã cấp mã" || tthai == "CQT đã nhận" || tthai == "Chấp nhận TBSS" || tthai == "Không chấp nhận TBSS" || tthai == "TBSS sai định dạng" || tthai == "Chờ phản hồi") && tthdon == "11"))
                                        {
                                            bool checkHD = true;
                                        }
                                        else
                                        {
                                            json.Add("error", "Không thể thực hiện chức năng này");
                                            return json;
                                        }
                                    }
                                    else if (tthdonNew == "2")
                                    {
                                        if ((tthai != null && (tthai == "Đã ký" || tthai == "Đã cấp mã" || tthai == "CQT đã nhận" || tthai == "Chấp nhận TBSS" || tthai == "Không chấp nhận TBSS" || tthai == "TBSS sai định dạng" || tthai == "Chờ phản hồi") && (tthdon == "0" || tthdon == "1"))
                                           || (tthai != null && (tthai == "Đã ký" || tthai == "Đã cấp mã" || tthai == "CQT đã nhận" || tthai == "Chấp nhận TBSS" || tthai == "Không chấp nhận TBSS" || tthai == "TBSS sai định dạng" || tthai == "Chờ phản hồi") && tthdon == "2")
                                           || (tthai != null && (tthai == "Chờ phản hồi") && tthdon == "3"))
                                        {
                                            bool checkHD = true;
                                        }
                                        else
                                        {
                                            json.Add("error", "Không thể thực hiện chức năng này");
                                            return json;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                if (masterObj["lhdclquan"] == null || masterObj["lhdclquan"] != null && string.IsNullOrEmpty(masterObj["lhdclquan"].ToString())
                                    || masterObj["khmshdclquan"] == null || masterObj["khmshdclquan"] != null && string.IsNullOrEmpty(masterObj["khmshdclquan"].ToString())
                                    || masterObj["khhdclquan"] == null || masterObj["khhdclquan"] != null && string.IsNullOrEmpty(masterObj["khhdclquan"].ToString())
                                    || masterObj["shdclquan"] == null || masterObj["shdclquan"] != null && string.IsNullOrEmpty(masterObj["shdclquan"].ToString())
                                    || masterObj["nlhdclquan"] == null || masterObj["nlhdclquan"] != null && string.IsNullOrEmpty(masterObj["nlhdclquan"].ToString()))
                                {
                                    json.Add("error", "Hoá đơn điều chỉnh, thay thế hoá đơn 32 phải đẩy đủ các thông tin lhdclquan, khmshdclquan, khhdclquan, shdclquan, nlhdclquan");
                                    return json;
                                }

                            }

                        }

                    }


                    if (editMode == "3")
                    {
                        //if (masterObj["nglap"] != null)
                        //{
                        //    string user_new = masterObj["nglap"].ToString();

                        //    if (isdeluser != "C" && user_new != username)
                        //    {
                        //        json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                        //        return json;
                        //    }
                        //}
                    }
                    #endregion
                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    if (editMode == "4" || editMode == "5")
                    {
                        if (details != null && details.Count > 0 && ((JArray)(details[0]["data"])).Count > 0)
                        {
                            string sql = "";
                            await _minvoiceDbContext.BeginTransactionAsync();

                            if (editMode == "4")
                            {
                                sql = "#SCHEMA_NAME#.crd_bangke_kemtheo_insert_n";
                                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                            }

                            sql = "#SCHEMA_NAME#.crd_bangke_kemtheo_update_pt";
                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                            #region Chi tiết hóa đơn
                            foreach (var dtl in details)
                            {
                                JArray obj_details = (JArray)dtl["data"];
                                sql = "DELETE FROM #SCHEMA_NAME#.bangke_kemtheo_chitiet WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                                if (obj_details.Count > 0)
                                {
                                    for (int j = 0; j < obj_details.Count; j++)
                                    {
                                        JObject js_details = (JObject)obj_details[j];

                                        if (js_details["hdon_id"] != null)
                                        {
                                            if (string.IsNullOrEmpty(js_details["hdon_id"].ToString()))
                                            {
                                                js_details["hdon_id"] = masterObj["hdon_id"];
                                            }
                                        }
                                        else
                                        {
                                            js_details.Add("hdon_id", masterObj["hdon_id"]);
                                        }

                                        if (js_details["cthdon_id"] != null)
                                        {
                                            if (string.IsNullOrEmpty(js_details["cthdon_id"].ToString()))
                                            {
                                                js_details["cthdon_id"] = Guid.NewGuid();
                                            }
                                        }
                                        else
                                        {
                                            js_details.Add("cthdon_id", Guid.NewGuid());
                                        }
                                        await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_bangke_kemtheo_chitiet_insert", CommandType.StoredProcedure, js_details);
                                    }
                                }

                            }
                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();
                            #endregion
                        }
                        else
                        {
                            json.Add("error", "Cần bổ sung chi tiết hóa đơn.");
                        }
                    }
                    else
                    {
                        if (editMode != "1" && editMode != "2")
                        {
                            string sql = "#SCHEMA_NAME#.crd_hdon68_delete";
                            await _minvoiceDbContext.BeginTransactionAsync();
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        else if (details != null && details.Count > 0 && ((JArray)(details[0]["data"])).Count > 0)
                        {
                            //masterObj.Add("branch_code", _webHelper.GetDvcs());
                            string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_hdon68_insert_hatm" : editMode == "2" ? "#SCHEMA_NAME#.crd_hdon68_update_n" : "#SCHEMA_NAME#.crd_hdon68_delete";

                            await _minvoiceDbContext.BeginTransactionAsync();
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                            if (editMode == "1" || editMode == "2")
                            {
                                sql = "#SCHEMA_NAME#.crd_hdon68_update_pt";
                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                            }

                            #region Chi tiết hóa đơn

                            foreach (var dtl in details)
                            {
                                JArray obj_details = (JArray)dtl["data"];
                                sql = "DELETE FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                                if (obj_details.Count > 0)
                                {
                                    for (int j = 0; j < obj_details.Count; j++)
                                    {
                                        JObject js_details = (JObject)obj_details[j];

                                        if (js_details["hdon_id"] != null)
                                        {
                                            if (string.IsNullOrEmpty(js_details["hdon_id"].ToString()))
                                            {
                                                js_details["hdon_id"] = masterObj["hdon_id"];
                                            }
                                        }
                                        else
                                        {
                                            js_details.Add("hdon_id", masterObj["hdon_id"]);
                                        }

                                        if (js_details["cthdon_id"] != null)
                                        {
                                            if (string.IsNullOrEmpty(js_details["cthdon_id"].ToString()))
                                            {
                                                js_details["cthdon_id"] = Guid.NewGuid();
                                            }
                                        }
                                        else
                                        {
                                            js_details.Add("cthdon_id", Guid.NewGuid());
                                        }
                                        /*if (js_details["tsuat"] != null && !string.IsNullOrEmpty(js_details["tsuat"].ToString()) &&  !new string[] { "0", "5", "10" }.Contains(js_details["tsuat"].ToString()) )
                                        {
                                                js_details["tsuatstr"] = "KHAC";
                                        }*/
                                        //if (fieldStt_rec0 != null)
                                        //{
                                        //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                        //}

                                        await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_chitiet_insert", CommandType.StoredProcedure, js_details);
                                    }
                                }

                            }

                            #region hatm luuthuephi
                            JArray phi = null;

                            if (masterObj["hoadon68_phi"] != null)
                            {
                                if (masterObj.SelectToken("hoadon68_phi") is JArray)
                                {
                                    phi = (JArray)masterObj["hoadon68_phi"];
                                }
                            }

                            if (phi != null)
                            {
                                foreach (var dtl in phi)
                                {
                                    JArray obj_phi = (JArray)dtl["data"];
                                    if (obj_phi.Count > 0)
                                    {
                                        sql = "DELETE FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                                        if (obj_phi.Count > 0)
                                        {
                                            for (int j = 0; j < obj_phi.Count; j++)
                                            {
                                                JObject js_phi = (JObject)obj_phi[j];

                                                if (js_phi["hdon_id"] != null)
                                                {
                                                    if (string.IsNullOrEmpty(js_phi["hdon_id"].ToString()))
                                                    {
                                                        js_phi["hdon_id"] = masterObj["hdon_id"];
                                                    }
                                                }
                                                else
                                                {
                                                    js_phi.Add("hdon_id", masterObj["hdon_id"]);
                                                }
                                                js_phi["phi_id"] = Guid.NewGuid();
                                                /*if (js_phi["phi_id"] != null)
                                                {
                                                    if (string.IsNullOrEmpty(js_phi["phi_id"].ToString()))
                                                    {
                                                        js_phi["phi_id"] = Guid.NewGuid();
                                                    }
                                                }
                                                else
                                                {
                                                    js_phi.Add("phi_id", Guid.NewGuid());
                                                }*/

                                                //if (fieldStt_rec0 != null)
                                                //{
                                                //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                                //}

                                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_phi_insert_n", CommandType.StoredProcedure, js_phi);
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion
                            #endregion
                            #region hatm luuthongtinkhac
                            JArray khac = null;

                            if (masterObj["hoadon68_khac"] != null)
                            {
                                if (masterObj.SelectToken("hoadon68_khac") is JArray)
                                {
                                    khac = (JArray)masterObj["hoadon68_khac"];
                                }
                            }

                            if (khac != null && khac.Count > 0)
                            {
                                foreach (var dtl in khac)
                                {
                                    JArray obj_khac = (JArray)dtl["data"];
                                    if (obj_khac.Count > 0)
                                    {
                                        sql = "DELETE FROM #SCHEMA_NAME#.hoadon68_khac WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                                        if (obj_khac.Count > 0)
                                        {
                                            for (int j = 0; j < obj_khac.Count; j++)
                                            {
                                                JObject js_phi = (JObject)obj_khac[j];

                                                if (js_phi["hdon_id"] != null)
                                                {
                                                    if (string.IsNullOrEmpty(js_phi["hdon_id"].ToString()))
                                                    {
                                                        js_phi["hdon_id"] = masterObj["hdon_id"];
                                                    }
                                                }
                                                else
                                                {
                                                    js_phi.Add("hdon_id", masterObj["hdon_id"]);
                                                }
                                                js_phi["khac_id"] = Guid.NewGuid();
                                                /*if (js_phi["phi_id"] != null)
                                                {
                                                    if (string.IsNullOrEmpty(js_phi["phi_id"].ToString()))
                                                    {
                                                        js_phi["phi_id"] = Guid.NewGuid();
                                                    }
                                                }
                                                else
                                                {
                                                    js_phi.Add("phi_id", Guid.NewGuid());
                                                }*/

                                                //if (fieldStt_rec0 != null)
                                                //{
                                                //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                                //}

                                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_phi_khac", CommandType.StoredProcedure, js_phi);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();

                            if (editMode != "3")
                            {
                                string select_cmd = $"SELECT hdon_id as id , * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='{id}'";
                                DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);
                                data = JArray.FromObject(tbl);
                                json.Add("data", data[0]);
                            }

                        }
                        else
                        {
                            json.Add("error", "Cần bổ sung chi tiết hóa đơn.");
                        }
                    }

                }
                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> SaveHoadon78(JObject obj, string username)
        {
            var json = new JObject();

            var data = (JArray)obj["data"];

            foreach (var d in data)
            {
                if (d["cctbao_id"] == null)
                {
                    json.Add("error", "cctbao_id chưa được nhập");
                    return json;
                }

            }

            json = await SaveHoadon(obj, username);
            return json;
        }

        public async Task<JObject> SaveHoadon78Pretreatment(JObject obj, string username)
        {
            JObject json = new JObject();

            try
            {
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    // convert data 32 - data 78

                    foreach (JProperty jp in masterObj.Properties().ToList())
                    {
                        if (jp.Name == "inv_invoicecode_id")
                        {
                            jp.Replace(new JProperty("cctbao_id", jp.Value));
                        }
                        if (jp.Name == "inv_invoiceauth_id")
                        {
                            jp.Replace(new JProperty("hdon_id", jp.Value));
                        }
                        if (jp.Name == "invoice_issued_date")
                        {
                            jp.Replace(new JProperty("nlap", jp.Value));
                        }
                        if (jp.Name == "invoice_number")
                        {
                            jp.Replace(new JProperty("shdon", jp.Value));
                        }
                        if (jp.Name == "status")
                        {
                            jp.Replace(new JProperty("tthai", jp.Value));
                        }
                        if (jp.Name == "original_invoice_id")
                        {
                            jp.Replace(new JProperty("hdon_id_old", jp.Value));
                        }
                        if (jp.Name == "order_number")
                        {
                            jp.Replace(new JProperty("sdhang", jp.Value));
                        }
                        if (jp.Name == "currency_code")
                        {
                            jp.Replace(new JProperty("dvtte", jp.Value));
                        }
                        if (jp.Name == "exchange_rate")
                        {
                            jp.Replace(new JProperty("tgia", jp.Value));
                        }
                        if (jp.Name == "payment_method_name")
                        {
                            jp.Replace(new JProperty("htttoan", jp.Value));
                        }
                        if (jp.Name == "seller_bank_account")
                        {
                            jp.Replace(new JProperty("stknban", jp.Value));
                        }
                        if (jp.Name == "seller_bank_name")
                        {
                            jp.Replace(new JProperty("tnhban", jp.Value));
                        }
                        if (jp.Name == "customer_code")
                        {
                            jp.Replace(new JProperty("mnmua", jp.Value));
                        }
                        if (jp.Name == "buyer_taxcode")
                        {
                            jp.Replace(new JProperty("mst", jp.Value));
                        }
                        if (jp.Name == "buyer_display_name")
                        {
                            jp.Replace(new JProperty("tnmua", jp.Value));
                        }
                        if (jp.Name == "buyer_email")
                        {
                            jp.Replace(new JProperty("email", jp.Value));
                        }
                        if (jp.Name == "buyer_legal_name")
                        {
                            jp.Replace(new JProperty("ten", jp.Value));
                        }
                        if (jp.Name == "buyer_address_line")
                        {
                            jp.Replace(new JProperty("dchi", jp.Value));
                        }
                        if (jp.Name == "buyer_bank_account")
                        {
                            jp.Replace(new JProperty("stknmua", jp.Value));
                        }
                        if (jp.Name == "buyer_tel")
                        {
                            jp.Replace(new JProperty("sdtnmua", jp.Value));
                        }
                        if (jp.Name == "buyer_bank_name")
                        {
                            jp.Replace(new JProperty("tnhmua", jp.Value));
                        }
                        if (jp.Name == "total_amount_without_vat")
                        {
                            jp.Replace(new JProperty("tgtcthue", jp.Value));
                        }
                        if (jp.Name == "vat_amount")
                        {
                            jp.Replace(new JProperty("tgtthue", jp.Value));
                        }
                        if (jp.Name == "total_amount")
                        {
                            jp.Replace(new JProperty("tgtttbso", jp.Value));
                        }
                        if (jp.Name == "total_amount_last")
                        {
                            jp.Replace(new JProperty("tgtttbso_last", jp.Value));
                        }
                        if (jp.Name == "branch_code")
                        {
                            jp.Replace(new JProperty("mdvi", jp.Value));
                        }
                        if (jp.Name == "invoice_status")
                        {
                            jp.Replace(new JProperty("tthdon", jp.Value));
                        }
                        if (jp.Name == "invoice_series")
                        {
                            jp.Replace(new JProperty("khieu", jp.Value));
                        }
                        if (jp.Name == "user_new")
                        {
                            jp.Replace(new JProperty("nglap", jp.Value));
                        }
                        if (jp.Name == "date_new")
                        {
                            jp.Replace(new JProperty("tdlap", jp.Value));
                        }
                        if (jp.Name == "date_edit")
                        {
                            jp.Replace(new JProperty("nsua", jp.Value));
                        }
                        if (jp.Name == "user_edit")
                        {
                            jp.Replace(new JProperty("ngsua", jp.Value));
                        }
                    }

                    // end convert data

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    JArray detailsNew = new JArray();

                    if (details != null && details.Count > 0 && ((JArray)(details[0]["data"])).Count > 0)
                    {

                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];

                            if (obj_details.Count > 0)
                            {
                                for (int j = 0; j < obj_details.Count; j++)
                                {
                                    JObject js_details = (JObject)obj_details[j];

                                    // convert detail data tt32 - tt78
                                    foreach (JProperty jp in js_details.Properties().ToList())
                                    {
                                        if (jp.Name == "row_ord")
                                        {
                                            jp.Replace(new JProperty("stt", jp.Value));
                                        }
                                        if (jp.Name == "item_code")
                                        {
                                            jp.Replace(new JProperty("ma", jp.Value));
                                        }
                                        if (jp.Name == "item_name")
                                        {
                                            jp.Replace(new JProperty("ten", jp.Value));
                                        }
                                        if (jp.Name == "unit_code")
                                        {
                                            jp.Replace(new JProperty("mdvtinh", jp.Value));
                                        }
                                        if (jp.Name == "unit_name")
                                        {
                                            jp.Replace(new JProperty("dvtinh", jp.Value));
                                        }
                                        if (jp.Name == "unit_price")
                                        {
                                            jp.Replace(new JProperty("dgia", jp.Value));
                                        }
                                        if (jp.Name == "quantity")
                                        {
                                            jp.Replace(new JProperty("sluong", jp.Value));
                                        }
                                        if (jp.Name == "total_amount_without_vat")
                                        {
                                            jp.Replace(new JProperty("thtien", jp.Value));
                                        }
                                        if (jp.Name == "vat_amount")
                                        {
                                            jp.Replace(new JProperty("tthue", jp.Value));
                                        }
                                        if (jp.Name == "total_amount")
                                        {
                                            jp.Replace(new JProperty("tgtien", jp.Value));
                                        }
                                        //if (jp.Name == "promotion")
                                        //{
                                        //    jp.Replace(new JProperty("kmai", jp.Value));
                                        //}
                                        if (jp.Name == "discount_percentage")
                                        {
                                            jp.Replace(new JProperty("tlckhau", jp.Value));
                                        }
                                        if (jp.Name == "discount_amount")
                                        {
                                            jp.Replace(new JProperty("stckhau", jp.Value));
                                        }
                                        if (jp.Name == "tax_type")
                                        {
                                            jp.Replace(new JProperty("tsuat", jp.Value));
                                        }
                                    }

                                }
                            }

                            dtl["data"] = obj_details;
                            detailsNew.Add(dtl);
                        }

                    }

                    masterObj["details"] = detailsNew;

                    data[i] = masterObj;
                }

                obj["data"] = data;

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                return json;
            }

            json = await SaveHoadon(obj, username);

            try
            {
                JObject json_data = (JObject)json["data"];

                foreach (JProperty jp in json_data.Properties().ToList())
                {
                    if (jp.Name == "cctbao_id")
                    {
                        jp.Replace(new JProperty("inv_invoicecode_id", jp.Value));
                    }
                    if (jp.Name == "hdon_id")
                    {
                        jp.Replace(new JProperty("inv_invoiceauth_id", jp.Value));
                    }
                    if (jp.Name == "nlap")
                    {
                        jp.Replace(new JProperty("invoice_issued_date", jp.Value));
                    }
                    if (jp.Name == "shdon")
                    {
                        jp.Replace(new JProperty("invoice_number", jp.Value));
                    }
                    if (jp.Name == "tthai")
                    {
                        jp.Replace(new JProperty("status", jp.Value));
                    }
                    if (jp.Name == "hdon_id_old")
                    {
                        jp.Replace(new JProperty("original_invoice_id", jp.Value));
                    }
                    if (jp.Name == "sdhang")
                    {
                        jp.Replace(new JProperty("order_number", jp.Value));
                    }
                    if (jp.Name == "dvtte")
                    {
                        jp.Replace(new JProperty("currency_code", jp.Value));
                    }
                    if (jp.Name == "tgia")
                    {
                        jp.Replace(new JProperty("exchange_rate", jp.Value));
                    }
                    if (jp.Name == "htttoan")
                    {
                        jp.Replace(new JProperty("payment_method_name", jp.Value));
                    }
                    if (jp.Name == "stknban")
                    {
                        jp.Replace(new JProperty("seller_bank_account", jp.Value));
                    }
                    if (jp.Name == "tnhban")
                    {
                        jp.Replace(new JProperty("seller_bank_name", jp.Value));
                    }
                    if (jp.Name == "customer_code")
                    {
                        jp.Replace(new JProperty("mnmua", jp.Value));
                    }
                    if (jp.Name == "mst")
                    {
                        jp.Replace(new JProperty("buyer_taxcode", jp.Value));
                    }
                    if (jp.Name == "tnmua")
                    {
                        jp.Replace(new JProperty("buyer_display_name", jp.Value));
                    }
                    if (jp.Name == "email")
                    {
                        jp.Replace(new JProperty("buyer_email", jp.Value));
                    }
                    if (jp.Name == "ten")
                    {
                        jp.Replace(new JProperty("buyer_legal_name", jp.Value));
                    }
                    if (jp.Name == "dchi")
                    {
                        jp.Replace(new JProperty("buyer_address_line", jp.Value));
                    }
                    if (jp.Name == "stknmua")
                    {
                        jp.Replace(new JProperty("buyer_bank_account", jp.Value));
                    }
                    if (jp.Name == "sdtnmua")
                    {
                        jp.Replace(new JProperty("buyer_tel", jp.Value));
                    }
                    if (jp.Name == "tnhmua")
                    {
                        jp.Replace(new JProperty("buyer_bank_name", jp.Value));
                    }
                    if (jp.Name == "tgtcthue")
                    {
                        jp.Replace(new JProperty("total_amount_without_vat", jp.Value));
                    }
                    if (jp.Name == "tgtthue")
                    {
                        jp.Replace(new JProperty("vat_amount", jp.Value));
                    }
                    if (jp.Name == "tgtttbso")
                    {
                        jp.Replace(new JProperty("total_amount", jp.Value));
                    }
                    if (jp.Name == "tgtttbso_last")
                    {
                        jp.Replace(new JProperty("total_amount_last", jp.Value));
                    }
                    if (jp.Name == "mdvi")
                    {
                        jp.Replace(new JProperty("branch_code", jp.Value));
                    }
                    if (jp.Name == "tthdon")
                    {
                        jp.Replace(new JProperty("invoice_status", jp.Value));
                    }
                    if (jp.Name == "nky")
                    {
                        jp.Replace(new JProperty("signed_date", jp.Value));
                    }
                    if (jp.Name == "ngky")
                    {
                        jp.Replace(new JProperty("signer", jp.Value));
                    }
                    if (jp.Name == "tgtttbchu")
                    {
                        jp.Replace(new JProperty("amount_to_word", jp.Value));
                    }
                    if (jp.Name == "sbmat")
                    {
                        jp.Replace(new JProperty("security_number", jp.Value));
                    }
                    if (jp.Name == "khieu")
                    {
                        jp.Replace(new JProperty("invoice_series", jp.Value));
                    }
                    if (jp.Name == "nglap")
                    {
                        jp.Replace(new JProperty("user_new", jp.Value));
                    }
                    if (jp.Name == "tdlap")
                    {
                        jp.Replace(new JProperty("date_new", jp.Value));
                    }
                    if (jp.Name == "nsua")
                    {
                        jp.Replace(new JProperty("date_edit", jp.Value));
                    }
                    if (jp.Name == "ngsua")
                    {
                        jp.Replace(new JProperty("user_edit", jp.Value));
                    }
                }

                json["data"] = json_data;
            }
            catch { }

            return json;
        }

        public async Task<JObject> GetData(string select, string ptrang, int start, int count, string filter, string tlbparam)
        {
            JObject json = new JObject();
            try
            {
                //string select = "SELECT * ,hdon_id as id, tnmua || '/' || ten as ten  FROM #SCHEMA_NAME#.hoadon68 ";
                //string ptrang = "ORDER BY shdon OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
                string txtFilter = "";

                if (filter != null && filter != "null" && filter != " ")
                {
                    JArray array = JArray.Parse(filter);
                    JObject js;
                    for (int i = 0; i < array.Count; i++)
                    {
                        js = (JObject)array[i];
                        if (js["columnType"] == null)
                        {
                            if (!string.IsNullOrEmpty(js["value"].ToString()) && !"null".Equals(js["value"].ToString()))
                            {
                                txtFilter += " AND " + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), "nvarchar");
                            }
                            if (!string.IsNullOrEmpty(js["value"].ToString()) && "null".Equals(js["value"].ToString()))
                            {
                                txtFilter += " AND " + js["columnName"] + " IS NULL ";
                            }
                        }
                        else
                        {
                            string type = js["columnType"].ToString();
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                            {
                                if (type == "datetime")
                                {
                                    JObject value = (JObject)js["value"];
                                    JToken jstart = (JToken)value["start"];
                                    JToken jend = (JToken)value["end"];

                                    if (jstart.Type != JTokenType.Null)
                                    {
                                        DateTime startTime = Convert.ToDateTime(jstart.ToString());
                                        startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime)).AddDays(-1);
                                        //startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime));

                                        txtFilter += " AND " + js["columnName"] + ">='" + string.Format("{0:yyyy-MM-dd}", startTime) + "' ";
                                    }

                                    if (jend.Type != JTokenType.Null)
                                    {
                                        DateTime endTime = Convert.ToDateTime(jend.ToString());
                                        endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime)).AddDays(1);
                                        //endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime));

                                        txtFilter += " AND " + js["columnName"] + "<='" + string.Format("{0:yyyy-MM-dd}", endTime) + "' ";
                                    }
                                }
                                else
                                {
                                    if (type == "numeric")
                                    {
                                        txtFilter += "AND CAST(" + js["columnName"] + " AS TEXT)" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                    //check ky hieu hoa don
                                    else if (type == "nvarchar" && js["columnName"].ToString().Equals("cctbao_id"))
                                    {
                                        txtFilter += " AND " + js["columnName"] + " IN " +
                                                     js["value"].ToString();
                                    }
                                    else if (type == "nvarchar" && js["columnName"].ToString().Equals("null"))
                                    {
                                        txtFilter += " AND " + js["columnName"] + " IS NULL ";
                                    }
                                    else
                                    {
                                        txtFilter += " AND lower(" + js["columnName"] + ")" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                }
                            }
                        }
                    }

                }
                if (!string.IsNullOrEmpty(tlbparam) && tlbparam != "null")
                {
                    JArray arr = JArray.Parse(tlbparam);

                    for (int i = 0; i < arr.Count; i++)
                    {
                        JObject jss = (JObject)arr[i];

                        string value = jss["value"].ToString();
                        value = value.Replace("'", "''");

                        txtFilter += " AND " + jss["columnName"] + "='" + value + "' ";

                    }
                }
                if (!string.IsNullOrEmpty(txtFilter))
                    txtFilter = " WHERE 1 = 1 " + txtFilter;
                var sql = await this._minvoiceDbContext.GetDataTableAsync(select + txtFilter + ptrang);

                var total = await this._minvoiceDbContext.GetDataTableAsync(select + txtFilter);

                JArray data = JArray.FromObject(sql);
                json.Add("data", data);
                json.Add("pos", start);
                if (start == 0)
                    json.Add("total_count", total.Rows.Count);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }
        public async Task<JObject> GetDataMaster(string select, string ptrang, int start, int count, string filter, string tlbparam)
        {
            JObject json = new JObject();
            try
            {
                //string select = "SELECT * ,hdon_id as id, tnmua || '/' || ten as ten  FROM #SCHEMA_NAME#.hoadon68 ";
                //string ptrang = "ORDER BY shdon OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
                string txtFilter = "";

                if (filter != null && filter != "null" && filter != " ")
                {
                    JArray array = JArray.Parse(filter);
                    JObject js;
                    for (int i = 0; i < array.Count; i++)
                    {
                        js = (JObject)array[i];
                        if (js["columnType"] == null)
                        {
                            if (!string.IsNullOrEmpty(js["value"].ToString()) && !"null".Equals(js["value"].ToString()))
                            {
                                txtFilter += " AND " + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), "nvarchar");
                            }
                            if (!string.IsNullOrEmpty(js["value"].ToString()) && "null".Equals(js["value"].ToString()))
                            {
                                txtFilter += " AND " + js["columnName"] + " IS NULL ";
                            }
                        }
                        else
                        {
                            string type = js["columnType"].ToString();
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                            {
                                if (type == "datetime")
                                {
                                    JObject value = (JObject)js["value"];
                                    JToken jstart = (JToken)value["start"];
                                    JToken jend = (JToken)value["end"];

                                    if (jstart.Type != JTokenType.Null)
                                    {
                                        DateTime startTime = Convert.ToDateTime(jstart.ToString());
                                        startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime)).AddDays(-1);
                                        //startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime));

                                        txtFilter += " AND " + js["columnName"] + ">='" + string.Format("{0:yyyy-MM-dd}", startTime) + "' ";
                                    }

                                    if (jend.Type != JTokenType.Null)
                                    {
                                        DateTime endTime = Convert.ToDateTime(jend.ToString());
                                        endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime)).AddDays(1);
                                        //endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime));

                                        txtFilter += " AND " + js["columnName"] + "<='" + string.Format("{0:yyyy-MM-dd}", endTime) + "' ";
                                    }
                                }
                                else
                                {
                                    if (type == "numeric")
                                    {
                                        txtFilter += "AND CAST(" + js["columnName"] + " AS TEXT)" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                    //check ky hieu hoa don
                                    else if (type == "nvarchar" && js["columnName"].ToString().Equals("cctbao_id"))
                                    {
                                        txtFilter += " AND " + js["columnName"] + " IN " +
                                                     js["value"].ToString();
                                    }
                                    else if (type == "nvarchar" && js["columnName"].ToString().Equals("null"))
                                    {
                                        txtFilter += " AND " + js["columnName"] + " IS NULL ";
                                    }
                                    else
                                    {
                                        txtFilter += " AND " + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                }
                            }
                        }
                    }

                }
                if (!string.IsNullOrEmpty(tlbparam) && tlbparam != "null")
                {
                    JArray arr = JArray.Parse(tlbparam);

                    for (int i = 0; i < arr.Count; i++)
                    {
                        JObject jss = (JObject)arr[i];

                        string value = jss["value"].ToString();
                        value = value.Replace("'", "''");

                        txtFilter += " AND " + jss["columnName"] + "='" + value + "' ";

                    }
                }
                if (!string.IsNullOrEmpty(txtFilter))
                    txtFilter = " WHERE 1 = 1 " + txtFilter;
                var sql = await this._masterInvoiceDbContext.GetDataTableAsync(select + txtFilter + ptrang);

                var total = await this._masterInvoiceDbContext.GetDataTableAsync(select + txtFilter);

                JArray data = JArray.FromObject(sql);
                json.Add("data", data);
                json.Add("pos", start);
                if (start == 0)
                    json.Add("total_count", total.Rows.Count);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }
        public async Task<JObject> GetDataNewWith_Bangke(string select, string ptrang, int start, int count, string filter, string tlbparam)
        {
            JObject json = new JObject();
            try
            {
                //string select = "SELECT * ,hdon_id as id, tnmua || '/' || ten as ten  FROM #SCHEMA_NAME#.hoadon68 ";
                //string ptrang = "ORDER BY shdon OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
                string txtFilter = "";

                if (filter != null && filter != "null" && filter != " ")
                {
                    JArray array = JArray.Parse(filter);
                    JObject js;
                    for (int i = 0; i < array.Count; i++)
                    {
                        js = (JObject)array[i];
                        if (js["columnType"] == null)
                        {
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                            {
                                txtFilter += " AND a." + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), "nvarchar");
                            }
                        }
                        else
                        {
                            string type = js["columnType"].ToString();
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                                if (type == "datetime")
                                {
                                    JObject value = (JObject)js["value"];
                                    JToken jstart = (JToken)value["start"];
                                    JToken jend = (JToken)value["end"];

                                    if (jstart.Type != JTokenType.Null)
                                    {
                                        DateTime startTime = Convert.ToDateTime(jstart.ToString());
                                        startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime)).AddDays(1);
                                        // startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime));

                                        txtFilter += " AND a." + js["columnName"] + ">='" + string.Format("{0:yyyy-MM-dd}", startTime) + "' ";
                                    }

                                    if (jend.Type != JTokenType.Null)
                                    {
                                        DateTime endTime = Convert.ToDateTime(jend.ToString());
                                        endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime)).AddDays(1);
                                        // endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime));

                                        txtFilter += " AND a." + js["columnName"] + "<='" + string.Format("{0:yyyy-MM-dd}", endTime) + "' ";
                                    }
                                }
                                else
                                {
                                    if (type == "numeric")
                                    {
                                        txtFilter += "AND CAST(" + "a." + js["columnName"] + " AS TEXT)" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                    else
                                    {
                                        txtFilter += " AND lower(" + js["columnName"] + ")" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                }
                        }
                    }

                }
                if (tlbparam != null)
                {
                    JArray arr = JArray.Parse(tlbparam);

                    for (int i = 0; i < arr.Count; i++)
                    {
                        JObject jss = (JObject)arr[i];

                        string value = jss["value"].ToString();
                        value = value.Replace("'", "''");

                        txtFilter += " AND a." + jss["columnName"] + "='" + value + "' ";

                    }
                }
                if (!string.IsNullOrEmpty(txtFilter))
                    txtFilter = " WHERE 1 = 1 " + txtFilter;
                var sql = await this._minvoiceDbContext.GetDataTableAsync(select + txtFilter + ptrang);

                var total = await this._minvoiceDbContext.GetDataTableAsync(select + txtFilter);

                JArray data = JArray.FromObject(sql);
                json.Add("data", data);
                json.Add("pos", start);
                if (start == 0)
                    json.Add("total_count", total.Rows.Count);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }
        public async Task<string> GetSqlData(string select, string filter, string tlbparam)
        {
            JObject json = new JObject();
            try
            {
                //string select = "SELECT * ,hdon_id as id, tnmua || '/' || ten as ten  FROM #SCHEMA_NAME#.hoadon68 ";
                //string ptrang = "ORDER BY shdon OFFSET " + start + " ROWS FETCH NEXT " + count + " ROW ONLY ";
                string txtFilter = "";

                if (filter != null && filter != "null" && filter != " ")
                {
                    JArray array = JArray.Parse(filter);
                    JObject js;
                    for (int i = 0; i < array.Count; i++)
                    {
                        js = (JObject)array[i];
                        if (js["columnType"] == null)
                        {
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                            {
                                txtFilter += " AND a." + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), "nvarchar");
                            }
                        }
                        else
                        {
                            string type = js["columnType"].ToString();
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                                if (type == "datetime")
                                {
                                    JObject value = (JObject)js["value"];
                                    JToken jstart = (JToken)value["start"];
                                    JToken jend = (JToken)value["end"];

                                    if (jstart.Type != JTokenType.Null)
                                    {
                                        DateTime startTime = Convert.ToDateTime(jstart.ToString());
                                        startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime)).AddDays(-1);
                                        //startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime));

                                        txtFilter += " AND a." + js["columnName"] + ">='" + string.Format("{0:yyyy-MM-dd}", startTime) + "' ";
                                    }

                                    if (jend.Type != JTokenType.Null)
                                    {
                                        DateTime endTime = Convert.ToDateTime(jend.ToString());
                                        endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime)).AddDays(1);
                                        //endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime));

                                        txtFilter += " AND a." + js["columnName"] + "<='" + string.Format("{0:yyyy-MM-dd}", endTime) + "' ";
                                    }
                                }
                                else
                                {
                                    if (type == "numeric")
                                    {
                                        txtFilter += "AND CAST(" + "a." + js["columnName"] + " AS TEXT)" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                    else
                                    {
                                        txtFilter += " AND lower(a." + js["columnName"] + ")" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                }
                        }
                    }

                }
                if (tlbparam != null)
                {
                    JArray arr = JArray.Parse(tlbparam);

                    for (int i = 0; i < arr.Count; i++)
                    {
                        JObject jss = (JObject)arr[i];

                        string value = jss["value"].ToString();
                        value = value.Replace("'", "''");

                        txtFilter += " AND a." + jss["columnName"] + "='" + value + "' ";

                    }
                }
                if (!string.IsNullOrEmpty(txtFilter))
                    txtFilter = " WHERE 1 = 1 " + txtFilter;
                return select + txtFilter;

                //var total = await this._minvoiceDbContext.GetDataTableAsync(select + txtFilter);

            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public async Task<JArray> GetDataDangkysudungDetail(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.mau01_68_chitiet WHERE mau01_id = '" + id + "' ORDER BY stt";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }


        public async Task<JArray> GetDataDangkysudungUyNhiem(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE mau01_id = '" + id + "' ORDER BY stt";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }

        public async Task<JArray> GetDataBangTHDLDetail(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet WHERE bangtonghopdl_68_id = '" + id + "' ORDER BY Cast (stt as numeric)";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }

        public async Task<JArray> GetResponseMessage(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.tonghopmaloiphanhoi_68 WHERE reference_id = '" + id + "'";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }
        public async Task<JArray> GetHistoryInvoice(string id)
        {
            JArray result = new JArray();

            string sql = $@"select * from (SELECT a.tgian_gui, a.mltdiep_gui, a.nguoi_gui, a.note, a.noidung, a.id as id,a.type_id as typeid FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 as a where type_id ='{id}' or (type_id in (select mau04_id from #SCHEMA_NAME#.mau04_68_chitiet where  hdon_id ='{id}'))
                           UNION SELECT b.tgian_gui, b.mltdtchieu as mltdiep_gui, b.nguoi_gui, b.note, b.is_error as noidung, b.ketqua_phkt_68_id as id,b.type_id as typeid FROM #SCHEMA_NAME#.ketqua_phkt_68 as b where type_id ='{id}' or (type_id in (select mau04_id from #SCHEMA_NAME#.mau04_68_chitiet where  hdon_id ='{id}'))
                           UNION select c.tgian_gui, c.mltdiep_tchieu as mltdiep_gui, c.nguoi_gui, c.note, c.is_error, c.ketqua_ktdl_68_id as id,c.type_id as typeid FROM #SCHEMA_NAME#.ketqua_ktdl_68 as c where type_id ='{id}' or (type_id in (select mau04_id from #SCHEMA_NAME#.mau04_68_chitiet where  hdon_id ='{id}'))
                           UNION select d.tgian_gui, d.mltdiepphoi as mltdiep_gui, d.nguoi_gui, d.note, d.noidung, d.ketqua_rasoathd_68_id as id,d.type_id as typeid FROM #SCHEMA_NAME#.ketqua_rasoathd_68 as d where type_id ='{id}' or (type_id in (select mau04_id from #SCHEMA_NAME#.mau04_68_chitiet where  hdon_id ='{id}'))
                           UNION select e.tgian_gui, e.mltdiepcqt as mltdiep_gui, e.nguoi_gui, e.note, e.noidung, e.ketqua_capmahd_68_id as id,e.type_id as typeid FROM #SCHEMA_NAME#.ketqua_capmahd_68 as e where type_id ='{id}' or (type_id in (select mau04_id from #SCHEMA_NAME#.mau04_68_chitiet where  hdon_id ='{id}'))
                           UNION select f.tgian_gui, f.mltdiepcqt as mltdiep_gui, f.nguoi_gui, f.note, f.tttnhan_cqt as noidung, f.ketqua_xulyhdsaisot_68_id as id,f.type_id as typeid FROM #SCHEMA_NAME#.ketqua_xulyhdsaisot_68 as f where type_id ='{id}' or (type_id in (select mau04_id from #SCHEMA_NAME#.mau04_68_chitiet where  hdon_id ='{id}'))) a
                           ORDER BY tgian_gui";

            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }

        public async Task<JArray> GetDataInvoiceDetail(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id = '" + id + "' ORDER BY Cast (stt as numeric)";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }

        public async Task<JArray> GetDataInvoicePhi(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id = '" + id + "'";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }
        public async Task<JArray> GetDataMau0405Detail(string id)
        {
            JArray result = new JArray();
            // string sql = "SELECT to_char(mc.ngay,'YYY/MM/DD') as ngay , mc.* FROM #SCHEMA_NAME#.mau04_68_chitiet mc WHERE mc.mau04_id = '" + id + "' ORDER BY stt";
            //string sql = "select * from #SCHEMA_NAME#.mau04_68_chitiet mc join #SCHEMA_NAME#.mau04_68 m on mc.mau04_id = m.mau04_id where mc.mau04_id = '" + id + "' ORDER BY stt";
            string sql = $@"SELECT to_char(ngay, 'yyyy-MM-dd') as ngay,
                                   mau04_id,
                                   mau04_chitiet_id,
                                   stt,
                                   (case when position(';' in khieu) > 0 then split_part(khieu, ';', 2)
                                       else khieu
                                       end) as khieu,
                                   (case when position(';' in khieu) > 0 then split_part(khieu, ';', 1)
                                       else substring(khieu, 1, 1)
                                       end) as mau_so,
                                   shdon,
                                   ngay,
                                   ladhddt,
                                   tctbao,
                                   ldo,
                                   hdon_id,
                                   tttnhan,
                                   mcqtcap,
                                   ly_do
                            FROM #SCHEMA_NAME#.mau04_68_chitiet
                            WHERE mau04_id = '{id}'
                            ORDER BY stt";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }

        public string GetCharQRCodeByTT(string ID, string giaTriTT)
        {
            int numberLength = 10;

            var lengthGiaTriTT = giaTriTT.Length;
            var charLength = lengthGiaTriTT < numberLength ? $"0{lengthGiaTriTT.ToString()}" : lengthGiaTriTT.ToString();

            return $"{ID}{charLength}{giaTriTT}";
        }

        public async Task<JObject> XmlInvoice(string hdon_id)
        {
            JObject _obj = new JObject();
            try
            {
                string qry = $"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code='{_webHelper.GetDvcs()}'";
                DataTable dmdvcs = await this._minvoiceDbContext.GetDataTableAsync(qry);
                string sql = $"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id = '{hdon_id}'";
                DataTable dtInvoice = await this._minvoiceDbContext.GetDataTableAsync(sql);

                //get DLQRCODE
                var dinhDanhDuyNhatToanCau = GetCharQRCodeByTT("00", Guid.NewGuid().ToString().Replace("-", ""));
                var maSoThue = GetCharQRCodeByTT("01", dmdvcs.Rows[0]["tax_code"].ToString().Replace("-", ""));
                var kyHieuMauSoHoaDon = GetCharQRCodeByTT("02", dtInvoice.Rows[0]["khieu"].ToString().Substring(0, 1));
                var kyHieuHoaDon = GetCharQRCodeByTT("03", dtInvoice.Rows[0]["khieu"].ToString().Substring(1, dtInvoice.Rows[0]["khieu"].ToString().Length - 1));
                var soHoaDon = GetCharQRCodeByTT("04", dtInvoice.Rows[0]["shdon"].ToString()); //shdon
                var ngayLapHD = GetCharQRCodeByTT("05", ((DateTime)dtInvoice.Rows[0]["nlap"]).ToString("yyyyMMdd")); //nlap
                var tongTienTTBangSo = GetCharQRCodeByTT("06", DoubleStrToString(dtInvoice.Rows[0]["tgtttbso_last"].ToString()));

                var charGetThongTinHoaDon = $"{dinhDanhDuyNhatToanCau}{maSoThue}{kyHieuMauSoHoaDon}{kyHieuHoaDon}{soHoaDon}{ngayLapHD}{tongTienTTBangSo}";

                var thongTinHoaDon = GetCharQRCodeByTT("99", charGetThongTinHoaDon);
                //get charDLQRCode not has Value CRC

                // get mã CRC
                var charGetCRC = $"{thongTinHoaDon}6304";
                string valueCRC = GetCRC.CalcCRC16(Encoding.ASCII.GetBytes(charGetCRC));

                var charDLQRCode = $"{charGetCRC}{valueCRC}";

                string khhdon = dtInvoice.Rows[0]["khieu"].ToString();
                string khmshdon = khhdon.Substring(0, 1);
                string tenloaihdon = string.Empty;

                bool lddnBo = dtInvoice.Rows[0].Table.Columns.Contains("lddnbo") && !string.IsNullOrEmpty(dtInvoice.Rows[0]["lddnbo"].ToString());

                switch (Convert.ToInt32(khmshdon))
                {
                    case 1:
                        tenloaihdon = "Hóa đơn giá trị gia tăng";
                        break;
                    case 2:
                        tenloaihdon = "Hóa đơn bán hàng";
                        break;
                    case 3:
                        tenloaihdon = "Hóa đơn điện tử bán tài sản công";
                        break;
                    case 4:
                        tenloaihdon = "Hóa đơn điện tử bán hàng dự trữ quốc gia";
                        break;
                    //case 5:
                    //    tenloaihdon = "Phiếu xuất kho kiêm vận chuyển nội bộ";
                    //    break;
                    case 5:
                        tenloaihdon = "Hóa đơn khác: tem, vé, thẻ, phiếu...";
                        break;
                    case 6:
                        if (lddnBo)
                        {
                            tenloaihdon = "Phiếu xuất kho kiêm vận chuyển nội bộ";
                        }
                        else
                        {
                            tenloaihdon = "Phiếu xuất kho hàng gửi bán đại lý";
                        }
                        break;
                    //case 7:
                    //    tenloaihdon = "Hóa đơn khác: tem, vé, thẻ, phiếu...";
                    //    break;
                    default:
                        tenloaihdon = "Hóa đơn giá trị gia tăng";
                        break;
                }

                bool giamthuebanhang20 = false;
                string thueGTGTBanHang = "";
                try
                {
                    giamthuebanhang20 = dtInvoice.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(dtInvoice.Rows[0]["giamthuebanhang20"].ToString());
                    string dvtte = dtInvoice.Rows[0]["dvtte"]?.ToString();

                    CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");

                    string tongTienGiam = double.Parse(DoubleStrToString(dtInvoice.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                    tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");
                    thueGTGTBanHang = "Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15";

                }
                catch (Exception ex)
                {
                    //Log.Error(ex);
                }

                string sqlDetails = "SELECT kmai as \"TChat\", stt as \"STT\",ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                 + " sluong as \"SLuong\", dgia as \"DGia\", tlckhau as \"TLCKhau\", stckhau as \"STCKhau\","
                 + " thtien as \"ThTien\", tsuat as \"TSuat\",tsuatstr as \"TSuatStr\",* "
                 + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' order by Cast (stt as numeric)";



                /*if (khmshdon == "3")
                {
                    sqlDetails = "SELECT 1 as \"TChat\", stt as \"STT\",ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                    + " sluong as \"SLuong\", dgia as \"DGia\","
                    + " thtien as \"ThTien\""
                    + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}'";
                }
                if (khmshdon == "2")
                {
                    sqlDetails = "SELECT 1 as \"TChat\", stt as \"STT\",ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                    + " sluong as \"SLuong\", dgia as \"DGia\", tlckhau as \"TLCKhau\", stckhau as \"STCKhau\","
                    + " thtien as \"ThTien\", tsuat as \"TSuat\""
                    + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}'";
                }*/
                DataTable dtInvocieDetails = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                string[] notGet = new string[] { "kmai", "stt", "ma", "ten", "mdvtinh", "sluong", "dgia", "tlckhau", "stckhau", "thtien", "tsuat", "tsuatstr", "hdon_id", "cthdon_id",
                    "kmai1", "stt1", "ma1", "ten1", "mdvtinh1", "sluong1", "dgia1", "tlckhau1", "stckhau1", "thtien1", "tsuat1", "tsuatstr1", "hdon_id1", "cthdon_id1","dvtinh1",
                    "TChat", "STT", "MHHDVu", "THHDVu", "DVTinh", "SLuong", "DGia", "TLCKhau", "STCKhau", "ThTien", "TSuat", "TSuatStr","tsuatstr","ptthue"};

                var clumDetail = dtInvocieDetails.Columns;

                string sqlThue = "SELECT tsuat as \"TSuat\","
                + " SUM(Case when kmai ='3' then (thtien * (-1)) else thtien end) as \"ThTien\", SUM(Case when kmai ='3' then (tthue * (-1)) else tthue end ) as \"TThue\" "
                + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' and (kmai ='1' or kmai ='2' or kmai ='3') GROUP BY tsuat";
                DataTable dtInvocieThue = await this._minvoiceDbContext.GetDataTableAsync(sqlThue);


                XElement thue;
                if (dtInvocieThue.Rows.Count > 0)
                {
                    thue = new XElement("THTTLTSuat",
                                            dtInvocieThue.AsEnumerable().Select(row2 => new XElement("LTSuat",
                                                GetDataRowDoubble("TSuat", row2, "TSuat", phantram: true),
                                                           GetDataRowDoubble("ThTien", row2, "ThTien"),
                                                           GetDataRowDoubble("TThue", row2, "TThue"))
                                                )
                                            );
                }
                else
                {
                    thue = new XElement("THTTLTSuat",
                                            new XElement("LTSuat",
                                                new XElement("TSuat", "0%"),
                                                new XElement("ThTien", 0),
                                                new XElement("TThue", 0)
                                                ));
                }

                string sqlPhi = "SELECT tnphi as \"TLPhi\", tienphi as \"TPhi\""
                + $" FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id ='{hdon_id}'";
                DataTable dtInvociePhi = await this._minvoiceDbContext.GetDataTableAsync(sqlPhi);

                var dataHoadon = dtInvocieDetails.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();
                var dataThue = dtInvocieThue.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();
                var dataPhi = dtInvociePhi.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string tdlap = string.Empty;
                string tdky = string.Empty;
                string nlap = string.Empty;

                if (string.IsNullOrEmpty(dtInvoice.Rows[0]["tdlap"].ToString()))
                {
                    DateTime dt = DateTime.Now;
                    tdlap = dt.ToString("yyyy-MM-dd");
                    tdky = dt.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(dtInvoice.Rows[0]["tdlap"].ToString());
                    tdlap = dt.ToString("yyyy-MM-dd");
                    tdky = dt.ToString("yyyy-MM-dd");
                }
                if (string.IsNullOrEmpty(dtInvoice.Rows[0]["nlap"].ToString()))
                {
                    DateTime dt = DateTime.Now;
                    nlap = dt.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(dtInvoice.Rows[0]["nlap"].ToString());
                    nlap = dt.ToString("yyyy-MM-dd");
                }
                DateTime nbke = DateTime.Now;
                string tthdon = dtInvoice.Rows[0]["tthdon"].ToString();
                //int sohdon = shdon > 0 ? shdon : Convert.ToInt32(string.IsNullOrEmpty(dtInvoice.Rows[0]["shdon"].ToString()) ? "0" : dtInvoice.Rows[0]["shdon"].ToString());
                int sohdon = 0;
                int.TryParse(string.IsNullOrEmpty(dtInvoice.Rows[0]["shdon"].ToString()) ? "0" : dtInvoice.Rows[0]["shdon"].ToString(), out sohdon);

                // get thông tin đơn vị ủy nhiệm tạo hóa đơn
                string mSTDVNUNLHDon = "";
                string tDVNUNLHDon = "";
                string dCDVNUNLHDon = "";


                string sqlSelect_mau01_68_uynhiem = $"SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE khhdunhiem ='{khhdon}'";
                DataTable dtMau01_68_uynhiem = await this._minvoiceDbContext.GetDataTableAsync(sqlSelect_mau01_68_uynhiem);
                if (dtMau01_68_uynhiem.Rows.Count > 0)
                {
                    mSTDVNUNLHDon = dtMau01_68_uynhiem.Rows[0]["mstunhiem"].ToString();
                    tDVNUNLHDon = dtMau01_68_uynhiem.Rows[0]["ttcdunhiem"].ToString();
                }

                // với hóa đơn điều chỉnh, thay thế, tìm thông tin hóa đơn gốc
                int lhdclquan = 0;
                string khmshdclquan = "";
                string nlap_hdon_orginal = "";
                string khieu_orginal = "";
                string shdon_orginal = "";

                if (tthdon == "2" || tthdon == "19" || tthdon == "21" || tthdon == "23")
                {
                    if (dtInvoice.Rows[0].Table.Columns.Contains("hdon68_id_lk") && !string.IsNullOrEmpty(dtInvoice.Rows[0]["hdon68_id_lk"].ToString()))
                    {
                        string sql1 = $"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id = '{dtInvoice.Rows[0]["hdon68_id_lk"].ToString()}'";
                        DataTable dtInvoice1 = await this._minvoiceDbContext.GetDataTableAsync(sql1);
                        if (dtInvoice1.Rows.Count > 0)
                        {
                            nlap_hdon_orginal = GetDataRowToStringDate(dtInvoice1.Rows[0], "nlap");
                            khieu_orginal = dtInvoice1.Rows[0]["khieu"].ToString().Substring(1);
                            shdon_orginal = dtInvoice1.Rows[0]["shdon"].ToString();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(dtInvoice.Rows[0]["lhdclquan"].ToString()) && !string.IsNullOrEmpty(dtInvoice.Rows[0]["khmshdclquan"].ToString())
                            && !string.IsNullOrEmpty(dtInvoice.Rows[0]["nlhdclquan"].ToString()) && !string.IsNullOrEmpty(dtInvoice.Rows[0]["khhdclquan"].ToString())
                            && !string.IsNullOrEmpty(dtInvoice.Rows[0]["shdclquan"].ToString()))
                        {
                            lhdclquan = Convert.ToInt32(dtInvoice.Rows[0]["lhdclquan"]);
                            khmshdclquan = dtInvoice.Rows[0]["khmshdclquan"].ToString();
                            nlap_hdon_orginal = GetDataRowToStringDate(dtInvoice.Rows[0], "nlhdclquan");
                            khieu_orginal = dtInvoice.Rows[0]["khhdclquan"].ToString();
                            shdon_orginal = dtInvoice.Rows[0]["shdclquan"].ToString();
                        }


                    }
                }
                string sqlKhac = $"SELECT * from #SCHEMA_NAME#.hoadon68_khac WHERE hdon_id ='{hdon_id}'";
                DataTable dtInvocieKhac = await this._minvoiceDbContext.GetDataTableAsync(sqlKhac);
                XDocument xmlInvoices =
                new XDocument(
                //new XDeclaration("1.0", Encoding.UTF8.HeaderName.ToUpper(), String.Empty),
                    new XElement("HDon",
                        new XElement("DLHDon", new XAttribute("Id", "data"),
                            new XElement("TTChung",
                                new XElement("PBan", "2.0.0"),
                                GetDataRow("THDon", tenloaihdon),
                                GetDataRow("KHMSHDon", khmshdon),
                                new XElement("KHHDon", GetDataRowToString(dtInvoice.Rows[0], "khieu").Substring(1)),
                                new XElement("SHDon", sohdon),

                                //(khmshdon != "6" && khmshdon != "6") ? new XElement("MHSo", ""):null,

                                new XElement("NLap", nlap),
                                //new XElement("HDXKhau", 0),
                                // hóa đơn bán hàng

                                khmshdon == "2" ? new XElement("HDDCKPTQuan", "true".Equals(dtInvoice.Rows[0]["hddckptquan"].ToString().ToLower()) ? 1 : 0) : null,

                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("SBKe", dtInvoice.Rows[0], "sbke") : null,

                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDate("NBKe", dtInvoice.Rows[0], "nbke") : null,

                                GetDataRow("DVTTe", dtInvoice.Rows[0], "dvtte"),
                                new XElement("TGia", dtInvoice.Rows[0]["tgia"] == null ? null : (string.IsNullOrEmpty(dtInvoice.Rows[0]["tgia"].ToString()) ? "1" : DoubleStrToString(dtInvoice.Rows[0]["tgia"].ToString()))),
                                (khmshdon != "6") ? GetDataRow("HTTToan", dtInvoice.Rows[0], "htttoan") : null,

                                new XElement("MSTTCGP", CommonConstants.MSTTCGP_TVAN),
                                //new XElement("TDLap", tdlap),
                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("MSTDVNUNLHDon", mSTDVNUNLHDon) : null,

                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("TDVNUNLHDon", tDVNUNLHDon) : null,

                               (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("DCDVNUNLHDon", dCDVNUNLHDon) : null,

                                (tthdon == "2" || tthdon == "19" || tthdon == "21" || tthdon == "23") ? new XElement("TTHDLQuan",
                                    new XElement("TCHDon", tthdon == "2" ? 1 : 2),
                                    //new XElement("KHMSHDCLQuan", khmshdon == "1" ? 1 : (khmshdon == "2" ? 2 : (khmshdon == "3" ? 3 : (khmshdon == "4" ? 4 : (khmshdon == "5" ? 5 : 6))))), // khmshdon = 5: hóa đơn khác
                                    new XElement("LHDCLQuan", lhdclquan == 3 ? lhdclquan : 1),
                                    new XElement("KHMSHDCLQuan", khmshdclquan != "" ? khmshdclquan : khmshdon == "1" ? "1" : (khmshdon == "2" ? "2" : (khmshdon == "3" ? "3" : (khmshdon == "4" ? "4" : (khmshdon == "5" ? "5" : "6"))))), // khmshdon = 5: hóa đơn khác
                                    new XElement("KHHDCLQuan", khieu_orginal),
                                    new XElement("SHDCLQuan", shdon_orginal),
                                    new XElement("NLHDCLQuan", nlap_hdon_orginal),
                                     GetDataRow("GChu", dtInvoice.Rows[0], "gchu")
                                    ) : null
                            ),
                            new XElement("NDHDon",
                                new XElement("NBan",
                                    GetDataRow("Ten", dmdvcs.Rows[0], "name"),
                                    GetDataRow("MST", dmdvcs.Rows[0], "tax_code"),
                                    (khmshdon != "6") ? GetDataRow("DChi", dmdvcs.Rows[0], "address") : GetDataRow("DChi", dtInvoice.Rows[0], "dckhoxuat"),
                                    (khmshdon != "6") ? GetDataRow("STKNHang", dmdvcs.Rows[0], "bank_account") : null,
                                    (khmshdon != "6") ? GetDataRow("TNHang", dmdvcs.Rows[0], "bank_name") : null,
                                    (khmshdon != "6") ? GetDataRow("DCTDTu", dmdvcs.Rows[0], "email") : null,
                                    (khmshdon != "6") ? GetDataRow("SDThoai", dmdvcs.Rows[0], "tel") : null,
                                    (khmshdon != "6") ? GetDataRow("Fax", dmdvcs.Rows[0], "fax") : null,
                                    //(khmshdon != "5" && khmshdon != "6") ? GetDataRow("Website", dmdvcs.Rows[0], "web") : null,

                                    khmshdon == "3" ? GetDataRow("MDVQHNSach", dtInvoice.Rows[0], "mdvqhnsach_ban") : null,

                                    khmshdon == "3" ? GetDataRow("SQDinh", dtInvoice.Rows[0], "sqdbants") : null,

                                    khmshdon == "3" ? GetDataRowDate("NQDinh", dtInvoice.Rows[0], "nqdbants") : null,

                                    khmshdon == "3" ? GetDataRow("CQBHQDinh", dtInvoice.Rows[0], "cqqdbants") : null,

                                    khmshdon == "3" ? GetDataRow("HTBan", dtInvoice.Rows[0], "htbants") : null,


                                    // PXK nội bộ
                                    khmshdon == "6" ? GetDataRow("LDDNBo", dtInvoice.Rows[0], "lddnbo") : null,

                                    (khmshdon == "6") ? GetDataRow("PTVChuyen", dtInvoice.Rows[0], "ptvchuyen") : null,

                                    // PXK hàng gửi bán đại lý
                                    khmshdon == "6" ? GetDataRow("HDKTSo", dtInvoice.Rows[0], "hdktso") : null,

                                    khmshdon == "6" ? GetDataRowDate("HDKTNgay", dtInvoice.Rows[0], "hdktngay") : null,

                                    khmshdon == "6" ? GetDataRow("TNVChuyen", dtInvoice.Rows[0], "tnvchuyen") : null,

                                    khmshdon == "6" ? GetDataRow("HVTNXHang", dtInvoice.Rows[0], "hvtnxhang") : null,

                                    khmshdon == "6" ? GetDataRow("HDSo", dtInvoice.Rows[0], "hdvc") : null

                                    //khmshdon == "3" ? new XElement("LDDNBo", dtInvoice.Rows[0]["lddnbo"].ToString()) : null,
                                    //khmshdon == "3" ? new XElement("TNVChuyen", dtInvoice.Rows[0]["tnvchuyen"].ToString()) : null,
                                    //khmshdon == "3" ? new XElement("PTVChuyen", dtInvoice.Rows[0]["ptvchuyen"].ToString()) : null
                                    ),
                                new XElement("NMua",
                                    GetDataRow("Ten", dtInvoice.Rows[0], "ten"),
                                    GetDataRow("MST", dtInvoice.Rows[0], "mst"),
                                    GetDataRow("DChi", dtInvoice.Rows[0], "dchi"),
                                    (khmshdon != "6") ? GetDataRow("TNHang", dtInvoice.Rows[0], "tnhmua") : null,

                                    (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("MKHang", dtInvoice.Rows[0], "mnmua") : null,

                                    (khmshdon != "6") ? GetDataRow("SDThoai", dtInvoice.Rows[0], "sdtnmua") : null,

                                    (khmshdon != "6") ? GetDataRow("DCTDTu", dtInvoice.Rows[0], "email") : null,

                                    (khmshdon != "6") ? GetDataRow("STKNHang", dtInvoice.Rows[0], "stknmua") : null,

                                    khmshdon == "3" ? GetDataRow("MDVQHNSach", dtInvoice.Rows[0], "mdvqhnsach_mua") : null,


                                    (khmshdon != "6") ? GetDataRow("HVTNMHang", dtInvoice.Rows[0], "tnmua") : null,
                                    (khmshdon == "6") ? GetDataRow("HVTNNHang", dtInvoice.Rows[0], "tnmua") : null,

                                    khmshdon == "3" ? GetDataRow("DDVCHDen", dtInvoice.Rows[0], "ddvchden") : null,

                                    khmshdon == "3" ? GetDataRowDate("TGVCHDTu", dtInvoice.Rows[0], "tgvchdtu") : null,

                                    khmshdon == "3" ? GetDataRowDate("TGVCHDDen", dtInvoice.Rows[0], "tgvchdden") : null,

                                    khmshdon == "4" ? GetDataRow("CMND", dtInvoice.Rows[0], "cmndmua") : null
                                ),
                                new XElement("DSHHDVu",
                                    dtInvocieDetails.AsEnumerable().Select(row =>
                                    {
                                        if (row["TChat"] != null && row["TChat"].ToString() == "2")
                                        {
                                            if (row["TSuat"] == null || string.IsNullOrEmpty(row["TSuat"].ToString())) row["TSuat"] = "0";
                                        };

                                        XElement ttKhac = new XElement("TTKhac");
                                        foreach (DataColumn item in clumDetail)
                                        {
                                            if (!notGet.Contains(item.ColumnName) && row[item.ColumnName] != null && !string.IsNullOrEmpty(row[item.ColumnName].ToString()))
                                            {
                                                ttKhac.Add(new XElement("TTin",
                                                  new XElement("TTruong", item.ColumnName),
                                                  new XElement("KDLieu", item.DataType.Name),
                                                  new XElement("DLieu", row[item.ColumnName])
                                                 )
                                               );
                                            }
                                        }

                                        return new XElement("HHDVu",
                                        GetDataRow("TChat", row, "TChat"),
                                        GetDataRow("STT", row, "STT"),
                                         GetDataRow("MHHDVu", row, "MHHDVu"),
                                        GetDataRow("THHDVu", row, "THHDVu"),
                                        GetDataRow("DVTinh", row, "DVTinh"),
                                        GetDataRowDoubble("SLuong", row, "SLuong"),
                                        GetDataRowDoubble("DGia", row, "DGia"),

                                        (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDoubble("TLCKhau", row, "TLCKhau") : null,

                                        (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDoubble("STCKhau", row, "STCKhau") : null,

                                        GetDataRowDoubble("ThTien", row, "ThTien"),

                                        (khmshdon != "2" && khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDoubble("TSuat", row, "TSuat", phantram: true) : null,
                                         ttKhac
                                        );
                                    }
                                    )

                                ),
                                 //khmshdon == "3" ? null :
                                 //khmshdon == "2" ? // hóa đơn bán hàng
                                 (khmshdon != "6") ? new XElement("TToan",
                                          // hóa đơn giá trị gia tăng, hóa đơn khác
                                          (khmshdon == "1" || khmshdon == "5") ? (
                                          new XElement("THTTLTSuat",
                                              thue
                                             )
                                         ) : null,

                                         (khmshdon != "2" && khmshdon != "3" && khmshdon != "4") ? GetDataRowDoubble("TgTCThue", dtInvoice.Rows[0], "tgtcthue") : null,
                                         (khmshdon != "2" && khmshdon != "3" && khmshdon != "4") ? GetDataRowDoubble("TgTThue", dtInvoice.Rows[0], "tgtthue") : null,

                                          dtInvociePhi.Rows.Count > 0 ? new XElement("DSLPhi",
                                             dtInvociePhi.AsEnumerable().Select(row3 => new XElement("LPhi",
                                               GetDataRowDoubble("TLPhi", row3, "TLPhi"),
                                               GetDataRowDoubble("TPhi", row3, "TPhi")
                                                 )
                                             )
                                         ) : null,
                                         // hóa đơn bán hàng dự trữ quốc gia
                                         (khmshdon != "3" && khmshdon != "4") ? GetDataRowDoubble("TTCKTMai", dtInvoice.Rows[0], "tkcktmn") : null,

                                         GetDataRowDoubble("TgTTTBSo", dtInvoice.Rows[0], "tgtttbso_last"),
                                         GetDataRowDoubble("TgTTTBChu", dtInvoice.Rows[0], "tgtttbchu")

                                     //new XElement("TgTHHDVu", dtInvoice.Rows[0]["tgtcthue"].ToString()),

                                     ) : null
                                ),
                                // điền thêm thông tin khác nếu có 
                                new XElement("TTKhac", dtInvocieKhac.AsEnumerable().Select(row =>
                                {
                                    return new XElement("TTin",
                                         new XElement("TTruong", row["ttruong"] != null ? row["ttruong"].ToString() : null),
                                         new XElement("KDLieu", row["kdlieu"] != null ? row["kdlieu"].ToString() : null),
                                         new XElement("DLieu", row["dlieu"] != null ? row["dlieu"].ToString() : null)
                                        );
                                }),

                                      giamthuebanhang20 ? new XElement("TTin",
                                         new XElement("TTruong", "GGTGTBHang20"),
                                         new XElement("KDLieu", "string"),
                                         new XElement("DLieu", thueGTGTBanHang)
                                        ) : null
                                )
                            ),
                             new XElement("DLQRCode", charDLQRCode),
                             //new XElement("TTKhac",
                             //    new XElement("HTTT", dtInvoice.Rows[0]["htttoan"].ToString()),
                             //    new XElement("MaTienTe", dtInvoice.Rows[0]["dvtte"].ToString()),
                             //    new XElement("TenNguoiMua", dtInvoice.Rows[0]["tnmua"].ToString()),
                             //    new XElement("StkNguoiMua", dtInvoice.Rows[0]["stknmua"].ToString()),
                             //    new XElement("SdtNguoiMua", dtInvoice.Rows[0]["sdtnmua"].ToString()),
                             //    new XElement("EmailNguoiMua", dtInvoice.Rows[0]["email"].ToString()),
                             //    new XElement("SoBaoMat", dtInvoice.Rows[0]["sbmat"].ToString())
                             //),
                             GetDataRow("MCCQT", dtInvoice.Rows[0], "mccqthue"),
                            new XElement("DSCKS",
                                new XElement("NBan"
                                //new XElement("Signature", dtInvoice.Rows[0]["signature"].ToString())
                                //new XElement("Signature", null)
                                ),
                                new XElement("NMua"),
                                new XElement("CCKSKhac")
                        )
                        )

                );
                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlInvoices.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }
                //string xml = string.Empty;
                //using (var hackedWriter = new SuppressEncodingStringWriter())
                //{
                //    xmlInvoices.Save(hackedWriter);
                //    xml = hackedWriter.ToString();
                //}
                _obj.Add("xml", xml);
                _obj.Add("tdlap", tdlap);
                _obj.Add("tdky", tdky);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        public async Task<JObject> XmlInvoice_BangKe(string hdon_id)
        {
            JObject _obj = new JObject();
            try
            {
                string qry = $"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code='{_webHelper.GetDvcs()}'";
                DataTable dmdvcs = await this._minvoiceDbContext.GetDataTableAsync(qry);
                //get thông tin bảng kê
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id = '{hdon_id}'";
                DataTable dtBangKe = await this._minvoiceDbContext.GetDataTableAsync(sql);

                // get thông tin hoá đơn
                sql = $"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id = '{hdon_id}'";
                DataTable dtInvoice = await this._minvoiceDbContext.GetDataTableAsync(sql);

                //get DLQRCODE
                var dinhDanhDuyNhatToanCau = GetCharQRCodeByTT("00", Guid.NewGuid().ToString().Replace("-", ""));
                var maSoThue = GetCharQRCodeByTT("01", dmdvcs.Rows[0]["tax_code"].ToString().Replace("-", ""));
                var kyHieuMauSoHoaDon = GetCharQRCodeByTT("02", dtInvoice.Rows[0]["khieu"].ToString().Substring(0, 1));
                var kyHieuHoaDon = GetCharQRCodeByTT("03", dtInvoice.Rows[0]["khieu"].ToString().Substring(1, dtInvoice.Rows[0]["khieu"].ToString().Length - 1));
                var soHoaDon = GetCharQRCodeByTT("04", dtInvoice.Rows[0]["shdon"].ToString()); //shdon
                var ngayLapHD = GetCharQRCodeByTT("05", ((DateTime)dtInvoice.Rows[0]["nlap"]).ToString("yyyyMMdd")); //nlap
                var tongTienTTBangSo = GetCharQRCodeByTT("06", DoubleStrToString(dtInvoice.Rows[0]["tgtttbso_last"].ToString()));

                var charGetThongTinHoaDon = $"{dinhDanhDuyNhatToanCau}{maSoThue}{kyHieuMauSoHoaDon}{kyHieuHoaDon}{soHoaDon}{ngayLapHD}{tongTienTTBangSo}";

                var thongTinHoaDon = GetCharQRCodeByTT("99", charGetThongTinHoaDon);
                //get charDLQRCode not has Value CRC

                // get mã CRC
                var charGetCRC = $"{thongTinHoaDon}6304";
                string valueCRC = GetCRC.CalcCRC16(Encoding.ASCII.GetBytes(charGetCRC));

                var charDLQRCode = $"{charGetCRC}{valueCRC}";

                string khhdon = dtInvoice.Rows[0]["khieu"].ToString();
                string khmshdon = khhdon.Substring(0, 1);
                string tenloaihdon = string.Empty;

                bool lddnBo = dtInvoice.Rows[0].Table.Columns.Contains("lddnbo") && !string.IsNullOrEmpty(dtInvoice.Rows[0]["lddnbo"].ToString());

                switch (Convert.ToInt32(khmshdon))
                {
                    case 1:
                        tenloaihdon = "Hóa đơn giá trị gia tăng";
                        break;
                    case 2:
                        tenloaihdon = "Hóa đơn bán hàng";
                        break;
                    case 3:
                        tenloaihdon = "Hóa đơn điện tử bán tài sản công";
                        break;
                    case 4:
                        tenloaihdon = "Hóa đơn điện tử bán hàng dự trữ quốc gia";
                        break;
                    //case 5:
                    //    tenloaihdon = "Phiếu xuất kho kiêm vận chuyển nội bộ";
                    //    break;
                    case 5:
                        tenloaihdon = "Hóa đơn khác: tem, vé, thẻ, phiếu...";
                        break;
                    case 6:
                        if (lddnBo)
                        {
                            tenloaihdon = "Phiếu xuất kho kiêm vận chuyển nội bộ";
                        }
                        else
                        {
                            tenloaihdon = "Phiếu xuất kho hàng gửi bán đại lý";
                        }
                        break;
                    //case 7:
                    //    tenloaihdon = "Hóa đơn khác: tem, vé, thẻ, phiếu...";
                    //    break;
                    default:
                        tenloaihdon = "Hóa đơn giá trị gia tăng";
                        break;
                }

                string sqlDetails = "SELECT kmai as \"TChat\", stt as \"STT\",ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                 + " sluong as \"SLuong\", dgia as \"DGia\", tlckhau as \"TLCKhau\", stckhau as \"STCKhau\","
                 + " thtien as \"ThTien\", tsuat as \"TSuat\",tsuatstr as \"TSuatStr\" "
                 + $" FROM #SCHEMA_NAME#.bangke_kemtheo_chitiet WHERE hdon_id ='{hdon_id}' order by Cast (stt as numeric)";
                /*if (khmshdon == "3")
                {
                    sqlDetails = "SELECT 1 as \"TChat\", stt as \"STT\",ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                    + " sluong as \"SLuong\", dgia as \"DGia\","
                    + " thtien as \"ThTien\""
                    + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}'";
                }
                if (khmshdon == "2")
                {
                    sqlDetails = "SELECT 1 as \"TChat\", stt as \"STT\",ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                    + " sluong as \"SLuong\", dgia as \"DGia\", tlckhau as \"TLCKhau\", stckhau as \"STCKhau\","
                    + " thtien as \"ThTien\", tsuat as \"TSuat\""
                    + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}'";
                }*/
                DataTable dtInvocieDetails = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                string sqlThue = "SELECT tsuat as \"TSuat\","
                + " SUM(Case when kmai ='3' then (thtien * (-1)) else thtien end) as \"ThTien\", SUM(Case when kmai ='3' then (tthue * (-1)) else tthue end ) as \"TThue\" "
                + $" FROM #SCHEMA_NAME#.bangke_kemtheo_chitiet WHERE hdon_id ='{hdon_id}' and (kmai ='1' or kmai ='2' or kmai ='3') GROUP BY tsuat";
                DataTable dtInvocieThue = await this._minvoiceDbContext.GetDataTableAsync(sqlThue);


                XElement thue;
                if (dtInvocieThue.Rows.Count > 0)
                {
                    thue = new XElement("THTTLTSuat",
                                            dtInvocieThue.AsEnumerable().Select(row2 => new XElement("LTSuat",
                                                GetDataRowDoubble("TSuat", row2, "TSuat", phantram: true),
                                                           GetDataRowDoubble("ThTien", row2, "ThTien"),
                                                           GetDataRowDoubble("TThue", row2, "TThue"))
                                                )
                                            );
                }
                else
                {
                    thue = new XElement("THTTLTSuat",
                                            new XElement("LTSuat",
                                                new XElement("TSuat", "0%"),
                                                new XElement("ThTien", 0),
                                                new XElement("TThue", 0)
                                                ));
                }

                string sqlPhi = "SELECT tnphi as \"TLPhi\", tienphi as \"TPhi\""
                + $" FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id ='{hdon_id}'";
                DataTable dtInvociePhi = await this._minvoiceDbContext.GetDataTableAsync(sqlPhi);

                var dataHoadon = dtInvocieDetails.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();
                var dataThue = dtInvocieThue.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();
                var dataPhi = dtInvociePhi.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string tdlap = string.Empty;
                string tdky = string.Empty;
                string nlap = string.Empty;

                if (string.IsNullOrEmpty(dtInvoice.Rows[0]["tdlap"].ToString()))
                {
                    DateTime dt = DateTime.Now;
                    tdlap = dt.ToString("yyyy-MM-dd");
                    tdky = dt.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(dtInvoice.Rows[0]["tdlap"].ToString());
                    tdlap = dt.ToString("yyyy-MM-dd");
                    tdky = dt.ToString("yyyy-MM-dd");
                }
                if (string.IsNullOrEmpty(dtInvoice.Rows[0]["nlap"].ToString()))
                {
                    DateTime dt = DateTime.Now;
                    nlap = dt.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(dtInvoice.Rows[0]["nlap"].ToString());
                    nlap = dt.ToString("yyyy-MM-dd");
                }
                DateTime nbke = DateTime.Now;
                string tthdon = dtInvoice.Rows[0]["tthdon"].ToString();
                //int sohdon = shdon > 0 ? shdon : Convert.ToInt32(string.IsNullOrEmpty(dtInvoice.Rows[0]["shdon"].ToString()) ? "0" : dtInvoice.Rows[0]["shdon"].ToString());
                int sohdon = 0;
                int sobangke = 0;
                int.TryParse(string.IsNullOrEmpty(dtInvoice.Rows[0]["shdon"].ToString()) ? "0" : dtInvoice.Rows[0]["shdon"].ToString(), out sohdon);
                int.TryParse(string.IsNullOrEmpty(dtBangKe.Rows[0]["shdon"].ToString()) ? "0" : dtBangKe.Rows[0]["shdon"].ToString(), out sobangke);

                // get thông tin đơn vị ủy nhiệm tạo hóa đơn
                string mSTDVNUNLHDon = "";
                string tDVNUNLHDon = "";
                string dCDVNUNLHDon = "";


                string sqlSelect_mau01_68_uynhiem = $"SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE khhdunhiem ='{khhdon}'";
                DataTable dtMau01_68_uynhiem = await this._minvoiceDbContext.GetDataTableAsync(sqlSelect_mau01_68_uynhiem);
                if (dtMau01_68_uynhiem.Rows.Count > 0)
                {
                    mSTDVNUNLHDon = dtMau01_68_uynhiem.Rows[0]["mstunhiem"].ToString();
                    tDVNUNLHDon = dtMau01_68_uynhiem.Rows[0]["ttcdunhiem"].ToString();
                }

                // với hóa đơn điều chỉnh, thay thế, tìm thông tin hóa đơn gốc
                string nlap_hdon_orginal = "";
                string khieu_orginal = "";
                string shdon_orginal = "";

                if (tthdon == "2" || tthdon == "19" || tthdon == "21" || tthdon == "23")
                {
                    if (dtInvoice.Rows[0].Table.Columns.Contains("hdon68_id_lk") && !string.IsNullOrEmpty(dtInvoice.Rows[0]["hdon68_id_lk"].ToString()))
                    {
                        string sql1 = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id = '{dtInvoice.Rows[0]["hdon68_id_lk"].ToString()}'";
                        DataTable dtInvoice1 = await this._minvoiceDbContext.GetDataTableAsync(sql1);
                        if (dtInvoice1.Rows.Count > 0)
                        {
                            nlap_hdon_orginal = GetDataRowToStringDate(dtInvoice1.Rows[0], "nlap");
                            khieu_orginal = dtInvoice1.Rows[0]["khieu"].ToString().Substring(1);
                            shdon_orginal = dtInvoice1.Rows[0]["shdon"].ToString();
                        }
                    }
                }
                string sqlKhac = $"SELECT * from #SCHEMA_NAME#.hoadon68_khac WHERE hdon_id ='{hdon_id}'";
                DataTable dtInvocieKhac = await this._minvoiceDbContext.GetDataTableAsync(sqlKhac);
                XDocument xmlInvoices =
                new XDocument(
                //new XDeclaration("1.0", Encoding.UTF8.HeaderName.ToUpper(), String.Empty),
                    new XElement("BKe",
                        new XElement("DLBKe", new XAttribute("Id", "data"),
                            new XElement("TTChung",
                                new XElement("PBan", "2.0.0"),
                                GetDataRow("TBKe", "BẢNG KÊ DANH SÁCH HÀNG HÓA DỊCH VỤ"),
                                //new XElement("SBKe", sobangke),
                                //new XElement("NBKe", nlap),
                                GetDataRow("KHMSHDon", khmshdon),
                                new XElement("KHHDon", GetDataRowToString(dtInvoice.Rows[0], "khieu").Substring(1)),
                                new XElement("SHDon", sohdon),

                                //(khmshdon != "6" && khmshdon != "6") ? new XElement("MHSo", ""):null,

                                new XElement("NLap", nlap),
                                //new XElement("HDXKhau", 0),
                                // hóa đơn bán hàng

                                khmshdon == "2" ? new XElement("HDDCKPTQuan", "true".Equals(dtInvoice.Rows[0]["hddckptquan"].ToString().ToLower()) ? 1 : 0) : null,

                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("SBKe", dtInvoice.Rows[0], "sbke") : null,

                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDate("NBKe", dtInvoice.Rows[0], "nbke") : null,

                                GetDataRow("DVTTe", dtInvoice.Rows[0], "dvtte"),
                                new XElement("TGia", dtInvoice.Rows[0]["tgia"] == null ? null : (string.IsNullOrEmpty(dtInvoice.Rows[0]["tgia"].ToString()) ? "1" : DoubleStrToString(dtInvoice.Rows[0]["tgia"].ToString()))),
                                (khmshdon != "6") ? GetDataRow("HTTToan", dtInvoice.Rows[0], "htttoan") : null,

                                new XElement("MSTTCGP", CommonConstants.MSTTCGP_TVAN),
                                //new XElement("TDLap", tdlap),
                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("MSTDVNUNLHDon", mSTDVNUNLHDon) : null,

                                (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("TDVNUNLHDon", tDVNUNLHDon) : null,

                               (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("DCDVNUNLHDon", dCDVNUNLHDon) : null,

                                (tthdon == "2" || tthdon == "19" || tthdon == "21" || tthdon == "23") ? new XElement("TTHDLQuan",
                                    new XElement("TCHDon", tthdon == "2" ? 1 : 2),
                                    new XElement("LHDCLQuan", 1),
                                    new XElement("KHMSHDCLQuan", khmshdon == "1" ? 1 : (khmshdon == "2" ? 2 : (khmshdon == "3" ? 3 : (khmshdon == "4" ? 4 : (khmshdon == "5" ? 5 : 6))))), // khmshdon = 5: hóa đơn khác
                                    new XElement("KHHDCLQuan", khieu_orginal),
                                    new XElement("SHDCLQuan", shdon_orginal),
                                    new XElement("NLHDCLQuan", nlap_hdon_orginal),
                                     GetDataRow("GChu", dtInvoice.Rows[0], "gchu")
                                    ) : null
                            ),
                            new XElement("NDBKe",
                                new XElement("NBan",
                                    GetDataRow("Ten", dmdvcs.Rows[0], "name"),
                                    GetDataRow("MST", dmdvcs.Rows[0], "tax_code"),
                                    (khmshdon != "6") ? GetDataRow("DChi", dmdvcs.Rows[0], "address") : GetDataRow("DChi", dtInvoice.Rows[0], "dckhoxuat"),
                                    (khmshdon != "6") ? GetDataRow("SDThoai", dtInvoice.Rows[0], "sdtban") : null,
                                    (khmshdon != "6") ? GetDataRow("DCTDTu", dmdvcs.Rows[0], "email") : null,
                                    (khmshdon != "6") ? GetDataRow("STKNHang", dmdvcs.Rows[0], "bank_account") : null,
                                    (khmshdon != "6") ? GetDataRow("TNHang", dmdvcs.Rows[0], "bank_name") : null,
                                    (khmshdon != "6") ? GetDataRow("Fax", dmdvcs.Rows[0], "fax") : null,
                                    //(khmshdon != "5" && khmshdon != "6") ? GetDataRow("Website", dmdvcs.Rows[0], "web") : null,

                                    khmshdon == "3" ? GetDataRow("MDVQHNSach", dtInvoice.Rows[0], "mdvqhnsach_ban") : null,

                                    khmshdon == "3" ? GetDataRow("SQDinh", dtInvoice.Rows[0], "sqdbants") : null,

                                    khmshdon == "3" ? GetDataRowDate("NQDinh", dtInvoice.Rows[0], "nqdbants") : null,

                                    khmshdon == "3" ? GetDataRow("CQBHQDinh", dtInvoice.Rows[0], "cqqdbants") : null,

                                    khmshdon == "3" ? GetDataRow("HTBan", dtInvoice.Rows[0], "htbants") : null,


                                    // PXK nội bộ
                                    khmshdon == "6" ? GetDataRow("LDDNBo", dtInvoice.Rows[0], "lddnbo") : null,

                                    (khmshdon == "6") ? GetDataRow("PTVChuyen", dtInvoice.Rows[0], "ptvchuyen") : null,

                                    // PXK hàng gửi bán đại lý
                                    khmshdon == "6" ? GetDataRow("HDKTSo", dtInvoice.Rows[0], "hdktso") : null,

                                    khmshdon == "6" ? GetDataRowDate("HDKTNgay", dtInvoice.Rows[0], "hdktngay") : null,

                                    khmshdon == "6" ? GetDataRow("TNVChuyen", dtInvoice.Rows[0], "tnvchuyen") : null,

                                    khmshdon == "6" ? GetDataRow("HVTNXHang", dtInvoice.Rows[0], "hvtnxhang") : null,

                                    khmshdon == "6" ? GetDataRow("HDSo", dtInvoice.Rows[0], "hdvc") : null

                                    //khmshdon == "3" ? new XElement("LDDNBo", dtInvoice.Rows[0]["lddnbo"].ToString()) : null,
                                    //khmshdon == "3" ? new XElement("TNVChuyen", dtInvoice.Rows[0]["tnvchuyen"].ToString()) : null,
                                    //khmshdon == "3" ? new XElement("PTVChuyen", dtInvoice.Rows[0]["ptvchuyen"].ToString()) : null
                                    ),
                                new XElement("NMua",
                                    GetDataRow("Ten", dtInvoice.Rows[0], "ten"),
                                    GetDataRow("MST", dtInvoice.Rows[0], "mst"),
                                    GetDataRow("DChi", dtInvoice.Rows[0], "dchi"),
                                    (khmshdon != "6") ? GetDataRow("TNHang", dtInvoice.Rows[0], "tnhmua") : null,

                                    (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRow("MKHang", dtInvoice.Rows[0], "mnmua") : null,

                                    (khmshdon != "6") ? GetDataRow("SDThoai", dtInvoice.Rows[0], "sdtnmua") : null,

                                    (khmshdon != "6") ? GetDataRow("DCTDTu", dtInvoice.Rows[0], "email") : null,

                                    (khmshdon != "6") ? GetDataRow("STKNHang", dtInvoice.Rows[0], "stknmua") : null,

                                    khmshdon == "3" ? GetDataRow("MDVQHNSach", dtInvoice.Rows[0], "mdvqhnsach_mua") : null,


                                    (khmshdon != "6") ? GetDataRow("HVTNMHang", dtInvoice.Rows[0], "tnmua") : null,
                                    (khmshdon == "6") ? GetDataRow("HVTNNHang", dtInvoice.Rows[0], "tnmua") : null,

                                    khmshdon == "3" ? GetDataRow("DDVCHDen", dtInvoice.Rows[0], "ddvchden") : null,

                                    khmshdon == "3" ? GetDataRowDate("TGVCHDTu", dtInvoice.Rows[0], "tgvchdtu") : null,

                                    khmshdon == "3" ? GetDataRowDate("TGVCHDDen", dtInvoice.Rows[0], "tgvchdden") : null,

                                    khmshdon == "4" ? GetDataRow("CMND", dtInvoice.Rows[0], "cmndmua") : null
                                ),
                                new XElement("DSHHDVu",
                                    dtInvocieDetails.AsEnumerable().Select(row =>
                                    {
                                        if (row["TChat"] != null && row["TChat"].ToString() == "2")
                                        {
                                            if (row["TSuat"] == null || string.IsNullOrEmpty(row["TSuat"].ToString())) row["TSuat"] = "0";
                                        };
                                        return new XElement("HHDVu",
                                        GetDataRow("TChat", row, "TChat"),
                                        GetDataRow("STT", row, "STT"),
                                         GetDataRow("MHHDVu", row, "MHHDVu"),
                                        GetDataRow("THHDVu", row, "THHDVu"),
                                        GetDataRow("DVTinh", row, "DVTinh"),
                                        GetDataRowDoubble("SLuong", row, "SLuong"),
                                        GetDataRowDoubble("DGia", row, "DGia"),

                                        (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDoubble("TLCKhau", row, "TLCKhau") : null,

                                        (khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDoubble("STCKhau", row, "STCKhau") : null,

                                        GetDataRowDoubble("ThTien", row, "ThTien"),

                                        (khmshdon != "2" && khmshdon != "3" && khmshdon != "4" && khmshdon != "6") ? GetDataRowDoubble("TSuat", row, "TSuat", phantram: true) : null
                                        /* (khmshdon == "1" || khmshdon == "5") ? new XElement("TTKhac",
                                               new XElement("TTin",
                                                  new XElement("TTruong", "tgtien"),
                                                  new XElement("KDLieu", "decimal"),
                                                  new XElement("DLieu", row["tgtien"])
                                                 ),
                                               new XElement("TTin",
                                                  new XElement("TTruong", "tthue"),
                                                  new XElement("KDLieu", "decimal"),
                                                  new XElement("DLieu", row["tthue"])
                                                 )
                                         ) :null*/
                                        //dt.Columns.Cast<DataColumn>().Select(col => new XElement(col.ColumnName,row[col.ColumnName])).ToString()
                                        );
                                    }
                                    )

                                ),
                                 //khmshdon == "3" ? null :
                                 //khmshdon == "2" ? // hóa đơn bán hàng
                                 (khmshdon != "6") ? new XElement("TToan",
                                          // hóa đơn giá trị gia tăng, hóa đơn khác
                                          (khmshdon == "1" || khmshdon == "5") ? (
                                          new XElement("THTTLTSuat",
                                              thue
                                             )
                                         ) : null,

                                         (khmshdon != "2" && khmshdon != "3" && khmshdon != "4") ? GetDataRowDoubble("TgTCThue", dtInvoice.Rows[0], "tgtcthue") : null,
                                         (khmshdon != "2" && khmshdon != "3" && khmshdon != "4") ? GetDataRowDoubble("TgTThue", dtInvoice.Rows[0], "tgtthue") : null,

                                          dtInvociePhi.Rows.Count > 0 ? new XElement("DSLPhi",
                                             dtInvociePhi.AsEnumerable().Select(row3 => new XElement("LPhi",
                                               GetDataRowDoubble("TLPhi", row3, "TLPhi"),
                                               GetDataRowDoubble("TPhi", row3, "TPhi")
                                                 )
                                             )
                                         ) : null,
                                         // hóa đơn bán hàng dự trữ quốc gia
                                         (khmshdon != "3" && khmshdon != "4") ? GetDataRowDoubble("TTCKTMai", dtInvoice.Rows[0], "tkcktmn") : null,

                                         GetDataRowDoubble("TgTTTBSo", dtInvoice.Rows[0], "tgtttbso_last"),
                                         GetDataRowDoubble("TgTTTBChu", dtInvoice.Rows[0], "tgtttbchu")

                                     //new XElement("TgTHHDVu", dtInvoice.Rows[0]["tgtcthue"].ToString()),

                                     ) : null
                                ),
                                // điền thêm thông tin khác nếu có 
                                new XElement("TTKhac", dtInvocieKhac.AsEnumerable().Select(row =>
                                {
                                    return new XElement("TTin",
                                         new XElement("TTruong", row["ttruong"] != null ? row["ttruong"].ToString() : null),
                                         new XElement("KDLieu", row["kdlieu"] != null ? row["kdlieu"].ToString() : null),
                                         new XElement("DLieu", row["dlieu"] != null ? row["dlieu"].ToString() : null)
                                        );
                                })
                                )
                            ),
                             new XElement("DLQRCode", charDLQRCode),
                             //new XElement("TTKhac",
                             //    new XElement("HTTT", dtInvoice.Rows[0]["htttoan"].ToString()),
                             //    new XElement("MaTienTe", dtInvoice.Rows[0]["dvtte"].ToString()),
                             //    new XElement("TenNguoiMua", dtInvoice.Rows[0]["tnmua"].ToString()),
                             //    new XElement("StkNguoiMua", dtInvoice.Rows[0]["stknmua"].ToString()),
                             //    new XElement("SdtNguoiMua", dtInvoice.Rows[0]["sdtnmua"].ToString()),
                             //    new XElement("EmailNguoiMua", dtInvoice.Rows[0]["email"].ToString()),
                             //    new XElement("SoBaoMat", dtInvoice.Rows[0]["sbmat"].ToString())
                             //),
                             GetDataRow("MCCQT", dtInvoice.Rows[0], "mccqthue"),
                            new XElement("DSCKS",
                                new XElement("NBan"
                                //new XElement("Signature", dtInvoice.Rows[0]["signature"].ToString())
                                //new XElement("Signature", null)
                                ),
                                new XElement("NMua"),
                                new XElement("CCKSKhac")
                        )
                        )

                );
                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlInvoices.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }
                //string xml = string.Empty;
                //using (var hackedWriter = new SuppressEncodingStringWriter())
                //{
                //    xmlInvoices.Save(hackedWriter);
                //    xml = hackedWriter.ToString();
                //}
                _obj.Add("xml", xml);
                _obj.Add("tdlap", tdlap);
                _obj.Add("tdky", tdky);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        #region hatm Lay giá trị để tạo xelement: nếu null hoặc "" thì không hiển thị trong file xml
        public string DoubleStrToString(string strdec)
        {
            try
            {
                if (strdec.Contains("."))
                {
                    string result = strdec.TrimEnd('0').TrimEnd('.');
                    if (result.Length > 0 && result[result.Length - 1] == '.') return result.Substring(0, result.Length - 1);
                    return strdec.Contains(".") ? strdec.TrimEnd('0').TrimEnd('.') : strdec;
                }
                else
                {
                    return strdec;
                }
            }
            catch
            {
                return strdec;
            }
        }
        public string GetDataRowToString(DataRow row, string field)
        {
            if (row.Table.Columns.Contains(field))
            {
                return row[field].ToString();
            }
            else
            {
                return null;
            }
        }
        public XElement GetDataRow(string elmName, DataRow row, string field)
        {
            if (row.Table.Columns.Contains(field) && !string.IsNullOrEmpty(row[field].ToString()))
            {
                return new XElement(elmName, row[field].ToString());
            }
            else
            {
                return null;
            }
        }
        public XElement GetDataRow(string elmName, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return new XElement(elmName, value);
            }
            else
            {
                return null;
            }
        }
        public XElement GetDataRowDoubble(string elmName, DataRow row, string field, bool phantram = false)
        {
            if (row.Table.Columns.Contains(field) && !string.IsNullOrEmpty(row[field].ToString()))
            {

                if (!phantram) return new XElement(elmName, DoubleStrToString(row[field].ToString()));
                else
                {
                    string sql = $"select name from #SCHEMA_NAME#.pl_vat_type where vat_percentage = {row[field].ToString()}  limit 1";
                    DataTable data = this._minvoiceDbContext.GetDataTable(sql);
                    if (data.Rows.Count > 0)
                    {
                        if (new string[] { "0%", "5%", "10%", "KKKNT", "KCT", "8%" }.Contains(data.Rows[0]["name"].ToString()))
                            return new XElement(elmName, data.Rows[0]["name"].ToString());
                        else
                        {
                            return new XElement(elmName, $"KHAC:{DoubleStrToString(row[field].ToString())}%");
                        }
                    }
                    else
                        return new XElement(elmName, null);
                }
            }
            else
            {
                if (!phantram) return null;
                else return new XElement(elmName, null);
            }
        }

        public XElement GetDataRowDate(string elmName, DataRow row, string field)
        {
            if (row.Table.Columns.Contains(field) && !string.IsNullOrEmpty(row[field].ToString()))
            {
                try
                {
                    return new XElement(elmName, Convert.ToDateTime(row[field].ToString()).ToString("yyyy-MM-dd"));
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public XElement GetDataRowDateTime(string elmName, DataRow row, string field)
        {
            if (row.Table.Columns.Contains(field) && !string.IsNullOrEmpty(row[field].ToString()))
            {
                try
                {
                    return new XElement(elmName, Convert.ToDateTime(row[field].ToString()).ToString("yyyy-MM-ddTHH:mm:ss"));
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public string GetDataRowToStringDate(DataRow row, string field)
        {
            if (row.Table.Columns.Contains(field) && !string.IsNullOrEmpty(row[field].ToString()))
            {
                try
                {
                    return Convert.ToDateTime(row[field].ToString()).ToString("yyyy-MM-dd");
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        #endregion
        public async Task<XElement> XmlInvoiceNew(string hdon_id)
        {
            JObject _obj = new JObject();

            XElement xmlInvoices = null;
            try
            {
                string qry = $"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code='{_webHelper.GetDvcs()}'";
                DataTable dmdvcs = await this._minvoiceDbContext.GetDataTableAsync(qry);
                string sql = $"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id = '{hdon_id}'";
                DataTable dtInvoice = await this._minvoiceDbContext.GetDataTableAsync(sql);

                string khhdon = dtInvoice.Rows[0]["khieu"].ToString();
                string khmshdon = khhdon.Substring(0, 1);
                string tenloaihdon = string.Empty;
                switch (Convert.ToInt32(khmshdon))
                {
                    case 1:
                        tenloaihdon = "Hóa đơn giá trị gia tăng";
                        break;
                    case 2:
                        tenloaihdon = "Hóa đơn bán hàng";
                        break;
                    case 3:
                        tenloaihdon = "Hóa đơn điện tử bán tài sản công";
                        break;
                    case 4:
                        tenloaihdon = "Hóa đơn điện tử bán hàng dự trữ quốc gia";
                        break;
                    case 5:
                        tenloaihdon = "Phiếu xuất kho kiêm vận chuyển nội bộ";
                        break;
                    case 6:
                        tenloaihdon = "Phiếu xuất kho hàng gửi bán đại lý";
                        break;
                    case 7:
                        tenloaihdon = "Hóa đơn khác: tem, vé, thẻ, phiếu...";
                        break;
                    default:
                        tenloaihdon = "Hóa đơn giá trị gia tăng";
                        break;
                }


                bool giamthuebanhang20 = false;
                string thueGTGTBanHang = "";
                try
                {
                    giamthuebanhang20 = dtInvoice.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(dtInvoice.Rows[0]["giamthuebanhang20"].ToString());
                    string dvtte = dtInvoice.Rows[0]["dvtte"]?.ToString();

                    CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                    string tongTienGiam = double.Parse(DoubleStrToString(dtInvoice.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                    tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");
                    thueGTGTBanHang = "Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15";

                }
                catch (Exception ex)
                {
                    //Log.Error(ex);
                }

                string sqlDetails = "SELECT 1 as \"TChat\", stt as \"STT\", ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                + " sluong as \"SLuong\", dgia as \"DGia\", tlckhau as \"TLCKhau\", stckhau as \"STCKhau\","
                + " thtien as \"ThTien\", tsuat as \"TSuat\""
                + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}'";
                if (khmshdon == "3")
                {
                    sqlDetails = "SELECT 1 as \"TChat\", stt as \"STT\", ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                    + " sluong as \"SLuong\", dgia as \"DGia\","
                    + " thtien as \"ThTien\""
                    + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}'";
                }
                if (khmshdon == "2")
                {
                    sqlDetails = "SELECT 1 as \"TChat\", stt as \"STT\", ma as \"MHHDVu\", ten as \"THHDVu\", mdvtinh as \"DVTinh\","
                    + " sluong as \"SLuong\", dgia as \"DGia\", tlckhau as \"TLCKhau\", stckhau as \"STCKhau\","
                    + " thtien as \"ThTien\", tsuat as \"TSuat\""
                    + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}'";
                }
                DataTable dtInvocieDetails = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                string sqlThue = "SELECT SUM(tthue) as \"TThue\","
                + " SUM(thtien) as \"ThTien\", tsuat as \"TSuat\""
                + $" FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' GROUP BY tsuat";
                DataTable dtInvocieThue = await this._minvoiceDbContext.GetDataTableAsync(sqlThue);


                string sqlPhi = "SELECT tnphi as \"TLPhi\", tienphi as \"TPhi\""
                + $" FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id ='{hdon_id}'";
                DataTable dtInvociePhi = await this._minvoiceDbContext.GetDataTableAsync(sqlPhi);

                var dataHoadon = dtInvocieDetails.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();
                var dataThue = dtInvocieThue.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();
                var dataPhi = dtInvociePhi.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "stt").ToList();

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string tdlap = string.Empty;
                string tdky = string.Empty;
                string nlap = string.Empty;

                if (string.IsNullOrEmpty(dtInvoice.Rows[0]["tdlap"].ToString()))
                {
                    DateTime dt = DateTime.Now;
                    tdlap = dt.ToString("yyyy-MM-dd");
                    tdky = dt.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(dtInvoice.Rows[0]["tdlap"].ToString());
                    tdlap = dt.ToString("yyyy-MM-dd");
                    tdky = dt.ToString("yyyy-MM-dd");
                }
                if (string.IsNullOrEmpty(dtInvoice.Rows[0]["nlap"].ToString()))
                {
                    DateTime dt = DateTime.Now;
                    nlap = dt.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(dtInvoice.Rows[0]["nlap"].ToString());
                    nlap = dt.ToString("yyyy-MM-dd");
                }
                DateTime nbke = DateTime.Now;
                string tthdon = dtInvoice.Rows[0]["tthdon"].ToString();

                //int sohdon = shdon > 0 ? shdon : Convert.ToInt32(string.IsNullOrEmpty(dtInvoice.Rows[0]["shdon"].ToString()) ? "0" : dtInvoice.Rows[0]["shdon"].ToString());
                string sohdon = string.IsNullOrEmpty(dtInvoice.Rows[0]["shdon"].ToString()) ? "0" : dtInvoice.Rows[0]["shdon"].ToString();

                // get thông tin đơn vị ủy nhiệm tạo hóa đơn
                string mSTDVNUNLHDon = "";
                string tDVNUNLHDon = "";
                string dCDVNUNLHDon = "";

                string sqlSelect_mau01_68_uynhiem = $"SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE khhdunhiem ='{khhdon}'";
                DataTable dtMau01_68_uynhiem = await this._minvoiceDbContext.GetDataTableAsync(sqlSelect_mau01_68_uynhiem);
                if (dtMau01_68_uynhiem.Rows.Count > 0)
                {
                    mSTDVNUNLHDon = dtMau01_68_uynhiem.Rows[0]["mstunhiem"].ToString();
                    tDVNUNLHDon = dtMau01_68_uynhiem.Rows[0]["ttcdunhiem"].ToString();
                }

                // với hóa đơn điều chỉnh, thay thế, tìm thông tin hóa đơn gốc
                string nlap_hdon_orginal = "";
                string khieu_orginal = "";
                string shdon_orginal = "";

                if (tthdon == "2" || tthdon == "19" || tthdon == "21" || tthdon == "23")
                {
                    string sql1 = $"SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id = '{hdon_id}'";
                    DataTable dtInvoice1 = await this._minvoiceDbContext.GetDataTableAsync(sql1);
                    if (dtInvoice1.Rows.Count > 0)
                    {
                        nlap_hdon_orginal = dtInvoice1.Rows[0]["nlap"].ToString();
                        khieu_orginal = dtInvoice1.Rows[0]["khieu"].ToString();
                        shdon_orginal = dtInvoice1.Rows[0]["shdon"].ToString();
                    }
                }

                xmlInvoices =
                    //new XDeclaration("1.0", Encoding.UTF8.HeaderName.ToUpper(), String.Empty),
                    new XElement("HDon",
                        new XElement("DLHDon", new XAttribute("Id", "data"),
                            new XElement("TTChung",
                                new XElement("PBan", "2.0.0"),
                                new XElement("THDon", tenloaihdon),
                                new XElement("KHMSHDon", khmshdon),
                                new XElement("KHHDon", dtInvoice.Rows[0]["khieu"].ToString().Substring(1)),
                                new XElement("SHDon", sohdon),
                                (khmshdon != "4" && khmshdon != "6" && khmshdon != "6") ? new XElement("MHSo", "") : null,
                                new XElement("NLap", nlap),
                                //new XElement("HDXKhau", 0),
                                // hóa đơn bán hàng
                                khmshdon == "2" ? new XElement("HDDCKPTQuan", "true".Equals(dtInvoice.Rows[0]["hddckptquan"].ToString().ToLower()) ? 1 : 0) : null,
                                new XElement("SBKe", ""),
                                new XElement("NBKe", nbke),
                                //new XElement("TDLap", tdlap),
                                GetDataRow("DVTTe", dtInvoice.Rows[0], "dvtte"),
                                new XElement("TGia", string.IsNullOrEmpty(dtInvoice.Rows[0]["tgia"].ToString()) ? "1" : DoubleStrToString(dtInvoice.Rows[0]["tgia"].ToString())),
                                (khmshdon == "1" || khmshdon == "2" || khmshdon == "5") ? new XElement("MSTDVNUNLHDon", mSTDVNUNLHDon) : null,
                                (khmshdon == "1" || khmshdon == "2" || khmshdon == "5") ? new XElement("TDVNUNLHDon", tDVNUNLHDon) : null,
                                (khmshdon == "1" || khmshdon == "2" || khmshdon == "5") ? new XElement("DCDVNUNLHDon", dCDVNUNLHDon) : null,
                                (tthdon == "2" || tthdon == "19" || tthdon == "21" || tthdon == "23") ? new XElement("TTHDLQuan",
                                    new XElement("TCHDon", tthdon == "2" ? 1 : 2),
                                    new XElement("LHDCLQuan", 1),
                                    new XElement("KHMSHDCLQuan", khmshdon == "1" ? 1 : (khmshdon == "2" ? 2 : (khmshdon == "3" ? 3 : (khmshdon == "4" ? 4 : (khmshdon == "5" ? 5 : 6))))),
                                    new XElement("KHHDCLQuan", khieu_orginal),
                                    new XElement("SHDCLQuan", shdon_orginal),
                                    new XElement("NLHDCLQuan", nlap_hdon_orginal)
                                    ) : null
                            ),
                            new XElement("NDHDon",
                                new XElement("NBan",
                                    GetDataRow("Ten", dtInvoice.Rows[0], "name"),
                                    GetDataRow("MST", dtInvoice.Rows[0], "tax_code"),
                                    khmshdon == "3" ? GetDataRow("MDVQHNSach", dtInvoice.Rows[0], "mdvqhnsach_ban") : null,
                                    new XElement("DChi", khmshdon == "3" ? dtInvoice.Rows[0]["dckhoxuat"].ToString() : dmdvcs.Rows[0]["address"].ToString()),
                                    // PXK nội bộ
                                    khmshdon == "6" ? new XElement("LDDNBo", dtInvoice.Rows[0]["lddnbo"].ToString()) : null,
                                    (khmshdon == "6" || khmshdon == "6") ? new XElement("PTVChuyen", dtInvoice.Rows[0]["ptvchuyen"].ToString()) : null,
                                    // PXK hàng gửi bán đại lý
                                    khmshdon == "6" ? new XElement("HDKTSo", dtInvoice.Rows[0]["hdktso"].ToString()) : null,
                                    khmshdon == "6" ? new XElement("HDKTNgay", dtInvoice.Rows[0]["hdktngay"].ToString()) : null,
                                    khmshdon == "6" ? new XElement("TNVChuyen", dtInvoice.Rows[0]["tnvchuyen"].ToString()) : null

                                    //khmshdon == "3" ? new XElement("LDDNBo", dtInvoice.Rows[0]["lddnbo"].ToString()) : null,
                                    //khmshdon == "3" ? new XElement("TNVChuyen", dtInvoice.Rows[0]["tnvchuyen"].ToString()) : null,
                                    //khmshdon == "3" ? new XElement("PTVChuyen", dtInvoice.Rows[0]["ptvchuyen"].ToString()) : null
                                    ),
                                new XElement("NMua",
                                    new XElement("Ten", dtInvoice.Rows[0]["ten"].ToString()),
                                    new XElement("MST", dtInvoice.Rows[0]["mst"].ToString()),
                                    khmshdon == "3" ? new XElement("MDVQHNSach", dtInvoice.Rows[0]["mdvqhnsach_mua"].ToString()) : null,
                                    new XElement("DChi", khmshdon == "3" ? dtInvoice.Rows[0]["dckhonhap"].ToString() : dtInvoice.Rows[0]["dchi"].ToString()),
                                    new XElement("HVTNMHang", dtInvoice.Rows[0]["tnmua"].ToString()),
                                    khmshdon == "3" ? new XElement("DDVCHDen", dtInvoice.Rows[0]["ddvchden"].ToString()) : null,
                                    khmshdon == "3" ? new XElement("TGVCHDTu", dtInvoice.Rows[0]["tgvchdtu"].ToString()) : null,
                                    khmshdon == "3" ? new XElement("TGVCHDDen", dtInvoice.Rows[0]["tgvchdden"].ToString()) : null
                                ),
                                new XElement("DSHHDVu",
                                    dtInvocieDetails.AsEnumerable().Select(row => new XElement("HHDVu",
                                        dataHoadon.Select(x => new XElement(x, row[x]))
                                        //dt.Columns.Cast<DataColumn>().Select(col => new XElement(col.ColumnName,row[col.ColumnName])).ToString()
                                        )
                                    )
                                ),
                                //khmshdon == "3" ? null :
                                //khmshdon == "2" ? // hóa đơn bán hàng
                                new XElement("TToan",
                                         // hóa đơn giá trị gia tăng, hóa đơn khác
                                         (khmshdon == "1" || khmshdon == "5") ? (
                                         new XElement("THTTLTSuat",
                                            dtInvocieThue.AsEnumerable().Select(row => new XElement("LTSuat",
                                                dataThue.Select(x => new XElement(x, row[x]))
                                                //dt.Columns.Cast<DataColumn>().Select(col => new XElement(col.ColumnName,row[col.ColumnName])).ToString()
                                                )
                                            )
                                        )) : null,

                                        new XElement("TgTCThue", dtInvoice.Rows[0]["tgtcthue"].ToString()),
                                        new XElement("TgTThue", dtInvoice.Rows[0]["tgtthue"].ToString()),
                                        new XElement("DSLPhi",
                                            dtInvociePhi.AsEnumerable().Select(row => new XElement("LPhi",
                                                dataThue.Select(x => new XElement(x, row[x]))
                                                )
                                            )
                                        ),
                                        // hóa đơn bán hàng dự trữ quốc gia
                                        (khmshdon != "3" || khmshdon != "4") ? new XElement("TTCKTMai", dtInvoice.Rows[0]["tkcktmn"].ToString()) : null,
                                        new XElement("TgTTTBSo", dtInvoice.Rows[0]["tgtttbso_last"].ToString()),
                                        new XElement("TgTTTBChu", dtInvoice.Rows[0]["tgtttbchu"].ToString())

                                    //new XElement("TgTHHDVu", dtInvoice.Rows[0]["tgtcthue"].ToString()),

                                    )

                                // điền thêm thông tin khác nếu có 

                                ),
                                new XElement("TTKhac", giamthuebanhang20 ? new XElement("TTin",
                                         new XElement("TTruong", "GGTGTBHang20"),
                                         new XElement("KDLieu", "string"),
                                         new XElement("DLieu", thueGTGTBanHang)
                                        ) : null)
                            ),
                            new XElement("DLQRCode"),
                            //new XElement("TTKhac",
                            //    new XElement("HTTT", dtInvoice.Rows[0]["htttoan"].ToString()),
                            //    new XElement("MaTienTe", dtInvoice.Rows[0]["dvtte"].ToString()),
                            //    new XElement("TenNguoiMua", dtInvoice.Rows[0]["tnmua"].ToString()),
                            //    new XElement("StkNguoiMua", dtInvoice.Rows[0]["stknmua"].ToString()),
                            //    new XElement("SdtNguoiMua", dtInvoice.Rows[0]["sdtnmua"].ToString()),
                            //    new XElement("EmailNguoiMua", dtInvoice.Rows[0]["email"].ToString()),
                            //    new XElement("SoBaoMat", dtInvoice.Rows[0]["sbmat"].ToString())
                            //),
                            new XElement("MCCQT", dtInvoice.Rows[0]["mccqthue"] != null && dtInvoice.Rows[0]["mccqthue"].ToString() != "" ? dtInvoice.Rows[0]["mccqthue"].ToString() : "000000000000"),
                            new XElement("DSCKS",
                                new XElement("NBan"
                                //new XElement("Signature", dtInvoice.Rows[0]["signature"].ToString())
                                ),
                                new XElement("NMua"),
                                new XElement("CCKSKhac")
                            )
                );

            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return xmlInvoices;
        }

        public async Task<JObject> SignInvoiceCertFile68ForHasCodeAndNotHaveAnyCode(string folder, JObject obj)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                JArray arrData = (JArray)obj["data"];

                JObject model = (JObject)arrData[0];

                //string cmd_type = model["checkHasCode"].ToString() == "true" ? "200" : "203";

                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                //Guid hdon_id = Guid.Parse(model["hdon_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }


                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                //get arr hdon_id từ obj để for
                JArray arrhdon_id = (JArray)model["lsthdon_id"];

                JArray arrayMail = new JArray();

                for (int i = 0; i < arrhdon_id.Count; i++)
                {
                    try
                    {
                        JObject element = (JObject)arrhdon_id[i];
                        var hdonid = element["hdon_id"].ToString();
                        string cmd_type = element["is_hdcma"].ToString() == "1" ? "200" : (element["is_hdcma"].ToString() == "0" ? "203" : "");

                        if (cmd_type != "200" && cmd_type != "203")
                        {
                            continue;
                        }

                        Guid hdon_id = Guid.Parse(hdonid);

                        #region hatm CheckQuyTrinhHoaDon
                        Dictionary<string, object> parametersN = new Dictionary<string, object>();
                        parametersN.Add("hdon_id", hdon_id);

                        DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon,cctbao_id,mdvi FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parametersN);
                        string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                        parametersN.Clear();
                        parametersN.Add("hdon_id", hdon_id);
                        parametersN.Add("nlap", DateTime.Parse((invoice_issued_date)));
                        parametersN.Add("cctbao_id", Guid.Parse(tblInvoiceAuth.Rows[0]["cctbao_id"].ToString()));
                        parametersN.Add("mdvi", tblInvoiceAuth.Rows[0]["mdvi"].ToString());

                        sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='Chờ ký' And cctbao_id =@cctbao_id And mdvi=@mdvi AND nlap< @nlap LIMIT 1";
                        DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parametersN);

                        if (tblInvoiceAuthBefore.Rows.Count > 0)
                        {
                            error = "Bạn chưa xử lý hóa đơn của ngày hôm trước";
                            break;
                        }

                        #endregion

                        await _minvoiceDbContext.BeginTransactionAsync();
                        #region lấy số hóa đơn khi không có
                        if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                        {
                            sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                            JObject masterObj = new JObject();
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                            masterObj.Add("hdon_id", hdon_id);

                            await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, CommandType.StoredProcedure, masterObj);
                        }
                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                        #endregion

                        await _minvoiceDbContext.BeginTransactionAsync();
                        // lấy thông tin xml dữ liệu    
                        //XElement xml_DuLieu = await XmlInvoiceNew(model["hdon_id"].ToString());

                        #region Xử lý hoá đơn theo hdon_id
                        DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hdon_id + "'");

                        string shdon = dtHdon.Rows[0]["shdon"]?.ToString();
                        string khieu = dtHdon.Rows[0]["khieu"]?.ToString();

                        //int shdon = await tangSoHD(dtHdon.Rows[0]["cctbao_id"].ToString());

                        //string shdon = dtHdon.Rows[0]["shdon"].ToString();

                        //xml bảng kê
                        string qrybangke = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id='{hdon_id.ToString()}'";
                        DataTable Rowbangke = await this._minvoiceDbContext.GetDataTableAsync(qrybangke);

                        JObject ObjXml_BangKe = null;
                        var xml_bangke = "";
                        if (Rowbangke.Rows.Count > 0)
                        {
                            ObjXml_BangKe = await XmlInvoice_BangKe(hdon_id.ToString());
                        }

                        if (ObjXml_BangKe != null)
                        {
                            //xml_bangke = ObjXml_BangKe["xml"].ToString().Replace("'", "''");
                            xml_bangke = ObjXml_BangKe["xml"].ToString();

                            if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                            {
                                xml_bangke = CommonManager.ThemPhanDonViKiThoiGianKy(xml_bangke, await getTenDv());
                                SignXML input = new SignXML()
                                {
                                    msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                    xmlData = xml_bangke,
                                    namespacePrefix = "",
                                    reason = "Ky bang ke dien tu",
                                    signatureTagId = "seller",
                                    signingTagId = "data",
                                    signingTagName = "NBan",
                                    prompt = "Ky bang ke dien tu"
                                };
                                SignedXmlReturn result = CommonManager.SignSimPKI(input);
                                //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                                xml_bangke = result.xmlSignedData;
                            }
                            else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                            {
                                xml_bangke = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                    xml_bangke, tblCertificate.Rows[0]["cer_serial"].ToString());
                            }
                            else
                            {
                                xml_bangke = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml_bangke, certFile, "NBan");
                            }

                        }

                        JObject ObjXml = await XmlInvoice(hdon_id.ToString());

                        //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                        string xml = ObjXml["xml"].ToString();
                        string tdlap = ObjXml["tdlap"].ToString().Replace("'", "''");
                        string tdky = ObjXml["tdky"].ToString().Replace("'", "''");

                        if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                        {
                            xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                            SignXML input = new SignXML()
                            {
                                msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                xmlData = xml,
                                namespacePrefix = "",
                                reason = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                                signatureTagId = "seller",
                                signingTagId = "data",
                                signingTagName = "NBan",
                                prompt = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                            };
                            SignedXmlReturn result = CommonManager.SignSimPKI(input);
                            //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                            xml = result.xmlSignedData;
                        }
                        else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                        {
                            xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                        }
                        else
                        {
                            xml = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml, certFile, "NBan");
                        }

                        //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                        //sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice('#SCHEMA_NAME#','" + hdon_id + "','" + xml + "','" + tblUser.Rows[0]["fullname"].ToString() + "')";
                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("dlxml_id", Guid.NewGuid());
                        parameters.Add("hdon_id", hdon_id);
                        parameters.Add("dlxml", xml);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }

                        string sqlXml = "";
                        if (!string.IsNullOrEmpty(xml_bangke))
                        {
                            if (CommonManager.CheckThoiGian(xml_bangke) == false)
                            {
                                //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                                //return res;
                                throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            }
                            parameters.Add("dlxml_bangke", xml_bangke);
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68(dlxml_id,hdon_id,dlxml,dlxml_bangke) VALUES(@dlxml_id,@hdon_id, @dlxml, @dlxml_bangke)";
                        }
                        else
                        {
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68 VALUES(@dlxml_id,@hdon_id, @dlxml)";
                        }

                        await _minvoiceDbContext.TransactionCommandAsync(sqlXml, CommandType.Text, parameters);

                        string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, ngky = @ngky, nky=@nky, tdlap = @tdlap, kygui_cqt =@kygui_cqt WHERE hdon_id = @hdon_id";
                        Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                        parameters2.Add("tthai", model["guiCQT"] == null ? "Đã gửi" : "Đã ký");
                        parameters2.Add("kygui_cqt", model["guiCQT"] == null ? 1 : 0);
                        parameters2.Add("ngky", username);
                        parameters2.Add("hdon_id", hdon_id);
                        //parameters2.Add("nky", DateTime.Parse(tdky));
                        parameters2.Add("nky", DateTime.Now);
                        //parameters2.Add("shdon", shdon);
                        parameters2.Add("tdlap", DateTime.Parse(tdlap));

                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                        // lấy giá trị signature

                        //XmlDocument doc = new XmlDocument();
                        //doc.LoadXml(xml);

                        // lấy thông tin xml TTChung của Thông điệp
                        string mst = await getMstDv();
                        string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                        XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, cmd_type, 1);

                        // lấy thông tin xml dữ liệu    
                        //XElement xml_DuLieu = await XmlInvoiceNew(model["hdon_id"].ToString());

                        // đẩy dữ liệu hóa đơn vào thẻ DLieu
                        XElement dLieu = new XElement("DLieu");
                        dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                        XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                        var writerSettings = new XmlWriterSettings();
                        writerSettings.OmitXmlDeclaration = true;

                        using (var buffer = new StringWriter())
                        using (var writer = XmlWriter.Create(buffer, writerSettings))
                        {
                            xmlThongDiep.Save(writer);
                            writer.Flush();
                            xml = buffer.ToString();
                        }

                        //xml = xmlThongDiep.ToString();

                        // ký thông điệp gửi lên thuế

                        //xml = CommonManager.SignThongDiep(xml, certFile, pass);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }
                        if (model["guiCQT"] == null)
                        {
                            // lưu vào bảng tonghop_gui_tvan_68

                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                      + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mltdiep_gui", cmd_type);
                            param_Insert.Add("xml_tdiep_gui", xml);
                            param_Insert.Add("type_id", hdon_id);
                            param_Insert.Add("mtdiep_gui", mtdiep_gui);
                            //param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                            param_Insert.Add("note", "app");
                            param_Insert.Add("mdvi", _webHelper.GetDvcs());

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            // update mã thông điệp gửi vào bảng hóa đơn
                            Dictionary<string, object> para = new Dictionary<string, object>();
                            para.Add("hdon_id", hdon_id);
                            if (cmd_type == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                            {
                                // update trạng thái
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("tthai", "Chờ cấp mã");
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }
                            else
                            {
                                // update mã thông điệp gửi
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }

                        }

                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();

                        if (model["guiCQT"] == null)
                        {
                            // đóng gói xml gửi lên TVAN
                            string cmd = cmd_type;

                            string data = await ThongDiepGui(cmd, xml);

                            // đẩy queue lên TVAN
                            _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                            //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, data);
                        }

                        sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                        DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                        //JArray array = JArray.FromObject(tblInvoices);

                        res.RemoveAll();

                        //res.Add("ok", true);
                        //res.Add("inv", (JObject)array[0]);
                        res.Add("hdon_id", hdon_id);
                        res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                        res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                        res.Add("tthai", "Đã ký");

                        //res.RemoveAll();
                        #endregion
                        if (cmd_type == "203")
                        {
                            #region xử lý gửi mail theo hdon_id
                            string email = tblInvoices.Rows[0]["email"].ToString();
                            string tieude = "Thông báo xuất hóa đơn điện tử";


                            dynamic ObjectMail = new JObject();
                            ObjectMail.id = hdon_id;
                            ObjectMail.nguoinhan = email;
                            ObjectMail.tieude = tieude;

                            arrayMail.Add(ObjectMail);


                            #endregion
                        }

                    }
                    catch (Exception ex)
                    {
                        error += ";" + ex.Message;

                        try
                        {
                            await _minvoiceDbContext.TransactionRollbackAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        catch { }
                        break;
                    }

                }

                if (arrayMail.Count > 0)
                {
                    Dictionary<string, object> dicPara = new Dictionary<string, object>();
                    dicPara.Add("branch_code", branch_code);
                    sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                    DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);
                    if (tblSetting.Rows.Count > 0)
                    {
                        string value = tblSetting.Rows[0]["value"].ToString();
                        if (value == "C")
                        {
                            ThreadPool.QueueUserWorkItem(state => ExecuteSendMailHdon(folder, arrayMail));
                        }
                    }
                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async Task<XElement> XmlTTChungTDiep(string mtdiep, string mltdiep, int sluong)
        {
            JObject _obj = new JObject();
            XElement xmlInvoices = null;
            try
            {
                string mstOrigin = await getMstDv();
                string mst = CommonManager.removeSpecialCharacter(mstOrigin);
                //string mngui = "K" + mst;
                string mngui = mst;
                // generate mã thông điệp
                //string guid_id = CommonManager.removeSpecialCharacter(Guid.NewGuid().ToString().ToUpper());
                //string mtdiep = string.Format("{0}{1}", mngui, guid_id);

                xmlInvoices =
                   new XElement("TTChung",
                        new XElement("PBan", "2.0.0"),
                        new XElement("MNGui", mngui),
                        new XElement("MNNhan", CommonConstants.TVAN_MST),
                        new XElement("MLTDiep", mltdiep),
                        new XElement("MTDiep", mtdiep),
                        new XElement("MTDTChieu", ""),
                        new XElement("MST", mstOrigin),
                        new XElement("SLuong", sluong)
                );
            }
            catch (Exception e)
            {

                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return xmlInvoices;
        }
        public XElement XmlTTChungTDiep(string mtdiep, string mltdiep, int sluong, string mstOrigin)
        {
            JObject _obj = new JObject();
            XElement xmlInvoices = null;
            try
            {
                string mst = CommonManager.removeSpecialCharacter(mstOrigin);
                //string mngui = "K" + mst;
                string mngui = mst;
                // generate mã thông điệp
                //string guid_id = CommonManager.removeSpecialCharacter(Guid.NewGuid().ToString().ToUpper());
                //string mtdiep = string.Format("{0}{1}", mngui, guid_id);

                xmlInvoices =
                   new XElement("TTChung",
                        new XElement("PBan", "2.0.0"),
                        new XElement("MNGui", mngui),
                        new XElement("MNNhan", CommonConstants.TVAN_MST),
                        new XElement("MLTDiep", mltdiep),
                        new XElement("MTDiep", mtdiep),
                        new XElement("MTDTChieu", ""),
                        new XElement("MST", mstOrigin),
                        new XElement("SLuong", sluong)
                );
            }
            catch (Exception e)
            {

                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return xmlInvoices;
        }
        public async Task<string> XmlThongDiepInvoice(string hdon_id)
        {
            string xml = string.Empty;
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE type_id = '{hdon_id}' and (mltdiep_gui = '200' or mltdiep_gui = '203')";
                DataTable dtInvoice = await this._minvoiceDbContext.GetDataTableAsync(sql);

                if (dtInvoice.Rows.Count > 0)
                {
                    xml = dtInvoice.Rows[0]["xml_tdiep_gui"].ToString();
                }
            }
            catch (Exception e)
            {
                //_obj.Add("error", e.Message);
            }
            return xml;
        }

        public async Task<JArray> GetListCertificatesFile68(string userName, string ma_dvcs)
        {
            string sql = "SELECT a.pl_certificate_id,a.issuer,a.signer,CONCAT (a.issuer, '; ', a.subject_name)  as subject_name,a.begin_date,a.end_date,a.cert_type,d.pass,a.cer_serial as id,a.cer_serial,a.cer_serial as so_serial FROM #SCHEMA_NAME#.pl_certificate a "
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id "
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id "
                        + "LEFT JOIN #SCHEMA_NAME#.wb_token d ON a.branch_code=d.branch_code AND a.token_serial=d.serial_number "
                        + "WHERE c.username=N'" + userName + "' AND a.branch_code=N'" + ma_dvcs + "' AND a.cert_type in ('FILE','SIM','HSM_SCTV')";
            DataTable tblCerts = await this._minvoiceDbContext.GetDataTableAsync(sql);

            JArray data = JArray.FromObject(tblCerts);

            return data;
        }

        public async Task<JArray> GetDetailHoadonChoXuLy(string data)
        {


            //string sql = "SELECT CAST(ROW_NUMBER() OVER (ORDER BY a.shdon,a.nlap,a.khieu asc) as integer) as stt, \n"
            //    + "a.khieu as khieu, a.shdon as shdon,a.nlap as nlap,CASE WHEN COALESCE(a.tnmua, '') = '' THEN a.ten ELSE a.tnmua END as tnmua, \n "
            //    + "a.mst as mstnmua, a.mnmua as mkhang, b.ma as mhhoa,  b.ten as thhdvu, b.mdvtinh as dvtinh, \n"
            //    + "b.sluong as sluong, (b.thtien - b.stckhau) as ttcthue, b.tsuat as tsuat, b.tthue as tgtthue, b.tgtien as tgtttoan, \n "
            //    + "0 as tthai, --a.tthai, trạng thái hóa đơn \n "
            //    + "a.hdon_ghichu as tttthai, --note trạng thái hóa đơn \n "
            //    + "'tthdlquan' as tthdlquan, -- Dữ liệu hóa đơn liên quan \n "
            //    + "1 as lhdlquan, --Hóa đơn điện tử theo Nghị định số 123 / 2020 / NĐ - CP \n "
            //    + "d.khieu as khhdlquan, d.shdon as shdlquan, a.gchu gchu, \n "
            //    + "null stbao, null ntbao, a.hdon_id hdon_id, b.cthdon_id cthdon_id FROM #SCHEMA_NAME#.hoadon68 a"
            //    + " INNER JOIN #SCHEMA_NAME#.hoadon68_chitiet b ON a.hdon_id = b.hdon_id \n"
            //    + " LEFT JOIN  #SCHEMA_NAME#.hoadon68 d on d.hdon68_id_lk = a.hdon_id \n"
            //    + $" WHERE a.hdon_id IN ({data}) order by shdon desc";

            //string qry = $"SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE branch_code = '{_webHelper.GetDvcs()}' AND code = 'DV_XANGDAU' ";

            //DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);

            //if (dt.Rows.Count > 0)
            //{
            //    if (dt.Rows[0]["value"].ToString().ToUpper() == "C")
            //    {
            //        sql = "SELECT a.nlap as nlap, a.khieu as khieu, a.shdon as shdon,b.sluong as sluong,a.ten as tnmua,a.mst as mstnmua,b.ten as thhdvu,b.tsuat as tsuat, b.mdvtinh as dvtinh,b.thtien as ttcthue,b.tthue as tgtthue,b.tgtien as tgtttoan, mst as mstnmua, tgtcthue as ttcthue,tgtttbso as tgtttoan FROM #SCHEMA_NAME#.hoadon68 a"
            //                + $" INNER JOIN #SCHEMA_NAME#.hoadon68_chitiet b ON a.hdon_id = b.hdon_id WHERE a.hdon_id IN ({data})";
            //    }
            //}

            //string qry2 = $"SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE branch_code = '{_webHelper.GetDvcs()}' AND code = 'DV_DIENNUOC' ";
            //DataTable dt2 = await this._minvoiceDbContext.GetDataTableAsync(qry2);

            //if (dt2.Rows.Count > 0)
            //{
            //    if (dt2.Rows[0]["value"].ToString().ToUpper() == "C")
            //    {
            //        sql = "SELECT a.nlap as nlap, a.khieu as khieu, a.shdon as shdon,null as sluong,a.ten as tnmua,CASE WHEN a.mst IS NULL THEN mnmua ELSE a.mst END as mstnmua, null as thhdvu,b.tsuat as tsuat, null as dvtinh,b.thtien as ttcthue,b.tthue as tgtthue,b.tgtien as tgtttoan, mst as mstnmua, tgtcthue as ttcthue,tgtttbso as tgtttoan FROM #SCHEMA_NAME#.hoadon68 a"
            //                + $" INNER JOIN #SCHEMA_NAME#.hoadon68_chitiet b ON a.hdon_id = b.hdon_id WHERE a.hdon_id IN ({data})";
            //    }
            //}

            //DataTable res = await this._minvoiceDbContext.GetDataTableAsync(sql);

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("ma_dvcs", _webHelper.GetDvcs());
            parameters.Add("p_list_hoadon_id", data);

            //var invoiceDb = this._nopDbContext.GetInvoiceDb();
            string sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bthdl_68_final_result_quang('#SCHEMA_NAME#',@ma_dvcs,@p_list_hoadon_id)";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
            JArray _res = JArray.FromObject(dt);
            return _res;
        }

        public async Task<JObject> GetHoadonChoXuLy(JObject model)
        {
            JObject json = new JObject();
            try
            {
                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string loai_hanghoa = model["lhhoa"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));

                string sql = "";

                if (loai_hanghoa == "1")
                {
                    sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bthdl_68_xangdau_get('#SCHEMA_NAME#',@ma_dvcs,@tu_ngay,@den_ngay)";
                }
                else
                {
                    //var invoiceDb = this._nopDbContext.GetInvoiceDb();
                    sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bthdl_68('#SCHEMA_NAME#',@ma_dvcs,@tu_ngay,@den_ngay)";
                }
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray jarr = JArray.FromObject(dt);
                json.Add("data", jarr);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;


        }

        public async Task<JObject> SignInvoiceCertFile68(string folder, JObject obj, bool reSign = false)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                JArray arrData = (JArray)obj["data"];

                JObject model = (JObject)arrData[0];

                string cmd_type = model["type_cmd"].ToString();

                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                //Guid hdon_id = Guid.Parse(model["hdon_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                         + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                         + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                         + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }


                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                //get arr hdon_id từ obj để for
                JArray arrhdon_id = (JArray)model["lsthdon_id"];

                JArray arrayMail = new JArray();

                for (int i = 0; i < arrhdon_id.Count; i++)
                {
                    try
                    {
                        var hdonid = arrhdon_id[i].ToString();

                        Guid hdon_id = Guid.Parse(hdonid);

                        #region hatm CheckQuyTrinhHoaDon
                        Dictionary<string, object> parametersN = new Dictionary<string, object>();
                        parametersN.Add("hdon_id", hdon_id);

                        DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon,cctbao_id,mdvi FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parametersN);
                        string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                        parametersN.Clear();
                        parametersN.Add("hdon_id", hdon_id);
                        parametersN.Add("nlap", DateTime.Parse((invoice_issued_date)));
                        parametersN.Add("cctbao_id", Guid.Parse(tblInvoiceAuth.Rows[0]["cctbao_id"].ToString()));
                        parametersN.Add("mdvi", tblInvoiceAuth.Rows[0]["mdvi"].ToString());

                        sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='Chờ ký' and cctbao_id = @cctbao_id and mdvi = @mdvi AND nlap< @nlap LIMIT 1";
                        DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parametersN);

                        if (tblInvoiceAuthBefore.Rows.Count > 0)
                        {
                            // res.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                            // return res;
                            throw new Exception("E0001");
                        }

                        #endregion

                        await _minvoiceDbContext.BeginTransactionAsync();

                        #region lấy số hóa đơn khi không có
                        if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                        {
                            sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                            JObject masterObj = new JObject();
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                            masterObj.Add("hdon_id", hdon_id);

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        }
                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                        #endregion

                        await _minvoiceDbContext.BeginTransactionAsync();

                        #region Xử lý hoá đơn theo hdon_id
                        DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT tthai, shdon, khieu FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hdon_id + "'");
                        if (dtHdon.Rows.Count > 0)
                        {
                            if (dtHdon.Rows[0]["tthai"].ToString() != "Chờ ký")
                            {
                                // res.Add("error", "Tồn tại hóa đơn không ở trạng thái chờ ký");
                                // return res;
                                throw new Exception("E0002");
                            }
                        }

                        string shdon = dtHdon.Rows[0]["shdon"]?.ToString();
                        string khieu = dtHdon.Rows[0]["khieu"]?.ToString();

                        //int shdon = await tangSoHD(dtHdon.Rows[0]["cctbao_id"].ToString());

                        //string shdon = dtHdon.Rows[0]["shdon"].ToString();

                        //xml bảng kê
                        string qrybangke = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id='{hdon_id.ToString()}'";
                        DataTable Rowbangke = await this._minvoiceDbContext.GetDataTableAsync(qrybangke);

                        JObject ObjXml_BangKe = null;
                        var xml_bangke = "";
                        if (Rowbangke.Rows.Count > 0)
                        {
                            ObjXml_BangKe = await XmlInvoice_BangKe(hdon_id.ToString());
                        }

                        if (ObjXml_BangKe != null)
                        {
                            //xml_bangke = ObjXml_BangKe["xml"].ToString().Replace("'", "''");
                            xml_bangke = ObjXml_BangKe["xml"].ToString();

                            if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                            {
                                xml_bangke = CommonManager.ThemPhanDonViKiThoiGianKy(xml_bangke, await getTenDv());
                                SignXML input = new SignXML()
                                {
                                    msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                    xmlData = xml_bangke,
                                    namespacePrefix = "",
                                    reason = "Ky bang ke dien tu",
                                    signatureTagId = "seller",
                                    signingTagId = "data",
                                    signingTagName = "NBan",
                                    prompt = "Ky bang ke dien tu"
                                };
                                SignedXmlReturn result = CommonManager.SignSimPKI(input);
                                //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                                xml_bangke = result.xmlSignedData;
                            }
                            else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                            {
                                xml_bangke = await CommonManager.SignXmlInvoiceHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                    xml_bangke, tblCertificate.Rows[0]["cer_serial"].ToString());
                            }
                            else
                            {
                                xml_bangke = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml_bangke, certFile, "NBan");
                            }

                        }

                        JObject ObjXml = await XmlInvoice(hdon_id.ToString());

                        //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                        string xml = ObjXml["xml"].ToString();
                        string tdlap = ObjXml["tdlap"].ToString().Replace("'", "''");
                        string tdky = ObjXml["tdky"].ToString().Replace("'", "''");

                        if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                        {
                            xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                            SignXML inputSign = new SignXML()
                            {
                                msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                xmlData = xml,
                                namespacePrefix = "",
                                reason = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                                signatureTagId = "seller",
                                signingTagId = "data",
                                signingTagName = "NBan",
                                prompt = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                            };
                            SignedXmlReturn resultSign = CommonManager.SignSimPKI(inputSign);
                            //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                            xml = resultSign.xmlSignedData;
                        }
                        else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                        {
                            xml = await CommonManager.SignXmlInvoiceHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                        }
                        else
                        {
                            xml = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml, certFile, "NBan");
                        }

                        //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                        //sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice('#SCHEMA_NAME#','" + hdon_id + "','" + xml + "','" + tblUser.Rows[0]["fullname"].ToString() + "')";
                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("dlxml_id", Guid.NewGuid());
                        parameters.Add("hdon_id", hdon_id);
                        parameters.Add("dlxml", xml);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }

                        string sqlXml = "";
                        if (!string.IsNullOrEmpty(xml_bangke))
                        {
                            if (CommonManager.CheckThoiGian(xml_bangke) == false)
                            {
                                //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                                //return res;
                                throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            }

                            parameters.Add("dlxml_bangke", xml_bangke);
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68(dlxml_id,hdon_id,dlxml,dlxml_bangke) VALUES(@dlxml_id,@hdon_id, @dlxml, @dlxml_bangke)";
                        }
                        else
                        {
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68 VALUES(@dlxml_id,@hdon_id, @dlxml)";
                        }

                        await _minvoiceDbContext.TransactionCommandAsync(sqlXml, CommandType.Text, parameters);

                        string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, ngky = @ngky, nky=@nky, tdlap = @tdlap, kygui_cqt =@kygui_cqt WHERE hdon_id = @hdon_id";
                        Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                        parameters2.Add("tthai", model["guiCQT"] == null ? "Đã gửi" : "Đã ký");
                        parameters2.Add("kygui_cqt", model["guiCQT"] == null ? 1 : 0);
                        parameters2.Add("ngky", username);
                        parameters2.Add("hdon_id", hdon_id);
                        //parameters2.Add("nky", DateTime.Parse(tdky));
                        parameters2.Add("nky", DateTime.Now);
                        //parameters2.Add("shdon", shdon);
                        parameters2.Add("tdlap", DateTime.Parse(tdlap));

                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                        // lấy giá trị signature

                        //XmlDocument doc = new XmlDocument();
                        //doc.LoadXml(xml);

                        // lấy thông tin xml TTChung của Thông điệp
                        string mst = await getMstDv();
                        string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                        XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, model["type_cmd"].ToString(), 1);

                        // lấy thông tin xml dữ liệu
                        //XElement xml_DuLieu = await XmlInvoiceNew(model["hdon_id"].ToString());

                        // đẩy dữ liệu hóa đơn vào thẻ DLieu
                        XElement dLieu = new XElement("DLieu");
                        dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                        XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                        var writerSettings = new XmlWriterSettings();
                        writerSettings.OmitXmlDeclaration = true;

                        using (var buffer = new StringWriter())
                        using (var writer = XmlWriter.Create(buffer, writerSettings))
                        {
                            xmlThongDiep.Save(writer);
                            writer.Flush();
                            xml = buffer.ToString();
                        }

                        //xml = xmlThongDiep.ToString();

                        // ký thông điệp gửi lên thuế

                        //xml = CommonManager.SignThongDiep(xml, certFile, pass);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }
                        if (model["guiCQT"] == null)
                        {
                            // lưu vào bảng tonghop_gui_tvan_68

                            if (model["is_api"] == null)
                            {
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                     + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                param_Insert.Add("id", Guid.NewGuid());
                                param_Insert.Add("mltdiep_gui", model["type_cmd"].ToString());
                                param_Insert.Add("xml_tdiep_gui", xml);
                                param_Insert.Add("type_id", hdon_id);
                                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                                param_Insert.Add("note", "app");
                                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                            }
                            else
                            {
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi, is_api) VALUES "
                                    + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi, @is_api)";
                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                param_Insert.Add("id", Guid.NewGuid());
                                param_Insert.Add("mltdiep_gui", model["type_cmd"].ToString());
                                param_Insert.Add("xml_tdiep_gui", xml);
                                param_Insert.Add("type_id", hdon_id);
                                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                                param_Insert.Add("note", "app");
                                param_Insert.Add("mdvi", _webHelper.GetDvcs());
                                param_Insert.Add("is_api", "1"); // phân biệt đơn vị gọi từ API

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                            }

                            // update mã thông điệp gửi vào bảng hóa đơn
                            Dictionary<string, object> para = new Dictionary<string, object>();
                            para.Add("hdon_id", hdon_id);
                            if (model["type_cmd"].ToString() == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                            {
                                // update trạng thái
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("tthai", "Chờ cấp mã");
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }
                            else
                            {
                                // update mã thông điệp gửi
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }

                        }

                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();

                        res.RemoveAll();

                        if (model["guiCQT"] == null)
                        {
                            // đóng gói xml gửi lên TVAN
                            string cmd = model["type_cmd"].ToString();

                            string data = await ThongDiepGui(cmd, xml);

                            // đẩy queue lên TVAN
                            _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                            res.Add("trang_thai", "Đã gửi hóa đơn tới Cơ quan thuế");

                            //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, data);
                        }

                        sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                        DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                        //JArray array = JArray.FromObject(tblInvoices);

                        //res.Add("ok", true);
                        //res.Add("inv", (JObject)array[0]);
                        //res.Add("hdon_id", hdon_id);
                        //res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                        //res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                        res.Add("tthai", "Đã ký");

                        //res.RemoveAll();
                        #endregion
                        if (cmd_type == "203")
                        {
                            #region xử lý gửi mail theo hdon_id
                            string email = tblInvoices.Rows[0]["email"].ToString();
                            string tieude = "Thông báo xuất hóa đơn điện tử";


                            dynamic ObjectMail = new JObject();
                            ObjectMail.id = hdon_id;
                            ObjectMail.nguoinhan = email;
                            ObjectMail.tieude = tieude;

                            arrayMail.Add(ObjectMail);


                            #endregion
                        }

                        // Update date trạng thái ký thành công trong trường hợp ký lại các hoá đơn bị lỗi
                        sql = $"UPDATE #SCHEMA_NAME#.wb_log_sign SET resign_success = true WHERE hdon_id = '{hdonid}'";
                        _minvoiceDbContext.ExecuteNoneQuery(sql);
                    }
                    catch (Exception ex)
                    {
                        // error += ex.Message + "; ";

                        try
                        {
                            await _minvoiceDbContext.TransactionRollbackAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        catch { }

                        // Xử lý khi ký bị lỗi
                        var hdonid = arrhdon_id[i].ToString();


                        var sqlCheckExist =
                            $"SELECT COUNT(1) FROM #SCHEMA_NAME#.wb_log_sign WHERE hdon_id = '{hdonid}'";
                        var rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheckExist);

                        if (!reSign && rsTable.Rows[0].Field<long>("count") == 0)
                        {
                            var sqlUpdate =
                                $"INSERT INTO #SCHEMA_NAME#.wb_log_sign (hdon_id, error_log, branch_code) VALUES ('{hdonid}', '{ex.Message}', '{branch_code}')";
                            _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
                        }
                        else
                        {
                            var sqlUpdate = $"UPDATE #SCHEMA_NAME#.wb_log_sign SET sign_retry = sign_retry + 1 WHERE hdon_id = '{hdonid}'";
                            _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
                        }

                        if ("E0002".Equals(ex.Message))
                        {
                            // res.Add("error", "Tồn tại hóa đơn không ở trạng thái chờ ký");
                            // return res;
                            error += "Tồn tại hóa đơn không ở trạng thái chờ ký" + "; ";
                        }
                        else if ("E0001".Equals(ex.Message))
                        {
                            // res.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                            // return res;
                            error += "Bạn chưa xử lý hóa đơn của ngày hôm trước" + "; ";
                        }
                        else
                        {
                            error += ex.Message + "; ";
                        }

                        // res.Add("error", ex.Message);
                        // Log.Error(ex.Message);
                        // return res;
                        break;
                    }
                }

                if (arrayMail.Count > 0)
                {
                    Dictionary<string, object> dicPara = new Dictionary<string, object>();
                    dicPara.Add("branch_code", branch_code);
                    sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                    DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);
                    if (tblSetting.Rows.Count > 0)
                    {
                        string value = tblSetting.Rows[0]["value"].ToString();
                        if (value == "C")
                        {
                            ThreadPool.QueueUserWorkItem(state => ExecuteSendMailHdon(folder, arrayMail));
                        }
                    }
                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);
                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }

            return res;
        }

        public async void ExecuteSendMailHdon(string folder, JArray arrMail)
        {
            for (int i = 0; i < arrMail.Count; i++)
            {
                await AutoSendInvoiceByEmail(folder, (JObject)arrMail[i]);
            }
        }

        public async Task<JObject> SignInvoiceCertFile68SCTV(string folder, JObject obj, bool reSign = false)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                JArray arrData = (JArray)obj["data"];

                JObject model = (JObject)arrData[0];

                string cmd_type = model["type_cmd"].ToString();

                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                //Guid hdon_id = Guid.Parse(model["hdon_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                         + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                         + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                         + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }


                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                //get arr hdon_id từ obj để for
                JArray arrhdon_id = (JArray)model["lsthdon_id"];

                JArray arrayMail = new JArray();

                for (int i = 0; i < arrhdon_id.Count; i++)
                {
                    try
                    {
                        var hdonid = arrhdon_id[i].ToString();

                        Guid hdon_id = Guid.Parse(hdonid);

                        #region hatm CheckQuyTrinhHoaDon
                        Dictionary<string, object> parametersN = new Dictionary<string, object>();
                        parametersN.Add("hdon_id", hdon_id);

                        DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon,cctbao_id,mdvi FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parametersN);
                        string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                        parametersN.Clear();
                        parametersN.Add("hdon_id", hdon_id);
                        parametersN.Add("nlap", DateTime.Parse((invoice_issued_date)));
                        parametersN.Add("cctbao_id", Guid.Parse(tblInvoiceAuth.Rows[0]["cctbao_id"].ToString()));
                        parametersN.Add("mdvi", tblInvoiceAuth.Rows[0]["mdvi"].ToString());

                        sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='Chờ ký' and cctbao_id = @cctbao_id and mdvi = @mdvi AND nlap< @nlap LIMIT 1";
                        DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parametersN);

                        if (tblInvoiceAuthBefore.Rows.Count > 0)
                        {
                            // res.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                            // return res;
                            throw new Exception("E0001");
                        }

                        #endregion

                        await _minvoiceDbContext.BeginTransactionAsync();

                        #region lấy số hóa đơn khi không có
                        if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                        {
                            sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                            JObject masterObj = new JObject();
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                            masterObj.Add("hdon_id", hdon_id);

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        }
                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                        #endregion

                        #region Xử lý hoá đơn theo hdon_id
                        DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT tthai, shdon, khieu FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hdon_id + "'");
                        if (dtHdon.Rows.Count > 0)
                        {
                            if (dtHdon.Rows[0]["tthai"].ToString() != "Chờ ký")
                            {
                                //res.Add("error", "Tồn tại hóa đơn không ở trạng thái chờ ký");
                                //return res;
                                res.RemoveAll();
                                res.Add("tthai", "Đã ký");
                                res.Add("ok", true);
                                return res;
                            }
                        }

                        await _minvoiceDbContext.BeginTransactionAsync();

                        string shdon = dtHdon.Rows[0]["shdon"]?.ToString();
                        string khieu = dtHdon.Rows[0]["khieu"]?.ToString();

                        //int shdon = await tangSoHD(dtHdon.Rows[0]["cctbao_id"].ToString());

                        //string shdon = dtHdon.Rows[0]["shdon"].ToString();

                        //xml bảng kê
                        string qrybangke = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id='{hdon_id.ToString()}'";
                        DataTable Rowbangke = await this._minvoiceDbContext.GetDataTableAsync(qrybangke);

                        JObject ObjXml_BangKe = null;
                        var xml_bangke = "";
                        if (Rowbangke.Rows.Count > 0)
                        {
                            ObjXml_BangKe = await XmlInvoice_BangKe(hdon_id.ToString());
                        }

                        if (ObjXml_BangKe != null)
                        {
                            //xml_bangke = ObjXml_BangKe["xml"].ToString().Replace("'", "''");
                            xml_bangke = ObjXml_BangKe["xml"].ToString();

                            if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                            {
                                xml_bangke = CommonManager.ThemPhanDonViKiThoiGianKy(xml_bangke, await getTenDv());
                                SignXML input = new SignXML()
                                {
                                    msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                    xmlData = xml_bangke,
                                    namespacePrefix = "",
                                    reason = "Ky bang ke dien tu",
                                    signatureTagId = "seller",
                                    signingTagId = "data",
                                    signingTagName = "NBan",
                                    prompt = "Ky bang ke dien tu"
                                };
                                SignedXmlReturn result = CommonManager.SignSimPKI(input);
                                //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                                xml_bangke = result.xmlSignedData;
                            }
                            else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                            {
                                xml_bangke = await CommonManager.SignXmlInvoiceHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                    xml_bangke, tblCertificate.Rows[0]["cer_serial"].ToString());
                            }
                            else
                            {
                                xml_bangke = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml_bangke, certFile, "NBan");
                            }

                        }

                        JObject ObjXml = await XmlInvoice(hdon_id.ToString());

                        //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                        string xml = ObjXml["xml"].ToString();
                        string tdlap = ObjXml["tdlap"].ToString().Replace("'", "''");
                        string tdky = ObjXml["tdky"].ToString().Replace("'", "''");

                        if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                        {
                            xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                            SignXML inputSign = new SignXML()
                            {
                                msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                xmlData = xml,
                                namespacePrefix = "",
                                reason = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                                signatureTagId = "seller",
                                signingTagId = "data",
                                signingTagName = "NBan",
                                prompt = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                            };
                            SignedXmlReturn resultSign = CommonManager.SignSimPKI(inputSign);
                            //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                            xml = resultSign.xmlSignedData;
                        }
                        else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                        {
                            xml = await CommonManager.SignXmlInvoiceHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                        }
                        else
                        {
                            xml = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml, certFile, "NBan");
                        }

                        //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                        //sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice('#SCHEMA_NAME#','" + hdon_id + "','" + xml + "','" + tblUser.Rows[0]["fullname"].ToString() + "')";
                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("dlxml_id", Guid.NewGuid());
                        parameters.Add("hdon_id", hdon_id);
                        parameters.Add("dlxml", xml);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }

                        string sqlXml = "";
                        if (!string.IsNullOrEmpty(xml_bangke))
                        {
                            if (CommonManager.CheckThoiGian(xml_bangke) == false)
                            {
                                //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                                //return res;
                                throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            }

                            parameters.Add("dlxml_bangke", xml_bangke);
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68(dlxml_id,hdon_id,dlxml,dlxml_bangke) VALUES(@dlxml_id,@hdon_id, @dlxml, @dlxml_bangke)";
                        }
                        else
                        {
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68 VALUES(@dlxml_id,@hdon_id, @dlxml)";
                        }

                        await _minvoiceDbContext.TransactionCommandAsync(sqlXml, CommandType.Text, parameters);

                        string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, ngky = @ngky, nky=@nky, tdlap = @tdlap, kygui_cqt =@kygui_cqt WHERE hdon_id = @hdon_id";
                        Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                        parameters2.Add("tthai", model["guiCQT"] == null ? "Đã gửi" : "Đã ký");
                        parameters2.Add("kygui_cqt", model["guiCQT"] == null ? 1 : 0);
                        parameters2.Add("ngky", username);
                        parameters2.Add("hdon_id", hdon_id);
                        //parameters2.Add("nky", DateTime.Parse(tdky));
                        parameters2.Add("nky", DateTime.Now);
                        //parameters2.Add("shdon", shdon);
                        parameters2.Add("tdlap", DateTime.Parse(tdlap));

                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                        // lấy giá trị signature

                        //XmlDocument doc = new XmlDocument();
                        //doc.LoadXml(xml);

                        // lấy thông tin xml TTChung của Thông điệp
                        string mst = await getMstDv();
                        string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                        XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, model["type_cmd"].ToString(), 1);

                        // lấy thông tin xml dữ liệu
                        //XElement xml_DuLieu = await XmlInvoiceNew(model["hdon_id"].ToString());

                        // đẩy dữ liệu hóa đơn vào thẻ DLieu
                        XElement dLieu = new XElement("DLieu");
                        dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                        XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                        var writerSettings = new XmlWriterSettings();
                        writerSettings.OmitXmlDeclaration = true;

                        using (var buffer = new StringWriter())
                        using (var writer = XmlWriter.Create(buffer, writerSettings))
                        {
                            xmlThongDiep.Save(writer);
                            writer.Flush();
                            xml = buffer.ToString();
                        }

                        //xml = xmlThongDiep.ToString();

                        // ký thông điệp gửi lên thuế

                        //xml = CommonManager.SignThongDiep(xml, certFile, pass);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }
                        if (model["guiCQT"] == null)
                        {
                            // lưu vào bảng tonghop_gui_tvan_68

                            if (model["is_api"] == null)
                            {
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                     + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                param_Insert.Add("id", Guid.NewGuid());
                                param_Insert.Add("mltdiep_gui", model["type_cmd"].ToString());
                                param_Insert.Add("xml_tdiep_gui", xml);
                                param_Insert.Add("type_id", hdon_id);
                                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                                param_Insert.Add("note", "app");
                                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                            }
                            else
                            {
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi, is_api) VALUES "
                                    + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi, @is_api)";
                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                param_Insert.Add("id", Guid.NewGuid());
                                param_Insert.Add("mltdiep_gui", model["type_cmd"].ToString());
                                param_Insert.Add("xml_tdiep_gui", xml);
                                param_Insert.Add("type_id", hdon_id);
                                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                                param_Insert.Add("note", "app");
                                param_Insert.Add("mdvi", _webHelper.GetDvcs());
                                param_Insert.Add("is_api", "1"); // phân biệt đơn vị gọi từ API

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                            }

                            // update mã thông điệp gửi vào bảng hóa đơn
                            Dictionary<string, object> para = new Dictionary<string, object>();
                            para.Add("hdon_id", hdon_id);
                            if (model["type_cmd"].ToString() == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                            {
                                // update trạng thái
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("tthai", "Chờ cấp mã");
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }
                            else
                            {
                                // update mã thông điệp gửi
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }

                        }

                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();

                        res.RemoveAll();

                        if (model["guiCQT"] == null)
                        {
                            // đóng gói xml gửi lên TVAN
                            string cmd = model["type_cmd"].ToString();

                            string data = await ThongDiepGui(cmd, xml);

                            // đẩy queue lên TVAN
                            _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                            res.Add("trang_thai", "Đã gửi hóa đơn tới Cơ quan thuế");

                            //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, data);
                        }

                        sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                        DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                        //JArray array = JArray.FromObject(tblInvoices);

                        //res.Add("ok", true);
                        //res.Add("inv", (JObject)array[0]);
                        //res.Add("hdon_id", hdon_id);
                        //res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                        //res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                        res.Add("tthai", "Đã ký");

                        //res.RemoveAll();
                        #endregion
                        if (cmd_type == "203")
                        {
                            #region xử lý gửi mail theo hdon_id
                            string email = tblInvoices.Rows[0]["email"].ToString();
                            string tieude = "Thông báo xuất hóa đơn điện tử";


                            dynamic ObjectMail = new JObject();
                            ObjectMail.id = hdon_id;
                            ObjectMail.nguoinhan = email;
                            ObjectMail.tieude = tieude;

                            arrayMail.Add(ObjectMail);


                            #endregion
                        }

                        // Update date trạng thái ký thành công trong trường hợp ký lại các hoá đơn bị lỗi
                        sql = $"UPDATE #SCHEMA_NAME#.wb_log_sign SET resign_success = true WHERE hdon_id = '{hdonid}'";
                        _minvoiceDbContext.ExecuteNoneQuery(sql);
                    }
                    catch (Exception ex)
                    {
                        // error += ex.Message + "; ";

                        try
                        {
                            await _minvoiceDbContext.TransactionRollbackAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        catch { }

                        // Xử lý khi ký bị lỗi
                        var hdonid = arrhdon_id[i].ToString();


                        var sqlCheckExist =
                            $"SELECT COUNT(1) FROM #SCHEMA_NAME#.wb_log_sign WHERE hdon_id = '{hdonid}'";
                        var rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheckExist);

                        if (!reSign && rsTable.Rows[0].Field<long>("count") == 0)
                        {
                            var sqlUpdate =
                                $"INSERT INTO #SCHEMA_NAME#.wb_log_sign (hdon_id, error_log, branch_code) VALUES ('{hdonid}', '{ex.Message}', '{branch_code}')";
                            _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
                        }
                        else
                        {
                            var sqlUpdate = $"UPDATE #SCHEMA_NAME#.wb_log_sign SET sign_retry = sign_retry + 1 WHERE hdon_id = '{hdonid}'";
                            _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
                        }

                        if ("E0002".Equals(ex.Message))
                        {
                            // res.Add("error", "Tồn tại hóa đơn không ở trạng thái chờ ký");
                            // return res;
                            error += "Tồn tại hóa đơn không ở trạng thái chờ ký" + "; ";
                        }
                        else if ("E0001".Equals(ex.Message))
                        {
                            // res.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                            // return res;
                            error += "Bạn chưa xử lý hóa đơn của ngày hôm trước" + "; ";
                        }
                        else
                        {
                            error += ex.Message + "; ";
                        }

                        // res.Add("error", ex.Message);
                        // Log.Error(ex.Message);
                        // return res;
                        break;
                    }
                }

                if (arrayMail.Count > 0)
                {
                    Dictionary<string, object> dicPara = new Dictionary<string, object>();
                    dicPara.Add("branch_code", branch_code);
                    sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                    DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);
                    if (tblSetting.Rows.Count > 0)
                    {
                        string value = tblSetting.Rows[0]["value"].ToString();
                        if (value == "C")
                        {
                            ThreadPool.QueueUserWorkItem(state => ExecuteSendMailHdon(folder, arrayMail));
                        }
                    }
                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }

            return res;
        }

        public async Task<JObject> SendInvoiceToCQT68(JObject obj)
        {
            JObject res = new JObject();
            string error = "";
            try
            {
                JArray invs = (JArray)obj["invs"];
                string mst = await getMstDv();
                // lấy dữ liệu xml thông tin chung

                for (int i = 0; i < invs.Count; i++)
                {
                    string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                    string mltdiep = obj["mltdiep"].ToString();

                    XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, mltdiep, 1);

                    XElement dLieu = new XElement("DLieu");

                    await _minvoiceDbContext.BeginTransactionAsync();

                    string hdon_id = invs[i].ToString();

                    Dictionary<string, object> dicParam = new Dictionary<string, object>();
                    dicParam.Add("hdon_id", Guid.Parse(hdon_id));

                    // kiểm tra xem hóa đơn đã được gửi đi hay chưa?

                    DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT kygui_cqt FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id", CommandType.Text, dicParam);
                    string is_kygui_cqt = tblInvoice.Rows[0]["kygui_cqt"].ToString();
                    if (is_kygui_cqt == "1")
                    {
                        //res.Add("error", "Có hóa đơn đã được gửi tới CQT");
                        //return res;
                        throw new Exception("Có hóa đơn đã được gửi tới CQT");
                    }

                    // get xml hóa đơn
                    DataTable tblXml68 = await _minvoiceDbContext.GetDataTableAsync("SELECT dlxml,dlxml_ngmua FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id=@hdon_id", CommandType.Text, dicParam);
                    // đẩy dữ liệu hóa đơn vào thẻ DLieu
                    if (tblXml68.Rows.Count == 0)
                    {
                        //res.Add("error", "Tồn tại hóa đơn chưa được ký");
                        //return res;
                        throw new Exception("Tồn tại hóa đơn chưa được ký");
                    }

                    string dlxml = "";

                    dlxml = tblXml68.Rows[0]["dlxml"].ToString();
                    string dlxml_ngmua = tblXml68.Rows[0]["dlxml_ngmua"].ToString();
                    //string dlxml_thue = tblXml68.Rows[0]["dlxml_thue"].ToString();

                    if (!string.IsNullOrEmpty(dlxml_ngmua))
                    {
                        dlxml = dlxml_ngmua;
                    }
                    //if (mltdiep == "200")
                    //{
                    //    if (!string.IsNullOrEmpty(dlxml_thue))
                    //    {
                    //        dlxml = dlxml_thue;
                    //    }
                    //}

                    //dLieu.Add(XElement.Parse(tblXml68.Rows[0]["dlxml"].ToString()));
                    dLieu.Add(XElement.Parse(dlxml));

                    XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                    string xml = string.Empty;
                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;

                    using (var buffer = new StringWriter())
                    using (var writer = XmlWriter.Create(buffer, writerSettings))
                    {
                        xmlThongDiep.Save(writer);
                        writer.Flush();
                        xml = buffer.ToString();
                    }

                    // gửi danh sách hóa đơn không mã lên cổng VAN

                    // đóng gói xml gửi lên TVAN
                    //string cmd = CommonConstants.MLTDIEP_GUI_HOA_DON_KHONG_MA;

                    string data = await ThongDiepGui(mltdiep, xml);

                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }
                    // lưu vào bảng tonghop_gui_tvan_68

                    string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                    Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                    param_Insert.Add("id", Guid.NewGuid());
                    param_Insert.Add("mltdiep_gui", mltdiep);
                    param_Insert.Add("xml_tdiep_gui", xml);
                    param_Insert.Add("type_id", Guid.Parse(invs[i].ToString()));
                    param_Insert.Add("mtdiep_gui", mtdiep_gui);
                    //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                    param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                    param_Insert.Add("note", "app");
                    param_Insert.Add("mdvi", _webHelper.GetDvcs());

                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                    // update mã thông điệp gửi vào bảng hóa đơn
                    Dictionary<string, object> para = new Dictionary<string, object>();
                    para.Add("hdon_id", Guid.Parse(invs[i].ToString()));
                    para.Add("mtdiep_gui", mtdiep_gui);

                    if (mltdiep == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                    {
                        // update trạng thái, mã thông điệp gửi
                        string sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                        para.Add("tthai", "Chờ cấp mã");

                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                    }
                    else
                    {
                        // update mã thông điệp gửi
                        string sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui, kygui_cqt=1 WHERE hdon_id = @hdon_id";
                        para.Add("tthai", "Đã gửi");
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    // đẩy queue lên TVAN
                    _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                }

                res.Add("trang_thai", "Đã gửi hóa đơn tới Cơ quan thuế");
                res.Add("ok", true);

            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return res;
        }

        public async Task<byte[]> SignXmlBangKeBanRa(JObject model)
        {
            JObject res = new JObject();
            string error = "";
            byte[] result = null;
            try
            {
                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký");
                    return result;
                }

                //Guid hdon_id = Guid.Parse(model["hdon_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                }


                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                //string xml = model["xml"].ToString().Replace("'", "''");
                string xml = model["xml"].ToString();
                if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                {
                    xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                    SignXML input = new SignXML()
                    {
                        msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                        xmlData = xml,
                        namespacePrefix = "",
                        reason = "Ky bang ke ban ra",
                        signatureTagId = "seller",
                        signingTagId = "data",
                        signingTagName = "NNT",
                        prompt = "Ky bang ke ban ra"
                    };
                    SignedXmlReturn resultSign = CommonManager.SignSimPKI(input);
                    //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                    xml = resultSign.xmlSignedData;
                }
                else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                {
                    xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NNT", "xml",
                        xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                }
                else
                {
                    xml = CommonManager.SignXml(await getTenDv(), "#data", "seller", xml, certFile, "NNT");
                }

                result = System.Text.Encoding.UTF8.GetBytes(xml);

            }
            catch (Exception ex)
            {
                error = ex.Message;
                res.Add("error", error);
                Log.Error(error);
            }

            return result;
        }

        public async Task<JObject> SaveXml(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {

                if (model["username"] == null)
                {
                    model.Add("username", _webHelper.GetUser());
                }
                else
                {
                    model["username"] = _webHelper.GetUser();
                }

                Guid hdon_id = Guid.Parse(model["hdon_id"].ToString());
                string xml = model["xml"].ToString();
                string xmlbangke = model["xmlbangke"] != null ? model["xmlbangke"].ToString() : null;
                string username = model["username"].ToString();
                //Int32 shdon = Convert.ToInt32(model["shdon"].ToString());

                await _minvoiceDbContext.BeginTransactionAsync();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("dlxml_id", Guid.NewGuid());
                parameters.Add("hdon_id", hdon_id);
                parameters.Add("dlxml", xml);
                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                var checkSoHdon = await CheckSoHoaDon(xml, hdon_id);

                if (checkSoHdon == false)
                {
                    throw new Exception("Số hóa đơn không đúng với hóa đơn ký");
                }

                string sqlCheckHDKy = "SELECT hdon_id from #SCHEMA_NAME#.dulieuxml68 where hdon_id=@hdon_id";

                DataTable tblCheckHDKy = await _minvoiceDbContext.GetDataTableAsync(sqlCheckHDKy, CommandType.Text, parameters);

                if (tblCheckHDKy.Rows.Count > 0)
                {
                    throw new Exception("Tồn tại hóa đơn đã ký.");
                }

                string sqlXml = "";
                if (!string.IsNullOrEmpty(xmlbangke))
                {
                    if (CommonManager.CheckThoiGian(xmlbangke) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }

                    var checkSoBke = await CheckSoBangKe(xml, hdon_id);

                    if (checkSoBke == false)
                    {
                        throw new Exception("Số bảng kê không đúng với hóa đơn ký");
                    }

                    parameters.Add("dlxml_bangke", xmlbangke);
                    sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68(dlxml_id, hdon_id, dlxml,dlxml_bangke) VALUES(@dlxml_id,@hdon_id, @dlxml,@dlxml_bangke)";
                }
                else
                {
                    sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68 VALUES(@dlxml_id,@hdon_id, @dlxml)";
                }

                await _minvoiceDbContext.TransactionCommandAsync(sqlXml, CommandType.Text, parameters);

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, ngky = @ngky, nky=@nky, tdlap = @tdlap, kygui_cqt = @kygui_cqt WHERE hdon_id = @hdon_id";
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("tthai", model["guiCQT"] == null ? "Đã gửi" : "Đã ký");
                parameters2.Add("kygui_cqt", model["guiCQT"] == null ? 1 : 0);
                parameters2.Add("ngky", username);
                parameters2.Add("hdon_id", hdon_id);
                parameters2.Add("nky", DateTime.Now);
                //parameters2.Add("shdon", shdon);
                parameters2.Add("tdlap", DateTime.Now);

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                //JObject parameters = null;
                //await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                // lấy thông tin xml TTChung của Thông điệp
                string mst = await getMstDv();
                string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, model["type_cmd"].ToString(), 1);

                // lấy thông tin xml dữ liệu
                //XElement xml_DuLieu = await XmlInvoiceNew(model["hdon_id"].ToString());

                // đẩy dữ liệu hóa đơn vào thẻ DLieu
                XElement dLieu = new XElement("DLieu");
                dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlThongDiep.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                if (model["guiCQT"] == null)
                {
                    // lưu vào bảng tonghop_gui_tvan_68

                    string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                              + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                    Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                    param_Insert.Add("id", Guid.NewGuid());
                    param_Insert.Add("mltdiep_gui", model["type_cmd"].ToString());
                    param_Insert.Add("xml_tdiep_gui", xml);
                    param_Insert.Add("type_id", hdon_id);
                    param_Insert.Add("mtdiep_gui", mtdiep_gui);
                    //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                    param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                    param_Insert.Add("note", "app");
                    param_Insert.Add("mdvi", _webHelper.GetDvcs());

                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                    // update mã thông điệp gửi vào bảng hóa đơn
                    Dictionary<string, object> para = new Dictionary<string, object>();
                    para.Add("hdon_id", hdon_id);
                    if (model["type_cmd"].ToString() == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                    {
                        // update trạng thái
                        string sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                        para.Add("tthai", "Chờ cấp mã");
                        para.Add("mtdiep_gui", mtdiep_gui);

                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                    }
                    else
                    {
                        // update mã thông điệp gửi
                        string sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                        para.Add("mtdiep_gui", mtdiep_gui);

                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                    }

                }

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                if (model["guiCQT"] == null)
                {
                    // đóng gói xml gửi lên TVAN
                    string cmd = model["type_cmd"].ToString();

                    string data = await ThongDiepGui(cmd, xml);

                    // đẩy queue lên TVAN
                    _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                    //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                    //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                }

                string sql1 = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql1);

                if (model["type_cmd"].ToString() == "203")
                {
                    string email = tblInvoices.Rows[0]["email"].ToString();
                    string tieude = "Thông báo xuất hóa đơn điện tử";


                    dynamic ObjectMail = new JObject();
                    ObjectMail.id = hdon_id;
                    ObjectMail.nguoinhan = email;
                    ObjectMail.tieude = tieude;
                    var arrayMail = new JArray();
                    arrayMail.Add(ObjectMail);

                    Dictionary<string, object> dicPara = new Dictionary<string, object>();
                    dicPara.Add("branch_code", _webHelper.GetDvcs());
                    string sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                    DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);
                    if (tblSetting.Rows.Count > 0)
                    {
                        string value = tblSetting.Rows[0]["value"].ToString();
                        if (value == "C")
                        {
                            string originalString = _webHelper.GetRequest().Url.OriginalString;
                            string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";
                            var folder = System.Web.HttpContext.Current.Server.MapPath(path);
                            ThreadPool.QueueUserWorkItem(state => ExecuteSendMailHdon(folder, arrayMail));
                        }
                    }
                }

                JArray array = JArray.FromObject(tblInvoices);

                res.Add("ok", true);
                res.Add("inv", (JObject)array[0]);
                res.Add("hdon_id", hdon_id);
                res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                res.Add("tthai", model["guiCQT"] == null ? "Đã gửi" : "Đã ký");
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return res;
        }
        public async Task<JArray> GetListCertificates(string userName, string ma_dvcs)
        {
            string sql = "SELECT a.pl_certificate_id,a.issuer,a.signer,a.subject_name,a.begin_date,a.end_date,a.cert_type,d.pass,a.cer_serial as id,a.cer_serial,a.cer_serial as so_serial FROM #SCHEMA_NAME#.pl_certificate a "
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id "
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id "
                        + "LEFT JOIN #SCHEMA_NAME#.wb_token d ON a.branch_code=d.branch_code AND a.token_serial=d.serial_number "
                        + "WHERE c.username=N'" + userName + "' AND a.branch_code=N'" + ma_dvcs + "'";
            DataTable tblCerts = await this._minvoiceDbContext.GetDataTableAsync(sql);

            JArray data = JArray.FromObject(tblCerts);

            return data;
        }

        public async Task<JObject> ExportInvoiceXml(Guid id)
        {
            JObject obj = new JObject();

            try
            {
                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + id + "'");

                if (tblInvoices.Rows.Count == 0)
                {
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                //DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + id + "'");
                //int shdon = await tangSoHD(tblInvoices.Rows[0]["cctbao_id"].ToString());
                string shdon = tblInvoices.Rows[0]["shdon"].ToString();
                var hdon_id = tblInvoices.Rows[0]["hdon_id"].ToString();
                #region hatm CheckQuyTrinhHoaDon
                //var hdon_id = Guid.Parse(tblInvoices.Rows[0]["hdon_id"].ToString());
                Dictionary<string, object> parametersN = new Dictionary<string, object>();
                parametersN.Add("hdon_id", hdon_id);

                DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon,cctbao_id,mdvi FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parametersN);
                string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                parametersN.Clear();
                parametersN.Add("hdon_id", hdon_id);
                parametersN.Add("nlap", DateTime.Parse((invoice_issued_date)));
                parametersN.Add("cctbao_id", Guid.Parse(tblInvoiceAuth.Rows[0]["cctbao_id"].ToString()));
                parametersN.Add("mdvi", tblInvoiceAuth.Rows[0]["mdvi"].ToString());

                string sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='Chờ ký' and cctbao_id=@cctbao_id and mdvi=@mdvi AND nlap< @nlap LIMIT 1";
                DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parametersN);

                if (tblInvoiceAuthBefore.Rows.Count > 0)
                {
                    obj.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                    return obj;
                }

                #endregion

                #region lấy số hóa đơn khi không có
                if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                {
                    await _minvoiceDbContext.BeginTransactionAsync();

                    sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                    JObject masterObj = new JObject();
                    masterObj.Add("branch_code", _webHelper.GetDvcs());
                    masterObj.Add("hdon_id", hdon_id);

                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                #endregion

                JObject ObjXml = await XmlInvoice(hdon_id);

                //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                string xml = ObjXml["xml"].ToString();

                string qrybangke = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id='{hdon_id}'";
                DataTable Rowbangke = await this._minvoiceDbContext.GetDataTableAsync(qrybangke);

                JObject ObjXml_BangKe = null;
                var xml_bangke = "";
                if (Rowbangke.Rows.Count > 0)
                {
                    ObjXml_BangKe = await XmlInvoice_BangKe(hdon_id);
                }

                if (ObjXml_BangKe != null)
                {
                    //xml_bangke = ObjXml_BangKe["xml"].ToString().Replace("'", "''");
                    xml_bangke = ObjXml_BangKe["xml"].ToString();
                }

                obj.Add("hdon_id", id);
                obj.Add("InvoiceXmlData", xml);

                if (!string.IsNullOrEmpty(xml_bangke))
                {
                    obj.Add("InvoiceXmlBangKe", xml_bangke);
                }
                obj.Add("shdon", shdon);
                obj.Add("dvky", await getTenDv());
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
                Log.Error(ex.Message);
            }

            return obj;
        }


        public async Task<string> getMstDv()
        {
            string sql = "SELECT tax_code FROM #SCHEMA_NAME#.wb_branch WHERE code ='" + _webHelper.GetDvcs() + "'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> getTenDv()
        {
            string sql = "SELECT name FROM #SCHEMA_NAME#.wb_branch WHERE code ='" + _webHelper.GetDvcs() + "'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql);
            return dt.Rows[0][0].ToString();
        }

        public async Task<int> tangSoHD(string ccttbao_id)
        {
            string sql = "SELECT coalesce(max(shdon),'0') as sohd FROM #SCHEMA_NAME#.hoadon68 WHERE cctbao_id ='" + ccttbao_id + "'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql);

            return Convert.ToInt16(dt.Rows[0][0]) + 1;
        }

        public async Task<int> tangSoBTHDL()
        {
            string sql = "SELECT coalesce(max(sbthdlieu),0) as sohd FROM #SCHEMA_NAME#.bangtonghopdl_68";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql);

            return Convert.ToInt16(dt.Rows[0][0]) + 1;
        }

        public async Task<int> tangSoBTHDL(string fromDate, string toDate)
        {
            // string sql = $@"SELECT coalesce(max(sbthdlieu),0) as sohd FROM #SCHEMA_NAME#.bangtonghopdl_68 
            //                 WHERE tu_ngay = '{fromDate}' AND den_ngay = '{toDate}'";
            string sql = $@"SELECT coalesce(max(t.sbthdlieu),0) as sohd FROM 
                            (SELECT a.sbthdlieu FROM #SCHEMA_NAME#.bangtonghopdl_68 a
                            WHERE a.tu_ngay::date = '{fromDate}'::date AND a.den_ngay::date = '{toDate}'::date AND a.mdvi ='{_webHelper.GetDvcs()}') t";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql);

            return Convert.ToInt16(dt.Rows[0][0]) + 1;
        }

        public async Task<JObject> SaveXmlPretreatment(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {

                if (model["username"] == null)
                {
                    model.Add("username", _webHelper.GetUser());
                }
                else
                {
                    model["username"] = _webHelper.GetUser();
                }

                if (string.IsNullOrEmpty(model.Value<string>("hdon_id")))
                {
                    throw new Exception("Id của hóa đơn không được để trống");
                }

                if (string.IsNullOrEmpty(model.Value<string>("xml")))
                {
                    throw new Exception("Xml không được bỏ trống");
                }

                if (string.IsNullOrEmpty(model.Value<string>("type_cmd"))
                    || (!string.IsNullOrEmpty(model.Value<string>("type_cmd"))
                    && !"200".Equals(model.Value<string>("type_cmd")) && !"203".Equals(model.Value<string>("type_cmd"))))
                {
                    throw new Exception("type_cmd bắt buộc nhập 200 hoặc 203");
                }

                if (!string.IsNullOrEmpty(model.Value<string>("guiCQT")) && !"NotSend".Equals(model.Value<string>("guiCQT"))
                    || "".Equals(model.Value<string>("guiCQT")))
                {
                    throw new Exception("Nếu chỉ ký không gửi cơ quan thuế thì bắt buộc nhập 'guiCQT' là 'NotSend'");
                }

                Guid hdon_id = Guid.Parse(model["hdon_id"].ToString());
                string xml = model["xml"].ToString();
                string xmlbangke = model["xmlbangke"]?.ToString();
                string username = model["username"].ToString();
                string branchCode = _webHelper.GetDvcs();
                string typeCmd = model.Value<string>("type_cmd");
                //Int32 shdon = Convert.ToInt32(model["shdon"].ToString());

                JArray listCertificates = await GetListCertificates(username, branchCode);

                if (listCertificates.Count == 0)
                {
                    throw new Exception("Bạn chưa được phân quyền chứng thư số");
                }

                var sqlCheckInvoice = $@"SELECT tthai, nlap, hdon_id, shdon, mdvi, khieu, is_hdcma
                                        FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id = '{hdon_id}'";

                DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync(sqlCheckInvoice);

                if (tblInv_InvoiceAuth.Rows.Count == 0)
                {
                    throw new Exception("Không tồn tại hóa đơn");
                }

                if (tblInv_InvoiceAuth.Rows[0]["tthai"].ToString() != "Chờ ký")
                {
                    throw new Exception("Hóa đơn không ở trạng thái chờ ký");
                }

                var hasCode = tblInv_InvoiceAuth.Rows[0].Field<int>("is_hdcma");
                if (hasCode == 0 && "200".Equals(typeCmd))
                {
                    throw new Exception("Hóa đơn ký số là hóa đơn không có mã CQT nên 'type_cmd' bắt buộc phải là '203'");
                }
                if (hasCode == 1 && "203".Equals(typeCmd))
                {
                    throw new Exception("Hóa đơn ký số là hóa đơn có mã CQT nên 'type_cmd' bắt buộc phải là '200'");
                }

                var sqlCheckTaxCode = $"SELECT tax_code FROM #SCHEMA_NAME#.wb_branch WHERE code = '{branchCode}'";
                DataTable taxCodeTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheckTaxCode);

                if (taxCodeTable.Rows.Count == 0)
                {
                    throw new Exception("Không tồn tại mã đơn vị");
                }

                var resultCheckHieuLucXml = CommonManager.CheckThoiGian(xml);
                if (resultCheckHieuLucXml == false)
                {
                    throw new Exception("XML hóa đơn được ký bởi chữ ký số đã hiệu lực");
                }

                var taxCode = taxCodeTable.Rows[0].Field<string>("tax_code");

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.PreserveWhitespace = true;
                xmlDocument.LoadXml(xml);

                //XmlNodeList taxCodeSeller = xmlDocument.GetElementsByTagName("MSTTCGP");
                //if (taxCodeSeller.Count > 0)
                //{
                //    if (!taxCode.Equals(taxCodeSeller[0].InnerText))
                //    {
                //        throw new Exception("Mã số thuế người bán với chứng thư số không khớp nhau");
                //    }
                //} 

                XmlNodeList nodeListSignature = xmlDocument.GetElementsByTagName("Signature");

                // Kiểm tra tính hợp lệ của chữ ký.
                if (nodeListSignature.Count <= 0)
                {
                    throw new Exception("Không tìm thấy chữ ký trong dữ liệu");
                }

                foreach (XmlElement element in nodeListSignature)
                {
                    // if (element.Attributes["Id"].Value == "seller")
                    // {
                    //     string subjectName = element.GetElementsByTagName("X509SubjectName")[0].InnerText;
                    //
                    //     Dictionary<string, object> par = new Dictionary<string, object>();
                    //     par.Add("branch_code", branchCode);
                    //     par.Add("subject_name", subjectName);
                    //
                    //     DataTable checkCKS = await _minvoiceDbContext.GetDataTableAsync(
                    //         "SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code and subject_name=@subject_name and current_timestamp <= end_date",
                    //         CommandType.Text, par);
                    //
                    //     if (checkCKS.Rows.Count == 0)
                    //     {
                    //         throw new Exception("Chữ ký số không tồn tại trên hệ thống");
                    //     }
                    // }

                    try
                    {
                        // Create a new SignedXml object and pass it
                        // the XML document class.
                        SignedXml signedXml = new SignedXml(xmlDocument);

                        // Load the signature node.
                        signedXml.LoadXml(element);

                        // Check the signature and return the result.
                        if (!signedXml.CheckSignature())
                        {
                            throw new Exception("Chữ ký số đã bị thay đổi");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.Message);
                        throw new Exception("Chữ ký số không hợp lệ");
                    }
                }

                if (!string.IsNullOrEmpty(xmlbangke))
                {
                    var resultCheckHieuLucXmlBangKe = CommonManager.CheckThoiGian(xmlbangke);
                    if (resultCheckHieuLucXmlBangKe == false)
                    {
                        throw new Exception("XML bảng kê kèm theo hóa đơn được ký bởi chữ ký số đã hiệu lực");
                    }

                    xmlDocument = new XmlDocument();
                    xmlDocument.Load(xml);

                    //taxCodeSeller = xmlDocument.GetElementsByTagName("MSTTCGP");
                    //if (taxCodeSeller.Count > 0)
                    //{
                    //    if (!taxCode.Equals(taxCodeSeller[0].InnerText))
                    //    {
                    //        throw new Exception("Mã số thuế người bán với chứng thư số không khớp nhau");
                    //    }
                    //}

                    nodeListSignature = xmlDocument.GetElementsByTagName("Signature");

                    // Kiểm tra tính hợp lệ của chữ ký.
                    if (nodeListSignature.Count <= 0)
                    {
                        throw new Exception("Không tìm thấy chữ ký trong dữ liệu");
                    }

                    foreach (XmlElement element in nodeListSignature)
                    {
                        if (element.Attributes["Id"].Value == "seller")
                        {
                            string subjectName = element.GetElementsByTagName("X509SubjectName")[0].InnerText;

                            Dictionary<string, object> par = new Dictionary<string, object>();
                            par.Add("branch_code", branchCode);
                            par.Add("subject_name", subjectName);

                            DataTable checkCKS = await _minvoiceDbContext.GetDataTableAsync(
                                "SELECT * FROM #SCHEMA_NAME#.pl_certificate WHERE branch_code=@branch_code and subject_name=@subject_name and current_timestamp <= end_date",
                                CommandType.Text, par);

                            if (checkCKS.Rows.Count == 0)
                            {
                                throw new Exception("Chữ ký số không tồn tại trên hệ thống");
                            }
                        }

                        try
                        {
                            // Create a new SignedXml object and pass it
                            // the XML document class.
                            SignedXml signedXml = new SignedXml(xmlDocument);

                            // Load the signature node.
                            signedXml.LoadXml(element);

                            // Check the signature and return the result.
                            if (!signedXml.CheckSignature())
                            {
                                throw new Exception("Chữ ký số đã bị thay đổi");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            throw new Exception("Chữ ký số không hợp lệ");
                        }
                    }
                }

                res = await SaveXml(model);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);
                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return res;
        }

        public async Task<JObject> ExportInvoiceXmlPretreatment(Guid id)
        {
            JObject obj = new JObject();

            try
            {
                DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + id + "'");

                if (tblInvoices.Rows.Count == 0)
                {
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                //DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + id + "'");
                //int shdon = await tangSoHD(tblInvoices.Rows[0]["cctbao_id"].ToString());
                string shdon = tblInvoices.Rows[0]["shdon"].ToString();
                var hdon_id = tblInvoices.Rows[0]["hdon_id"].ToString();
                #region hatm CheckQuyTrinhHoaDon
                //var hdon_id = Guid.Parse(tblInvoices.Rows[0]["hdon_id"].ToString());
                Dictionary<string, object> parametersN = new Dictionary<string, object>();
                parametersN.Add("hdon_id", hdon_id);

                DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon,cctbao_id,mdvi FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parametersN);
                string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                parametersN.Clear();
                parametersN.Add("hdon_id", hdon_id);
                parametersN.Add("nlap", DateTime.Parse((invoice_issued_date)));
                parametersN.Add("cctbao_id", Guid.Parse(tblInvoiceAuth.Rows[0]["cctbao_id"].ToString()));
                parametersN.Add("mdvi", tblInvoiceAuth.Rows[0]["mdvi"].ToString());

                string sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='Chờ ký' and cctbao_id=@cctbao_id and mdvi=@mdvi AND nlap< @nlap LIMIT 1";
                DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parametersN);

                if (tblInvoiceAuthBefore.Rows.Count > 0)
                {
                    obj.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                    return obj;
                }

                #endregion

                #region lấy số hóa đơn khi không có
                if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                {
                    await _minvoiceDbContext.BeginTransactionAsync();

                    sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                    JObject masterObj = new JObject();
                    masterObj.Add("branch_code", _webHelper.GetDvcs());
                    masterObj.Add("hdon_id", hdon_id);

                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                #endregion

                JObject ObjXml = await XmlInvoice(hdon_id);

                //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                string xml = ObjXml["xml"].ToString();
                xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());

                string qrybangke = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id='{hdon_id}'";
                DataTable Rowbangke = await this._minvoiceDbContext.GetDataTableAsync(qrybangke);

                JObject ObjXml_BangKe = null;
                var xml_bangke = "";
                if (Rowbangke.Rows.Count > 0)
                {
                    ObjXml_BangKe = await XmlInvoice_BangKe(hdon_id);
                }

                if (ObjXml_BangKe != null)
                {
                    //xml_bangke = ObjXml_BangKe["xml"].ToString().Replace("'", "''");
                    xml_bangke = ObjXml_BangKe["xml"].ToString();
                    xml_bangke = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                }

                obj.Add("hdon_id", id);
                obj.Add("InvoiceXmlData", xml);

                if (!string.IsNullOrEmpty(xml_bangke))
                {
                    obj.Add("InvoiceXmlBangKe", xml_bangke);
                }
                obj.Add("shdon", shdon);
                obj.Add("dvky", await getTenDv());
            }
            catch (Exception ex)
            {
                obj.Add("error", ex.Message);
                Log.Error(ex.Message);
            }

            return obj;
        }

        class SuppressEncodingStringWriter : StringWriter
        {
            public sealed override Encoding Encoding => Encoding.UTF8;
        }

        public async Task<JObject> Amount_ToWord(string number, string type_amount)
        {
            JObject json = new JObject();
            try
            {
                if (type_amount == "USD")
                {
                    json.Add("data", FunctionUtil.ConvertUpper(FunctionUtil.USD_Amount(number)));
                }
                else
                {
                    json.Add("data", FunctionUtil.ConvertUpper(FunctionUtil.VND_Amount2(number)));
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }

        public async Task<JObject> Amount_ToWord_New(string number, int checkReadTV)
        {
            JObject json = new JObject();
            try
            {
                if (checkReadTV == 0)
                {
                    json.Add("data", FunctionUtil.ConvertUpper(FunctionUtil.USD_Amount_New(number)));
                }
                else
                {
                    json.Add("data", FunctionUtil.ConvertUpper(FunctionUtil.USD_Amount_Read_TV(number)));
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }

        #region quản lý mẫu thiết kế
        public async Task<JObject> deleteMau68(JObject model)
        {
            JObject _result = new JObject();
            try
            {
                string sql = "#SCHEMA_NAME#.crd_quanlymau68_delete";

                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, model);
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception e)
            {
                _result.Add("error", e.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return _result;
        }
        public async Task<JObject> SaveQuanlymau68(string editMode, JObject model, byte[] bytes)
        {
            JObject _result = new JObject();
            try
            {
                string report = System.Text.Encoding.UTF8.GetString(bytes);
                if (model["dulieumau"] == null)
                {
                    model.Add("dulieumau", report);
                }
                else
                {
                    model["dulieumau"] = report;
                }

                var id = Guid.NewGuid();
                if (editMode == "2")
                {
                    if (model["qlmtke_id"] == null)
                    {
                        model.Add("qlmtke_id", model["id"]);
                    }
                    else
                    {
                        model["qlmtke_id"] = model["id"];
                    }
                }

                #region set default thêm mới

                if (editMode == "1")
                {
                    if (model["qlmtke_id"] == null)
                    {
                        model.Add("qlmtke_id", id);
                    }
                    else
                    {
                        model["qlmtke_id"] = id;
                    }

                    if (model["mmau"] == null)
                    {
                        model.Add("mmau", await tangsoMau());
                    }
                    else
                    {
                        model["mmau"] = await tangsoMau();
                    }

                    if (model["ngtao"] == null)
                    {
                        model.Add("ngtao", _webHelper.GetUser());
                    }
                    else
                    {
                        model["ngtao"] = _webHelper.GetUser();
                    }

                    if (model["ntao"] == null)
                    {
                        model.Add("ntao", DateTime.Now);
                    }
                    else
                    {
                        model["ntao"] = DateTime.Now;
                    }

                    if (model["ttsudung"] == null)
                    {
                        model.Add("ttsudung", 0);
                    }
                    else
                    {
                        model["ttsudung"] = 0;
                    }

                    if (model["code"] == null)
                    {
                        model.Add("code", _webHelper.GetDvcs());
                    }
                    else
                    {
                        model["code"] = _webHelper.GetDvcs();
                    }
                }

                #endregion

                string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_quanlymau68_insert" :
                    editMode == "2" ? "#SCHEMA_NAME#.crd_quanlymau68_update" : "#SCHEMA_NAME#.crd_quanlymau68_delete";

                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, model);
                await _minvoiceDbContext.TransactionCommitAsync();
                return _result;
                //string sql = "UPDATE #SCHEMA_NAME#.sl_invoice_template SET template_status='Đã tạo mẫu',report=@report WHERE sl_invoice_template_id=@sl_invoice_template_id";

                //Dictionary<string, object> parameters = new Dictionary<string, object>();
                //parameters.Add("report", report);
                //parameters.Add("sl_invoice_template_id", Guid.Parse(dmmauhoadon_id));

                //await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);

                //string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", dmmauhoadon_id);
                //_cacheManager.Remove(cacheReportKey);
            }
            catch (Exception e)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _result.Add("error", e.Message);
                Log.Error(e.Message);
                return _result;
            }
            finally
            {
                _minvoiceDbContext.CloseTransaction();
            }
        }
        #endregion

        #region Quản lý ký hiệu
        public async Task<JObject> deletequanlykyhieu68(JObject model)
        {
            JObject _result = new JObject();
            try
            {
                string sql = "#SCHEMA_NAME#.crd_quanlykyhieu68_delete";

                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, model);
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception e)
            {
                _result.Add("error", e.Message);
                Log.Error(e.Message);
                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return _result;
        }



        public async Task<JObject> SaveQuanlykyhieu68(string editMode, JObject model)
        {
            JObject _result = new JObject();
            try
            {
                var id = Guid.NewGuid();
                if (editMode == "2")
                {
                    if (model["qlkhsdung_id"] == null)
                    {
                        model.Add("qlkhsdung_id", model["id"]);
                    }
                    else
                    {
                        model["qlkhsdung_id"] = model["id"];
                    }
                }
                #region set default thêm mới
                if (editMode == "1")
                {
                    if (model["qlkhsdung_id"] == null)
                    {
                        model.Add("qlkhsdung_id", id);
                    }
                    else
                    {
                        model["qlkhsdung_id"] = id;
                    }

                    if (model["nglap"] == null)
                    {
                        model.Add("nglap", _webHelper.GetUser());
                    }
                    else
                    {
                        model["nglap"] = _webHelper.GetUser();
                    }

                    if (model["nlap"] == null)
                    {
                        model.Add("nlap", DateTime.Now);
                    }
                    else
                    {
                        model["nlap"] = DateTime.Now;
                    }

                    if (model["code"] == null)
                    {
                        model.Add("code", _webHelper.GetDvcs());
                    }

                }
                #endregion

                string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_quanlykyhieu68_insert" : editMode == "2" ? "#SCHEMA_NAME#.crd_quanlykyhieu68_update" : "#SCHEMA_NAME#.crd_quanlykyhieu68_delete";

                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, model);
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception e)
            {
                _result.Add("error", e.Message);
                Log.Error(e.Message);
                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return _result;
        }
        #endregion

        public async Task<string> tangsoMau()
        {
            string ma = string.Empty;
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync("SELECT COALESCE(MAX(NULLIF(regexp_replace(mmau, '\\D','','g'), '')::numeric), 0) FROM #SCHEMA_NAME#.quanlymau68");

            ma = "M" + (Convert.ToInt16(dt.Rows[0][0]) + 1).ToString().PadLeft(2, '0');
            return ma;
        }

        public byte[] DowloadRepx(string id)
        {
            byte[] s = null;
            DataTable dt = this._minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id = '" + id + "'");
            string dtt = dt.Rows[0]["dulieumau"].ToString();

            s = System.Text.Encoding.UTF8.GetBytes(dtt);
            return s;
        }

        public byte[] DowloadRepxBangKe(string id)
        {
            byte[] s = null;
            DataTable dt = this._minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id = '" + id + "'");
            string dtt = dt.Rows[0]["dulieumaubangke"].ToString();

            s = System.Text.Encoding.UTF8.GetBytes(dtt);
            return s;
        }

        #region Đăng ký/ thay đổi sử dụng

        public async Task<JObject> SignDangkysudung(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {

                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                Guid mau01_id = Guid.Parse(model["mau01_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                }


                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                string mst = await getMstDv();
                //if (!certFile.Subject.ToString().Contains(mst))
                //{
                //    res.Add("error", "Mã số thuế chữ ký số không khớp với đơn vị ");
                //    return res;
                //}
                await _minvoiceDbContext.BeginTransactionAsync();

                string xml = await XmlDangkysudung(mau01_id.ToString());

                if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                {
                    xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                    SignXML input = new SignXML()
                    {
                        msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                        xmlData = xml,
                        namespacePrefix = "",
                        reason = "Ky dang ky 01/DKDT-HDDT",
                        signatureTagId = "seller",
                        signingTagId = "data",
                        signingTagName = "NNT",
                        prompt = "Ky dang ky 01/DKDT-HDDT"
                    };
                    SignedXmlReturn resultSign = CommonManager.SignSimPKI(input);
                    //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                    xml = resultSign.xmlSignedData;
                }
                else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                {
                    xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NNT", "xml",
                        xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                }
                else
                {
                    xml = CommonManager.SignXml(await getTenDv(), "#data", "seller", xml, certFile, "NNT");
                }

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.mau01_68 SET xml_send = @xml_send, trang_thai = @trang_thai, ngay_gui = @ngay_gui, nguoi_gui = @nguoi_gui WHERE mau01_id = @mau01_id";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("mau01_id", mau01_id);
                parameters.Add("xml_send", xml);
                parameters.Add("ngay_gui", DateTime.Now);
                parameters.Add("nguoi_gui", _webHelper.GetUser());
                parameters.Add("trang_thai", "1");

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters);

                bool isUyNhiem = await DangkysudungUyNhiem(mau01_id.ToString());

                // lấy thông tin xml TTChung của Thông điệp
                string mltdiep = isUyNhiem ? CommonConstants.MLTDIEP_TO_KHAI_UY_NHIEM : CommonConstants.MLTDIEP_TO_KHAI_KHONG_UY_NHIEM;
                int sluong = 1;
                string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, mltdiep, sluong);

                // lấy thông tin xml dữ liệu
                //XElement xml_DuLieu = await XmlDangkysudungNew(mau01_id.ToString());

                //string xmlDuLieu = await XmlDangkysudung(mau01_id.ToString());
                //string xmlKy = CommonManager.SignXml(await getTenDv(), "#data", "seller", xmlDuLieu, certFile, "NNT");

                // đẩy dữ liệu hóa đơn vào thẻ DLieu
                XElement dLieu = new XElement("DLieu");
                dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlThongDiep.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                //xml = xmlThongDiep.ToString();

                // ký thông điệp gửi lên thuế
                //xml = CommonManager.SignThongDiep(xml, certFile, pass);

                // đóng gói xml gửi lên TVAN
                string cmd = isUyNhiem ? CommonConstants.MLTDIEP_TO_KHAI_UY_NHIEM : CommonConstants.MLTDIEP_TO_KHAI_KHONG_UY_NHIEM;

                string data = await ThongDiepGui(cmd, xml);

                // lưu vào bảng tonghop_gui_tvan_68

                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                param_Insert.Add("id", Guid.NewGuid());
                param_Insert.Add("mltdiep_gui", cmd);
                param_Insert.Add("xml_tdiep_gui", xml);
                param_Insert.Add("type_id", mau01_id);
                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                param_Insert.Add("note", "app");
                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                // update mã thông điệp gửi vào bảng mau01_68
                param_Insert.Clear();
                param_Insert.Add("mau01_id", mau01_id);
                param_Insert.Add("mtdiep_gui", mtdiep_gui);

                string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.mau01_68 SET mtdiep_gui=@mtdiep_gui WHERE mau01_id = @mau01_id";
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param_Insert);

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                // đẩy queue lên TVAN
                _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                //_rabbitMQService.PublishTaskQueue(_queueNameDkPhathanh, rabbitObj.ToString());
                //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());

                //_kafkaService.PublishTopic(_topicVendorSendToVan, data);

                //sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                //DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                //JArray array = JArray.FromObject(tblInvoices);

                //res.Add("ok", true);
                //res.Add("inv", (JObject)array[0]);
                //res.Add("hdon_id", hdon_id);
                //res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                //res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                //res.Add("tthai", "Đã ký");

            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async Task<JObject> XmlMau01(string id)
        {
            JObject _res = new JObject();
            try
            {
                string xml = await XmlDangkysudung(id);
                _res.Add("xml", xml);
                _res.Add("ok", true);
            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
            }
            return _res;
        }

        public async Task<JObject> SaveXmlMau01(JObject model)
        {
            JObject _res = new JObject();
            try
            {
                Guid mau01_id = Guid.Parse(model["id"].ToString());
                string xml = model["xml"].ToString();

                await _minvoiceDbContext.BeginTransactionAsync();

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.mau01_68 SET xml_send = @xml_send, trang_thai = @trang_thai, ngay_gui = @ngay_gui, nguoi_gui = @nguoi_gui WHERE mau01_id = @mau01_id";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("mau01_id", mau01_id);
                parameters.Add("xml_send", xml);
                parameters.Add("ngay_gui", DateTime.Now);
                parameters.Add("nguoi_gui", _webHelper.GetUser());
                parameters.Add("trang_thai", "1");

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters);

                bool isUyNhiem = await DangkysudungUyNhiem(mau01_id.ToString());

                // lấy thông tin xml TTChung của Thông điệp
                string mltdiep = isUyNhiem ? CommonConstants.MLTDIEP_TO_KHAI_UY_NHIEM : CommonConstants.MLTDIEP_TO_KHAI_KHONG_UY_NHIEM;
                int sluong = 1;
                string mst = await getMstDv();
                string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, mltdiep, sluong);

                // lấy thông tin xml dữ liệu
                //XElement xml_DuLieu = await XmlDangkysudungNew(mau01_id.ToString());

                // đẩy dữ liệu hóa đơn vào thẻ DLieu
                XElement dLieu = new XElement("DLieu");
                dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlThongDiep.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                // đóng gói xml gửi lên TVAN
                string cmd = isUyNhiem ? CommonConstants.MLTDIEP_TO_KHAI_UY_NHIEM : CommonConstants.MLTDIEP_TO_KHAI_KHONG_UY_NHIEM;

                string data = await ThongDiepGui(cmd, xml);

                // lưu vào bảng tonghop_gui_tvan_68

                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                param_Insert.Add("id", Guid.NewGuid());
                param_Insert.Add("mltdiep_gui", cmd);
                param_Insert.Add("xml_tdiep_gui", xml);
                param_Insert.Add("type_id", mau01_id);
                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                param_Insert.Add("note", "app");
                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                // update mã thông điệp gửi vào bảng mau01_68
                param_Insert.Clear();
                param_Insert.Add("mau01_id", mau01_id);
                param_Insert.Add("mtdiep_gui", mtdiep_gui);

                string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.mau01_68 SET mtdiep_gui=@mtdiep_gui WHERE mau01_id = @mau01_id";
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param_Insert);

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                // đẩy queue lên TVAN
                _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                //_rabbitMQService.PublishTaskQueue(_queueNameDkPhathanh, rabbitObj.ToString());
                //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());

            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
                Log.Error(e.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }

            }
            return _res;
        }
        public async Task<string> ExportXMLMau01(string id)
        {
            string qry = $"SELECT xml_send FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> ExportXMLBangTHDL(string id)
        {
            string qry = $"SELECT xml_send FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> ExportXMLMau02(string id)
        {
            string qry = $"SELECT xml_rep FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> ExportXMLMau03(string id)
        {
            string qry = $"SELECT xml_tiepnhan FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> ExportXMLMau04(string id)
        {
            string qry = $"SELECT xml_send FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> ExportXMLMau04Rep(string id)
        {
            string qry = $"SELECT xml_nhan FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> ExportXMLMaThongDiepCQT(string id)
        {
            string qry = $"SELECT xml_cqt FROM #SCHEMA_NAME#.quanly_mathongdiep_cqt WHERE id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<string> ExportXMLHoadon(string id)
        {
            string qry = $"SELECT dlxml, dlxml_thue, dlxml_ngmua FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            string xml = dt.Rows[0]["dlxml"].ToString();
            string xml_thue = dt.Rows[0]["dlxml_thue"].ToString();
            string xml_ngmua = dt.Rows[0]["dlxml_ngmua"].ToString();

            if (!string.IsNullOrEmpty(xml_thue))
            {
                //XDocument doc = XDocument.Parse(xml_thue);

                //xml_thue = doc.Descendants("HDon").First().ToString();

                XmlDocument document = new XmlDocument();
                document.LoadXml(xml_thue);
                XmlNode nodeHDon = document.SelectSingleNode("/TDiep/DLieu/HDon");
                xml_thue = nodeHDon.OuterXml;

                return xml_thue;
            }
            else if (!string.IsNullOrEmpty(xml_ngmua))
            {
                return xml_ngmua;
            }
            else
            {
                return xml;
            }

        }

        public async Task<string> ExportXMLThongDiepHoadon(string id)
        {
            //string qry = $"SELECT dlxml_thongdiep FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id ='{id}'";
            //DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            //return dt.Rows[0][0].ToString();

            string xml = await XmlThongDiepInvoice(id);
            return xml;
        }

        public async Task<JObject> deleteMau04(JObject model)
        {
            JObject _result = new JObject();
            try
            {
                if (!model.ContainsKey("mau04_id") || string.IsNullOrEmpty(model["mau04_id"].ToString()))
                {
                    throw new Exception("mau04_id không được bỏ trống");
                }

                if (Guid.TryParse(model["mau04_id"].ToString(), out Guid mau04Id) == false)
                {
                    throw new Exception("mau04_id không đúng định dạng");
                }

                var sqlCheck = $"select ngay_gui from #SCHEMA_NAME#.mau04_68 where mau04_id = '{mau04Id}'";
                DataTable rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheck);
                if (rsTable.Rows.Count == 0)
                {
                    throw new Exception("Thông báo sai sót không tồn tại");
                }

                if (rsTable.Rows[0]["ngay_gui"] != null && !string.IsNullOrEmpty(rsTable.Rows[0]["ngay_gui"].ToString()))
                {
                    throw new Exception("Thông báo hóa đơn sai sót đã ký gửi CQT không thể xóa");
                }

                string sql = "#SCHEMA_NAME#.crd_mau04_68_delete";

                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, model);
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
                _result.Add("oke", "true");
            }
            catch (Exception e)
            {
                _result.Add("error", e.Message);
                Log.Error(e.Message);
                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return _result;
        }

        public async Task<JObject> deleteMau01(JObject model)
        {
            JObject _result = new JObject();
            try
            {
                string sql = "#SCHEMA_NAME#.crd_mau01_68_delete";

                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, model);
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

            }
            catch (Exception e)
            {
                _result.Add("error", e.Message);
                Log.Error(e.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }
            return _result;
        }

        public async Task<string> XmlDangkysudung(string id)
        {
            DataTable data = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='" + id + "'");
            DataTable dataDetails = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68_chitiet WHERE mau01_id ='" + id + "'");
            DataTable dataUyNhiems = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE mau01_id ='" + id + "'");

            var datadangkysdung = dataDetails.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "mau01_id" && c.ToString() != "mau01_chitiet_id").ToList();
            var datadangkyuynhiem = dataUyNhiems.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "mau01_id" && c.ToString() != "mau01_uynhiem_id" && c.ToString() != "thunhiem" && c.ToString() != "ket_qua").ToList();

            Dictionary<string, string> headerCTS = new Dictionary<string, string>();
            headerCTS.Add("stt", "STT");
            headerCTS.Add("ttchuc", "TTChuc");
            headerCTS.Add("seri", "Seri");
            headerCTS.Add("tngay", "TNgay");
            headerCTS.Add("dngay", "DNgay");
            headerCTS.Add("hthuc", "HThuc");

            Dictionary<string, string> headeruyNhiem = new Dictionary<string, string>();
            headeruyNhiem.Add("stt", "STT");
            headeruyNhiem.Add("tlhdunhiem", "TLHDon");
            headeruyNhiem.Add("khmhdon", "KHMSHDon");
            headeruyNhiem.Add("khhdunhiem", "KHHDon");
            headeruyNhiem.Add("mstunhiem", "MST");
            headeruyNhiem.Add("ttcdunhiem", "TTChuc");
            headeruyNhiem.Add("mdunhiem", "MDich");
            headeruyNhiem.Add("tngay", "TNgay");
            headeruyNhiem.Add("dngay", "DNgay");
            headeruyNhiem.Add("pttthdunhiem", "PThuc");


            //DateTime dt;
            //DateTime.TryParseExact(data.Rows[0]["nlap"].ToString(), "MM/dd/yyyy hh:mm:ss tt",
            //               CultureInfo.InvariantCulture, DateTimeStyles.None,
            //               out dt);
            String ngay_lap = DateTime.Parse(data.Rows[0]["nlap"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            XDocument doc = new XDocument();

            if (dataUyNhiems.Rows.Count == 0)
            {
                doc = new XDocument(
                   new XElement("TKhai",
                       new XElement("DLTKhai", new XAttribute("Id", "data"),
                           new XElement("TTChung",
                               new XElement("PBan", "2.0.0"),
                               new XElement("MSo", "01/ĐKTĐ-HĐĐT"),
                               new XElement("Ten", "Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử"),
                               GetDataRow("HThuc", data.Rows[0], "loai"),
                               GetDataRow("TNNT", data.Rows[0], "tnnt"),
                               GetDataRow("MST", data.Rows[0], "mst"),
                               GetDataRow("CQTQLy", data.Rows[0], "cqtqly"),
                               GetDataRow("MCQTQLy", data.Rows[0], "mcqtqly"),
                               GetDataRow("NLHe", data.Rows[0], "nlhe"),
                               GetDataRow("DCLHe", data.Rows[0], "dclhe"),
                               GetDataRow("DCTDTu", data.Rows[0], "dctdtu"),
                               GetDataRow("DTLHe", data.Rows[0], "dtlhe"),
                               GetDataRow("DDanh", data.Rows[0], "ddanh"),
                               new XElement("NLap", ngay_lap)
                           ),
                           new XElement("NDTKhai",
                               new XElement("HTHDon",
                                   GetDataRow("CMa", data.Rows[0], "adhdcma"),
                                   GetDataRow("KCMa", data.Rows[0], "adhdkma")
                               ),
                               new XElement("HTGDLHDDT",
                                    GetDataRow("NNTDBKKhan", data.Rows[0], "htgdlhddtcmkkhan"),
                                    GetDataRow("NNTKTDNUBND", data.Rows[0], "htgdlhddtcmkcncao"),
                                    GetDataRow("CDLTTDCQT", data.Rows[0], "htgdlhddtkmttiep"),
                                    GetDataRow("CDLQTCTN", data.Rows[0], "htgdlhddtkmtchuc")
                               ),
                               new XElement("PThuc",
                                    GetDataRow("CDDu", data.Rows[0], "ptcdlhdndthdon"),
                                    GetDataRow("CBTHop", data.Rows[0], "ptcdlhdbthop")
                               ),
                               new XElement("LHDSDung",
                                   GetDataRow("HDGTGT", data.Rows[0], "hdgtgt"),
                                   GetDataRow("HDBHang", data.Rows[0], "hdbhang"),
                                   GetDataRow("HDBTSCong", data.Rows[0], "hdbtscong"),
                                   GetDataRow("HDBHDTQGia", data.Rows[0], "hdbhdtqgia"),
                                   GetDataRow("HDKhac", data.Rows[0], "hdkhac"),
                                   GetDataRow("CTu", data.Rows[0], "qlnhdon")
                               ),
                               new XElement("DSCTSSDung",
                                   dataDetails.AsEnumerable().Select(row => new XElement("CTS",
                                        GetDataRow("STT", row, "stt"),
                                        GetDataRow("TTChuc", row, "ttchuc"),
                                        GetDataRow("Seri", row, "seri"),
                                        GetDataRowDateTime("TNgay", row, "tngay"),
                                        GetDataRowDateTime("DNgay", row, "dngay"),
                                        GetDataRow("HThuc", row, "hthuc")
                                        )
                                    )
                               )
                           )
                       ),
                       new XElement("DSCKS",
                           new XElement("NNT"),
                           new XElement("CCKSKhac")
                       )
                   )
               );
            }
            else
            {
                doc = new XDocument(
                   new XElement("TKhai",
                       new XElement("DLTKhai", new XAttribute("Id", "data"),
                           new XElement("TTChung",
                               new XElement("PBan", "2.0.0"),
                               new XElement("MSo", "01/ĐKTĐ-HĐĐT"),
                               new XElement("Ten", "Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử"),
                               GetDataRow("LDKUNhiem", data.Rows[0], "ldkunhiem"), // 1 đăng ký, 2 thay đổi
                               GetDataRow("TNNT", data.Rows[0], "tnnt"),
                               GetDataRow("MST", data.Rows[0], "mst"),
                               GetDataRow("CQTQLy", data.Rows[0], "cqtqly"),
                               GetDataRow("MCQTQLy", data.Rows[0], "mcqtqly"),
                               GetDataRow("NLHe", data.Rows[0], "nlhe"),
                               GetDataRow("DCLHe", data.Rows[0], "dclhe"),
                               GetDataRow("DCTDTu", data.Rows[0], "dctdtu"),
                                GetDataRow("DTLHe", data.Rows[0], "dtlhe"),
                                GetDataRow("DDanh", data.Rows[0], "ddanh"),
                               new XElement("NLap", ngay_lap)
                           ),
                           new XElement("NDTKhai",
                               new XElement("DSCTSSDung",
                                   dataDetails.AsEnumerable().Select(row => new XElement("CTS",
                                        GetDataRow("STT", row, "stt"),
                                        GetDataRow("TTChuc", row, "ttchuc"),
                                        GetDataRow("Seri", row, "seri"),
                                        GetDataRowDateTime("TNgay", row, "tngay"),
                                        GetDataRowDateTime("DNgay", row, "dngay"),
                                        GetDataRow("HThuc", row, "hthuc")
                                        )
                                    )
                               ),
                                new XElement("DSDKUNhiem",
                                   dataUyNhiems.AsEnumerable().Select(row => new XElement("DKUNhiem",
                                        GetDataRow("STT", row, "stt"),
                                        GetDataRow("TLHDon", row, "tlhdunhiem"),
                                        GetDataRow("KHMSHDon", row, "khmhdon"),
                                        GetDataRow("KHHDon", row, "khhdunhiem"),
                                        GetDataRow("MST", row, "mstunhiem"),
                                        GetDataRow("TTChuc", row, "ttcdunhiem"),
                                        GetDataRow("MDich", row, "mdunhiem"),
                                        GetDataRowDateTime("TNgay", row, "tngay"),
                                        GetDataRowDateTime("DNgay", row, "dngay"),
                                        GetDataRow("PThuc", row, "pttthdunhiem")
                                        )
                                    )
                               )
                           )
                       ),
                       new XElement("DSCKS",
                           new XElement("NNT"),
                           new XElement("CCKSKhac")
                       )
                   )
               );
            }


            string xml = string.Empty;
            var writerSettings = new XmlWriterSettings();
            writerSettings.OmitXmlDeclaration = true;

            using (var buffer = new StringWriter())
            using (var writer = XmlWriter.Create(buffer, writerSettings))
            {
                doc.Save(writer);
                writer.Flush();
                xml = buffer.ToString();
            }
            return xml;
        }
        public async Task<XElement> XmlDangkysudungNew(string id)
        {
            DataTable data = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='" + id + "'");
            DataTable dataDetails = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68_chitiet WHERE mau01_id ='" + id + "'");
            DataTable dataUyNhiems = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE mau01_id ='" + id + "'");

            var datadangkysdung = dataDetails.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "mau01_id" && c.ToString() != "mau01_chitiet_id").ToList();
            var datadangkyuynhiem = dataUyNhiems.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "mau01_id" && c.ToString() != "mau01_uynhiem_id" && c.ToString() != "thunhiem" && c.ToString() != "ket_qua").ToList();

            Dictionary<string, string> headerCTS = new Dictionary<string, string>();
            headerCTS.Add("stt", "STT");
            headerCTS.Add("ttchuc", "TTChuc");
            headerCTS.Add("seri", "Seri");
            headerCTS.Add("tngay", "TNgay");
            headerCTS.Add("dngay", "DNgay");
            headerCTS.Add("hthuc", "HThuc");

            Dictionary<string, string> headeruyNhiem = new Dictionary<string, string>();
            headeruyNhiem.Add("stt", "STT");
            headeruyNhiem.Add("tlhdunhiem", "TLHDon");
            headeruyNhiem.Add("khmhdon", "KHMSHDon");
            headeruyNhiem.Add("khhdunhiem", "KHHDon");
            headeruyNhiem.Add("mstunhiem", "MST");
            headeruyNhiem.Add("ttcdunhiem", "TTChuc");
            headeruyNhiem.Add("mdunhiem", "MDich");
            headeruyNhiem.Add("tngay", "TNgay");
            headeruyNhiem.Add("dngay", "DNgay");
            headeruyNhiem.Add("pttthdunhiem", "PThuc");

            String ngay_lap = DateTime.Parse(data.Rows[0]["nlap"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            //XDocument doc = new XDocument();
            XElement doc = null;

            if (dataUyNhiems.Rows.Count == 0)
            {
                doc =
                   new XElement("TKhai",
                       new XElement("DLTKhai", new XAttribute("Id", "data"),
                           new XElement("TTChung",
                               new XElement("PBan", "2.0.0"),
                               new XElement("MSo", "01/ĐKTĐ-HĐĐT"),
                               new XElement("Ten", "Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử"),
                               new XElement("HThuc", data.Rows[0]["loai"].ToString()), // 1 đăng ký, 2 thay đổi
                               new XElement("TNNT", data.Rows[0]["tnnt"].ToString()),
                               new XElement("MST", data.Rows[0]["mst"].ToString()),
                               new XElement("CQTQLy", data.Rows[0]["cqtqly"].ToString()),
                               new XElement("MCQTQLy", data.Rows[0]["mcqtqly"].ToString()),
                               new XElement("NLHe", data.Rows[0]["nlhe"].ToString()),
                               new XElement("DCLHe", data.Rows[0]["dclhe"].ToString()),
                               new XElement("DCTDTu", data.Rows[0]["dctdtu"].ToString()),
                               new XElement("DTLHe", data.Rows[0]["dtlhe"].ToString()),
                               new XElement("DDanh", data.Rows[0]["ddanh"].ToString()),
                               new XElement("NLap", ngay_lap)
                           ),
                           new XElement("NDTKhai",
                               new XElement("HTHDon",
                                   new XElement("CMa", data.Rows[0]["adhdcma"].ToString()),
                                   new XElement("KCMa", data.Rows[0]["adhdkma"].ToString())
                               ),
                               new XElement("HTGDLHDDT",
                                   new XElement("NNTDBKKhan", data.Rows[0]["htgdlhddtcmkkhan"].ToString()),
                                   new XElement("NNTKTDNUBND", data.Rows[0]["htgdlhddtcmkcncao"].ToString()),
                                   new XElement("CDLTTDCQT", data.Rows[0]["htgdlhddtkmttiep"].ToString()),
                                   new XElement("CDLQTCTN", data.Rows[0]["htgdlhddtkmtchuc"].ToString())
                               ),
                               new XElement("PThuc",
                                   new XElement("CDDu", data.Rows[0]["ptcdlhdndthdon"].ToString()),
                                   new XElement("CBTHop", data.Rows[0]["ptcdlhdbthop"].ToString())
                               ),
                               new XElement("LHDSDung",
                                   new XElement("HDGTGT", data.Rows[0]["hdgtgt"].ToString()),
                                   new XElement("HDBHang", data.Rows[0]["hdbhang"].ToString()),
                                   new XElement("HDBTSCong", data.Rows[0]["hdbtscong"].ToString()),
                                   new XElement("HDBHDTQGia", data.Rows[0]["hdbhdtqgia"].ToString()),
                                   new XElement("HDKhac", data.Rows[0]["hdkhac"].ToString()),
                                   new XElement("CTu", data.Rows[0]["qlnhdon"].ToString())
                               ),
                               new XElement("DSCTSSDung",
                                   dataDetails.AsEnumerable().Select(row => new XElement("CTS",
                                        datadangkysdung.Select(x => new XElement(headerCTS[x], row[x]))
                                        )
                                    )
                               )
                           )
                       ),
                       new XElement("DSCKS",
                           new XElement("NNT"),
                           new XElement("CCKSKhac")
                       )
               );
            }
            else
            {
                doc =
                   new XElement("TKhai",
                       new XElement("DLTKhai", new XAttribute("Id", "data"),
                           new XElement("TTChung",
                               new XElement("PBan", "2.0.0"),
                               new XElement("MSo", "01/ĐKTĐ-HĐĐT"),
                               new XElement("Ten", "Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử"),
                               new XElement("LDKUNhiem", data.Rows[0]["ldkunhiem"].ToString()), // 1 đăng ký, 2 thay đổi
                               new XElement("TNNT", data.Rows[0]["tnnt"].ToString()),
                               new XElement("MST", data.Rows[0]["mst"].ToString()),
                               new XElement("CQTQLy", data.Rows[0]["cqtqly"].ToString()),
                               new XElement("MCQTQLy", data.Rows[0]["mcqtqly"].ToString()),
                               new XElement("NLHe", data.Rows[0]["nlhe"].ToString()),
                               new XElement("DCLHe", data.Rows[0]["dclhe"].ToString()),
                               new XElement("DCTDTu", data.Rows[0]["dctdtu"].ToString()),
                               new XElement("DTLHe", data.Rows[0]["dtlhe"].ToString()),
                               new XElement("DDanh", data.Rows[0]["ddanh"].ToString()),
                               new XElement("NLap", ngay_lap)
                           ),
                           new XElement("NDTKhai",
                               new XElement("DSCTSSDung",
                                   dataDetails.AsEnumerable().Select(row => new XElement("CTS",
                                        datadangkysdung.Select(x => new XElement(headerCTS[x], row[x]))
                                        )
                                    )
                               ),
                                new XElement("DSDKUNhiem",
                                   dataUyNhiems.AsEnumerable().Select(row => new XElement("DKUNhiem",
                                        datadangkyuynhiem.Select(x => new XElement(headeruyNhiem[x], row[x]))
                                        )
                                    )
                               )
                           )
                       ),
                       new XElement("DSCKS",
                           new XElement("NNT"),
                           new XElement("CCKSKhac")
                       )
               );
            }

            return doc;
        }
        public async Task<bool> DangkysudungUyNhiem(string id)
        {
            bool result = false;
            DataTable dataUyNhiems = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE mau01_id ='" + id + "'");

            if (dataUyNhiems.Rows.Count > 0)
            {
                result = true;
            }

            return result;
        }

        public async Task<JObject> getDangkysudung()
        {
            JObject _res = new JObject();
            string sql = "SELECT * FROM #SCHEMA_NAME#.mau01_68 ORDER BY nlap DESC LIMIT 1";

            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql);
            if (tblData.Rows.Count > 0)
            {
                JArray ar = JArray.FromObject(tblData);
                _res = (JObject)ar[0];

                string qry = "SELECT * FROM #SCHEMA_NAME#.mau01_68_chitiet WHERE mau01_id = '" + tblData.Rows[0]["mau01_id"] + "'";
                DataTable tblData2 = await _minvoiceDbContext.GetDataTableAsync(qry);
                _res.Add("data", JArray.FromObject(tblData2));
            }
            return _res;
        }
        public async Task<JObject> SaveDangkysudung(JObject obj)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    masterObj["tnnt"] = masterObj["tnnt"]?.ToString().Trim();
                    masterObj["mst"] = masterObj["mst"]?.ToString().Trim();
                    masterObj["ddanh"] = masterObj["ddanh"]?.ToString().Trim();
                    masterObj["nlhe"] = masterObj["nlhe"]?.ToString().Trim();
                    masterObj["dtlhe"] = masterObj["dtlhe"]?.ToString().Trim();
                    masterObj["dclhe"] = masterObj["dclhe"]?.ToString().Trim();

                    if (editMode == "2")
                    {
                        id = Guid.Parse(masterObj["id"].ToString());
                        if (masterObj["mau01_id"] == null)
                        {
                            masterObj.Add("mau01_id", masterObj["id"]);
                        }
                        else
                        {
                            masterObj["mau01_id"] = masterObj["id"];
                        }
                    }
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {
                        if (masterObj["nlap"] != null)
                        {
                            masterObj["nlap"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (masterObj["mau01_id"] != null)
                        {
                            masterObj["mau01_id"] = id;
                        }
                        else
                        {
                            masterObj.Add("mau01_id", id);
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                    }

                    if (editMode == "3")
                    {
                        //if (masterObj["nglap"] != null)
                        //{
                        //    string user_new = masterObj["nglap"].ToString();

                        //    if (isdeluser != "C" && user_new != username)
                        //    {
                        //        json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                        //        return json;
                        //    }
                        //}
                    }
                    #endregion

                    string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_mau01_68_insert" : editMode == "2" ? "#SCHEMA_NAME#.crd_mau01_68_update" : "#SCHEMA_NAME#.crd_mau01_68_insert";

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    #region Chi tiết hóa đơn
                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.mau01_68_chitiet WHERE mau01_id = '" + masterObj["mau01_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details["mau01_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["mau01_id"].ToString()))
                                    {
                                        js_details["mau01_id"] = masterObj["mau01_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add("mau01_id", masterObj["mau01_id"]);
                                }

                                if (js_details["mau01_chitiet_id"] != null)
                                {
                                    js_details["mau01_chitiet_id"] = Guid.NewGuid();
                                }
                                else
                                {
                                    js_details.Add("mau01_chitiet_id", Guid.NewGuid());
                                }

                                //if (fieldStt_rec0 != null)
                                //{
                                //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                //}

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_mau01_68_chitiet_insert", CommandType.StoredProcedure, js_details);
                            }
                        }

                    }
                    #endregion

                    JArray uynhiems = null;

                    if (masterObj["uynhiems"] != null)
                    {
                        if (masterObj.SelectToken("uynhiems") is JArray)
                        {
                            uynhiems = (JArray)masterObj["uynhiems"];
                        }
                    }

                    #region Ủy nhiệm tờ khai
                    if (uynhiems != null)
                    {
                        foreach (var dtl in uynhiems)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE mau01_id = '" + masterObj["mau01_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details["mau01_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["mau01_id"].ToString()))
                                    {
                                        js_details["mau01_id"] = masterObj["mau01_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add("mau01_id", masterObj["mau01_id"]);
                                }

                                if (js_details["mau01_uynhiem_id"] != null)
                                {
                                    js_details["mau01_uynhiemt_id"] = Guid.NewGuid();
                                }
                                else
                                {
                                    js_details.Add("mau01_uynhiem_id", Guid.NewGuid());
                                }

                                //if (fieldStt_rec0 != null)
                                //{
                                //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                //}

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_mau01_68_uynhiem_insert", CommandType.StoredProcedure, js_details);
                            }
                        }

                    }
                    #endregion

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }

                // tạo xml update vào lại bảng
                string xml = await XmlDangkysudung(id.ToString());
                //xml = xml.Replace("'", "''");
                await _minvoiceDbContext.BeginTransactionAsync();
                string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau01_68 SET xml_send = @xml_send WHERE mau01_id = @mau01_id";
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("xml_send", xml);
                parameters2.Add("mau01_id", id);

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);
                await _minvoiceDbContext.TransactionCommitAsync();

                if (editMode != "3")
                {
                    //string select_cmd = $"SELECT mau01_id as id , * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='{id}'";
                    //DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);
                    //data = JArray.FromObject(tbl);
                    //json.Add("data", data[0]);
                }

                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        #endregion

        public async Task<JObject> SaveHuyhoadon(JObject obj)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];
                    if (editMode == "2")
                    {
                        // id = Guid.Parse(masterObj["mau04_id"].ToString());
                        if (masterObj["mau04_id"] == null)
                        {
                            masterObj.Add("mau04_id", masterObj["id"]);
                        }
                    }
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {

                        if (masterObj["ntbao"] != null)
                        {
                            masterObj["ntbao"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("ntbao", DateTime.Now);
                        }
                        if (masterObj["mau04_id"] != null)
                        {
                            masterObj["mau04_id"] = id;
                        }
                        else
                        {
                            masterObj.Add("mau04_id", id);
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }
                        if (masterObj["loai"] == null)
                        {
                            masterObj.Add("loai", 1);
                        }

                    }

                    if (editMode == "3")
                    {
                        //if (masterObj["nglap"] != null)
                        //{
                        //    string user_new = masterObj["nglap"].ToString();

                        //    if (isdeluser != "C" && user_new != username)
                        //    {
                        //        json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                        //        return json;
                        //    }
                        //}
                    }
                    #endregion

                    string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_mau04_68_insert" : editMode == "2" ? "#SCHEMA_NAME#.crd_mau04_68_update" : "#SCHEMA_NAME#.crd_mau04_68_update";

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    #region Chi tiết hóa đơn
                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id = '" + masterObj["mau04_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            if (obj_details != null)
                                foreach (var item in obj_details)
                                {
                                    JObject js_details = (JObject)item;
                                    // validate trước khi lưu
                                    if (js_details["khieu"] == null || string.IsNullOrEmpty(js_details["khieu"].ToString())
                                        || js_details["ngay"] == null || string.IsNullOrEmpty(js_details["ngay"].ToString())
                                        || js_details["shdon"] == null || string.IsNullOrEmpty(js_details["shdon"].ToString())
                                        || js_details["ladhddt"] == null || string.IsNullOrEmpty(js_details["ladhddt"].ToString())
                                        || js_details["tctbao"] == null || string.IsNullOrEmpty(js_details["tctbao"].ToString()))
                                    {
                                        throw new Exception("Ký hiệu, ngày hóa đơn, số hóa đơn, loại áp dụng hóa đơn và tính chất thông báo Không được để trống");
                                    }

                                    if (js_details["hdon_id"] == null && (js_details["mau_so"] == null || string.IsNullOrEmpty(js_details["mau_so"].ToString())))
                                    {
                                        throw new Exception("Mẫu số, Ký hiệu, Ngày hóa đơn, Số hóa đơn, Loại áp dụng hóa đơn và Tính chất thông báo Không được để trống");
                                    }

                                    if (js_details["hdon_id"] != null && js_details["hdon_id"].ToString() != string.Empty)
                                    {
                                        if (!string.IsNullOrEmpty(js_details["hdon_id"].ToString()))
                                        {
                                            string hoadon_id = js_details["hdon_id"].ToString();
                                            DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT tthai, tthdon FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hoadon_id + "'");
                                            if (dtHdon.Rows.Count > 0)
                                            {
                                                string tthaiHdOld = dtHdon.Rows[0]["tthai"]?.ToString();
                                                string tthdonHdOld = dtHdon.Rows[0]["tthdon"]?.ToString();

                                                if (((tthaiHdOld == "Đã cấp mã" || tthaiHdOld == "CQT đã nhận" || tthaiHdOld == "Chấp nhận TBSS" || tthaiHdOld == "Không chấp nhận TBSS" || tthaiHdOld == "TBSS sai định dạng" || tthaiHdOld == "Chờ phản hồi") && (tthdonHdOld == "0" || tthdonHdOld == "1"))
                                                    || ((tthaiHdOld == "Đã cấp mã" || tthaiHdOld == "CQT đã nhận" || tthaiHdOld == "Chấp nhận TBSS" || tthaiHdOld == "Không chấp nhận TBSS" || tthaiHdOld == "TBSS sai định dạng" || tthaiHdOld == "Chờ phản hồi") && tthdonHdOld == "11")
                                                    //|| ((tthaiHdOld == "Đã gửi" || tthaiHdOld == "Đã cấp mã") && (tthdonHdOld == "19" || tthdonHdOld == "21" || tthdonHdOld == "23"))
                                                    || ((tthaiHdOld == "Đã cấp mã" || tthaiHdOld == "CQT đã nhận" || tthaiHdOld == "Không chấp nhận TBSS" || tthaiHdOld == "TBSS sai định dạng" || tthaiHdOld == "Chờ phản hồi") && tthdonHdOld == "17")
                                                    || ((tthaiHdOld == "Đã cấp mã" || tthaiHdOld == "CQT đã nhận" || tthaiHdOld == "Chấp nhận TBSS" || tthaiHdOld == "Không chấp nhận TBSS" || tthaiHdOld == "TBSS sai định dạng" || tthaiHdOld == "Chờ phản hồi") && tthdonHdOld == "2")
                                                    || ((tthaiHdOld == "CQT đã nhận" || tthaiHdOld == "Chờ phản hồi") && tthdonHdOld == "3"))
                                                {
                                                    bool checkHD = true;
                                                }
                                                else
                                                {
                                                    throw new Exception("Tồn tại hóa đơn không thể thực hiện chức năng này!");
                                                }
                                            }
                                            else
                                            {
                                                throw new Exception("Không tồn tại hóa đơn theo thông 78!");
                                            }
                                        }
                                    }

                                    if (js_details["mau04_id"] != null)
                                    {
                                        if (string.IsNullOrEmpty(js_details["mau04_id"].ToString()))
                                        {
                                            js_details["mau04_id"] = masterObj["mau04_id"];
                                        }
                                    }
                                    else
                                    {
                                        js_details.Add("mau04_id", masterObj["mau04_id"]);
                                    }

                                    if (js_details["mau04_chitiet_id"] != null)
                                    {
                                        js_details["mau04_chitiet_id"] = Guid.NewGuid();
                                    }
                                    else
                                    {
                                        js_details.Add("mau04_chitiet_id", Guid.NewGuid());
                                    }

                                    //if (fieldStt_rec0 != null)
                                    //{
                                    //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                    //}

                                    if (js_details.ContainsKey("mau_so") && ((js_details["mau_so"] != null ||
                                        !string.IsNullOrEmpty(js_details["mau_so"].ToString()))))
                                    {
                                        js_details["khieu"] = js_details["mau_so"] + ";" + js_details["khieu"];
                                    }

                                    await this._minvoiceDbContext.TransactionCommandAsync(
                                        "#SCHEMA_NAME#.crd_mau04_68_chitiet_insert", CommandType.StoredProcedure,
                                        js_details);
                                }
                        }

                    }
                    #endregion

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }

                // tạo xml update vào lại bảng
                JObject dataObj = (JObject)data[0];
                var mau04Id = dataObj["mau04_id"].ToString();
                string xml = await XmlHuyhoadonMau04(mau04Id);
                //xml = xml.Replace("'", "''");
                await _minvoiceDbContext.BeginTransactionAsync();
                string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau04_68 SET xml_send = @xml_send WHERE mau04_id = @mau04_id";
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("xml_send", xml);
                parameters2.Add("mau04_id", Guid.Parse(mau04Id));

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                parameters2.Clear();
                parameters2.Add("mau04_id", Guid.Parse(mau04Id));
                //parameters2.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                //parameters2.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                //parameters2.Add("ladhddt", Convert.ToInt32(hoadon.SelectSingleNode("LADHDDT")?.InnerText));

                string sqlSelect_mau04chitiet = $"SELECT hdon_id FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                   + " WHERE mau04_id = @mau04_id";
                DataTable mau04_chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04chitiet, CommandType.Text, parameters2);

                string hdon_id = "";
                if (mau04_chitiet_Table.Rows.Count > 0)
                {
                    hdon_id = mau04_chitiet_Table.Rows[0]["hdon_id"].ToString();
                }

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                if (editMode != "3")
                {
                    //string select_cmd = $"SELECT mau01_id as id , * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='{id}'";
                    //DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);
                    //data = JArray.FromObject(tbl);
                    //json.Add("data", data[0]);
                }
                if (string.IsNullOrEmpty(hdon_id.ToString()))
                {
                    json.Add("id", mau04Id);
                    // json.Add("hdon_id", hdon_id);
                    json.Add("ok", "true");
                }
                else
                {
                    json.Add("id", id);
                    json.Add("hdon_id", Guid.Parse(hdon_id));
                    json.Add("ok", "true");
                }

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> SaveHuyhoadonPretreatment(JObject obj)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {

                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                foreach (JProperty jp in js_details.Properties().ToList())
                                {
                                    if (jp.Name == "invoice_number")
                                    {
                                        jp.Replace(new JProperty("shdon", jp.Value));
                                    }
                                    if (jp.Name == "invoice_series")
                                    {
                                        jp.Replace(new JProperty("khieu", jp.Value));
                                    }
                                    if (jp.Name == "invoice_issued_date")
                                    {
                                        jp.Replace(new JProperty("ngay", jp.Value));
                                    }
                                    if (jp.Name == "inv_invoiceauth_id")
                                    {
                                        jp.Replace(new JProperty("hdon_id", jp.Value));
                                    }
                                    if (jp.Name == "branch_code")
                                    {
                                        jp.Replace(new JProperty("mdvi", jp.Value));
                                    }
                                    if (jp.Name == "buyer_taxcode")
                                    {
                                        jp.Replace(new JProperty("mst", jp.Value));
                                    }
                                }

                            }

                            //dtl["data"] = obj_details;
                        }

                    }
                    json = await SaveHuyhoadon(obj);

                }

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<string> XmlHuyhoadonMau04(string id)
        {
            DataTable table = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id ='" + id + "'");
            DataTable dataDetails = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id ='" + id + "'");

            String ntbao = DateTime.Parse(table.Rows[0]["ntbao"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            // String ngay = DateTime.Parse(dataDetails.Rows[0]["ngay"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            var doc = new XDocument(
                new XElement("TBao",
                    new XElement("DLTBao", new XAttribute("Id", "data"),
                        new XElement("PBan", "2.0.0"),
                        new XElement("MSo", "04/SS-HĐĐT"),
                        new XElement("Ten", "Thông báo hóa đơn điện tử có sai sót"),
                        GetDataRow("Loai", table.Rows[0], "loai"),
                        GetDataRow("So", table.Rows[0], "so"),
                        GetDataRowDate("NTBCCQT", table.Rows[0], "ntbccqt"),
                        GetDataRow("MCQT", table.Rows[0], "mcqt"),
                        GetDataRow("TCQT", table.Rows[0], "tcqt"),
                        GetDataRow("TNNT", table.Rows[0], "tnnt"),
                        GetDataRow("MST", table.Rows[0], "mst"),
                        GetDataRow("MDVQHNSach", table.Rows[0], "mdvqhnsach"),
                        GetDataRow("DDanh", table.Rows[0], "ddanh"),
                        new XElement("NTBao", ntbao),
                        new XElement("DSHDon",
                            from row in dataDetails.AsEnumerable()
                            select new XElement("HDon",
                                GetDataRow("STT", row, "stt"),
                                GetDataRow("MCQTCap", row, "mcqtcap"),
                                new XElement("KHMSHDon", row["ladhddt"].ToString() == "1" ? row["khieu"].ToString().Substring(0, 1) : row["khieu"].ToString().Split(';').GetValue(0)),
                                new XElement("KHHDon", row["ladhddt"].ToString() == "1" ? row["khieu"].ToString().Substring(1) : row["khieu"].ToString().Split(';').GetValue(1)),
                                GetDataRow("SHDon", row, "shdon"),
                                new XElement("Ngay", row.Table.Columns.Contains("ngay") && !string.IsNullOrEmpty(row["ngay"].ToString()) ? row.Field<DateTime>("ngay").ToString("yyyy-MM-dd") : null),
                                // new XElement("Ngay", ngay),
                                GetDataRow("LADHDDT", row, "ladhddt"),
                                //new XElement("TCTBao", row["tctbao"].ToString())
                                new XElement("TCTBao", (row["tctbao"].ToString() == "5" || row["tctbao"].ToString() == "6" || row["tctbao"].ToString() == "7" || row["tctbao"].ToString() == "8") ? "2" : row["tctbao"].ToString()),
                                GetDataRow("LDo", row, "ldo")
                            )
                        )
                    ),
                    new XElement("DSCKS",
                        new XElement("NNT"),
                        new XElement("CCKSKhac")
                    )
                )
            );

            string xml = string.Empty;
            var writerSettings = new XmlWriterSettings();
            writerSettings.OmitXmlDeclaration = true;

            using (var buffer = new StringWriter())
            using (var writer = XmlWriter.Create(buffer, writerSettings))
            {
                doc.Save(writer);
                writer.Flush();
                xml = buffer.ToString();
            }
            return xml;
        }
        public async Task<XElement> XmlHuyhoadonMau04New(string id)
        {
            DataTable table = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id ='" + id + "'");
            DataTable dataDetails = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id ='" + id + "'");

            String ntbao = DateTime.Parse(table.Rows[0]["ntbao"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String ngay = DateTime.Parse(dataDetails.Rows[0]["ngay"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            XElement doc =
                new XElement("TBao",
                    new XElement("DLTBao", new XAttribute("Id", "data"),
                        new XElement("PBan", "2.0.0"),
                        new XElement("MSo", "04/SS-HĐĐT"),
                        new XElement("Ten", "Thông báo hóa đơn điện tử có sai sót"),
                        new XElement("Loai", table.Rows[0]["loai"].ToString()),
                        new XElement("So", table.Rows[0]["so"]?.ToString()),
                        new XElement("NTBCCQT", table.Rows[0]["ntbccqt"]?.ToString()),
                        new XElement("MCQT", table.Rows[0]["mcqt"]?.ToString()),
                        new XElement("TCQT", table.Rows[0]["tcqt"].ToString()),
                        new XElement("TNNT", table.Rows[0]["tnnt"].ToString()),
                        new XElement("MST", table.Rows[0]["mst"].ToString()),
                        new XElement("DDanh", table.Rows[0]["ddanh"].ToString()),
                        new XElement("NTBao", ntbao),
                        new XElement("DSHDon",
                            from row in dataDetails.AsEnumerable()
                            select new XElement("HDon",
                                new XElement("STT", row["stt"].ToString()),
                                new XElement("MCQTCap", row["mcqtcap"].ToString()),
                                new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                new XElement("SHDon", row["shdon"].ToString()),
                                new XElement("Ngay", ngay),
                                new XElement("LADHDDT", row["ladhddt"].ToString()),
                                //new XElement("TCTBao", row["tctbao"].ToString())
                                new XElement("TCTBao", (row["tctbao"].ToString() == "5" || row["tctbao"].ToString() == "6" || row["tctbao"].ToString() == "7" || row["tctbao"].ToString() == "8") ? "2" : row["tctbao"].ToString()),
                                new XElement("LDo", row["ldo"].ToString())
                            )
                        )
                    ),
                    new XElement("DSCKS",
                        new XElement("NNT"),
                        new XElement("CCKSKhac")
                    )
            );

            return doc;
        }
        public async Task<JObject> SignHuyhoadon(JObject model, string id, string hdon_id, string tthdon)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();
                string check_HSM = model["check_HSM"] != null ? model["check_HSM"].ToString() : "";


                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                if (!string.IsNullOrEmpty(check_HSM) && tblUser.Rows[0]["is_sign_invoice_hsm"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn bằng HSM");
                    return res;
                }
                // validate trước khi ký
                if (hdon_id != string.Empty)
                {
                    DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT tthai FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hdon_id + "'");
                    if (dtHdon.Rows.Count > 0)
                    {
                        if (dtHdon.Rows[0]["tthai"].ToString() == "Chờ gửi" || dtHdon.Rows[0]["tthai"].ToString() == "Chờ ký" || dtHdon.Rows[0]["tthai"].ToString() == "Đã ký")
                        {
                            res.Add("error", "Hóa đơn chưa gửi không thể thông báo sai sót");
                            return res;
                        }
                    }
                }

                X509Certificate2 certFile = null;

                string mst = await getMstDv();
                //if (!certFile.Subject.ToString().Contains(mst))
                //{
                //    res.Add("error", "Mã số thuế chữ ký số không khớp với đơn vị ");
                //    return res;
                //}
                await _minvoiceDbContext.BeginTransactionAsync();

                DataTable mau04 = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id ='" + id + "'");
                //string xml = mau04.Rows[0]["xml_send"].ToString();
                //if (string.IsNullOrEmpty(xml))
                //{
                string xml = await XmlHuyhoadonMau04(id.ToString());
                //xml = xml.Replace("'", "''");

                string tenCty = await getTenDv();
                if (string.IsNullOrEmpty(check_HSM))
                {
                    //Guid mau01_id = Guid.Parse(model["mau01_id"].ToString());
                    string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                    string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                            + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                            + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                            + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                    if (branch_code.Length > 0)
                    {
                        sql += " AND a.branch_code='" + branch_code + "' ";
                    }

                    if (cert_serial.Length > 0)
                    {
                        sql += " AND a.cer_serial='" + cert_serial + "'";
                    }

                    sql += " ORDER BY end_date desc LIMIT 1";

                    DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                    if (tblCertificate.Rows.Count == 0)
                    {
                        res.Add("error", "Không tìm thấy chứng thư số");
                    }

                    try
                    {
                        string pass = tblCertificate.Rows[0]["pass"].ToString();
                        byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                        certFile = new X509Certificate2(bytes, pass);
                    }
                    catch (Exception)
                    {
                    }

                    if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                    {
                        xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                        SignXML input = new SignXML()
                        {
                            msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                            xmlData = xml,
                            namespacePrefix = "",
                            reason = "Ky thong bao hoa don sai sot",
                            signatureTagId = "seller",
                            signingTagId = "data",
                            signingTagName = "NNT",
                            prompt = "Ky thong bao hoa don sai sot"
                        };
                        SignedXmlReturn resultSign = CommonManager.SignSimPKI(input);
                        //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                        xml = resultSign.xmlSignedData;
                    }
                    else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                    {
                        xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NNT", "xml",
                            xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                    }
                    else
                    {
                        xml = CommonManager.SignXml(tenCty, "#data", "seller", xml, certFile, "NNT");
                    }
                }
                else
                {
                    Dictionary<string, object> dicParam = new Dictionary<string, object>();
                    string userId = tblUser.Rows[0]["wb_user_id"].ToString();
                    dicParam.Add("wb_user_id", Guid.Parse(userId));

                    DataTable tblUser_Hsm = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id", CommandType.Text, dicParam);

                    if (tblUser_Hsm.Rows.Count == 0)
                    {
                        //res.Add("error", "Chưa có tài khoản cấu hình");
                        //return res;
                        throw new Exception("Chưa có tài khoản cấu hình");
                    }
                    // lấy account hsm portal
                    string username_hsm = tblUser_Hsm.Rows[0]["username"].ToString();
                    string password_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password"].ToString()));
                    string password2_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password_level2"].ToString()));
                    string ma_dvcs_hsm = tblUser_Hsm.Rows[0]["ma_dvcs"].ToString();

                    // call api login
                    HttpClient client = new HttpClient();

                    var api = CommonConstants.LOGIN;
                    JObject user = new JObject();
                    user.Add("username", username_hsm);
                    user.Add("password", password_hsm);
                    user.Add("ma_dvcs", ma_dvcs_hsm);

                    var token = "";
                    var resLogin = executeLogin(CommonConstants.BASE_HSM_URL, api, user).Result;

                    // kiểm tra nếu login lỗi thì thông báo
                    if (resLogin["error"] != null)
                    {
                        //res.Add("error", resLogin["error"].ToString());
                        //return res;
                        throw new Exception(resLogin["error"].ToString());
                    }
                    else
                    {
                        token = resLogin["token"].ToString();
                        token = token + ";" + ma_dvcs_hsm;
                    }

                    XmlDocument document = new XmlDocument();
                    document.PreserveWhitespace = false;
                    //xml = xml.Replace("'", "''");
                    xml = xml.Replace("\r", "").Replace("\n", "");
                    document.LoadXml(xml);
                    XmlNode nodeTT = document.SelectSingleNode("/HDon/DLHDon/TTKhac");

                    if (nodeTT != null)
                    {
                        XmlElement TTin1 = document.CreateElement("TTin");
                        XmlElement TTruong1 = document.CreateElement("TTruong");
                        TTruong1.InnerText = "DVKy";
                        XmlElement KDLieu1 = document.CreateElement("KDLieu");
                        KDLieu1.InnerText = "string";
                        XmlElement DLieu1 = document.CreateElement("DLieu");
                        DLieu1.InnerText = tenCty;
                        TTin1.AppendChild(TTruong1);
                        TTin1.AppendChild(KDLieu1);
                        TTin1.AppendChild(DLieu1);

                        XmlElement TTin2 = document.CreateElement("TTin");
                        XmlElement TTruong2 = document.CreateElement("TTruong");
                        TTruong2.InnerText = "NKy";
                        XmlElement KDLieu2 = document.CreateElement("KDLieu");
                        KDLieu2.InnerText = "date";
                        XmlElement DLieu2 = document.CreateElement("DLieu");
                        DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                        TTin2.AppendChild(TTruong2);
                        TTin2.AppendChild(KDLieu2);
                        TTin2.AppendChild(DLieu2);


                        nodeTT.AppendChild(TTin1);
                        nodeTT.AppendChild(TTin2);
                    }

                    xml = document.OuterXml;
                    // Call Sign HSM API
                    JObject result = new JObject();

                    api = CommonConstants.SIGN_HSM_URL_78;
                    JObject json = new JObject();
                    json.Add("password2", password2_hsm);
                    json.Add("idData", "#data");
                    json.Add("idSigner", "seller");
                    json.Add("tagSign", "NNT");
                    json.Add("dataType", "xml");
                    json.Add("xml", xml);

                    var resSign = executePost(CommonConstants.BASE_HSM_URL, api, token, json).Result;

                    if ("false".Equals(resSign["status"].ToString().ToLower()))
                    {
                        //res.Add("error", resSign["message"].ToString());
                        //return res;
                        throw new Exception(resSign["message"].ToString());
                    }
                    else
                    {
                        xml = resSign["data"].ToString();
                    }
                    //xml = xml.Replace("'", "''");
                }

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.mau04_68 SET xml_send = @xml_send, trang_thai = @trang_thai, ngay_gui = @ngay_gui, nguoi_gui = @nguoi_gui WHERE mau04_id = @mau04_id";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("mau04_id", Guid.Parse(id));
                parameters.Add("xml_send", xml);
                parameters.Add("ngay_gui", DateTime.Now);
                parameters.Add("nguoi_gui", _webHelper.GetUser());
                parameters.Add("trang_thai", "1");
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters);

                DataTable mau04ct = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id ='" + mau04.Rows[0]["mau04_id"] + "'");
                //foreach (DataRow dr in mau04ct.Rows)
                //{
                //    //string ud = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthdon = @tthdon WHERE hdon_id = @hdon_id";

                //    Dictionary<string, object> para = new Dictionary<string, object>();
                //    string tctbao = dr["tctbao"].ToString();
                //    string ud = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthdon = (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END), \n"
                //            + "   tthdon_old = (CASE WHEN @tctbao = '1' THEN tthdon WHEN @tctbao = '4' THEN tthdon ELSE tthdon_old END) WHERE hdon_id = @hdon_id";

                //    //int tctbao = dr["tctbao"].ToString() == "5" || dr["tctbao"].ToString() == "6" || dr["tctbao"].ToString() == "7" || dr["tctbao"].ToString() == "8" ? 7
                //    //    : (dr["tctbao"].ToString() == "1" ? 13 : (dr["tctbao"].ToString() == "3" ? 15 : 1));

                //    para.Add("tctbao", tctbao);
                //    para.Add("hdon_id", dr["hdon_id"]);
                //    await _minvoiceDbContext.TransactionCommandAsync(ud, CommandType.Text, para);
                //}

                // lấy thông tin xml TTChung của Thông điệp
                string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_HOA_DON_SAI_SOT, 1);

                // lấy thông tin xml dữ liệu
                //XElement xml_DuLieu = await XmlHuyhoadonMau04New(id.ToString());

                // đẩy dữ liệu hóa đơn vào thẻ DLieu
                XElement dLieu = new XElement("DLieu");
                dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlThongDiep.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                //xml = xmlThongDiep.ToString();

                // ký thông điệp gửi lên thuế
                //xml = CommonManager.SignThongDiep(xml, certFile, pass);

                // đóng gói xml gửi lên TVAN
                //string cmd = "300";

                string data = await ThongDiepGui(CommonConstants.MLTDIEP_HOA_DON_SAI_SOT, xml);

                // QuyenNH: update trạng thái hóa đơn chờ phản hồi CQT
                foreach (DataRow dr in mau04ct.Rows)
                {
                    string ud = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai WHERE hdon_id = @hdon_id";
                    Dictionary<string, object> para = new Dictionary<string, object>();
                    para.Add("tthai", "Chờ phản hồi");
                    para.Add("hdon_id", dr["hdon_id"]);
                    await _minvoiceDbContext.TransactionCommandAsync(ud, CommandType.Text, para);
                }

                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                // lưu vào bảng tonghop_gui_tvan_68
                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                param_Insert.Add("id", Guid.NewGuid());
                param_Insert.Add("mltdiep_gui", CommonConstants.MLTDIEP_HOA_DON_SAI_SOT);
                param_Insert.Add("xml_tdiep_gui", xml);
                param_Insert.Add("type_id", Guid.Parse(id));
                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                param_Insert.Add("note", "app");
                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                // update mã thông điệp vào bảng hóa đơn
                if (hdon_id == string.Empty)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    // update vào bảng mau04_68
                    string sqlSelect_mau04_chitiet = $"SELECT * FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id = '" + id + "'";
                    DataTable mau04Chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04_chitiet, CommandType.Text, param);
                    string mau04_id = "";
                    if (mau04Chitiet_Table.Rows.Count > 0)
                    {
                        mau04_id = mau04Chitiet_Table.Rows[0]["mau04_id"].ToString();
                    }

                    string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.mau04_68 SET mtdiep_gui=@mtdiep_gui WHERE mau04_id = '" + Guid.Parse(mau04_id) + "'";
                    //param.Add("mau04_id", Guid.Parse(mau04_id));
                    //await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                else
                {

                    string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui = @mtdiep_gui WHERE hdon_id = @hdon_id";

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("hdon_id", Guid.Parse(hdon_id));
                    param.Add("mtdiep_gui", mtdiep_gui);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param);

                    // update vào bảng mau04_68
                    string sqlSelect_mau04_chitiet = $"SELECT * FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE hdon_id = @hdon_id";
                    DataTable mau04Chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04_chitiet, CommandType.Text, param);
                    string mau04_id = "";
                    if (mau04Chitiet_Table.Rows.Count > 0)
                    {
                        mau04_id = mau04Chitiet_Table.Rows[0]["mau04_id"].ToString();
                    }

                    sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.mau04_68 SET mtdiep_gui = @mtdiep_gui WHERE mau04_id = '" + mau04_id + "'";
                    param.Add("mau04_id", Guid.Parse(mau04_id));
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }
                // đẩy queue lên TVAN
                _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());

                //_kafkaService.PublishTopic(_topicVendorSendToVan, data);
                res.Add("ok", true);
                res.Add("trang_thai", "Gửi thành công đến cơ quan thuế");

            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }
        public async Task<JObject> XmlMau0405(string id)
        {
            JObject _res = new JObject();
            try
            {
                var idFormatCheck = Guid.TryParse(id, out var idMau0405);
                if (idFormatCheck == false)
                {
                    throw new Exception("Id truyền vào không đúng định dạng");
                }

                string xml;
                DataTable mau04 = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id ='" + idMau0405 + "'");
                if (mau04.Rows.Count > 0)
                {
                    xml = mau04.Rows[0]["xml_send"].ToString();
                    if (string.IsNullOrEmpty(xml))
                    {
                        xml = await XmlHuyhoadonMau04(id);
                        //xml = xml.Replace("'", "''");
                    }
                }
                else
                {
                    throw new Exception("Không tồn tại mẫu thông báo sai sót");
                }
                _res.Add("xml", xml);
                _res.Add("id", id);
                _res.Add("dvky", await getTenDv());
            }
            catch (Exception ex)
            {
                _res.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return _res;
        }

        public async Task<JObject> SaveXmlMau0405(JObject model)
        {
            JObject _res = new JObject();
            try
            {
                if (!model.ContainsKey("xml") || string.IsNullOrEmpty(model["xml"].ToString()))
                {
                    throw new Exception("Xml không được bỏ trống");
                }

                if (!model.ContainsKey("id") || string.IsNullOrEmpty(model["id"].ToString()))
                {
                    throw new Exception("Id không được bỏ trống");
                }

                string xml = model["xml"].ToString();
                // string id = model["id"].ToString();
                var idFormatCheck = Guid.TryParse(model["id"].ToString(), out var idMau0405);
                if (idFormatCheck == false)
                {
                    throw new Exception("Id truyền vào không đúng định dạng");
                }
                // string tthdon = model["tthdon"].ToString(); // 3= hủy , 1 = giải trình

                await _minvoiceDbContext.BeginTransactionAsync();

                DataTable mau04 = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68 WHERE mau04_id ='" + idMau0405 + "'");

                if (mau04.Rows.Count == 0)
                {
                    throw new Exception("Không tồn tại mẫu thông báo sai sót");
                }

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.mau04_68 SET xml_send = @xml_send, trang_thai = @trang_thai, ngay_gui = @ngay_gui, nguoi_gui = @nguoi_gui WHERE mau04_id = @mau04_id";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("mau04_id", idMau0405);
                parameters.Add("xml_send", xml);
                parameters.Add("ngay_gui", DateTime.Now);
                parameters.Add("nguoi_gui", _webHelper.GetUser());
                parameters.Add("trang_thai", "1");

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters);

                DataTable mau04ct = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id ='" + mau04.Rows[0]["mau04_id"] + "'");
                //foreach (DataRow dr in mau04ct.Rows)
                //{
                //    if (dr["hdon_id"] != null || !string.IsNullOrEmpty(dr["hdon_id"].ToString()))
                //    {
                //        Dictionary<string, object> para = new Dictionary<string, object>();
                //        string tctbao = dr["tctbao"].ToString();
                //        string ud = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthdon = (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END), \n"
                //               + "   tthdon_old = (CASE WHEN @tctbao = '1' THEN tthdon WHEN @tctbao = '4' THEN tthdon ELSE tthdon_old END) WHERE hdon_id = @hdon_id";

                //        para.Add("tctbao", tctbao);
                //        para.Add("hdon_id", dr["hdon_id"]);
                //        await _minvoiceDbContext.TransactionCommandAsync(ud, CommandType.Text, para);
                //    }
                //}

                // lấy thông tin xml TTChung của Thông điệp
                string mst = await getMstDv();
                string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_HOA_DON_SAI_SOT, 1);

                // lấy thông tin xml dữ liệu
                //XElement xml_DuLieu = await XmlHuyhoadonMau04New(id.ToString());

                // đẩy dữ liệu hóa đơn vào thẻ DLieu
                XElement dLieu = new XElement("DLieu");
                dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlThongDiep.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                // đóng gói xml gửi lên TVAN
                //string cmd = "300";

                string data = await ThongDiepGui(CommonConstants.MLTDIEP_HOA_DON_SAI_SOT, xml);

                // QuyenNH: update trạng thái hóa đơn chờ phản hồi CQT
                foreach (DataRow dr in mau04ct.Rows)
                {
                    if (dr["hdon_id"] != null || !string.IsNullOrEmpty(dr["hdon_id"].ToString()))
                    {
                        string ud = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai WHERE hdon_id = @hdon_id";
                        Dictionary<string, object> para = new Dictionary<string, object>();
                        para.Add("tthai", "Chờ phản hồi");
                        para.Add("hdon_id", dr["hdon_id"]);
                        await _minvoiceDbContext.TransactionCommandAsync(ud, CommandType.Text, para);
                    }
                }

                //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());

                // lưu vào bảng tonghop_gui_tvan_68

                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                param_Insert.Add("id", Guid.NewGuid());
                param_Insert.Add("mltdiep_gui", CommonConstants.MLTDIEP_HOA_DON_SAI_SOT);
                param_Insert.Add("xml_tdiep_gui", xml);
                param_Insert.Add("type_id", idMau0405);
                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                param_Insert.Add("note", "app");
                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                // lấy giá trị hdon_id từ mau04_id

                //Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                //parameters2.Add("mau04_id", Guid.Parse(model["id"].ToString()));
                //string sqlSelect_mau04_ct = $"SELECT hdon_id FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                //                                        + " WHERE mau04_id = @mau04_id";
                //DataTable dtmau04_ct = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04_ct, CommandType.Text, parameters2);
                //string hdon_id = "";
                //if (dtmau04_ct.Rows.Count > 0)
                //{
                //    hdon_id = dtmau04_ct.Rows[0]["hdon_id"].ToString();
                //}

                Dictionary<string, object> param = new Dictionary<string, object>();

                foreach (DataRow dr in mau04ct.Rows)
                {
                    // update mã thông điệp vào bảng hóa đơn
                    if (dr["hdon_id"] != null || !string.IsNullOrEmpty(dr["hdon_id"].ToString()))
                    {
                        string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                        param.Clear();
                        param.Add("hdon_id", dr["hdon_id"]);
                        param.Add("mtdiep_gui", mtdiep_gui);

                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param);
                    }

                }

                // update vào bảng mau04_68
                //string sqlSelect_mau04_chitiet = $"SELECT * FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE hdon_id = @hdon_id";
                //DataTable mau04Chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04_chitiet, CommandType.Text, param);
                //string mau04_id = "";
                //if (mau04Chitiet_Table.Rows.Count > 0)
                //{
                //    mau04_id = mau04Chitiet_Table.Rows[0]["mau04_id"].ToString();
                //}

                string sqlUpdate2 = $"UPDATE #SCHEMA_NAME#.mau04_68 SET mtdiep_gui=@mtdiep_gui WHERE mau04_id = @mau04_id";
                param.Clear();
                param.Add("mau04_id", idMau0405);
                param.Add("mtdiep_gui", mtdiep_gui);
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate2, CommandType.Text, param);

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                // đẩy queue lên TVAN
                _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                _res.Add("ok", true);
                _res.Add("status", "Đã gửi lên cơ quan thuế");

            }
            catch (Exception ex)
            {
                _res.Add("error", ex.Message);
                Log.Error(ex.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }
            return _res;
        }
        public async Task<JObject> SaveHuyHoadonTaomoi(JObject model, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                //await _minvoiceDbContext.BeginTransactionAsync();
                #region Save hóa đơn
                JObject obj = (JObject)model;
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];
                    if (!ValidInvoice.InsertValid(masterObj, ref json))
                    {
                        return json;
                    }
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {
                        if (masterObj["tthdon"] == null)
                        {
                            masterObj.Add("tthdon", 0);
                        }

                        if (masterObj["shdon"] != null)
                        {
                            masterObj["shdon"] = null;
                        }
                        else
                        {
                            masterObj.Add("shdon", null);
                        }

                        if (masterObj["tthai"] != null)
                        {
                            masterObj["tthai"] = "Chờ ký";
                        }
                        else
                        {
                            masterObj.Add("tthai", "Chờ ký");
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                        if (masterObj["nglap"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["nglap"].ToString()))
                            {
                                masterObj["nglap"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("nglap", username);
                        }

                        if (masterObj["nlap"] != null)
                        {
                            masterObj["nlap"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (masterObj["hdon_id"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["hdon_id"].ToString()))
                            {
                                masterObj["hdon_id"] = id;
                            }
                            else
                            {
                                id = Guid.Parse(masterObj["hdon_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add("hdon_id", id);
                        }
                    }

                    if (editMode == "2")
                    {

                        if (masterObj["ngsua"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["ngsua"].ToString()))
                            {
                                masterObj["ngsua"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("ngsua", username);
                        }

                        if (masterObj["nsua"] != null)
                        {
                            masterObj["nsua"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nsua", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj["hdon_id"].ToString());
                    }

                    if (editMode == "3")
                    {
                    }
                    #endregion

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }
                    if (editMode != "1" && editMode != "2")
                    {
                        string sql = "#SCHEMA_NAME#.crd_hdon68_delete";
                        await _minvoiceDbContext.BeginTransactionAsync();
                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        await this._minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                    }
                    else if (details != null && details.Count > 0 && ((JArray)(details[0]["data"])).Count > 0)
                    {
                        string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_hdon68_insert_n" : editMode == "2" ? "#SCHEMA_NAME#.crd_hdon68_update_n" : "#SCHEMA_NAME#.crd_hdon68_delete";

                        await _minvoiceDbContext.BeginTransactionAsync();
                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        if (editMode == "1" || editMode == "2")
                        {
                            sql = "#SCHEMA_NAME#.crd_hdon68_update_pt";
                            //await _minvoiceDbContext.BeginTransactionAsync();
                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        }


                        #region Chi tiết hóa đơn

                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details["hdon_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["hdon_id"].ToString()))
                                    {
                                        js_details["hdon_id"] = masterObj["hdon_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add("hdon_id", masterObj["hdon_id"]);
                                }

                                if (js_details["cthdon_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["cthdon_id"].ToString()))
                                    {
                                        js_details["cthdon_id"] = Guid.NewGuid();
                                    }
                                }
                                else
                                {
                                    js_details.Add("cthdon_id", Guid.NewGuid());
                                }

                                //if (fieldStt_rec0 != null)
                                //{
                                //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                //}

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_chitiet_insert", CommandType.StoredProcedure, js_details);
                            }
                        }
                        #endregion
                        #region hatm luuthuephi
                        JArray phi = null;

                        if (masterObj["hoadon68_phi"] != null)
                        {
                            if (masterObj.SelectToken("hoadon68_phi") is JArray)
                            {
                                phi = (JArray)masterObj["hoadon68_phi"];
                            }
                        }

                        if (phi != null)
                        {
                            foreach (var dtl in phi)
                            {
                                JArray obj_phi = (JArray)dtl["data"];
                                if (obj_phi.Count > 0)
                                {
                                    sql = "DELETE FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                                    if (obj_phi.Count > 0)
                                    {
                                        for (int j = 0; j < obj_phi.Count; j++)
                                        {
                                            JObject js_phi = (JObject)obj_phi[j];

                                            if (js_phi["hdon_id"] != null)
                                            {
                                                if (string.IsNullOrEmpty(js_phi["hdon_id"].ToString()))
                                                {
                                                    js_phi["hdon_id"] = masterObj["hdon_id"];
                                                }
                                            }
                                            else
                                            {
                                                js_phi.Add("hdon_id", masterObj["hdon_id"]);
                                            }
                                            js_phi["phi_id"] = Guid.NewGuid();
                                            /*if (js_phi["phi_id"] != null)
                                            {
                                                if (string.IsNullOrEmpty(js_phi["phi_id"].ToString()))
                                                {
                                                    js_phi["phi_id"] = Guid.NewGuid();
                                                }
                                            }
                                            else
                                            {
                                                js_phi.Add("phi_id", Guid.NewGuid());
                                            }*/

                                            //if (fieldStt_rec0 != null)
                                            //{
                                            //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                            //}

                                            await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_phi_insert_n", CommandType.StoredProcedure, js_phi);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();

                    }
                    else json.Add("error", "Cẩn bổ sung chi tiết hóa đơn");
                }
                #endregion

                #region Save mẫu 04
                JObject Mau04 = (JObject)model["mau04"];
                string editModeMau04 = (string)Mau04["editmode"];
                var idMau04 = Guid.NewGuid();
                JArray dataMau04 = (JArray)Mau04["data"];

                await _minvoiceDbContext.BeginTransactionAsync();

                for (int i = 0; i < dataMau04.Count; i++)
                {
                    JObject masterObj = (JObject)dataMau04[i];
                    if (editModeMau04 == "2")
                    {
                        id = Guid.Parse(masterObj["id"].ToString());
                        if (masterObj["mau04_id"] == null)
                        {
                            masterObj.Add("mau04_id", masterObj["id"]);
                        }
                        else
                        {
                            masterObj["mau04_id"] = masterObj["id"];
                        }
                    }
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editModeMau04 == "1")
                    {

                        if (masterObj["ntbao"] != null)
                        {
                            masterObj["ntbao"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("ntbao", DateTime.Now);
                        }

                        if (masterObj["mau04_id"] != null)
                        {
                            masterObj["mau04_id"] = idMau04;
                        }
                        else
                        {
                            masterObj.Add("mau04_id", idMau04);
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                    }

                    if (editMode == "3")
                    {
                    }
                    #endregion

                    string sql = editModeMau04 == "1" ? "#SCHEMA_NAME#.crd_mau04_68_insert" : editModeMau04 == "2" ? "#SCHEMA_NAME#.crd_mau04_68_insert" : "#SCHEMA_NAME#.crd_mau04_68_insert";

                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }
                    #region Chi tiết mẫu 04
                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id = '" + masterObj["mau04_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details["mau04_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["mau04_id"].ToString()))
                                    {
                                        js_details["mau04_id"] = masterObj["mau04_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add("mau04_id", masterObj["mau04_id"]);
                                }

                                if (js_details["mau04_chitiet_id"] != null)
                                {
                                    js_details["mau04_chitiet_id"] = Guid.NewGuid();
                                }
                                else
                                {
                                    js_details.Add("mau04_chitiet_id", Guid.NewGuid());
                                }

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_mau04_68_chitiet_insert", CommandType.StoredProcedure, js_details);
                            }
                        }

                    }
                    #endregion

                }

                await this._minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                #endregion

                // get hdon_id
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("mau04_id", idMau04);
                string sqlSelect_mau04chitiet = $"SELECT hdon_id FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                   + " WHERE mau04_id = @mau04_id";
                DataTable mau04_chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04chitiet, CommandType.Text, parameters2);

                string hdon_id = "";
                if (mau04_chitiet_Table.Rows.Count > 0)
                {
                    hdon_id = mau04_chitiet_Table.Rows[0]["hdon_id"].ToString();
                }
                else
                {
                    hdon_id = idMau04.ToString();
                }

                // ký và gửi mẫu 04 lên cơ quan thuế
                JObject _res = await SignHuyhoadon((JObject)model["sign"], idMau04.ToString(), hdon_id, "3");
                if (_res["error"] != null)
                {
                    return _res;
                }
                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> SaveHuyHoadonTaomoiUSB(JObject model, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                //await _minvoiceDbContext.BeginTransactionAsync();
                #region Save hóa đơn
                JObject obj = (JObject)model;
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];
                    if (!ValidInvoice.InsertValid(masterObj, ref json))
                    {
                        return json;
                    }
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {
                        if (masterObj["tthdon"] == null)
                        {
                            masterObj.Add("tthdon", 0);
                        }

                        if (masterObj["shdon"] != null)
                        {
                            masterObj["shdon"] = null;
                        }
                        else
                        {
                            masterObj.Add("shdon", null);
                        }

                        if (masterObj["tthai"] != null)
                        {
                            masterObj["tthai"] = "Chờ ký";
                        }
                        else
                        {
                            masterObj.Add("tthai", "Chờ ký");
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                        if (masterObj["nglap"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["nglap"].ToString()))
                            {
                                masterObj["nglap"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("nglap", username);
                        }

                        if (masterObj["nlap"] != null)
                        {
                            masterObj["nlap"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (masterObj["hdon_id"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["hdon_id"].ToString()))
                            {
                                masterObj["hdon_id"] = id;
                            }
                            else
                            {
                                id = Guid.Parse(masterObj["hdon_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add("hdon_id", id);
                        }
                    }

                    if (editMode == "2")
                    {

                        if (masterObj["ngsua"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["ngsua"].ToString()))
                            {
                                masterObj["ngsua"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("ngsua", username);
                        }

                        if (masterObj["nsua"] != null)
                        {
                            masterObj["nsua"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nsua", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj["hdon_id"].ToString());
                    }

                    if (editMode == "3")
                    {
                    }
                    #endregion

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }
                    if (editMode != "1" && editMode != "2")
                    {
                        string sql = "#SCHEMA_NAME#.crd_hdon68_delete";
                        await _minvoiceDbContext.BeginTransactionAsync();
                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        await this._minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                    }
                    else if (details != null && details.Count > 0 && ((JArray)(details[0]["data"])).Count > 0)
                    {
                        string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_hdon68_insert_n" : editMode == "2" ? "#SCHEMA_NAME#.crd_hdon68_update_n" : "#SCHEMA_NAME#.crd_hdon68_delete";

                        await _minvoiceDbContext.BeginTransactionAsync();
                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        if (editMode == "1" || editMode == "2")
                        {
                            sql = "#SCHEMA_NAME#.crd_hdon68_update_pt";
                            //await _minvoiceDbContext.BeginTransactionAsync();
                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        }


                        #region Chi tiết hóa đơn

                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details["hdon_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["hdon_id"].ToString()))
                                    {
                                        js_details["hdon_id"] = masterObj["hdon_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add("hdon_id", masterObj["hdon_id"]);
                                }

                                if (js_details["cthdon_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["cthdon_id"].ToString()))
                                    {
                                        js_details["cthdon_id"] = Guid.NewGuid();
                                    }
                                }
                                else
                                {
                                    js_details.Add("cthdon_id", Guid.NewGuid());
                                }

                                //if (fieldStt_rec0 != null)
                                //{
                                //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                //}

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_chitiet_insert", CommandType.StoredProcedure, js_details);
                            }
                        }
                        #endregion
                        #region hatm luuthuephi
                        JArray phi = null;

                        if (masterObj["hoadon68_phi"] != null)
                        {
                            if (masterObj.SelectToken("hoadon68_phi") is JArray)
                            {
                                phi = (JArray)masterObj["hoadon68_phi"];
                            }
                        }

                        if (phi != null)
                        {
                            foreach (var dtl in phi)
                            {
                                JArray obj_phi = (JArray)dtl["data"];
                                if (obj_phi.Count > 0)
                                {
                                    sql = "DELETE FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                                    if (obj_phi.Count > 0)
                                    {
                                        for (int j = 0; j < obj_phi.Count; j++)
                                        {
                                            JObject js_phi = (JObject)obj_phi[j];

                                            if (js_phi["hdon_id"] != null)
                                            {
                                                if (string.IsNullOrEmpty(js_phi["hdon_id"].ToString()))
                                                {
                                                    js_phi["hdon_id"] = masterObj["hdon_id"];
                                                }
                                            }
                                            else
                                            {
                                                js_phi.Add("hdon_id", masterObj["hdon_id"]);
                                            }
                                            js_phi["phi_id"] = Guid.NewGuid();
                                            /*if (js_phi["phi_id"] != null)
                                            {
                                                if (string.IsNullOrEmpty(js_phi["phi_id"].ToString()))
                                                {
                                                    js_phi["phi_id"] = Guid.NewGuid();
                                                }
                                            }
                                            else
                                            {
                                                js_phi.Add("phi_id", Guid.NewGuid());
                                            }*/

                                            //if (fieldStt_rec0 != null)
                                            //{
                                            //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                            //}

                                            await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_phi_insert_n", CommandType.StoredProcedure, js_phi);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        await this._minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                    }
                    else json.Add("error", "Cẩn bổ sung chi tiết hóa đơn");
                }
                #endregion

                #region Save mẫu 04
                JObject Mau04 = (JObject)model["mau04"];
                string editModeMau04 = (string)Mau04["editmode"];
                var idMau04 = Guid.NewGuid();
                JArray dataMau04 = (JArray)Mau04["data"];

                await _minvoiceDbContext.BeginTransactionAsync();

                for (int i = 0; i < dataMau04.Count; i++)
                {
                    JObject masterObj = (JObject)dataMau04[i];
                    if (editModeMau04 == "2")
                    {
                        id = Guid.Parse(masterObj["id"].ToString());
                        if (masterObj["mau04_id"] == null)
                        {
                            masterObj.Add("mau04_id", masterObj["id"]);
                        }
                        else
                        {
                            masterObj["mau04_id"] = masterObj["id"];
                        }
                    }
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editModeMau04 == "1")
                    {

                        if (masterObj["ntbao"] != null)
                        {
                            masterObj["ntbao"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("ntbao", DateTime.Now);
                        }

                        if (masterObj["mau04_id"] != null)
                        {
                            masterObj["mau04_id"] = idMau04;
                        }
                        else
                        {
                            masterObj.Add("mau04_id", idMau04);
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                    }

                    if (editMode == "3")
                    {
                    }
                    #endregion

                    string sql = editModeMau04 == "1" ? "#SCHEMA_NAME#.crd_mau04_68_insert" : editModeMau04 == "2" ? "#SCHEMA_NAME#.crd_mau04_68_insert" : "#SCHEMA_NAME#.crd_mau04_68_insert";

                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }
                    #region Chi tiết mẫu 04
                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.mau04_68_chitiet WHERE mau04_id = '" + masterObj["mau04_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details["mau04_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["mau04_id"].ToString()))
                                    {
                                        js_details["mau04_id"] = masterObj["mau04_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add("mau04_id", masterObj["mau04_id"]);
                                }

                                if (js_details["mau04_chitiet_id"] != null)
                                {
                                    js_details["mau04_chitiet_id"] = Guid.NewGuid();
                                }
                                else
                                {
                                    js_details.Add("mau04_chitiet_id", Guid.NewGuid());
                                }

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_mau04_68_chitiet_insert", CommandType.StoredProcedure, js_details);
                            }
                        }

                    }
                    #endregion

                }

                await this._minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                #endregion

                json.Add("ok", "true");
                json.Add("idMau04", idMau04.ToString());

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> SaveBangTHDL(JObject obj, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {
                        if (masterObj["trang_thai"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["trang_thai"].ToString()))
                            {
                                masterObj["trang_thai"] = "0";
                            }
                        }
                        else
                        {
                            masterObj.Add("trang_thai", "0");
                        }

                        if (masterObj["nglap"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["nglap"].ToString()))
                            {
                                masterObj["nglap"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("nglap", username);
                        }

                        if (masterObj["nlap"] != null)
                        {
                            masterObj["nlap"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (masterObj["bangtonghopdl_68_id"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["bangtonghopdl_68_id"].ToString()))
                            {
                                masterObj["bangtonghopdl_68_id"] = id;
                            }
                            else
                            {
                                id = Guid.Parse(masterObj["bangtonghopdl_68_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add("bangtonghopdl_68_id", id);
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                    }

                    if (editMode == "2")
                    {
                        ////if (masterObj["nglap"] != null)
                        ////{
                        ////    string user_new = masterObj["hdon_id"].ToString();

                        ////    if (isedituser != "C" && user_new != username)
                        ////    {
                        ////        json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                        ////        return json;
                        ////    }
                        ////}

                        //if (masterObj["ngsua"] != null)
                        //{
                        //    if (string.IsNullOrEmpty(masterObj["ngsua"].ToString()))
                        //    {
                        //        masterObj["ngsua"] = username;
                        //    }
                        //}
                        //else
                        //{
                        //    masterObj.Add("ngsua", username);
                        //}

                        //if (masterObj["nsua"] != null)
                        //{
                        //    masterObj["nsua"] = DateTime.Now;
                        //}
                        //else
                        //{
                        //    masterObj.Add("nsua", DateTime.Now);
                        //}

                        id = Guid.Parse(masterObj["bangtonghopdl_68_id"].ToString());

                    }

                    if (editMode == "3")
                    {
                        //if (masterObj["nglap"] != null)
                        //{
                        //    string user_new = masterObj["nglap"].ToString();

                        //    if (isdeluser != "C" && user_new != username)
                        //    {
                        //        json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                        //        return json;
                        //    }
                        //}
                    }
                    #endregion

                    string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_bangtonghopdl_68_insert" : editMode == "2" ? "#SCHEMA_NAME#.crd_bangtonghopdl_68_update" : "#SCHEMA_NAME#.crd_bangtonghopdl_68_delete";

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    #region Chi tiết hóa đơn
                    if (details != null)
                    {
                        sql = "DELETE FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet WHERE bangtonghopdl_68_id = '" + masterObj["bangtonghopdl_68_id"] + "'";
                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);
                        foreach (var dtl in details)
                        {
                            JObject js_details = (JObject)dtl;

                            if (js_details["bangtonghopdl_68_id"] != null)
                            {
                                if (string.IsNullOrEmpty(js_details["bangtonghopdl_68_id"].ToString()))
                                {
                                    js_details["bangtonghopdl_68_id"] = masterObj["bangtonghopdl_68_id"];
                                }
                            }
                            else
                            {
                                js_details.Add("bangtonghopdl_68_id", masterObj["bangtonghopdl_68_id"]);
                            }

                            if (js_details["bangtonghopdl_68_chitiet_id"] != null)
                            {
                                if (string.IsNullOrEmpty(js_details["bangtonghopdl_68_chitiet_id"].ToString()))
                                {
                                    js_details["bangtonghopdl_68_chitiet_id"] = Guid.NewGuid();
                                }
                            }
                            else
                            {
                                js_details.Add("bangtonghopdl_68_chitiet_id", Guid.NewGuid());
                            }

                            //if (fieldStt_rec0 != null)
                            //{
                            //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                            //}

                            await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_bangtonghopdl_68_chitiet_insert", CommandType.StoredProcedure, js_details);
                        }

                    }
                    #endregion

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }

                if (editMode != "3")
                {
                    //string select_cmd = $"SELECT bangtonghopdl_68_id as id , * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{id}'";
                    //DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);
                    //data = JArray.FromObject(tbl);
                    //json.Add("data", data[0]);
                }

                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }
        public async Task<JObject> XmlBangTHDL(string bangtonghopdl_68_id, int sbthdlieu)
        {
            JObject _obj = new JObject();
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id = '{bangtonghopdl_68_id}'";

                string thang = "";
                string ngay = "";
                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["thang"].ToString()))
                {
                    int thangDT = Convert.ToInt16(dauphieu.Rows[0]["thang"].ToString());
                    thang = thangDT.ToString();
                    if (thangDT < 10)
                    {
                        thang = "0" + thangDT;
                    }
                }

                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay"].ToString()))
                {
                    int ngayDT = Convert.ToInt16(dauphieu.Rows[0]["ngay"].ToString());
                    ngay = ngayDT.ToString();
                    if (ngayDT < 10)
                    {
                        ngay = "0" + ngayDT;
                    }
                }

                string sqlBangTongHop = "SELECT COALESCE(sbthdlieu, -1) sbthdlieu"
               + $" FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}'";
                DataTable bangtonghopDauPhieu = await this._minvoiceDbContext.GetDataTableAsync(sqlBangTongHop);

                if (bangtonghopDauPhieu.Rows.Count > 0)
                {
                    int valuesbthdlieu = Convert.ToInt32(bangtonghopDauPhieu.Rows[0]["sbthdlieu"].ToString());
                    if (valuesbthdlieu != -1)
                    {
                        sbthdlieu = valuesbthdlieu;
                    }
                }

                string lhhoa = dauphieu.Rows[0]["lhhoa"].ToString();

                string sqlDetails = "SELECT *"
               + $" FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}'  order by  Cast (stt as numeric)";
                DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                DateTime dt = Convert.ToDateTime(dauphieu.Rows[0]["nlap"].ToString());
                XElement xmlBTHDL = new XElement("BTHDLieu",
                                new XElement("DLBTHop", new XAttribute("Id", "data"),
                                    new XElement("TTChung",
                                        new XElement("PBan", "2.0.0"),
                                        new XElement("MSo", "01/TH-HĐĐT"),
                                        new XElement("Ten", "Bảng tổng hợp dữ liệu hóa đơn điện tử"),
                                        new XElement("SBTHDLieu", sbthdlieu),
                                        GetDataRow("LKDLieu", dauphieu.Rows[0], "lkdlieu"),
                                        new XElement("KDLieu", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                        GetDataRow("LDau", dauphieu.Rows[0], "ldau"),
                                        GetDataRow("BSLThu", dauphieu.Rows[0], "bslthu"),
                                        // GetDataRow("SDLThu", dauphieu.Rows[0], "sdlthu"),
                                        new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                        GetDataRow("TNNT", dauphieu.Rows[0], "tnnt"),
                                        GetDataRow("MST", dauphieu.Rows[0], "mst"),
                                        GetDataRow("HDDIn", dauphieu.Rows[0], "hddin"),
                                        GetDataRow("LHHoa", dauphieu.Rows[0], "lhhoa")
                                    ),
                                    new XElement("NDBTHDLieu",
                                            new XElement("DSDLieu",
                                                from row in chitiet.AsEnumerable()
                                                select new XElement("DLieu",
                                                    new XElement("STT", row["stt"].ToString()),
                                                    new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                                    new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                                    GetDataRow("SHDon", row, "shdon"),
                                                    GetDataRowDate("NLap", row, "nlap"),
                                                    GetDataRow("TNMua", row, "tnmua"),
                                                    GetDataRow("MSTNMua", row, "mstnmua"),
                                                    GetDataRow("MKHang", row, "mkhang"),
                                                    GetDataRow("MHHDVu", row, "mhhoa"),
                                                    GetDataRow("THHDVu", row, "thhdvu"),
                                                    GetDataRow("DVTinh", row, "dvtinh"),
                                                    GetDataRowDoubble("SLuong", row, "sluong"),
                                                    GetDataRowDoubble("TTCThue", row, "ttcthue"),
                                                    GetDataRow("TSuat", row, "tsuat"),
                                                    GetDataRowDoubble("TgTThue", row, "tgtthue"),
                                                    GetDataRowDoubble("TgTTToan", row, "tgtttoan"),
                                                    GetDataRow("TThai", row, "tthai"),
                                                    GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(0)) : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(1)) : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                                    GetDataRow("SHDCLQuan", row, "shdlquan"),
                                                    GetDataRow("STBao", row, "stbao"),
                                                    GetDataRowDate("NTBao", row, "ntbao"),
                                                    GetDataRow("GChu", row, "gchu")
                                                )
                                            )
                                        )
                                ),
                                new XElement("DSCKS",
                                    new XElement("NNT"),
                                    new XElement("CCKSKhac")
                                    )
                        );

                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBTHDL.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        public async Task<JObject> XmlBangTHDL(string bangtonghopdl_68_id, string mtdiep_gui, DataTable tblCertificate, DataTable tblUser, string check_HSM)
        {
            JObject _obj = new JObject();
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id in {bangtonghopdl_68_id} order by sbthdlieu";

                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);

                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_BTHDL, dauphieu.Rows.Count);
                XElement xml_DLieu = new XElement("DLieu");
                foreach (DataRow item in dauphieu.Rows)
                {
                    string thang = "";
                    string ngay = "";

                    if (!string.IsNullOrEmpty(item["thang"].ToString()))
                    {
                        int thangDT = Convert.ToInt16(item["thang"].ToString());
                        thang = thangDT.ToString();
                        if (thangDT < 10)
                        {
                            thang = "0" + thangDT;
                        }
                    }

                    if (!string.IsNullOrEmpty(item["ngay"].ToString()))
                    {
                        int ngayDT = Convert.ToInt16(item["ngay"].ToString());
                        ngay = ngayDT.ToString();
                        if (ngayDT < 10)
                        {
                            ngay = "0" + ngayDT;
                        }
                    }

                    string fromDate = ((DateTime)item["tu_ngay"]).ToString("yyyy-MM-dd");
                    string toDate = ((DateTime)item["den_ngay"]).ToString("yyyy-MM-dd");

                    int sbthdlieu = await tangSoBTHDL(fromDate, toDate);

                    await this._minvoiceDbContext.ExecuteNoneQueryAsync("Update #SCHEMA_NAME#.bangtonghopdl_68 set sbthdlieu=" + sbthdlieu + " WHERE sbthdlieu is null AND bangtonghopdl_68_id ='" + item["bangtonghopdl_68_id"].ToString() + "'");

                    string sqlBangTongHop = "SELECT COALESCE(sbthdlieu, -1) sbthdlieu"
                 + $" FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{item["bangtonghopdl_68_id"].ToString()}'";
                    DataTable bangtonghopDauPhieu = await this._minvoiceDbContext.GetDataTableAsync(sqlBangTongHop);

                    if (bangtonghopDauPhieu.Rows.Count > 0)
                    {
                        int valuesbthdlieu = Convert.ToInt32(bangtonghopDauPhieu.Rows[0]["sbthdlieu"].ToString());
                        if (valuesbthdlieu != -1)
                        {
                            sbthdlieu = valuesbthdlieu;
                        }
                    }

                    string sqlDetails = "SELECT *"
                    + $" FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet WHERE bangtonghopdl_68_id ='{item["bangtonghopdl_68_id"].ToString()}'  order by  Cast (stt as numeric)";
                    DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                    //XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                    //XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                    DateTime dt = Convert.ToDateTime(item["nlap"].ToString());
                    XElement xmlBTHDL_O = new XElement("BTHDLieu",
                                 new XElement("DLBTHop", new XAttribute("Id", "data"),
                                     new XElement("TTChung",
                                         new XElement("PBan", "2.0.0"),
                                         new XElement("MSo", "01/TH-HĐĐT"),
                                         new XElement("Ten", "Bảng tổng hợp dữ liệu hóa đơn điện tử"),
                                         new XElement("SBTHDLieu", sbthdlieu),
                                         GetDataRow("LKDLieu", item, "lkdlieu"),
                                         new XElement("KDLieu", (item["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + item["nam"].ToString()),
                                         GetDataRow("LDau", item, "ldau"),
                                         GetDataRow("BSLThu", item, "bslthu"),
                                         // GetDataRow("SDLThu", item, "sdlthu"),
                                         new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                         GetDataRow("TNNT", item, "tnnt"),
                                         GetDataRow("MST", item, "mst"),
                                         GetDataRow("HDDIn", item, "hddin"),
                                         GetDataRow("LHHoa", item, "lhhoa")
                                     ),
                                     new XElement("NDBTHDLieu",
                                             new XElement("DSDLieu",
                                                 from row in chitiet.AsEnumerable()
                                                 select new XElement("DLieu",
                                                     new XElement("STT", row["stt"].ToString()),
                                                     new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                                     new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                                     GetDataRow("SHDon", row, "shdon"),
                                                     GetDataRowDate("NLap", row, "nlap"),
                                                     GetDataRow("TNMua", row, "tnmua"),
                                                     GetDataRow("MSTNMua", row, "mstnmua"),
                                                     GetDataRow("MKHang", row, "mkhang"),
                                                     GetDataRow("MHHDVu", row, "mhhoa"),
                                                     GetDataRow("THHDVu", row, "thhdvu"),
                                                     GetDataRow("DVTinh", row, "dvtinh"),
                                                     GetDataRowDoubble("SLuong", row, "sluong"),
                                                     GetDataRowDoubble("TTCThue", row, "ttcthue"),
                                                     GetDataRow("TSuat", row, "tsuat"),
                                                     GetDataRowDoubble("TgTThue", row, "tgtthue"),
                                                     GetDataRowDoubble("TgTTToan", row, "tgtttoan"),
                                                     GetDataRow("TThai", row, "tthai"),
                                                     GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                                     row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(0)) : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                                     row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(1)) : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                                     GetDataRow("SHDCLQuan", row, "shdlquan"),
                                                     GetDataRow("STBao", row, "stbao"),
                                                     GetDataRowDate("NTBao", row, "ntbao"),
                                                     GetDataRow("GChu", row, "gchu")
                                                 )
                                             )
                                         )
                                 ),
                                 new XElement("DSCKS",
                                     new XElement("NNT"),
                                     new XElement("CCKSKhac")
                                     )
                         );
                    string xml_o = "";
                    string tenCty = await getTenDv();
                    if (string.IsNullOrEmpty(check_HSM))
                    {
                        xml_o = "";
                        if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                        {
                            xml_o = CommonManager.ThemPhanDonViKiThoiGianKy(xmlBTHDL_O.ToString(), await getTenDv());
                            SignXML input = new SignXML()
                            {
                                msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                xmlData = xml_o,
                                namespacePrefix = "",
                                reason = "Ky bang tong hop du lieu",
                                signatureTagId = "seller",
                                signingTagId = "data",
                                signingTagName = "NNT",
                                prompt = "Ky bang tong hop du lieu"
                            };
                            SignedXmlReturn resultSign = CommonManager.SignSimPKI(input);
                            //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                            xml_o = resultSign.xmlSignedData;
                        }
                        else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                        {
                            xml_o = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NNT", "xml",
                                xmlBTHDL_O.ToString(), tblCertificate.Rows[0]["cer_serial"].ToString());
                        }
                        else
                        {
                            string pass = tblCertificate.Rows[0]["pass"].ToString();
                            byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                            X509Certificate2 certFile = new X509Certificate2(bytes, pass);
                            xml_o = CommonManager.SignXml(await getTenDv(), "#data", "seller", xmlBTHDL_O.ToString(), certFile, "NNT");
                        }

                    }
                    else
                    {
                        Dictionary<string, object> dicParam = new Dictionary<string, object>();
                        string userId = tblUser.Rows[0]["wb_user_id"].ToString();
                        dicParam.Add("wb_user_id", Guid.Parse(userId));

                        DataTable tblUser_Hsm = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id", CommandType.Text, dicParam);

                        if (tblUser_Hsm.Rows.Count == 0)
                        {
                            _obj.Add("error", "Chưa có tài khoản cấu hình");
                            return _obj;
                        }
                        // lấy account hsm portal
                        string username_hsm = tblUser_Hsm.Rows[0]["username"].ToString();
                        string password_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password"].ToString()));
                        string password2_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password_level2"].ToString()));
                        string ma_dvcs_hsm = tblUser_Hsm.Rows[0]["ma_dvcs"].ToString();

                        // call api login
                        HttpClient client = new HttpClient();

                        var api = CommonConstants.LOGIN;
                        JObject user = new JObject();
                        user.Add("username", username_hsm);
                        user.Add("password", password_hsm);
                        user.Add("ma_dvcs", ma_dvcs_hsm);

                        var token = "";
                        var resLogin = executeLogin(CommonConstants.BASE_HSM_URL, api, user).Result;

                        // kiểm tra nếu login lỗi thì thông báo
                        if (resLogin["error"] != null)
                        {
                            //_obj.Add("error", resLogin["error"].ToString());
                            //return _obj;
                            throw new Exception(resLogin["error"].ToString());
                        }
                        else
                        {
                            token = resLogin["token"].ToString();
                            token = token + ";" + ma_dvcs_hsm;
                        }

                        XmlDocument document = new XmlDocument();
                        document.PreserveWhitespace = false;
                        //xml_o = xmlBTHDL_O.ToString().Replace("'", "''");
                        xml_o = xmlBTHDL_O.ToString();
                        xml_o = xml_o.Replace("\r", "").Replace("\n", "");
                        document.LoadXml(xml_o);
                        XmlNode nodeTT = document.SelectSingleNode("/HDon/DLHDon/TTKhac");

                        if (nodeTT != null)
                        {
                            XmlElement TTin1 = document.CreateElement("TTin");
                            XmlElement TTruong1 = document.CreateElement("TTruong");
                            TTruong1.InnerText = "DVKy";
                            XmlElement KDLieu1 = document.CreateElement("KDLieu");
                            KDLieu1.InnerText = "string";
                            XmlElement DLieu1 = document.CreateElement("DLieu");
                            DLieu1.InnerText = tenCty;
                            TTin1.AppendChild(TTruong1);
                            TTin1.AppendChild(KDLieu1);
                            TTin1.AppendChild(DLieu1);

                            XmlElement TTin2 = document.CreateElement("TTin");
                            XmlElement TTruong2 = document.CreateElement("TTruong");
                            TTruong2.InnerText = "NKy";
                            XmlElement KDLieu2 = document.CreateElement("KDLieu");
                            KDLieu2.InnerText = "date";
                            XmlElement DLieu2 = document.CreateElement("DLieu");
                            DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                            TTin2.AppendChild(TTruong2);
                            TTin2.AppendChild(KDLieu2);
                            TTin2.AppendChild(DLieu2);


                            nodeTT.AppendChild(TTin1);
                            nodeTT.AppendChild(TTin2);
                        }

                        xml_o = document.OuterXml;
                        // Call Sign HSM API
                        JObject result = new JObject();

                        api = CommonConstants.SIGN_HSM_URL_78;
                        JObject json = new JObject();
                        json.Add("password2", password2_hsm);
                        json.Add("idData", "#data");
                        json.Add("idSigner", "seller");
                        json.Add("tagSign", "NNT");
                        json.Add("dataType", "xml");
                        json.Add("xml", xml_o);

                        var resSign = executePost(CommonConstants.BASE_HSM_URL, api, token, json).Result;

                        if ("false".Equals(resSign["status"].ToString().ToLower()))
                        {
                            //_obj.Add("error", resSign["message"].ToString());
                            //return _obj;
                            throw new Exception(resSign["message"].ToString());
                        }
                        else
                        {
                            xml_o = resSign["data"].ToString();
                        }
                        //xml_o = xml_o.Replace("'", "''");
                    }
                    xml_DLieu.Add(XElement.Parse(xml_o, LoadOptions.PreserveWhitespace));
                }
                XDocument xmlBTHDL = new XDocument(new XElement("TDiep",
                        xml_TTChung, xml_DLieu
                        )
                    );
                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBTHDL.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        public async Task<XElement> XmlBangTHDLNew(string bangtonghopdl_68_id, int sbthdlieu)
        {
            JObject _obj = new JObject();
            XElement xmlBTHDL = null;
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id = '{bangtonghopdl_68_id}'";

                string thang = "";
                string ngay = "";
                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["thang"].ToString()))
                {
                    int thangDT = Convert.ToInt16(dauphieu.Rows[0]["thang"].ToString());
                    thang = thangDT.ToString();
                    if (thangDT < 10)
                    {
                        thang = "0" + thangDT;
                    }
                }

                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay"].ToString()))
                {
                    int ngayDT = Convert.ToInt16(dauphieu.Rows[0]["ngay"].ToString());
                    ngay = ngayDT.ToString();
                    if (ngayDT < 10)
                    {
                        ngay = "0" + ngayDT;
                    }
                }

                string sqlDetails = "SELECT *"
                + $" FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}' order by  Cast (stt as numeric)";
                DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                DateTime dt = Convert.ToDateTime(dauphieu.Rows[0]["nlap"].ToString());

                xmlBTHDL =
                     new XElement("BTHDLieu",
                             new XElement("DLBTHop", new XAttribute("Id", "data"),
                                 new XElement("TTChung",
                                     new XElement("PBan", "2.0.0"),
                                     new XElement("MSo", "01/TH-HĐĐT"),
                                     new XElement("Ten", "Bảng tổng hợp dữ liệu hóa đơn điện tử"),
                                     new XElement("SBTHDLieu", sbthdlieu),
                                     GetDataRow("LKDLieu", dauphieu.Rows[0], "lkdlieu"),
                                     new XElement("KDLieu", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                     GetDataRow("LDau", dauphieu.Rows[0], "ldau"),
                                     GetDataRow("BSLThu", dauphieu.Rows[0], "bslthu"),
                                     // GetDataRow("SDLThu", dauphieu.Rows[0], "sdlthu"),
                                     new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                     GetDataRow("TNNT", dauphieu.Rows[0], "tnnt"),
                                     GetDataRow("MST", dauphieu.Rows[0], "mst"),
                                     GetDataRow("HDDIn", dauphieu.Rows[0], "hddin"),
                                     GetDataRow("LHHoa", dauphieu.Rows[0], "lhhoa")
                                 ),
                                 new XElement("NDBTHDLieu",
                                         new XElement("DSDlieu",
                                             from row in chitiet.AsEnumerable()
                                             select new XElement("DLieu",
                                                 new XElement("STT", row["stt"].ToString()),
                                                     new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                                     new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                                     GetDataRow("SHDon", row, "shdon"),
                                                     GetDataRowDate("NLap", row, "nlap"),
                                                     GetDataRow("TNMua", row, "tnmua"),
                                                     GetDataRow("MSTNMua", row, "mstnmua"),
                                                     GetDataRow("MKHang", row, "mkhang"),
                                                     GetDataRow("MHHDVu", row, "mhhoa"),
                                                     GetDataRow("THHDVu", row, "thhdvu"),
                                                     GetDataRow("DVTinh", row, "dvtinh"),
                                                     GetDataRowDoubble("SLuong", row, "sluong"),
                                                     GetDataRowDoubble("TTCThue", row, "ttcthue"),
                                                     GetDataRow("TSuat", row, "tsuat"),
                                                     GetDataRowDoubble("TgTThue", row, "tgtthue"),
                                                     GetDataRowDoubble("TgTTToan", row, "tgtttoan"),
                                                     GetDataRow("TThai", row, "tthai"),
                                                     GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                                     row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(0)) : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                                     row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(1)) : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                                     GetDataRow("SHDCLQuan", row, "shdlquan"),
                                                     GetDataRow("STBao", row, "stbao"),
                                                     GetDataRowDate("NTBao", row, "ntbao"),
                                                     GetDataRow("GChu", row, "gchu")
                                             )
                                         )
                                     )
                             ),
                             new XElement("DSCKS",
                                 new XElement("NNT"),
                                 new XElement("CCKSKhac")
                                 )
                     );
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return xmlBTHDL;
        }

        public async Task<JObject> XmlBangTHDLIn(string bangtonghopdl_68_id, int sbthdlieu)
        {
            JObject _obj = new JObject();
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id = '{bangtonghopdl_68_id}'";

                string thang = "";
                string ngay = "";
                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["thang"].ToString()))
                {
                    int thangDT = Convert.ToInt16(dauphieu.Rows[0]["thang"].ToString());
                    thang = thangDT.ToString();
                    if (thangDT < 10)
                    {
                        thang = "0" + thangDT;
                    }
                }

                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay"].ToString()))
                {
                    int ngayDT = Convert.ToInt16(dauphieu.Rows[0]["ngay"].ToString());
                    ngay = ngayDT.ToString();
                    if (ngayDT < 10)
                    {
                        ngay = "0" + ngayDT;
                    }
                }

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("is_ky", 0);

                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay_ky"].ToString()))
                {
                    parameters["is_ky"] = 1;
                }
                else
                {
                    parameters["is_ky"] = 0;

                }


                string sqlDetails = "SELECT *"
                + $" FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}' order by  Cast (stt as numeric)";
                DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                DateTime dt = Convert.ToDateTime(dauphieu.Rows[0]["nlap"].ToString());
                XDocument xmlBTHDL = new XDocument(
                    new XElement("BTHDLieu",
                            new XElement("DLBTHop", new XAttribute("Id", "data"),
                                new XElement("TTChung",
                                    new XElement("PBan", "2.0.0"),
                                    new XElement("MSo", "01/TH-HĐĐT"),
                                    new XElement("Ten", "Bảng tổng hợp dữ liệu hóa đơn điện tử"),
                                    new XElement("SBTHDLieu", sbthdlieu),
                                    GetDataRow("LKDLieu", dauphieu.Rows[0], "lkdlieu"),
                                    new XElement("KDLieu", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                    GetDataRow("LDau", dauphieu.Rows[0], "ldau"),
                                    GetDataRow("BSLThu", dauphieu.Rows[0], "bslthu"),
                                    //GetDataRow("SDLThu", dauphieu.Rows[0], "sdlthu"),
                                    new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                    GetDataRow("TNNT", dauphieu.Rows[0], "tnnt"),
                                    GetDataRow("MST", dauphieu.Rows[0], "mst"),
                                    GetDataRow("HDDIn", dauphieu.Rows[0], "hddin"),
                                    GetDataRow("LHHoa", dauphieu.Rows[0], "lhhoa")
                                ),
                                new XElement("NDBTHDLieu",
                                        new XElement("DSDlieu",
                                            from row in chitiet.AsEnumerable()
                                            select new XElement("DLieu",
                                                new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                                    new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                                    GetDataRow("SHDon", row, "shdon"),
                                                    GetDataRowDate("NLap", row, "nlap"),
                                                    GetDataRow("TNMua", row, "tnmua"),
                                                    GetDataRow("MSTNMua", row, "mstnmua"),
                                                    GetDataRow("MKHang", row, "mkhang"),
                                                    GetDataRow("MHHDVu", row, "mhhoa"),
                                                    GetDataRow("THHDVu", row, "thhdvu"),
                                                    GetDataRow("DVTinh", row, "dvtinh"),
                                                    GetDataRowDoubble("SLuong", row, "sluong"),
                                                    GetDataRowDoubble("TTCThue", row, "ttcthue"),
                                                    GetDataRow("TSuat", row, "tsuat"),
                                                    GetDataRowDoubble("TgTThue", row, "tgtthue"),
                                                    GetDataRowDoubble("TgTTToan", row, "tgtttoan"),
                                                    GetDataRow("TThai", row, "tthai"),
                                                    GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(0)) : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : row["lhdlquan"].ToString() == "3" ? new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Split(',').GetValue(1)) : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                                    GetDataRow("SHDCLQuan", row, "shdlquan"),
                                                    GetDataRow("LKDLDChinh", dauphieu.Rows[0], "lkdlieu"),
                                                    new XElement("KDLDChinh", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                                    GetDataRow("STBao", row, "stbao"),
                                                    GetDataRowDate("NTBao", row, "ntbao"),
                                                    GetDataRow("GChu", row, "gchu")
                                            )
                                        )
                                    )
                            ),
                            new XElement("DSCKS",
                                new XElement("NNT"),
                                new XElement("CCKSKhac"),
                                new XElement("is_ky", parameters["is_ky"])
                                )
                        )
                    );

                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBTHDL.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        public async Task<JObject> SignBTHDL(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                string check_HSM = model["CheckHSM"] != null ? model["CheckHSM"].ToString() : "";

                if (username != _webHelper.GetUser())
                {
                    res.Add("error", "Sai mã xác thực");
                    return res;
                }

                if (branch_code != _webHelper.GetDvcs())
                {
                    res.Add("error", "Sai đơn vị");
                    return res;
                }

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                if (tblUser.Rows[0]["is_sign_invoice_hsm"].ToString() != "C" && !string.IsNullOrEmpty(check_HSM))
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn bằng HSM");
                    return res;
                }

                //hatm lấy chuỗi id và thay đổi vào bangtonghopdl_id
                JArray bangtonghopdl_id_array = (JArray)model["id"];
                for (int i = 0; i < bangtonghopdl_id_array.Count(); i++)
                {
                    string bangtonghopdl_id = "(";
                    bangtonghopdl_id += $"'{bangtonghopdl_id_array[i].ToString()}')";
                    X509Certificate2 certFile = null;
                    DataTable tblCertificate = null;
                    if (string.IsNullOrEmpty(check_HSM))
                    {
                        string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                        string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                                + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                                + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                                + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                        if (branch_code.Length > 0)
                        {
                            sql += " AND a.branch_code='" + branch_code + "' ";
                        }

                        if (cert_serial.Length > 0)
                        {
                            sql += " AND a.cer_serial='" + cert_serial + "'";
                        }

                        sql += " ORDER BY end_date desc LIMIT 1";

                        tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);
                        if (tblCertificate.Rows.Count == 0)
                        {
                            res.Add("error", "Không tìm thấy chứng thư số");
                        }

                        /*string pass = tblCertificate.Rows[0]["pass"].ToString();
                        byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];

                        certFile = new X509Certificate2(bytes, pass);*/
                    }


                    await _minvoiceDbContext.BeginTransactionAsync();
                    //DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='" + bangtonghopdl_id + "'");
                    //int shdon = await tangSoBTHDL();

                    string mst = await getMstDv();
                    string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                    JObject ObjXml = await XmlBangTHDL(bangtonghopdl_id, mtdiep_gui, tblCertificate, tblUser, check_HSM);

                    //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                    string xml = ObjXml["xml"].ToString();

                    //xml = CommonManager.SignXml("#data", "seller", xml, certFile, "NNT");

                    string sqlUpdate = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET trang_thai=@trang_thai, xml_send = @xml_send, ngay_ky = @ngay_ky, ngky = @ngky WHERE bangtonghopdl_68_id in {bangtonghopdl_id}";
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("trang_thai", 1);
                    parameters2.Add("xml_send", xml);
                    //parameters2.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    parameters2.Add("ngay_ky", DateTime.Now);
                    parameters2.Add("ngky", username);
                    //parameters2.Add("sbthdlieu", shdon);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                    // lấy thông tin xml TTChung của Thông điệp
                    //string mst = await getMstDv();
                    //string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                    //XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_BTHDL, 1);

                    // lấy thông tin xml dữ liệu
                    //XElement xml_DuLieu = await XmlBangTHDLNew(model["id"].ToString(), shdon);

                    // đẩy dữ liệu hóa đơn vào thẻ DLieu
                    XElement dLieu = new XElement("DLieu");
                    dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                    XDocument xmlThongDiep = new XDocument(XElement.Parse(xml, LoadOptions.PreserveWhitespace));

                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;

                    using (var buffer = new StringWriter())
                    using (var writer = XmlWriter.Create(buffer, writerSettings))
                    {
                        xmlThongDiep.Save(writer);
                        writer.Flush();
                        xml = buffer.ToString();
                    }

                    //xml = xmlThongDiep.ToString();

                    // ký thông điệp gửi lên thuế
                    //xml = CommonManager.SignThongDiep(xml, certFile, pass);

                    // đóng gói xml gửi lên TVAN
                    //string cmd = CommonConstants.MLTDIEP_BTHDL;

                    string data = await ThongDiepGui(CommonConstants.MLTDIEP_BTHDL, xml);
                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }
                    // lưu vào bảng tonghop_gui_tvan_68
                    foreach (var item in bangtonghopdl_id.Remove(bangtonghopdl_id.Length - 1, 1).Remove(0, 1).Split(','))
                    {
                        string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                      + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                        Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                        param_Insert.Add("id", Guid.NewGuid());
                        param_Insert.Add("mltdiep_gui", CommonConstants.MLTDIEP_BTHDL);
                        param_Insert.Add("xml_tdiep_gui", xml);
                        param_Insert.Add("type_id", Guid.Parse(item.Replace("'", "")));
                        param_Insert.Add("mtdiep_gui", mtdiep_gui);
                        //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                        param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                        param_Insert.Add("note", "app");
                        param_Insert.Add("mdvi", _webHelper.GetDvcs());

                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                    }
                    // update vào bảng bangtonghopdl_68
                    Dictionary<string, object> param_Insert_f = new Dictionary<string, object>();
                    //param_Insert.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    param_Insert_f.Add("mtdiep_gui", mtdiep_gui);

                    string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET mtdiep_gui=@mtdiep_gui WHERE bangtonghopdl_68_id in {bangtonghopdl_id}";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param_Insert_f);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    // đẩy queue lên TVAN
                    if (!CommonManager.CheckData(data, 2 * 1024 * 1024))
                    {
                        error = " Dữ liệu không vượt quả 2MB, Số hóa đơn nên ở số lượng từ 0-> 1000";
                    }
                    _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                    //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                    //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                    //_kafkaService.PublishTopic(_topicVendorSendToVan, data);

                    //sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                    //DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                    //JArray array = JArray.FromObject(tblInvoices);

                    //res.Add("ok", true);
                    //res.Add("inv", (JObject)array[0]);
                    //res.Add("hdon_id", hdon_id);
                    //res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                    //res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                    //res.Add("tthai", "Đã ký");

                    //Đổi trạng thái của các hoá đơn trong BTHDL đã ký và gửi lên cơ quan thuế
                    /*string sqlUpdate3 = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = 'Đã gửi', kygui_cqt =1 "
                                      + "WHERE tthai = 'Đã ký' AND hdon_id in ("
                                      + "SELECT b.hdon_id FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet b "
                                      + $"WHERE bangtonghopdl_68_id in {bangtonghopdl_id} "
                                      + "GROUP BY b.hdon_id)";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdate3);*/
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async Task<JObject> XmlBTHDL(string id)
        {
            JObject _res = new JObject();
            try
            {
                DataTable rsDataTable = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='" + id + "'");

                int sbthdlieu = 0;

                if (rsDataTable.Rows.Count > 0)
                {
                    string fromDate = ((DateTime)rsDataTable.Rows[0]["tu_ngay"]).ToString("yyyy-MM-dd");
                    string toDate = ((DateTime)rsDataTable.Rows[0]["den_ngay"]).ToString("yyyy-MM-dd");

                    sbthdlieu = await tangSoBTHDL(fromDate, toDate);
                }

                JObject ObjXml = await XmlBangTHDL(id, sbthdlieu);

                //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                string xml = ObjXml["xml"].ToString();
                _res.Add("xml", xml);
                _res.Add("ok", true);
                _res.Add("shdon", sbthdlieu);

                await this._minvoiceDbContext.ExecuteNoneQueryAsync("Update #SCHEMA_NAME#.bangtonghopdl_68 set sbthdlieu=" + sbthdlieu + " WHERE sbthdlieu is null AND bangtonghopdl_68_id ='" + id + "'");

            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _res;
        }

        public async Task<JObject> SaveXmlBTHDL(JArray model_list)
        {
            JObject _res = new JObject();
            try
            {
                string xml;
                string mst = await getMstDv();
                foreach (JObject model in model_list)
                {
                    await _minvoiceDbContext.BeginTransactionAsync();
                    xml = model["xml"].ToString();
                    Guid bangtonghopdl_id = Guid.Parse(model["id"].ToString());
                    Int32 shdon = Convert.ToInt32(model["shdon"].ToString());
                    //await _minvoiceDbContext.BeginTransactionAsync();

                    string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                    XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_BTHDL, 1);

                    XElement dLieu = new XElement("DLieu");
                    dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                    XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;

                    using (var buffer = new StringWriter())
                    using (var writer = XmlWriter.Create(buffer, writerSettings))
                    {
                        xmlThongDiep.Save(writer);
                        writer.Flush();
                        xml = buffer.ToString();
                    }

                    string sqlUpdate = "UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET trang_thai=@trang_thai, xml_send = @xml_send, ngay_ky = @ngay_ky, ngky = @ngky WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("trang_thai", 1);
                    parameters2.Add("xml_send", xml);
                    parameters2.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    parameters2.Add("ngay_ky", DateTime.Now);
                    parameters2.Add("ngky", _webHelper.GetUser());
                    //parameters2.Add("sbthdlieu", shdon);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                    // lấy thông tin xml TTChung của Thông điệp
                    // lưu vào bảng tonghop_gui_tvan_68

                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }

                    string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                    Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                    param_Insert.Add("id", Guid.NewGuid());
                    param_Insert.Add("mltdiep_gui", CommonConstants.MLTDIEP_BTHDL);
                    param_Insert.Add("xml_tdiep_gui", xml);
                    param_Insert.Add("type_id", bangtonghopdl_id);
                    param_Insert.Add("mtdiep_gui", mtdiep_gui);
                    //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                    param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                    param_Insert.Add("note", "app");
                    param_Insert.Add("mdvi", _webHelper.GetDvcs());

                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                    param_Insert.Clear();
                    param_Insert.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    param_Insert.Add("mtdiep_gui", mtdiep_gui);

                    string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET mtdiep_gui=@mtdiep_gui WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param_Insert);

                    //await _minvoiceDbContext.TransactionCommitAsync();
                    //_minvoiceDbContext.CloseTransaction();

                    string data = await ThongDiepGui(CommonConstants.MLTDIEP_BTHDL, xml);

                    // đẩy queue lên TVAN
                    _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);
                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    // Thay đổi trạng thái các hoá đơn đã gửi lên cơ quan thuế
                    /*string sqlUpdate3 = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = 'Đã gửi', kygui_cqt =1 "
                                        + "WHERE tthai = 'Đã ký' AND hdon_id in ("
                                        + "SELECT b.hdon_id FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet b "
                                        + $"WHERE bangtonghopdl_68_id = '{bangtonghopdl_id}' "
                                        + "GROUP BY b.hdon_id) ";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdate3);*/
                }

            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
                Log.Error(e.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }
            return _res;
        }

        public async Task<byte[]> inMauHoadon(string id, string type, string xml)
        {
            byte[] bytes = null;

            string qry = $"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code='{_webHelper.GetDvcs()}'";
            DataTable dmdvcs = await this._minvoiceDbContext.GetDataTableAsync(qry);

            DataTable tblKyhieu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + id + "'");
            DataTable tblDmmauhd = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + tblKyhieu.Rows[0]["qlmtke_id"] + "'");
            string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

            //DataTable tblMauHoaDon = await this._minvoiceDbContext.GetDataTableAsync(sql);
            //DataRow row = tblMauHoaDon.Rows[0];

            //string branch_code = row["branch_code"].ToString();

            //string name = "INHD_" + row["template_code"].ToString().Replace("/", "") + "_" + row["invoice_series"].ToString().Replace("/", "") + ".repx";

            XtraReport report = ReportUtil.LoadReportFromString(invReport);

            //   }
            xml = xml.Replace("#TenCongTy#",
                        dmdvcs.Rows[0]["name"].ToString())
                    .Replace("#MaSoThue#", dmdvcs.Rows[0]["tax_code"].ToString())
                    .Replace("#DiaChiCongTy#", dmdvcs.Rows[0]["address"].ToString())
                    .Replace("#KyHieu#", tblKyhieu.Rows[0]["khhdon"].ToString())
                    .Replace("#TenNganHang#", dmdvcs.Rows[0]["bank_name"].ToString())
                    .Replace("#SoTKNganHang#", dmdvcs.Rows[0]["bank_account"].ToString())
                    .Replace("#SDThoai#", dmdvcs.Rows[0]["tel"].ToString())
                ;
            //string kyhieu = tblKyhieu.Rows[0]["khhdon"].ToString();
            report.Name = "XtraReport1";
            report.ScriptReferencesString = "AccountSignature.dll";

            DataSet ds = new DataSet();

            using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
            {
                ds.ReadXmlSchema(xmlReader);
                xmlReader.Close();
            }

            using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
            {
                ds.ReadXml(xmlReader);
                xmlReader.Close();
            }

            if (ds.Tables.Contains("TblXmlData"))
            {
                ds.Tables.Remove("TblXmlData");
            }


            DataTable tblXmlData = new DataTable();
            tblXmlData.TableName = "TblXmlData";
            tblXmlData.Columns.Add("data");

            DataRow r = tblXmlData.NewRow();
            r["data"] = xml;
            tblXmlData.Rows.Add(r);
            ds.Tables.Add(tblXmlData);

            string datamember = report.DataMember;

            if (datamember.Length > 0)
            {
                if (ds.Tables.Contains(datamember))
                {
                    DataTable tblChiTiet = ds.Tables[datamember];
                    int rowcount = ds.Tables[datamember].Rows.Count;
                }
            }
            if (report.Parameters["MSG_TB"] != null)
            {
                report.Parameters["MSG_TB"].Value = "";
            }

            if (report.Parameters["MCCQT"] != null)
            {
                report.Parameters["MCCQT"].Value = "";
            }

            // số bảo mật
            if (report.Parameters["SoBaoMat"] != null)
            {
                report.Parameters["SoBaoMat"].Value = "";
            }

            //var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

            //if (lblHoaDonMau != null)
            //{
            //    lblHoaDonMau.Visible = false;
            //}


            report.DataSource = ds;
            report.CreateDocument();
            MemoryStream ms = new MemoryStream();


            if (type == "Html")
            {
                report.ExportOptions.Html.EmbedImagesInHTML = true;
                report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                report.ExportOptions.Html.Title = "Mẫu hóa đơn";
                report.ExportToHtml(ms);

                string html = Encoding.UTF8.GetString(ms.ToArray());

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);


                string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                if (nodes != null)
                {
                    foreach (HtmlNode node in nodes)
                    {
                        string eventMouseDown = node.Attributes["onmousedown"].Value;

                        if (eventMouseDown.Contains("showCert('seller')"))
                        {
                            node.SetAttributeValue("id", "certSeller");
                        }
                        if (eventMouseDown.Contains("showCert('buyer')"))
                        {
                            node.SetAttributeValue("id", "certBuyer");
                        }
                        if (eventMouseDown.Contains("showCert('vacom')"))
                        {
                            node.SetAttributeValue("id", "certVacom");
                        }
                        if (eventMouseDown.Contains("showCert('minvoice')"))
                        {
                            node.SetAttributeValue("id", "certMinvoice");
                        }
                    }
                }

                HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                HtmlNode xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("id", "xmlData");
                xmlNode.SetAttributeValue("type", "text/xmldata");

                xmlNode.AppendChild(doc.CreateTextNode(xml));
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("type", "text/javascript");

                xmlNode.InnerHtml = "$(function () { "
                                   + "  var url = 'http://localhost:19898/signalr'; "
                                   + "  var connection = $.hubConnection(url, {  "
                                   + "     useDefaultPath: false "
                                   + "  });"
                                   + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                   + " invoiceHubProxy.on('resCommand', function (result) { "
                                   + " }); "
                                   + " connection.start().done(function () { "
                                   + "      console.log('Connect to the server successful');"
                                   + "      $('#certSeller').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'seller' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certVacom').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'vacom' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certBuyer').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'buyer' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certMinvoice').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'minvoice' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "})"
                                   + ".fail(function () { "
                                   + "     alert('failed in connecting to the signalr server'); "
                                   + "});"
                                   + "});";

                head.AppendChild(xmlNode);

                if (report.Watermark != null)
                {
                    if (report.Watermark.Image != null)
                    {
                        ImageConverter _imageConverter = new ImageConverter();
                        byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                        string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                        HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                        string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                        string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                        HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                        style.AppendChild(textNode);

                        HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                        HtmlNodeCollection pageNodes = body.SelectNodes("div");

                        foreach (var pageNode in pageNodes)
                        {
                            pageNode.Attributes.Add("class", "waterMark");

                            string divStyle = pageNode.Attributes["style"].Value;
                            divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                            pageNode.Attributes["style"].Value = divStyle;
                        }
                    }
                }

                ms.SetLength(0);
                doc.Save(ms);

                doc = null;
            }
            else if (type == "Image")
            {
                var options = new ImageExportOptions(ImageFormat.Png)
                {
                    ExportMode = ImageExportMode.SingleFilePageByPage,
                };

                report.ExportToImage(ms, options);
            }
            else
            {
                report.ExportToPdf(ms);
            }

            bytes = ms.ToArray();
            return bytes;
        }

        public async Task<JObject> GetDSHoaDonBangKeBanRa(JObject model)
        {
            JObject json = new JObject();
            try
            {
                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string ky_hieu = model["ky_hieu"].ToString();
                string lst_branch_code = model["lst_branch_code"].ToString();
                string username = _webHelper.GetUser();

                if (string.IsNullOrEmpty(ky_hieu))
                {
                    var sqlCheck = $@"select c.khhdon
                                    from #SCHEMA_NAME#.phanhoadon_78 p
                                             inner join #SCHEMA_NAME#.phanhoadon_78_donvi p78 on p78.phanhoadon_78_id = p.id
                                             join (select q.khhdon, q.qlkhsdung_id
                                                   from #SCHEMA_NAME#.permission_kyhieu68 a
                                                            join #SCHEMA_NAME#.wb_user b on a.wb_user_id = b.wb_user_id
                                                            join #SCHEMA_NAME#.quanlykyhieu68 q on a.qlkhsdung_id = q.qlkhsdung_id
                                                   where b.username = '{username}') c on p.kyhieu_id = c.qlkhsdung_id
                                    where POSITION(p78.branch_code in '{lst_branch_code}') > 0";

                    DataTable rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheck);
                    ky_hieu = string.Join(",", rsTable.AsEnumerable().Select(row => row.Field<string>("khhdon")).ToArray());
                }

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));
                parameters.Add("ky_hieu", ky_hieu);
                parameters.Add("lst_branch_code", lst_branch_code);

                //var invoiceDb = this._nopDbContext.GetInvoiceDb();
                string sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bangkebanra_68_new('#SCHEMA_NAME#',@ma_dvcs,@lst_branch_code,@ky_hieu,@tu_ngay,@den_ngay)";
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray jarr = JArray.FromObject(dt);
                json.Add("data", jarr);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }
        public async Task<JObject> GetDSHoaDonBangKeBanRaChiTiet(JObject model)
        {
            JObject json = new JObject();
            try
            {
                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string ky_hieu = model["ky_hieu"].ToString();
                string lst_branch_code = model["lst_branch_code"].ToString();
                string username = _webHelper.GetUser();

                if (string.IsNullOrEmpty(ky_hieu))
                {
                    var sqlCheck = $@"select c.khhdon
                                    from #SCHEMA_NAME#.phanhoadon_78 p
                                             inner join #SCHEMA_NAME#.phanhoadon_78_donvi p78 on p78.phanhoadon_78_id = p.id
                                             join (select q.khhdon, q.qlkhsdung_id
                                                   from #SCHEMA_NAME#.permission_kyhieu68 a
                                                            join #SCHEMA_NAME#.wb_user b on a.wb_user_id = b.wb_user_id
                                                            join #SCHEMA_NAME#.quanlykyhieu68 q on a.qlkhsdung_id = q.qlkhsdung_id
                                                   where b.username = '{username}') c on p.kyhieu_id = c.qlkhsdung_id
                                    where POSITION(p78.branch_code in '{lst_branch_code}') > 0";

                    DataTable rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheck);
                    ky_hieu = string.Join(",", rsTable.AsEnumerable().Select(row => row.Field<string>("khhdon")).ToArray());
                }

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));
                parameters.Add("ky_hieu", ky_hieu);
                parameters.Add("lst_branch_code", lst_branch_code);

                //var invoiceDb = this._nopDbContext.GetInvoiceDb();
                string sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bangkebanra_68_hatm('#SCHEMA_NAME#',@ma_dvcs,@lst_branch_code,@ky_hieu,@tu_ngay,@den_ngay)";
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray jarr = JArray.FromObject(dt);
                json.Add("data", jarr);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }

        public async Task<DataSet> GetDataForPrint(string sql, Dictionary<string, object> parameters, JArray filter, string order)
        {
            DataSet ds = new DataSet();
            try
            {
                string txtFilter = "";
                if (filter != null && filter.Count > 0)
                {
                    JObject js;
                    for (int i = 0; i < filter.Count; i++)
                    {
                        js = (JObject)filter[i];
                        if (js["columnType"] == null)
                        {
                            if (!string.IsNullOrEmpty(js["value"].ToString()) && !"null".Equals(js["value"].ToString()))
                            {
                                txtFilter += " AND " + js["columnName"] + Filter.FilterGrid(js["value"].ToString(), "nvarchar");
                            }
                            if (!string.IsNullOrEmpty(js["value"].ToString()) && "null".Equals(js["value"].ToString()))
                            {
                                txtFilter += " AND " + js["columnName"] + " IS NULL ";
                            }
                        }
                        else
                        {
                            string type = js["columnType"].ToString();
                            if (!string.IsNullOrEmpty(js["value"].ToString()))
                            {
                                if (type == "datetime")
                                {
                                    JObject value = (JObject)js["value"];
                                    JToken jstart = (JToken)value["start"];
                                    JToken jend = (JToken)value["end"];

                                    if (jstart.Type != JTokenType.Null)
                                    {
                                        DateTime startTime = Convert.ToDateTime(jstart.ToString());
                                        // startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime)).AddDays(1);
                                        startTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", startTime));

                                        txtFilter += " AND " + js["columnName"] + ">='" + string.Format("{0:yyyy-MM-dd}", startTime) + "' ";
                                    }

                                    if (jend.Type != JTokenType.Null)
                                    {
                                        DateTime endTime = Convert.ToDateTime(jend.ToString());
                                        // endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime)).AddDays(1);
                                        endTime = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd}", endTime));

                                        txtFilter += " AND " + js["columnName"] + "<='" + string.Format("{0:yyyy-MM-dd}", endTime) + "' ";
                                    }
                                }
                                else
                                {
                                    if (type == "numeric")
                                    {
                                        txtFilter += "AND CAST(" + js["columnName"] + " AS TEXT)" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                    //check ky hieu hoa don
                                    else if (type == "nvarchar" && js["columnName"].ToString().Equals("cctbao_id"))
                                    {
                                        txtFilter += " AND " + js["columnName"] + " IN " +
                                                     js["value"].ToString();
                                    }
                                    else if (type == "nvarchar" && js["columnName"].ToString().Equals("null"))
                                    {
                                        txtFilter += " AND " + js["columnName"] + " IS NULL ";
                                    }
                                    else
                                    {
                                        txtFilter += " AND lower(" + js["columnName"] + ")" + Filter.FilterGrid(js["value"].ToString(), type);
                                    }
                                }
                            }
                        }
                    }

                }
                if (!string.IsNullOrEmpty(txtFilter))
                    txtFilter = " WHERE 1 = 1 " + txtFilter + " ";
                ds = await _minvoiceDbContext.GetDataSetAsync(sql + txtFilter + order, CommandType.Text, parameters);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
            return ds;
        }

        public async Task<JObject> ReSignInvoiceCertFile68(string folder, JObject obj, bool reSign = false)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                JArray arrData = (JArray)obj["data"];

                JObject model = (JObject)arrData[0];

                string cmd_type = model["type_cmd"].ToString();

                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                //Guid hdon_id = Guid.Parse(model["hdon_id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                             + "WHERE a.cert_type in ('FILE', 'HSM_SCTV') AND a.begin_date<=current_timestamp AND a.end_date>=current_timestamp";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                    return res;
                }


                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                //get arr hdon_id từ obj để for
                JArray arrhdon_id = (JArray)model["lsthdon_id"];

                JArray arrayMail = new JArray();

                for (int i = 0; i < arrhdon_id.Count; i++)
                {
                    try
                    {
                        var hdonid = arrhdon_id[i].ToString();

                        Guid hdon_id = Guid.Parse(hdonid);

                        #region hatm CheckQuyTrinhHoaDon
                        Dictionary<string, object> parametersN = new Dictionary<string, object>();
                        parametersN.Add("hdon_id", hdon_id);

                        DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon,cctbao_id,mdvi FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parametersN);
                        string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                        parametersN.Clear();
                        parametersN.Add("hdon_id", hdon_id);
                        parametersN.Add("nlap", DateTime.Parse((invoice_issued_date)));
                        parametersN.Add("cctbao_id", Guid.Parse(tblInvoiceAuth.Rows[0]["cctbao_id"].ToString()));
                        parametersN.Add("mdvi", tblInvoiceAuth.Rows[0]["mdvi"].ToString());

                        sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='Chờ ký' and cctbao_id = @cctbao_id and mdvi = @mdvi AND nlap< @nlap LIMIT 1";
                        DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parametersN);

                        if (tblInvoiceAuthBefore.Rows.Count > 0)
                        {
                            // res.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                            // return res;
                            throw new Exception("E0001");
                        }

                        #endregion

                        await _minvoiceDbContext.BeginTransactionAsync();

                        #region lấy số hóa đơn khi không có
                        if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                        {
                            sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                            JObject masterObj = new JObject();
                            masterObj.Add("branch_code", _webHelper.GetDvcs());
                            masterObj.Add("hdon_id", hdon_id);

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                        }
                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                        #endregion

                        #region Xử lý hoá đơn theo hdon_id
                        DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT tthai, shdon, khieu FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hdon_id + "'");
                        if (dtHdon.Rows.Count > 0)
                        {
                            if (dtHdon.Rows[0]["tthai"].ToString() != "Chờ ký")
                            {
                                //res.Add("error", "Tồn tại hóa đơn không ở trạng thái chờ ký");
                                //return res;
                                if (reSign)
                                {
                                    sql = $"UPDATE #SCHEMA_NAME#.wb_log_sign SET resign_success = true WHERE hdon_id = '{hdonid}'";
                                    _minvoiceDbContext.ExecuteNoneQuery(sql);
                                }
                                res.RemoveAll();
                                res.Add("tthai", "Đã ký");
                                res.Add("ok", true);
                                return res;
                            }
                        }

                        await _minvoiceDbContext.BeginTransactionAsync();

                        string shdon = dtHdon.Rows[0]["shdon"]?.ToString();
                        string khieu = dtHdon.Rows[0]["khieu"]?.ToString();

                        //int shdon = await tangSoHD(dtHdon.Rows[0]["cctbao_id"].ToString());

                        //string shdon = dtHdon.Rows[0]["shdon"].ToString();

                        //xml bảng kê
                        string qrybangke = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id='{hdon_id.ToString()}'";
                        DataTable Rowbangke = await this._minvoiceDbContext.GetDataTableAsync(qrybangke);

                        JObject ObjXml_BangKe = null;
                        var xml_bangke = "";
                        if (Rowbangke.Rows.Count > 0)
                        {
                            ObjXml_BangKe = await XmlInvoice_BangKe(hdon_id.ToString());
                        }

                        if (ObjXml_BangKe != null)
                        {
                            //xml_bangke = ObjXml_BangKe["xml"].ToString().Replace("'", "''");
                            xml_bangke = ObjXml_BangKe["xml"].ToString();

                            if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                            {
                                xml_bangke = CommonManager.ThemPhanDonViKiThoiGianKy(xml_bangke, await getTenDv());
                                SignXML input = new SignXML()
                                {
                                    msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                    xmlData = xml_bangke,
                                    namespacePrefix = "",
                                    reason = "Ky bang ke dien tu",
                                    signatureTagId = "seller",
                                    signingTagId = "data",
                                    signingTagName = "NBan",
                                    prompt = "Ky bang ke dien tu"
                                };
                                SignedXmlReturn result = CommonManager.SignSimPKI(input);
                                //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                                xml_bangke = result.xmlSignedData;
                            }
                            else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                            {
                                xml_bangke = await CommonManager.SignXmlInvoiceHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                    xml_bangke, tblCertificate.Rows[0]["cer_serial"].ToString());
                            }
                            else
                            {
                                xml_bangke = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml_bangke, certFile, "NBan");
                            }

                        }

                        JObject ObjXml = await XmlInvoice(hdon_id.ToString());

                        //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                        string xml = ObjXml["xml"].ToString();
                        string tdlap = ObjXml["tdlap"].ToString().Replace("'", "''");
                        string tdky = ObjXml["tdky"].ToString().Replace("'", "''");

                        if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                        {
                            xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                            SignXML inputSign = new SignXML()
                            {
                                msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                xmlData = xml,
                                namespacePrefix = "",
                                reason = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                                signatureTagId = "seller",
                                signingTagId = "data",
                                signingTagName = "NBan",
                                prompt = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                            };
                            SignedXmlReturn resultSign = CommonManager.SignSimPKI(inputSign);
                            //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                            xml = resultSign.xmlSignedData;
                        }
                        else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                        {
                            xml = await CommonManager.SignXmlInvoiceHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                                xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                        }
                        else
                        {
                            xml = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml, certFile, "NBan");
                        }

                        //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                        //sql = "SELECT #SCHEMA_NAME#.qr_save_xmlinvoice('#SCHEMA_NAME#','" + hdon_id + "','" + xml + "','" + tblUser.Rows[0]["fullname"].ToString() + "')";
                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        parameters.Add("dlxml_id", Guid.NewGuid());
                        parameters.Add("hdon_id", hdon_id);
                        parameters.Add("dlxml", xml);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }

                        string sqlXml = "";
                        if (!string.IsNullOrEmpty(xml_bangke))
                        {
                            if (CommonManager.CheckThoiGian(xml_bangke) == false)
                            {
                                //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                                //return res;
                                throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            }

                            parameters.Add("dlxml_bangke", xml_bangke);
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68(dlxml_id,hdon_id,dlxml,dlxml_bangke) VALUES(@dlxml_id,@hdon_id, @dlxml, @dlxml_bangke)";
                        }
                        else
                        {
                            sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68 VALUES(@dlxml_id,@hdon_id, @dlxml)";
                        }

                        await _minvoiceDbContext.TransactionCommandAsync(sqlXml, CommandType.Text, parameters);

                        string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, ngky = @ngky, nky=@nky, tdlap = @tdlap, kygui_cqt =@kygui_cqt WHERE hdon_id = @hdon_id";
                        Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                        parameters2.Add("tthai", model["guiCQT"] == null ? "Đã gửi" : "Đã ký");
                        parameters2.Add("kygui_cqt", model["guiCQT"] == null ? 1 : 0);
                        parameters2.Add("ngky", username);
                        parameters2.Add("hdon_id", hdon_id);
                        //parameters2.Add("nky", DateTime.Parse(tdky));
                        parameters2.Add("nky", DateTime.Now);
                        //parameters2.Add("shdon", shdon);
                        parameters2.Add("tdlap", DateTime.Parse(tdlap));

                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                        // lấy giá trị signature

                        //XmlDocument doc = new XmlDocument();
                        //doc.LoadXml(xml);

                        // lấy thông tin xml TTChung của Thông điệp
                        string mst = await getMstDv();
                        string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                        XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, model["type_cmd"].ToString(), 1);

                        // lấy thông tin xml dữ liệu
                        //XElement xml_DuLieu = await XmlInvoiceNew(model["hdon_id"].ToString());

                        // đẩy dữ liệu hóa đơn vào thẻ DLieu
                        XElement dLieu = new XElement("DLieu");
                        dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                        XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                        var writerSettings = new XmlWriterSettings();
                        writerSettings.OmitXmlDeclaration = true;

                        using (var buffer = new StringWriter())
                        using (var writer = XmlWriter.Create(buffer, writerSettings))
                        {
                            xmlThongDiep.Save(writer);
                            writer.Flush();
                            xml = buffer.ToString();
                        }

                        //xml = xmlThongDiep.ToString();

                        // ký thông điệp gửi lên thuế

                        //xml = CommonManager.SignThongDiep(xml, certFile, pass);
                        if (CommonManager.CheckThoiGian(xml) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }
                        if (model["guiCQT"] == null)
                        {
                            // lưu vào bảng tonghop_gui_tvan_68

                            if (model["is_api"] == null)
                            {
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                     + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                param_Insert.Add("id", Guid.NewGuid());
                                param_Insert.Add("mltdiep_gui", model["type_cmd"].ToString());
                                param_Insert.Add("xml_tdiep_gui", xml);
                                param_Insert.Add("type_id", hdon_id);
                                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                                param_Insert.Add("note", "app");
                                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                            }
                            else
                            {
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi, is_api) VALUES "
                                    + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi, @is_api)";
                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                param_Insert.Add("id", Guid.NewGuid());
                                param_Insert.Add("mltdiep_gui", model["type_cmd"].ToString());
                                param_Insert.Add("xml_tdiep_gui", xml);
                                param_Insert.Add("type_id", hdon_id);
                                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                                param_Insert.Add("note", "app");
                                param_Insert.Add("mdvi", _webHelper.GetDvcs());
                                param_Insert.Add("is_api", "1"); // phân biệt đơn vị gọi từ API

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                            }

                            // update mã thông điệp gửi vào bảng hóa đơn
                            Dictionary<string, object> para = new Dictionary<string, object>();
                            para.Add("hdon_id", hdon_id);
                            if (model["type_cmd"].ToString() == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                            {
                                // update trạng thái
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("tthai", "Chờ cấp mã");
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }
                            else
                            {
                                // update mã thông điệp gửi
                                sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                                para.Add("mtdiep_gui", mtdiep_gui);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                            }

                        }

                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();

                        res.RemoveAll();

                        if (model["guiCQT"] == null)
                        {
                            // đóng gói xml gửi lên TVAN
                            string cmd = model["type_cmd"].ToString();

                            string data = await ThongDiepGui(cmd, xml);

                            // đẩy queue lên TVAN
                            _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                            res.Add("trang_thai", "Đã gửi hóa đơn tới Cơ quan thuế");

                            //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                            //_kafkaService.PublishTopic(_topicVendorSendToVan, data);
                        }

                        sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                        DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                        //JArray array = JArray.FromObject(tblInvoices);

                        //res.Add("ok", true);
                        //res.Add("inv", (JObject)array[0]);
                        //res.Add("hdon_id", hdon_id);
                        //res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                        //res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                        res.Add("tthai", "Đã ký");

                        //res.RemoveAll();
                        #endregion
                        if (cmd_type == "203")
                        {
                            #region xử lý gửi mail theo hdon_id
                            string email = tblInvoices.Rows[0]["email"].ToString();
                            string tieude = "Thông báo xuất hóa đơn điện tử";


                            dynamic ObjectMail = new JObject();
                            ObjectMail.id = hdon_id;
                            ObjectMail.nguoinhan = email;
                            ObjectMail.tieude = tieude;

                            arrayMail.Add(ObjectMail);


                            #endregion
                        }

                        // Update date trạng thái ký thành công trong trường hợp ký lại các hoá đơn bị lỗi
                        sql = $"UPDATE #SCHEMA_NAME#.wb_log_sign SET resign_success = true WHERE hdon_id = '{hdonid}'";
                        _minvoiceDbContext.ExecuteNoneQuery(sql);
                    }
                    catch (Exception ex)
                    {
                        // error += ex.Message + "; ";

                        try
                        {
                            await _minvoiceDbContext.TransactionRollbackAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        catch { }

                        // Xử lý khi ký bị lỗi
                        var hdonid = arrhdon_id[i].ToString();


                        var sqlCheckExist =
                            $"SELECT COUNT(1) FROM #SCHEMA_NAME#.wb_log_sign WHERE hdon_id = '{hdonid}'";
                        var rsTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheckExist);

                        if (!reSign && rsTable.Rows[0].Field<long>("count") == 0)
                        {
                            var sqlUpdate =
                                $"INSERT INTO #SCHEMA_NAME#.wb_log_sign (hdon_id, error_log) VALUES ('{hdonid}', '{ex.Message}')";
                            _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
                        }
                        else
                        {
                            var sqlUpdate = $"UPDATE #SCHEMA_NAME#.wb_log_sign SET sign_retry = sign_retry + 1 WHERE hdon_id = '{hdonid}'";
                            _minvoiceDbContext.ExecuteNoneQuery(sqlUpdate);
                        }

                        if ("E0002".Equals(ex.Message))
                        {
                            // res.Add("error", "Tồn tại hóa đơn không ở trạng thái chờ ký");
                            // return res;
                            error += "Tồn tại hóa đơn không ở trạng thái chờ ký" + "; ";
                        }
                        else if ("E0001".Equals(ex.Message))
                        {
                            // res.Add("error", "Bạn chưa xử lý hóa đơn của ngày hôm trước");
                            // return res;
                            error += "Bạn chưa xử lý hóa đơn của ngày hôm trước" + "; ";
                        }
                        else
                        {
                            error += ex.Message + "; ";
                        }

                        // res.Add("error", ex.Message);
                        // Log.Error(ex.Message);
                        // return res;
                        break;
                    }
                }

                if (arrayMail.Count > 0)
                {
                    Dictionary<string, object> dicPara = new Dictionary<string, object>();
                    dicPara.Add("branch_code", branch_code);
                    sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                    DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);
                    if (tblSetting.Rows.Count > 0)
                    {
                        string value = tblSetting.Rows[0]["value"].ToString();
                        if (value == "C")
                        {
                            ThreadPool.QueueUserWorkItem(state => ExecuteSendMailHdon(folder, arrayMail));
                        }
                    }
                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }

            return res;
        }

        public async Task<JObject> SaveBangKeBanRa(JObject obj, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {
                        if (masterObj["trang_thai"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["trang_thai"].ToString()))
                            {
                                masterObj["trang_thai"] = "0";
                            }
                        }
                        else
                        {
                            masterObj.Add("trang_thai", "0");
                        }

                        if (masterObj["nglap"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["nglap"].ToString()))
                            {
                                masterObj["nglap"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("nglap", username);
                        }

                        if (masterObj["nlap"] != null)
                        {
                            masterObj["nlap"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (masterObj["bangkebanra_68_id"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["bangkebanra_68_id"].ToString()))
                            {
                                masterObj["bangkebanra_68_id"] = id;
                            }
                            else
                            {
                                id = Guid.Parse(masterObj["bangkebanra_68_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add("bangkebanra_68_id", id);
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                    }

                    if (editMode == "2")
                    {
                        ////if (masterObj["nglap"] != null)
                        ////{
                        ////    string user_new = masterObj["hdon_id"].ToString();

                        ////    if (isedituser != "C" && user_new != username)
                        ////    {
                        ////        json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                        ////        return json;
                        ////    }
                        ////}

                        //if (masterObj["ngsua"] != null)
                        //{
                        //    if (string.IsNullOrEmpty(masterObj["ngsua"].ToString()))
                        //    {
                        //        masterObj["ngsua"] = username;
                        //    }
                        //}
                        //else
                        //{
                        //    masterObj.Add("ngsua", username);
                        //}

                        //if (masterObj["nsua"] != null)
                        //{
                        //    masterObj["nsua"] = DateTime.Now;
                        //}
                        //else
                        //{
                        //    masterObj.Add("nsua", DateTime.Now);
                        //}

                        id = Guid.Parse(masterObj["bangkebanra_68_id"].ToString());

                    }

                    if (editMode == "3")
                    {
                        //if (masterObj["nglap"] != null)
                        //{
                        //    string user_new = masterObj["nglap"].ToString();

                        //    if (isdeluser != "C" && user_new != username)
                        //    {
                        //        json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                        //        return json;
                        //    }
                        //}
                    }
                    #endregion

                    string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_bangkebanra_68_insert" : editMode == "2" ? "#SCHEMA_NAME#.crd_bangkebanra_68_update" : "#SCHEMA_NAME#.crd_bangkebanra_68_delete";

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    #region Chi tiết hóa đơn
                    if (details != null)
                    {
                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            sql = "DELETE FROM #SCHEMA_NAME#.bangkebanra_68_chitiet WHERE bangkebanra_68_id = '" + masterObj["bangkebanra_68_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);

                            for (int j = 0; j < obj_details.Count; j++)
                            {
                                JObject js_details = (JObject)obj_details[j];

                                if (js_details["bangkebanra_68_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["bangkebanra_68_id"].ToString()))
                                    {
                                        js_details["bangkebanra_68_id"] = masterObj["bangkebanra_68_id"];
                                    }
                                }
                                else
                                {
                                    js_details.Add("bangkebanra_68_id", masterObj["bangkebanra_68_id"]);
                                }

                                if (js_details["bangkebanra_68_chitiet_id"] != null)
                                {
                                    if (string.IsNullOrEmpty(js_details["bangkebanra_68_chitiet_id"].ToString()))
                                    {
                                        js_details["bangkebanra_68_chitiet_id"] = Guid.NewGuid();
                                    }
                                }
                                else
                                {
                                    js_details.Add("bangkebanra_68_chitiet_id", Guid.NewGuid());
                                }

                                //if (fieldStt_rec0 != null)
                                //{
                                //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                                //}

                                await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_bangkebanra_68_chitiet_insert", CommandType.StoredProcedure, js_details);
                            }
                        }

                    }
                    #endregion

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }

                if (editMode != "3")
                {
                    //string select_cmd = $"SELECT bangkebanra_68_id as id , * FROM #SCHEMA_NAME#.bangkebanra_68 WHERE bangkebanra_68_id ='{id}'";
                    //DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);
                    //data = JArray.FromObject(tbl);
                    //json.Add("data", data[0]);
                }

                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JArray> GetDataBangKeBanRaDetail(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.bangkebanra_68_chitiet WHERE bangkebanra_68_id = '" + id + "' ORDER BY Cast (stt as numeric)";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));
            return result;
        }

        public async Task<JObject> SignBangKeBanRa(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                Guid bangkebanra_id = Guid.Parse(model["id"].ToString());
                string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                        + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                        + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                if (branch_code.Length > 0)
                {
                    sql += " AND a.branch_code='" + branch_code + "' ";
                }

                if (cert_serial.Length > 0)
                {
                    sql += " AND a.cer_serial='" + cert_serial + "'";
                }

                sql += " ORDER BY end_date desc LIMIT 1";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    res.Add("error", "Không tìm thấy chứng thư số");
                }

                X509Certificate2 certFile = null;
                try
                {
                    string pass = tblCertificate.Rows[0]["pass"].ToString();
                    byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                    certFile = new X509Certificate2(bytes, pass);
                }
                catch (Exception)
                {
                }

                await _minvoiceDbContext.BeginTransactionAsync();
                DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangkebanra_68 WHERE bangkebanra_68_id ='" + bangkebanra_id + "'");

                JObject ObjXml = await XmlBKBR(model["id"].ToString());

                //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                string xml = ObjXml["xml"].ToString();

                if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                {
                    xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                    SignXML input = new SignXML()
                    {
                        msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                        xmlData = xml,
                        namespacePrefix = "",
                        reason = "Ki bang ke ban ra",
                        signatureTagId = "seller",
                        signingTagId = "data",
                        signingTagName = "NNT",
                        prompt = "Ki bang ke ban ra"
                    };
                    SignedXmlReturn resultSign = CommonManager.SignSimPKI(input);
                    //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                    xml = resultSign.xmlSignedData;
                }
                else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                {
                    xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NNT", "xml",
                        xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                }
                else
                {
                    xml = CommonManager.SignXml(await getTenDv(), "#data", "seller", xml, certFile, "NNT");
                }

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.bangkebanra_68 SET trang_thai=@trang_thai, xml_send = @xml_send, ngay_ky = @ngay_ky, ngky = @ngky WHERE bangkebanra_68_id = @bangkebanra_68_id";
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("trang_thai", 1);
                parameters2.Add("xml_send", xml);
                parameters2.Add("bangkebanra_68_id", bangkebanra_id);
                parameters2.Add("ngay_ky", DateTime.Now);
                parameters2.Add("ngky", username);

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                //JObject parameters = null;
                //await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                //JObject rabbitObj = new JObject
                //{
                //    //{ "task_name", _queueNameInvoice},
                //    //{ "msg" , new JObject {
                //            {"cmd", "401" },
                //            {"data", new JObject {
                //                    {"inv_id", bangkebanra_id },
                //                    {"mst", await getMstDv() },
                //                    {"mngui", "K" },
                //                    {"xml", xml }
                //                }
                //            }
                //    //    }
                //    //}
                //};
                //// đẩy lên cơ quan thuế
                //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());


                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();


            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async Task<JObject> XmlBangKeBanRa(string id)
        {
            JObject _res = new JObject();
            try
            {
                DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangkebanra_68 WHERE bangkebanra_68_id ='" + id + "'");

                JObject ObjXml = await XmlBKBR(id);

                //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                string xml = ObjXml["xml"].ToString();
                _res.Add("xml", xml);
                _res.Add("ok", true);

            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _res;
        }

        public async Task<JObject> SaveXmlBangKeBanRa(JObject model)
        {
            JObject _res = new JObject();
            try
            {
                string xml = model["xml"].ToString();
                Guid bangkebanra_id = Guid.Parse(model["id"].ToString());
                Int32 shdon = Convert.ToInt32(model["shdon"].ToString());
                await _minvoiceDbContext.BeginTransactionAsync();

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.bangkebanra_68 SET trang_thai=@trang_thai, xml_send = @xml_send, ngay_ky = @ngay_ky WHERE bangkebanra_68_id = @bangkebanra_68_id";
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("trang_thai", 1);
                parameters2.Add("xml_send", xml);
                parameters2.Add("bangkebanra_68_id", bangkebanra_id);
                parameters2.Add("ngay_ky", DateTime.Now);

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                //JObject parameters = null;
                //await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                //JObject rabbitObj = new JObject
                //{
                //    //{ "task_name", _queueNameInvoice},
                //    //{ "msg" , new JObject {
                //            {"cmd", "401" },
                //            {"data", new JObject {
                //                    {"inv_id", bangkebanra_id },
                //                    {"mst", await getMstDv() },
                //                    {"mngui", "K" },
                //                    {"xml", xml }
                //                }
                //            }
                //    //    }
                //    //}
                //};
                //// đẩy lên cơ quan thuế
                //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());


                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
                Log.Error(e.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }
            return _res;
        }

        public async Task<JObject> XmlBKBR(string bangkebanra_68_id)
        {
            JObject _obj = new JObject();
            try
            {

                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangkebanra_68 WHERE bangkebanra_68_id = '{bangkebanra_68_id}'";
                string thang = "";
                string ngay = "";
                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["thang"].ToString()))
                {
                    int thangDT = Convert.ToInt16(dauphieu.Rows[0]["thang"].ToString());
                    thang = thangDT.ToString();
                    if (thangDT < 10)
                    {
                        thang = "0" + thangDT;
                    }
                }

                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay"].ToString()))
                {
                    int ngayDT = Convert.ToInt16(dauphieu.Rows[0]["ngay"].ToString());
                    ngay = ngayDT.ToString();
                    if (ngayDT < 10)
                    {
                        ngay = "0" + ngayDT;
                    }
                }
                string sqlDetails = $"SELECT * FROM #SCHEMA_NAME#.bangkebanra_68_chitiet WHERE bangkebanra_68_id ='{bangkebanra_68_id}' order by nhom";
                DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                DateTime dt = Convert.ToDateTime(dauphieu.Rows[0]["nlap"].ToString());


                XElement ndTKhai = new XElement("NDTKhai");

                var tsuat = "ThueSuat";
                double tgdtcthue = 0;
                double tgtgtgt = 0;

                XElement aRow = new XElement("TTSuat");

                foreach (DataRow row in chitiet.Rows)
                {
                    if (tsuat != row["tsuat"].ToString())
                    {
                        if (tsuat != "ThueSuat")
                        {
                            aRow.Add(new XElement("TgDTCThue", String.Format("{0:0.0000}", tgdtcthue)),
                                        new XElement("TgTGTGT", String.Format("{0:0.0000}", tgtgtgt)));

                            ndTKhai.Add(aRow);
                            tgdtcthue = 0;
                            tgtgtgt = 0;
                        }

                        aRow = new XElement("TTSuat");
                        tsuat = row["tsuat"].ToString();
                        aRow.Add(new XElement("TTSuat", tsuat));
                        aRow.Add(new XElement("DSHDon"));
                    }

                    tgdtcthue += row["dtcthue"] == null || row["dtcthue"].ToString() == "" ? 0 : Convert.ToDouble(row["dtcthue"]);
                    tgtgtgt += row["tgtgt"] == null || row["tgtgt"].ToString() == "" ? 0 : Convert.ToDouble(row["tgtgt"]);

                    XElement DsHDon = aRow.Element("DSHDon");

                    DsHDon.Add(
                        new XElement("HDon",
                            new XElement("STT", row["stt"]?.ToString()),
                            new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                            new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                            GetDataRow("SHDon", row, "shdon"),
                            GetDataRowDate("NLap", row, "nlap"),
                            GetDataRow("TNMua", row, "tnmua"),
                            GetDataRow("MSTNMua", row, "mstnmua"),
                            GetDataRow("DTCThue", row, "dtcthue"),
                            GetDataRow("TGTGT", row, "tgtgt"),
                            GetDataRow("TThai", row, "tthai"),
                            GetDataRow("LHDCLQuan", row, "lhdlquan"),
                            row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                            row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                            GetDataRow("SHDCLQuan", row, "shdlquan"),
                            GetDataRow("STBao", row, "stbao"),
                            GetDataRowDate("NTBao", row, "ntbao"),
                            GetDataRow("GChu", row, "gchu")
                        )
                    );

                }

                if (tsuat != "")
                {
                    aRow.Add(new XElement("TgDTCThue", String.Format("{0:0.0000}", tgdtcthue)),
                                         new XElement("TgTGTGT", String.Format("{0:0.0000}", tgtgtgt)));

                    ndTKhai.Add(aRow);
                    tgdtcthue = 0;
                    tgtgtgt = 0;
                }

                ndTKhai.Add(GetDataRow("TgDThu", dauphieu.Rows[0], "tgtcthue"),
                                GetDataRow("TgThue", dauphieu.Rows[0], "tgtthue"));

                XDocument xmlBKBR = new XDocument(
                    new XElement("TKhai",
                        new XElement("DLTKhai",
                            new XElement("TTChung", new XAttribute("Id", "data"),
                                new XElement("PBan", "2.0.0"),
                                new XElement("MSo", "03/DL-HĐĐT"),
                                new XElement("Ten", "Tờ khai dữ liệu hóa đơn, chứng từ hàng hoá, dịch vụ bán ra đến cơ quan thuế"),
                                GetDataRow("LKTThue", dauphieu.Rows[0], "lkdlieu"),
                                new XElement("KTThue", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                GetDataRow("LDau", dauphieu.Rows[0], "ldau"),
                                GetDataRow("BSLThu", dauphieu.Rows[0], "bslthu"),
                               GetDataRow("DDanh", dauphieu.Rows[0], "ddanh"),
                                new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                GetDataRow("TNNT", dauphieu.Rows[0], "tnnt"),
                               GetDataRow("MST", dauphieu.Rows[0], "mst"),
                               GetDataRow("TDLThue", dauphieu.Rows[0], "tdlthue"),
                               GetDataRow("MSTDLThue", dauphieu.Rows[0], "mstdlthue"),
                               GetDataRow("DVTTe", dauphieu.Rows[0], "dvtte")
                            ),
                            ndTKhai
                        ),
                        new XElement("DSCKS",
                            new XElement("NNT"),
                            new XElement("CCKSKhac")
                        )
                    )
                );

                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBKBR.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }


        public async Task<JObject> XmlBKBRIn(string bangkebanra_68_id)
        {
            JObject _obj = new JObject();
            try
            {

                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangkebanra_68 WHERE bangkebanra_68_id = '{bangkebanra_68_id}'";
                string thang = "";
                string ngay = "";
                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["thang"].ToString()))
                {
                    int thangDT = Convert.ToInt16(dauphieu.Rows[0]["thang"].ToString());
                    thang = thangDT.ToString();
                    if (thangDT < 10)
                    {
                        thang = "0" + thangDT;
                    }
                }
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay"].ToString()))
                {
                    int ngayDT = Convert.ToInt16(dauphieu.Rows[0]["ngay"].ToString());
                    ngay = ngayDT.ToString();
                    if (ngayDT < 10)
                    {
                        ngay = "0" + ngayDT;
                    }
                }
                string sqlDetails = $"SELECT * FROM #SCHEMA_NAME#.bangkebanra_68_chitiet WHERE bangkebanra_68_id ='{bangkebanra_68_id}' order by nhom";
                DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                DateTime dt = Convert.ToDateTime(dauphieu.Rows[0]["nlap"].ToString());

                XDocument xmlBKBR = new XDocument(
                    new XElement("TKhai",
                        new XElement("DLTKhai",
                            new XElement("TTChung", new XAttribute("Id", "data"),
                                new XElement("PBan", "2.0.0"),
                                new XElement("MSo", "03/DL-HĐĐT"),
                                new XElement("Ten", "Tờ khai dữ liệu hóa đơn, chứng từ hàng hoá, dịch vụ bán ra đến cơ quan thuế"),
                                GetDataRow("LKDLieu", dauphieu.Rows[0], "lkdlieu"),
                                new XElement("KDLieu", dauphieu.Rows[0]["lkdlieu"].ToString() == "Q" ? "Quý " + thang + " năm " + dauphieu.Rows[0]["nam"].ToString() : dauphieu.Rows[0]["lkdlieu"].ToString() == "T" ? "Tháng " + thang + " năm " + dauphieu.Rows[0]["nam"].ToString() : "Ngày " + ngay + " tháng " + thang + " năm " + dauphieu.Rows[0]["nam"].ToString()),
                                GetDataRow("LDau", dauphieu.Rows[0], "ldau"),
                                GetDataRow("BSLThu", dauphieu.Rows[0], "bslthu"),
                                GetDataRow("DDanh", dauphieu.Rows[0], "ddanh"),
                                new XElement("NLap", dt.ToString("dd/MM/yyyy")),
                                GetDataRow("TNNT", dauphieu.Rows[0], "tnnt"),
                                GetDataRow("MST", dauphieu.Rows[0], "mst"),
                                GetDataRow("TDLThue", dauphieu.Rows[0], "tdlthue"),
                                GetDataRow("MSTDLThue", dauphieu.Rows[0], "mstdlthue"),
                                GetDataRow("DVTTe", dauphieu.Rows[0], "dvtte"),
                                GetDataRowDateTime("TNgay", dauphieu.Rows[0], "tu_ngay"),
                                GetDataRowDateTime("DNgay", dauphieu.Rows[0], "den_ngay"),
                                new XElement("TgDTCThue", dauphieu.Rows[0]["tgtcthue"] == null || dauphieu.Rows[0]["tgtcthue"].ToString() == "" ? 0 : Convert.ToDouble(dauphieu.Rows[0]["tgtcthue"])),
                                new XElement("TgTGTGT", dauphieu.Rows[0]["tgtthue"] == null || dauphieu.Rows[0]["tgtthue"].ToString() == "" ? 0 : Convert.ToDouble(dauphieu.Rows[0]["tgtthue"]))
                            ),
                            new XElement("NDTKhai",
                                new XElement("DSHDon",
                                    from row in chitiet.AsEnumerable()
                                    select new XElement("HDon",
                                    new XElement("STT", row["stt"]?.ToString()),
                                    new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                    new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                    GetDataRow("SHDon", row, "shdon"),
                                    GetDataRowDate("NLap", row, "nlap"),
                                    GetDataRow("TNMua", row, "tnmua"),
                                    GetDataRow("MSTNMua", row, "mstnmua"),
                                    new XElement("DTCThue", row["dtcthue"] == null || row["dtcthue"].ToString() == "" ? 0 : Convert.ToDouble(row["dtcthue"])),
                                    new XElement("TGTGT", row["tgtgt"] == null || row["tgtgt"].ToString() == "" ? 0 : Convert.ToDouble(row["tgtgt"])),
                                    GetDataRow("TThai", row, "tthai"),
                                    GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                    GetDataRow("SHDCLQuan", row, "shdlquan"),
                                    GetDataRow("STBao", row, "stbao"),
                                    GetDataRowDate("NTBao", row, "ntbao"),
                                    GetDataRow("GChu", row, "gchu"),
                                    GetDataRow("Nhom", row, "nhom")
                                    )
                                )
                            )
                        ),
                        new XElement("DSCKS",
                            new XElement("NNT"),
                            new XElement("CCKSKhac")
                        )
                    )
                );

                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBKBR.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        public async Task<byte[]> inBangKeBanRa(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";

            try
            {

                JObject x = await XmlBKBRIn(id);
                xml = x["xml"].ToString();

                DataSet ds = new DataSet();
                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, null);

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }

        public async Task<string> ExportXMLBangKeBanRa(string id)
        {
            string qry = $"SELECT xml_send FROM #SCHEMA_NAME#.bangkebanra_68 WHERE bangkebanra_68_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<JArray> GetInfoCompany(string branch_code)
        {
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("code", branch_code);

            string sql = "SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code=@code";

            DataTable tblData = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameter);

            return JArray.FromObject(tblData);
        }

        public async Task<string> ExportXMLBTHDLRep(string id)
        {
            string qry = $"SELECT xml_nhan FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{id}'";
            DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
            return dt.Rows[0][0].ToString();
        }

        public async Task<byte[]> inBTHDLRep(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id='" + id + "'");

                if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_nhan"].ToString()))
                {
                    xml = tblHoadon.Rows[0]["xml_nhan"].ToString();
                }
                else
                {
                    throw new Exception("Chưa có dữ liệu tiếp nhận");
                }

                DataSet ds = new DataSet();

                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, null);

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }


        public async Task<string> XmlDangkysudungIn(string id)
        {
            DataTable data = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68 WHERE mau01_id ='" + id + "'");
            DataTable dataDetails = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68_chitiet WHERE mau01_id ='" + id + "'");
            DataTable dataUyNhiems = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.mau01_68_uynhiem WHERE mau01_id ='" + id + "'");

            var datadangkysdung = dataDetails.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "mau01_id" && c.ToString() != "mau01_chitiet_id").ToList();
            var datadangkyuynhiem = dataUyNhiems.Columns.Cast<DataColumn>().Select(dc => dc.ColumnName).Where(c => c.ToString() != "mau01_id" && c.ToString() != "mau01_uynhiem_id" && c.ToString() != "thunhiem" && c.ToString() != "ket_qua").ToList();

            Dictionary<string, string> headerCTS = new Dictionary<string, string>();
            headerCTS.Add("stt", "STT");
            headerCTS.Add("ttchuc", "TTChuc");
            headerCTS.Add("seri", "Seri");
            headerCTS.Add("tngay", "TNgay");
            headerCTS.Add("dngay", "DNgay");
            headerCTS.Add("hthuc", "HThuc");

            Dictionary<string, string> headeruyNhiem = new Dictionary<string, string>();
            headeruyNhiem.Add("stt", "STT");
            headeruyNhiem.Add("tlhdunhiem", "TLHDon");
            headeruyNhiem.Add("khmhdon", "KHMSHDon");
            headeruyNhiem.Add("khhdunhiem", "KHHDon");
            headeruyNhiem.Add("mstunhiem", "MST");
            headeruyNhiem.Add("ttcdunhiem", "TTChuc");
            headeruyNhiem.Add("mdunhiem", "MDich");
            headeruyNhiem.Add("tngay", "TNgay");
            headeruyNhiem.Add("dngay", "DNgay");
            headeruyNhiem.Add("pttthdunhiem", "PThuc");


            //DateTime dt;
            //DateTime.TryParseExact(data.Rows[0]["nlap"].ToString(), "MM/dd/yyyy hh:mm:ss tt",
            //               CultureInfo.InvariantCulture, DateTimeStyles.None,
            //               out dt);
            String ngay_lap = DateTime.Parse(data.Rows[0]["nlap"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            XDocument doc = new XDocument();

            doc = new XDocument(
               new XElement("TKhai",
                   new XElement("DLTKhai", new XAttribute("Id", "data"),
                       new XElement("TTChung",
                           new XElement("PBan", "2.0.0"),
                           new XElement("MSo", "01/ĐKTĐ-HĐĐT"),
                           new XElement("Ten", "Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử"),
                           GetDataRow("HThuc", data.Rows[0], "loai"), // 1 đăng ký, 2 thay đổi
                           GetDataRow("TNNT", data.Rows[0], "tnnt"),
                           GetDataRow("MST", data.Rows[0], "mst"),
                           GetDataRow("CQTQLy", data.Rows[0], "cqtqly"),
                           GetDataRow("MCQTQLy", data.Rows[0], "mcqtqly"),
                           GetDataRow("NLHe", data.Rows[0], "nlhe"),
                           GetDataRow("DCLHe", data.Rows[0], "dclhe"),
                           GetDataRow("DCTDTu", data.Rows[0], "dctdtu"),
                          GetDataRow("DTLHe", data.Rows[0], "dtlhe"),
                           GetDataRow("DDanh", data.Rows[0], "ddanh"),
                           new XElement("NLap", ngay_lap)
                       ),
                       new XElement("NDTKhai",
                           new XElement("HTHDon",
                               GetDataRow("CMa", data.Rows[0], "adhdcma"),
                               GetDataRow("KCMa", data.Rows[0], "adhdkma")
                           ),
                           new XElement("HTGDLHDDT",
                               GetDataRow("NNTDBKKhan", data.Rows[0], "htgdlhddtcmkkhan"),
                               GetDataRow("NNTKTDNUBND", data.Rows[0], "htgdlhddtcmkcncao"),
                               GetDataRow("CDLTTDCQT", data.Rows[0], "htgdlhddtkmttiep"),
                               GetDataRow("CDLQTCTN", data.Rows[0], "htgdlhddtkmtchuc")
                           ),
                           new XElement("PThuc",
                                GetDataRow("CDDu", data.Rows[0], "ptcdlhdndthdon"),
                               GetDataRow("CBTHop", data.Rows[0], "ptcdlhdbthop")
                           ),
                           new XElement("LHDSDung",
                               GetDataRow("HDGTGT", data.Rows[0], "hdgtgt"),
                               GetDataRow("HDBHang", data.Rows[0], "hdbhang"),
                               GetDataRow("HDBTSCong", data.Rows[0], "hdbtscong"),
                               GetDataRow("HDBHDTQGia", data.Rows[0], "hdbhdtqgia"),
                               GetDataRow("HDKhac", data.Rows[0], "hdkhac"),
                               GetDataRow("CTu", data.Rows[0], "qlnhdon")
                           ),
                           new XElement("DSCTSSDung",
                               dataDetails.AsEnumerable().Select(row => new XElement("CTS",
                                    datadangkysdung.Select(x => GetDataRow(headerCTS[x], row, x))
                                    )
                                )
                           ),
                            new XElement("DSDKUNhiem",
                               dataUyNhiems.AsEnumerable().Select(row => new XElement("DKUNhiem",
                                    datadangkyuynhiem.Select(x => GetDataRow(headeruyNhiem[x], row, x))
                                    )
                                )
                           )
                       )
                   ),
                   new XElement("DSCKS",
                       new XElement("NNT"),
                       new XElement("CCKSKhac")
                   )
               )
           );

            string xml = string.Empty;
            var writerSettings = new XmlWriterSettings();
            writerSettings.OmitXmlDeclaration = true;

            using (var buffer = new StringWriter())
            using (var writer = XmlWriter.Create(buffer, writerSettings))
            {
                doc.Save(writer);
                writer.Flush();
                xml = buffer.ToString();
            }
            return xml;
        }

        public async Task<JObject> GetDataTruyenNhan(string sql, int start, int count, string filter, string tggui_tu, string tggui_den, int check_error)
        {
            JObject json = new JObject();
            try
            {
                if (filter == "null") filter = null;
                string queryWhere = "";
                string queryPage = $"  OFFSET {start} ROWS FETCH NEXT {count} ROW only";
                queryWhere = $"  xml_thongdiep is not null ";
                string queryOrder = $" order by tg_gui desc, A.stt desc ";

                if (!string.IsNullOrEmpty(filter) && filter != "null")
                {
                    queryWhere += $" and (mtdiep_gui like '%{filter}%' or mtdiep_thamchieu like '%{filter}%' or mngui like '%{filter}%' or vendor like '%{filter}%' or mngui_ft like '%{filter}%') ";
                }
                if (!string.IsNullOrEmpty(tggui_tu) && tggui_tu != "null")
                {
                    queryWhere += $" and  tg_gui >= '{tggui_tu}' ";
                }
                if (!string.IsNullOrEmpty(tggui_den) && tggui_den != "null")
                {
                    queryWhere += $" and tg_gui <= '{tggui_den}' ";
                }
                if (check_error == 1)
                {
                    queryWhere += $" and (xml_thongdiep like '%LDo%' or xml_thongdiep like '%Ldo%') and xml_thongdiep like '%MLoi%' ";
                }

                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("p_condition", queryWhere);
                parameter.Add("p_page", queryPage);
                parameter.Add("p_order", queryOrder);
                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("hdon68_log_truyennhan", CommandType.StoredProcedure, parameter);

                Dictionary<string, object> parameterCount = new Dictionary<string, object>();
                parameterCount.Add("p_condition", queryWhere);
                DataTable tblDataCount = await _minvoiceDbContext.GetDataTableAsync("hdon68_log_truyenhan_count", CommandType.StoredProcedure, parameterCount);

                JArray data = JArray.FromObject(tblData);
                json.Add("data", data);
                json.Add("pos", start);
                if (start == 0)
                    json.Add("total_count", tblDataCount.Rows[0]["total"].ToString());
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }
        public async Task<JObject> GetDataTruyenNhan_Chitiet(string sql, string id)
        {
            JObject json = new JObject();
            try
            {
                string queryWhere = "";
                queryWhere = $" A.reg_id = '{id}'::uuid and xml_thongdiep is not null ";
                string queryOrder = $" order by tg_gui, A.stt  ";

                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("p_condition", queryWhere);
                parameter.Add("p_page", "");
                parameter.Add("p_order", queryOrder);
                DataTable tblData = await _minvoiceDbContext.GetDataTableAsync("hdon68_log_truyennhan", CommandType.StoredProcedure, parameter);

                JArray data = JArray.FromObject(tblData);
                json.Add("data", data);
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }

        public async Task<JObject> CreateHoaDonSendTCT(JObject obj, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                //await _minvoiceDbContext.BeginTransactionAsync();
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];
                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {
                        if (masterObj["tthdon"] == null)
                        {
                            masterObj.Add("tthdon", 0);
                        }

                        if (masterObj["shdon"] != null)
                        {
                            masterObj["shdon"] = null;
                        }
                        else
                        {
                            masterObj.Add("shdon", null);
                        }

                        if (masterObj["tthai"] != null)
                        {
                            masterObj["tthai"] = "Chờ ký";
                        }
                        else
                        {
                            masterObj.Add("tthai", "Chờ ký");
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                        if (masterObj["nglap"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["nglap"].ToString()))
                            {
                                masterObj["nglap"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("nglap", username);
                        }

                        if (masterObj["nlap"] == null)
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (masterObj["hdon_id"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["hdon_id"].ToString()))
                            {
                                masterObj["hdon_id"] = id;
                            }
                            else
                            {
                                id = Guid.Parse(masterObj["hdon_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add("hdon_id", id);
                        }
                    }

                    if (editMode == "2")
                    {
                        if (masterObj["ngsua"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["ngsua"].ToString()))
                            {
                                masterObj["ngsua"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("ngsua", username);
                        }

                        if (masterObj["nsua"] != null)
                        {
                            masterObj["nsua"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nsua", DateTime.Now);
                        }

                        id = Guid.Parse(masterObj["hdon_id"].ToString());

                    }

                    if (editMode == "3")
                    {

                    }
                    #endregion
                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }
                    if (editMode != "1" && editMode != "2")
                    {
                        string sql1 = "#SCHEMA_NAME#.crd_hdon68_delete";
                        await _minvoiceDbContext.BeginTransactionAsync();
                        await this._minvoiceDbContext.TransactionCommandAsync(sql1, CommandType.StoredProcedure, masterObj);
                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                    }
                    else if (details != null && details.Count > 0 && ((JArray)(details[0]["data"])).Count > 0)
                    {
                        string sql2 = editMode == "1" ? "#SCHEMA_NAME#.crd_hdon68_insert_n" : editMode == "2" ? "#SCHEMA_NAME#.crd_hdon68_update_n" : "#SCHEMA_NAME#.crd_hdon68_delete";

                        await _minvoiceDbContext.BeginTransactionAsync();
                        await this._minvoiceDbContext.TransactionCommandAsync(sql2, CommandType.StoredProcedure, masterObj);
                        if (editMode == "1" || editMode == "2")
                        {
                            sql2 = "#SCHEMA_NAME#.crd_hdon68_update_pt";
                            await _minvoiceDbContext.TransactionCommandAsync(sql2, CommandType.StoredProcedure, masterObj);
                        }

                        #region Chi tiết hóa đơn

                        foreach (var dtl in details)
                        {
                            JArray obj_details = (JArray)dtl["data"];
                            string sql3 = "DELETE FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                            await this._minvoiceDbContext.TransactionCommandAsync(sql3, CommandType.Text, (JObject)null);

                            if (obj_details.Count > 0)
                            {
                                for (int j = 0; j < obj_details.Count; j++)
                                {
                                    JObject js_details = (JObject)obj_details[j];

                                    if (js_details["hdon_id"] != null)
                                    {
                                        if (string.IsNullOrEmpty(js_details["hdon_id"].ToString()))
                                        {
                                            js_details["hdon_id"] = masterObj["hdon_id"];
                                        }
                                    }
                                    else
                                    {
                                        js_details.Add("hdon_id", masterObj["hdon_id"]);
                                    }

                                    if (js_details["cthdon_id"] != null)
                                    {
                                        if (string.IsNullOrEmpty(js_details["cthdon_id"].ToString()))
                                        {
                                            js_details["cthdon_id"] = Guid.NewGuid();
                                        }
                                    }
                                    else
                                    {
                                        js_details.Add("cthdon_id", Guid.NewGuid());
                                    }

                                    await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_chitiet_insert", CommandType.StoredProcedure, js_details);
                                }
                            }

                        }
                        #region hatm luuthuephi
                        JArray phi = null;

                        if (masterObj["hoadon68_phi"] != null)
                        {
                            if (masterObj.SelectToken("hoadon68_phi") is JArray)
                            {
                                phi = (JArray)masterObj["hoadon68_phi"];
                            }
                        }

                        if (phi != null)
                        {
                            foreach (var dtl in phi)
                            {
                                JArray obj_phi = (JArray)dtl["data"];
                                if (obj_phi.Count > 0)
                                {
                                    string sql4 = "DELETE FROM #SCHEMA_NAME#.hoadon68_phi WHERE hdon_id = '" + masterObj["hdon_id"] + "'";
                                    await this._minvoiceDbContext.TransactionCommandAsync(sql4, CommandType.Text, (JObject)null);

                                    if (obj_phi.Count > 0)
                                    {
                                        for (int j = 0; j < obj_phi.Count; j++)
                                        {
                                            JObject js_phi = (JObject)obj_phi[j];

                                            if (js_phi["hdon_id"] != null)
                                            {
                                                if (string.IsNullOrEmpty(js_phi["hdon_id"].ToString()))
                                                {
                                                    js_phi["hdon_id"] = masterObj["hdon_id"];
                                                }
                                            }
                                            else
                                            {
                                                js_phi.Add("hdon_id", masterObj["hdon_id"]);
                                            }
                                            js_phi["phi_id"] = Guid.NewGuid();

                                            await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_hdon68_phi_insert_n", CommandType.StoredProcedure, js_phi);
                                        }
                                    }
                                }
                            }
                        }
                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                        #endregion
                        if (editMode != "3")
                        {
                            string select_cmd = $"SELECT hdon_id as id , * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='{id}'";
                            DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);
                            data = JArray.FromObject(tbl);
                            json.Add("data", data[0]);
                        }

                        #endregion
                    }
                    else
                    {
                        json.Add("error", "Cần bổ sung chi tiết hóa đơn.");
                    }


                    // ký hóa đơn
                    #region ký hóa đơn
                    string branch_code = _webHelper.GetDvcs();
                    string cmd = obj["type_cmd"].ToString();
                    string cert_serial = obj["cert_serial"].ToString();


                    DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                    if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                    {
                        json.Add("error", "Bạn không có quyền ký hóa đơn");
                        return json;
                    }

                    Guid hdon_id = id;

                    string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                         + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                         + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                         + "WHERE a.cert_type in ('FILE','SIM', 'HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                    if (branch_code.Length > 0)
                    {
                        sql += " AND a.branch_code='" + branch_code + "' ";
                    }

                    if (cert_serial.Length > 0)
                    {
                        sql += " AND a.cer_serial='" + cert_serial + "'";
                    }

                    sql += " ORDER BY end_date desc LIMIT 1";

                    DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                    if (tblCertificate.Rows.Count == 0)
                    {
                        json.Add("error", "Không tìm thấy chứng thư số");
                        return json;
                    }

                    X509Certificate2 certFile = null;
                    try
                    {
                        string pass = tblCertificate.Rows[0]["pass"].ToString();
                        byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                        certFile = new X509Certificate2(bytes, pass);
                    }
                    catch (Exception)
                    {
                    }

                    await _minvoiceDbContext.BeginTransactionAsync();
                    DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hdon_id + "'");

                    string shdon = dtHdon.Rows[0]["shdon"]?.ToString();
                    string khieu = dtHdon.Rows[0]["khieu"]?.ToString();

                    JObject ObjXml = await XmlInvoice(id.ToString());

                    //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                    string xml = ObjXml["xml"].ToString();
                    string tdlap = ObjXml["tdlap"].ToString().Replace("'", "''");
                    string tdky = ObjXml["tdky"].ToString().Replace("'", "''");

                    if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                    {
                        xml = CommonManager.ThemPhanDonViKiThoiGianKy(xml, await getTenDv());
                        SignXML input = new SignXML()
                        {
                            msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                            xmlData = xml,
                            namespacePrefix = "",
                            reason = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                            signatureTagId = "seller",
                            signingTagId = "data",
                            signingTagName = "NBan",
                            prompt = "Ky hoa don dien tu so " + shdon + " ky hieu " + khieu,
                        };
                        SignedXmlReturn result = CommonManager.SignSimPKI(input);
                        //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                        xml = result.xmlSignedData;
                    }
                    else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                    {
                        xml = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NBan", "xml",
                            xml, tblCertificate.Rows[0]["cer_serial"].ToString());
                    }
                    else
                    {
                        xml = CommonManager.SignXmlInvoice(await getTenDv(), "#data", "seller", xml, certFile, "NBan");
                    }

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("dlxml_id", Guid.NewGuid());
                    parameters.Add("hdon_id", hdon_id);
                    parameters.Add("dlxml", xml);
                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }
                    string sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68 VALUES(@dlxml_id,@hdon_id, @dlxml)";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlXml, CommandType.Text, parameters);

                    string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, ngky = @ngky, nky=@nky, tdlap = @tdlap, kygui_cqt =@kygui_cqt WHERE hdon_id = @hdon_id";
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("tthai", obj["guiCQT"] == null ? "Đã gửi" : "Đã ký");
                    parameters2.Add("kygui_cqt", obj["guiCQT"] == null ? 1 : 0);
                    parameters2.Add("ngky", username);
                    parameters2.Add("hdon_id", hdon_id);
                    //parameters2.Add("nky", DateTime.Parse(tdky));
                    parameters2.Add("nky", DateTime.Now);
                    parameters2.Add("tdlap", DateTime.Parse(tdlap));

                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                    // lấy giá trị signature

                    //XmlDocument doc = new XmlDocument();
                    //doc.LoadXml(xml);

                    // lấy thông tin xml TTChung của Thông điệp
                    string mst = await getMstDv();
                    string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                    XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, obj["type_cmd"].ToString(), 1);

                    // đẩy dữ liệu hóa đơn vào thẻ DLieu
                    XElement dLieu = new XElement("DLieu");
                    dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                    XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;

                    using (var buffer = new StringWriter())
                    using (var writer = XmlWriter.Create(buffer, writerSettings))
                    {
                        xmlThongDiep.Save(writer);
                        writer.Flush();
                        xml = buffer.ToString();
                    }

                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }


                    if (obj["guiCQT"] == null)
                    {
                        // lưu vào bảng tonghop_gui_tvan_68

                        string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                        Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                        param_Insert.Add("id", Guid.NewGuid());
                        param_Insert.Add("mltdiep_gui", obj["type_cmd"].ToString());
                        param_Insert.Add("xml_tdiep_gui", xml);
                        param_Insert.Add("type_id", hdon_id);
                        param_Insert.Add("mtdiep_gui", mtdiep_gui);
                        //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                        param_Insert.Add("nguoi_gui", username);
                        param_Insert.Add("note", "app");
                        param_Insert.Add("mdvi", branch_code);

                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                        // update mã thông điệp gửi vào bảng hóa đơn
                        Dictionary<string, object> para = new Dictionary<string, object>();
                        para.Add("hdon_id", hdon_id);
                        if (obj["type_cmd"].ToString() == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                        {
                            // update trạng thái
                            sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                            para.Add("tthai", "Chờ cấp mã");
                            para.Add("mtdiep_gui", mtdiep_gui);

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                        }
                        else
                        {
                            // update mã thông điệp gửi
                            sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                            para.Add("mtdiep_gui", mtdiep_gui);

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                        }

                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    if (obj["guiCQT"] == null)
                    {
                        // đóng gói xml gửi lên TVAN

                        string data1 = await ThongDiepGui(cmd, xml);

                        // đẩy queue lên TVAN
                        _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data1);
                    }

                    sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                    DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                    JArray array = JArray.FromObject(tblInvoices);

                    json.RemoveAll();

                    //res.Add("ok", true);
                    json.Add("inv", (JObject)array[0]);
                    json.Add("hdon_id", hdon_id);
                    json.Add("email", tblInvoices.Rows[0]["email"].ToString());
                    json.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                    json.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                    json.Add("tthai", "Đã ký");

                    #endregion

                }
                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }
        public async Task<JObject> KiHoaDonGuiTCT(JObject obj)
        {
            JObject json = new JObject();
            bool isError = false;

            string serial = obj["serial"].ToString();
            JArray xmlLst = (JArray)obj["xml"];
            string cmd = obj["type_cmd"].ToString();
            string mst = obj["mst"].ToString();
            string donvi = obj["donvi"].ToString();

            //copy dữ liệu nhập vào, mặc định alf 1
            int copy = 0;
            int.TryParse(obj["copy"].ToString(), out copy);
            if (copy == 0) copy = 1;

            string xml = "";
            //string username = obj["username"].ToString();
            try
            {
                string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                        + "WHERE a.cert_type='FILE' "
                        + "AND a.cer_serial='" + serial + "'";

                DataTable tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);

                if (tblCertificate.Rows.Count == 0)
                {
                    json.Add("error", "Không tìm thấy chứng thư số");
                    return json;
                }

                string pass = tblCertificate.Rows[0]["pass"].ToString();
                byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];

                X509Certificate2 certFile = new X509Certificate2(bytes, pass);

                /*sql = "SELECT b.* FROM dbo_migration.wb_user a join dbo_migration.wb_branch b on a.branch_code = b.code where username =  '" + username + "'";
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql);
                string mst = dt.Rows[0]["tax_code"].ToString();*/
                string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                XElement dLieu = new XElement("DLieu");
                if (cmd == "200" || cmd == "203")
                {
                    dLieu = new XElement("DLieu");
                    for (int i = 0; i < copy; i++)
                    {
                        foreach (string txtXml in xmlLst)
                        {
                            xml = CommonManager.SignXmlInvoiceGiaLap(donvi, "#data", "seller", txtXml, certFile, "NBan");
                            dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                        }
                    }
                }
                else if (cmd == "100" || cmd == "101")
                {
                    dLieu = new XElement("DLieu");
                    for (int i = 0; i < copy; i++)
                    {
                        foreach (string txtXml in xmlLst)
                        {
                            xml = CommonManager.SignXmlInvoiceGiaLap(donvi, "#data", "seller", xml, certFile, "NNT");
                            dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                        }
                    }
                }
                else if (cmd == "400")
                {

                    dLieu = new XElement("DLieu");
                    for (int i = 0; i < copy; i++)
                    {
                        foreach (string txtXml in xmlLst)
                        {
                            xml = CommonManager.SignXmlInvoiceGiaLap(donvi, "#data", "seller", xml, certFile, "NNT");
                            dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                        }
                    }
                }
                XElement xml_TTChung = XmlTTChungTDiep(mtdiep_gui, cmd, 1, mst);
                // đẩy dữ liệu hóa đơn vào thẻ DLieu

                XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlThongDiep.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                if (obj["guiCQT"] == null)
                {
                    // đóng gói xml gửi lên TVAN

                    string data1 = await ThongDiepGui(cmd, xml);

                    // đẩy queue lên TVAN
                    //_rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data1);
                    json.Add("xml", data1);
                }

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
            }

            return json;
        }

        public async Task<string> GetInvoiceFileName(string id)
        {
            string fileName = "";
            string mst = "";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("hdon_id", Guid.Parse(id));

            DataTable tblInv_InvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id", CommandType.Text, parameters);

            //get mẫu ký hiệu, và số HD
            string khieu = tblInv_InvoiceAuth.Rows[0]["khieu"].ToString();
            string shdon = tblInv_InvoiceAuth.Rows[0]["shdon"].ToString();

            //get MST
            string qryMST = $"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code='{_webHelper.GetDvcs()}'";
            DataTable dmdvcs = await _minvoiceDbContext.GetDataTableAsync(qryMST);
            if (dmdvcs.Rows.Count > 0)
            {
                mst = dmdvcs.Rows[0]["tax_code"].ToString();
            }

            fileName = mst + "_" + khieu + "_" + shdon;

            return fileName;
        }

        public async Task<JObject> SaveSoHoaDonKiHieu(JObject obj)
        {
            JObject json = new JObject();

            string editmode = obj["editmode"].ToString();
            string sql = "";
            if (editmode == "1")
            {
                obj.Add("id", Guid.NewGuid());
                obj.Add("phanhoadon_78_donvi_id", Guid.NewGuid());
                obj.Add("branch_code", _webHelper.GetDvcs());
                sql = "#SCHEMA_NAME#.crd_insert_phanhoadon_78";
            }
            else if (editmode == "2")
            {
                sql = "#SCHEMA_NAME#.crd_update_phanhoadon_78";
            }
            else if (editmode == "3")
            {
                sql = "#SCHEMA_NAME#.crd_delete_phanhoadon_78";
            }
            if (!string.IsNullOrEmpty(sql))
            {
                try
                {
                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, obj);
                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex)
                {
                    json.Add("error", ex.Message);
                    Log.Error(ex.Message);

                    try
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                        _minvoiceDbContext.CloseTransaction();
                    }
                    catch { }
                }
            }
            //_minvoiceDbContext.CloseTransaction();
            return json;
        }
        public async Task<JObject> PhanKiHieuDonVi(JObject obj)
        {
            JObject json = new JObject();
            obj.Add("id", Guid.NewGuid());
            string sql = "#SCHEMA_NAME#.crd_insert_phanhoadon_78_donvi";
            try
            {
                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, obj);
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }
            //_minvoiceDbContext.CloseTransaction();
            return json;
        }
        public async Task<JObject> DeletePhanKiHieuDonVi(JObject obj)
        {
            JObject json = new JObject();
            string sql = "#SCHEMA_NAME#.crd_delete_phanhoadon_78_donvi";
            try
            {
                await _minvoiceDbContext.BeginTransactionAsync();
                await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, obj);
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }
            //_minvoiceDbContext.CloseTransaction();
            return json;
        }


        public async Task GenerateThongDiep()
        {
            string sqlSelect_mau04_chitiet = $"SELECT xml_gui, mltdiep, schema_name FROM reg.v_tong_hop_loi";
            DataTable mau04_chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04_chitiet, CommandType.Text, new Dictionary<string, object>());

            foreach (DataRow row in mau04_chitiet_Table.Rows)
            {

                String xml = row["xml_gui"].ToString();
                String cmd = row["mltdiep"].ToString();

                string vendor = "mbf_sctv";
                string host = "backend";
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                string privateKey = CommonConstants.PRIVATE_KEY;

                string hashMessage = cmd + ":" + vendor + ":" + host + ":" + timestamp + ":" + privateKey;

                var token = FunctionUtil.ComputeSha256Hash(FunctionUtil.ComputeSha256Hash(xml) + ":" + hashMessage);

                string thongDiepXml = "<Body>\n" +
                    "\t<MLTDiep></MLTDiep>\n" +
                    "\t<Vendor></Vendor>\n" +
                    "\t<Site></Site>\n" +
                    "\t<Timestamp></Timestamp>\n" +
                    "\t<Token></Token>\n" +
                    "\t<DLTDiep></DLTDiep>\n" +
                    "</Body>";

                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = false;

                doc.LoadXml(thongDiepXml);

                doc.GetElementsByTagName("MLTDiep")[0].InnerText = cmd;
                doc.GetElementsByTagName("Vendor")[0].InnerText = vendor;
                doc.GetElementsByTagName("Site")[0].InnerText = host;
                doc.GetElementsByTagName("Timestamp")[0].InnerText = timestamp;
                doc.GetElementsByTagName("Token")[0].InnerText = token;
                doc.GetElementsByTagName("DLTDiep")[0].InnerText = xml;

                string xmlThongDiep = doc.OuterXml;

                _rabbitMQService.PublishTaskQueue(CommonConstants.QUEUE_VENDOR_SEND_REQUEST_TO_VAN, xmlThongDiep);

            }

        }

        public async Task<JArray> GetDecimalPlace(string ma_nt)
        {
            //Int32 decimal_place = 2;
            JArray jarr = new JArray();
            try
            {
                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("code", ma_nt);

                DataTable dmnt = await this._minvoiceDbContext.GetDataTableAsync("SELECT hoadon123 FROM #SCHEMA_NAME#.sl_currency WHERE code = @code", CommandType.Text, dicParam);

                jarr = JArray.FromObject(dmnt);
                //decimal_place = Convert.ToInt32(dmnt.Rows[0]["decimal_place"]);
            }
            catch (Exception ex)
            {

            }
            return jarr;
        }

        public async Task<JObject> bangkebanra(JObject model)
        {
            JObject json = new JObject();
            try
            {
                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string ky_hieu = model["ky_hieu"].ToString();
                string lst_branch_code = model["lst_branch_code"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));
                parameters.Add("ky_hieu", ky_hieu);
                parameters.Add("lst_branch_code", lst_branch_code);

                //var invoiceDb = this._nopDbContext.GetInvoiceDb();
                string sql = "SELECT * FROM #SCHEMA_NAME#.qr_bc_bkbr_tr('#SCHEMA_NAME#',@ma_dvcs,null,@tu_ngay,@den_ngay, @ky_hieu,@lst_branch_code)";
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray jarr = JArray.FromObject(dt);
                json.Add("data", jarr);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;
        }

        public async Task<JArray> GetDataInvoiceKhac(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68_khac WHERE hdon_id = '" + id + "'";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;

        }
        public async Task<JObject> GetInvoiceById(Guid id)
        {
            JObject obj = new JObject();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("hdon_id", id);

            DataTable tblHoadon = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id", CommandType.Text, parameters);

            if (tblHoadon.Rows.Count == 0)
            {
                obj.Add("error", "Không tìm thấy hóa đơn");
                return obj;
            }

            DataTable tblHoaDonChiTiet = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id=@hdon_id", CommandType.Text, parameters);

            JArray jarray = JArray.FromObject(tblHoadon);

            obj = (JObject)jarray[0];

            jarray = JArray.FromObject(tblHoaDonChiTiet);
            obj.Add("HoaDon68_ChiTiet", jarray);

            FunctionUtil.AddInfoToResult(obj);
            return obj;

        }

        public async Task<JObject> GetXmlData(Guid id)
        {
            JObject json = new JObject();

            try
            {
                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("hdon_id", id);

                DataTable tblResult = await _minvoiceDbContext.GetDataTableAsync("SELECT dlxml FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id=@hdon_id", CommandType.Text, dicParam);

                if (tblResult.Rows.Count > 0)
                {
                    string xml = tblResult.Rows[0][0].ToString();

                    json.Add("id", id);
                    json.Add("xml", xml);
                }
                else
                {
                    json.Add("error", "Hóa đơn chưa có file Xml");
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }

            return json;
        }


        public async Task<JObject> SignConfirm(JObject parameters)
        {
            JObject obj = new JObject();
            string error = "";
            bool isOpen = false;
            try
            {

                string xml = parameters["InvoiceXmlData"].ToString();
                var id = parameters["hdon_id"].ToString();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("hdon_id", Guid.Parse(id));

                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id", CommandType.Text, dicParam);

                if (tblInvoice.Rows.Count == 0)
                {
                    obj.Add("error", "Không tìm thấy hóa đơn");
                    return obj;
                }

                int idx = xml.IndexOf("Id=\"buyer\"");

                if (idx < 0)
                {
                    obj.Add("error", "Người mua chưa ký");
                    return obj;
                }

                await _minvoiceDbContext.BeginTransactionAsync();
                isOpen = true;

                string sql = "UPDATE #SCHEMA_NAME#.dulieuxml68 SET dlxml_ngmua=CAST(@data as xml) WHERE hdon_id=@hdon_id";

                dicParam.Clear();
                dicParam.Add("hdon_id", Guid.Parse(id));
                dicParam.Add("data", xml);

                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, dicParam);
                await _minvoiceDbContext.TransactionCommitAsync();
            }
            catch (Exception ex)
            {
                error = ex.Message;
                obj.Add("error", ex.Message);
                Log.Error(ex.Message);
            }

            if (isOpen)
            {
                if (error.Length > 0)
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                }

                _minvoiceDbContext.CloseTransaction();
            }

            return obj;
        }

        public async Task<JArray> GetDataInvoiceDetail_BangKe(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo_chitiet WHERE hdon_id = '" + id + "' ORDER BY Cast (stt as numeric)";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));

            return result;
        }

        public async Task<JArray> GetInvoiceFromdateTodate(JObject obj)
        {
            DateTime tu_ngay = DateTime.Parse(obj["tu_ngay"].ToString());
            DateTime den_ngay = DateTime.Parse(obj["den_ngay"].ToString());

            JArray result = new JArray();
            string sql = @"SELECT a.*, (SELECT array_to_json(array_agg(to_json(fields))) FROM ( SELECT b.* FROM #SCHEMA_NAME#.hoadon68_chitiet b where b.hdon_id= a.hdon_id ) fields) as details,
                            (SELECT array_to_json(array_agg(to_json(fields))) FROM ( SELECT c.* FROM #SCHEMA_NAME#.hoadon68_khac c where c.hdon_id= a.hdon_id ) fields) as hoadon68_khac,
                            (SELECT array_to_json(array_agg(to_json(fields))) FROM ( SELECT d.* FROM #SCHEMA_NAME#.hoadon68_phi d where d.hdon_id= a.hdon_id ) fields) as hoadon68_phi
                            FROM #SCHEMA_NAME#.hoadon68 a " +
                           "WHERE a.nlap >= DATE_TRUNC('day', @tu_ngay) and a.nlap <= DATE_TRUNC('day', @den_ngay) " +
                           "ORDER BY nlap, shdon DESC ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("tu_ngay", tu_ngay);
            parameters.Add("den_ngay", den_ngay);

            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters));
            return result;
        }

        public async Task<JArray> GetListInvoiceIdFromdateTodate(JObject obj)
        {
            DateTime tu_ngay = DateTime.Parse(obj["tu_ngay"].ToString());
            DateTime den_ngay = DateTime.Parse(obj["den_ngay"].ToString());

            JArray result = new JArray();
            string sql = "SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 \n" +
                           "WHERE nlap >= DATE_TRUNC('day', @tu_ngay) and nlap <= DATE_TRUNC('day', @den_ngay) \n" +
                           "ORDER BY nlap, shdon DESC ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("tu_ngay", tu_ngay);
            parameters.Add("den_ngay", den_ngay);

            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters));
            return result;
        }

        public JArray DashboardTiles78(string ma_dvcs)
        {

            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam.Add("ma_dvcs", ma_dvcs);
            dicParam.Add("user", _webHelper.GetUser());

            // string sql = "SELECT * FROM #SCHEMA_NAME#.qr_report_dashboard_tiles('#SCHEMA_NAME#',@ma_dvcs , @user)";
            string sql = "SELECT * FROM #SCHEMA_NAME#.qr_report_dashboard_tiles_78('#SCHEMA_NAME#',@ma_dvcs , @user)";
            DataTable tblData = _minvoiceDbContext.GetDataTable(sql, CommandType.Text, dicParam);
            JArray array = JArray.FromObject(tblData);

            return array;

        }

        public async Task<JObject> GetEmailTemplate78(string type, string id, string ma_dvcs, string originalString)
        {

            JObject result = new JObject();
            try
            {
                DataTable tblInvoice = await _minvoiceDbContext.GetDataTableAsync("select mdvi, mnmua, tnmua,ten, hdon_id,tgtttbso FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@P_0", Guid.Parse(id));
                string mnmua = tblInvoice.Rows[0]["mnmua"].ToString();
                string mdvi = tblInvoice.Rows[0]["mdvi"].ToString();

                DataTable tblTemp = await _minvoiceDbContext.GetDataTableAsync("SELECT *,body as noi_dung FROM #SCHEMA_NAME#.pl_tempemail WHERE yesno='C' AND tempemail_type=@P_0 AND branch_code=@P_1 LIMIT 1 OFFSET 0", type, ma_dvcs);

                string pass1 = tblTemp.Rows[0]["pass"].ToString();

                JArray array = JArray.FromObject(tblTemp);

                result = (JObject)array[0];
                result["pass"] = pass1;

                //string mdvi = tblInvoice.Rows[0]["branch_code"]?.ToString();
                DataTable tblBranch = await _minvoiceDbContext.GetDataTableAsync($"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code = '{mdvi}'");

                DataTable tblEmailBody = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.qr_get_email_invoice_68('#SCHEMA_NAME#',@P_0)", Guid.Parse(id));

                string noi_dung = result["noi_dung"].ToString();
                string tieude = result["subject"].ToString();

                if (type == "Ký xác nhận")
                {
                    var hostEmail = _webHelper.GetUrlHost();
                    string url = originalString + "/Content/App/SignInvoice/SignInvoiceApplication.application?type=07&id=" + id;
                    noi_dung = noi_dung.Replace("{URL_CONFIRM}", url + "type=78&mst=" + hostEmail);
                    noi_dung = noi_dung.Replace("{ID_INV}", id);
                }

                noi_dung = noi_dung.Replace("#tgtttbso#", DoubleStrToString(tblInvoice.Rows[0]["tgtttbso_last"].ToString()));

                DataRow row = tblEmailBody.Rows[0];

                foreach (DataColumn column in tblEmailBody.Columns)
                {
                    if (column.ColumnName == "url_confirm")
                    {
                        var hostEmail = _webHelper.GetUrlHost();
                        string value = row[column.ColumnName].ToString();
                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value + "&type=78&mst=" + hostEmail);
                    }
                    else
                    {
                        string value = row[column.ColumnName].ToString();
                        noi_dung = noi_dung.Replace("#" + column.ColumnName + "#", value);
                    }
                }

                string tendvi = tblInvoice.Rows[0]["ten"]?.ToString();

                tieude = tieude.Replace("#invoice_number#", tblInvoice.Rows[0]["hdon_id"].ToString());
                tieude = tieude.Replace("#seller_legal_name#", tblBranch.Rows[0]["name"].ToString());
                tieude = tieude.Replace("#buyer_name#", tendvi ?? tblInvoice.Rows[0]["tnmua"].ToString());

                DataTable tblHisEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT COUNT(*) FROM #SCHEMA_NAME#.wb_log_email WHERE inv_invoiceauth_id=@P_0", Guid.Parse(id));
                int his_mail = Convert.ToInt32(tblHisEmail.Rows[0][0]);

                result["subject"] = tieude;
                result["noi_dung"] = noi_dung;
                result["body"] = noi_dung;

                result.Add("his_email", his_mail);

                DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT a.email,b.email as email1 FROM #SCHEMA_NAME#.pl_customer a LEFT JOIN #SCHEMA_NAME#.pl_customer_email b ON a.pl_customer_id=b.pl_customer_id WHERE a.code=@P_0 AND a.branch_code=@P_1", mnmua, mdvi);

                array = new JArray();

                if (tblEmail.Rows.Count > 0)
                {
                    string email = tblEmail.Rows[0]["email"].ToString();

                    if (email.Length > 0)
                    {
                        JObject obj = new JObject();
                        obj.Add("id", email);
                        obj.Add("value", email);

                        array.Add(obj);
                    }

                    for (int i = 0; i < tblEmail.Rows.Count; i++)
                    {
                        DataRow r = tblEmail.Rows[i];
                        string email1 = r["email1"].ToString();

                        if (email1.Length > 0 && email1 != email)
                        {
                            JObject obj = new JObject();
                            obj.Add("id", email1);
                            obj.Add("value", email1);

                            array.Add(obj);
                        }
                    }
                }

                result.Add("lst_email", array);

                return result;
            }
            catch (Exception ex)
            {
                result.Add("error", ex.Message);
                Log.Error(ex.Message);
                return result;
            }

        }
        public async Task<JObject> Xoanhieuhoadon(JArray data, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {

                string sql = "#SCHEMA_NAME#.crd_hdon68_delete";
                await _minvoiceDbContext.BeginTransactionAsync();
                for (int i = data.Count - 1; i >= 0; i--)
                {

                    JObject masterObj = (JObject)data[i];
                    #region Check

                    Dictionary<string, object> obj_check = new Dictionary<string, object>();

                    var sql_check = @"select * from #SCHEMA_NAME#.permission_kyhieu68 a 
                                join #SCHEMA_NAME#.quanlykyhieu68 b on a.qlkhsdung_id = b.qlkhsdung_id 
                                join #SCHEMA_NAME#.wb_user c on a.wb_user_id =c.wb_user_id 
                                where c.username =   @username and b.qlkhsdung_id = @qlkhsdung_id";
                    obj_check.Add("username", username);
                    obj_check.Add("qlkhsdung_id", Guid.Parse(masterObj["cctbao_id"].ToString()));
                    DataTable data_check = await _minvoiceDbContext.GetDataTableAsync(sql_check, CommandType.Text, obj_check);

                    if (data_check.Rows.Count == 0)
                    {
                        throw new Exception("Người dùng không có quyền sử dụng kí hiệu này");
                    }

                    sql_check = $@"select * from #SCHEMA_NAME#.phanhoadon_78_donvi a 
                                            join #SCHEMA_NAME#.phanhoadon_78 b on a.phanhoadon_78_id = b.id 
                                            join #SCHEMA_NAME#.quanlykyhieu68 c on b.kyhieu_id =c.qlkhsdung_id 
                                            where a.branch_code =@branch_code and c.qlkhsdung_id = @qlkhsdung_id";
                    obj_check = new Dictionary<string, object>();
                    obj_check.Add("branch_code", _webHelper.GetDvcs());
                    obj_check.Add("qlkhsdung_id", Guid.Parse(masterObj["cctbao_id"].ToString()));
                    data_check = await _minvoiceDbContext.GetDataTableAsync(sql_check, CommandType.Text, obj_check);

                    if (data_check.Rows.Count == 0)
                    {
                        throw new Exception("Đơn vị không được phân quyền cho kí hiệu này");
                    }
                    #endregion
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                }
                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();
                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JObject> SignListInvoiceCertFile_HSM(string folder, JObject model)
        {
            JObject res = new JObject();
            string error = "";
            bool isOpenConnection = false;

            try
            {
                var branch_code = model["branch_code"].ToString();

                string username = _webHelper.GetUser();

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                dicParam.Add("username", username);

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=@username", CommandType.Text, dicParam);

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    _minvoiceDbContext.CloseTransaction();
                    return res;
                }

                if (tblUser.Rows[0]["is_sign_invoice_hsm"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn bằng HSM");
                    _minvoiceDbContext.CloseTransaction();
                    return res;
                }

                JArray lsthdon_id = (JArray)model["lsthdon_id"];

                string userId = tblUser.Rows[0]["wb_user_id"].ToString();
                dicParam.Add("wb_user_id", Guid.Parse(userId));

                DataTable tblUser_Hsm = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id", CommandType.Text, dicParam);

                if (tblUser_Hsm.Rows.Count == 0)
                {
                    res.Add("error", "Chưa có tài khoản cấu hình");
                    return res;
                }
                // lấy account hsm portal
                string username_hsm = tblUser_Hsm.Rows[0]["username"].ToString();
                string password_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password"].ToString()));
                string password2_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password_level2"].ToString()));
                string ma_dvcs_hsm = tblUser_Hsm.Rows[0]["ma_dvcs"].ToString();

                JArray arrayMail = new JArray();

                // call api login
                HttpClient client = new HttpClient();

                var api = CommonConstants.LOGIN;
                JObject user = new JObject();
                user.Add("username", username_hsm);
                user.Add("password", password_hsm);
                user.Add("ma_dvcs", ma_dvcs_hsm);

                var token = "";
                var resLogin = executeLogin(CommonConstants.BASE_HSM_URL, api, user).Result;

                // kiểm tra nếu login lỗi thì thông báo
                if (resLogin["error"] != null)
                {
                    //res.Add("error", resLogin["error"].ToString());
                    //return res;
                    throw new Exception(resLogin["error"].ToString());

                }
                else
                {
                    token = resLogin["token"].ToString();
                    token = token + ";" + ma_dvcs_hsm;
                }

                for (int i = 0; i < lsthdon_id.Count; i++)
                {
                    string cmd_type = "";

                    var hdonid = "";

                    if (model["AllHoaDon"] != null)
                    {
                        JObject element = (JObject)lsthdon_id[i];
                        hdonid = element["hdon_id"].ToString();

                        cmd_type = element["is_hdcma"].ToString() == "1" ? "200" : (element["is_hdcma"].ToString() == "0" ? "203" : "");

                        if (cmd_type != "200" && cmd_type != "203")
                        {
                            continue;
                        }
                    }
                    else
                    {
                        hdonid = lsthdon_id[i].ToString();
                        cmd_type = model["type_cmd"].ToString();
                    }

                    Guid hdon_id = Guid.Parse(hdonid);
                    #region hatm CheckQuyTrinhHoaDon
                    Dictionary<string, object> parametersN = new Dictionary<string, object>();
                    parametersN.Add("hdon_id", hdon_id);

                    DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon,cctbao_id,mdvi FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parametersN);
                    string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                    parametersN.Clear();
                    parametersN.Add("hdon_id", hdon_id);
                    parametersN.Add("nlap", DateTime.Parse((invoice_issued_date)));
                    parametersN.Add("cctbao_id", Guid.Parse(tblInvoiceAuth.Rows[0]["cctbao_id"].ToString()));
                    parametersN.Add("mdvi", tblInvoiceAuth.Rows[0]["mdvi"].ToString());

                    string sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='Chờ ký' And cctbao_id =@cctbao_id And mdvi=@mdvi AND nlap< @nlap LIMIT 1";
                    DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parametersN);

                    if (tblInvoiceAuthBefore.Rows.Count > 0)
                    {
                        throw new Exception("Bạn chưa xử lý hóa đơn của ngày hôm trước");
                    }

                    #endregion
                    await _minvoiceDbContext.BeginTransactionAsync();

                    #region lấy số hóa đơn khi không có
                    if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                    {
                        sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                        JObject masterObj = new JObject();
                        masterObj.Add("branch_code", _webHelper.GetDvcs());
                        masterObj.Add("hdon_id", hdon_id);

                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sql, CommandType.StoredProcedure, masterObj);
                    }
                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                    #endregion

                    await _minvoiceDbContext.BeginTransactionAsync();

                    #region Xử lý hoá đơn theo hdon_id
                    DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT tthai FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id ='" + hdon_id + "'");
                    if (dtHdon.Rows.Count > 0)
                    {
                        if (dtHdon.Rows[0]["tthai"].ToString() != "Chờ ký")
                        {
                            //res.Add("error", "Tồn tại hóa đơn không ở trạng thái chờ ký");
                            //return res;
                            throw new Exception("Tồn tại hóa đơn không ở trạng thái chờ ký");
                        }
                    }

                    // get tencty
                    var tencty = await getTenDv();

                    #region check xml bảng kê xem có không, có thì xử lý
                    //xml bảng kê
                    string qrybangke = $"SELECT * FROM #SCHEMA_NAME#.bangke_kemtheo WHERE hdon_id='{hdon_id.ToString()}'";
                    DataTable Rowbangke = await this._minvoiceDbContext.GetDataTableAsync(qrybangke);

                    JObject ObjXml_BangKe = null;
                    var xml_bangke = "";
                    if (Rowbangke.Rows.Count > 0)
                    {
                        ObjXml_BangKe = await XmlInvoice_BangKe(hdon_id.ToString());
                    }

                    if (ObjXml_BangKe != null)
                    {
                        //xml_bangke = ObjXml_BangKe["xml"].ToString().Replace("'", "''");
                        xml_bangke = ObjXml_BangKe["xml"].ToString();

                        XmlDocument document_Bke = new XmlDocument();
                        document_Bke.PreserveWhitespace = false;
                        document_Bke.LoadXml(xml_bangke);

                        XmlNode nodeTT_BangKe = document_Bke.SelectSingleNode("/BKe/DLBKe/TTKhac");
                        if (nodeTT_BangKe != null)
                        {
                            XmlElement TTin1 = document_Bke.CreateElement("TTin");
                            XmlElement TTruong1 = document_Bke.CreateElement("TTruong");
                            TTruong1.InnerText = "DVKy";
                            XmlElement KDLieu1 = document_Bke.CreateElement("KDLieu");
                            KDLieu1.InnerText = "string";
                            XmlElement DLieu1 = document_Bke.CreateElement("DLieu");
                            DLieu1.InnerText = tencty;
                            TTin1.AppendChild(TTruong1);
                            TTin1.AppendChild(KDLieu1);
                            TTin1.AppendChild(DLieu1);


                            XmlElement TTin2 = document_Bke.CreateElement("TTin");
                            XmlElement TTruong2 = document_Bke.CreateElement("TTruong");
                            TTruong2.InnerText = "NKy";
                            XmlElement KDLieu2 = document_Bke.CreateElement("KDLieu");
                            KDLieu2.InnerText = "date";
                            XmlElement DLieu2 = document_Bke.CreateElement("DLieu");
                            DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                            TTin2.AppendChild(TTruong2);
                            TTin2.AppendChild(KDLieu2);
                            TTin2.AppendChild(DLieu2);

                            nodeTT_BangKe.AppendChild(TTin1);
                            nodeTT_BangKe.AppendChild(TTin2);
                        }

                        xml_bangke = document_Bke.OuterXml;

                        // Call Sign HSM API

                        api = CommonConstants.SIGN_HSM_URL_78;
                        JObject json_bke = new JObject();
                        json_bke.Add("password2", password2_hsm);
                        json_bke.Add("idData", "#data");
                        json_bke.Add("idSigner", "seller");
                        json_bke.Add("tagSign", "NBan");
                        json_bke.Add("dataType", "xml");
                        json_bke.Add("xml", xml_bangke);

                        var resSign_bke = executePost(CommonConstants.BASE_HSM_URL, api, token, json_bke).Result;

                        if ("false".Equals(resSign_bke["status"].ToString().ToLower()))
                        {
                            //res.Add("error", resSign_bke["message"].ToString());
                            //return res;
                            throw new Exception(resSign_bke["message"].ToString());
                        }
                        else
                        {
                            xml_bangke = resSign_bke["data"].ToString();
                        }
                        //xml_bangke = xml_bangke.Replace("'", "''");
                    }
                    #endregion

                    JObject ObjXml = await XmlInvoice(hdon_id.ToString()); // tạo xml hoá đơn

                    //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                    string xml = ObjXml["xml"].ToString();
                    string tdlap = ObjXml["tdlap"].ToString().Replace("'", "''");
                    string tdky = ObjXml["tdky"].ToString().Replace("'", "''");

                    XmlDocument document = new XmlDocument();
                    document.PreserveWhitespace = false;
                    document.LoadXml(xml);

                    XmlNode nodeTT = document.SelectSingleNode("/HDon/DLHDon/TTKhac");
                    if (nodeTT != null)
                    {
                        XmlElement TTin1 = document.CreateElement("TTin");
                        XmlElement TTruong1 = document.CreateElement("TTruong");
                        TTruong1.InnerText = "DVKy";
                        XmlElement KDLieu1 = document.CreateElement("KDLieu");
                        KDLieu1.InnerText = "string";
                        XmlElement DLieu1 = document.CreateElement("DLieu");
                        DLieu1.InnerText = tencty;
                        TTin1.AppendChild(TTruong1);
                        TTin1.AppendChild(KDLieu1);
                        TTin1.AppendChild(DLieu1);


                        XmlElement TTin2 = document.CreateElement("TTin");
                        XmlElement TTruong2 = document.CreateElement("TTruong");
                        TTruong2.InnerText = "NKy";
                        XmlElement KDLieu2 = document.CreateElement("KDLieu");
                        KDLieu2.InnerText = "date";
                        XmlElement DLieu2 = document.CreateElement("DLieu");
                        DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                        TTin2.AppendChild(TTruong2);
                        TTin2.AppendChild(KDLieu2);
                        TTin2.AppendChild(DLieu2);

                        nodeTT.AppendChild(TTin1);
                        nodeTT.AppendChild(TTin2);
                    }

                    xml = document.OuterXml;

                    // Call Sign HSM API
                    JObject result = new JObject();

                    api = CommonConstants.SIGN_HSM_URL_78;
                    JObject json = new JObject();
                    json.Add("password2", password2_hsm);
                    json.Add("idData", "#data");
                    json.Add("idSigner", "seller");
                    json.Add("tagSign", "NBan");
                    json.Add("dataType", "xml");
                    json.Add("xml", xml);

                    var resSign = executePost(CommonConstants.BASE_HSM_URL, api, token, json).Result;

                    if ("false".Equals(resSign["status"].ToString().ToLower()))
                    {
                        //res.Add("error", resSign["message"].ToString());
                        //return res;
                        throw new Exception(resSign["message"].ToString());
                    }
                    else
                    {
                        xml = resSign["data"].ToString();
                    }
                    //xml = xml.Replace("'", "''");

                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("dlxml_id", Guid.NewGuid());
                    parameters.Add("hdon_id", hdon_id);
                    parameters.Add("dlxml", xml);
                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }

                    string sqlXml = "";
                    if (!string.IsNullOrEmpty(xml_bangke))
                    {
                        if (CommonManager.CheckThoiGian(xml_bangke) == false)
                        {
                            //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                            //return res;
                            throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        }

                        parameters.Add("dlxml_bangke", xml_bangke);
                        sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68(dlxml_id,hdon_id,dlxml,dlxml_bangke) VALUES(@dlxml_id,@hdon_id, @dlxml, @dlxml_bangke)";
                    }
                    else
                    {
                        sqlXml = "INSERT INTO #SCHEMA_NAME#.dulieuxml68 VALUES(@dlxml_id,@hdon_id, @dlxml)";
                    }

                    await _minvoiceDbContext.TransactionCommandAsync(sqlXml, CommandType.Text, parameters);

                    string sqlUpdate = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, ngky = @ngky, nky=@nky, tdlap = @tdlap, kygui_cqt =@kygui_cqt WHERE hdon_id = @hdon_id";
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("tthai", model["guiCQT"] == null ? "Đã gửi" : "Đã ký");
                    parameters2.Add("kygui_cqt", model["guiCQT"] == null ? 1 : 0);
                    parameters2.Add("ngky", username);
                    parameters2.Add("hdon_id", hdon_id);
                    parameters2.Add("nky", DateTime.Now);
                    parameters2.Add("tdlap", DateTime.Parse(tdlap));

                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                    // lấy thông tin xml TTChung của Thông điệp
                    string mst = await getMstDv();
                    string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                    XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, cmd_type, 1);

                    // đẩy dữ liệu hóa đơn vào thẻ DLieu
                    XElement dLieu = new XElement("DLieu");
                    dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                    XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;

                    using (var buffer = new StringWriter())
                    using (var writer = XmlWriter.Create(buffer, writerSettings))
                    {
                        xmlThongDiep.Save(writer);
                        writer.Flush();
                        xml = buffer.ToString();
                    }
                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }
                    //xml = xmlThongDiep.ToString();

                    // ký thông điệp gửi lên thuế

                    //xml = CommonManager.SignThongDiep(xml, certFile, pass);

                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }

                    if (model["guiCQT"] == null)
                    {
                        // lưu vào bảng tonghop_gui_tvan_68

                        if (model["is_api"] == null)
                        {
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                 + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mltdiep_gui", cmd_type);
                            param_Insert.Add("xml_tdiep_gui", xml);
                            param_Insert.Add("type_id", hdon_id);
                            param_Insert.Add("mtdiep_gui", mtdiep_gui);
                            //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                            param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                            param_Insert.Add("note", "app");
                            param_Insert.Add("mdvi", _webHelper.GetDvcs());
                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                        }
                        else
                        {
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi, is_api) VALUES "
                                + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi, @is_api)";
                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mltdiep_gui", cmd_type);
                            param_Insert.Add("xml_tdiep_gui", xml);
                            param_Insert.Add("type_id", hdon_id);
                            param_Insert.Add("mtdiep_gui", mtdiep_gui);
                            //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                            param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                            param_Insert.Add("note", "app");
                            param_Insert.Add("mdvi", _webHelper.GetDvcs());
                            param_Insert.Add("is_api", "1"); // phân biệt đơn vị gọi từ API

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                        }

                        // update mã thông điệp gửi vào bảng hóa đơn
                        Dictionary<string, object> para = new Dictionary<string, object>();
                        para.Add("hdon_id", hdon_id);
                        if (cmd_type == CommonConstants.MLTDIEP_YEU_CAU_CAP_MA)
                        {
                            // update trạng thái
                            sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai = @tthai, mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                            para.Add("tthai", "Chờ cấp mã");
                            para.Add("mtdiep_gui", mtdiep_gui);

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                        }
                        else
                        {
                            // update mã thông điệp gửi
                            sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET mtdiep_gui=@mtdiep_gui WHERE hdon_id = @hdon_id";
                            para.Add("mtdiep_gui", mtdiep_gui);

                            await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, para);
                        }
                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    if (model["guiCQT"] == null)
                    {
                        // đóng gói xml gửi lên TVAN
                        string cmd = cmd_type;

                        string data = await ThongDiepGui(cmd, xml);

                        // đẩy queue lên TVAN
                        _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                        res.Add("trang_thai", "Đã gửi hóa đơn tới Cơ quan thuế");

                        //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                        //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                        //_kafkaService.PublishTopic(_topicVendorSendToVan, data);
                    }

                    sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                    DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                    //JArray array = JArray.FromObject(tblInvoices);

                    res.RemoveAll();
                    res.Add("tthai", "Đã ký");
                    //res.RemoveAll();
                    #endregion

                    if (cmd_type == "203")
                    {
                        #region xử lý gửi mail theo hdon_id
                        string email = tblInvoices.Rows[0]["email"].ToString();
                        string tieude = "Thông báo xuất hóa đơn điện tử";

                        dynamic ObjectMail = new JObject();
                        ObjectMail.id = hdon_id;
                        ObjectMail.nguoinhan = email;
                        ObjectMail.tieude = tieude;

                        arrayMail.Add(ObjectMail);
                        #endregion
                    }
                }

                if (arrayMail.Count > 0)
                {
                    Dictionary<string, object> dicPara = new Dictionary<string, object>();
                    dicPara.Add("branch_code", branch_code);
                    var sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                    DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);
                    if (tblSetting.Rows.Count > 0)
                    {
                        string value = tblSetting.Rows[0]["value"].ToString();
                        if (value == "C")
                        {
                            ThreadPool.QueueUserWorkItem(state => ExecuteSendMailHdon(folder, arrayMail));
                        }
                    }
                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                res.Add("error", error);
                Log.Error(error);
            }

            try
            {
                if (error.Length > 0)
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                }

                _minvoiceDbContext.CloseTransaction();
            }
            catch (Exception ex) { }


            return res;
        }

        public async Task<JArray> GetListInvoiceIdFromdateTodate_Pretreatment(JObject obj)
        {
            DateTime tu_ngay = DateTime.Parse(obj["tu_ngay"].ToString());
            DateTime den_ngay = DateTime.Parse(obj["den_ngay"].ToString());

            JArray result = new JArray();
            JObject data_convert = new JObject();
            JArray arr_data_final = new JArray();

            string sql = "SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 \n" +
                           "WHERE nlap >= DATE_TRUNC('day', @tu_ngay) and nlap <= DATE_TRUNC('day', @den_ngay) \n" +
                           "ORDER BY nlap, shdon DESC ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("tu_ngay", tu_ngay);
            parameters.Add("den_ngay", den_ngay);

            JArray data = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters));
            for (int i = 0; i < data.Count; i++)
            {
                data_convert = (JObject)data[i];
                foreach (JProperty jp in data_convert.Properties().ToList())
                {
                    if (jp.Name == "hdon_id")
                    {
                        jp.Replace(new JProperty("inv_invoiceauth_id", jp.Value));
                    }
                }
                arr_data_final.Add(data_convert);
            }

            result = arr_data_final;
            return result;
        }

        public async Task<JArray> GetInvoiceFromdateTodate_Pretreatment(JObject obj)
        {
            DateTime tu_ngay = DateTime.Parse(obj["tu_ngay"].ToString());
            DateTime den_ngay = DateTime.Parse(obj["den_ngay"].ToString());

            JArray result = new JArray();
            JObject data_convert = new JObject();
            JArray arr_data_final = new JArray();

            string sql = @"SELECT a.*, (SELECT array_to_json(array_agg(to_json(fields))) FROM ( SELECT cthdon_id as inv_invoiceauthdetail_id, hdon_id as inv_invoiceauth_id, stt as row_ord, ma as item_code,
                            ten as item_name, mdvtinh as unit_code, dvtinh as unit_name, sluong as quantity, dgia as unit_price, 
                            thtien as total_amount_without_vat, tthue as vat_amount, tgtien as total_amount, tlckhau as discount_percentage, 
                            stckhau as discount_amount, tsuat as tax_type FROM #SCHEMA_NAME#.hoadon68_chitiet b where b.hdon_id= a.hdon_id ) fields) as details, 
                            (SELECT array_to_json(array_agg(to_json(fields))) FROM ( SELECT c.* FROM #SCHEMA_NAME#.hoadon68_khac c where c.hdon_id= a.hdon_id ) fields) as hoadon68_khac,
                            (SELECT array_to_json(array_agg(to_json(fields))) FROM ( SELECT d.* FROM #SCHEMA_NAME#.hoadon68_phi d where d.hdon_id= a.hdon_id ) fields) as hoadon68_phi
                            FROM #SCHEMA_NAME#.hoadon68 a
                           WHERE a.nlap >= DATE_TRUNC('day', @tu_ngay) and a.nlap <= DATE_TRUNC('day', @den_ngay)
                           ORDER BY nlap, shdon DESC ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("tu_ngay", tu_ngay);
            parameters.Add("den_ngay", den_ngay);

            JArray data = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters));

            for (int i = 0; i < data.Count; i++)
            {
                data_convert = (JObject)data[i];
                foreach (JProperty jp in data_convert.Properties().ToList())
                {
                    if (jp.Name == "cctbao_id")
                    {
                        jp.Replace(new JProperty("inv_invoicecode_id", jp.Value));
                    }
                    if (jp.Name == "hdon_id")
                    {
                        jp.Replace(new JProperty("inv_invoiceauth_id", jp.Value));
                    }
                    if (jp.Name == "nlap")
                    {
                        jp.Replace(new JProperty("invoice_issued_date", jp.Value));
                    }
                    if (jp.Name == "shdon")
                    {
                        jp.Replace(new JProperty("invoice_number", jp.Value));
                    }
                    if (jp.Name == "tthai")
                    {
                        jp.Replace(new JProperty("status", jp.Value));
                    }
                    if (jp.Name == "hdon_id_old")
                    {
                        jp.Replace(new JProperty("original_invoice_id", jp.Value));
                    }
                    if (jp.Name == "sdhang")
                    {
                        jp.Replace(new JProperty("order_number", jp.Value));
                    }
                    if (jp.Name == "dvtte")
                    {
                        jp.Replace(new JProperty("currency_code", jp.Value));
                    }
                    if (jp.Name == "tgia")
                    {
                        jp.Replace(new JProperty("exchange_rate", jp.Value));
                    }
                    if (jp.Name == "htttoan")
                    {
                        jp.Replace(new JProperty("payment_method_name", jp.Value));
                    }
                    if (jp.Name == "stknban")
                    {
                        jp.Replace(new JProperty("seller_bank_account", jp.Value));
                    }
                    if (jp.Name == "tnhban")
                    {
                        jp.Replace(new JProperty("seller_bank_name", jp.Value));
                    }
                    if (jp.Name == "customer_code")
                    {
                        jp.Replace(new JProperty("mnmua", jp.Value));
                    }
                    if (jp.Name == "mst")
                    {
                        jp.Replace(new JProperty("buyer_taxcode", jp.Value));
                    }
                    if (jp.Name == "tnmua")
                    {
                        jp.Replace(new JProperty("buyer_display_name", jp.Value));
                    }
                    if (jp.Name == "email")
                    {
                        jp.Replace(new JProperty("buyer_email", jp.Value));
                    }
                    if (jp.Name == "ten")
                    {
                        jp.Replace(new JProperty("buyer_legal_name", jp.Value));
                    }
                    if (jp.Name == "dchi")
                    {
                        jp.Replace(new JProperty("buyer_address_line", jp.Value));
                    }
                    if (jp.Name == "stknmua")
                    {
                        jp.Replace(new JProperty("buyer_bank_account", jp.Value));
                    }
                    if (jp.Name == "sdtnmua")
                    {
                        jp.Replace(new JProperty("buyer_tel", jp.Value));
                    }
                    if (jp.Name == "tnhmua")
                    {
                        jp.Replace(new JProperty("buyer_bank_name", jp.Value));
                    }
                    if (jp.Name == "tgtcthue")
                    {
                        jp.Replace(new JProperty("total_amount_without_vat", jp.Value));
                    }
                    if (jp.Name == "tgtthue")
                    {
                        jp.Replace(new JProperty("vat_amount", jp.Value));
                    }
                    if (jp.Name == "tgtttbso")
                    {
                        jp.Replace(new JProperty("total_amount", jp.Value));
                    }
                    if (jp.Name == "tgtttbso_last")
                    {
                        jp.Replace(new JProperty("total_amount_last", jp.Value));
                    }
                    if (jp.Name == "mdvi")
                    {
                        jp.Replace(new JProperty("branch_code", jp.Value));
                    }
                    if (jp.Name == "tthdon")
                    {
                        jp.Replace(new JProperty("invoice_status", jp.Value));
                    }
                    if (jp.Name == "nky")
                    {
                        jp.Replace(new JProperty("signed_date", jp.Value));
                    }
                    if (jp.Name == "ngky")
                    {
                        jp.Replace(new JProperty("signer", jp.Value));
                    }
                    if (jp.Name == "tgtttbchu")
                    {
                        jp.Replace(new JProperty("amount_to_word", jp.Value));
                    }
                    if (jp.Name == "sbmat")
                    {
                        jp.Replace(new JProperty("security_number", jp.Value));
                    }
                    if (jp.Name == "khieu")
                    {
                        jp.Replace(new JProperty("invoice_series", jp.Value));
                    }
                    if (jp.Name == "nglap")
                    {
                        jp.Replace(new JProperty("user_new", jp.Value));
                    }
                    if (jp.Name == "tdlap")
                    {
                        jp.Replace(new JProperty("date_new", jp.Value));
                    }
                    if (jp.Name == "nsua")
                    {
                        jp.Replace(new JProperty("date_edit", jp.Value));
                    }
                    if (jp.Name == "ngsua")
                    {
                        jp.Replace(new JProperty("user_edit", jp.Value));
                    }

                }
                arr_data_final.Add(data_convert);
            }

            result = arr_data_final;
            return result;
        }

        public async Task<JObject> GetInvoiceById_Pretreatment(Guid id)
        {
            JObject obj = new JObject();
            JObject obj_chitiet = new JObject();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("hdon_id", id);

            DataTable tblHoadon = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id", CommandType.Text, parameters);

            if (tblHoadon.Rows.Count == 0)
            {
                obj.Add("error", "Không tìm thấy hóa đơn");
                return obj;
            }

            DataTable tblHoaDonChiTiet = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id=@hdon_id", CommandType.Text, parameters);

            JArray jarray = JArray.FromObject(tblHoadon);

            obj = (JObject)jarray[0];

            // convert thông tin đầu phiếu từ tt32 - tt78

            foreach (JProperty jp in obj.Properties().ToList())
            {
                if (jp.Name == "cctbao_id")
                {
                    jp.Replace(new JProperty("inv_invoicecode_id", jp.Value));
                }
                if (jp.Name == "hdon_id")
                {
                    jp.Replace(new JProperty("inv_invoiceauth_id", jp.Value));
                }
                if (jp.Name == "nlap")
                {
                    jp.Replace(new JProperty("invoice_issued_date", jp.Value));
                }
                if (jp.Name == "shdon")
                {
                    jp.Replace(new JProperty("invoice_number", jp.Value));
                }
                if (jp.Name == "tthai")
                {
                    jp.Replace(new JProperty("status", jp.Value));
                }
                if (jp.Name == "hdon_id_old")
                {
                    jp.Replace(new JProperty("original_invoice_id", jp.Value));
                }
                if (jp.Name == "sdhang")
                {
                    jp.Replace(new JProperty("order_number", jp.Value));
                }
                if (jp.Name == "dvtte")
                {
                    jp.Replace(new JProperty("currency_code", jp.Value));
                }
                if (jp.Name == "tgia")
                {
                    jp.Replace(new JProperty("exchange_rate", jp.Value));
                }
                if (jp.Name == "htttoan")
                {
                    jp.Replace(new JProperty("payment_method_name", jp.Value));
                }
                if (jp.Name == "stknban")
                {
                    jp.Replace(new JProperty("seller_bank_account", jp.Value));
                }
                if (jp.Name == "tnhban")
                {
                    jp.Replace(new JProperty("seller_bank_name", jp.Value));
                }
                if (jp.Name == "customer_code")
                {
                    jp.Replace(new JProperty("mnmua", jp.Value));
                }
                if (jp.Name == "mst")
                {
                    jp.Replace(new JProperty("buyer_taxcode", jp.Value));
                }
                if (jp.Name == "tnmua")
                {
                    jp.Replace(new JProperty("buyer_display_name", jp.Value));
                }
                if (jp.Name == "email")
                {
                    jp.Replace(new JProperty("buyer_email", jp.Value));
                }
                if (jp.Name == "ten")
                {
                    jp.Replace(new JProperty("buyer_legal_name", jp.Value));
                }
                if (jp.Name == "dchi")
                {
                    jp.Replace(new JProperty("buyer_address_line", jp.Value));
                }
                if (jp.Name == "stknmua")
                {
                    jp.Replace(new JProperty("buyer_bank_account", jp.Value));
                }
                if (jp.Name == "sdtnmua")
                {
                    jp.Replace(new JProperty("buyer_tel", jp.Value));
                }
                if (jp.Name == "tnhmua")
                {
                    jp.Replace(new JProperty("buyer_bank_name", jp.Value));
                }
                if (jp.Name == "tgtcthue")
                {
                    jp.Replace(new JProperty("total_amount_without_vat", jp.Value));
                }
                if (jp.Name == "tgtthue")
                {
                    jp.Replace(new JProperty("vat_amount", jp.Value));
                }
                if (jp.Name == "tgtttbso")
                {
                    jp.Replace(new JProperty("total_amount", jp.Value));
                }
                if (jp.Name == "tgtttbso_last")
                {
                    jp.Replace(new JProperty("total_amount_last", jp.Value));
                }
                if (jp.Name == "mdvi")
                {
                    jp.Replace(new JProperty("branch_code", jp.Value));
                }
                if (jp.Name == "tthdon")
                {
                    jp.Replace(new JProperty("invoice_status", jp.Value));
                }
                if (jp.Name == "nky")
                {
                    jp.Replace(new JProperty("signed_date", jp.Value));
                }
                if (jp.Name == "ngky")
                {
                    jp.Replace(new JProperty("signer", jp.Value));
                }
                if (jp.Name == "tgtttbchu")
                {
                    jp.Replace(new JProperty("amount_to_word", jp.Value));
                }
                if (jp.Name == "sbmat")
                {
                    jp.Replace(new JProperty("security_number", jp.Value));
                }
                if (jp.Name == "khieu")
                {
                    jp.Replace(new JProperty("invoice_series", jp.Value));
                }
                if (jp.Name == "nglap")
                {
                    jp.Replace(new JProperty("user_new", jp.Value));
                }
                if (jp.Name == "tdlap")
                {
                    jp.Replace(new JProperty("date_new", jp.Value));
                }
                if (jp.Name == "nsua")
                {
                    jp.Replace(new JProperty("date_edit", jp.Value));
                }
                if (jp.Name == "ngsua")
                {
                    jp.Replace(new JProperty("user_edit", jp.Value));
                }
            }

            JArray arr_chitiet = JArray.FromObject(tblHoaDonChiTiet);

            JArray arr_ct_final = new JArray();

            for (int i = 0; i < arr_chitiet.Count; i++)
            {
                obj_chitiet = (JObject)arr_chitiet[i];

                // convert chi tiết hóa đơn
                foreach (JProperty jp in obj_chitiet.Properties().ToList())
                {
                    if (jp.Name == "cthdon_id")
                    {
                        jp.Replace(new JProperty("inv_invoiceauthdetail_id", jp.Value));
                    }
                    if (jp.Name == "hdon_id")
                    {
                        jp.Replace(new JProperty("inv_invoiceauth_id", jp.Value));
                    }
                    if (jp.Name == "stt")
                    {
                        jp.Replace(new JProperty("row_ord", jp.Value));
                    }
                    if (jp.Name == "ma")
                    {
                        jp.Replace(new JProperty("item_code", jp.Value));
                    }
                    if (jp.Name == "ten")
                    {
                        jp.Replace(new JProperty("item_name", jp.Value));
                    }
                    if (jp.Name == "mdvtinh")
                    {
                        jp.Replace(new JProperty("unit_code", jp.Value));
                    }
                    if (jp.Name == "dvtinh")
                    {
                        jp.Replace(new JProperty("unit_name", jp.Value));
                    }
                    if (jp.Name == "sluong")
                    {
                        jp.Replace(new JProperty("quantity", jp.Value));
                    }
                    if (jp.Name == "dgia")
                    {
                        jp.Replace(new JProperty("unit_price", jp.Value));
                    }
                    if (jp.Name == "thtien")
                    {
                        jp.Replace(new JProperty("total_amount_without_vat", jp.Value));
                    }
                    if (jp.Name == "tthue")
                    {
                        jp.Replace(new JProperty("vat_amount", jp.Value));
                    }
                    if (jp.Name == "tgtien")
                    {
                        jp.Replace(new JProperty("total_amount", jp.Value));
                    }
                    //if (jp.Name == "kmai")
                    //{
                    //    jp.Replace(new JProperty("promotion", jp.Value));
                    //}
                    if (jp.Name == "tlckhau")
                    {
                        jp.Replace(new JProperty("discount_percentage", jp.Value));
                    }
                    if (jp.Name == "stckhau")
                    {
                        jp.Replace(new JProperty("discount_amount", jp.Value));
                    }
                    if (jp.Name == "tsuat")
                    {
                        jp.Replace(new JProperty("tax_type", jp.Value));
                    }
                }
                arr_ct_final.Add(obj_chitiet);
            }

            //jarray = JArray.FromObject(tblHoaDonChiTiet);
            obj.Add("HoaDon68_ChiTiet", arr_ct_final);

            return obj;
        }

        public async Task<JObject> SignDangkysudung_HSM(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                Dictionary<string, object> dicParam = new Dictionary<string, object>();

                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                if (tblUser.Rows[0]["is_sign_invoice_hsm"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn bằng HSM");
                    return res;
                }
                string mst = await getMstDv();

                Guid mau01_id = Guid.Parse(model["mau01_id"].ToString());

                string userId = tblUser.Rows[0]["wb_user_id"].ToString();
                dicParam.Add("wb_user_id", Guid.Parse(userId));

                DataTable tblUser_Hsm = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id", CommandType.Text, dicParam);

                if (tblUser_Hsm.Rows.Count == 0)
                {
                    res.Add("error", "Chưa có tài khoản cấu hình");
                    return res;
                }

                // lấy account hsm portal
                string username_hsm = tblUser_Hsm.Rows[0]["username"].ToString();
                string password_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password"].ToString()));
                string password2_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password_level2"].ToString()));
                string ma_dvcs_hsm = tblUser_Hsm.Rows[0]["ma_dvcs"].ToString();

                var api = CommonConstants.LOGIN;
                JObject user = new JObject();
                user.Add("username", username_hsm);
                user.Add("password", password_hsm);
                user.Add("ma_dvcs", ma_dvcs_hsm);

                var token = "";
                var resLogin = executeLogin(CommonConstants.BASE_HSM_URL, api, user).Result;

                // kiểm tra nếu login lỗi thì thông báo
                if (resLogin["error"] != null)
                {
                    //res.Add("error", resLogin["error"].ToString());
                    //return res;
                    throw new Exception(resLogin["error"].ToString());

                }
                else
                {
                    token = resLogin["token"].ToString();
                    token = token + ";" + ma_dvcs_hsm;
                }

                await _minvoiceDbContext.BeginTransactionAsync();

                string xml = await XmlDangkysudung(mau01_id.ToString());

                var tencty = await getTenDv();

                XmlDocument document = new XmlDocument();
                document.PreserveWhitespace = false;
                document.LoadXml(xml);

                XmlNode nodeTT = document.SelectSingleNode("/HDon/DLHDon/TTKhac");
                if (nodeTT != null)
                {
                    XmlElement TTin1 = document.CreateElement("TTin");
                    XmlElement TTruong1 = document.CreateElement("TTruong");
                    TTruong1.InnerText = "DVKy";
                    XmlElement KDLieu1 = document.CreateElement("KDLieu");
                    KDLieu1.InnerText = "string";
                    XmlElement DLieu1 = document.CreateElement("DLieu");
                    DLieu1.InnerText = tencty;
                    TTin1.AppendChild(TTruong1);
                    TTin1.AppendChild(KDLieu1);
                    TTin1.AppendChild(DLieu1);


                    XmlElement TTin2 = document.CreateElement("TTin");
                    XmlElement TTruong2 = document.CreateElement("TTruong");
                    TTruong2.InnerText = "NKy";
                    XmlElement KDLieu2 = document.CreateElement("KDLieu");
                    KDLieu2.InnerText = "date";
                    XmlElement DLieu2 = document.CreateElement("DLieu");
                    DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                    TTin2.AppendChild(TTruong2);
                    TTin2.AppendChild(KDLieu2);
                    TTin2.AppendChild(DLieu2);

                    nodeTT.AppendChild(TTin1);
                    nodeTT.AppendChild(TTin2);
                }

                xml = document.OuterXml;

                // Call Sign HSM API
                JObject result = new JObject();

                api = CommonConstants.SIGN_HSM_URL_78;
                JObject json = new JObject();
                json.Add("password2", password2_hsm);
                json.Add("idData", "#data");
                json.Add("idSigner", "seller");
                json.Add("tagSign", "NNT");
                json.Add("dataType", "xml");
                json.Add("xml", xml);

                var resSign = executePost(CommonConstants.BASE_HSM_URL, api, token, json).Result;

                if ("false".Equals(resSign["status"].ToString().ToLower()))
                {
                    //res.Add("error", resSign["message"].ToString());
                    //return res;
                    throw new Exception(resSign["message"].ToString());
                }
                else
                {
                    xml = resSign["data"].ToString();
                }
                //xml = xml.Replace("'", "''");

                string sqlUpdate = "UPDATE #SCHEMA_NAME#.mau01_68 SET xml_send = @xml_send, trang_thai = @trang_thai, ngay_gui = @ngay_gui, nguoi_gui = @nguoi_gui WHERE mau01_id = @mau01_id";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("mau01_id", mau01_id);
                parameters.Add("xml_send", xml);
                parameters.Add("ngay_gui", DateTime.Now);
                parameters.Add("nguoi_gui", _webHelper.GetUser());
                parameters.Add("trang_thai", "1");

                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters);

                bool isUyNhiem = await DangkysudungUyNhiem(mau01_id.ToString());

                // lấy thông tin xml TTChung của Thông điệp
                string mltdiep = isUyNhiem ? CommonConstants.MLTDIEP_TO_KHAI_UY_NHIEM : CommonConstants.MLTDIEP_TO_KHAI_KHONG_UY_NHIEM;
                int sluong = 1;
                string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, mltdiep, sluong);

                // lấy thông tin xml dữ liệu
                //XElement xml_DuLieu = await XmlDangkysudungNew(mau01_id.ToString());

                //string xmlDuLieu = await XmlDangkysudung(mau01_id.ToString());
                //string xmlKy = CommonManager.SignXml(await getTenDv(), "#data", "seller", xmlDuLieu, certFile, "NNT");

                // đẩy dữ liệu hóa đơn vào thẻ DLieu
                XElement dLieu = new XElement("DLieu");
                dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlThongDiep.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                //xml = xmlThongDiep.ToString();

                // ký thông điệp gửi lên thuế
                //xml = CommonManager.SignThongDiep(xml, certFile, pass);

                // đóng gói xml gửi lên TVAN
                string cmd = isUyNhiem ? CommonConstants.MLTDIEP_TO_KHAI_UY_NHIEM : CommonConstants.MLTDIEP_TO_KHAI_KHONG_UY_NHIEM;

                string data = await ThongDiepGui(cmd, xml);

                // lưu vào bảng tonghop_gui_tvan_68

                if (CommonManager.CheckThoiGian(xml) == false)
                {
                    //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    //return res;
                    throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                }

                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                param_Insert.Add("id", Guid.NewGuid());
                param_Insert.Add("mltdiep_gui", cmd);
                param_Insert.Add("xml_tdiep_gui", xml);
                param_Insert.Add("type_id", mau01_id);
                param_Insert.Add("mtdiep_gui", mtdiep_gui);
                //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                param_Insert.Add("note", "app");
                param_Insert.Add("mdvi", _webHelper.GetDvcs());

                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                // update mã thông điệp gửi vào bảng mau01_68
                param_Insert.Clear();
                param_Insert.Add("mau01_id", mau01_id);
                param_Insert.Add("mtdiep_gui", mtdiep_gui);

                string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.mau01_68 SET mtdiep_gui=@mtdiep_gui WHERE mau01_id = @mau01_id";
                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param_Insert);

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                // đẩy queue lên TVAN
                _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                //_rabbitMQService.PublishTaskQueue(_queueNameDkPhathanh, rabbitObj.ToString());
                //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());

                //_kafkaService.PublishTopic(_topicVendorSendToVan, data);

                //sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                //DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                //JArray array = JArray.FromObject(tblInvoices);

                //res.Add("ok", true);
                //res.Add("inv", (JObject)array[0]);
                //res.Add("hdon_id", hdon_id);
                //res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                //res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                //res.Add("tthai", "Đã ký");

            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async static Task<JObject> executePost(string baseUrl, string api, string token, JObject json)
        {
            //string token = await Login();
            JObject result = new JObject();
            try
            {
                HttpClient client = new HttpClient();

                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bear", token);

                HttpResponseMessage respon = client.PostAsJsonAsync(api, json).Result;
                string conten = await respon.Content.ReadAsStringAsync();

                result = JObject.Parse(conten);
            }
            catch (Exception)
            {
                result = new JObject();
            }

            return result;
        }

        public async static Task<JObject> executeLogin(string baseUrl, string api, JObject json)
        {
            JObject result = new JObject();
            try
            {
                HttpClient client = new HttpClient();

                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage respon = client.PostAsJsonAsync(api, json).Result;
                string conten = await respon.Content.ReadAsStringAsync();

                result = JObject.Parse(conten);
            }
            catch (Exception)
            {
                result = new JObject(); ;
            }

            return result;
        }

        public async Task<string> ExportXMLBangKe_HD(string id)
        {

            string xml_bangke = "";
            try
            {
                string qry = $"SELECT dlxml_bangke FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id ='{id}'";
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(qry);
                xml_bangke = dt.Rows[0]["dlxml_bangke"].ToString();
            }
            catch (Exception ex)
            {
            }
            return xml_bangke;
        }

        public async Task<string> UploadBangKeTemplate(string ma_dvcs, string user_login, string dmmauhoadon_id, byte[] bytes)
        {
            string report = System.Text.Encoding.UTF8.GetString(bytes);
            string sql = "UPDATE #SCHEMA_NAME#.quanlymau68 SET dulieumaubangke=@report WHERE qlmtke_id=@qlmtke_id";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("report", report);
            parameters.Add("qlmtke_id", Guid.Parse(dmmauhoadon_id));

            await _minvoiceDbContext.ExecuteNoneQueryAsyncNoneSQLInjection(sql, parameters);

            return "";
        }

        public async Task<byte[]> inMauHoadon_QuanLyMau(string id, string type, string xml, bool checkBangKe = false)
        {
            byte[] bytes = null;

            string qry = $"SELECT * FROM #SCHEMA_NAME#.wb_branch WHERE code='{_webHelper.GetDvcs()}'";
            DataTable dmdvcs = await this._minvoiceDbContext.GetDataTableAsync(qry);

            DataTable tblDmmauhd = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + id + "'");
            string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

            if (checkBangKe == true)
            {
                if (!string.IsNullOrEmpty(tblDmmauhd.Rows[0]["dulieumaubangke"].ToString()))
                {
                    invReport = tblDmmauhd.Rows[0]["dulieumaubangke"].ToString();
                }
                else
                {
                    return bytes;
                }
            }

            XtraReport report = ReportUtil.LoadReportFromString(invReport);

            //   }
            xml = xml.Replace("#TenCongTy#", dmdvcs.Rows[0]["name"].ToString()).Replace("#MaSoThue#", dmdvcs.Rows[0]["tax_code"].ToString()).Replace("#DiaChiCongTy#", dmdvcs.Rows[0]["address"].ToString());

            report.Name = "XtraReport1";
            report.ScriptReferencesString = "AccountSignature.dll";

            DataSet ds = new DataSet();

            using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
            {
                ds.ReadXmlSchema(xmlReader);
                xmlReader.Close();
            }

            using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
            {
                ds.ReadXml(xmlReader);
                xmlReader.Close();
            }

            if (ds.Tables.Contains("TblXmlData"))
            {
                ds.Tables.Remove("TblXmlData");
            }


            DataTable tblXmlData = new DataTable();
            tblXmlData.TableName = "TblXmlData";
            tblXmlData.Columns.Add("data");

            DataRow r = tblXmlData.NewRow();
            r["data"] = xml;
            tblXmlData.Rows.Add(r);
            ds.Tables.Add(tblXmlData);

            string datamember = report.DataMember;

            if (datamember.Length > 0)
            {
                if (ds.Tables.Contains(datamember))
                {
                    DataTable tblChiTiet = ds.Tables[datamember];
                    int rowcount = ds.Tables[datamember].Rows.Count;
                }
            }
            if (report.Parameters["MSG_TB"] != null)
            {
                report.Parameters["MSG_TB"].Value = "";
            }

            if (report.Parameters["MCCQT"] != null)
            {
                report.Parameters["MCCQT"].Value = "";
            }

            // số bảo mật
            if (report.Parameters["SoBaoMat"] != null)
            {
                report.Parameters["SoBaoMat"].Value = "";
            }

            //check in bảng kê
            //check in bảng kê
            if (checkBangKe == true)
            {
                if (report.Parameters["CheckBangKe"] != null)
                {
                    report.Parameters["CheckBangKe"].Value = 1;
                }
            }
            else
            {
                if (report.Parameters["CheckBangKe"] != null)
                {
                    report.Parameters["CheckBangKe"].Value = 0;
                }

            }
            //var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

            //if (lblHoaDonMau != null)
            //{
            //    lblHoaDonMau.Visible = false;
            //}


            report.DataSource = ds;
            report.CreateDocument();
            MemoryStream ms = new MemoryStream();


            if (type == "Html")
            {
                report.ExportOptions.Html.EmbedImagesInHTML = true;
                report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                report.ExportOptions.Html.Title = "Mẫu hóa đơn";
                report.ExportToHtml(ms);

                string html = Encoding.UTF8.GetString(ms.ToArray());

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);


                string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                if (nodes != null)
                {
                    foreach (HtmlNode node in nodes)
                    {
                        string eventMouseDown = node.Attributes["onmousedown"].Value;

                        if (eventMouseDown.Contains("showCert('seller')"))
                        {
                            node.SetAttributeValue("id", "certSeller");
                        }
                        if (eventMouseDown.Contains("showCert('buyer')"))
                        {
                            node.SetAttributeValue("id", "certBuyer");
                        }
                        if (eventMouseDown.Contains("showCert('vacom')"))
                        {
                            node.SetAttributeValue("id", "certVacom");
                        }
                        if (eventMouseDown.Contains("showCert('minvoice')"))
                        {
                            node.SetAttributeValue("id", "certMinvoice");
                        }
                    }
                }

                HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                HtmlNode xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("id", "xmlData");
                xmlNode.SetAttributeValue("type", "text/xmldata");

                xmlNode.AppendChild(doc.CreateTextNode(xml));
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                head.AppendChild(xmlNode);

                xmlNode = doc.CreateElement("script");
                xmlNode.SetAttributeValue("type", "text/javascript");

                xmlNode.InnerHtml = "$(function () { "
                                   + "  var url = 'http://localhost:19898/signalr'; "
                                   + "  var connection = $.hubConnection(url, {  "
                                   + "     useDefaultPath: false "
                                   + "  });"
                                   + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                   + " invoiceHubProxy.on('resCommand', function (result) { "
                                   + " }); "
                                   + " connection.start().done(function () { "
                                   + "      console.log('Connect to the server successful');"
                                   + "      $('#certSeller').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'seller' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certVacom').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'vacom' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certBuyer').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'buyer' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "      $('#certMinvoice').click(function () { "
                                   + "         var arg = { "
                                   + "              xml: document.getElementById('xmlData').innerHTML, "
                                   + "              id: 'minvoice' "
                                   + "         }; "
                                   + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                   + "         }); "
                                   + "})"
                                   + ".fail(function () { "
                                   + "     alert('failed in connecting to the signalr server'); "
                                   + "});"
                                   + "});";

                head.AppendChild(xmlNode);

                if (report.Watermark != null)
                {
                    if (report.Watermark.Image != null)
                    {
                        ImageConverter _imageConverter = new ImageConverter();
                        byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                        string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                        HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                        string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                        string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                        HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                        style.AppendChild(textNode);

                        HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                        HtmlNodeCollection pageNodes = body.SelectNodes("div");

                        foreach (var pageNode in pageNodes)
                        {
                            pageNode.Attributes.Add("class", "waterMark");

                            string divStyle = pageNode.Attributes["style"].Value;
                            divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                            pageNode.Attributes["style"].Value = divStyle;
                        }
                    }
                }

                ms.SetLength(0);
                doc.Save(ms);

                doc = null;
            }
            else if (type == "Image")
            {
                var options = new ImageExportOptions(ImageFormat.Png)
                {
                    ExportMode = ImageExportMode.SingleFilePageByPage,
                };

                report.ExportToImage(ms, options);
            }
            else
            {
                report.ExportToPdf(ms);
            }

            bytes = ms.ToArray();
            return bytes;
        }

        public async Task<JObject> GetCTS_HSM_ToKhai()
        {
            JObject res = new JObject();
            try
            {
                var username = _webHelper.GetUser();
                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice_hsm"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn bằng HSM");
                    return res;
                }

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                string userId = tblUser.Rows[0]["wb_user_id"].ToString();
                dicParam.Add("wb_user_id", Guid.Parse(userId));

                DataTable tblUser_Hsm = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id", CommandType.Text, dicParam);

                if (tblUser_Hsm.Rows.Count == 0)
                {
                    res.Add("error", "Chưa có tài khoản cấu hình HSM");
                    return res;
                }
                // lấy account hsm portal
                string username_hsm = tblUser_Hsm.Rows[0]["username"].ToString();
                string password_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password"].ToString()));
                string password2_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password_level2"].ToString()));
                string ma_dvcs_hsm = tblUser_Hsm.Rows[0]["ma_dvcs"].ToString();

                // call api login
                HttpClient client = new HttpClient();

                var api = CommonConstants.LOGIN;
                JObject user = new JObject();
                user.Add("username", username_hsm);
                user.Add("password", password_hsm);
                user.Add("ma_dvcs", ma_dvcs_hsm);

                var token = "";
                var resLogin = executeLogin(CommonConstants.BASE_HSM_URL, api, user).Result;

                // kiểm tra nếu login lỗi thì thông báo
                if (resLogin["error"] != null)
                {
                    //res.Add("error", resLogin["error"].ToString());
                    //return res;
                    throw new Exception(resLogin["error"].ToString());
                }
                else
                {
                    token = resLogin["token"].ToString();
                    token = token + ";" + ma_dvcs_hsm;
                }

                var api_cts = CommonConstants.CTS_HSM;
                JObject cts = new JObject();
                cts.Add("password2", password2_hsm);

                var resSign = executePost(CommonConstants.BASE_HSM_URL, api_cts, token, cts).Result;
                JObject result = (JObject)resSign;
                var status = result["status"].ToString();
                if (status == "OK")
                {
                    return result;
                }
                else
                {
                    res.Add("error", result["message"].ToString());
                    return res;
                }
            }
            catch (Exception)
            {
                return res;
            }
        }

        public async Task<JObject> SaveBangTHDLXangDau(JObject obj, string username)
        {
            JObject json = new JObject();
            bool isError = false;

            try
            {
                string editMode = (string)obj["editmode"];
                var id = Guid.NewGuid();
                JArray data = (JArray)obj["data"];

                for (int i = 0; i < data.Count; i++)
                {
                    JObject masterObj = (JObject)data[i];

                    #region Set mã đơn vị, ngày lập , ngày sửa, người sửa, người lập
                    if (editMode == "1")
                    {
                        if (masterObj["trang_thai"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["trang_thai"].ToString()))
                            {
                                masterObj["trang_thai"] = "0";
                            }
                        }
                        else
                        {
                            masterObj.Add("trang_thai", "0");
                        }

                        if (masterObj["nglap"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["nglap"].ToString()))
                            {
                                masterObj["nglap"] = username;
                            }
                        }
                        else
                        {
                            masterObj.Add("nglap", username);
                        }

                        if (masterObj["nlap"] != null)
                        {
                            masterObj["nlap"] = DateTime.Now;
                        }
                        else
                        {
                            masterObj.Add("nlap", DateTime.Now);
                        }

                        if (masterObj["bangtonghopdl_68_id"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["bangtonghopdl_68_id"].ToString()))
                            {
                                masterObj["bangtonghopdl_68_id"] = id;
                            }
                            else
                            {
                                id = Guid.Parse(masterObj["bangtonghopdl_68_id"].ToString());
                            }
                        }
                        else
                        {
                            masterObj.Add("bangtonghopdl_68_id", id);
                        }

                        if (masterObj["mdvi"] != null)
                        {
                            if (string.IsNullOrEmpty(masterObj["mdvi"].ToString()))
                            {
                                masterObj["mdvi"] = _webHelper.GetDvcs();
                            }
                        }
                        else
                        {
                            masterObj.Add("mdvi", _webHelper.GetDvcs());
                        }

                    }

                    if (editMode == "2")
                    {
                        ////if (masterObj["nglap"] != null)
                        ////{
                        ////    string user_new = masterObj["hdon_id"].ToString();

                        ////    if (isedituser != "C" && user_new != username)
                        ////    {
                        ////        json.Add("error", "Bạn không có quyền sửa chứng từ của người dùng khác");
                        ////        return json;
                        ////    }
                        ////}

                        //if (masterObj["ngsua"] != null)
                        //{
                        //    if (string.IsNullOrEmpty(masterObj["ngsua"].ToString()))
                        //    {
                        //        masterObj["ngsua"] = username;
                        //    }
                        //}
                        //else
                        //{
                        //    masterObj.Add("ngsua", username);
                        //}

                        //if (masterObj["nsua"] != null)
                        //{
                        //    masterObj["nsua"] = DateTime.Now;
                        //}
                        //else
                        //{
                        //    masterObj.Add("nsua", DateTime.Now);
                        //}

                        id = Guid.Parse(masterObj["bangtonghopdl_68_id"].ToString());

                    }

                    if (editMode == "3")
                    {
                        //if (masterObj["nglap"] != null)
                        //{
                        //    string user_new = masterObj["nglap"].ToString();

                        //    if (isdeluser != "C" && user_new != username)
                        //    {
                        //        json.Add("error", "Bạn không có quyền xóa chứng từ của người dùng khác");
                        //        return json;
                        //    }
                        //}
                    }
                    #endregion

                    string sql = editMode == "1" ? "#SCHEMA_NAME#.crd_bangtonghopdl_68_insert" : editMode == "2" ? "#SCHEMA_NAME#.crd_bangtonghopdl_68_update" : "#SCHEMA_NAME#.crd_bangtonghopdl_68_delete";

                    await _minvoiceDbContext.BeginTransactionAsync();
                    await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);

                    JArray details = null;

                    if (masterObj["details"] != null)
                    {
                        if (masterObj.SelectToken("details") is JArray)
                        {
                            details = (JArray)masterObj["details"];
                        }
                    }

                    #region Chi tiết hóa đơn
                    if (details != null)
                    {
                        sql = "DELETE FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd WHERE bangtonghopdl_68_id = '" + masterObj["bangtonghopdl_68_id"] + "'";
                        await this._minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)null);
                        foreach (var dtl in details)
                        {
                            JObject js_details = (JObject)dtl;

                            if (js_details["bangtonghopdl_68_id"] != null)
                            {
                                if (string.IsNullOrEmpty(js_details["bangtonghopdl_68_id"].ToString()))
                                {
                                    js_details["bangtonghopdl_68_id"] = masterObj["bangtonghopdl_68_id"];
                                }
                            }
                            else
                            {
                                js_details.Add("bangtonghopdl_68_id", masterObj["bangtonghopdl_68_id"]);
                            }

                            if (js_details["bangtonghopdl_68_chitiet_xd_id"] != null)
                            {
                                if (string.IsNullOrEmpty(js_details["bangtonghopdl_68_chitiet_xd_id"].ToString()))
                                {
                                    js_details["bangtonghopdl_68_chitiet_xd_id"] = Guid.NewGuid();
                                }
                            }
                            else
                            {
                                js_details.Add("bangtonghopdl_68_chitiet_xd_id", Guid.NewGuid());
                            }

                            //if (fieldStt_rec0 != null)
                            //{
                            //    js_details["row_ord"] = prefix + (j + 1).ToString().PadLeft(9, '0');
                            //}

                            await this._minvoiceDbContext.TransactionCommandAsync("#SCHEMA_NAME#.crd_bangtonghopdl_68_chitiet_xd_insert", CommandType.StoredProcedure, js_details);
                        }

                    }
                    #endregion

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }

                if (editMode != "3")
                {
                    //string select_cmd = $"SELECT bangtonghopdl_68_id as id , * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{id}'";
                    //DataTable tbl = await this._minvoiceDbContext.GetDataTableAsync(select_cmd);
                    //data = JArray.FromObject(tbl);
                    //json.Add("data", data[0]);
                }

                json.Add("ok", "true");

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
                isError = true;
            }

            if (isError)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();
            }

            return json;
        }

        public async Task<JArray> GetDetailHoaDonXangDauChoXuLy(string data)
        {
            try
            {
                string sqlCheck = $@"select a.shdon from #SCHEMA_NAME#.hoadon68 a inner join #SCHEMA_NAME#.hoadon68_chitiet b on a.hdon_id = b.hdon_id
                                     where (b.mdvtinh is null or b.mdvtinh = '') and POSITION(',''' || a.hdon_id || ''',' IN ',' || '{data.Replace("'", "''")}' || ',') > 0
                                     group by a.shdon
                                     order by a.shdon";
                DataTable checkDataTable = await _minvoiceDbContext.GetDataTableAsync(sqlCheck, CommandType.Text, "");
                if (checkDataTable.Rows.Count > 0)
                {
                    var checkResult = JArray.FromObject(checkDataTable);
                    return checkResult;
                }
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());
                parameters.Add("p_list_hoadon_id", data);

                //var invoiceDb = this._nopDbContext.GetInvoiceDb();
                string sql =
                    "SELECT * FROM #SCHEMA_NAME#.rp_bc_bthdl_68_xangdau('#SCHEMA_NAME#',@ma_dvcs,@p_list_hoadon_id)";
                DataTable resultDataTable =
                    await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray result = JArray.FromObject(resultDataTable);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<JArray> GetDataBangTHDLXangDauDetail(string id)
        {
            JArray result = new JArray();
            string sql = "SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd WHERE bangtonghopdl_68_id = '" + id + "' ORDER BY Cast (stt as numeric)";
            result = JArray.FromObject(await this._minvoiceDbContext.GetDataTableAsync(sql));
            return result;
        }

        public async Task<JObject> SignBTHDLXangDau(JObject model)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                string branch_code = model["branch_code"].ToString();
                string username = model["username"].ToString();

                string check_HSM = model["CheckHSM"] != null ? model["CheckHSM"].ToString() : "";

                if (username != _webHelper.GetUser())
                {
                    res.Add("error", "Sai mã xác thực");
                    return res;
                }

                if (branch_code != _webHelper.GetDvcs())
                {
                    res.Add("error", "Sai đơn vị");
                    return res;
                }

                DataTable tblUser = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user WHERE username=N'" + username + "'");

                if (tblUser.Rows[0]["is_sign_invoice"].ToString() != "C")
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn");
                    return res;
                }

                if (tblUser.Rows[0]["is_sign_invoice_hsm"].ToString() != "C" && !string.IsNullOrEmpty(check_HSM))
                {
                    res.Add("error", "Bạn không có quyền ký hóa đơn bằng HSM");
                    return res;
                }

                //hatm lấy chuỗi id và thay đổi vào bangtonghopdl_id
                JArray bangtonghopdl_id_array = (JArray)model["id"];
                for (int i = 0; i < bangtonghopdl_id_array.Count(); i++)
                {
                    string bangtonghopdl_id = "(";
                    bangtonghopdl_id += $"'{bangtonghopdl_id_array[i].ToString()}')";
                    X509Certificate2 certFile = null;
                    DataTable tblCertificate = null;
                    if (string.IsNullOrEmpty(check_HSM))
                    {
                        string cert_serial = model["cer_serial"] == null ? "" : model["cer_serial"].ToString();

                        string sql = "SELECT a.* FROM #SCHEMA_NAME#.pl_certificate a \n"
                                + "INNER JOIN #SCHEMA_NAME#.pl_certificate_permission b ON a.pl_certificate_id=b.pl_certificate_id \n"
                                + "INNER JOIN #SCHEMA_NAME#.wb_user c ON b.wb_user_id=c.wb_user_id \n"
                                + "WHERE a.cert_type in ('FILE','SIM','HSM_SCTV') AND a.end_date>=current_timestamp  AND c.username='" + username + "'";

                        if (branch_code.Length > 0)
                        {
                            sql += " AND a.branch_code='" + branch_code + "' ";
                        }

                        if (cert_serial.Length > 0)
                        {
                            sql += " AND a.cer_serial='" + cert_serial + "'";
                        }

                        sql += " ORDER BY end_date desc LIMIT 1";

                        tblCertificate = await _minvoiceDbContext.GetDataTableAsync(sql);
                        if (tblCertificate.Rows.Count == 0)
                        {
                            res.Add("error", "Không tìm thấy chứng thư số");
                        }

                        /*string pass = tblCertificate.Rows[0]["pass"].ToString();
                        byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];

                        certFile = new X509Certificate2(bytes, pass);*/
                    }


                    await _minvoiceDbContext.BeginTransactionAsync();
                    //DataTable dtHdon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='" + bangtonghopdl_id + "'");
                    //int shdon = await tangSoBTHDL();

                    string mst = await getMstDv();
                    string mtdiep_gui = CommonManager.GetMaThongDiep(mst);

                    JObject ObjXml = await XmlBangTHDLXangDau(bangtonghopdl_id, mtdiep_gui, tblCertificate, tblUser, check_HSM);

                    //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                    string xml = ObjXml["xml"].ToString();

                    //xml = CommonManager.SignXml("#data", "seller", xml, certFile, "NNT");

                    string sqlUpdate = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET trang_thai=@trang_thai, xml_send = @xml_send, ngay_ky = @ngay_ky, ngky = @ngky WHERE bangtonghopdl_68_id in {bangtonghopdl_id}";
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("trang_thai", 1);
                    parameters2.Add("xml_send", xml);
                    //parameters2.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    parameters2.Add("ngay_ky", DateTime.Now);
                    parameters2.Add("ngky", username);
                    //parameters2.Add("sbthdlieu", shdon);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                    // lấy thông tin xml TTChung của Thông điệp
                    //string mst = await getMstDv();
                    //string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                    //XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_BTHDL, 1);

                    // lấy thông tin xml dữ liệu
                    //XElement xml_DuLieu = await XmlBangTHDLNew(model["id"].ToString(), shdon);

                    // đẩy dữ liệu hóa đơn vào thẻ DLieu
                    XElement dLieu = new XElement("DLieu");
                    dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                    XDocument xmlThongDiep = new XDocument(XElement.Parse(xml, LoadOptions.PreserveWhitespace));

                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;

                    using (var buffer = new StringWriter())
                    using (var writer = XmlWriter.Create(buffer, writerSettings))
                    {
                        xmlThongDiep.Save(writer);
                        writer.Flush();
                        xml = buffer.ToString();
                    }

                    //xml = xmlThongDiep.ToString();

                    // ký thông điệp gửi lên thuế
                    //xml = CommonManager.SignThongDiep(xml, certFile, pass);

                    // đóng gói xml gửi lên TVAN
                    //string cmd = CommonConstants.MLTDIEP_BTHDL;

                    string data = await ThongDiepGui(CommonConstants.MLTDIEP_BTHDL, xml);

                    // lưu vào bảng tonghop_gui_tvan_68
                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }
                    foreach (var item in bangtonghopdl_id.Remove(bangtonghopdl_id.Length - 1, 1).Remove(0, 1).Split(','))
                    {
                        string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                      + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                        Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                        param_Insert.Add("id", Guid.NewGuid());
                        param_Insert.Add("mltdiep_gui", CommonConstants.MLTDIEP_BTHDL);
                        param_Insert.Add("xml_tdiep_gui", xml);
                        param_Insert.Add("type_id", Guid.Parse(item.Replace("'", "")));
                        param_Insert.Add("mtdiep_gui", mtdiep_gui);
                        //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                        param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                        param_Insert.Add("note", "app");
                        param_Insert.Add("mdvi", _webHelper.GetDvcs());

                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                    }
                    // update vào bảng bangtonghopdl_68
                    Dictionary<string, object> param_Insert_f = new Dictionary<string, object>();
                    //param_Insert.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    param_Insert_f.Add("mtdiep_gui", mtdiep_gui);

                    string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET mtdiep_gui=@mtdiep_gui WHERE bangtonghopdl_68_id in {bangtonghopdl_id}";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param_Insert_f);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    // đẩy queue lên TVAN
                    if (!CommonManager.CheckData(data, 2 * 1024 * 1024))
                    {
                        error = " Dữ liệu không vượt quả 2MB, Số hóa đơn nên ở số lượng từ 0-> 1000";
                    }
                    _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                    //_rabbitMQService.PublishTaskQueue(_queueNameInvoice, rabbitObj.ToString());
                    //_kafkaService.PublishTopic(_topicVendorSendToVan, rabbitObj.ToString());
                    //_kafkaService.PublishTopic(_topicVendorSendToVan, data);

                    //sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'";
                    //DataTable tblInvoices = await _minvoiceDbContext.GetDataTableAsync(sql);

                    //JArray array = JArray.FromObject(tblInvoices);

                    //res.Add("ok", true);
                    //res.Add("inv", (JObject)array[0]);
                    //res.Add("hdon_id", hdon_id);
                    //res.Add("tdlap", String.Format("{0:yyyy-MM-dd}", tblInvoices.Rows[0]["tdlap"]));
                    //res.Add("shdon", tblInvoices.Rows[0]["shdon"].ToString());
                    //res.Add("tthai", "Đã ký");

                    //Đổi trạng thái của các hoá đơn trong BTHDL đã ký và gửi lên cơ quan thuế
                    /*string sqlUpdate3 = "UPDATE #SCHEMA_NAME#.hoadon68 a SET tthai = 'Đã gửi', kygui_cqt =1 "
                                      + "WHERE tthai = 'Đã ký' AND POSITION( '' || a.hdon_id || '' in ("
                                      + "SELECT b.hdon_id FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd b "
                                      + $"WHERE bangtonghopdl_68_id in {bangtonghopdl_id} "
                                      + "GROUP BY b.hdon_id)) > 0";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdate3);*/
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (error.Length > 0)
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async Task<JObject> XmlBangTHDLXangDau(string bangtonghopdl_68_id, string mtdiep_gui, DataTable tblCertificate, DataTable tblUser, string check_HSM)
        {
            JObject _obj = new JObject();
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id in {bangtonghopdl_68_id} order by sbthdlieu";

                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);

                XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_BTHDL, dauphieu.Rows.Count);
                XElement xml_DLieu = new XElement("DLieu");
                foreach (DataRow item in dauphieu.Rows)
                {
                    string thang = "";
                    string ngay = "";

                    if (!string.IsNullOrEmpty(item["thang"].ToString()))
                    {
                        int thangDT = Convert.ToInt16(item["thang"].ToString());
                        thang = thangDT.ToString();
                        if (thangDT < 10)
                        {
                            thang = "0" + thangDT;
                        }
                    }

                    if (!string.IsNullOrEmpty(item["ngay"].ToString()))
                    {
                        int ngayDT = Convert.ToInt16(item["ngay"].ToString());
                        ngay = ngayDT.ToString();
                        if (ngayDT < 10)
                        {
                            ngay = "0" + ngayDT;
                        }
                    }

                    string fromDate = ((DateTime)item["tu_ngay"]).ToString("yyyy-MM-dd");
                    string toDate = ((DateTime)item["den_ngay"]).ToString("yyyy-MM-dd");

                    int sbthdlieu = await tangSoBTHDL(fromDate, toDate);

                    await this._minvoiceDbContext.ExecuteNoneQueryAsync("Update #SCHEMA_NAME#.bangtonghopdl_68 set sbthdlieu=" + sbthdlieu + " WHERE sbthdlieu is null AND bangtonghopdl_68_id ='" + item["bangtonghopdl_68_id"].ToString() + "'");

                    string sqlBangTongHop = "SELECT COALESCE(sbthdlieu, -1) sbthdlieu"
                 + $" FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{item["bangtonghopdl_68_id"].ToString()}'";
                    DataTable bangtonghopDauPhieu = await this._minvoiceDbContext.GetDataTableAsync(sqlBangTongHop);

                    if (bangtonghopDauPhieu.Rows.Count > 0)
                    {
                        int valuesbthdlieu = Convert.ToInt32(bangtonghopDauPhieu.Rows[0]["sbthdlieu"].ToString());
                        if (valuesbthdlieu != -1)
                        {
                            sbthdlieu = valuesbthdlieu;
                        }
                    }

                    string sqlDetails = "SELECT *"
                    + $" FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd WHERE bangtonghopdl_68_id ='{item["bangtonghopdl_68_id"].ToString()}' order by Cast (stt as numeric)";
                    DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                    //XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                    //XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                    DateTime dt = Convert.ToDateTime(item["nlap"].ToString());
                    XElement xmlBTHDL_O = new XElement("BTHDLieu",
                                new XElement("DLBTHop", new XAttribute("Id", "data"),
                                    new XElement("TTChung",
                                        new XElement("PBan", "2.0.0"),
                                        new XElement("MSo", "01/TH-HĐĐT"),
                                        new XElement("Ten", "Bảng tổng hợp dữ liệu hóa đơn điện tử"),
                                        new XElement("SBTHDLieu", sbthdlieu),
                                        GetDataRow("LKDLieu", item, "lkdlieu"),
                                        new XElement("KDLieu", (item["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + item["nam"].ToString()),
                                        GetDataRow("LDau", item, "ldau"),
                                        GetDataRow("BSLThu", item, "bslthu"),
                                        // GetDataRow("SDLThu", item, "SDlthu"),
                                        new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                        GetDataRow("TNNT", item, "tnnt"),
                                        GetDataRow("MST", item, "mst"),
                                        GetDataRow("HDDIn", item, "hddin"),
                                        GetDataRow("LHHoa", item, "lhhoa")
                                    ),
                                    new XElement("NDBTHDLieu",
                                            new XElement("DSDLieu",
                                                from row in chitiet.AsEnumerable()
                                                select new XElement("DLieu",
                                                    new XElement("STT", row["stt"].ToString()),
                                                    row["khieu"] == null || row["khieu"].ToString() == "" ? null : new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                                    row["khieu"] == null || row["khieu"].ToString() == "" ? null : new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                                    GetDataRow("SHDon", row, "shdon"),
                                                    GetDataRowDate("NLap", row, "nlap"),
                                                    GetDataRow("TNMua", row, "tnmua"),
                                                    GetDataRow("MSTNMua", row, "mstnmua"),
                                                    GetDataRow("MKHang", row, "mkhang"),
                                                    GetDataRow("MHHDVu", row, "mhhoa"),
                                                    GetDataRow("THHDVu", row, "thhdvu"),
                                                    GetDataRow("DVTinh", row, "dvtinh"),
                                                    GetDataRowDoubble("SLuong", row, "sluong"),
                                                    GetDataRowDoubble("TTCThue", row, "ttcthue"),
                                                    GetDataRow("TSuat", row, "tsuat"),
                                                    GetDataRowDoubble("TgTThue", row, "tgtthue"),
                                                    GetDataRowDoubble("TgTTToan", row, "tgtttoan"),
                                                    GetDataRow("TThai", row, "tthai"),
                                                    GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                                    GetDataRow("SHDCLQuan", row, "shdlquan"),
                                                    GetDataRow("LKDLDChinh", dauphieu.Rows[0], "lkdlieu"),
                                                    new XElement("KDLDChinh", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                                    GetDataRow("STBao", row, "stbao"),
                                                    GetDataRowDate("NTBao", row, "ntbao"),
                                                    GetDataRow("GChu", row, "gchu")
                                                )
                                            )
                                        )
                                ),
                                new XElement("DSCKS",
                                    new XElement("NNT"),
                                    new XElement("CCKSKhac")
                                    )
                        );
                    string xml_o = "";
                    string tenCty = await getTenDv();
                    if (string.IsNullOrEmpty(check_HSM))
                    {
                        xml_o = "";
                        if (tblCertificate.Rows[0]["cert_type"].ToString() == "SIM")
                        {
                            xml_o = CommonManager.ThemPhanDonViKiThoiGianKy(xmlBTHDL_O.ToString(), await getTenDv());
                            SignXML input = new SignXML()
                            {
                                msisdn = tblCertificate.Rows[0]["token_serial"].ToString(),
                                xmlData = xml_o,
                                namespacePrefix = "",
                                reason = "Ky bang tong hop du lieu",
                                signatureTagId = "seller",
                                signingTagId = "data",
                                signingTagName = "NNT",
                                prompt = "Ky bang tong hop du lieu"
                            };
                            SignedXmlReturn resultSign = CommonManager.SignSimPKI(input);
                            //xml = CommonManager.SignByMInvoice(xml, "Signatures");
                            xml_o = resultSign.xmlSignedData;
                        }
                        else if (tblCertificate.Rows[0]["cert_type"].ToString() == "HSM_SCTV")
                        {
                            xml_o = await CommonManager.SignXmlHSM(await getTenDv(), "#data", "seller", "NNT", "xml",
                                xmlBTHDL_O.ToString(), tblCertificate.Rows[0]["cer_serial"].ToString());
                        }
                        else
                        {
                            string pass = tblCertificate.Rows[0]["pass"].ToString();
                            byte[] bytes = (byte[])tblCertificate.Rows[0]["cert_data"];
                            X509Certificate2 certFile = new X509Certificate2(bytes, pass);
                            xml_o = CommonManager.SignXml(await getTenDv(), "#data", "seller", xmlBTHDL_O.ToString(), certFile, "NNT");
                        }
                    }
                    else
                    {
                        Dictionary<string, object> dicParam = new Dictionary<string, object>();
                        string userId = tblUser.Rows[0]["wb_user_id"].ToString();
                        dicParam.Add("wb_user_id", Guid.Parse(userId));

                        DataTable tblUser_Hsm = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_user_hsm_portal_78 WHERE wb_user_id=@wb_user_id", CommandType.Text, dicParam);

                        if (tblUser_Hsm.Rows.Count == 0)
                        {
                            _obj.Add("error", "Chưa có tài khoản cấu hình");
                            return _obj;
                        }
                        // lấy account hsm portal
                        string username_hsm = tblUser_Hsm.Rows[0]["username"].ToString();
                        string password_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password"].ToString()));
                        string password2_hsm = Encoding.UTF8.GetString(Convert.FromBase64String(tblUser_Hsm.Rows[0]["password_level2"].ToString()));
                        string ma_dvcs_hsm = tblUser_Hsm.Rows[0]["ma_dvcs"].ToString();

                        // call api login
                        HttpClient client = new HttpClient();

                        var api = CommonConstants.LOGIN;
                        JObject user = new JObject();
                        user.Add("username", username_hsm);
                        user.Add("password", password_hsm);
                        user.Add("ma_dvcs", ma_dvcs_hsm);

                        var token = "";
                        var resLogin = executeLogin(CommonConstants.BASE_HSM_URL, api, user).Result;

                        // kiểm tra nếu login lỗi thì thông báo
                        if (resLogin["error"] != null)
                        {
                            //_obj.Add("error", resLogin["error"].ToString());
                            //return _obj;
                            throw new Exception(resLogin["error"].ToString());
                        }
                        else
                        {
                            token = resLogin["token"].ToString();
                            token = token + ";" + ma_dvcs_hsm;
                        }

                        XmlDocument document = new XmlDocument();
                        document.PreserveWhitespace = false;
                        //xml_o = xmlBTHDL_O.ToString().Replace("'", "''");
                        xml_o = xmlBTHDL_O.ToString();
                        xml_o = xml_o.Replace("\r", "").Replace("\n", "");
                        document.LoadXml(xml_o);
                        XmlNode nodeTT = document.SelectSingleNode("/HDon/DLHDon/TTKhac");
                        if (nodeTT != null)
                        {
                            XmlElement TTin1 = document.CreateElement("TTin");
                            XmlElement TTruong1 = document.CreateElement("TTruong");
                            TTruong1.InnerText = "DVKy";
                            XmlElement KDLieu1 = document.CreateElement("KDLieu");
                            KDLieu1.InnerText = "string";
                            XmlElement DLieu1 = document.CreateElement("DLieu");
                            DLieu1.InnerText = tenCty;
                            TTin1.AppendChild(TTruong1);
                            TTin1.AppendChild(KDLieu1);
                            TTin1.AppendChild(DLieu1);


                            XmlElement TTin2 = document.CreateElement("TTin");
                            XmlElement TTruong2 = document.CreateElement("TTruong");
                            TTruong2.InnerText = "NKy";
                            XmlElement KDLieu2 = document.CreateElement("KDLieu");
                            KDLieu2.InnerText = "date";
                            XmlElement DLieu2 = document.CreateElement("DLieu");
                            DLieu2.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                            TTin2.AppendChild(TTruong2);
                            TTin2.AppendChild(KDLieu2);
                            TTin2.AppendChild(DLieu2);


                            nodeTT.AppendChild(TTin1);
                            nodeTT.AppendChild(TTin2);
                        }
                        xml_o = document.OuterXml;
                        // Call Sign HSM API
                        JObject result = new JObject();

                        api = CommonConstants.SIGN_HSM_URL_78;
                        JObject json = new JObject();
                        json.Add("password2", password2_hsm);
                        json.Add("idData", "#data");
                        json.Add("idSigner", "seller");
                        json.Add("tagSign", "NNT");
                        json.Add("dataType", "xml");
                        json.Add("xml", xml_o);

                        var resSign = executePost(CommonConstants.BASE_HSM_URL, api, token, json).Result;

                        if ("false".Equals(resSign["status"].ToString().ToLower()))
                        {
                            //_obj.Add("error", resSign["message"].ToString());
                            //return _obj;
                            throw new Exception(resSign["message"].ToString());
                        }
                        else
                        {
                            xml_o = resSign["data"].ToString();
                        }
                        //xml_o = xml_o.Replace("'", "''");
                    }
                    xml_DLieu.Add(XElement.Parse(xml_o, LoadOptions.PreserveWhitespace));
                }
                XDocument xmlBTHDL = new XDocument(new XElement("TDiep",
                        xml_TTChung, xml_DLieu
                        )
                    );
                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBTHDL.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
                return _obj;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<JObject> XmlBTHDLXangDau(string id)
        {
            JObject _res = new JObject();
            try
            {
                DataTable rsDataTable = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='" + id + "'");

                int sbthdlieu = 0;

                if (rsDataTable.Rows.Count > 0)
                {
                    string fromDate = ((DateTime)rsDataTable.Rows[0]["tu_ngay"]).ToString("yyyy-MM-dd");
                    string toDate = ((DateTime)rsDataTable.Rows[0]["den_ngay"]).ToString("yyyy-MM-dd");

                    sbthdlieu = await tangSoBTHDL(fromDate, toDate);
                }

                JObject ObjXml = await XmlBangTHDLXangDau(id, sbthdlieu);

                //string xml = ObjXml["xml"].ToString().Replace("'", "''");
                string xml = ObjXml["xml"].ToString();
                _res.Add("xml", xml);
                _res.Add("ok", true);
                _res.Add("shdon", sbthdlieu);

                await this._minvoiceDbContext.ExecuteNoneQueryAsync("Update #SCHEMA_NAME#.bangtonghopdl_68 set sbthdlieu=" + sbthdlieu + " WHERE sbthdlieu is null AND bangtonghopdl_68_id ='" + id + "'");

            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _res;
        }

        public async Task<JObject> XmlBangTHDLXangDau(string bangtonghopdl_68_id, int sbthdlieu)
        {
            JObject _obj = new JObject();
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id = '{bangtonghopdl_68_id}'";

                string thang = "";
                string ngay = "";
                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["thang"].ToString()))
                {
                    int thangDT = Convert.ToInt16(dauphieu.Rows[0]["thang"].ToString());
                    thang = thangDT.ToString();
                    if (thangDT < 10)
                    {
                        thang = "0" + thangDT;
                    }
                }

                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay"].ToString()))
                {
                    int ngayDT = Convert.ToInt16(dauphieu.Rows[0]["ngay"].ToString());
                    ngay = ngayDT.ToString();
                    if (ngayDT < 10)
                    {
                        ngay = "0" + ngayDT;
                    }
                }

                string sqlBangTongHop = "SELECT COALESCE(sbthdlieu, -1) sbthdlieu"
               + $" FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}'";
                DataTable bangtonghopDauPhieu = await this._minvoiceDbContext.GetDataTableAsync(sqlBangTongHop);

                if (bangtonghopDauPhieu.Rows.Count > 0)
                {
                    int valuesbthdlieu = Convert.ToInt32(bangtonghopDauPhieu.Rows[0]["sbthdlieu"].ToString());
                    if (valuesbthdlieu != -1)
                    {
                        sbthdlieu = valuesbthdlieu;
                    }
                }

                string sqlDetails = "SELECT *"
                                    + $" FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}' order by Cast (stt as numeric)";
                DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                DateTime dt = Convert.ToDateTime(dauphieu.Rows[0]["nlap"].ToString());
                XElement xmlBTHDL = new XElement("BTHDLieu",
                                new XElement("DLBTHop", new XAttribute("Id", "data"),
                                    new XElement("TTChung",
                                        new XElement("PBan", "2.0.0"),
                                        new XElement("MSo", "01/TH-HĐĐT"),
                                        new XElement("Ten", "Bảng tổng hợp dữ liệu hóa đơn điện tử"),
                                        new XElement("SBTHDLieu", sbthdlieu),
                                        GetDataRow("LKDLieu", dauphieu.Rows[0], "lkdlieu"),
                                        new XElement("KDLieu", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                        GetDataRow("LDau", dauphieu.Rows[0], "ldau"),
                                        GetDataRow("BSLThu", dauphieu.Rows[0], "bslthu"),
                                        // GetDataRow("SDLThu", dauphieu.Rows[0], "sdlthu"),
                                        new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                        GetDataRow("TNNT", dauphieu.Rows[0], "tnnt"),
                                        GetDataRow("MST", dauphieu.Rows[0], "mst"),
                                        GetDataRow("HDDIn", dauphieu.Rows[0], "hddin"),
                                        GetDataRow("LHHoa", dauphieu.Rows[0], "lhhoa")
                                    ),
                                    new XElement("NDBTHDLieu",
                                            new XElement("DSDLieu",
                                                from row in chitiet.AsEnumerable()
                                                select new XElement("DLieu",
                                                    new XElement("STT", row["stt"].ToString()),
                                                    row["khieu"] == null || row["khieu"].ToString() == "" ? null : new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                                    row["khieu"] == null || row["khieu"].ToString() == "" ? null : new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                                    GetDataRow("SHDon", row, "shdon"),
                                                    GetDataRowDate("NLap", row, "nlap"),
                                                    GetDataRow("TNMua", row, "tnmua"),
                                                    GetDataRow("MSTNMua", row, "mstnmua"),
                                                    GetDataRow("MKHang", row, "mkhang"),
                                                    GetDataRow("MHHDVu", row, "mhhoa"),
                                                    GetDataRow("THHDVu", row, "thhdvu"),
                                                    GetDataRow("DVTinh", row, "dvtinh"),
                                                    GetDataRowDoubble("SLuong", row, "sluong"),
                                                    GetDataRowDoubble("TTCThue", row, "ttcthue"),
                                                    GetDataRow("TSuat", row, "tsuat"),
                                                    GetDataRowDoubble("TgTThue", row, "tgtthue"),
                                                    GetDataRowDoubble("TgTTToan", row, "tgtttoan"),
                                                    GetDataRow("TThai", row, "tthai"),
                                                    GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                                    GetDataRow("SHDCLQuan", row, "shdlquan"),
                                                    GetDataRow("LKDLDChinh", dauphieu.Rows[0], "lkdlieu"),
                                                    new XElement("KDLDChinh", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                                    GetDataRow("STBao", row, "stbao"),
                                                    GetDataRowDate("NTBao", row, "ntbao"),
                                                    GetDataRow("GChu", row, "gchu")
                                                )
                                            )
                                        )
                                ),
                                new XElement("DSCKS",
                                    new XElement("NNT"),
                                    new XElement("CCKSKhac")
                                    )
                        );

                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBTHDL.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        public async Task<JObject> XmlBangTHDLXangDauIn(string bangtonghopdl_68_id, int sbthdlieu)
        {
            JObject _obj = new JObject();
            try
            {
                string sql = $"SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id = '{bangtonghopdl_68_id}'";

                string thang = "";
                string ngay = "";
                DataTable dauphieu = await this._minvoiceDbContext.GetDataTableAsync(sql);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["thang"].ToString()))
                {
                    int thangDT = Convert.ToInt16(dauphieu.Rows[0]["thang"].ToString());
                    thang = thangDT.ToString();
                    if (thangDT < 10)
                    {
                        thang = "0" + thangDT;
                    }
                }

                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay"].ToString()))
                {
                    int ngayDT = Convert.ToInt16(dauphieu.Rows[0]["ngay"].ToString());
                    ngay = ngayDT.ToString();
                    if (ngayDT < 10)
                    {
                        ngay = "0" + ngayDT;
                    }
                }

                string sqlBangTongHop = "SELECT COALESCE(sbthdlieu, -1) sbthdlieu"
               + $" FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}'";
                DataTable bangtonghopDauPhieu = await this._minvoiceDbContext.GetDataTableAsync(sqlBangTongHop);

                if (bangtonghopDauPhieu.Rows.Count > 0)
                {
                    int valuesbthdlieu = Convert.ToInt32(bangtonghopDauPhieu.Rows[0]["sbthdlieu"].ToString());
                    if (valuesbthdlieu != -1)
                    {
                        sbthdlieu = valuesbthdlieu;
                    }
                }

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("is_ky", 0);
                if (!string.IsNullOrEmpty(dauphieu.Rows[0]["ngay_ky"].ToString()))
                {
                    parameters["is_ky"] = 1;
                }
                else
                {
                    parameters["is_ky"] = 0;
                }


                string sqlDetails = "SELECT *"
                                    + $" FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd WHERE bangtonghopdl_68_id ='{bangtonghopdl_68_id}' order by Cast (stt as numeric)";
                DataTable chitiet = await this._minvoiceDbContext.GetDataTableAsync(sqlDetails);

                XNamespace name = "http://kekhaithue.gdt.gov.vn/TKhaiThue";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                DateTime dt = Convert.ToDateTime(dauphieu.Rows[0]["nlap"].ToString());
                XElement xmlBTHDL = new XElement("BTHDLieu",
                                new XElement("DLBTHop", new XAttribute("Id", "data"),
                                    new XElement("TTChung",
                                        new XElement("PBan", "2.0.0"),
                                        new XElement("MSo", "01/TH-HĐĐT"),
                                        new XElement("Ten", "Bảng tổng hợp dữ liệu hóa đơn điện tử"),
                                        new XElement("SBTHDLieu", sbthdlieu),
                                        GetDataRow("LKDLieu", dauphieu.Rows[0], "lkdlieu"),
                                        new XElement("KDLieu", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                        GetDataRow("LDau", dauphieu.Rows[0], "ldau"),
                                        GetDataRow("BSLThu", dauphieu.Rows[0], "bslthu"),
                                        GetDataRow("SDLThu", dauphieu.Rows[0], "sdlthu"),
                                        new XElement("NLap", dt.ToString("yyyy-MM-dd")),
                                        GetDataRow("TNNT", dauphieu.Rows[0], "tnnt"),
                                        GetDataRow("MST", dauphieu.Rows[0], "mst"),
                                        GetDataRow("HDDIn", dauphieu.Rows[0], "hddin"),
                                        GetDataRow("LHHoa", dauphieu.Rows[0], "lhhoa")
                                    ),
                                    new XElement("NDBTHDLieu",
                                            new XElement("DSDLieu",
                                                from row in chitiet.AsEnumerable()
                                                select new XElement("DLieu",
                                                    new XElement("STT", row["stt"].ToString()),
                                                    row["khieu"] == null || row["khieu"].ToString() == "" ? null : new XElement("KHMSHDon", row["khieu"].ToString().Substring(0, 1)),
                                                    row["khieu"] == null || row["khieu"].ToString() == "" ? null : new XElement("KHHDon", row["khieu"].ToString().Substring(1)),
                                                    GetDataRow("SHDon", row, "shdon"),
                                                    GetDataRowDate("NLap", row, "nlap"),
                                                    GetDataRow("TNMua", row, "tnmua"),
                                                    GetDataRow("MSTNMua", row, "mstnmua"),
                                                    GetDataRow("MKHang", row, "mkhang"),
                                                    GetDataRow("MHHDVu", row, "mhhoa"),
                                                    GetDataRow("THHDVu", row, "thhdvu"),
                                                    GetDataRow("DVTinh", row, "dvtinh"),
                                                    GetDataRowDoubble("SLuong", row, "sluong"),
                                                    GetDataRowDoubble("TTCThue", row, "ttcthue"),
                                                    GetDataRow("TSuat", row, "tsuat"),
                                                    GetDataRowDoubble("TgTThue", row, "tgtthue"),
                                                    GetDataRowDoubble("TgTTToan", row, "tgtttoan"),
                                                    GetDataRow("TThai", row, "tthai"),
                                                    GetDataRow("LHDCLQuan", row, "lhdlquan"),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHMSHDCLQuan", row["khhdlquan"].ToString().Substring(0, 1)),
                                                    row["khhdlquan"] == null || row["khhdlquan"].ToString() == "" ? null : new XElement("KHHDCLQuan", row["khhdlquan"].ToString().Substring(1)),
                                                    GetDataRow("SHDCLQuan", row, "shdlquan"),
                                                    GetDataRow("LKDLDChinh", dauphieu.Rows[0], "lkdlieu"),
                                                    new XElement("KDLDChinh", (dauphieu.Rows[0]["lkdlieu"].ToString() == "N" ? ngay + "/" : "") + thang + "/" + dauphieu.Rows[0]["nam"].ToString()),
                                                    GetDataRow("STBao", row, "stbao"),
                                                    GetDataRowDate("NTBao", row, "ntbao"),
                                                    GetDataRow("GChu", row, "gchu")
                                                )
                                            )
                                        )
                                ),
                                new XElement("DSCKS",
                                    new XElement("NNT"),
                                    new XElement("CCKSKhac"),
                                    new XElement("is_ky", parameters["is_ky"])
                                    )
                        );

                string xml = string.Empty;
                var writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = true;

                using (var buffer = new StringWriter())
                using (var writer = XmlWriter.Create(buffer, writerSettings))
                {
                    xmlBTHDL.Save(writer);
                    writer.Flush();
                    xml = buffer.ToString();
                }

                _obj.Add("xml", xml);
            }
            catch (Exception e)
            {
                _obj.Add("error", e.Message);
                Log.Error(e.Message);
            }
            return _obj;
        }

        public async Task<JObject> SaveXmlBTHDLXangDau(JArray model_list)
        {
            JObject _res = new JObject();
            try
            {
                string xml;
                string mst = await getMstDv();
                foreach (JObject model in model_list)
                {
                    await _minvoiceDbContext.BeginTransactionAsync();
                    xml = model["xml"].ToString();
                    Guid bangtonghopdl_id = Guid.Parse(model["id"].ToString());
                    Int32 shdon = Convert.ToInt32(model["shdon"].ToString());
                    //await _minvoiceDbContext.BeginTransactionAsync();

                    string mtdiep_gui = CommonManager.GetMaThongDiep(mst);
                    XElement xml_TTChung = await XmlTTChungTDiep(mtdiep_gui, CommonConstants.MLTDIEP_BTHDL, 1);

                    XElement dLieu = new XElement("DLieu");
                    dLieu.Add(XElement.Parse(xml, LoadOptions.PreserveWhitespace));
                    XDocument xmlThongDiep = new XDocument(new XElement("TDiep", xml_TTChung, dLieu));

                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;

                    using (var buffer = new StringWriter())
                    using (var writer = XmlWriter.Create(buffer, writerSettings))
                    {
                        xmlThongDiep.Save(writer);
                        writer.Flush();
                        xml = buffer.ToString();
                    }

                    string sqlUpdate = "UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET trang_thai=@trang_thai, xml_send = @xml_send, ngay_ky = @ngay_ky, ngky = @ngky WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    parameters2.Add("trang_thai", 1);
                    parameters2.Add("xml_send", xml);
                    parameters2.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    parameters2.Add("ngay_ky", DateTime.Now);
                    parameters2.Add("ngky", _webHelper.GetUser());
                    //parameters2.Add("sbthdlieu", shdon);

                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);




                    if (CommonManager.CheckThoiGian(xml) == false)
                    {
                        //res.Add("error", "Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                        //return res;
                        throw new Exception("Chứng thư số hết hiệu lực. Vui lòng kiểm tra lại chứng thư số");
                    }

                    // lấy thông tin xml TTChung của Thông điệp
                    // lưu vào bảng tonghop_gui_tvan_68
                    string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68 (id, mltdiep_gui, xml_tdiep_gui, type_id, mtdiep_gui, tgian_gui, nguoi_gui, note, mdvi) VALUES "
                                  + " (@id ,@mltdiep_gui, @xml_tdiep_gui::xml, @type_id, @mtdiep_gui, current_timestamp, @nguoi_gui, @note, @mdvi)";
                    Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                    param_Insert.Add("id", Guid.NewGuid());
                    param_Insert.Add("mltdiep_gui", CommonConstants.MLTDIEP_BTHDL);
                    param_Insert.Add("xml_tdiep_gui", xml);
                    param_Insert.Add("type_id", bangtonghopdl_id);
                    param_Insert.Add("mtdiep_gui", mtdiep_gui);
                    //param_Insert.Add("tgian_gui", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
                    param_Insert.Add("nguoi_gui", _webHelper.GetUser());
                    param_Insert.Add("note", "app");
                    param_Insert.Add("mdvi", _webHelper.GetDvcs());

                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);
                    param_Insert.Clear();
                    param_Insert.Add("bangtonghopdl_68_id", bangtonghopdl_id);
                    param_Insert.Add("mtdiep_gui", mtdiep_gui);

                    string sqlUpdate1 = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET mtdiep_gui=@mtdiep_gui WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate1, CommandType.Text, param_Insert);

                    //await _minvoiceDbContext.TransactionCommitAsync();
                    //_minvoiceDbContext.CloseTransaction();

                    string data = await ThongDiepGui(CommonConstants.MLTDIEP_BTHDL, xml);

                    // đẩy queue lên TVAN
                    _rabbitMQService.PublishTaskQueue(_queue_vendor_request_van, data);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                    //Đổi trạng thái của các hoá đơn trong BTHDL đã ký và gửi lên cơ quan thuế
                    /*string sqlUpdate3 = "UPDATE #SCHEMA_NAME#.hoadon68 a SET tthai = 'Đã gửi', kygui_cqt =1 "
                                        + "WHERE tthai = 'Đã ký' AND POSITION( '' || a.hdon_id || '' in ("
                                        + "SELECT b.hdon_id FROM #SCHEMA_NAME#.bangtonghopdl_68_chitiet_xd b "
                                        + $"WHERE bangtonghopdl_68_id = '{bangtonghopdl_id}' "
                                        + "GROUP BY b.hdon_id)) > 0";
                    await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdate3);*/
                }

            }
            catch (Exception e)
            {
                _res.Add("error", e.Message);
                Log.Error(e.Message);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch { }
            }
            return _res;
        }

        public async Task<byte[]> inBTHDLieuXangDau(string id, string folder, string type)
        {
            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                DataTable tblHoadon = await this._minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.bangtonghopdl_68 WHERE bangtonghopdl_68_id='" + id + "'");

                //if (!string.IsNullOrEmpty(tblHoadon.Rows[0]["xml_send"].ToString()))
                //{
                //    xml = tblHoadon.Rows[0]["xml_send"].ToString();
                //}
                //else
                //{
                int so = string.IsNullOrEmpty(tblHoadon.Rows[0]["sbthdlieu"].ToString()) ? 0 : Convert.ToInt32(tblHoadon.Rows[0]["sbthdlieu"].ToString());
                JObject x = await XmlBangTHDLXangDauIn(tblHoadon.Rows[0]["bangtonghopdl_68_id"].ToString(), so);
                xml = x["xml"].ToString();
                //}

                //XtraReport report = XtraReport.FromFile(folder, true);

                //report.Name = "XtraReport1";
                //report.ScriptReferencesString = "AccountSignature.dll";

                DataSet ds = new DataSet();
                StringReader theReader = new StringReader(xml);

                ds.ReadXml(theReader);

                bytes = ReportUtil.PrintReport(ds, folder, type, "Dlieu");

                if (bytes == null)
                {
                    throw new Exception("null");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bytes;
        }
        public async Task<JObject> DuyetHoaDon(JObject obj)
        {
            JObject res = new JObject();
            string error = "";

            try
            {
                JArray arrData = (JArray)obj["data"];

                string status_old = obj["status"].ToString();

                JObject model = (JObject)arrData[0];

                //get arr hdon_id từ obj để for
                JArray arrhdon_id = (JArray)model["lsthdon_id"];

                string dvcs = _webHelper.GetDvcs();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("branch_code", dvcs);

                DataTable tblInvoiceProcess = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.inv_invoice_process WHERE branch_code=@branch_code ORDER BY ord", CommandType.Text, parameters);
                int m = 0;

                for (int j = 0; j < tblInvoiceProcess.Rows.Count; j++)
                {
                    DataRow row = tblInvoiceProcess.Rows[j];
                    string name = row["name"].ToString();

                    if (name == status_old)
                    {
                        m = j;
                        break;
                    }
                }

                string status = "";

                if (tblInvoiceProcess.Rows.Count == 0)
                {
                    status = "Chờ ký";
                }
                else if (tblInvoiceProcess.Rows.Count == 1)
                {
                    status = tblInvoiceProcess.Rows[0]["name"].ToString();
                }
                else
                {
                    status = tblInvoiceProcess.Rows[m + 1]["name"].ToString();
                }

                for (int i = 0; i < arrhdon_id.Count; i++)
                {

                    string hdon_id = arrhdon_id[i].ToString();

                    parameters.Clear();
                    parameters.Add("hdon_id", Guid.Parse(hdon_id));

                    DataTable tblInvoiceAuth = await _minvoiceDbContext.GetDataTableAsync("SELECT to_char(nlap,'YYYY-MM-DD') as nlap,shdon FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id=@hdon_id::uuid", CommandType.Text, parameters);
                    string invoice_issued_date = tblInvoiceAuth.Rows[0]["nlap"].ToString();

                    parameters.Clear();
                    parameters.Add("hdon_id", Guid.Parse(hdon_id));
                    parameters.Add("nlap", DateTime.Parse((invoice_issued_date)));
                    parameters.Add("mdvi", dvcs);

                    string sql = $"SELECT hdon_id FROM #SCHEMA_NAME#.hoadon68 WHERE tthai='{status_old}' and mdvi=@mdvi  AND nlap < @nlap and hdon_id <> @hdon_id::uuid LIMIT 1";
                    DataTable tblInvoiceAuthBefore = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                    if (tblInvoiceAuthBefore.Rows.Count > 0)
                    {
                        error = "Bạn chưa xử lý hóa đơn của ngày hôm trước";
                        break;
                    }

                    await _minvoiceDbContext.BeginTransactionAsync();

                    parameters.Clear();

                    parameters.Add("status", status);
                    parameters.Add("hdon_id", Guid.Parse(hdon_id));

                    sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthai=@status  WHERE hdon_id=@hdon_id";
                    await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, parameters);

                    if (status_old == "Chờ sinh số")
                    {
                        sql = "select value from #SCHEMA_NAME#.wb_setting where code = 'SIGN_CREATE_NUMBER'";
                        DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text);
                        if (tblSetting.Rows[0][0].ToString() == "C")
                        {
                            #region lấy số hóa đơn khi không có
                            if (string.IsNullOrEmpty(tblInvoiceAuth.Rows[0]["shdon"].ToString()))
                            {
                                sql = "#SCHEMA_NAME#.crd_hdon68_update_shdon";
                                JObject masterObj = new JObject();
                                masterObj.Add("branch_code", _webHelper.GetDvcs());
                                masterObj.Add("hdon_id", hdon_id);

                                await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.StoredProcedure, masterObj);
                            }
                            #endregion
                        }
                    }

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();

                }

                res.Add("ok", true);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            if (!string.IsNullOrEmpty(error))
            {
                res.Add("error", error);
                Log.Error(error);

                try
                {
                    await _minvoiceDbContext.TransactionRollbackAsync();
                    _minvoiceDbContext.CloseTransaction();
                }
                catch (Exception ex) { }
            }

            return res;
        }

        public async Task<byte[]> InDanhSachHoaDon(JObject model, string folder, string type, bool inchuyendoi, bool checkBangKe)
        {
            JArray array = (JArray)model["invs"];
            byte[] bytes = null;
            //XtraReport totalReport = new XtraReport();
            String folderPrint = CommonConstants.FOLDER_PRINTLIST + "/" + CommonManager.GetTimestamp(DateTime.Now);
            CommonManager.createFolder(folderPrint);
            try
            {
                for (int inv = 0; inv < array.Count; inv++)
                {
                    string xml = "";
                    string msg_tb = "";
                    Guid hdon_id = Guid.Parse(array[inv].ToString());

                    DataTable tblHoadon =
                        await this._minvoiceDbContext.GetDataTableAsync(
                            "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id='" + hdon_id + "'");
                    DataTable tblInvoiceXmlData =
                        await this._minvoiceDbContext.GetDataTableAsync(
                            "SELECT * FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id='" + hdon_id + "'");

                    if (tblInvoiceXmlData.Rows.Count > 0)
                    {
                        if (checkBangKe == true)
                        {
                            if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_bangke"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_bangke"].ToString();
                            }
                            else
                            {
                                JObject jsXml = await XmlInvoice_BangKe(hdon_id.ToString());
                                xml = jsXml["xml"].ToString();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString();
                                //XDocument doc = XDocument.Parse(xml);

                                //xml = doc.Descendants("HDon").First().ToString();

                                XmlDocument document = new XmlDocument();
                                document.LoadXml(xml);
                                XmlNode nodeHDon = document.SelectSingleNode("/TDiep/DLieu/HDon");
                                xml = nodeHDon.OuterXml;
                            }
                            else if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString();
                            }
                            else
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml"].ToString();
                            }
                        }
                    }
                    else
                    {
                        if (checkBangKe == true)
                        {
                            JObject jsXml = await XmlInvoice_BangKe(hdon_id.ToString());
                            xml = jsXml["xml"].ToString();
                        }
                        else
                        {
                            JObject jsXml = await XmlInvoice(hdon_id.ToString());
                            xml = jsXml["xml"].ToString();
                        }
                    }

                    string sqlDetails =
                        $"SELECT  * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' order by Cast (stt as numeric)";
                    DataTable detail = await _minvoiceDbContext.GetDataTableAsync(sqlDetails);

                    string[] notGet = new string[]
                    {
                        "kmai", "stt", "ma", "ten", "mdvtinh", "sluong", "dgia", "tlckhau", "stckhau", "thtien",
                        "tsuat", "tsuatstr", "hdon_id", "cthdon_id"
                    };

                    string cctbao_id = tblHoadon.Rows[0]["cctbao_id"].ToString();
                    string sbmat = tblHoadon.Rows[0]["sbmat"].ToString();

                    string SqlDongMau = $"Select sdmau from #SCHEMA_NAME#.quanlykyhieu68 where qlkhsdung_id = '{cctbao_id}'";
                    DataTable dongMau = await _minvoiceDbContext.GetDataTableAsync(SqlDongMau);
                    int soDongMau = Convert.ToInt32(dongMau.Rows[0][0].ToString());

                    xml = FunctionUtil.AddXml(xml, detail, notGet, "HHDVu", cctbao_id, soDongMau);

                    //string inv_originalId = tblInv_InvoiceAuth.Rows[0]["original_invoice_id"].ToString();

                    //DataTable tblCtthongbao = await _minvoiceDbContext.GetDataTableAsync("SELECT a.*,b.thousand_point as hang_nghin,b.decimal_point as thap_phan FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id WHERE a.inv_notificationdetail_id='" + inv_InvoiceCode_id + "'");

                    //string hang_nghin = tblCtthongbao.Rows[0]["hang_nghin"].ToString();
                    //string thap_phan = tblCtthongbao.Rows[0]["thap_phan"].ToString();

                    //string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", tblCtthongbao.Rows[0]["inv_notificationdetail_id"]);

                    //XtraReport report = _cacheManager.Get<XtraReport>(cacheReportKey);
                    XtraReport report = new XtraReport();
                    //if (report == null)
                    //{
                    DataTable tblKyhieu = await _minvoiceDbContext.GetDataTableAsync(
                        "SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + cctbao_id + "'");
                    DataTable tblDmmauhd = await _minvoiceDbContext.GetDataTableAsync(
                        "SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + tblKyhieu.Rows[0]["qlmtke_id"] +
                        "'");
                    string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

                    if (invReport.Length > 0)
                    {
                        report = ReportUtil.LoadReportFromString(invReport);
                        // _cacheManager.Set(cacheReportKey, report, 30);
                    }
                    else
                    {
                        throw new Exception("Không tải được mẫu hóa đơn");
                    }

                    //   }


                    report.Name = "XtraReport1";
                    report.ScriptReferencesString = "AccountSignature.dll";

                    DataSet ds = new DataSet();

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                    {
                        ds.ReadXmlSchema(xmlReader);
                        xmlReader.Close();
                    }

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                    {
                        ds.ReadXml(xmlReader);
                        xmlReader.Close();
                    }

                    if (ds.Tables.Contains("TblXmlData"))
                    {
                        ds.Tables.Remove("TblXmlData");
                    }


                    DataTable tblXmlData = new DataTable();
                    tblXmlData.TableName = "TblXmlData";
                    tblXmlData.Columns.Add("data");

                    DataRow r = tblXmlData.NewRow();
                    r["data"] = xml;
                    tblXmlData.Rows.Add(r);
                    ds.Tables.Add(tblXmlData);

                    string datamember = report.DataMember;

                    if (datamember.Length > 0)
                    {
                        if (ds.Tables.Contains(datamember))
                        {
                            DataTable tblChiTiet = ds.Tables[datamember];
                            int rowcount = ds.Tables[datamember].Rows.Count;
                        }
                    }

                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "3")
                    {
                        Guid hoadonIdLK =
                            tblHoadon.Rows[0]["hdon68_id_lk"] != null &&
                            !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                     + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql);

                        if (tblInv.Rows.Count > 0)
                        {
                            //    string msg = tblInv.Rows[0]["msg"].ToString();

                            //    if (msg.Length > 0)
                            //    {
                            //        DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=" + trang_thai_hd);
                            //        string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();

                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                     " năm " + nam;

                        }
                        //}
                    }

                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "2")
                    {
                        Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                            ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                            : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                     + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql);

                        if (tblInv.Rows.Count > 0)
                        {

                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                     " năm " + nam;
                        }
                    }

                    if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "19" ||
                        tblHoadon.Rows[0]["tthdon_original"].ToString() == "21" ||
                        tblHoadon.Rows[0]["tthdon_original"].ToString() == "23")
                    {
                        Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                            ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                            : hdon_id;
                        string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                     + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                        DataTable tblInv = await _minvoiceDbContext.GetDataTableAsync(sql);

                        if (tblInv.Rows.Count > 0)
                        {
                            DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                            string ngay = tdlapDate.Day.ToString();
                            string thang = tdlapDate.Month.ToString();
                            string nam = tdlapDate.Year.ToString();

                            msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " +
                                     tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                     + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                     tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                     " năm " + nam;
                        }
                    }


                    //if (Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]) == 7)
                    //{
                    //    msg_tb = "";
                    //}
                    if (tblHoadon.Rows[0]["dvtte"] != null && tblHoadon.Rows[0]["dvtte"].ToString() == "USD" &&
                        !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgia"].ToString()))
                    {
                        decimal tienthanhtoan = 0;
                        if (tblHoadon.Rows[0]["tgtttbso_last"] != null &&
                            !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgtttbso_last"].ToString()))
                        {
                            tienthanhtoan = Convert.ToDecimal(tblHoadon.Rows[0]["tgtttbso_last"].ToString());
                        }

                        decimal tigia = Convert.ToDecimal(tblHoadon.Rows[0]["tgia"].ToString());
                        decimal tienquydoi = tienthanhtoan * tigia;
                        if (report.Parameters["QUYDOI"] != null)
                        {
                            report.Parameters["QUYDOI"].Value = tienquydoi.ToString();
                        }
                    }

                    if (report.Parameters["MSG_TB"] != null)
                    {
                        report.Parameters["MSG_TB"].Value = msg_tb;
                    }

                    if (report.Parameters["MCCQT"] != null)
                    {
                        report.Parameters["MCCQT"].Value = tblHoadon.Rows[0]["mccqthue"].ToString();
                    }

                    // số bảo mật
                    if (report.Parameters["SoBaoMat"] != null)
                    {
                        report.Parameters["SoBaoMat"].Value = sbmat;
                    }

                    //check in bảng kê
                    if (checkBangKe == true)
                    {
                        if (report.Parameters["CheckBangKe"] != null)
                        {
                            report.Parameters["CheckBangKe"].Value = 1;
                        }
                    }
                    else
                    {
                        if (report.Parameters["CheckBangKe"] != null)
                        {
                            report.Parameters["CheckBangKe"].Value = 0;
                        }

                    }

                    try
                    {

                        if (report.Parameters["SoThapPhan"] != null)
                        {
                            string sql = "SELECT * FROM #SCHEMA_NAME#.sl_currency a "
                                         + "WHERE a.code='" + tblHoadon.Rows[0]["dvtte"].ToString() + "' limit 1";

                            DataTable currency = await _minvoiceDbContext.GetDataTableAsync(sql);
                            if (currency.Rows.Count > 0)
                            {
                                report.Parameters["SoThapPhan"].Value = currency.Rows[0]["hoadon123"].ToString();
                            }
                        }
                    }
                    catch
                    {

                    }
                    //var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

                    //if (lblHoaDonMau != null)
                    //{
                    //    lblHoaDonMau.Visible = false;
                    //}

                    if (inchuyendoi)
                    {
                        var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi")
                            .FirstOrDefault<XRTable>();

                        if (tblInChuyenDoi != null)
                        {
                            tblInChuyenDoi.Visible = true;
                        }

                        if (report.Parameters["MSG_HD_TITLE"] != null)
                        {
                            report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                        }

                        if (report.Parameters["NGUOI_IN_CDOI"] != null)
                        {
                            report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                            report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                        }

                        if (report.Parameters["NGAY_IN_CDOI"] != null)
                        {
                            report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                            report.Parameters["NGAY_IN_CDOI"].Visible = true;
                        }
                    }

                    report.DataSource = ds;

                    try
                    {
                        bool giamthuebanhang20 = tblHoadon.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblHoadon.Rows[0]["giamthuebanhang20"].ToString());
                        string dvtte = tblHoadon.Rows[0]["dvtte"]?.ToString();

                        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                        string tongTienGiam = double.Parse(DoubleStrToString(tblHoadon.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                        tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");
                        string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";

                        if (giamthuebanhang20)
                        {
                            var dt = ds.Tables["TToan"];
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Columns.Contains("TgTTTBChu"))
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        var docTien = (dt.Rows[i]["TgTTTBChu"]?.ToString()).Trim();

                                        ds.Tables["TToan"].Rows[i]["TgTTTBChu"] = docTien + ". " + Environment.NewLine + thueGTGTBanHang;
                                        report.DataSource = ds;

                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //Log.Error(ex);
                    }

                    #region Them Hien Thi Gia khung hinh
                    try
                    {
                        //if (tblHoadon.Rows[0]["error_status"].ToString() == "1")
                        //{
                        //    int pageCount = report.Pages.Count;
                        //    Watermark pmk = report.Watermark;
                        //    pmk.Text = tblHoadon.Rows[0]["is_hdcma"].ToString() == "1" ? "CQT không cấp mã" : (tblHoadon.Rows[0]["is_hdcma"].ToString() == "0" ? "CQT không tiếp nhận HĐ" : "");
                        //    pmk.TextDirection = DirectionMode.ForwardDiagonal;
                        //    pmk.Font = new Font("Times New Roman", 50);
                        //    pmk.TextTransparency = 150;
                        //    pmk.PageRange = "1-" + pageCount;
                        //}
                        //else
                        //{
                        if (tblHoadon.Rows[0]["tthdon"].ToString() == "3" || tblHoadon.Rows[0]["tthdon"].ToString() == "17")
                        {
                            // Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                            // report.Watermark.Image = bmp;
                            Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                            int pageCount = report.Pages.Count;

                            Watermark pmk = report.Watermark;
                            pmk.Image = bmp;
                            pmk.PageRange = "1-" + pageCount;

                        }
                        //}
                    }
                    catch (Exception ex)
                    {
                    }
                    #endregion

                    report.CreateDocument();

                    MemoryStream ms = new MemoryStream();

                    if (type == "Image")
                    {
                        var options = new ImageExportOptions(ImageFormat.Png)
                        {
                            ExportMode = ImageExportMode.SingleFilePageByPage,
                        };
                        report.ExportToImage(ms, options);
                    }
                    else if (type == "Excel")
                    {
                        report.ExportToXlsx(ms);
                    }
                    else if (type == "Rtf")
                    {
                        report.ExportToRtf(ms);
                    }
                    else
                    {
                        report.ExportToPdf(ms);
                    }

                    CommonManager.createFileRandomName(folderPrint, ms.ToArray());

                    ms.Close();

                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            try
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(folderPrint);

                //Get the PDF files in folder path into FileInfo
                FileInfo[] files = directoryInfo.GetFiles("*.pdf");

                //Create a new PDF document 
                PdfDocument document = new PdfDocument();

                //Set enable memory optimization as true 
                document.EnableMemoryOptimization = true;

                foreach (FileInfo file in files)
                {
                    //Load the PDF document 
                    FileStream fileStream = new FileStream(file.FullName, FileMode.Open);
                    Syncfusion.Pdf.Parsing.PdfLoadedDocument loadedDocument = new Syncfusion.Pdf.Parsing.PdfLoadedDocument(fileStream);

                    //Merge PDF file
                    PdfDocumentBase.Merge(document, loadedDocument);

                    //Close the existing PDF document 
                    loadedDocument.Close(true);
                }


                String fileNameFinal = CommonManager.GetTimestamp(DateTime.Now);

                //Creates a new Memory stream
                //MemoryStream stream = new MemoryStream();

                //Save the PDF document
                document.Save(folderPrint + "/" + fileNameFinal + ".pdf");

                //Close the instance of PdfDocument
                document.Close(true);

                //byteOut = stream.ToArray();

                bytes = System.IO.File.ReadAllBytes(folderPrint + "/" + fileNameFinal + ".pdf");

                //stream.Close();

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return bytes;

        }

        public async Task<byte[]> TaiDanhSachHoaDon(JObject model, string folder, string type, bool inchuyendoi, bool checkBangKe)
        {
            JArray array = (JArray)model["invs"];
            byte[] bytes = null;
            string folderZip = CommonManager.GetTimestamp(DateTime.Now);
            //XtraReport totalReport = new XtraReport();
            String folderPrint = CommonConstants.FOLDER_PRINTLIST + "/" + folderZip;
            CommonManager.createFolder(folderPrint);
            try
            {
                for (int inv = 0; inv < array.Count; inv++)
                {
                    MemoryStream msPdf = null;
                    MemoryStream msXml = null;

                    Guid hdon_id = Guid.Parse(array[inv].ToString());
                    string inv_InvoiceAuth_id = array[inv].ToString();

                    string fileName = await GetInvoiceFileName(inv_InvoiceAuth_id);

                    string pathHoaDon = folderPrint + "/" + fileName;

                    CommonManager.createFolder(pathHoaDon);

                    byte[] dataPdf = await inHoadon(inv_InvoiceAuth_id, folder, "PDF", false);
                    msPdf = new MemoryStream(dataPdf);

                    //get xml hóa đơn
                    string xml = await ExportXMLHoadon(inv_InvoiceAuth_id);
                    byte[] dataXml = System.Text.UTF8Encoding.UTF8.GetBytes(xml);
                    msXml = new MemoryStream(dataXml);

                    CommonManager.createFileRandomName(pathHoaDon, msPdf.ToArray(), fileName, "pdf");
                    CommonManager.createFileRandomName(pathHoaDon, msXml.ToArray(), fileName, "xml");

                    //get xml bảng kê hoá đơn 
                    MemoryStream msXml_bangke = null;
                    byte[] dataXml_bangke = null;
                    string xml_bangke = await ExportXMLBangKe_HD(inv_InvoiceAuth_id);

                    if (!string.IsNullOrEmpty(xml_bangke))
                    {
                        dataXml_bangke = System.Text.UTF8Encoding.UTF8.GetBytes(xml_bangke);
                        msXml_bangke = new MemoryStream(dataXml_bangke);
                        CommonManager.createFileRandomName(pathHoaDon, msXml_bangke.ToArray(), fileName + "_bangke", "xml");
                        msXml_bangke.Close();
                    }

                    msPdf.Close();
                    msXml.Close();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            try
            {
                System.IO.Compression.ZipFile.CreateFromDirectory(folderPrint, CommonConstants.FOLDER_PRINTLIST + "/" + folderZip + ".zip", CompressionLevel.Optimal, true);
                bytes = System.IO.File.ReadAllBytes(CommonConstants.FOLDER_PRINTLIST + "/" + folderZip + ".zip");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return bytes;

        }


        public async Task<JObject> uploadCanceledInv(string id)
        {
            JObject _res = new JObject();

            try
            {
                Dictionary<string, object> paras = new Dictionary<string, object>();
                paras.Add("hdon_id", Guid.Parse(id));

                string sqlHoaDon = $"SELECT tthai, tthdon FROM #SCHEMA_NAME#.hoadon68 WHERE hdon_id =@hdon_id";

                DataTable tblHoaDon = await _minvoiceDbContext.GetDataTableAsync(sqlHoaDon, CommandType.Text, paras);

                if (tblHoaDon.Rows.Count > 0)
                {
                    string tthai = tblHoaDon.Rows[0]["tthai"]?.ToString();
                    string tthdon = tblHoaDon.Rows[0]["tthdon"]?.ToString();

                    if (tthdon == "3")
                    {
                        _res.Add("ok", true);
                        return _res;
                    }

                    if ((tthai != null && (tthai == "Đã ký" || tthai == "CQT đã nhận" || tthai == "CQT không tiếp nhận HĐ" || tthai == "Chờ phản hồi") && (tthdon == "0" || tthdon == "1"))
                    //|| (tthai != null && (tthai == "CQT không tiếp nhận HĐ") && (tthdon == "11"))
                    || (tthai != null && (tthai == "CQT không tiếp nhận HĐ") && (tthdon == "19" || tthdon == "21" || tthdon == "23"))
                    //|| (tthai != null && (tthai == "CQT không tiếp nhận HĐ") && (tthdon == "17"))
                    || (tthai != null && (tthai == "CQT không tiếp nhận HĐ") && (tthdon == "2")))
                    //|| (tthai != null && (tthai == "CQT không tiếp nhận HĐ") && (tthdon == "3")))
                    {
                        string sql = "UPDATE #SCHEMA_NAME#.hoadon68 SET tthdon_old = tthdon, tthdon = 3 WHERE hdon_id =@hdon_id ";
                        await _minvoiceDbContext.BeginTransactionAsync();
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, paras);
                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();

                        _res.Add("ok", true);
                    }
                    else
                    {
                        throw new Exception("Không thể thực hiện chức năng này");
                    }

                }
                else
                {
                    throw new Exception("Không tồn tại hóa đơn");
                }


            }
            catch (Exception ex)
            {
                _res.Add("error", ex.Message);
                Log.Error(ex.Message);

                await _minvoiceDbContext.TransactionRollbackAsync();
                _minvoiceDbContext.CloseTransaction();

            }

            return _res;

        }

        public async Task<JObject> GetHoadonChoXuLy_LayToanBoHoaDon(JObject model)
        {
            JObject json = new JObject();
            try
            {
                string tu_ngay = model["tu_ngay"].ToString();
                string den_ngay = model["den_ngay"].ToString();
                string loaihh = model["lhhoa"].ToString();

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("ma_dvcs", _webHelper.GetDvcs());
                parameters.Add("tu_ngay", DateTime.Parse(tu_ngay));
                parameters.Add("den_ngay", DateTime.Parse(den_ngay));

                string sql = "";
                if (loaihh == "1")
                {
                    sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bthdl_68_final_result_hatm_xangdau('#SCHEMA_NAME#',@ma_dvcs,@tu_ngay,@den_ngay)";
                }
                else
                {
                    sql = "SELECT * FROM #SCHEMA_NAME#.rp_bc_bthdl_68_final_result_hatm('#SCHEMA_NAME#',@ma_dvcs,@tu_ngay,@den_ngay)";
                }
                //var invoiceDb = this._nopDbContext.GetInvoiceDb();
                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                JArray jarr = JArray.FromObject(dt);
                json.Add("data", jarr);

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
                Log.Error(ex.Message);
            }
            return json;


        }

        private async Task<bool> CheckSoHoaDon(string xml, Guid hdon_id)
        {
            bool result = true;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                string shdon = xmlDoc.GetElementsByTagName("SHDon")[0].InnerText;

                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("shdon", shdon);
                param1.Add("hdon_id", hdon_id);

                string sql = "select shdon from #SCHEMA_NAME#.hoadon68 where shdon = @shdon and hdon_id = @hdon_id";

                DataTable tblHoaDon = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, param1);

                if (tblHoaDon.Rows.Count <= 0)
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {
                result = false;
                Log.Error(ex.Message);
            }

            return result;
        }

        private async Task<bool> CheckSoBangKe(string xml, Guid hdon_id)
        {
            bool result = true;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                string shdon = xmlDoc.GetElementsByTagName("SBKe")[0].InnerText;

                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("shdon", shdon);
                param1.Add("hdon_id", hdon_id);

                string sql = "select shdon from #SCHEMA_NAME#.bangke_kemtheo where shdon = @shdon and hdon_id = @hdon_id";

                DataTable tblHoaDon = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, param1);

                if (tblHoaDon.Rows.Count <= 0)
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {
                result = false;
                Log.Error(ex.Message);
            }

            return result;
        }
        public async Task<JObject> RegisterUpdateAcount(JObject input)
        {
            JObject res = new JObject();
            try
            {
                Guid inv_user_id = input["inv_user_id"] !=null? Guid.Parse(input["inv_user_id"].ToString()):Guid.NewGuid();
                string masothue = input["username"].ToString();
                string email = input["email"].ToString();
                string fullname = input["fullname"].ToString();
                string editmode = input["editmode"].ToString();
                int active =int.Parse(input["active"].ToString());

                string passwordnew =  FunctionUtil.RandomPassWord();
                string tax_code = _minvoiceDbContext.GetTaxCodeSite();
                string sql = "";
                PassCommand pwcmd = new PassCommand(passwordnew);


                Dictionary<string, object> parameters = new Dictionary<string, object>();
                DataTable user = null;
                if (editmode == "1")
                {
                     sql = $@"Select * from #SCHEMA_NAME#.inv_user where username = @username";

                    parameters.Add("username", masothue);

                    user = await _masterInvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                    if (user.Rows.Count > 0)
                    {
                        throw new Exception("Đã tồn tại tên đăng nhập trong hệ thống");
                    }

                    sql = "SELECT x.* FROM #SCHEMA_NAME#.pl_customer x WHERE tax_code = @tax_code";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("tax_code", masothue);
                    DataTable data_customer = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                    string code_customer = masothue;
                    if (data_customer.Rows.Count > 0)
                    {
                        code_customer = data_customer.Rows[0]["code"].ToString();
                    }
                
                    sql = $@"INSERT INTO #SCHEMA_NAME#.inv_user(inv_user_id, tax_code, username, code_customer, password, email, date_new, date_edit, schema_name, schema_url,active,fullname)
                                            VALUES(@inv_user_id, @tax_code, @username,@code_customer, @password, @email, @date_new, @date_edit, @schema_name, @schema_url, @active,@fullname);";

                    parameters = new Dictionary<string, object>();
                    parameters.Add("inv_user_id", inv_user_id);
                    parameters.Add("tax_code", tax_code);
                    parameters.Add("username", masothue);
                    parameters.Add("code_customer", code_customer);
                    parameters.Add("password", pwcmd.CreateHashedPassword(passwordnew, null));
                    parameters.Add("email", email);
                    parameters.Add("date_new", DateTime.Now);
                    parameters.Add("date_edit", DateTime.Now);
                    parameters.Add("schema_name", tax_code);
                    parameters.Add("active", active);
                    parameters.Add("fullname", fullname);
                    parameters.Add("schema_url", ConfigurationManager.ConnectionStrings["InvoiceConnection"].ConnectionString);
                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    res.Add("ok", "Tạo mới thành công!");

                    sql = $@"Select * from #SCHEMA_NAME#.inv_user where username = @username";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("username", masothue);
                    user = await _masterInvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                }
                else if(editmode =="2")
                {
                    sql = $@"update #SCHEMA_NAME#.inv_user set fullname = @fullname, email = @email, active=@active where inv_user_id = @inv_user_id";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("inv_user_id", inv_user_id);
                    parameters.Add("fullname", fullname);
                    parameters.Add("email", email);
                    parameters.Add("active", active);

                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    res.Add("ok", "Update thông tin thành công !");

                    sql = $@"Select * from #SCHEMA_NAME#.inv_user where username = @username";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("username", masothue);
                    user = await _masterInvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                }
                else if(editmode == "3")
                {
                    sql = $@"Select * from #SCHEMA_NAME#.inv_user where username = @username";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("username", masothue);
                    user = await _masterInvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);

                    sql = $@"Delete from #SCHEMA_NAME#.inv_user where inv_user_id = @inv_user_id";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("inv_user_id", inv_user_id);

                    await _masterInvoiceDbContext.ExecuteNoneQueryAsync(sql, parameters);
                    res.Add("ok", "Xóa thành công !");
                }   
                else if (editmode == "4")
                {
                    sql = $@"update #SCHEMA_NAME#.inv_user set password = @password where inv_user_id = @inv_user_id";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("inv_user_id", Guid.NewGuid());
                    parameters.Add("password", pwcmd.CreateHashedPassword(passwordnew, null));
                    parameters.Add("email", email);
                    res.Add("ok", "Reset mật khẩu thành công !");

                    sql = $@"Select * from #SCHEMA_NAME#.inv_user where username = @username";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("username", masothue);
                    user = await _masterInvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                }

                if (editmode == "1" || editmode == "4")
                {
                    DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='CREATE_CUSTOMER_LOGIN' AND yesno='C' LIMIT 1");

                    if (tblEmail.Rows.Count > 0)
                    {
                        string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                        if (smtp_address.Length > 0)
                        {
                            int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                            bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                            this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                        }



                        string subject = tblEmail.Rows[0]["subject"].ToString();
                        string body = tblEmail.Rows[0]["body"].ToString()
                                        .Replace("#username#", masothue)
                                        .Replace("#password#", passwordnew)
                                        .Replace("#taxcode#", tax_code)
                                        .Replace("#fullname#", user.Rows[0]["fullname"].ToString())
                                        .Replace("#trangthai#", user.Rows[0]["active"].ToString() == "1" ? "Hoạt động" : "Tạm khóa")
                                        .Replace("#url_host#", _webHelper.GetUrlHost());

                        ThreadPool.QueueUserWorkItem(state => this._emailService.Send(email, subject, body));

                    }
                    else
                    {
                        throw new Exception("Đã tạo tài khoản nhưng mẫu Email chưa được thiếp lập");
                    }
                }
                else if(editmode == "2")
                {
                    DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='UPDATE_CUSTOMER_LOGIN' AND yesno='C' LIMIT 1");

                    if (tblEmail.Rows.Count > 0)
                    {
                        string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                        if (smtp_address.Length > 0)
                        {
                            int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                            bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                            this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                        }



                        string subject = tblEmail.Rows[0]["subject"].ToString();
                        string body = tblEmail.Rows[0]["body"].ToString()
                                        .Replace("#username#", masothue)
                                        .Replace("#taxcode#", tax_code)
                                        .Replace("#fullname#", user.Rows[0]["fullname"].ToString())
                                        .Replace("#trangthai#", user.Rows[0]["active"].ToString() == "1" ? "Hoạt động" : "Tạm khóa")
                                        .Replace("#url_host#", _webHelper.GetUrlHost());

                        ThreadPool.QueueUserWorkItem(state => this._emailService.Send(email, subject, body));

                    }
                    else
                    {
                        throw new Exception("Đã chỉnh sửa nhưng mẫu Email chưa được thiếp lập");
                    }
                }
                else
                {
                    DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='DELETE_CUSTOMER_LOGIN' AND yesno='C' LIMIT 1");

                    if (tblEmail.Rows.Count > 0)
                    {
                        string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                        if (smtp_address.Length > 0)
                        {
                            int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                            bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                            this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                        }



                        string subject = tblEmail.Rows[0]["subject"].ToString();
                        string body = tblEmail.Rows[0]["body"].ToString()
                                        .Replace("#username#", masothue)
                                        .Replace("#password#", passwordnew)
                                        .Replace("#taxcode#", tax_code)
                                        .Replace("#fullname#", user.Rows[0]["fullname"].ToString())
                                        .Replace("#trangthai#", user.Rows[0]["active"].ToString() == "1" ? "Hoạt động" : "Tạm khóa")
                                        .Replace("#url_host#", _webHelper.GetUrlHost());

                        ThreadPool.QueueUserWorkItem(state => this._emailService.Send(email, subject, body));

                    }
                    else
                    {
                        throw new Exception("Đã tạo tài khoản nhưng mẫu Email chưa được thiếp lập");
                    }
                }

            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }
            return res;

        }
    }
}
