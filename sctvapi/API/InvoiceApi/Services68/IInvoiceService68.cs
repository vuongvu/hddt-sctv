﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InvoiceApi.Services68
{
    public partial interface IInvoiceService68
    {
        Task<JArray> GetDetailHoadonChoXuLy(string data);
        Task<byte[]> SignXmlBangKeBanRa(JObject model);
        Task<JObject> GetEmailTemplate(string type, string id, string ma_dvcs, string originalString);
        Task<JObject> SaveHoadon(JObject obj, string username);
        Task<JObject> GetData(string select, string ptrang, int start, int count, string filter, string tlbparam);
        Task<JArray> GetDataInvoiceDetail(string id);
        //Task<JObject> XmlInvoice(string hdon_id, int shdon);
        Task<JObject> XmlInvoice(string hdon_id);
        Task<string> XmlThongDiepInvoice(string hdon_id);
        Task<JObject> Amount_ToWord(string number, string type_amount);
        Task<JObject> Amount_ToWord_New(string number, int checkReadTV);
        Task<JArray> GetListCertificatesFile68(string userName, string ma_dvcs);
        Task<JObject> SignInvoiceCertFile68(string folder, JObject model, bool reSign = false);
        Task<JObject> SignInvoiceCertFile68SCTV(string folder, JObject model, bool reSign = false);
        Task<JObject> SignInvoiceCertFile68ForHasCodeAndNotHaveAnyCode(string folder, JObject model);
        Task<JObject> SendInvoiceToCQT68(JObject model);
        Task<JObject> SaveQuanlymau68(string editMode, JObject model, byte[] bytes);
        Task<JObject> deleteMau68(JObject model);
        byte[] DowloadRepx(string id);
        Task<JObject> SaveQuanlykyhieu68(string editMode, JObject model);
        Task<JObject> deletequanlykyhieu68(JObject model);
        Task<JArray> getKyhieu(string cctbao_id);
        Task<JObject> SaveDangkysudung(JObject obj);
        Task<JObject> getDangkysudung();
        Task<JArray> GetDataDangkysudungDetail(string id);
        Task<JArray> GetDataDangkysudungUyNhiem(string id);
        Task<JObject> deleteMau01(JObject model);
        Task<JObject> SignDangkysudung(JObject model);
        Task<string> ExportXMLMau01(string id);
        Task<byte[]> inHoadon(string id, string folder, string type, bool inchuyendoi, bool checkBangKe = false);

        Task<JObject> SaveHuyhoadon(JObject obj);
        Task<JObject> SignHuyhoadon(JObject model, string id, string hdon_id, string tthdon);
        Task<JObject> SaveHuyHoadonTaomoi(JObject model, string username);
        Task<JArray> GetDataMau0405Detail(string id);
        Task<byte[]> inMau01(string id, string folder, string type);
        Task<JObject> deleteMau04(JObject model);
        Task<string> ExportXMLMau02(string id);
        Task<string> ExportXMLMau03(string id);
        Task<byte[]> inMau02(string id, string folder, string type);
        Task<byte[]> inMau03(string id, string folder, string type);
        Task<string> ExportXMLHoadon(string id);
        Task<string> ExportXMLThongDiepHoadon(string id);
        Task<JObject> GetHoadonChoXuLy(JObject model);
        Task<JObject> SaveBangTHDL(JObject obj, string username);
        Task<JArray> GetDataBangTHDLDetail(string id);
        Task<JArray> GetResponseMessage(string id);
        Task<JArray> GetHistoryInvoice(string id);
        Task<byte[]> inMau04(string id, string folder, string type, string loai);
        Task<byte[]> inMau04Rep(string id, string folder, string type);
        Task<JObject> SignBTHDL(JObject model);
        Task<string> ExportXMLBangTHDL(string id);
        Task<string> ExportXMLMau04(string id);
        Task<string> ExportXMLMau04Rep(string id);
        Task<string> ExportXMLMaThongDiepCQT(string id);
        Task<byte[]> inMaThongDiepCQT(string xml, string folder, string type);

        Task<byte[]> inBTHDLieu(string id, string folder, string type);
        Task<byte[]> inMauHoadon(string id, string type, string xml = "");
        Task<JObject> SendInvoiceByEmail(string folder, JObject model, string ma_dvcs, string type);
        Task<JObject> AutoSendInvoiceByEmail(string folder, JObject model);
        Task<JArray> GetSignType();
        Task<JObject> ExportInvoiceXml(Guid id);
        Task<JArray> GetListCertificates(string userName, string ma_dvcs);
        Task<JObject> SaveXml(JObject model);
        Task<string> XmlHuyhoadonMau04(string id);
        Task<JObject> XmlMau0405(string id);
        Task<JObject> SaveXmlMau0405(JObject model);
        Task<JObject> SaveHuyHoadonTaomoiUSB(JObject model, string username);
        Task<JObject> SaveXmlBTHDL(JArray model);
        Task<JObject> XmlBTHDL(string id);
        Task<JObject> XmlMau01(string id);
        Task<JObject> SaveXmlMau01(JObject model);
        Task<JObject> GetDSHoaDonBangKeBanRa(JObject model);
        Task<JObject> SaveBangKeBanRa(JObject obj, string username);
        Task<JArray> GetDataBangKeBanRaDetail(string id);
        Task<JObject> SignBangKeBanRa(JObject model);
        Task<JObject> SaveXmlBangKeBanRa(JObject model);
        Task<JObject> XmlBangKeBanRa(string id);
        Task<byte[]> inBangKeBanRa(string id, string folder, string type);
        Task<string> ExportXMLBangKeBanRa(string id);
        Task<JArray> GetInfoCompany(string branch_code);
        Task<string> ExportXMLBTHDLRep(string id);
        Task<byte[]> inBTHDLRep(string id, string folder, string type);
        Task<JArray> GetDataInvoicePhi(string id);
        Task<JObject> GetDataTruyenNhan(string sql, int start, int count, string filter, string tggui_tu, string tggui_den, int check_error);
        Task<JObject> GetDataTruyenNhan_Chitiet(string sql, string id);

        // giả lập gửi CQT
        Task<JObject> CreateHoaDonSendTCT(JObject obj, string username);
        Task<JObject> KiHoaDonGuiTCT(JObject obj);
        Task<string> GetInvoiceFileName(string id);
        Task GenerateThongDiep();

        Task<JObject> SaveSoHoaDonKiHieu(JObject obj);
        Task<JObject> PhanKiHieuDonVi(JObject obj);
        Task<JObject> bangkebanra(JObject obj);
        Task<JObject> DeletePhanKiHieuDonVi(JObject obj);

        Task<JArray> GetDecimalPlace(string ma_nt);

        Task<JArray> GetDataInvoiceKhac(string id);

        Task<JObject> GetXmlData(Guid id);
        Task<JObject> SignConfirm(JObject obj);
        // API tích hợp với PMKT
        Task<JObject> SaveHoadon78(JObject obj, string username);
        Task<JObject> SaveHoadon78Pretreatment(JObject obj, string username);
        Task<JObject> GetInvoiceById(Guid id);
        Task<JObject> GetInvoiceById_Pretreatment(Guid id);
        Task<JObject> GetDataNewWith_Bangke(string select, string ptrang, int start, int count, string filter, string tlbparam);
        Task<JArray> GetDataInvoiceDetail_BangKe(string id);
        Task<JArray> GetInvoiceFromdateTodate(JObject obj);
        Task<JArray> GetListInvoiceIdFromdateTodate(JObject obj);
        Task<JArray> GetListInvoiceIdFromdateTodate_Pretreatment(JObject obj);
        Task<JArray> GetInvoiceFromdateTodate_Pretreatment(JObject obj);
        JArray DashboardTiles78(string ma_dvcs);
        Task<JObject> GetEmailTemplate78(string type, string id, string ma_dvcs, string originalString);
        Task<JObject> SignListInvoiceCertFile_HSM(string folder,JObject model);
        Task<JObject> SignDangkysudung_HSM(JObject model);
        Task<JObject> Xoanhieuhoadon(JArray data, string username);
        Task<string> ExportXMLBangKe_HD(string id);
        Task<JObject> SaveHuyhoadonPretreatment(JObject obj);
        Task<string> UploadBangKeTemplate(string ma_dvcs, string user_login, string excel_id, byte[] bytes);
        byte[] DowloadRepxBangKe(string id);
        Task<byte[]> inMauHoadon_QuanLyMau(string id, string type, string xml = "", bool checkBangKe = false);
        Task<JObject> GetCTS_HSM_ToKhai();

        Task<string> GetSqlData(string select, string filter, string tlbparam);

        Task<JObject> SaveBangTHDLXangDau(JObject model, string userName);
        Task<JArray> GetDetailHoaDonXangDauChoXuLy(string data);
        Task<JArray> GetDataBangTHDLXangDauDetail(string id);
        Task<JObject> SignBTHDLXangDau(JObject model);
        Task<JObject> XmlBTHDLXangDau(string id);
        Task<JObject> SaveXmlBTHDLXangDau(JArray model);
        Task<byte[]> inBTHDLieuXangDau(string id, string folder, string type);
        Task<JObject> DuyetHoaDon(JObject obj);
        Task<byte[]> InDanhSachHoaDon(JObject model, string folder, string type, bool inChuyenDoi, bool checkBangKe);
        Task<byte[]> TaiDanhSachHoaDon(JObject model, string folder, string type, bool inChuyenDoi, bool checkBangKe);

        //Task<JArray> uploadCanceledInv(string id);
        Task<JObject> uploadCanceledInv(string id);

        Task<JObject> GetHoadonChoXuLy_LayToanBoHoaDon(JObject model);
        Task<int> tangSoBTHDL(string fromDate, string toDate);
        Task<JObject> SaveXmlPretreatment(JObject model);
        Task<JObject> ExportInvoiceXmlPretreatment(Guid id);
        Task<JObject> GetDSHoaDonBangKeBanRaChiTiet(JObject model);

        Task<DataSet> GetDataForPrint(string sql, Dictionary<string,object> parameters, JArray filter, string order);
        Task<JObject> ReSignInvoiceCertFile68(string folder, JObject model, bool reSign = false);
        Task<JObject> GetDataMaster(string select, string ptrang, int start, int count, string filter, string tlbparam);
        Task<JObject> RegisterUpdateAcount(JObject input);
    }
}