﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class NhapXuatTonModel
    {
        public string ma_hv { get; set; }
        public string ten_hv { get; set; }
        public decimal? sl_dk { get; set; }
        public decimal? sl_nhap { get; set; }
        public decimal? sl_xuat { get; set; }
        public decimal? sl_ck { get; set; }
        public string rowstyle { get; set; }
    }
}