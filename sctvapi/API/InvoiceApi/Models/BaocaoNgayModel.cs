﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class BaocaoNgayModel
    {
        public int? stt { get; set; }
        public string dien_giai { get; set; }
        public decimal? tien { get; set; }
        public string nhom { get; set; }
    }
}