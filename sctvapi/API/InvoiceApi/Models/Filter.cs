﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApi.Models
{
    public class Filter
    {
        public string value { get; set; }

        public static string FilterGrid(string txtFilter, string type)
        {
            string text = "";
            txtFilter = txtFilter.Replace("'", "''");

            if (type == "decimal")
            {
                text = txtFilter;
            }
            else if (type == "datetime")
            {
                string dk = "";
                string str = "";
                if (txtFilter.StartsWith("="))
                {
                    dk = "=";
                    str = txtFilter.Substring(1);
                }
                else if (txtFilter.StartsWith(">="))
                {
                    dk = ">=";
                    str = txtFilter.Substring(2);
                }
                else if (txtFilter.StartsWith("<="))
                {
                    dk = "<=";
                    str = txtFilter.Substring(2);
                }
                else if (txtFilter.StartsWith(">"))
                {
                    dk = ">";
                    str = txtFilter.Substring(1);
                }
                else if (txtFilter.StartsWith("<"))
                {
                    dk = "<";
                    str = txtFilter.Substring(1);
                }

                string[] chuoi = str.Split('/');
                string ngay = "";
                for (int i = chuoi.Length - 1; i >= 0; i--)
                {
                    ngay += "-" + chuoi[i];
                }
                text = dk + "'" + ngay.Substring(1) + "'";
            }
            else {

                // if (txtFilter.StartsWith("$") || txtFilter.EndsWith("$"))
                // {
                //     if (txtFilter.StartsWith("$"))
                //     {
                //         txtFilter = "%" + txtFilter.Substring(1, txtFilter.Length);
                //     }
                //
                //     if (txtFilter.EndsWith("$"))
                //     {
                //         txtFilter = txtFilter.Substring(0, txtFilter.Length-1) + "%";
                //     }
                //
                //     text = " ILIKE N'" + txtFilter + "' ";
                // }
                // else if (txtFilter.StartsWith("="))
                // {
                //     text = txtFilter + " ";
                // }
                // else
                // {
                //     text = " ILIKE N'%" + txtFilter + "%' ";
                // }
                
                if (txtFilter.Contains("%"))
                {
                    txtFilter = txtFilter.Replace("%", "\\%");
                }
                if (txtFilter.Contains("_"))
                {
                    txtFilter = txtFilter.Replace("_", "\\_");
                }
                text = " ILIKE lower(N'%" + txtFilter + "%') ";
            }
            return text;
        }
    }
}