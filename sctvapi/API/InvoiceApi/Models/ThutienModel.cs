﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class ThutienModel
    {
        public Guid? hp_dmhs_id { get; set; }
        public Guid? hp_khoahoc_id { get; set; }
        public Guid? hp_lop_id { get; set; }
        public DateTime? thang { get; set; }
        public Guid? hp_khoanthu_id { get; set; }
        public decimal? so_luong { get; set; }
        public decimal? gia { get; set; }
        public decimal? tien { get; set; }
        public decimal? giam_tru { get; set; }
        public decimal? thanh_tien { get; set; }
        public decimal? da_thu { get; set; }
        public decimal? da_tra { get; set; }
        public decimal? con_lai { get; set; }
        public int? nam1 { get; set; }
        public int? nam2 { get; set; }
        public string ten_khoa_hoc { get; set; }
        public string ten_khoan_thu { get; set; }
        public string ten_lop { get; set; }
        public string ho_ten { get; set; }
        public bool chon { get; set; }
        public DateTime? ngay_sinh { get; set; }
        public string hoc_ky { get; set; }
        public string nam_hoc { get; set; }

        public string ten_thang
        {
            get
            {
                if (thang != null)
                {
                    return string.Format("{0:yyyy-MM}", thang);
                }

                return "";
            }
        }

        public string value
        {
            get
            {
                return ten_khoan_thu;
            }
        }
    }
}