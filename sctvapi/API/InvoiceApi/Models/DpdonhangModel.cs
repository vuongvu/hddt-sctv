﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class DpdonhangModel
    {
        public Guid id { get; set; }
        public DateTime? ngay_dh { get; set; }
        public string so_dh { get; set; }
        public Guid? dmdaily_id { get; set; }
        public string user_new { get; set; }
        public DateTime? date_new { get; set; }
        public string user_edit { get; set; }
        public DateTime? date_edit { get; set; }
        public string ma_dvcs { get; set; }
        public string ten_daily { get; set; }
        public string trang_thai { get; set; }
    }
}