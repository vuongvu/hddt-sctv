﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class HpctchamcongModel
    {
        public Guid? id { get; set; }
        public Guid? hp_dpchamcong_id { get; set; }
        public Guid? hp_dmhs_id { get; set; }
        public bool? ngay_01 { get; set; }
        public bool? ngay_02 { get; set; }
        public bool? ngay_03 { get; set; }
        public bool? ngay_04 { get; set; }
        public bool? ngay_05 { get; set; }
        public bool? ngay_06 { get; set; }
        public bool? ngay_07 { get; set; }
        public bool? ngay_08 { get; set; }
        public bool? ngay_09 { get; set; }
        public bool? ngay_10 { get; set; }
        public bool? ngay_11 { get; set; }
        public bool? ngay_12 { get; set; }
        public bool? ngay_13 { get; set; }
        public bool? ngay_14 { get; set; }
        public bool? ngay_15 { get; set; }
        public bool? ngay_16 { get; set; }
        public bool? ngay_17 { get; set; }
        public bool? ngay_18 { get; set; }
        public bool? ngay_19 { get; set; }
        public bool? ngay_20 { get; set; }
        public bool? ngay_21 { get; set; }
        public bool? ngay_22 { get; set; }
        public bool? ngay_23 { get; set; }
        public bool? ngay_24 { get; set; }
        public bool? ngay_25 { get; set; }
        public bool? ngay_26 { get; set; }
        public bool? ngay_27 { get; set; }
        public bool? ngay_28 { get; set; }
        public bool? ngay_29 { get; set; }
        public bool? ngay_30 { get; set; }
        public bool? ngay_31 { get; set; }
        public int? so_ngay { get; set; }
        public Guid? hp_lop_id { get; set; }
        public int? so_thu { get; set; }
        public string ho_ten { get; set; }
    }
}