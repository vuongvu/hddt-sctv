﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApi.Models
{
    public class SignedXmlReturn
    {
        public string id { get; set; }
        public string uploadXmlId { get; set; }
        public string signedBy { get; set; }
        public string xmlData { get; set; }
        public string createdAt { get; set; }
        public string updatedAt { get; set; }
        public string xmlSignedData { get; set; }
    }
}