﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApi.Models
{
    public class ResultCheckSimPKI
    {
        public string Status { get; set; }
        public string StrError { get; set; }
        public string X509SubjectName { get; set; }
        public string dtStart { get; set; }
        public string dtEnd { get; set; }
        public string issuer { get; set; }
        public string serial { get; set; }
    }
}