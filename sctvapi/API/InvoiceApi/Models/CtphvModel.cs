﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class CtphvModel
    {
        public Guid id { get; set; }
        public Guid dpphv_id { get; set; }
        public string ma_kho { get; set; }
        public string ma_hv { get; set; }
        //public decimal? size { get; set; }
        public decimal? so_luong { get; set; }
        public decimal? gia2 { get; set; }
        public decimal? tien2 { get; set; }
        public string ten_hv { get; set; }
        public string ma_dvcs { get; set; }
        public string ten_hv1 { get; set; }
        public decimal? gia { get; set; }
        public decimal? tien { get; set; }
        public string mau { get; set; }
        public decimal? quai { get; set; }
        public decimal? size { get; set; }
        public string stt_rec0 { get; set; }
    }
}