﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class TonkhoModel
    {
        public string ma_kho { get; set; }
        public string ma_hv { get; set; }
        public decimal? so_luong { get; set; }
        public decimal? tien_nt { get; set; }
        public decimal? tien { get; set; }
        public string ten_hv { get;set;}
        public decimal? gia_ban { get;set;}
        public string mau { get; set; }
        public decimal? size { get; set; }
        public decimal? doday { get; set; }
        public decimal? quai { get; set; }
        public string pic { get; set; }
    }
}