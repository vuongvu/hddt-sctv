﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class CtNhomQuyenModel
    {
        public Guid? id { get; set; }
        public Guid? wb_menu_id { get; set; }
        public Guid? wb_nhomquyen_id { get; set; }
        public string ten_menu { get; set; }
        public string ten_phanhe { get; set; }
        public string ma_phanhe { get; set; }
        public string ma_menu { get; set; }
        public bool chon { get; set; }
    }
}