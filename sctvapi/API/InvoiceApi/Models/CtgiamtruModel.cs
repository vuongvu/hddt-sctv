﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class CtgiamtruModel
    {
        public Guid id { get; set; }
        public Guid? hp_dmhs_id { get; set; }
        public Guid? hp_khoangiamtru_id { get; set; }
        public Guid? hp_khoahoc_id { get; set; }
        public Guid? hp_lop_id { get; set; }
        public int? hoc_ky { get; set; }
        public decimal? tien { get; set; }
        public DateTime? thang { get; set; }
        public string user_new { get; set; }
        public DateTime? date_new { get; set; }
        public string user_edit { get; set; }
        public DateTime? date_edit { get; set; }
        public string ma_dvcs { get; set; }
        public string ten_khoa_hoc { get; set; }
        public string ten_lop { get; set; }
        public string ho_ten { get; set; }
        public string ten_giam_tru { get; set; }
    }
}