﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class DpphvModel
    {
        public Guid id { get; set; }
        public string ma_ct { get; set; }
        public int? nhom_ct { get; set; }
        public DateTime? ngay_ct { get; set; }
        public string so_ct { get; set; }
        public Guid? dmdaily_id { get; set; }
        public string user_new { get; set; }
        public DateTime? date_new { get; set; }
        public string user_edit { get; set; }
        public DateTime? date_edit { get; set; }
        public string ma_dvcs { get; set; }
        public string ten_daily { get; set; }
        public string so_dh { get; set; }
        public Guid? dpdonhang_id { get; set; }
    }
}