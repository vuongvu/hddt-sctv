﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class HpdpchamcongModel
    {
        public Guid id { get; set; }
        public Guid? hp_khoahoc_id { get; set; }
        public Guid? hp_lop_id { get; set; }
        public Guid? hp_khoanthu_id { get; set; }
        public DateTime? thang { get; set; }
        public DateTime? ngaycham { get; set; }
        public int? so_ngay { get; set; }
        public decimal? gia { get; set; }
        public string bua_an { get; set; }
        public string user_new { get; set; }
        public DateTime? date_new { get; set; }
        public string user_edit { get; set; }
        public DateTime? date_edit { get; set; }
        public string ma_dvcs { get; set; }
        public string ten_lop { get; set; }

        
    }
}