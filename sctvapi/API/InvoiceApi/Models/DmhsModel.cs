﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class DmhsModel
    {
        public Guid? id { get; set; }
        public string ho_ten { get; set; }
        public DateTime? ngay_sinh { get; set; }
        public string gioi_tinh { get; set; }
        public string ten_khoa_hoc { get; set; }
        public string ten_lop { get; set; }
    }
}