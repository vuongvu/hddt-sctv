﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class DmhvModel
    {
        public Guid id { get; set; }
        public string ma_hv { get; set; }
        public string ten_hv { get; set; }
        public decimal? gia_nhap { get; set; }
        public decimal? gia_ban { get; set; }
        public string mau { get; set; }
        public decimal? doday { get; set; }
        public decimal? quai { get; set; }
        public decimal? size { get; set; }
        public string user_new { get; set; }
        public DateTime? date_new { get; set; }
        public string user_edit { get; set; }
        public DateTime? date_edit { get; set; }
        public string ma_dvcs { get; set; }
        public string pic { get; set; }
        public decimal? ton_min { get; set; }
        public decimal? ton_max { get; set; }
        
    }
}