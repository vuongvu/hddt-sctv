﻿namespace InvoiceApi.Models
{
    public class Response
    {
        public int Code { get; set; } = 1;
        public string Message { get; set; } = "";
        public dynamic Data { get; set; }
    }
}