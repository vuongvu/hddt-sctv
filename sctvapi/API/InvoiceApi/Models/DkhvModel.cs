﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class DkhvModel
    {
        public Guid id { get; set; }
        public string ma_hv { get; set; }
        public string ma_kho { get; set; }
        public decimal? so_luong { get; set; }
        public decimal? gia { get; set; }
        public decimal? tien { get; set; }
        public decimal? size { get; set; }
        public string user_new { get; set; }
        public DateTime? date_new { get; set; }
        public string user_edit { get; set; }
        public DateTime? date_edit { get; set; }
        public string ma_dvcs { get; set; }
        public int? nam { get; set; }
    }
}