﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class DppttModel
    {
        public Guid id { get; set; }
        public DateTime? ngay_ct { get; set; }
        public string so_ct { get; set; }
        public Guid? hp_dmhs_id { get; set; }
        public Guid? hp_khoahoc_id { get; set; }
        public Guid? hp_lop_id { get; set; }
        public string nguoi_nop { get; set; }
        public string dia_chi { get; set; }
        public string dien_giai { get; set; }
        public string ma_ct { get; set; }
        public int? nhom_ct { get; set; }
        public string ma_nt { get; set; }
        public decimal? ty_gia { get; set; }
        public Guid? dmdt_id { get; set; }
        public string tai_khoan { get; set; }
        public string ngan_hang { get; set; }
        public DateTime? ngay_sinh { get; set; }
        public string htthu { get; set; }
        public string user_new { get; set; }
        public DateTime? date_new { get; set; }
        public string user_edit { get; set; }
        public DateTime? date_edit { get; set; }
        public string ma_dvcs { get; set; }
        public string ho_ten { get; set; }
        public string ten_khoa_hoc { get; set; }
        public string ten_lop { get; set; }

    }
}