﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class ChangePassModel
    {
        public string oldpass { get; set; }
        public string newpass { get; set; }
    }
}