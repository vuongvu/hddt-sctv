﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class LichsuDonHangModel
    {
        public DateTime? ngay_ct { get; set; }
        public string so_ct { get; set; }
        public string ong_ba { get; set; }
        public DateTime? date_new { get; set; }
        public decimal? tien2 { get; set; }
        public decimal? so_luong { get; set; }
        public Guid? id { get; set; }
    }
}