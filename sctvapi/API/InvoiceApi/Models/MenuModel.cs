﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThuhpApi.Models
{
    public class MenuModel
    {
        public Guid id { get; set; }
        public string ma_menu { get; set; }
        public string ten_menu { get; set; }
        public string icon { get; set; }
        public string details { get; set; }
        public Guid? wb_phanhe_id { get; set; }
        public string ten_phanhe { get; set; }
        public string code { get; set; }
        public string parameter { get; set; }
                
        public string value
        {
            get
            {
                return ten_menu;
            }
        }

    }
}