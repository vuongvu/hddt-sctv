﻿namespace InvoiceApi.Models
{
    public class SignXML
    {
        public string xmlData { get; set; }
        public string msisdn { get; set; }
        public string signingTagId { get; set; }
        public string signingTagName { get; set; }
        public string namespacePrefix { get; set; }
        public string signatureTagId { get; set; }
        public string prompt { get; set; }
        public string reason { get; set; }
    }
}