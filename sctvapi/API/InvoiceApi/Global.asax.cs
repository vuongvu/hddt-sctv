﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using InvoiceApi.Services;
using System.Configuration;
using PkiTokenManager;
using InvoiceApi.Util;
using System.Globalization;
using InvoiceApi.Data;
using InvoiceApi.Services68;

namespace InvoiceApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RegisterAutofacApi();
            log4net.Config.XmlConfigurator.Configure();

            AppContext.SetSwitch("Switch.System.Security.Cryptography.Xml.UseInsecureHashAlgorithms", true);
            AppContext.SetSwitch("Switch.System.Security.Cryptography.Pkcs.UseInsecureHashAlgorithms", true);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                
                HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }            
        }       

        
        private void RegisterAutofacApi()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<MemoryCacheManager>().As<ICacheManager>().Named<ICacheManager>("nop_cache_static").SingleInstance();
            builder.Register<IWebHelper>(c => new WebHelper(new HttpContextWrapper(HttpContext.Current) as HttpContextBase)).InstancePerLifetimeScope();
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerLifetimeScope();
            builder.RegisterType<EmailService>().As<IEmailService>().InstancePerLifetimeScope();
            builder.RegisterType<WbWindowService>().As<IWbWindowService>().InstancePerLifetimeScope();
            builder.RegisterType<WbTabService>().As<IWbTabService>().InstancePerLifetimeScope();
            builder.RegisterType<WbFieldService>().As<IWbFieldService>().InstancePerLifetimeScope();
            builder.RegisterType<SystemService>().As<ISystemService>().InstancePerLifetimeScope();
            builder.RegisterType<NopDbContext>().As<INopDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<MInvoiceDbContext>().As<IMinvoiceDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<InvoiceService>().As<IInvoiceService>().InstancePerLifetimeScope();
            builder.RegisterType<BcthsdService>().As<IBcthsdService>().InstancePerLifetimeScope();
            builder.RegisterType<LogService>().As<ILogService>().InstancePerLifetimeScope();
            builder.RegisterType<LicenseService>().As<ILicenseService>().InstancePerLifetimeScope();
            builder.RegisterType<TokenService>().As<ITokenService>().InstancePerLifetimeScope();
            builder.RegisterType<SearchInvoicesService>().As<ISearchInvoicesService>().InstancePerLifetimeScope();
            builder.RegisterType<RabbitMQService>().As<IRabbitMQService>().SingleInstance();
            builder.RegisterType<TaskService>().As<ITaskService>().InstancePerLifetimeScope();
            builder.RegisterType<InvoiceService68>().As<IInvoiceService68>().InstancePerLifetimeScope();
            builder.RegisterType<MasterInvoiceDbContext>().As<IMasterInvoiceDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<WbLogAPIService>().As<IWbLogAPI>().InstancePerLifetimeScope();
            //builder.RegisterType<KafkaService>().As<IKafkaService>().SingleInstance();

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            if (ConfigurationManager.AppSettings["USB_DLL"] != null)
            {
                PKI.GetInstance(ConfigurationManager.AppSettings["USB_DLL"].ToString());
            }

            var rabbitMq = container.Resolve<IRabbitMQService>();
            //var kafka = container.Resolve<IKafkaService>();

        }
    }
}
