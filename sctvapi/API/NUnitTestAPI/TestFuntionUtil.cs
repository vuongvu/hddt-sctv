﻿using InvoiceApi.Util;
using NUnit.Framework;

namespace NUnitTestAPI
{
    [TestFixture]
    public class TestDownload
    {
        [Test]
        public void TestNumberPriceToString()
        {
            string test = FunctionUtil.VND_Amount2("151320");
            Assert.AreEqual(test, "Một trăm năm mươi mốt nghìn ba trăm hai mươi đồng.");
        }
    }
}
