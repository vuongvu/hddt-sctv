﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Search_Invoice.Data.Domain;
using Search_Invoice.DAL;
using Search_Invoice.Services;
using Search_Invoice.Util;
using System.Data;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Search_Invoice.Data;
using System.Threading.Tasks;

namespace Search_Invoice.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IMasterInvoiceDbContext _masterDbContext;
        private readonly IMinvoiceDbContext _minvoiceDbContext;

        //private string URL = "http://0100686209.minvoice.com.vn/";
        public CustomerController()
        {
            _masterDbContext = new MasterInvoiceDbContext();
            _minvoiceDbContext = new MInvoiceDbContext(null);
        }
        // GET: Customer
        public ActionResult Search_Invoice()
        {
            var us = (inv_user)Session[CommonConstants.USER_SESSION];
            if (us == null)
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult buyerSign(string id, string type, string mst)
        {
            if (type == "78")
            {
                ViewBag.URL = mst + "/api/Invoice68/Preview?id=" + id;
            }
            else
            {
                ViewBag.URL = mst + "/api/Invoice/Preview?id=" + id;
            }
            ViewBag.Version = type;
            ViewBag.ID = id;
            ViewBag.Mst = mst;
            return View();
        }

        [AllowAnonymous]
        public JObject getXml(string id, string mst)
        {
            JObject result = new JObject();
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("Content-Type", "application/json");
                client.Encoding = System.Text.Encoding.UTF8;
                string obj = client.DownloadString(mst + "/api/Invoice/SignConfirm?id=" + id);
                result = JObject.Parse(obj);
            }
            catch (Exception e)
            {
                result.Add("error", e.Message);
            }
            return result;
        }

        [AllowAnonymous]
        public JObject getXml78(string id, string mst)
        {
            JObject result = new JObject();
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("Content-Type", "application/json");
                client.Encoding = System.Text.Encoding.UTF8;
                string obj = client.DownloadString(mst + "/api/Invoice68/SignConfirm?id=" + id);
                result = JObject.Parse(obj);
            }
            catch (Exception e)
            {
                result.Add("error", e.Message);
            }
            return result;
        }

        public async Task<JObject> GetInvoiceFromdateTodate(DateTime tu_ngay, DateTime den_ngay)
        {
            JObject result = new JObject();
            try
            {
                inv_user us = (inv_user)Session[CommonConstants.USER_SESSION];

                if (us == null)
                {
                    result.Add("error_login", "Hết phiên làm việc. Vui lòng đăng nhập lại ");
                    return result;
                }

                string sqlGetschema = $@"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.inv_user WHERE username =@username AND tax_code =@tax_code";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("username", us.username);
                parameters.Add("tax_code", us.tax_code);

                DataTable dtschema = await _masterDbContext.GetDataTableAsync(sqlGetschema,CommandType.Text, parameters);

                if (dtschema.Rows.Count == 0)
                {
                    result.Add("error", "Không tìm thấy MST.");
                    return result;
                }

                string mst = dtschema.Rows[0]["tax_code"].ToString();
                this._minvoiceDbContext.SetSiteHddt(mst);
                string sql = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth \n" +
                    "WHERE status != 'Chờ ký' AND signed_date is not null \n" +
                    "AND invoice_issued_date >= DATE_TRUNC('day', @tu_nay) and invoice_issued_date <= DATE_TRUNC('day', @den_ngay) \n" +
                    "AND buyer_taxcode =@username AND invoice_status != 13 ORDER BY invoice_issued_date, invoice_number DESC ";

                //parameters = new Dictionary<string, object>();
                parameters.Clear();
                parameters.Add("tu_nay", tu_ngay);
                parameters.Add("den_ngay", den_ngay);
                parameters.Add("username", us.username);

                DataTable dt = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
               
                dt.Columns.Add("mst", typeof(string));

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        row.BeginEdit();
                        row["mst"] = us.tax_code;
                        row.EndEdit();
                    }

                    JArray jar = JArray.FromObject(dt);
                    result.Add("data", jar);
                    result.Add("isTT32", "1");
                }
                else
                {
                    sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 \n" +
                    "WHERE tthai != 'Chờ ký' AND nky is not null \n" +
                    "AND nlap >= DATE_TRUNC('day', @tu_nay) and nlap <= DATE_TRUNC('day', @den_ngay) \n" +
                    "AND mst =@username AND tthdon != 13 ORDER BY nlap, shdon DESC ";

                    DataTable dt_tt78 = await _minvoiceDbContext.GetDataTableAsync(sql,CommandType.Text ,parameters);
                    //dt.Columns.Add("mst", typeof(string));

                    if (dt_tt78.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt_tt78.Rows)
                        {
                            row.BeginEdit();
                            row["mst"] = us.tax_code;
                            row.EndEdit();
                        }

                        JArray jar = JArray.FromObject(dt_tt78);
                        result.Add("data", jar);
                        result.Add("isTT32", "0");
                    }
                    else
                    {
                        result.Add("error", "Không tìm thấy dữ liệu.");
                    }
                }
            }
            catch (Exception ex)
            {
                result = new JObject();
                result.Add("error", ex.Message.ToString());
            }
            return result;

        }

        //[HttpPost]
        //[Route("Tracuu2/PrintInvoice")]
        //[AllowAnonymous]
        public HttpResponseMessage PrintInvoice(JObject model)
        {

            HttpResponseMessage result = null;
            try
            {
                string type = model["type"].ToString();
                string sobaomat = model["sobaomat"].ToString();
                string masothue = model["masothue"].ToString();
                //if (_tracuuService2 == null)
                //{
                //    throw new Exception("Không tồn tại mst:");
                //}
                //string type = "PDF";
                string path = Server.MapPath("~/Content/report/");
                //string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                //string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";
                //string path = "~/Content/report/";
                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = null;
                //_tracuuService2.PrintInvoiceFromSBM(sobaomat, masothue, folder, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.BadRequest);
                result.Content = new StringContent(ex.Message, System.Text.Encoding.UTF8);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        public JsonResult SaveEmployeeRecord(string id, string name)
        {
            string this_id = id;
            string this_name = name;
            // do here some operation  
            return Json(new { id = this_id, name = this_name }, JsonRequestBehavior.AllowGet);
        }
    }
}