﻿using Newtonsoft.Json.Linq;
using Search_Invoice.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Search_Invoice.Controllers
{
    public class Tracuu2Controller : ApiController
    {
        private ITracuuService2 _tracuuService2;
        //private string URL = "http://0100686209.minvoice.com.vn/";
        public Tracuu2Controller(ITracuuService2 tracuuService2)
        {
            this._tracuuService2 = tracuuService2;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Tracuu2/nam1")]
        public JArray nam1()
        {
            JArray jarr = new JArray() { "avvvvvvv", "aaaaaaaaaaaa" };
            return jarr;
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("Tracuu2/GetInvoiceFromdateTodate")]
        //public JObject GetInvoiceFromdateTodate(JObject model)
        //{
        //    Session
        //    JObject json = _tracuuService2.GetInfoInvoice(model);
        //    return json;
        //}

        [HttpPost]
        [AllowAnonymous]
        [Route("Tracuu2/GetInfoInvoice")]
        public async Task<JObject> GetInfoInvoice(JObject model)
        {
            JObject json = await _tracuuService2.GetInfoInvoice(model);
            return json;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Customer/saveXml")]
        public JObject saveXml(JObject model)
        {
            JObject result = new JObject();
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("Content-Type", "application/json");
                client.Encoding = System.Text.Encoding.UTF8;
                string resAPI = client.UploadString(model["urlHost"].ToString() + "/api/Invoice/SignConfirm", model.ToString());
                result = JObject.Parse(resAPI);

            }
            catch (Exception e)
            {
                result.Add("error", e.Message);
            }
            return result;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Customer/saveXml78")]
        public JObject saveXml78(JObject model)
        {
            JObject result = new JObject();
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("Content-Type", "application/json");
                client.Encoding = System.Text.Encoding.UTF8;
                string resAPI = client.UploadString(model["urlHost"].ToString() + "/api/Invoice68/SignConfirm", model.ToString());
                result = JObject.Parse(resAPI);

            }
            catch (Exception e)
            {
                result.Add("error", e.Message);
            }
            return result;
        }

        [HttpPost]
        [Route("Tracuu2/PrintInvoice")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PrintInvoice(JObject model)
        {

            HttpResponseMessage result = null;
            try
            {
                string type = model["type"].ToString();
                string sobaomat = model["sobaomat"].ToString();
                string masothue = model["masothue"].ToString();
                //if (_tracuuService2 == null)
                //{
                //    throw new Exception("Không tồn tại mst:");
                //}
                //string type = "PDF";
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";
                //string path = "~/Content/report/";
                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                byte[] bytes = await _tracuuService2.PrintInvoiceFromSBM(sobaomat, masothue, folder, type);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytes);

                if (type == "PDF")
                {
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
                    result.Content.Headers.ContentDisposition.FileName = "InvoiceTemplate.pdf";
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (type == "Html")
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.BadRequest);
                result.Content = new StringContent(ex.Message, System.Text.Encoding.UTF8);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }
        [HttpPost]
        [Route("Tracuu2/ExportZipFileXML")]
        public HttpResponseMessage ExportZipFileXML(JObject model)
        {

            HttpResponseMessage result = null;
            
            try
            {
                string masothue = model["masothue"].ToString();
                string sobaomat = model["sobaomat"].ToString();
                string originalString = this.ActionContext.Request.RequestUri.OriginalString;
                string path = originalString.StartsWith("/api") ? "~/api/Content/report/" : "~/Content/report/";

                var folder = System.Web.HttpContext.Current.Server.MapPath(path);

                string fileName = "";
                string key = "";
                byte[] bytes = _tracuuService2.ExportZipFileXML(sobaomat, masothue, folder, ref fileName, ref key);

                result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(bytes)
                };
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach")
                {
                    FileName = fileName + ".zip"
                };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
                result.Content.Headers.ContentLength = bytes.Length;
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.BadRequest);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
            }

            return result;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Account/ForgotPassword")]
        public async Task<JObject> ForgotPassword(JObject input)
        {
            string masothue = input["masothue"].ToString();
            string email = input["email"].ToString();
            JObject result =await _tracuuService2.SendEmail(masothue,email);
            return result;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("Account/Register")]
        public async Task<JObject> AccountRegister(JObject input)
        {
            string masothue = input["masothue"].ToString();
            string email = input["email"].ToString();
            string name = input["name"].ToString();
            JObject result = await _tracuuService2.RegisterAcount(masothue, email, name);
            return result;
        }
    }
}
