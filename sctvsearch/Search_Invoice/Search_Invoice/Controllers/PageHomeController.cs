﻿using Newtonsoft.Json.Linq;
using Search_Invoice.Data;
using Search_Invoice.Data.Domain;
using Search_Invoice.Models;
using Search_Invoice.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using Search_Invoice.DAL;
using LicenseContext = System.ComponentModel.LicenseContext;
using Search_Invoice.Util;

namespace Search_Invoice.Controllers
{
    public class PageHomeController : Controller
    {
        private INopDbContext _nopDbContext;

        // GET: PageHome

        [AllowAnonymous]
        public ActionResult PageHomeIndex()
        {
            return View();
        }

        [AllowAnonymous]
        public PartialViewResult PageHomeHeader()
        {
            return PartialView("PageHomeHeader");
        }
        [AllowAnonymous]
        public ActionResult ExportExcel()
        {
            var us = (inv_user_tax)Session[CommonConstants.USER_SESSION_TAX];
            if (us == null)
                return RedirectToAction("LoginTax", "Account");
            return View();
        }
        [DeleteFileDown]
        [HttpGet]
        public FileResult GetFile(string path)
        {
            var result = new FilePathResult(path, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = new FileInfo(path).Name
            };
            return result;
        }
        [HttpPost]
        public JsonResult GetDataCompany()
        {
            var dbContext = new TracuuHDDTContext();
            var list = dbContext.company.Select(c => new { c.taxcode, c.name });
            return Json(new { data = list, MaxJsonLength = int.MaxValue }, JsonRequestBehavior.AllowGet);
        }

        private const int RecordsPerPage = 10048575;

        [HttpPost]
        public async Task<JsonResult> ExportAll(string startDate, string endDate)
        {
            try
            {
                var us = (inv_user_tax)Session[CommonConstants.USER_SESSION_TAX];
                if (us == null)
                    return Json(JsonConvert.SerializeObject(new { status = 500 }), JsonRequestBehavior.AllowGet);
                var dbContext = new TracuuHDDTContext();
                //select * from reg.getcntmaster('2020-10-24','2020-12-30')
                DateTime dt;
                if (DateTime.TryParseExact(startDate, "yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.DateTimeStyles.None, out dt))
                    startDate = dt.ToString("yyyy-MM-dd");
                if (DateTime.TryParseExact(endDate, "yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.DateTimeStyles.None, out dt))
                    endDate = dt.ToString("yyyy-MM-dd");
                var schemaTest = ConfigurationManager.AppSettings["schema_test"];

                string sqlMaster = "SELECT * FROM #SCHEMA_NAME#.getcntmaster(@startDate,@endDate,@schemaTest)";
                string sqlDetail = "SELECT * FROM #SCHEMA_NAME#.getcntdetail(@startDate,@endDate,@schemaTest)";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("startDate", startDate);
                parameters.Add("endDate", endDate);
                parameters.Add("schemaTest", schemaTest);

                var cntMaster = Convert.ToInt32((await dbContext.GetDataTableAsync(us.schema_url, "reg", sqlMaster, parameters)).Rows[0][0].ToString());

                var cntDetail = Convert.ToInt32((await dbContext.GetDataTableAsync(us.schema_url, "reg", sqlDetail, parameters)).Rows[0][0].ToString());

                var ds = new DataSet();
                var pageMaster = (cntMaster + RecordsPerPage - 1) / RecordsPerPage;
                var pageDetail = (cntDetail + RecordsPerPage - 1) / RecordsPerPage;
                var headers = HeaderMasters();
                var headersDetail = HeaderDetails();
                for (var i = 1; i <= pageMaster; i++)
                {

                    string sqlDataMaster = "SELECT * FROM #SCHEMA_NAME#.getdatamaster(@records,@RecordsPerPage,@startDate,@endDate,@schemaTest)";

                    parameters = new Dictionary<string, object>();
                    parameters.Add("records", (i - 1) * RecordsPerPage);
                    parameters.Add("RecordsPerPage", RecordsPerPage);
                    parameters.Add("startDate", startDate);
                    parameters.Add("endDate", endDate);
                    parameters.Add("schemaTest", schemaTest);

                    var d = await dbContext.GetDataTableAsync(us.schema_url, "reg", sqlDataMaster, parameters);
                    d.TableName = "HOADON_MASTER" + (pageMaster > 1 ? $"{i}" : "");
                    d.Columns.Remove("schema_name");
                    //var cols = d.Columns.Cast<DataColumn>().Where(c=>c.ColumnName != "schema_name").ToArray();
                    for (var c = 0; c < d.Columns.Count; c++)
                        d.Columns[c].ColumnName = headers[c];
                    d.AcceptChanges();
                    ds.Tables.Add(d);

                }

                for (var i = 1; i <= pageDetail; i++)
                {

                    string sqlDataDetail = "SELECT * FROM #SCHEMA_NAME#.getdatadetail(@records,@RecordsPerPage,@startDate,@endDate,@schemaTest)";

                    parameters = new Dictionary<string, object>();
                    parameters.Add("records", (i - 1) * RecordsPerPage);
                    parameters.Add("RecordsPerPage", RecordsPerPage);
                    parameters.Add("startDate", startDate);
                    parameters.Add("endDate", endDate);
                    parameters.Add("schemaTest", schemaTest);

                    var d = await dbContext.GetDataTableAsync(us.schema_url, "reg", sqlDataDetail, parameters);
                    d.TableName = "HOADON_DETAIL" + (pageDetail > 1 ? $"{i}" : "");
                    d.Columns.Remove("schema_name");
                    //var cols = d.Columns.Cast<DataColumn>().Where(c=>c.ColumnName != "schema_name").ToArray();
                    for (var c = 0; c < d.Columns.Count; c++)
                        d.Columns[c].ColumnName = headersDetail[c];
                    d.AcceptChanges();
                    ds.Tables.Add(d);
                }

                var a = from DataColumn d in ds.Tables[0].Columns select d;
                var file = System.Web.HttpContext.Current.Server.MapPath($"~/Content/report/ExportData_{DateTime.Now:yyyyMMddhhmmss}.xlsx");
                // var file = $"ExportData_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

                using (var excel = new ExcelPackage(new FileInfo(file)))
                {
                    for (var i = 0; i < ds.Tables.Count; i++)
                    {
                        var ws = excel.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                        var dateColumns = from DataColumn d in ds.Tables[i].Columns
                                          where d.DataType == typeof(DateTime) || d.ColumnName.Contains("Date")
                                          select d.Ordinal + 1;
                        var decimalColumns = from DataColumn d in ds.Tables[i].Columns
                                             where d.DataType == typeof(decimal)
                                             select d.Ordinal + 1;
                        ws.View.FreezePanes(2, 1);
                        ws.Cells[1, 1, 1, ds.Tables[i].Columns.Count].AutoFilter = true;

                        ws.Cells.LoadFromDataTable(ds.Tables[i], true);
                        ws.Cells.AutoFitColumns();
                        //format date
                        foreach (var d in dateColumns)
                        {
                            ws.Column(d).Style.Numberformat.Format = "dd/mm/yyyy";
                        }
                        //format number
                        foreach (var d in decimalColumns)
                        {
                            ws.Column(d).Style.Numberformat.Format = "0.0000";
                        }
                        ws.Column(13).Style.Numberformat.Format = "0.00"; // column Tỷ giá
                        // column số thứ tự
                        if (ds.Tables[i].TableName.Contains("HOADON_DETAIL"))
                            ws.Column(8).Style.Numberformat.Format = "0";
                        if (ds.Tables[i].TableName.Contains("HOADON_MASTER"))
                            ws.Column(8).Style.Numberformat.Format = "@";
                    }
                    await excel.SaveAsync();
                }

                return Json(JsonConvert.SerializeObject(new
                {
                    status = 200,
                    path = file
                }), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(JsonConvert.SerializeObject(new
                {
                    status = 400,
                    e.Message
                }), JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public async Task<JsonResult> GetData(string startDate, string endDate, int page, int pageSize)
        {
            //string startDate = "", endDate= "";
            DateTime dt;
            if (DateTime.TryParseExact(startDate, "yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out dt))
                startDate = dt.ToString("yyyy-MM-dd");
            if (DateTime.TryParseExact(endDate, "yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out dt))
                endDate = dt.ToString("yyyy-MM-dd");
            try
            {
                var schemaTest = ConfigurationManager.AppSettings["schema_test"];
                var us = (inv_user_tax)Session[CommonConstants.USER_SESSION_TAX];
                if (us == null)
                    return Json(JsonConvert.SerializeObject(new { status = 500 }), JsonRequestBehavior.AllowGet);
                var dbContext = new TracuuHDDTContext();

                string sqlCntMaster = "SELECT * FROM #SCHEMA_NAME#.getcntmaster(@startDate,@endDate,@schemaTest)";

                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("startDate", startDate);
                param1.Add("endDate", endDate);
                param1.Add("schemaTest", schemaTest);

                string sqlDataMaster = "SELECT * FROM #SCHEMA_NAME#.getdatamaster(@records,@pageSize,@startDate,@endDate,@schemaTest)";

                Dictionary<string, object> param2 = new Dictionary<string, object>();
                param2.Add("records", (page - 1) * pageSize);
                param2.Add("pageSize", pageSize);
                param2.Add("startDate", startDate);
                param2.Add("endDate", endDate);
                param2.Add("schemaTest", schemaTest);

                var dtCnt = await dbContext.GetDataTableAsync(us.schema_url, "reg", sqlCntMaster, param1);
                var data = await dbContext.GetDataTableAsync(us.schema_url, "reg", sqlDataMaster, param2);

                if (data.Rows.Count <= 0)
                    return Json(JsonConvert.SerializeObject(new { status = 300, Message = "Không tìm thấy dữ liệu." }), JsonRequestBehavior.AllowGet);
                var list = Enumerable.Cast<object>((data.AsEnumerable()
                        .Select(rw => new
                        {
                            inv_invoiceauth_id = rw["inv_invoiceauth_id"].ToString(),
                            security_number = rw["security_number"].ToString(),
                            invoice_type = rw["invoice_type"].ToString(),
                            invoice_name = rw["invoice_name"].ToString(),
                            template_code = rw["template_code"].ToString(),
                            invoice_series = rw["invoice_series"].ToString(),
                            invoice_number = rw["invoice_number"].ToString(),
                            invoice_status = rw["invoice_status"].ToString(),
                            invoice_issued_date = !string.IsNullOrWhiteSpace(rw["invoice_issued_date"].ToString()) ? Convert.ToDateTime(rw["invoice_issued_date"].ToString()).ToString("dd/MM/yyyy") : "",
                            print_convert = rw["print_convert"].ToString(),
                            date_convert = !string.IsNullOrWhiteSpace(rw["date_convert"].ToString()) ? Convert.ToDateTime(rw["date_convert"].ToString()).ToString("dd/MM/yyyy") : "",
                            currency_code = rw["currency_code"].ToString(),
                            exchange_rate = rw["exchange_rate"].ToString(),
                            name = rw["name"].ToString(),
                            tax_code = rw["tax_code"].ToString(),
                            address = rw["address"].ToString(),
                            buyer_display_name = rw["buyer_display_name"].ToString(),
                            buyer_taxcode = rw["buyer_taxcode"].ToString(),
                            buyer_address_line = rw["buyer_address_line"].ToString(),
                            total_amount_without_vat = rw["total_amount_without_vat"].ToString(),
                            vat_amount = rw["vat_amount"].ToString(),
                            total_phi = rw["total_phi"].ToString(),
                            total_amount = rw["total_amount"].ToString(),
                            amount_to_word = rw["amount_to_word"].ToString(),
                            signed = rw["signed"].ToString(),
                            signed_date = !string.IsNullOrWhiteSpace(rw["signed_date"].ToString()) ? Convert.ToDateTime(rw["signed_date"].ToString()).ToString("dd/MM/yyyy") : "",
                            chuky = rw["chuky"].ToString(),
                            ngaymua = !string.IsNullOrWhiteSpace(rw["ngaymua"].ToString()) ? Convert.ToDateTime(rw["ngaymua"].ToString()).ToString("dd/MM/yyyy") : "",
                            mstncc = rw["mstncc"].ToString(),
                            codepm = rw["codepm"].ToString(),
                            mahoadonxuly = rw["mahoadonxuly"].ToString(),
                            ngayhoadonxuly = !string.IsNullOrWhiteSpace(rw["ngayhoadonxuly"].ToString()) ? Convert.ToDateTime(rw["ngayhoadonxuly"].ToString()).ToString("dd/MM/yyyy") : "",
                            lydo = rw["lydo"].ToString(),
                            congtracuu = rw["congtracuu"].ToString(),
                            schema_name = rw["schema_name"].ToString()
                        })))
                    .ToList();
                var headers = data.Columns.Cast<DataColumn>()
                    .Where(w => w.ColumnName != "schema_name")
                    .Select(x => x.ColumnName)
                    .ToArray();
                var model = new Pager(Convert.ToInt32(dtCnt.Rows[0][0].ToString()), page, pageSize);
                var json = JsonConvert.SerializeObject(new { status = 200, data = list, pager = RenderRazorViewToString("_Pager", model), headers }, Formatting.None);
                return Json(json, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(JsonConvert.SerializeObject(new { status = 400, e.Message }), JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public async Task<JsonResult> GetDataDetail(string schema_name, string inv_invoiceauth_id)
        {
            try
            {
                var us = (inv_user_tax)Session[CommonConstants.USER_SESSION_TAX];
                if (us == null)
                    return Json(JsonConvert.SerializeObject(new { status = 500 }), JsonRequestBehavior.AllowGet);
                var dbContext = new TracuuHDDTContext();
                var data = await GetDataDetailExport(dbContext, us, schema_name, inv_invoiceauth_id);
                if (data.Rows.Count <= 0)
                    return Json(JsonConvert.SerializeObject(new { status = 300, Message = "Không tìm thấy dữ liệu." }), JsonRequestBehavior.AllowGet);
                var headers = data.Columns.Cast<DataColumn>()
                    .Select(x => x.ColumnName)
                    .ToArray();
                var list = (from rw in data.AsEnumerable()
                            select new
                            {
                                inv_invoiceauth_id = rw["inv_invoiceauth_id"].ToString(),
                                codepm = rw["codepm"].ToString(),
                                invoice_type = rw["invoice_type"].ToString(),
                                template_code = rw["template_code"].ToString(),
                                invoice_series = rw["invoice_series"].ToString(),
                                invoice_number = rw["invoice_number"].ToString(),
                                tax_code = rw["tax_code"].ToString(),
                                row_ord = rw["row_ord"].ToString(),
                                item_name = rw["item_name"].ToString(),
                                unit_name = rw["unit_name"].ToString(),
                                quantity = rw["quantity"].ToString(),
                                unit_price = rw["unit_price"].ToString(),
                                discount_percentage = rw["discount_percentage"].ToString(),
                                discount_amount = rw["discount_amount"].ToString(),
                                total_amount_without_vat = rw["total_amount_without_vat"].ToString(),
                                tax_type = rw["tax_type"].ToString(),
                                vat_amount = rw["vat_amount"].ToString(),
                                total_amount = rw["total_amount"].ToString(),
                                name_phi = rw["name_phi"].ToString(),
                                amount_phi = rw["amount_phi"].ToString()
                            }).ToList();
                var json = JsonConvert.SerializeObject(new { status = 200, data = list, headers }, Formatting.None);
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(JsonConvert.SerializeObject(new { status = 400, e.Message }), JsonRequestBehavior.AllowGet);
            }
        }

        //public async Task<DataTable> GetDataMasterExport(TracuuHDDTContext dbContext, string schema, int page, int record, bool isCnt)
        //{
        //    var sqlMaster = new StringBuilder();
        //    var headers = new List<string>();
        //    DataTable dt = null;
        //    sqlMaster.Append("SELECT ");
        //    try
        //    {
        //        if (isCnt)
        //        {
        //            sqlMaster.Append(" Count(1) as cnt");
        //            sqlMaster.Append(" FROM #SCHEMA_NAME#.inv_invoiceauth inv");
        //            sqlMaster.Append(" INNER JOIN #SCHEMA_NAME#.wb_branch d ON inv.branch_code = d.code");
        //            dt = await dbContext.GetDataTableAsync(us.schema_url, schema, sqlMaster.ToString());
        //        }
        //        else
        //        {
        //            sqlMaster.Append("inv.inv_invoiceauth_id,");
        //            headers.Add("Mã hóa đơn");

        //            sqlMaster.Append("inv.security_number,");
        //            headers.Add("Mã tra cứu hóa đơn");

        //            sqlMaster.Append("inv.invoice_type,");
        //            headers.Add("Loại hóa đơn");

        //            sqlMaster.Append("inv.invoice_name,");
        //            headers.Add("Tên loại hóa đơn");

        //            sqlMaster.Append("inv.template_code,");
        //            headers.Add("Ký hiệu mẫu số hóa đơn");

        //            sqlMaster.Append("inv.invoice_series,");
        //            headers.Add("Ký hiệu hóa đơn");

        //            sqlMaster.Append("inv.invoice_number,");
        //            headers.Add("Số hóa đơn");

        //            sqlMaster.Append("inv.invoice_status,");
        //            headers.Add("Trạng thái hóa đơn");

        //            sqlMaster.Append("TO_CHAR(inv.invoice_issued_date, 'DD/MM/YYYY') as invoice_issued_date,");
        //            headers.Add("Thời điểm lập");

        //            sqlMaster.Append("inv.print_convert,");
        //            headers.Add("Hóa đơn đã in chuyển đổi");

        //            sqlMaster.Append("TO_CHAR(inv.date_convert, 'DD/MM/YYYY') as date_convert,");
        //            headers.Add("Thời điểm in chuyển đổi");

        //            sqlMaster.Append("inv.currency_code,");
        //            headers.Add("Đơn vị tiền tệ");

        //            sqlMaster.Append("inv.exchange_rate,");
        //            headers.Add("Tỷ giá");

        //            sqlMaster.Append("d.name,");
        //            headers.Add("Tên người bán");

        //            sqlMaster.Append("d.tax_code,");
        //            headers.Add("Mã số thuế (người bán)");

        //            sqlMaster.Append("d.address,");
        //            headers.Add("Địa chỉ ");

        //            sqlMaster.Append("inv.buyer_display_name,");
        //            headers.Add("Tên người mua");

        //            sqlMaster.Append("inv.buyer_taxcode,");
        //            headers.Add("Mã số thuế (người mua)");

        //            sqlMaster.Append("inv.buyer_address_line,");
        //            headers.Add("Địa chỉ");

        //            sqlMaster.Append("inv.total_amount_without_vat,");
        //            headers.Add("Tổng tiền hàng (Tổng cộng thành tiền chưa có thuế GTGT)");

        //            sqlMaster.Append("inv.vat_amount,");
        //            headers.Add("Tổng tiền thuế (Tổng cộng tiền thuế GTGT)");

        //            //sqlMaster.Append("--TongTienPhi");

        //            sqlMaster.Append("inv.total_amount,");
        //            headers.Add("Tổng tiền thanh toán bằng số");

        //            sqlMaster.Append("inv.amount_to_word,");
        //            headers.Add("Tổng tiền thanh toán bằng chữ");

        //            sqlMaster.Append(@"(SELECT unnest(xpath('//ds:Signature[@Id=""seller""]/ds:KeyInfo/ds:X509Data/ds:X509Certificate/text()', data ,ARRAY[ARRAY['ds', 'http://www.w3.org/2000/09/xmldsig#']])) 
        //                        FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id = inv.inv_invoiceauth_id LIMIT 1) as signed,");
        //            headers.Add("Chữ ký số người bán");

        //            sqlMaster.Append("TO_CHAR(inv.signed_date, 'DD/MM/YYYY') as signed_date,");
        //            headers.Add("Ngày người bán ký");

        //            sqlMaster.Append("'Chukynguoimua' as chuky,");
        //            headers.Add("Chữ ký số người mua");

        //            sqlMaster.Append($"'{DateTime.Now:dd-MM-yyyy}' as ngaymua,");
        //            headers.Add("Ngày người mua ký");

        //            sqlMaster.Append("'MInvoice' as MSTNCC,");
        //            headers.Add("Nhà cung cấp giải pháp phần mềm (MST NCC)");

        //            sqlMaster.Append(" 'MInvoice' as CodePM,");
        //            headers.Add("Mã hiệu phần mềm tạo hóa đơn");

        //            sqlMaster.Append("inv.original_invoice_id as MaHoaDonXuLy,");
        //            headers.Add("Mã hóa đơn sau xử lý");

        //            sqlMaster.Append("(SELECT TO_CHAR(inv.invoice_issued_date, 'DD/MM/YYYY') FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id = inv.original_invoice_id) as NgayHoaDonXuLy,");
        //            headers.Add("Ngày sau xử lý hóa đơn");

        //            sqlMaster.Append("(SELECT additional_reference_note FROM vienthong1611.inv_invoiceauth WHERE inv_invoiceauth_id = inv.original_invoice_id) as Lydo,");
        //            headers.Add("Lý do xử lý");

        //            sqlMaster.Append("'http://tracuuhoadon.mobifoneinvoice.vn' as CongTraCuu");
        //            headers.Add("Cổng tra cứu hóa đơn");

        //            sqlMaster.Append(" FROM #SCHEMA_NAME#.inv_invoiceauth inv");
        //            sqlMaster.Append(" INNER JOIN #SCHEMA_NAME#.wb_branch d ON inv.branch_code = d.code");
        //            sqlMaster.Append(" ORDER BY inv_invoiceauth_id ,invoice_series");
        //            sqlMaster.Append(" OFFSET @OFFSET");
        //            sqlMaster.Append(" LIMIT @LIMIT");

        //            Dictionary<string, object> parameters = new Dictionary<string, object>();
        //            parameters.Add("OFFSET", (page - 1) * record);
        //            parameters.Add("LIMIT", record);

        //            dt = await dbContext.GetDataTableAsync(us.schema_url, schema, sqlMaster.ToString(), parameters);
        //            dt.TableName = "HOADON_MASTER" + (page == 1 ? "" : page.ToString());
        //            for (var i = 0; i < dt.Columns.Count; i++)
        //                dt.Columns[i].ColumnName = headers[i];
        //            dt.AcceptChanges();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    return dt;
        //}

        private static async Task<DataTable> GetDataDetailExport(TracuuHDDTContext dbContext, inv_user_tax us, string schema, string inv_invoiceauth_id)
        {
            var sqlDetail = new StringBuilder();
            DataTable dt = null;
            try
            {
                string sql = "SELECT * FROM #SCHEMA_NAME#.getdatadetail_invoice(@inv_invoiceauth_id) ";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("inv_invoiceauth_id", Guid.Parse(inv_invoiceauth_id));
                dt = await dbContext.GetDataTableAsync(us.schema_url, "reg", sql, parameters);
            }
            catch (Exception e)
            {
                throw e;
            }
            return dt;
        }

        private static List<string> HeaderMasters()
        {
            var headers = new List<string>();
            headers.Add("Mã hóa đơn");

            //sqlMaster.Append("inv.security_number,");
            headers.Add("Mã tra cứu hóa đơn");

            //sqlMaster.Append("inv.invoice_type,");
            headers.Add("Loại hóa đơn");

            //sqlMaster.Append("inv.invoice_name,");
            headers.Add("Tên loại hóa đơn");

            //sqlMaster.Append("inv.template_code,");
            headers.Add("Ký hiệu mẫu số hóa đơn");

            //sqlMaster.Append("inv.invoice_series,");
            headers.Add("Ký hiệu hóa đơn");

            //sqlMaster.Append("inv.invoice_number,");
            headers.Add("Số hóa đơn");

            //sqlMaster.Append("inv.invoice_status,");
            headers.Add("Trạng thái hóa đơn");

            //sqlMaster.Append("TO_CHAR(inv.invoice_issued_date, 'DD/MM/YYYY') as invoice_issued_date,");
            headers.Add("Thời điểm lập");

            //sqlMaster.Append("inv.print_convert,");
            headers.Add("Hóa đơn đã in chuyển đổi");

            //sqlMaster.Append("TO_CHAR(inv.date_convert, 'DD/MM/YYYY') as date_convert,");
            headers.Add("Thời điểm in chuyển đổi");

            //sqlMaster.Append("inv.currency_code,");
            headers.Add("Đơn vị tiền tệ");

            //sqlMaster.Append("inv.exchange_rate,");
            headers.Add("Tỷ giá");

            //sqlMaster.Append("d.name,");
            headers.Add("Tên người bán");

            //sqlMaster.Append("d.tax_code,");
            headers.Add("Mã số thuế (người bán)");

            //sqlMaster.Append("d.address,");
            headers.Add("Địa chỉ ");

            //sqlMaster.Append("inv.buyer_display_name,");
            headers.Add("Tên người mua");

            //sqlMaster.Append("inv.buyer_taxcode,");
            headers.Add("Mã số thuế (người mua)");

            //sqlMaster.Append("inv.buyer_address_line,");
            headers.Add("Địa chỉ");

            //sqlMaster.Append("inv.total_amount_without_vat,");
            headers.Add("Tổng tiền hàng (Tổng cộng thành tiền chưa có thuế GTGT)");

            //sqlMaster.Append("inv.vat_amount,");
            headers.Add("Tổng tiền thuế (Tổng cộng tiền thuế GTGT)");

            //sqlMaster.Append("--TongTienPhi");
            headers.Add("Tổng tiền phí");

            //sqlMaster.Append("inv.total_amount,");
            headers.Add("Tổng tiền thanh toán bằng số");

            //sqlMaster.Append("inv.amount_to_word,");
            headers.Add("Tổng tiền thanh toán bằng chữ");

            //sqlMaster.Append(@"(SELECT unnest(xpath('//ds:Signature[@Id=""seller""]/ds:KeyInfo/ds:X509Data/ds:X509Certificate/text()', data ,ARRAY[ARRAY['ds', 'http://www.w3.org/2000/09/xmldsig#']])) 
            //                    FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id = inv.inv_invoiceauth_id LIMIT 1) as signed,");
            headers.Add("Chữ ký số người bán");

            //sqlMaster.Append("TO_CHAR(inv.signed_date, 'DD/MM/YYYY') as signed_date,");
            headers.Add("Ngày người bán ký");

            //sqlMaster.Append("'Chukynguoimua' as chuky,");
            headers.Add("Chữ ký số người mua");

            //sqlMaster.Append($"'{DateTime.Now:dd-MM-yyyy}' as ngaymua,");
            headers.Add("Ngày người mua ký");

            //sqlMaster.Append("'MInvoice' as MSTNCC,");
            headers.Add("Nhà cung cấp giải pháp phần mềm (MST NCC)");

            //sqlMaster.Append(" 'MInvoice' as CodePM,");
            headers.Add("Mã hiệu phần mềm tạo hóa đơn");

            //sqlMaster.Append("inv.original_invoice_id as MaHoaDonXuLy,");
            headers.Add("Mã hóa đơn sau xử lý");

            //sqlMaster.Append("(SELECT TO_CHAR(inv.invoice_issued_date, 'DD/MM/YYYY') FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id = inv.original_invoice_id) as NgayHoaDonXuLy,");
            headers.Add("Ngày sau xử lý hóa đơn");

            //sqlMaster.Append("(SELECT additional_reference_note FROM vienthong1611.inv_invoiceauth WHERE inv_invoiceauth_id = inv.original_invoice_id) as Lydo,");
            headers.Add("Lý do xử lý");

            //sqlMaster.Append("'http://tracuuhoadon.mobifoneinvoice.vn' as CongTraCuu");
            headers.Add("Cổng tra cứu hóa đơn");
            return headers;
        }

        private static List<string> HeaderDetails()
        {
            var headers = new List<string>();
            //sqlDetail.Append("SELECT ");
            //sqlDetail.Append("inv.inv_invoiceauth_id,");
            headers.Add("Mã hóa đơn");

            //sqlDetail.Append(" 'MInvoice' as CodePM,");
            headers.Add("Mã hiệu phần mềm tạo hóa đơn");

            //sqlDetail.Append("inv.invoice_type,");
            headers.Add("Loại hóa đơn");

            //sqlDetail.Append("inv.template_code,");
            headers.Add("Ký hiệu mẫu số hóa đơn");

            //sqlDetail.Append("inv.invoice_series,");
            headers.Add("Ký hiệu hóa đơn");

            //sqlDetail.Append("inv.invoice_number,");
            headers.Add("Số hóa đơn");

            //sqlDetail.Append("d.tax_code,");
            headers.Add("Mã số thuế (người bán)");

            //sqlDetail.Append("invd.row_ord,");
            headers.Add("Số thứ tự");

            //sqlDetail.Append("invd.item_name,");
            headers.Add("Tên hàng hóa dịch vụ");

            //sqlDetail.Append("invd.unit_name,");
            headers.Add("Đơn vị tính");

            //sqlDetail.Append("invd.quantity,");
            headers.Add("Số lượng");

            //sqlDetail.Append("invd.unit_price,");
            headers.Add("Đơn giá");

            //sqlDetail.Append("invd.discount_percentage,");
            headers.Add("Tỷ lệ % chiết khấu (Trong trường hợp muốn thể hiện thông tin chiết khấu theo cột cho từng hàng hóa, dịch vụ)");

            //sqlDetail.Append("invd.discount_amount,");
            headers.Add("Số tiền chiết khấu (Trong trường hợp muốn thể hiện thông tin chiết khấu theo cột cho từng hàng hóa, dịch vụ)");

            //sqlDetail.Append("invd.total_amount_without_vat,");
            headers.Add("Thành tiền (Thành tiền chưa có thuế GTGT)");

            //sqlDetail.Append("invd.tax_type,");
            headers.Add("Thuế suất (Thuế suất thuế GTGT)");

            //sqlDetail.Append("invd.vat_amount,");
            headers.Add("Tiền thuế (Tiền thuế GTGT)");

            //sqlDetail.Append("invd.total_amount");
            headers.Add("Tiền phải thành toán");
            headers.Add("Tên loại phí");

            headers.Add("Tiền phí");
            return headers;
        }

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                    viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                    ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}