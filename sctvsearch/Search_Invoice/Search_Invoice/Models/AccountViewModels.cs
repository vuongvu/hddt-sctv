﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Search_Invoice.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    //public class LoginViewModel
    //{
    //    [Required]
    //    [Display(Name = "Email")]
    //    // [EmailAddress]
    //    public string Email { get; set; }

    //    [Required]
    //    [DataType(DataType.Password)]
    //    [Display(Name = "Password")]
    //    public string Password { get; set; }

    //    [Display(Name = "Remember me?")]
    //    public bool RememberMe { get; set; }
    //}
    public class LoginViewModel
    {
        //[Required(ErrorMessage = "Vui lòng nhập mã số thuế đơn vị")]
        //[Display(Name = "mst")]
        // [EmailAddress]
        public string mst { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên truy cập")]
        [Display(Name = "username")]
        // [EmailAddress]
        public string username { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [DataType(DataType.Password)]
        [Display(Name = "password")]
        public string password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
    public class LoginTaxViewModel
    {
        //[Required(ErrorMessage = "Vui lòng nhập mã số thuế đơn vị")]
        //[Display(Name = "mst")]
        // [EmailAddress]
        //public string mst { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên truy cập")]
        [Display(Name = "username")]
        // [EmailAddress]
        public string username { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [DataType(DataType.Password)]
        [Display(Name = "password")]
        public string password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
    public class Pager
    {
        public int TotalItems { get; set; }
        public int PageCurrent { get; set; }
        public int NumberPage { get; set; }
        public int Offset { get; set; }
        public Pager(int totalItems, int? page, int pageSize = 100)
        {
            //var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            //var currentPage = page ?? 1;
            //var startPage = currentPage - 5;
            //var endPage = currentPage + 4;
            //if (startPage <= 0)
            //{
            //    endPage -= (startPage - 1);
            //    startPage = 1;
            //}
            //if (endPage > totalPages)
            //{
            //    endPage = totalPages;
            //    if (endPage > 10)
            //    {
            //        startPage = endPage - 9;
            //    }
            //}
            TotalItems = totalItems;
            PageCurrent = page ?? 1;
            NumberPage = (int)Math.Ceiling(totalItems / (decimal)pageSize);
            Offset = pageSize;
        }

    }
}