﻿using System;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml;

namespace Search_Invoice.Util
{
    public class FunctionUtil
    {

        public static string AddXml(string xml, DataTable detail, string[] notGet, string treeName, string cctbao_id, int soDongMau)
        {
            try
            {
                var test = detail.Columns;

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList xmlNode = doc.GetElementsByTagName(treeName);
                for (int i = 0; i < xmlNode.Count; i++)
                {
                    if (i < detail.Rows.Count)
                    {
                        foreach (DataColumn item in test)
                        {
                            var key = item.ColumnName;
                            if (!notGet.Contains(key) && xmlNode[i].SelectSingleNode(key) == null && detail.Rows[i][key] != null)
                            {
                                var giatri = detail.Rows[i][key].ToString();
                                if (item.DataType == typeof(Decimal))
                                {
                                    giatri = DoubleStrToString(giatri);
                                }
                                XmlElement xmlkey = doc.CreateElement(key);
                                xmlkey.InnerText = giatri;
                                xmlNode[i].AppendChild(xmlkey);
                            }
                        }
                    }
                }
                //DataTable tblKyhieu = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + cctbao_id + "'");


                //string kyHieu = await this._invoiceService68.getSoDongMau(cctbao_id);

                XmlNode parent = xmlNode[0].ParentNode;
                for (int i = xmlNode.Count; i < soDongMau; i++)
                {
                    XmlElement xmlkey = doc.CreateElement(treeName);
                    string blankValue = "";
                    xmlkey.InnerText = blankValue;
                    parent.AppendChild(xmlkey);
                }


                return doc.InnerXml.ToString();
            }
            catch
            {
                return xml;
            }
        }

        public static string DoubleStrToString(string strdec)
        {
            try
            {
                if (strdec.Contains("."))
                {
                    string result = strdec.TrimEnd('0').TrimEnd('.');
                    if (result.Length > 0 && result[result.Length - 1] == '.') return result.Substring(0, result.Length - 1);
                    return strdec.Contains(".") ? strdec.TrimEnd('0').TrimEnd('.') : strdec;
                }
                else
                {
                    return strdec;
                }
            }
            catch
            {
                return strdec;
            }
        }
        public static string RandomPassWord()
        {
            return Guid.NewGuid().ToString("d").Substring(1, 8);
        }

        public static string CreateHashedPassword(string password, byte[] existingSalt)
        {
            byte[] data = null;
            if (existingSalt == null)
            {
                Random random = new Random();
                data = new byte[random.Next(0x10, 0x40)];
                new RNGCryptoServiceProvider().GetNonZeroBytes(data);
            }
            else
            {
                data = existingSalt;
            }
            byte[] bytes = Encoding.UTF8.GetBytes(password);
            byte[] array = new byte[bytes.Length + data.Length];
            bytes.CopyTo(array, 0);
            data.CopyTo(array, bytes.Length);
            byte[] buffer4 = new SHA512Managed().ComputeHash(array);
            byte[] buffer5 = new byte[buffer4.Length + data.Length];
            buffer4.CopyTo(buffer5, 0);
            data.CopyTo(buffer5, buffer4.Length);
            return Convert.ToBase64String(buffer5);
        }
    }
}
