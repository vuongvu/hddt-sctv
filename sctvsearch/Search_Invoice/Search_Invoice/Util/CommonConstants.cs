﻿using System.Configuration;

namespace Search_Invoice.Util
{
    public class CommonConstants
    {
 
        public static readonly string MASTER_SCHEMA = ConfigurationManager.AppSettings["MASTER_SCHEMA"].ToString();

    
        //Create KH MOIK
       
        public static readonly string[] WHITE_LIST_COLUMN = { "on_click", "body", "data", "xml_data", "xml", "data_xml", "content",
            "response", "icon_css", "xml_send", "xml_rep", "dlxml", "dlxml_thue", "ly_do", "xml_nhan", "xml_cqt", "dulieumau", "xml_tdiep_gui", "signature" };

        public static string USER_SESSION = "USER_SESSION";
        public static string USER_SESSION_TAX = "USER_SESSION_TAX";
        public static string SCHEMA_TAX_CODE = ConfigurationManager.AppSettings["schema_tax_Code"];
    }
}
