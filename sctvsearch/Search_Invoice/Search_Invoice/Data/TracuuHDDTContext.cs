﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Search_Invoice.Data.Domain;
using System.Data;
using Npgsql;
using System.Threading.Tasks;
using System.Configuration;
using Search_Invoice.Util;

namespace Search_Invoice.Data
{
    public class TracuuHDDTContext : DbContext
    {
        private string _schema;
        public TracuuHDDTContext()
          : base(nameOrConnectionString: "MasterConnection")
        {
            this._schema = CommonConstants.MASTER_SCHEMA;
        }

        public DbSet<inv_user> inv_users { get; set; }
        public DbSet<inv_admin> Inv_admin { get; set; }
        public DbSet<inv_user_tax> inv_user_taxs { get; set; }
        public DbSet<company> company { get; set; }


        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.HasDefaultSchema(this._schema);
            base.OnModelCreating(builder);
        }

        public async Task<DataTable> GetDatatable(string schema_url, string schemaName, string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTable tblData = new DataTable();
            string connection = schema_url;

            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            using (NpgsqlConnection conn = new NpgsqlConnection(connection))
            {
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value;

                            if (CommonConnect.CheckForSqlInjection(entry.Value.ToString()))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key.ToString()} : {entry.Value.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);
                }
            }

            return tblData;
        }
        public async Task<DataTable> GetDataTableAsync(string schema_url, string schemaName, string sql)
        {
            return await GetDatatable(schema_url, schemaName, sql, CommandType.Text, null);
        }

        public async Task<DataTable> GetDataTableAsync(string schema_url, string schemaName, string sql, Dictionary<string, object> parameters)
        {
            return await GetDatatable(schema_url, schemaName, sql, CommandType.Text, parameters);
        }
    }
}