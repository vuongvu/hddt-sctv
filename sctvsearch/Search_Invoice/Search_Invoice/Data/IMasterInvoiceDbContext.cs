﻿using Newtonsoft.Json.Linq;
using Search_Invoice.Util;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Search_Invoice.Data
{
    public partial interface IMasterInvoiceDbContext
    {
        void ExecuteNoneQuery(string sql);
        void ExecuteNoneQuery(string sql, DataRow row);

        Task<string> ExecuteNoneQueryAsync(string sql);
        Task<string> ExecuteNoneQueryAsync(string sql, DataRow row);
        Task<string> ExecuteNoneQueryAsyncNotCheckHTML(string sql, DataRow row);
        Task<string> ExecuteNoneQueryAsync(string sql, Dictionary<string, object> parameters);
        Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, DataRow row);
        Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, JObject obj);
        Task<string> ExecuteNoneQueryAsyncNoneSQLInjection(string sql, Dictionary<string, object> parameters);
        DataTableExtend GetDataTable(string sql, CommandType commandType, Dictionary<string, object> parameters);
        DataTableExtend GetDataTable(string sql, CommandType commandType, params object[] parameters);
        DataTableExtend GetDataTable(string sql);
        Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, params object[] parameters);
        Task<DataTableExtend> GetDataTableAsync(string sql, params object[] parameters);
        Task<DataSet> GetDataSetAsyncPrintf(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataTableExtend> GetDataTableAsync(string sql);
        Task<DataTableExtend> GetTransactionDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<DataSet> GetDataSetAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);

        Task<string> BeginTransactionAsync();
        Task<string> TransactionCommandAsync(string sql, CommandType commandType, Dictionary<string, object> parameters);
        Task<string> TransactionCommandAsync(string sql, CommandType commandType, JObject parameters);
        Task<string> TransactionCommitAsync();
        Task<string> TransactionRollbackAsync();
        void CloseTransaction();
        string GetSchemaName();

    }
}
