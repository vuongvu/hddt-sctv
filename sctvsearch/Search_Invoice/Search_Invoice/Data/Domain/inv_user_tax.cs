﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Search_Invoice.Data.Domain
{
    public class inv_user_tax
    {
        [Key]
        public Guid inv_user_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public DateTime? date_create { get; set; }
        public DateTime? date_edit { get; set; }
        public string schema_url { get; set; }
    }
}