﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Search_Invoice.Data.Domain
{
    public class company
    {
        [Key]
        public Guid taxcode { get; set; }
        public string name { get; set; }
        public string schema_name { get; set; }
    }
}