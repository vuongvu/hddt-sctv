﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Search_Invoice.Services;
using Npgsql;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Configuration;
using Search_Invoice.Util;
using Search_Invoice.Redis;

namespace Search_Invoice.Data
{
    public class MInvoiceDbContext : IMinvoiceDbContext
    {
        #region Fields

        private readonly IWebHelper _webHelper;

        private NpgsqlConnection _npgsqlConnection;
        private NpgsqlTransaction _npgsqlTransaction;
        private string site_hddt;

        #endregion

        #region Ctor

        public MInvoiceDbContext(IWebHelper webHelper)
        {
            this._webHelper = webHelper;
        }

        #endregion

        #region Public Methods
        public void SetSiteHddt(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                site_hddt = CommonConstants.SCHEMA_TAX_CODE;
            }
            else
            {
                site_hddt = CommonConstants.SCHEMA_TAX_CODE;
            }
            //return site_hddt;
        }
        public void ExecuteNoneQuery(string sql)
        {
            this.ExecuteNoneQuery(sql, null);
        }

        public void ExecuteNoneQuery(string sql, DataRow row)
        {
            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            string connectionString = this.GetConnectionTenantString();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (row != null)
                    {
                        DataTable table = row.Table;

                        foreach (DataColumn column in table.Columns)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + column.ColumnName;
                            parameter.Value = row[column.ColumnName];

                            if (CommonManager.CheckForSqlInjection(row[column.ColumnName]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(column.ColumnName.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {column.ColumnName} : {row[column.ColumnName]?.ToString()}");
                            }

                            command.Parameters.Add(parameter);
                        }
                    }

                    //command.ExecuteNonQuery();

                    command.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

        }

        public async Task<string> ExecuteNoneQueryAsync(string sql)
        {
            DataRow row = null;
            return await this.ExecuteNoneQueryAsync(sql, CommandType.Text, row);
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, DataRow row)
        {
            return await this.ExecuteNoneQueryAsync(sql, CommandType.Text, row);
        }

        public async Task<string> ExecuteNoneQueryAsyncNotCheckHTML(string sql, DataRow row)
        {
            return await this.ExecuteNoneQueryAsyncNotCheckHTML(sql, CommandType.Text, row);
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, DataRow row)
        {
            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (row != null)
                    {
                        DataTable table = row.Table;

                        foreach (DataColumn column in table.Columns)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + column.ColumnName;
                            parameter.Value = row[column.ColumnName];

                            if (CommonManager.CheckForSqlInjection(row[column.ColumnName]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(column.ColumnName.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {column.ColumnName} : {row[column.ColumnName]?.ToString()}");
                            }

                            command.Parameters.Add(parameter);
                        }
                    }
                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }


            return "";
        }

        public async Task<string> ExecuteNoneQueryAsyncNotCheckHTML(string sql, CommandType commandType, DataRow row)
        {
            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (row != null)
                    {
                        DataTable table = row.Table;

                        foreach (DataColumn column in table.Columns)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + column.ColumnName;
                            parameter.Value = row[column.ColumnName];

                            if (CommonManager.CheckForSqlInjection(row[column.ColumnName]?.ToString(), false))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {column.ColumnName} : {row[column.ColumnName]?.ToString()}");
                            }

                            command.Parameters.Add(parameter);
                        }
                    }
                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, CommandType commandType, JObject obj)
        {
            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();


            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand())
                {
                    command.Connection = conn;
                    command.CommandType = CommandType.Text;

                    if (commandType == CommandType.StoredProcedure)
                    {
                        sql = sql.Replace("#SCHEMA_NAME#.", "");

                        if (CommonManager.CheckForSqlInjection(sql))
                        {

                            throw new Exception($"Dữ liệu không hợp lệ {sql}");
                        }

                        string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                    + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                    + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "'";

                        DataTable tblParameter = new DataTable();

                        using (NpgsqlCommand cmd = new NpgsqlCommand(query, conn))
                        {
                            cmd.CommandType = CommandType.Text;

                            var reader = await cmd.ExecuteReaderAsync();
                            tblParameter.Load(reader);

                            string parameterName = "";

                            if (tblParameter.Rows.Count > 0)
                            {
                                foreach (DataRow row in tblParameter.Rows)
                                {
                                    string proargnames = row["proargnames"].ToString();
                                    string proargtypes = row["proargtypes"].ToString();

                                    parameterName = parameterName + "@" + proargnames + ",";

                                    string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                    NpgsqlParameter parameter = command.CreateParameter();
                                    parameter.ParameterName = "@" + proargnames;
                                    parameter.NpgsqlValue = DBNull.Value;

                                    if (proargnames == "p_schemaname")
                                    {
                                        parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                        parameter.NpgsqlValue = schemaName;
                                    }
                                    else
                                    {
                                        if (obj != null)
                                        {
                                            if (obj[fieldName] != null)
                                            {

                                                if (proargtypes == "uuid")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Guid.Parse(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "timestamp without time zone")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "date")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "integer")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToInt32(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "numeric")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric;

                                                    if (obj[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDecimal(obj[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "boolean")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                    bool _value = obj[fieldName].ToString() == "1" || obj[fieldName].ToString() == "true" ? true : false;
                                                    parameter.NpgsqlValue = _value;
                                                }
                                                else if (proargtypes == "character varying" || proargtypes == "text")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                    parameter.NpgsqlValue = obj[fieldName].ToString();
                                                }
                                            }
                                        }

                                    }

                                    if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                    {

                                        throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                    }

                                    command.Parameters.Add(parameter);
                                }
                            }

                            if (string.IsNullOrEmpty(parameterName))
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                            }
                            else
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                            }
                        }
                    }
                    else
                    {
                        sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                        command.CommandText = sql;

                        if (obj != null)
                        {
                            foreach (var entry in obj)
                            {
                                string type = entry.Value.Type.ToString();

                                if (type == "Guid")
                                {
                                    command.Parameters.AddWithValue(entry.Key, Guid.Parse(entry.Value.ToString()));
                                }
                                else
                                {
                                    command.Parameters.AddWithValue(entry.Key, entry.Value);
                                }
                            }
                        }
                    }

                    //await command.ExecuteNonQueryAsync();


                    await command.ExecuteNonQueryAsync();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public async Task<string> ExecuteNoneQueryAsync(string sql, Dictionary<string, object> parameters)
        {
            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + entry.Key;
                            parameter.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            //if(entry.Key == "sl_invoice_template_id")
                            //{
                            //    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;
                            //}
                            command.Parameters.Add(parameter);
                        }
                    }

                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public async Task<string> ExecuteNoneQueryAsyncNoneSQLInjection(string sql, Dictionary<string, object> parameters)
        {
            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter parameter = command.CreateParameter();
                            parameter.ParameterName = "@" + entry.Key;
                            parameter.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            command.Parameters.Add(parameter);
                        }
                    }

                    //await command.ExecuteNonQueryAsync();

                    await command.ExecuteNonQueryAsync();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return "";
        }

        public DataTableExtend GetDataTable(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = this.GetConnectionTenantString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    //var reader = command.ExecuteReader();
                    //tblData.Load(reader);

                    var reader = command.ExecuteReader();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public DataTableExtend GetDataTable(string sql, CommandType commandType, params object[] parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = this.GetConnectionTenantString();

            string schemaName = this.GetSchemaName();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@P_" + i.ToString();
                        npgsqlParam.Value = parameters[i];

                        if (CommonManager.CheckForSqlInjection(parameters[i]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains("p_" + i.ToString().ToLower())))
                        {

                            throw new Exception($"Dữ liệu không hợp lệ {parameters[i]} : {parameters[i]?.ToString()}");
                        }

                        command.Parameters.Add(npgsqlParam);
                    }


                    var reader = command.ExecuteReader();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }


            return tblData;
        }

        public DataTableExtend GetDataTable(string sql)
        {
            return this.GetDataTable(sql, CommandType.Text, (Dictionary<string, object>)null);
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);


            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    //var reader = await command.ExecuteReaderAsync();
                    //tblData.Load(reader);

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql, CommandType commandType, params object[] parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@P_" + i.ToString();
                        npgsqlParam.Value = parameters[i];

                        if (CommonManager.CheckForSqlInjection(parameters[i]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains("p_" + i.ToString().ToLower())))
                        {


                            throw new Exception($"Dữ liệu không hợp lệ {parameters[i]} : {parameters[i]?.ToString()}");
                        }


                        command.Parameters.Add(npgsqlParam);
                    }

                    //var reader = await command.ExecuteReaderAsync();
                    //tblData.Load(reader);

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql, params object[] parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@P_" + i.ToString();
                        npgsqlParam.Value = parameters[i];

                        if (CommonManager.CheckForSqlInjection(parameters[i]?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains("p_" + i.ToString().ToLower())))
                        {

                            throw new Exception($"Dữ liệu không hợp lệ {parameters[i]} : {parameters[i]?.ToString()}");
                        }

                        command.Parameters.Add(npgsqlParam);
                    }

                    //var reader = await command.ExecuteReaderAsync();
                    //tblData.Load(reader);

                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTableExtend> GetDataTableAsync(string sql)
        {
            return await this.GetDataTableAsync(sql, CommandType.Text, (Dictionary<string, object>)null);
        }

        public async Task<DataSet> GetDataSetAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "dataSet1";

            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            //sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (commandType == CommandType.StoredProcedure)
                    {
                        sql = sql.Replace("#SCHEMA_NAME#.", "");

                        if (CommonManager.CheckForSqlInjection(sql))
                        {
                            throw new Exception($"Dữ liệu không hợp lệ {sql}");
                        }

                        string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                    + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                    + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "' AND p.proargtypes IS NOT NULL";


                        DataTable tblParameter = new DataTable();

                        using (NpgsqlCommand cmd = new NpgsqlCommand(query, conn))
                        {

                            cmd.CommandType = CommandType.Text;

                            var reader = await cmd.ExecuteReaderAsync();
                            tblParameter.Load(reader);

                            string parameterName = "";

                            if (tblParameter.Rows.Count > 0)
                            {
                                foreach (DataRow row in tblParameter.Rows)
                                {
                                    string proargnames = row["proargnames"].ToString();
                                    string proargtypes = row["proargtypes"].ToString();

                                    if (proargtypes.Length == 0)
                                    {
                                        continue;
                                    }

                                    parameterName = parameterName + "@" + proargnames + ",";

                                    string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                    NpgsqlParameter parameter = command.CreateParameter();
                                    parameter.ParameterName = "@" + proargnames;
                                    parameter.NpgsqlValue = DBNull.Value;



                                    if (proargnames == "p_schemaname")
                                    {
                                        parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                        parameter.NpgsqlValue = schemaName;
                                    }
                                    else
                                    {
                                        if (parameters != null)
                                        {
                                            if (parameters.ContainsKey(fieldName))
                                            {

                                                if (proargtypes == "uuid")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Guid.Parse(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "timestamp without time zone")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "date")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "integer")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                    if (parameters[fieldName].ToString() != "")
                                                    {
                                                        parameter.NpgsqlValue = Convert.ToInt32(parameters[fieldName].ToString());
                                                    }
                                                }
                                                else if (proargtypes == "boolean")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                    bool _value = parameters[fieldName].ToString() == "1" || parameters[fieldName].ToString() == "true" ? true : false;
                                                    parameter.NpgsqlValue = _value;
                                                }
                                                else if (proargtypes == "character varying" || proargtypes == "text")
                                                {
                                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                    parameter.NpgsqlValue = parameters[fieldName].ToString();
                                                }
                                            }
                                        }

                                    }

                                    if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                    {

                                        throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                    }

                                    command.Parameters.Add(parameter);
                                }
                            }

                            if (string.IsNullOrEmpty(parameterName))
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                            }
                            else
                            {
                                command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                            }
                        }
                    }
                    else
                    {
                        sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                        command.CommandText = sql;

                        if (parameters != null)
                        {
                            foreach (var entry in parameters)
                            {

                                if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                                {

                                    throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                                }

                                command.Parameters.AddWithValue(entry.Key, entry.Value == null ? DBNull.Value : entry.Value);
                            }
                        }
                    }

                    var reader1 = await command.ExecuteReaderAsync();
                    DataTable table = new DataTable();
                    table.TableName = "Table";

                    do
                    {
                        table.Load(reader1);

                    } while (!reader1.IsClosed);

                    ds.Tables.Add(table);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }


            return ds;
        }
        public async Task<DataSet> GetDataSetAsyncPrintf(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "dataSet1";

            string connectionString = await this.GetConnectionTenantAsyncString();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = commandType;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = entry.Key;

                            npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }
                    NpgsqlParameter npgsqlParam2 = command.CreateParameter();
                    npgsqlParam2.ParameterName = "p_schemaname";
                    npgsqlParam2.Value = schemaName;
                    npgsqlParam2.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text;
                    command.Parameters.Add(npgsqlParam2);
                    var reader = command.ExecuteReader();
                    DataTable table = new DataTable();
                    table.TableName = "Table";

                    do
                    {
                        table.Load(reader);

                    } while (!reader.IsClosed);

                    ds.Tables.Add(table);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return ds;
        }
        public async Task<string> BeginTransactionAsync()
        {
            string connectionString = await this.GetConnectionTenantAsyncString();

            _npgsqlConnection = new NpgsqlConnection(connectionString);
            await _npgsqlConnection.OpenAsync();

            _npgsqlTransaction = _npgsqlConnection.BeginTransaction();

            return "";
        }

        public async Task<string> TransactionCommandAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            string schemaName = await this.GetSchemaNameAsync();

            using (var command = new NpgsqlCommand())
            {
                command.Connection = _npgsqlConnection;
                command.CommandType = CommandType.Text;
                command.Transaction = _npgsqlTransaction;

                if (commandType == CommandType.StoredProcedure)
                {
                    sql = sql.Replace("#SCHEMA_NAME#.", "");

                    if (CommonManager.CheckForSqlInjection(sql))
                    {
                        throw new Exception($"Dữ liệu không hợp lệ {sql}");
                    }

                    string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "'";


                    DataTable tblParameter = new DataTable();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(query, _npgsqlConnection))
                    {

                        cmd.CommandType = CommandType.Text;

                        var reader = await cmd.ExecuteReaderAsync();
                        tblParameter.Load(reader);

                        string parameterName = "";

                        if (tblParameter.Rows.Count > 0)
                        {
                            foreach (DataRow row in tblParameter.Rows)
                            {
                                string proargnames = row["proargnames"].ToString();
                                string proargtypes = row["proargtypes"].ToString();

                                parameterName = parameterName + "@" + proargnames + ",";

                                string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                NpgsqlParameter parameter = command.CreateParameter();
                                parameter.ParameterName = "@" + proargnames;
                                parameter.NpgsqlValue = DBNull.Value;

                                if (proargnames == "p_schemaname")
                                {
                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                    parameter.NpgsqlValue = schemaName;
                                }
                                else
                                {
                                    if (parameters != null)
                                    {
                                        if (parameters.ContainsKey(fieldName))
                                        {

                                            if (proargtypes == "uuid")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Guid.Parse(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "timestamp without time zone")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "date")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "integer")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToInt32(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "boolean")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                bool _value = parameters[fieldName].ToString() == "1" || parameters[fieldName].ToString() == "true" ? true : false;
                                                parameter.NpgsqlValue = _value;
                                            }
                                            else if (proargtypes == "character varying" || proargtypes == "text")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                parameter.NpgsqlValue = parameters[fieldName].ToString();
                                            }
                                        }
                                    }

                                }

                                if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                {
                                    throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                }

                                command.Parameters.Add(parameter);
                            }
                        }

                        if (string.IsNullOrEmpty(parameterName))
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                        }
                        else
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                        }
                    }
                }
                else
                {
                    sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                    command.CommandText = sql;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                            }

                            if (entry.Key == "dlxml" || entry.Key == "xml_send" || entry.Key == "xml_rep" || entry.Key == "dlxml_thue")
                            {
                                command.Parameters.Add(new NpgsqlParameter(entry.Key, DbType.Xml)).Value = entry.Value == null ? DBNull.Value : entry.Value;
                            }
                            else
                            {
                                command.Parameters.AddWithValue(entry.Key, entry.Value == null ? DBNull.Value : entry.Value);
                            }

                        }
                    }
                }

                await command.ExecuteNonQueryAsync();
            }

            return "";
        }

        public async Task<string> TransactionCommandAsync(string sql, CommandType commandType, JObject parameters)
        {
            string schemaName = await this.GetSchemaNameAsync();

            using (var command = new NpgsqlCommand())
            {
                command.Connection = _npgsqlConnection;
                command.CommandType = CommandType.Text;
                command.Transaction = _npgsqlTransaction;

                if (commandType == CommandType.StoredProcedure)
                {
                    sql = sql.Replace("#SCHEMA_NAME#.", "");

                    if (CommonManager.CheckForSqlInjection(sql))
                    {
                        throw new Exception($"Dữ liệu không hợp lệ {sql}");
                    }

                    string query = "SELECT unnest(p.proargnames) as proargnames, unnest(public.format_types(p.proargtypes)) as proargtypes FROM pg_catalog.pg_proc p "
                                + "INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace "
                                + "WHERE  p.proname='" + sql + "' AND n.nspname = '" + schemaName + "'";


                    DataTable tblParameter = new DataTable();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(query, _npgsqlConnection))
                    {

                        cmd.CommandType = CommandType.Text;

                        var reader = await cmd.ExecuteReaderAsync();
                        tblParameter.Load(reader);

                        string parameterName = "";

                        if (tblParameter.Rows.Count > 0)
                        {
                            foreach (DataRow row in tblParameter.Rows)
                            {
                                string proargnames = row["proargnames"].ToString();
                                string proargtypes = row["proargtypes"].ToString();

                                parameterName = parameterName + "@" + proargnames + ",";

                                string fieldName = proargnames.StartsWith("p_") ? proargnames.Substring(2) : proargnames;

                                NpgsqlParameter parameter = command.CreateParameter();
                                parameter.ParameterName = "@" + proargnames;
                                parameter.NpgsqlValue = DBNull.Value;

                                if (proargnames == "p_schemaname")
                                {
                                    parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                    parameter.NpgsqlValue = schemaName;
                                }
                                else
                                {
                                    if (parameters != null)
                                    {
                                        if (parameters[fieldName] != null)
                                        {

                                            if (proargtypes == "uuid")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Uuid;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Guid.Parse(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "timestamp without time zone")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "date")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Date;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDateTime(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "numeric")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToDecimal(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "integer")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer;

                                                if (parameters[fieldName].ToString() != "")
                                                {
                                                    parameter.NpgsqlValue = Convert.ToInt32(parameters[fieldName].ToString());
                                                }
                                            }
                                            else if (proargtypes == "boolean")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Boolean;
                                                bool _value = parameters[fieldName].ToString() == "1" || parameters[fieldName].ToString().ToLower() == "true" ? true : false;
                                                parameter.NpgsqlValue = _value;
                                            }
                                            else if (proargtypes == "character varying" || proargtypes == "text")
                                            {
                                                parameter.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                                                parameter.NpgsqlValue = parameters[fieldName].ToString();
                                            }
                                        }
                                    }

                                }

                                if (CommonManager.CheckForSqlInjection(parameter.NpgsqlValue?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(fieldName.ToLower())))
                                {
                                    throw new Exception($"Dữ liệu không hợp lệ {parameter.ParameterName} : {parameter.NpgsqlValue?.ToString()}");
                                }

                                command.Parameters.Add(parameter);
                            }
                        }

                        if (string.IsNullOrEmpty(parameterName))
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "()";
                        }
                        else
                        {
                            command.CommandText = "SELECT * FROM " + schemaName + "." + sql + "(" + parameterName.Substring(0, parameterName.Length - 1) + ")";
                        }
                    }
                }
                else
                {
                    sql = sql.Replace("#SCHEMA_NAME#", schemaName);
                    command.CommandText = sql;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.AddWithValue(entry.Key, entry.Value.ToString());
                        }
                    }
                }

                await command.ExecuteNonQueryAsync();
            }

            return "";
        }

        public async Task<string> TransactionCommitAsync()
        {
            await _npgsqlTransaction.CommitAsync();

            return "";
        }

        public async Task<DataTableExtend> GetTransactionDataTableAsync(string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTableExtend tblData = new DataTableExtend();

            string schemaName = await this.GetSchemaNameAsync();
            sql = sql.Replace("#SCHEMA_NAME#", schemaName);

            using (NpgsqlCommand command = new NpgsqlCommand(sql, _npgsqlConnection))
            {
                command.CommandType = commandType;
                command.Transaction = _npgsqlTransaction;

                if (parameters != null)
                {
                    foreach (var entry in parameters)
                    {
                        NpgsqlParameter npgsqlParam = command.CreateParameter();
                        npgsqlParam.ParameterName = "@" + entry.Key;
                        npgsqlParam.Value = entry.Value == null ? DBNull.Value : entry.Value;


                        if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                        {
                            throw new Exception($"Dữ liệu không hợp lệ {entry.Value} : {entry.Value?.ToString()}");
                        }

                        command.Parameters.Add(npgsqlParam);
                    }
                }

                var reader = await command.ExecuteReaderAsync();
                tblData.Load(reader);
            }

            return tblData;
        }

        public async Task<string> TransactionRollbackAsync()
        {
            if (_npgsqlTransaction != null)
            {
                await _npgsqlTransaction.RollbackAsync();
            }

            return "";
        }

        public void CloseTransaction()
        {
            if (_npgsqlTransaction != null)
            {
                _npgsqlTransaction.Dispose();
                _npgsqlTransaction = null;
            }

            if (_npgsqlConnection != null)
            {
                _npgsqlConnection.Close();
                _npgsqlConnection.Dispose();

                _npgsqlConnection = null;
            }
        }

        #endregion

        #region Private Method

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["InvoiceConnection"].ConnectionString;
        }


        private string GetMasterConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MasterConnection"].ConnectionString;
        }

        private string GetConnectionTenantString()
        {
            return GetConnectionCache(this.GetTaxCodeSite());
        }

        private async Task<string> GetConnectionTenantAsyncString()
        {
            return await GetConnectionAsyncCache(this.GetTaxCodeSite());
        }

        public string GetTaxCodeSite()
        {
            string host = "";
            if (!string.IsNullOrEmpty(site_hddt))
            {
                host = site_hddt;
            }
            else
            {
                host = this._webHelper.GetRequest().Url.Host;
            }
            string[] paths = host.Split('.');

            string taxCode = paths[0];

            return taxCode;

        }

        public string GetSchemaName()
        {
            string schemaName = "";

            string host = "";
            if (!string.IsNullOrEmpty(site_hddt))
            {
                host = site_hddt;
            }
            else
            {
                host = this._webHelper.GetRequest().Url.Host;
            }
            string[] paths = host.Split('.');

            string taxCode = paths[0];

            if (taxCode == "localhost")
            {
                return "dbo";
            }

            string sql = "SELECT * FROM reg.company WHERE taxcode=@taxcode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            RedisCacheManager _redisCacheManager = new RedisCacheManager(this);
            DataTable tblData = _redisCacheManager.GetDataTable(sql, CommandType.Text, parameters, taxCode, 60 * 24);

            if (tblData.Rows.Count > 0)
            {
                schemaName = tblData.Rows[0]["schema_name"].ToString();
            }

            return schemaName;

        }

        public async Task<string> GetSchemaNameAsync()
        {
            string schemaName = "";

            //var site_hddt = getSiteHDDTName(site_hddt);
            string host = "";
            if (!string.IsNullOrEmpty(site_hddt))
            {
                host = site_hddt;
            }
            else
            {
                host = this._webHelper.GetRequest().Url.Host;
            }
            string[] paths = host.Split('.');

            string taxCode = paths[0];

            if (taxCode == "localhost")
            {
                return "dbo";
            }

            string sql = "SELECT * FROM reg.company WHERE taxcode=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            RedisCacheManager _redisCacheManager = new RedisCacheManager(this);
            DataTable tblData = await _redisCacheManager.GetDataTableAsync(sql, CommandType.Text, parameters, taxCode, 60 * 24);

            if (tblData.Rows.Count > 0)
            {
                schemaName = tblData.Rows[0]["schema_name"].ToString();
            }

            return schemaName;

        }

        public DataTable GetDataTableCache(string taxCode)
        {
            string sql = "SELECT * FROM reg.company WHERE taxcode=@taxcode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            DataTable tblData = new DataTable();

            string connectionString = this.GetConnectionTenantString();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    var reader = command.ExecuteReader();
                    tblData.Load(reader);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTable> GetDataTableAsyncCache(string taxCode)
        {
            string sql = "SELECT * FROM reg.company WHERE taxcode=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            DataTable tblData = new DataTable();

            string connectionString = await this.GetConnectionTenantAsyncString();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }


                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<string> GetConnectionAsyncCache(string taxCode)
        {
            string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.tenant_config WHERE tax_code=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            RedisCacheManager _redisCacheManager = new RedisCacheManager(this);
            DataTable tblData = await _redisCacheManager.GetDataTableAsyncTenant(sql, CommandType.Text, parameters, taxCode, 60 * 24);

            string connectionTenant = "";

            if (tblData.Rows.Count > 0)
            {
                connectionTenant = tblData.Rows[0]["schema_url"].ToString();
            }

            return connectionTenant;
        }

        public string GetConnectionCache(string taxCode)
        {
            string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.tenant_config WHERE tax_code=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            RedisCacheManager _redisCacheManager = new RedisCacheManager(this);
            DataTable tblData = _redisCacheManager.GetDataTableTenant(sql, CommandType.Text, parameters, taxCode, 60 * 24);

            string connectionTenant = "";

            if (tblData.Rows.Count > 0)
            {
                connectionTenant = tblData.Rows[0]["schema_url"].ToString();
            }

            return connectionTenant;
        }


        public DataTable GetDataTableTenantCache(string taxCode)
        {
            string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.tenant_config WHERE tax_code=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            DataTable tblData = new DataTable();

            string connectionString = this.GetMasterConnectionString();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                conn.Open();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {
                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }

                    var reader = command.ExecuteReader();
                    tblData.Load(reader);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        public async Task<DataTable> GetDataTableTenantAsyncCache(string taxCode)
        {
            string sql = $"SELECT * FROM {CommonConstants.MASTER_SCHEMA}.tenant_config WHERE tax_code=@taxCode ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("taxCode", taxCode);

            DataTable tblData = new DataTable();

            string connectionString = this.GetMasterConnectionString();

            NpgsqlConnection conn = null;

            try
            {
                conn = new NpgsqlConnection(connectionString);
                await conn.OpenAsync();

                using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
                {
                    command.CommandType = CommandType.Text;

                    if (parameters != null)
                    {
                        foreach (var entry in parameters)
                        {
                            NpgsqlParameter npgsqlParam = command.CreateParameter();
                            npgsqlParam.ParameterName = "@" + entry.Key;
                            npgsqlParam.Value = entry.Value;

                            if (CommonManager.CheckForSqlInjection(entry.Value?.ToString(), !CommonConstants.WHITE_LIST_COLUMN.Contains(entry.Key.ToString().ToLower())))
                            {

                                throw new Exception($"Dữ liệu không hợp lệ {entry.Key} : {entry.Value?.ToString()}");
                            }

                            command.Parameters.Add(npgsqlParam);
                        }
                    }


                    var reader = await command.ExecuteReaderAsync();
                    tblData.Load(reader);


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
                catch { }
                try
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
                catch { }
            }

            return tblData;
        }

        #endregion

    }
}