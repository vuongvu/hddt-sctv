﻿using Newtonsoft.Json.Linq;
using Search_Invoice.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Search_Invoice.Services
{
    public partial interface ITracuuService2
    {
        Task<JObject> GetInvoiceFromdateTodateWeb(DateTime tu_ngay, DateTime den_ngay, inv_user us);
        Task<JObject> GetInvoiceFromdateTodate(JObject model);
        Task<JObject> GetInfoInvoice(JObject model);
        Task<byte[]> PrintInvoiceFromSBM(string sobaomat, string masothue, string folder, string type);
        Task<byte[]> PrintInvoiceFromSBM(string sobaomat, string masothue, string folder, string type, bool inchuyendoi);
        byte[] ExportZipFileXML(string sobaomat, string masothue, string pathReport, ref string fileName, ref string key);
        Task<JObject> SendEmail(string masothue, string email);

        Task<JObject> RegisterAcount(string masothue, string email, string name);
    }
}