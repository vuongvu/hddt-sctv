﻿using System.Web;
using System.Threading.Tasks;
using System.Data;
using System.Drawing;
using Search_Invoice.Data.Domain;
using System.Text;
using System.IO;
using HtmlAgilityPack;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraPrinting;
using Search_Invoice.Util;
using DevExpress.XtraReports.UI;
using System.Globalization;
using System.Xml;
using System.Drawing.Imaging;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections.Generic;
using Search_Invoice.Data;
using System.Xml.Linq;
using Search_Invoice.DAL;
using InvoiceApi.Services;
using System.Configuration;
using System.Threading;

namespace Search_Invoice.Services
{
    public class TracuuService2 : ITracuuService2
    {
        private INopDbContext2 _nopDbContext2;
        private ICacheManager _cacheManager;
        private IWebHelper _webHelper;
        private IMinvoiceDbContext _minvoiceDbContext;
        private IMasterInvoiceDbContext _masterDbContext;
        private IEmailService _emailService;

        public TracuuService2(
                              INopDbContext2 nopDbContext2,
                              ICacheManager cacheManager,
                              IWebHelper webHelper,
                              IMinvoiceDbContext minvoiceDbContext,
                              IEmailService emailService,
                              IMasterInvoiceDbContext masterDbContext
          )
        {
            this._nopDbContext2 = nopDbContext2;
            this._cacheManager = cacheManager;
            this._webHelper = webHelper;
            this._minvoiceDbContext = minvoiceDbContext;
            _emailService = emailService;
            _masterDbContext = masterDbContext;
        }
        public async Task<JObject> GetInvoiceFromdateTodateWeb(DateTime tu_ngay, DateTime den_ngay, inv_user us)
        {
            JObject json = new JObject();
            try
            {
                //inv_user us = (inv_user)Session[CommonConstants.USER_SESSION];
                // CommonConnect cn = new CommonConnect();
                _minvoiceDbContext.SetSiteHddt(us.tax_code);
                //cn.setConnect(us.tax_code);
                string sql = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE DATE_TRUNC('day',invoice_issued_date) >= DATE_TRUNC('day', @tu_ngay) \n" +
                    "and DATE_TRUNC('day',invoice_issued_date) <=  DATE_TRUNC('day', @den_ngay) AND customer_code =@customer_code";
                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("tu_ngay", tu_ngay);
                parameter.Add("den_ngay", den_ngay);
                parameter.Add("customer_code", us.code_customer);

                DataTable dt = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameter);
                dt.Columns.Add("mst", typeof(string));

                foreach (DataRow row in dt.Rows)
                {
                    row.BeginEdit();
                    row["mst"] = us.tax_code;
                    row.EndEdit();
                }


                if (dt.Rows.Count > 0)
                {
                    JArray jar = JArray.FromObject(dt);
                    json.Add("data", jar);
                }
                else
                {
                    json.Add("error", "Không tìm thấy dữ liệu.");
                }
                // return result;
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }
        public async Task<JObject> GetInvoiceFromdateTodate(JObject model)
        {
            JObject json = new JObject();
            try
            {
                string mst = model["masothue"].ToString().Replace("-", "");

                DateTime tu_ngay = (DateTime)model["tu_ngay"];
                DateTime den_ngay = (DateTime)model["den_ngay"];
                string ma_dt = model["ma_dt"].ToString();
                //_nopDbContext2.setConnect(mst);
                this._minvoiceDbContext.SetSiteHddt(mst);

                string sqlInvAuth = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth \n"
                    + "WHERE DATE_TRUNC('day', invoice_issued_date) >= DATE_TRUNC('day', @tu_ngay) AND invoice_issued_date <= DATE_TRUNC('day', @den_ngay) \n"
                    + "AND customer_code =@customer_code";

                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("tu_ngay", tu_ngay);
                parameter.Add("den_ngay", den_ngay);
                parameter.Add("customer_code", ma_dt);

                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sqlInvAuth, CommandType.Text, parameter);
                //dt.Columns.Add("mst", typeof(string));

                //foreach (DataRow row in dt.Rows)
                //{
                //    row.BeginEdit();
                //    row["mst"] = mst;
                //    row.EndEdit();
                //}
                if (dt.Rows.Count > 0)
                {
                    JArray jar = JArray.FromObject(dt);
                    json.Add("data", jar);
                }
                else
                {
                    json.Add("error", "Không tìm thấy dữ liệu.");
                    return json;
                }
            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }

        public async Task<JObject> GetInfoInvoice(JObject model)
        {
            JObject json = new JObject();
            try
            {
                //bool isTT32 = true;
                //string mst = string.Empty;
                string mst = model["masothue"].ToString().Replace("-", "");
                if (string.IsNullOrEmpty(mst))
                {
                    json.Add("error", "Vui lòng nhập mã số thuế");
                    return json;
                }
                string sobaomat = model["sobaomat"].ToString();
                if (string.IsNullOrEmpty(sobaomat))
                {
                    json.Add("error", "Vui lòng nhập Mã tra cứu hoá đơn");
                    return json;
                }

                if (CommonConnect.CheckForSqlInjection(sobaomat))
                {
                    json.Add("error", "Mã tra cứu hoá đơn không hợp lệ! ");
                    return json;
                }

                //_nopDbContext2.setConnect(mst);
                this._minvoiceDbContext.SetSiteHddt(mst);

                string sqlInvoice = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE security_number =@sobaomat";

                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("sobaomat", sobaomat);

                DataTable dt = await this._minvoiceDbContext.GetDataTableAsync(sqlInvoice, CommandType.Text, parameter);

                // tra cứu theo TT32
                if (dt.Rows.Count > 0)
                {
                    dt.Columns.Add("mst", typeof(string));

                    foreach (DataRow row in dt.Rows)
                    {
                        row.BeginEdit();
                        row["mst"] = mst;
                        row.EndEdit();
                    }
                    JArray jar = JArray.FromObject(dt);
                    json.Add("data", jar);
                    json.Add("isTT32", "1");

                }
                else
                {
                    // tra cứu theo TT78
                    string sqlInvoice_tt78 = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE sbmat =@sobaomat";
                    DataTable dt_tt78 = await this._minvoiceDbContext.GetDataTableAsync(sqlInvoice_tt78, CommandType.Text, parameter);
                    if (dt_tt78.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt_tt78.Rows)
                        {
                            row.BeginEdit();
                            row["mst"] = mst;
                            row.EndEdit();
                        }
                        JArray jar = JArray.FromObject(dt_tt78);
                        json.Add("data", jar);
                        json.Add("isTT32", "0");
                    }
                    else
                    {
                        json.Add("error", "Không tồn tại hóa đơn có Mã tra cứu hoá đơn: " + sobaomat);
                        return json;
                    }

                }

            }
            catch (Exception ex)
            {
                json.Add("error", ex.Message);
            }
            return json;
        }

        public async Task<byte[]> PrintInvoiceFromSBM(string sobaomat, string masothue, string folder, string type)
        {
            byte[] results = await PrintInvoiceFromSBM(sobaomat, masothue, folder, type, false);
            return results;
        }

        public async Task<byte[]> PrintInvoiceFromSBM(string sobaomat, string masothue, string folder, string type, bool inchuyendoi)
        {
            _minvoiceDbContext.SetSiteHddt(masothue);
            //var db = this._nopDbContext2.GetInvoiceDb();

            byte[] bytes = null;

            string xml = "";
            string msg_tb = "";

            try
            {
                // Guid inv_InvoiceAuth_id = Guid.Parse(id);
                string sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE security_number=@sobaomat ";
                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("sobaomat", sobaomat);

                DataTable tblInv_InvoiceAuth = await this._minvoiceDbContext.GetDataTableAsync(sqlInvoiceAuth, CommandType.Text, parameter);
                if (tblInv_InvoiceAuth.Rows.Count == 0)
                {
                    // kiểm tra TT78
                    sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE sbmat =@sobaomat";
                    DataTable tblHoadon = this._minvoiceDbContext.GetDataTable(sqlInvoiceAuth, CommandType.Text, parameter);
                    if (tblHoadon.Rows.Count == 0)
                    {
                        throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                    }

                    //throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                }

                if (tblInv_InvoiceAuth.Rows.Count > 0)
                {
                    string inv_InvoiceAuth_id = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

                    string sqlInvoiceDetail = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauthdetail WHERE inv_invoiceauth_id =@inv_InvoiceAuth_id ";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_InvoiceAuth_id", Guid.Parse(inv_InvoiceAuth_id));

                    DataTable tblInv_InvoiceAuthDetail = await this._minvoiceDbContext.GetDataTableAsync(sqlInvoiceDetail, CommandType.Text, parameter);

                    string sqlXmlData = "SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_InvoiceAuth_id ";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_InvoiceAuth_id", Guid.Parse(inv_InvoiceAuth_id));

                    DataTable tblInvoiceXmlData = await this._minvoiceDbContext.GetDataTableAsync(sqlXmlData, CommandType.Text, parameter);

                    //if (masothue == "2700638514" && tblInv_InvoiceAuthDetail.Rows.Count > 9)
                    //{
                    //    xml = db.Database.SqlQuery<string>("EXECUTE sproc_export_XmlInvoice_BK '" + inv_InvoiceAuth_id + "'").FirstOrDefault<string>();
                    //}
                    //else
                    //{
                    if (tblInvoiceXmlData.Rows.Count > 0)
                    {
                        xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                    }
                    else
                    {
                        string sqlExportXml = "SELECT #SCHEMA_NAME#.sproc_export_XmlInvoice(@inv_InvoiceAuth_id)";
                        parameter = new Dictionary<string, object>();
                        parameter.Add("inv_InvoiceAuth_id", Guid.Parse(inv_InvoiceAuth_id));

                        DataTable tbl_xml = await _minvoiceDbContext.GetDataTableAsync(sqlExportXml, CommandType.Text, parameter);
                        xml = tbl_xml.Rows[0][0].ToString();
                    }
                    xml = ReportUtil.ReverseXml(xml);
                    //}
                    //var invoiceDb = this._nopDbContext2.GetInvoiceDb();
                    string inv_InvoiceCode_id = tblInv_InvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();
                    int trang_thai_hd = Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["invoice_status"]);
                    string inv_originalId = tblInv_InvoiceAuth.Rows[0]["original_invoice_id"].ToString();
                    string user_name = _webHelper.GetUser();
                    // wb_user wbuser = invoiceDb.WbUsers.Where(c => c.username == user_name).FirstOrDefault<wb_user>();

                    string sqlNotifice = "SELECT * FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id \n"
                        + "WHERE a.inv_notificationdetail_id=@inv_InvoiceCode_id";

                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_InvoiceCode_id", Guid.Parse(inv_InvoiceCode_id));

                    DataTable tblCtthongbao = await this._minvoiceDbContext.GetDataTableAsync(sqlNotifice, CommandType.Text, parameter);
                    string hang_nghin = ".";
                    string thap_phan = ",";
                    DataColumnCollection columns = tblCtthongbao.Columns;
                    if (columns.Contains("decimal_point"))
                    {
                        thap_phan = tblCtthongbao.Rows[0]["decimal_point"].ToString();
                    }
                    if (columns.Contains("thousand_point"))
                    {
                        hang_nghin = tblCtthongbao.Rows[0]["thousand_point"].ToString();
                    }
                    if (hang_nghin == null || hang_nghin == "")
                    {
                        hang_nghin = ".";
                    }
                    if (thap_phan == "" || thap_phan == null)
                    {
                        thap_phan = ",";
                    }
                    //string hang_nghin = tblCtthongbao.Rows[0]["hang_nghin"].ToString();
                    //string thap_phan = tblCtthongbao.Rows[0]["thap_phan"].ToString();

                    //string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", tblCtthongbao.Rows[0]["sl_invoice_template_id"]);

                    //XtraReport report = _cacheManager.Get<XtraReport>(cacheReportKey);
                    XtraReport report = new XtraReport();
                    report = null;

                    if (report == null)
                    {
                        string sqlTemplate = "SELECT * FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id";
                        parameter = new Dictionary<string, object>();
                        parameter.Add("sl_invoice_template_id", Guid.Parse(tblCtthongbao.Rows[0]["sl_invoice_template_id"].ToString()));

                        DataTable tblDmmauhd = await this._minvoiceDbContext.GetDataTableAsync(sqlTemplate, CommandType.Text, parameter);
                        string invReport = tblDmmauhd.Rows[0]["report"].ToString();

                        if (invReport.Length > 0)
                        {
                            report = ReportUtil.LoadReportFromString(invReport);
                            //_cacheManager.Set(cacheReportKey, report, 30);
                        }
                        else
                        {
                            throw new Exception("Không tải được mẫu hóa đơn");
                        }

                    }

                    report.Name = "XtraReport1";
                    report.ScriptReferencesString = "AccountSignature.dll";

                    DataSet ds = new DataSet();

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                    {
                        ds.ReadXmlSchema(xmlReader);
                        xmlReader.Close();
                    }

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                    {
                        ds.ReadXml(xmlReader);
                        xmlReader.Close();
                    }

                    if (ds.Tables.Contains("TblXmlData"))
                    {
                        ds.Tables.Remove("TblXmlData");
                    }

                    DataTable tblXmlData = new DataTable();
                    tblXmlData.TableName = "TblXmlData";
                    tblXmlData.Columns.Add("data");

                    DataRow r = tblXmlData.NewRow();
                    r["data"] = xml;
                    tblXmlData.Rows.Add(r);
                    ds.Tables.Add(tblXmlData);

                    string datamember = report.DataMember;

                    if (datamember.Length > 0)
                    {
                        if (ds.Tables.Contains(datamember))
                        {
                            DataTable tblChiTiet = ds.Tables[datamember];

                            int rowcount = ds.Tables[datamember].Rows.Count;


                        }
                    }

                    if (trang_thai_hd == 11 || trang_thai_hd == 13 || trang_thai_hd == 17)
                    {
                        if (!string.IsNullOrEmpty(inv_originalId))
                        {
                            string sqlInvAuth = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id ";
                            parameter = new Dictionary<string, object>();
                            parameter.Add("inv_invoiceauth_id", Guid.Parse(inv_originalId));

                            DataTable tblInv = await this._minvoiceDbContext.GetDataTableAsync(sqlInvAuth, CommandType.Text, parameter);
                            string inv_adjustmentType = tblInv.Rows[0]["adjustment_type"].ToString();

                            string loai = inv_adjustmentType.ToString() == "5" || inv_adjustmentType.ToString() == "19" || inv_adjustmentType.ToString() == "21" ? "điều chỉnh" : inv_adjustmentType.ToString() == "3" ? "thay thế" : inv_adjustmentType.ToString() == "7" ? "xóa bỏ" : "";

                            if (inv_adjustmentType.ToString() == "5" || inv_adjustmentType.ToString() == "7" || inv_adjustmentType.ToString() == "3" || inv_adjustmentType.ToString() == "19" || inv_adjustmentType.ToString() == "21")
                            {
                                msg_tb = "Hóa đơn bị " + loai + " bởi hóa đơn số: " + tblInv.Rows[0]["invoice_number"] + " ngày " + string.Format("{0:dd/MM/yyyy}", tblInv.Rows[0]["invoice_issued_date"]) + ", mẫu số " + tblInv.Rows[0]["template_code"] + " ký hiệu " + tblInv.Rows[0]["invoice_series"];

                            }
                        }
                    }

                    if (Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]) == 7)
                    {
                        msg_tb = "";
                    }

                    if (report.Parameters["MSG_TB"] != null)
                    {
                        report.Parameters["MSG_TB"].Value = msg_tb;
                    }

                    var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

                    if (lblHoaDonMau != null)
                    {
                        lblHoaDonMau.Visible = false;
                    }

                    // Mã tra cứu hoá đơn
                    string sbmat = tblInv_InvoiceAuth.Rows[0]["security_number"].ToString();
                    if (report.Parameters["security_number"] != null)
                    {
                        report.Parameters["security_number"].Value = sbmat;
                    }

                    if (inchuyendoi)
                    {
                        var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                        if (tblInChuyenDoi != null)
                        {
                            tblInChuyenDoi.Visible = true;
                        }

                        if (report.Parameters["MSG_HD_TITLE"] != null)
                        {
                            report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                        }

                        //if (report.Parameters["NGUOI_IN_CDOI"] != null)
                        //{
                        //    report.Parameters["NGUOI_IN_CDOI"].Value = wbuser.ten_nguoi_sd == null ? "" : wbuser.ten_nguoi_sd;
                        //    report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                        //}

                        if (report.Parameters["NGAY_IN_CDOI"] != null)
                        {
                            report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                            report.Parameters["NGAY_IN_CDOI"].Visible = true;
                        }
                    }

                    report.DataSource = ds;

                    try
                    {
                        bool giamthuebanhang20 = tblInv_InvoiceAuth.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblInv_InvoiceAuth.Rows[0]["giamthuebanhang20"].ToString());
                        string dvtte = tblInv_InvoiceAuth.Rows[0]["currency_code"]?.ToString();

                        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                        string tongTienGiam = double.Parse(DoubleStrToString(tblInv_InvoiceAuth.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                        tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");

                        string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";
                        if (giamthuebanhang20)
                        {
                            var dt = ds.Tables["invoiceinformation"];
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Columns.Contains("amount_by_word"))
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        var docTien = (dt.Rows[i]["amount_by_word"]?.ToString()).Trim();
                                        docTien = String.IsNullOrEmpty(docTien) ? "Không đồng." : docTien;
                                        ds.Tables["invoiceinformation"].Rows[i]["amount_by_word"] = docTien + " " + Environment.NewLine + thueGTGTBanHang;
                                        report.DataSource = ds;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    var t = Task.Run(() =>
                    {
                        var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        newCulture.NumberFormat.NumberDecimalSeparator = thap_phan;
                        newCulture.NumberFormat.NumberGroupSeparator = hang_nghin;

                        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
                        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;

                        report.CreateDocument();

                    });

                    t.Wait();

                    if (tblInv_InvoiceAuth.Columns.Contains("list_number"))
                    {
                        if (tblInv_InvoiceAuth.Rows[0]["list_number"].ToString().Length > 0)
                        {
                            string fileName = folder + "\\BangKeDinhKem.repx";

                            XtraReport rpBangKe = null;

                            if (!File.Exists(fileName))
                            {
                                rpBangKe = new XtraReport();
                                rpBangKe.SaveLayout(fileName);
                            }
                            else
                            {
                                rpBangKe = XtraReport.FromFile(fileName, true);
                            }

                            rpBangKe.ScriptReferencesString = "AccountSignature.dll";
                            rpBangKe.Name = "rpBangKe";
                            rpBangKe.DisplayName = "BangKeDinhKem.repx";

                            rpBangKe.DataSource = report.DataSource;

                            rpBangKe.CreateDocument();
                            report.Pages.AddRange(rpBangKe.Pages);
                        }



                    }

                    if (tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString() == "7")
                    {

                        Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        int pageCount = report.Pages.Count;


                        for (int i = 0; i < pageCount; i++)
                        {
                            Page page = report.Pages[i];
                            PageWatermark pmk = new PageWatermark();
                            pmk.Image = bmp;
                            page.AssignWatermark(pmk);
                        }

                        string fileName = folder + "\\BienBanXoaBo.repx";
                        XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                        rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                        rpBienBan.Name = "rpBienBan";
                        rpBienBan.DisplayName = "BienBanXoaBo.repx";

                        rpBienBan.DataSource = report.DataSource;
                        rpBienBan.DataMember = report.DataMember;

                        rpBienBan.CreateDocument();

                        rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                        report.PrintingSystem.ContinuousPageNumbering = false;

                        report.Pages.AddRange(rpBienBan.Pages);

                        int idx = pageCount;
                        pageCount = report.Pages.Count;

                        for (int i = idx; i < pageCount; i++)
                        {
                            PageWatermark pmk = new PageWatermark();
                            pmk.ShowBehind = false;
                            report.Pages[i].AssignWatermark(pmk);
                        }

                    }

                    if (trang_thai_hd == 13 || trang_thai_hd == 17)
                    {
                        Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        int pageCount = report.Pages.Count;

                        for (int i = 0; i < pageCount; i++)
                        {
                            PageWatermark pmk = new PageWatermark();
                            pmk.Image = bmp;
                            report.Pages[i].AssignWatermark(pmk);
                        }
                    }



                    MemoryStream ms = new MemoryStream();

                    if (type == "Html")
                    {
                        report.ExportOptions.Html.EmbedImagesInHTML = true;
                        report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                        report.ExportOptions.Html.Title = "Hóa đơn điện tử M-Invoice";
                        report.ExportToHtml(ms);

                        string html = Encoding.UTF8.GetString(ms.ToArray());

                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(html);


                        string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                        string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                        var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");
                        //td[@onmousedown]

                        if (nodes != null)
                        {
                            foreach (HtmlNode node in nodes)
                            {
                                string eventMouseDown = node.Attributes["onmousedown"].Value;

                                if (eventMouseDown.Contains("showCert('seller')"))
                                {
                                    node.SetAttributeValue("id", "certSeller");
                                }
                                if (eventMouseDown.Contains("showCert('buyer')"))
                                {
                                    node.SetAttributeValue("id", "certBuyer");
                                }
                                if (eventMouseDown.Contains("showCert('vacom')"))
                                {
                                    node.SetAttributeValue("id", "certVacom");
                                }
                                if (eventMouseDown.Contains("showCert('minvoice')"))
                                {
                                    node.SetAttributeValue("id", "certMinvoice");
                                }
                            }
                        }

                        HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                        HtmlNode xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("id", "xmlData");
                        xmlNode.SetAttributeValue("type", "text/xmldata");

                        xmlNode.AppendChild(doc.CreateTextNode(xml));
                        head.AppendChild(xmlNode);

                        xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                        head.AppendChild(xmlNode);

                        xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                        head.AppendChild(xmlNode);

                        xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("type", "text/javascript");

                        xmlNode.InnerHtml = "$(function () { "
                                           + "  var url = 'http://localhost:19898/signalr'; "
                                           + "  var connection = $.hubConnection(url, {  "
                                           + "     useDefaultPath: false "
                                           + "  });"
                                           + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                           + " invoiceHubProxy.on('resCommand', function (result) { "
                                           + " }); "
                                           + " connection.start().done(function () { "
                                           + "      console.log('Connect to the server successful');"
                                           + "      $('#certSeller').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'seller' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "      $('#certVacom').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'vacom' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "      $('#certBuyer').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'buyer' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "      $('#certMinvoice').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'minvoice' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "})"
                                           + ".fail(function () { "
                                           + "     alert('failed in connecting to the signalr server'); "
                                           + "});"
                                           + "});";

                        head.AppendChild(xmlNode);

                        if (report.Watermark != null)
                        {
                            if (report.Watermark.Image != null)
                            {
                                ImageConverter _imageConverter = new ImageConverter();
                                byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                                string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                                HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                                string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                                string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                                HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                                style.AppendChild(textNode);

                                HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                                HtmlNodeCollection pageNodes = body.SelectNodes("div");

                                foreach (var pageNode in pageNodes)
                                {
                                    pageNode.Attributes.Add("class", "waterMark");

                                    string divStyle = pageNode.Attributes["style"].Value;
                                    divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                    pageNode.Attributes["style"].Value = divStyle;
                                }
                            }
                        }

                        ms.SetLength(0);
                        doc.Save(ms);

                        doc = null;
                    }
                    else if (type == "Image")
                    {
                        var options = new ImageExportOptions(ImageFormat.Png)
                        {
                            ExportMode = ImageExportMode.SingleFilePageByPage,
                        };
                        report.ExportToImage(ms, options);
                    }
                    else
                    {
                        report.ExportToPdf(ms);
                    }

                    bytes = ms.ToArray();
                    ms.Close();

                    if (bytes == null)
                    {
                        throw new Exception("null");
                    }
                }
                else
                {
                    // TT78
                    sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE sbmat =@sobaomat";

                    //Dictionary<string, object> parameter = new Dictionary<string, object>();
                    //parameter.Add("sobaomat", sobaomat);

                    DataTable tblHoadon = this._minvoiceDbContext.GetDataTable(sqlInvoiceAuth, CommandType.Text, parameter);
                    if (tblHoadon.Rows.Count == 0)
                    {
                        throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                    }
                    else if (tblHoadon.Rows.Count > 0)
                    {
                        Guid hdon_id = Guid.Parse(tblHoadon.Rows[0]["hdon_id"].ToString());

                        DataTable tblInvoiceXmlData = this._minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id='" + hdon_id + "'");

                        if (tblInvoiceXmlData.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString();
                                //XDocument doc = XDocument.Parse(xml);

                                //xml = doc.Descendants("HDon").First().ToString();

                                XmlDocument document = new XmlDocument();
                                document.LoadXml(xml);
                                XmlNode nodeHDon = document.SelectSingleNode("/TDiep/DLieu/HDon");
                                xml = nodeHDon.OuterXml;
                            }
                            else if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString();
                            }
                            else
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml"].ToString();
                            }

                        }
                        else
                        {
                            throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                        }

                        string sqlDetails = $"SELECT  * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' order by Cast (stt as numeric)";
                        DataTable detail = await _minvoiceDbContext.GetDataTableAsync(sqlDetails);

                        string[] notGet = new string[] { "kmai", "stt", "ma", "ten", "mdvtinh", "sluong", "dgia", "tlckhau", "stckhau", "thtien", "tsuat", "tsuatstr", "hdon_id", "cthdon_id" };

                        string cctbao_id = tblHoadon.Rows[0]["cctbao_id"].ToString();
                        int trang_thai_hd = Convert.ToInt32(tblHoadon.Rows[0]["tthdon"]);
                        string sbmat = tblHoadon.Rows[0]["sbmat"].ToString();

                        string SqlDongMau = $"Select sdmau from #SCHEMA_NAME#.quanlykyhieu68 where qlkhsdung_id = '{cctbao_id}'";
                        DataTable dongMau = await _minvoiceDbContext.GetDataTableAsync(SqlDongMau);
                        int soDongMau = 1;
                        try
                        {
                            soDongMau = Convert.ToInt32(dongMau.Rows[0][0].ToString());
                        }
                        catch
                        {

                        }

                        xml = FunctionUtil.AddXml(xml, detail, notGet, "HHDVu", cctbao_id, soDongMau);

                        XtraReport report = new XtraReport();

                        DataTable tblKyhieu = _minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + cctbao_id + "'");
                        DataTable tblDmmauhd = _minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + tblKyhieu.Rows[0]["qlmtke_id"] + "'");
                        string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

                        if (invReport.Length > 0)
                        {
                            report = ReportUtil.LoadReportFromString(invReport);
                            // _cacheManager.Set(cacheReportKey, report, 30);
                        }
                        else
                        {
                            throw new Exception("Không tải được mẫu hóa đơn");
                        }

                        report.Name = "XtraReport1";
                        report.ScriptReferencesString = "AccountSignature.dll";

                        DataSet ds = new DataSet();

                        using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                        {
                            ds.ReadXmlSchema(xmlReader);
                            xmlReader.Close();
                        }

                        using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                        {
                            ds.ReadXml(xmlReader);
                            xmlReader.Close();
                        }

                        if (ds.Tables.Contains("TblXmlData"))
                        {
                            ds.Tables.Remove("TblXmlData");
                        }


                        DataTable tblXmlData = new DataTable();
                        tblXmlData.TableName = "TblXmlData";
                        tblXmlData.Columns.Add("data");

                        DataRow r = tblXmlData.NewRow();
                        r["data"] = xml;
                        tblXmlData.Rows.Add(r);
                        ds.Tables.Add(tblXmlData);

                        string datamember = report.DataMember;

                        if (datamember.Length > 0)
                        {
                            if (ds.Tables.Contains(datamember))
                            {
                                DataTable tblChiTiet = ds.Tables[datamember];
                                int rowcount = ds.Tables[datamember].Rows.Count;
                            }
                        }

                        if (tblHoadon.Rows[0]["hdon68_id_lk"] != null &&
                    !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString()))
                        {
                            if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "3")
                            {
                                Guid hoadonIdLK =
                                    tblHoadon.Rows[0]["hdon68_id_lk"] != null &&
                                    !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                        ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                        : hdon_id;
                                string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                             + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                                DataTable tblInv = _minvoiceDbContext.GetDataTable(sql);

                                if (tblInv.Rows.Count > 0)
                                {
                                    //    string msg = tblInv.Rows[0]["msg"].ToString();

                                    //    if (msg.Length > 0)
                                    //    {
                                    //        DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=" + trang_thai_hd);
                                    //        string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();

                                    DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                                    string ngay = tdlapDate.Day.ToString();
                                    string thang = tdlapDate.Month.ToString();
                                    string nam = tdlapDate.Year.ToString();

                                    msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                             tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                             + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                             tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                                                                  " năm " + nam;

                                }
                                //}
                            }

                            if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "2")
                            {
                                Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                                    ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                    : hdon_id;
                                string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                             + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                                DataTable tblInv = _minvoiceDbContext.GetDataTable(sql);

                                if (tblInv.Rows.Count > 0)
                                {
                                    DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                                    string ngay = tdlapDate.Day.ToString();
                                    string thang = tdlapDate.Month.ToString();
                                    string nam = tdlapDate.Year.ToString();

                                    msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                             tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                             + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                             tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                             " năm " + nam;
                                }
                            }

                            if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "19" ||
                                tblHoadon.Rows[0]["tthdon_original"].ToString() == "21" ||
                                tblHoadon.Rows[0]["tthdon_original"].ToString() == "23")
                            {
                                Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                                    ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                    : hdon_id;
                                string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                             + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                                DataTable tblInv = _minvoiceDbContext.GetDataTable(sql);

                                if (tblInv.Rows.Count > 0)
                                {
                                    DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                                    string ngay = tdlapDate.Day.ToString();
                                    string thang = tdlapDate.Month.ToString();
                                    string nam = tdlapDate.Year.ToString();

                                    msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " +
                                             tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                             + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                             tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                             " năm " + nam;
                                }
                            }
                        }

                        if (tblHoadon.Rows[0]["dvtte"] != null && tblHoadon.Rows[0]["dvtte"].ToString() == "USD" && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgia"].ToString()))
                        {
                            decimal tienthanhtoan = 0;
                            if (tblHoadon.Rows[0]["tgtttbso_last"] != null && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgtttbso_last"].ToString()))
                            {
                                tienthanhtoan = Convert.ToDecimal(tblHoadon.Rows[0]["tgtttbso_last"].ToString());
                            }
                            decimal tigia = Convert.ToDecimal(tblHoadon.Rows[0]["tgia"].ToString());
                            decimal tienquydoi = tienthanhtoan * tigia;
                            if (report.Parameters["QUYDOI"] != null)
                            {
                                report.Parameters["QUYDOI"].Value = tienquydoi.ToString();
                            }
                        }
                        if (report.Parameters["MSG_TB"] != null)
                        {
                            report.Parameters["MSG_TB"].Value = msg_tb;
                        }

                        if (report.Parameters["MCCQT"] != null)
                        {
                            report.Parameters["MCCQT"].Value = tblHoadon.Rows[0]["mccqthue"].ToString();
                        }

                        // Mã tra cứu hoá đơn
                        if (report.Parameters["SoBaoMat"] != null)
                        {
                            report.Parameters["SoBaoMat"].Value = sbmat;
                        }

                        try
                        {

                            if (report.Parameters["SoThapPhan"] != null)
                            {
                                string sql = "SELECT * FROM #SCHEMA_NAME#.sl_currency a "
                                             + "WHERE a.code='" + tblHoadon.Rows[0]["dvtte"].ToString() + "' limit 1";

                                DataTable currency = await _minvoiceDbContext.GetDataTableAsync(sql);
                                if (currency.Rows.Count > 0)
                                {
                                    report.Parameters["SoThapPhan"].Value = currency.Rows[0]["hoadon123"].ToString();
                                }
                            }
                        }
                        catch
                        {

                        }

                        if (inchuyendoi)
                        {
                            var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                            if (tblInChuyenDoi != null)
                            {
                                tblInChuyenDoi.Visible = true;
                            }

                            if (report.Parameters["MSG_HD_TITLE"] != null)
                            {
                                report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                            }

                            if (report.Parameters["NGUOI_IN_CDOI"] != null)
                            {
                                report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                                report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                            }

                            if (report.Parameters["NGAY_IN_CDOI"] != null)
                            {
                                report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                                report.Parameters["NGAY_IN_CDOI"].Visible = true;
                            }
                        }

                        report.DataSource = ds;

                        try
                        {
                            bool giamthuebanhang20 = tblHoadon.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblHoadon.Rows[0]["giamthuebanhang20"].ToString());
                            string dvtte = tblHoadon.Rows[0]["dvtte"]?.ToString();

                            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                            string tongTienGiam = double.Parse(DoubleStrToString(tblHoadon.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                            tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");
                            string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";

                            if (giamthuebanhang20)
                            {
                                var dt = ds.Tables["TToan"];
                                if (dt.Rows.Count > 0)
                                {
                                    if (dt.Columns.Contains("TgTTTBChu"))
                                    {
                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            var docTien = (dt.Rows[i]["TgTTTBChu"]?.ToString()).Trim();

                                            ds.Tables["TToan"].Rows[i]["TgTTTBChu"] = docTien + ". " + Environment.NewLine + thueGTGTBanHang;
                                            report.DataSource = ds;

                                        }
                                    }
                                }
                            }

                        }
                        catch (Exception)
                        {
                        }

                        #region Them Hien Thi Gia khung hinh
                        try
                        {
                            //if (tblHoadon.Rows[0]["error_status"].ToString() == "1")
                            //{
                            //    int pageCount = report.Pages.Count;
                            //    Watermark pmk = report.Watermark;
                            //    pmk.Text = tblHoadon.Rows[0]["is_hdcma"].ToString() == "1" ? "CQT không cấp mã" : (tblHoadon.Rows[0]["is_hdcma"].ToString() == "0" ? "CQT không tiếp nhận HĐ" : "");
                            //    pmk.TextDirection = DirectionMode.ForwardDiagonal;
                            //    pmk.Font = new Font("Times New Roman", 50);
                            //    pmk.TextTransparency = 150;
                            //    pmk.PageRange = "1-" + pageCount;
                            //}
                            //else
                            //{
                            if (tblHoadon.Rows[0]["tthdon"].ToString() == "3" || tblHoadon.Rows[0]["tthdon"].ToString() == "17")
                            {
                                // Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                                // report.Watermark.Image = bmp;
                                Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                                int pageCount = report.Pages.Count;

                                Watermark pmk = report.Watermark;
                                pmk.Image = bmp;
                                pmk.PageRange = "1-" + pageCount;

                            }
                            //}
                        }
                        catch (Exception ex)
                        {
                        }
                        #endregion

                        report.CreateDocument();

                        MemoryStream ms = new MemoryStream();
                        #region HTML
                        if (type == "Html")
                        {
                            report.ExportOptions.Html.EmbedImagesInHTML = true;
                            report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                            report.ExportOptions.Html.Title = "Hóa đơn điện tử";
                            report.ExportToHtml(ms);

                            string html = Encoding.UTF8.GetString(ms.ToArray());

                            HtmlDocument doc = new HtmlDocument();
                            doc.LoadHtml(html);


                            string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                            string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                            var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                            if (nodes != null)
                            {
                                foreach (HtmlNode node in nodes)
                                {
                                    string eventMouseDown = node.Attributes["onmousedown"].Value;

                                    if (eventMouseDown.Contains("showCert('seller')"))
                                    {
                                        node.SetAttributeValue("id", "certSeller");
                                    }
                                    if (eventMouseDown.Contains("showCert('buyer')"))
                                    {
                                        node.SetAttributeValue("id", "certBuyer");
                                    }
                                    if (eventMouseDown.Contains("showCert('vacom')"))
                                    {
                                        node.SetAttributeValue("id", "certVacom");
                                    }
                                    if (eventMouseDown.Contains("showCert('minvoice')"))
                                    {
                                        node.SetAttributeValue("id", "certMinvoice");
                                    }
                                }
                            }

                            HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                            HtmlNode xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("id", "xmlData");
                            xmlNode.SetAttributeValue("type", "text/xmldata");

                            xmlNode.AppendChild(doc.CreateTextNode(xml));
                            head.AppendChild(xmlNode);

                            xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                            head.AppendChild(xmlNode);

                            xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                            head.AppendChild(xmlNode);

                            xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("type", "text/javascript");

                            xmlNode.InnerHtml = "$(function () { "
                                               + "  var url = 'http://localhost:19898/signalr'; "
                                               + "  var connection = $.hubConnection(url, {  "
                                               + "     useDefaultPath: false "
                                               + "  });"
                                               + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                               + " invoiceHubProxy.on('resCommand', function (result) { "
                                               + " }); "
                                               + " connection.start().done(function () { "
                                               + "      console.log('Connect to the server successful');"
                                               + "      $('#certSeller').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'seller' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "      $('#certVacom').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'vacom' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "      $('#certBuyer').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'buyer' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "      $('#certMinvoice').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'minvoice' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "})"
                                               + ".fail(function () { "
                                               + "     alert('failed in connecting to the signalr server'); "
                                               + "});"
                                               + "});";

                            head.AppendChild(xmlNode);

                            if (report.Watermark != null)
                            {
                                if (report.Watermark.Image != null)
                                {
                                    ImageConverter _imageConverter = new ImageConverter();
                                    byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                                    string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                                    HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                                    string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                                    string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                                    HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                                    style.AppendChild(textNode);

                                    HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                                    HtmlNodeCollection pageNodes = body.SelectNodes("div");

                                    foreach (var pageNode in pageNodes)
                                    {
                                        pageNode.Attributes.Add("class", "waterMark");

                                        string divStyle = pageNode.Attributes["style"].Value;
                                        divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                        pageNode.Attributes["style"].Value = divStyle;
                                    }
                                }
                            }

                            ms.SetLength(0);
                            doc.Save(ms);

                            doc = null;
                        }
                        #endregion
                        else if (type == "Image")
                        {
                            var options = new ImageExportOptions(ImageFormat.Png)
                            {
                                ExportMode = ImageExportMode.SingleFilePageByPage,
                            };
                            report.ExportToImage(ms, options);
                        }
                        else if (type == "Excel")
                        {
                            report.ExportToXlsx(ms);
                        }
                        else if (type == "Rtf")
                        {
                            report.ExportToRtf(ms);
                        }
                        else
                        {
                            report.ExportToPdf(ms);
                        }

                        bytes = ms.ToArray();
                        ms.Close();

                        if (bytes == null)
                        {
                            throw new Exception("null");
                        }
                    }
                    else
                    {
                        throw new Exception("Không tìm thấy dữ liệu");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

            return bytes;
        }
        public byte[] PrintInvoiceSBMEmail(string sobaomat, string masothue, string folder, string type, bool inchuyendoi)
        {
            _minvoiceDbContext.SetSiteHddt(masothue);
            //var db = this._nopDbContext2.GetInvoiceDb();

            byte[] bytes = null;

            string xml = "";
            string xml_thue = "";
            string msg_tb = "";

            try
            {
                // Guid inv_InvoiceAuth_id = Guid.Parse(id);
                string sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.inv_InvoiceAuth WHERE security_number =@sobaomat";

                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("sobaomat", sobaomat);

                DataTable tblInv_InvoiceAuth = this._minvoiceDbContext.GetDataTable(sqlInvoiceAuth, CommandType.Text, parameter);
                if (tblInv_InvoiceAuth.Rows.Count == 0)
                {
                    // kiểm tra TT78
                    sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE sbmat =@sobaomat";
                    DataTable tblHoadon = this._minvoiceDbContext.GetDataTable(sqlInvoiceAuth, CommandType.Text, parameter);
                    if (tblHoadon.Rows.Count == 0)
                    {
                        throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                    }
                    //throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                }

                // TT32
                if (tblInv_InvoiceAuth.Rows.Count > 0)
                {
                    string inv_InvoiceAuth_id = tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString();

                    string sqlInvoiceDetail = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauthdetail WHERE inv_invoiceauth_id = @inv_invoiceauth_id";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_invoiceauth_id", Guid.Parse(inv_InvoiceAuth_id));

                    DataTable tblInv_InvoiceAuthDetail = this._minvoiceDbContext.GetDataTable(sqlInvoiceDetail, CommandType.Text, parameter);

                    string sqlXmlData = "SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_InvoiceAuth_id";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_InvoiceAuth_id", Guid.Parse(inv_InvoiceAuth_id));

                    DataTable tblInvoiceXmlData = this._minvoiceDbContext.GetDataTable(sqlXmlData, CommandType.Text, parameter);

                    if (tblInvoiceXmlData.Rows.Count > 0)
                    {
                        xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                    }
                    else
                    {
                        string sqlExportXml = "SELECT #SCHEMA_NAME#.sproc_export_XmlInvoice(@inv_InvoiceAuth_id)";
                        parameter = new Dictionary<string, object>();
                        parameter.Add("inv_InvoiceAuth_id", Guid.Parse(inv_InvoiceAuth_id));

                        DataTable tbl_xml = _minvoiceDbContext.GetDataTable(sqlExportXml, CommandType.Text, parameter);
                        xml = tbl_xml.Rows[0][0].ToString();
                    }
                    xml = ReportUtil.ReverseXml(xml);
                    //}
                    //var invoiceDb = this._nopDbContext2.GetInvoiceDb();
                    string inv_InvoiceCode_id = tblInv_InvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();
                    int trang_thai_hd = Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["invoice_status"]);
                    string inv_originalId = tblInv_InvoiceAuth.Rows[0]["original_invoice_id"].ToString();
                    string user_name = _webHelper.GetUser();
                    // wb_user wbuser = invoiceDb.WbUsers.Where(c => c.username == user_name).FirstOrDefault<wb_user>();

                    string sqlNotiDetail = "SELECT * FROM #SCHEMA_NAME#.inv_notificationdetail a INNER JOIN #SCHEMA_NAME#.inv_notification b ON a.inv_notification_id=b.inv_notification_id \n" +
                        "WHERE a.inv_notificationdetail_id=@inv_notificationdetail_id";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_notificationdetail_id", Guid.Parse(inv_InvoiceCode_id));

                    DataTable tblCtthongbao = this._minvoiceDbContext.GetDataTable(sqlNotiDetail, CommandType.Text, parameter);
                    string hang_nghin = ".";
                    string thap_phan = ",";
                    DataColumnCollection columns = tblCtthongbao.Columns;
                    if (columns.Contains("decimal_point"))
                    {
                        thap_phan = tblCtthongbao.Rows[0]["decimal_point"].ToString();
                    }
                    if (columns.Contains("thousand_point"))
                    {
                        hang_nghin = tblCtthongbao.Rows[0]["thousand_point"].ToString();
                    }
                    if (hang_nghin == null || hang_nghin == "")
                    {
                        hang_nghin = ".";
                    }
                    if (thap_phan == "" || thap_phan == null)
                    {
                        thap_phan = ",";
                    }
                    //string hang_nghin = tblCtthongbao.Rows[0]["hang_nghin"].ToString();
                    //string thap_phan = tblCtthongbao.Rows[0]["thap_phan"].ToString();

                    //string cacheReportKey = string.Format(CachePattern.INVOICE_REPORT_PATTERN_KEY + "{0}", tblCtthongbao.Rows[0]["sl_invoice_template_id"]);

                    //XtraReport report = _cacheManager.Get<XtraReport>(cacheReportKey);
                    XtraReport report = new XtraReport();
                    report = null;

                    if (report == null)
                    {
                        string sqlTemplate = "SELECT * FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id";
                        parameter = new Dictionary<string, object>();
                        parameter.Add("sl_invoice_template_id", Guid.Parse(tblCtthongbao.Rows[0]["sl_invoice_template_id"].ToString()));

                        DataTable tblDmmauhd = this._minvoiceDbContext.GetDataTable(sqlTemplate, CommandType.Text, parameter);
                        string invReport = tblDmmauhd.Rows[0]["report"].ToString();

                        if (invReport.Length > 0)
                        {
                            report = ReportUtil.LoadReportFromString(invReport);
                            //_cacheManager.Set(cacheReportKey, report, 30);
                        }
                        else
                        {
                            throw new Exception("Không tải được mẫu hóa đơn");
                        }

                    }

                    report.Name = "XtraReport1";
                    report.ScriptReferencesString = "AccountSignature.dll";

                    DataSet ds = new DataSet();

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                    {
                        ds.ReadXmlSchema(xmlReader);
                        xmlReader.Close();
                    }

                    using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                    {
                        ds.ReadXml(xmlReader);
                        xmlReader.Close();
                    }

                    if (ds.Tables.Contains("TblXmlData"))
                    {
                        ds.Tables.Remove("TblXmlData");
                    }

                    DataTable tblXmlData = new DataTable();
                    tblXmlData.TableName = "TblXmlData";
                    tblXmlData.Columns.Add("data");

                    DataRow r = tblXmlData.NewRow();
                    r["data"] = xml;
                    tblXmlData.Rows.Add(r);
                    ds.Tables.Add(tblXmlData);

                    string datamember = report.DataMember;

                    if (datamember.Length > 0)
                    {
                        if (ds.Tables.Contains(datamember))
                        {
                            DataTable tblChiTiet = ds.Tables[datamember];

                            int rowcount = ds.Tables[datamember].Rows.Count;


                        }
                    }

                    if (trang_thai_hd == 11 || trang_thai_hd == 13 || trang_thai_hd == 17)
                    {
                        if (!string.IsNullOrEmpty(inv_originalId))
                        {
                            string sqlInvAuth = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE inv_invoiceauth_id=@inv_invoiceauth_id ";
                            parameter = new Dictionary<string, object>();
                            parameter.Add("inv_invoiceauth_id", Guid.Parse(inv_originalId));

                            DataTable tblInv = this._minvoiceDbContext.GetDataTable(sqlInvAuth, CommandType.Text, parameter);
                            string inv_adjustmentType = tblInv.Rows[0]["adjustment_type"].ToString();

                            string loai = inv_adjustmentType.ToString() == "5" || inv_adjustmentType.ToString() == "19" || inv_adjustmentType.ToString() == "21" ? "điều chỉnh" : inv_adjustmentType.ToString() == "3" ? "thay thế" : inv_adjustmentType.ToString() == "7" ? "xóa bỏ" : "";

                            if (inv_adjustmentType.ToString() == "5" || inv_adjustmentType.ToString() == "7" || inv_adjustmentType.ToString() == "3" || inv_adjustmentType.ToString() == "19" || inv_adjustmentType.ToString() == "21")
                            {
                                msg_tb = "Hóa đơn bị " + loai + " bởi hóa đơn số: " + tblInv.Rows[0]["invoice_number"] + " ngày " + string.Format("{0:dd/MM/yyyy}", tblInv.Rows[0]["invoice_issued_date"]) + ", mẫu số " + tblInv.Rows[0]["template_code"] + " ký hiệu " + tblInv.Rows[0]["invoice_series"];

                            }
                        }
                    }

                    if (Convert.ToInt32(tblInv_InvoiceAuth.Rows[0]["adjustment_type"]) == 7)
                    {
                        msg_tb = "";
                    }

                    if (report.Parameters["MSG_TB"] != null)
                    {
                        report.Parameters["MSG_TB"].Value = msg_tb;
                    }

                    var lblHoaDonMau = report.AllControls<XRLabel>().Where(c => c.Name == "lblHoaDonMau").FirstOrDefault<XRLabel>();

                    if (lblHoaDonMau != null)
                    {
                        lblHoaDonMau.Visible = false;
                    }

                    // Mã tra cứu hoá đơn
                    string sbmat = tblInv_InvoiceAuth.Rows[0]["security_number"].ToString();
                    if (report.Parameters["security_number"] != null)
                    {
                        report.Parameters["security_number"].Value = sbmat;
                    }

                    if (inchuyendoi)
                    {
                        var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                        if (tblInChuyenDoi != null)
                        {
                            tblInChuyenDoi.Visible = true;
                        }

                        if (report.Parameters["MSG_HD_TITLE"] != null)
                        {
                            report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                        }

                        //if (report.Parameters["NGUOI_IN_CDOI"] != null)
                        //{
                        //    report.Parameters["NGUOI_IN_CDOI"].Value = wbuser.ten_nguoi_sd == null ? "" : wbuser.ten_nguoi_sd;
                        //    report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                        //}

                        if (report.Parameters["NGAY_IN_CDOI"] != null)
                        {
                            report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                            report.Parameters["NGAY_IN_CDOI"].Visible = true;
                        }
                    }

                    report.DataSource = ds;

                    try
                    {
                        bool giamthuebanhang20 = tblInv_InvoiceAuth.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblInv_InvoiceAuth.Rows[0]["giamthuebanhang20"].ToString());
                        string dvtte = tblInv_InvoiceAuth.Rows[0]["currency_code"]?.ToString();

                        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                        string tongTienGiam = double.Parse(DoubleStrToString(tblInv_InvoiceAuth.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                        tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");
                        string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";
                        if (giamthuebanhang20)
                        {
                            var dt = ds.Tables["invoiceinformation"];
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Columns.Contains("amount_by_word"))
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        var docTien = (dt.Rows[i]["amount_by_word"]?.ToString()).Trim();
                                        docTien = String.IsNullOrEmpty(docTien) ? "Không đồng." : docTien;
                                        ds.Tables["invoiceinformation"].Rows[i]["amount_by_word"] = docTien + " " + Environment.NewLine + thueGTGTBanHang;
                                        report.DataSource = ds;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    var t = Task.Run(() =>
                    {
                        var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        newCulture.NumberFormat.NumberDecimalSeparator = thap_phan;
                        newCulture.NumberFormat.NumberGroupSeparator = hang_nghin;

                        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
                        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;

                        report.CreateDocument();

                    });

                    t.Wait();

                    if (tblInv_InvoiceAuth.Columns.Contains("list_number"))
                    {
                        if (tblInv_InvoiceAuth.Rows[0]["list_number"].ToString().Length > 0)
                        {
                            string fileName = folder + "\\BangKeDinhKem.repx";

                            XtraReport rpBangKe = null;

                            if (!File.Exists(fileName))
                            {
                                rpBangKe = new XtraReport();
                                rpBangKe.SaveLayout(fileName);
                            }
                            else
                            {
                                rpBangKe = XtraReport.FromFile(fileName, true);
                            }

                            rpBangKe.ScriptReferencesString = "AccountSignature.dll";
                            rpBangKe.Name = "rpBangKe";
                            rpBangKe.DisplayName = "BangKeDinhKem.repx";

                            rpBangKe.DataSource = report.DataSource;

                            rpBangKe.CreateDocument();
                            report.Pages.AddRange(rpBangKe.Pages);
                        }



                    }

                    if (tblInv_InvoiceAuth.Rows[0]["invoice_status"].ToString() == "7")
                    {

                        Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        int pageCount = report.Pages.Count;


                        for (int i = 0; i < pageCount; i++)
                        {
                            Page page = report.Pages[i];
                            PageWatermark pmk = new PageWatermark();
                            pmk.Image = bmp;
                            page.AssignWatermark(pmk);
                        }

                        string fileName = folder + "\\BienBanXoaBo.repx";
                        XtraReport rpBienBan = XtraReport.FromFile(fileName, true);

                        rpBienBan.ScriptReferencesString = "AccountSignature.dll";
                        rpBienBan.Name = "rpBienBan";
                        rpBienBan.DisplayName = "BienBanXoaBo.repx";

                        rpBienBan.DataSource = report.DataSource;
                        rpBienBan.DataMember = report.DataMember;

                        rpBienBan.CreateDocument();

                        rpBienBan.PrintingSystem.ContinuousPageNumbering = false;
                        report.PrintingSystem.ContinuousPageNumbering = false;

                        report.Pages.AddRange(rpBienBan.Pages);

                        int idx = pageCount;
                        pageCount = report.Pages.Count;

                        for (int i = idx; i < pageCount; i++)
                        {
                            PageWatermark pmk = new PageWatermark();
                            pmk.ShowBehind = false;
                            report.Pages[i].AssignWatermark(pmk);
                        }

                    }

                    if (trang_thai_hd == 13 || trang_thai_hd == 17)
                    {
                        Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                        int pageCount = report.Pages.Count;

                        for (int i = 0; i < pageCount; i++)
                        {
                            PageWatermark pmk = new PageWatermark();
                            pmk.Image = bmp;
                            report.Pages[i].AssignWatermark(pmk);
                        }
                    }



                    MemoryStream ms = new MemoryStream();

                    if (type == "Html")
                    {
                        report.ExportOptions.Html.EmbedImagesInHTML = true;
                        report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                        report.ExportOptions.Html.Title = "Hóa đơn điện tử M-Invoice";
                        report.ExportToHtml(ms);

                        string html = Encoding.UTF8.GetString(ms.ToArray());

                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(html);


                        string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                        string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                        var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");
                        //td[@onmousedown]

                        if (nodes != null)
                        {
                            foreach (HtmlNode node in nodes)
                            {
                                string eventMouseDown = node.Attributes["onmousedown"].Value;

                                if (eventMouseDown.Contains("showCert('seller')"))
                                {
                                    node.SetAttributeValue("id", "certSeller");
                                }
                                if (eventMouseDown.Contains("showCert('buyer')"))
                                {
                                    node.SetAttributeValue("id", "certBuyer");
                                }
                                if (eventMouseDown.Contains("showCert('vacom')"))
                                {
                                    node.SetAttributeValue("id", "certVacom");
                                }
                                if (eventMouseDown.Contains("showCert('minvoice')"))
                                {
                                    node.SetAttributeValue("id", "certMinvoice");
                                }
                            }
                        }

                        HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                        HtmlNode xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("id", "xmlData");
                        xmlNode.SetAttributeValue("type", "text/xmldata");

                        xmlNode.AppendChild(doc.CreateTextNode(xml));
                        head.AppendChild(xmlNode);

                        xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                        head.AppendChild(xmlNode);

                        xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                        head.AppendChild(xmlNode);

                        xmlNode = doc.CreateElement("script");
                        xmlNode.SetAttributeValue("type", "text/javascript");

                        xmlNode.InnerHtml = "$(function () { "
                                           + "  var url = 'http://localhost:19898/signalr'; "
                                           + "  var connection = $.hubConnection(url, {  "
                                           + "     useDefaultPath: false "
                                           + "  });"
                                           + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                           + " invoiceHubProxy.on('resCommand', function (result) { "
                                           + " }); "
                                           + " connection.start().done(function () { "
                                           + "      console.log('Connect to the server successful');"
                                           + "      $('#certSeller').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'seller' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "      $('#certVacom').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'vacom' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "      $('#certBuyer').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'buyer' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "      $('#certMinvoice').click(function () { "
                                           + "         var arg = { "
                                           + "              xml: document.getElementById('xmlData').innerHTML, "
                                           + "              id: 'minvoice' "
                                           + "         }; "
                                           + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                           + "         }); "
                                           + "})"
                                           + ".fail(function () { "
                                           + "     alert('failed in connecting to the signalr server'); "
                                           + "});"
                                           + "});";

                        head.AppendChild(xmlNode);

                        if (report.Watermark != null)
                        {
                            if (report.Watermark.Image != null)
                            {
                                ImageConverter _imageConverter = new ImageConverter();
                                byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                                string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                                HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                                string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                                string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                                HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                                style.AppendChild(textNode);

                                HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                                HtmlNodeCollection pageNodes = body.SelectNodes("div");

                                foreach (var pageNode in pageNodes)
                                {
                                    pageNode.Attributes.Add("class", "waterMark");

                                    string divStyle = pageNode.Attributes["style"].Value;
                                    divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                    pageNode.Attributes["style"].Value = divStyle;
                                }
                            }
                        }

                        ms.SetLength(0);
                        doc.Save(ms);

                        doc = null;
                    }
                    else if (type == "Image")
                    {
                        var options = new ImageExportOptions(ImageFormat.Png)
                        {
                            ExportMode = ImageExportMode.SingleFilePageByPage,
                        };
                        report.ExportToImage(ms, options);
                    }
                    else
                    {
                        report.ExportToPdf(ms);
                    }

                    bytes = ms.ToArray();
                    ms.Close();

                    if (bytes == null)
                    {
                        throw new Exception("null");
                    }
                }
                else
                {
                    // TT78
                    sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE sbmat =@sobaomat";
                    DataTable tblHoadon = this._minvoiceDbContext.GetDataTable(sqlInvoiceAuth, CommandType.Text, parameter);
                    if (tblHoadon.Rows.Count == 0)
                    {
                        throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                    }
                    else if (tblHoadon.Rows.Count > 0)
                    {
                        Guid hdon_id = Guid.Parse(tblHoadon.Rows[0]["hdon_id"].ToString());

                        DataTable tblInvoiceXmlData = this._minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id='" + hdon_id + "'");

                        if (tblInvoiceXmlData.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString();
                                //XDocument doc = XDocument.Parse(xml);

                                //xml = doc.Descendants("HDon").First().ToString();

                                XmlDocument document = new XmlDocument();
                                document.LoadXml(xml);
                                XmlNode nodeHDon = document.SelectSingleNode("/TDiep/DLieu/HDon");
                                xml = nodeHDon.OuterXml;
                            }
                            else if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString();
                            }
                            else
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml"].ToString();
                            }

                        }
                        else
                        {
                            throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                        }

                        string sqlDetails = $"SELECT  * FROM #SCHEMA_NAME#.hoadon68_chitiet WHERE hdon_id ='{hdon_id}' order by Cast (stt as numeric)";
                        DataTable detail = _minvoiceDbContext.GetDataTable(sqlDetails);

                        string[] notGet = new string[] { "kmai", "stt", "ma", "ten", "mdvtinh", "sluong", "dgia", "tlckhau", "stckhau", "thtien", "tsuat", "tsuatstr", "hdon_id", "cthdon_id" };

                        string cctbao_id = tblHoadon.Rows[0]["cctbao_id"].ToString();
                        int trang_thai_hd = Convert.ToInt32(tblHoadon.Rows[0]["tthdon"]);
                        string sbmat = tblHoadon.Rows[0]["sbmat"].ToString();

                        string SqlDongMau = $"Select sdmau from #SCHEMA_NAME#.quanlykyhieu68 where qlkhsdung_id = '{cctbao_id}'";
                        DataTable dongMau = _minvoiceDbContext.GetDataTable(SqlDongMau);
                        int soDongMau = 1;
                        try
                        {
                            soDongMau = Convert.ToInt32(dongMau.Rows[0][0].ToString());
                        }
                        catch
                        {

                        }

                        xml = FunctionUtil.AddXml(xml, detail, notGet, "HHDVu", cctbao_id, soDongMau);

                        XtraReport report = new XtraReport();

                        DataTable tblKyhieu = _minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + cctbao_id + "'");
                        DataTable tblDmmauhd = _minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + tblKyhieu.Rows[0]["qlmtke_id"] + "'");
                        string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

                        if (invReport.Length > 0)
                        {
                            report = ReportUtil.LoadReportFromString(invReport);
                            // _cacheManager.Set(cacheReportKey, report, 30);
                        }
                        else
                        {
                            throw new Exception("Không tải được mẫu hóa đơn");
                        }

                        report.Name = "XtraReport1";
                        report.ScriptReferencesString = "AccountSignature.dll";

                        DataSet ds = new DataSet();

                        using (XmlReader xmlReader = XmlReader.Create(new StringReader(report.DataSourceSchema)))
                        {
                            ds.ReadXmlSchema(xmlReader);
                            xmlReader.Close();
                        }

                        using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml)))
                        {
                            ds.ReadXml(xmlReader);
                            xmlReader.Close();
                        }

                        if (ds.Tables.Contains("TblXmlData"))
                        {
                            ds.Tables.Remove("TblXmlData");
                        }


                        DataTable tblXmlData = new DataTable();
                        tblXmlData.TableName = "TblXmlData";
                        tblXmlData.Columns.Add("data");

                        DataRow r = tblXmlData.NewRow();
                        r["data"] = xml;
                        tblXmlData.Rows.Add(r);
                        ds.Tables.Add(tblXmlData);

                        string datamember = report.DataMember;

                        if (datamember.Length > 0)
                        {
                            if (ds.Tables.Contains(datamember))
                            {
                                DataTable tblChiTiet = ds.Tables[datamember];
                                int rowcount = ds.Tables[datamember].Rows.Count;
                            }
                        }
                        if (tblHoadon.Rows[0]["hdon68_id_lk"] != null &&
                    !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString()))
                        {
                            if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "3")
                            {
                                Guid hoadonIdLK =
                                    tblHoadon.Rows[0]["hdon68_id_lk"] != null &&
                                    !string.IsNullOrEmpty(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                        ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                        : hdon_id;
                                string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                             + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                                DataTable tblInv = _minvoiceDbContext.GetDataTable(sql);

                                if (tblInv.Rows.Count > 0)
                                {
                                    //    string msg = tblInv.Rows[0]["msg"].ToString();

                                    //    if (msg.Length > 0)
                                    //    {
                                    //        DataTable tblInvoiceStatus = await _minvoiceDbContext.GetDataTableAsync("SELECT name FROM #SCHEMA_NAME#.inv_invoicestatus WHERE code=" + trang_thai_hd);
                                    //        string loai = tblInvoiceStatus.Rows[0]["name"].ToString().ToLower();

                                    DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                                    string ngay = tdlapDate.Day.ToString();
                                    string thang = tdlapDate.Month.ToString();
                                    string nam = tdlapDate.Year.ToString();

                                    msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                             tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                             + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                             tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                                                                  " năm " + nam;

                                }
                                //}
                            }

                            if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "2")
                            {
                                Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                                    ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                    : hdon_id;
                                string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                             + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                                DataTable tblInv = _minvoiceDbContext.GetDataTable(sql);

                                if (tblInv.Rows.Count > 0)
                                {
                                    DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                                    string ngay = tdlapDate.Day.ToString();
                                    string thang = tdlapDate.Month.ToString();
                                    string nam = tdlapDate.Year.ToString();

                                    msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " +
                                             tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                             + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                             tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                             " năm " + nam;
                                }
                            }

                            if (tblHoadon.Rows[0]["tthdon_original"].ToString() == "19" ||
                                tblHoadon.Rows[0]["tthdon_original"].ToString() == "21" ||
                                tblHoadon.Rows[0]["tthdon_original"].ToString() == "23")
                            {
                                Guid hoadonIdLK = tblHoadon.Rows[0]["hdon68_id_lk"] != null
                                    ? Guid.Parse(tblHoadon.Rows[0]["hdon68_id_lk"].ToString())
                                    : hdon_id;
                                string sql = "SELECT * FROM #SCHEMA_NAME#.hoadon68 a "
                                             + "WHERE a.hdon_id='" + hoadonIdLK + "'";

                                DataTable tblInv = _minvoiceDbContext.GetDataTable(sql);

                                if (tblInv.Rows.Count > 0)
                                {
                                    DateTime tdlapDate = DateTime.Parse(tblInv.Rows[0]["nlap"].ToString());

                                    string ngay = tdlapDate.Day.ToString();
                                    string thang = tdlapDate.Month.ToString();
                                    string nam = tdlapDate.Year.ToString();

                                    msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " +
                                             tblInv.Rows[0]["khieu"].ToString().Substring(0, 1) + " ký hiệu "
                                             + tblInv.Rows[0]["khieu"].ToString().Substring(1) + " số " +
                                             tblInv.Rows[0]["shdon"].ToString() + " ngày " + ngay + " tháng " + thang +
                                             " năm " + nam;
                                }
                            }
                        }

                        else
                        {
                            if (trang_thai_hd == 5 || trang_thai_hd == 19 || trang_thai_hd == 21 || trang_thai_hd == 23)
                            {
                                string kyHieuMauSoLQ = tblHoadon.Rows[0]["khmshdclquan"].ToString();
                                string kyHieuHoaDonLQ = tblHoadon.Rows[0]["khhdclquan"].ToString();
                                string soHoaDonLQ = tblHoadon.Rows[0]["shdclquan"].ToString();
                                DateTime ngayLap = DateTime.Parse(tblHoadon.Rows[0]["nlhdclquan"].ToString());
                                string ngay = ngayLap.Day.ToString();
                                string thang = ngayLap.Month.ToString();
                                string nam = ngayLap.Year.ToString();
                                msg_tb = "Hóa đơn điều chỉnh cho hóa đơn Mẫu số " + kyHieuMauSoLQ + " ký hiệu " +
                                         kyHieuHoaDonLQ
                                         + " số " + soHoaDonLQ + " ngày " + ngay + " tháng " + thang + " năm " + nam;
                            }

                            if (trang_thai_hd == 2)
                            {
                                string kyHieuMauSoLQ = tblHoadon.Rows[0]["khmshdclquan"].ToString();
                                string kyHieuHoaDonLQ = tblHoadon.Rows[0]["khhdclquan"].ToString();
                                string soHoaDonLQ = tblHoadon.Rows[0]["shdclquan"].ToString();
                                DateTime ngayLap = DateTime.Parse(tblHoadon.Rows[0]["nlhdclquan"].ToString());
                                string ngay = ngayLap.Day.ToString();
                                string thang = ngayLap.Month.ToString();
                                string nam = ngayLap.Year.ToString();
                                msg_tb = "Hóa đơn thay thế cho hóa đơn Mẫu số " + kyHieuMauSoLQ + " ký hiệu " +
                                         kyHieuHoaDonLQ
                                         + " số " + soHoaDonLQ + " ngày " + ngay + " tháng " + thang + " năm " + nam;
                            }
                        }

                        if (tblHoadon.Rows[0]["dvtte"] != null && tblHoadon.Rows[0]["dvtte"].ToString() == "USD" && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgia"].ToString()))
                        {
                            decimal tienthanhtoan = 0;
                            if (tblHoadon.Rows[0]["tgtttbso_last"] != null && !string.IsNullOrEmpty(tblHoadon.Rows[0]["tgtttbso_last"].ToString()))
                            {
                                tienthanhtoan = Convert.ToDecimal(tblHoadon.Rows[0]["tgtttbso_last"].ToString());
                            }
                            decimal tigia = Convert.ToDecimal(tblHoadon.Rows[0]["tgia"].ToString());
                            decimal tienquydoi = tienthanhtoan * tigia;
                            if (report.Parameters["QUYDOI"] != null)
                            {
                                report.Parameters["QUYDOI"].Value = tienquydoi.ToString();
                            }
                        }
                        if (report.Parameters["MSG_TB"] != null)
                        {
                            report.Parameters["MSG_TB"].Value = msg_tb;
                        }

                        if (report.Parameters["MCCQT"] != null)
                        {
                            report.Parameters["MCCQT"].Value = tblHoadon.Rows[0]["mccqthue"].ToString();
                        }

                        // Mã tra cứu hoá đơn
                        if (report.Parameters["SoBaoMat"] != null)
                        {
                            report.Parameters["SoBaoMat"].Value = sbmat;
                        }

                        try
                        {

                            if (report.Parameters["SoThapPhan"] != null)
                            {
                                string sql = "SELECT * FROM #SCHEMA_NAME#.sl_currency a "
                                             + "WHERE a.code='" + tblHoadon.Rows[0]["dvtte"].ToString() + "' limit 1";

                                DataTable currency = _minvoiceDbContext.GetDataTable(sql);
                                if (currency.Rows.Count > 0)
                                {
                                    report.Parameters["SoThapPhan"].Value = currency.Rows[0]["hoadon123"].ToString();
                                }
                            }
                        }
                        catch
                        {

                        }

                        if (inchuyendoi)
                        {
                            var tblInChuyenDoi = report.AllControls<XRTable>().Where(c => c.Name == "tblInChuyenDoi").FirstOrDefault<XRTable>();

                            if (tblInChuyenDoi != null)
                            {
                                tblInChuyenDoi.Visible = true;
                            }

                            if (report.Parameters["MSG_HD_TITLE"] != null)
                            {
                                report.Parameters["MSG_HD_TITLE"].Value = "Hóa đơn chuyển đổi từ hóa đơn điện tử";
                            }

                            if (report.Parameters["NGUOI_IN_CDOI"] != null)
                            {
                                report.Parameters["NGUOI_IN_CDOI"].Value = _webHelper.GetUser();
                                report.Parameters["NGUOI_IN_CDOI"].Visible = true;
                            }

                            if (report.Parameters["NGAY_IN_CDOI"] != null)
                            {
                                report.Parameters["NGAY_IN_CDOI"].Value = DateTime.Now;
                                report.Parameters["NGAY_IN_CDOI"].Visible = true;
                            }
                        }

                        report.DataSource = ds;

                        try
                        {
                            bool giamthuebanhang20 = tblHoadon.Rows[0].Table.Columns.Contains("giamthuebanhang20") && Boolean.Parse(tblHoadon.Rows[0]["giamthuebanhang20"].ToString());
                            string dvtte = tblHoadon.Rows[0]["dvtte"]?.ToString();

                            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                            string tongTienGiam = double.Parse(DoubleStrToString(tblHoadon.Rows[0]["tienthuegtgtgiam"].ToString())).ToString("#,###", cul.NumberFormat);
                            tongTienGiam = (tongTienGiam == "" ? "0" : tongTienGiam) + (dvtte == "USD" ? " USD" : " đồng");
                            string thueGTGTBanHang = "(Đã giảm " + tongTienGiam + " tương ứng 20% mức tỷ lệ % để tính thuế giá trị gia tăng theo Nghị quyết số 43/2022/QH15)";

                            if (giamthuebanhang20)
                            {
                                var dt = ds.Tables["TToan"];
                                if (dt.Rows.Count > 0)
                                {
                                    if (dt.Columns.Contains("TgTTTBChu"))
                                    {
                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            var docTien = (dt.Rows[i]["TgTTTBChu"]?.ToString()).Trim();

                                            ds.Tables["TToan"].Rows[i]["TgTTTBChu"] = docTien + ". " + Environment.NewLine + thueGTGTBanHang;
                                            report.DataSource = ds;

                                        }
                                    }
                                }
                            }

                        }
                        catch (Exception)
                        {
                        }

                        #region Them Hien Thi Gia khung hinh
                        try
                        {
                            //if (tblHoadon.Rows[0]["error_status"].ToString() == "1")
                            //{
                            //    int pageCount = report.Pages.Count;
                            //    Watermark pmk = report.Watermark;
                            //    pmk.Text = tblHoadon.Rows[0]["is_hdcma"].ToString() == "1" ? "CQT không cấp mã" : (tblHoadon.Rows[0]["is_hdcma"].ToString() == "0" ? "CQT không tiếp nhận HĐ" : "");
                            //    pmk.TextDirection = DirectionMode.ForwardDiagonal;
                            //    pmk.Font = new Font("Times New Roman", 50);
                            //    pmk.TextTransparency = 150;
                            //    pmk.PageRange = "1-" + pageCount;
                            //}
                            //else
                            //{
                            if (tblHoadon.Rows[0]["tthdon"].ToString() == "3" || tblHoadon.Rows[0]["tthdon"].ToString() == "17")
                            {
                                // Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                                // report.Watermark.Image = bmp;
                                Bitmap bmp = ReportUtil.DrawDiagonalLine(report);
                                int pageCount = report.Pages.Count;

                                Watermark pmk = report.Watermark;
                                pmk.Image = bmp;
                                pmk.PageRange = "1-" + pageCount;

                            }
                            //}
                        }
                        catch (Exception ex)
                        {
                        }
                        #endregion

                        report.CreateDocument();

                        MemoryStream ms = new MemoryStream();
                        #region HTML
                        if (type == "Html")
                        {
                            report.ExportOptions.Html.EmbedImagesInHTML = true;
                            report.ExportOptions.Html.ExportMode = HtmlExportMode.SingleFilePageByPage;
                            report.ExportOptions.Html.Title = "Hóa đơn điện tử";
                            report.ExportToHtml(ms);

                            string html = Encoding.UTF8.GetString(ms.ToArray());

                            HtmlDocument doc = new HtmlDocument();
                            doc.LoadHtml(html);


                            string api = this._webHelper.GetRequest().ApplicationPath.StartsWith("/api") ? "/api" : "";
                            string serverApi = this._webHelper.GetRequest().Url.Scheme + "://" + this._webHelper.GetRequest().Url.Authority + api;

                            var nodes = doc.DocumentNode.SelectNodes("//td/@onmousedown");

                            if (nodes != null)
                            {
                                foreach (HtmlNode node in nodes)
                                {
                                    string eventMouseDown = node.Attributes["onmousedown"].Value;

                                    if (eventMouseDown.Contains("showCert('seller')"))
                                    {
                                        node.SetAttributeValue("id", "certSeller");
                                    }
                                    if (eventMouseDown.Contains("showCert('buyer')"))
                                    {
                                        node.SetAttributeValue("id", "certBuyer");
                                    }
                                    if (eventMouseDown.Contains("showCert('vacom')"))
                                    {
                                        node.SetAttributeValue("id", "certVacom");
                                    }
                                    if (eventMouseDown.Contains("showCert('minvoice')"))
                                    {
                                        node.SetAttributeValue("id", "certMinvoice");
                                    }
                                }
                            }

                            HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");

                            HtmlNode xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("id", "xmlData");
                            xmlNode.SetAttributeValue("type", "text/xmldata");

                            xmlNode.AppendChild(doc.CreateTextNode(xml));
                            head.AppendChild(xmlNode);

                            xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery-1.6.4.min.js");
                            head.AppendChild(xmlNode);

                            xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("src", serverApi + "/Content/Scripts/jquery.signalR-2.2.3.min.js");
                            head.AppendChild(xmlNode);

                            xmlNode = doc.CreateElement("script");
                            xmlNode.SetAttributeValue("type", "text/javascript");

                            xmlNode.InnerHtml = "$(function () { "
                                               + "  var url = 'http://localhost:19898/signalr'; "
                                               + "  var connection = $.hubConnection(url, {  "
                                               + "     useDefaultPath: false "
                                               + "  });"
                                               + " var invoiceHubProxy = connection.createHubProxy('invoiceHub'); "
                                               + " invoiceHubProxy.on('resCommand', function (result) { "
                                               + " }); "
                                               + " connection.start().done(function () { "
                                               + "      console.log('Connect to the server successful');"
                                               + "      $('#certSeller').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'seller' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "      $('#certVacom').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'vacom' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "      $('#certBuyer').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'buyer' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "      $('#certMinvoice').click(function () { "
                                               + "         var arg = { "
                                               + "              xml: document.getElementById('xmlData').innerHTML, "
                                               + "              id: 'minvoice' "
                                               + "         }; "
                                               + "         invoiceHubProxy.invoke('execCommand', 'ShowCert', JSON.stringify(arg)); "
                                               + "         }); "
                                               + "})"
                                               + ".fail(function () { "
                                               + "     alert('failed in connecting to the signalr server'); "
                                               + "});"
                                               + "});";

                            head.AppendChild(xmlNode);

                            if (report.Watermark != null)
                            {
                                if (report.Watermark.Image != null)
                                {
                                    ImageConverter _imageConverter = new ImageConverter();
                                    byte[] img = (byte[])_imageConverter.ConvertTo(report.Watermark.Image, typeof(byte[]));

                                    string imgUrl = "data:image/png;base64," + Convert.ToBase64String(img);

                                    HtmlNode style = doc.DocumentNode.SelectSingleNode("//style");

                                    string strechMode = report.Watermark.ImageViewMode == ImageViewMode.Stretch ? "background-size: 100% 100%;" : "";
                                    string waterMarkClass = ".waterMark { background-image:url(" + imgUrl + ");background-repeat:no-repeat;background-position:center;" + strechMode + " }";

                                    HtmlTextNode textNode = doc.CreateTextNode(waterMarkClass);
                                    style.AppendChild(textNode);

                                    HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");

                                    HtmlNodeCollection pageNodes = body.SelectNodes("div");

                                    foreach (var pageNode in pageNodes)
                                    {
                                        pageNode.Attributes.Add("class", "waterMark");

                                        string divStyle = pageNode.Attributes["style"].Value;
                                        divStyle = divStyle + "margin-left:auto;margin-right:auto;";

                                        pageNode.Attributes["style"].Value = divStyle;
                                    }
                                }
                            }

                            ms.SetLength(0);
                            doc.Save(ms);

                            doc = null;
                        }
                        #endregion
                        else if (type == "Image")
                        {
                            var options = new ImageExportOptions(ImageFormat.Png)
                            {
                                ExportMode = ImageExportMode.SingleFilePageByPage,
                            };
                            report.ExportToImage(ms, options);
                        }
                        else if (type == "Excel")
                        {
                            report.ExportToXlsx(ms);
                        }
                        else if (type == "Rtf")
                        {
                            report.ExportToRtf(ms);
                        }
                        else
                        {
                            report.ExportToPdf(ms);
                        }

                        bytes = ms.ToArray();
                        ms.Close();

                        if (bytes == null)
                        {
                            throw new Exception("null");
                        }

                    }
                    else
                    {
                        throw new Exception("Không tồn tại hóa đơn");
                    }
                }

            }
            catch (Exception ex)
            {
                //_logService.Insert("PrintInvoiceFromId", ex.ToString());

                throw new Exception(ex.Message.ToString());

            }

            return bytes;
        }
        public byte[] ExportZipFileXML(string sobaomat, string masothue, string pathReport, ref string fileName, ref string key)
        {
            byte[] result = null;

            try
            {
                _minvoiceDbContext.SetSiteHddt(masothue);

                string sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.inv_invoiceauth WHERE security_number=@sobaomat";
                Dictionary<string, object> parameter = new Dictionary<string, object>();
                parameter.Add("sobaomat", sobaomat);

                DataTable tblInv_InvoiceAuth = this._minvoiceDbContext.GetDataTable(sqlInvoiceAuth, CommandType.Text, parameter);
                //if (tblInv_InvoiceAuth.Rows.Count <= 0)
                //{
                //    return null;
                //}
                if (tblInv_InvoiceAuth.Rows.Count > 0)
                {
                    string mau_hd = tblInv_InvoiceAuth.Rows[0]["template_code"].ToString();
                    string sbm = tblInv_InvoiceAuth.Rows[0]["security_number"].ToString();
                    string so_serial = tblInv_InvoiceAuth.Rows[0]["invoice_series"].ToString();
                    string so_hd = tblInv_InvoiceAuth.Rows[0]["invoice_number"].ToString();
                    string inv_InvoiceCode_id = tblInv_InvoiceAuth.Rows[0]["inv_invoicecode_id"].ToString();

                    fileName = masothue + "_" + mau_hd.Replace("/", "") + "_" + so_serial.Replace("/", "").Trim() + "_" + so_hd;
                    Guid inv_InvoiceAuth_id = Guid.Parse(tblInv_InvoiceAuth.Rows[0]["inv_invoiceauth_id"].ToString());

                    string sqlXmlData = "SELECT * FROM #SCHEMA_NAME#.inv_invoicexmldata WHERE inv_invoiceauth_id=@inv_InvoiceAuth_id";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_InvoiceAuth_id", inv_InvoiceAuth_id);

                    DataTable tblInvoiceXmlData = this._minvoiceDbContext.GetDataTable(sqlXmlData, CommandType.Text, parameter);

                    string xml = "";

                    if (tblInvoiceXmlData.Rows.Count > 0)
                    {
                        xml = tblInvoiceXmlData.Rows[0]["data"].ToString();
                    }
                    else
                    {
                        throw new Exception("Không tồn tại xml hóa đơn");
                    }

                    string sqlNotifice = "SELECT * FROM  #SCHEMA_NAME#.inv_notificationdetail WHERE inv_notificationdetail_id=@inv_InvoiceCode_id";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("inv_InvoiceCode_id", Guid.Parse(inv_InvoiceCode_id));

                    DataTable tblCtthongbao = this._minvoiceDbContext.GetDataTable(sqlNotifice, CommandType.Text, parameter);

                    string sqlTemplate = "SELECT sl_invoice_template_id,report FROM #SCHEMA_NAME#.sl_invoice_template WHERE sl_invoice_template_id=@sl_invoice_template_id";
                    parameter = new Dictionary<string, object>();
                    parameter.Add("sl_invoice_template_id", Guid.Parse(tblCtthongbao.Rows[0]["sl_invoice_template_id"].ToString()));

                    DataTable tblMauHoaDon = this._minvoiceDbContext.GetDataTable(sqlTemplate, CommandType.Text, parameter);


                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + xml;
                    //key = Guid.NewGuid().ToString();
                    //string encodeXML = EncodeXML.Encrypt(xml, key);

                    byte[] dataPdf = PrintInvoiceSBMEmail(sbm, masothue, pathReport, "PDF", false);

                    MemoryStream outputMemStream = new MemoryStream();
                    ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

                    zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

                    // attack file xml
                    ZipEntry newEntry = new ZipEntry(fileName + ".xml");
                    newEntry.DateTime = DateTime.Now;
                    newEntry.IsUnicodeText = true;

                    zipStream.PutNextEntry(newEntry);
                    byte[] bytes = Encoding.UTF8.GetBytes(xml);

                    MemoryStream inStream = new MemoryStream(bytes);
                    inStream.WriteTo(zipStream);
                    inStream.Close();
                    zipStream.CloseEntry();
                    // attack pdf
                    newEntry = new ZipEntry(fileName + ".pdf");
                    newEntry.DateTime = DateTime.Now;
                    newEntry.IsUnicodeText = true;

                    zipStream.PutNextEntry(newEntry);

                    inStream = new MemoryStream(dataPdf);
                    inStream.WriteTo(zipStream);
                    inStream.Close();
                    zipStream.CloseEntry();
                    // attack file key
                    //newEntry = new ZipEntry("key.txt");
                    //newEntry.DateTime = DateTime.Now;
                    //newEntry.IsUnicodeText = true;

                    //zipStream.PutNextEntry(newEntry);
                    //byte[] bytekey = Encoding.UTF8.GetBytes(key);
                    //inStream = new MemoryStream(bytekey);
                    //inStream.WriteTo(zipStream);
                    //inStream.Close();
                    //zipStream.CloseEntry();

                    inStream = new MemoryStream();
                    using (StreamWriter sw = new StreamWriter(inStream))
                    {
                        sw.Write(tblMauHoaDon.Rows[0]["report"].ToString());
                        sw.Flush();

                        newEntry = new ZipEntry(fileName + ".repx");
                        newEntry.DateTime = DateTime.Now;
                        newEntry.IsUnicodeText = true;
                        zipStream.PutNextEntry(newEntry);

                        inStream.WriteTo(zipStream);
                        inStream.Close();
                        zipStream.CloseEntry();

                        sw.Close();
                    }

                    zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
                    zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

                    outputMemStream.Position = 0;

                    result = outputMemStream.ToArray();

                    outputMemStream.Close();
                }
                else
                {
                    sqlInvoiceAuth = "SELECT * FROM #SCHEMA_NAME#.hoadon68 WHERE sbmat =@sobaomat";

                    DataTable tblHoadon = this._minvoiceDbContext.GetDataTable(sqlInvoiceAuth, CommandType.Text, parameter);
                    if (tblHoadon.Rows.Count == 0)
                    {
                        throw new Exception("Không tồn tại hóa đơn có Mã tra cứu hoá đơn " + sobaomat);
                    }

                    if (tblHoadon.Rows.Count > 0)
                    {
                        Guid hdon_id = Guid.Parse(tblHoadon.Rows[0]["hdon_id"].ToString());

                        //get kyhieu hoá đơn số hoá đơn
                        string mau_kihieu = tblHoadon.Rows[0]["khieu"].ToString();
                        string so_hd = tblHoadon.Rows[0]["shdon"].ToString();

                        fileName = masothue + "_" + mau_kihieu + "_" + so_hd;

                        DataTable tblInvoiceXmlData = this._minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.dulieuxml68 WHERE hdon_id='" + hdon_id + "'");

                        string xml = "";
                        string xml_thue = "";

                        if (tblInvoiceXmlData.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_thue"].ToString();
                                //XDocument doc = XDocument.Parse(xml);

                                //xml = doc.Descendants("HDon").First().ToString();

                                XmlDocument document = new XmlDocument();
                                document.LoadXml(xml);
                                XmlNode nodeHDon = document.SelectSingleNode("/TDiep/DLieu/HDon");
                                xml = nodeHDon.OuterXml;
                            }
                            else if (!string.IsNullOrEmpty(tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString()))
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml_ngmua"].ToString();
                            }
                            else
                            {
                                xml = tblInvoiceXmlData.Rows[0]["dlxml"].ToString();
                            }

                        }
                        else
                        {
                            throw new Exception("Không tồn tại xml hóa đơn");
                        }

                        string cctbao_id = tblHoadon.Rows[0]["cctbao_id"].ToString();
                        int trang_thai_hd = Convert.ToInt32(tblHoadon.Rows[0]["tthdon"]);

                        XtraReport report = new XtraReport();

                        DataTable tblKyhieu = _minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlykyhieu68 WHERE qlkhsdung_id='" + cctbao_id + "'");
                        DataTable tblDmmauhd = _minvoiceDbContext.GetDataTable("SELECT * FROM #SCHEMA_NAME#.quanlymau68 WHERE qlmtke_id='" + tblKyhieu.Rows[0]["qlmtke_id"] + "'");
                        string invReport = tblDmmauhd.Rows[0]["dulieumau"].ToString();

                        if (invReport.Length > 0)
                        {
                            report = ReportUtil.LoadReportFromString(invReport);
                            // _cacheManager.Set(cacheReportKey, report, 30);
                        }
                        else
                        {
                            throw new Exception("Không tải được mẫu hóa đơn");
                        }


                        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + xml;
                        //key = Guid.NewGuid().ToString();
                        //string encodeXML = EncodeXML.Encrypt(xml, key);

                        byte[] dataPdf = PrintInvoiceSBMEmail(sobaomat, masothue, pathReport, "PDF", false);

                        MemoryStream outputMemStream = new MemoryStream();
                        ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

                        zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

                        // attack file xml
                        ZipEntry newEntry = new ZipEntry(fileName + ".xml");
                        newEntry.DateTime = DateTime.Now;
                        newEntry.IsUnicodeText = true;

                        zipStream.PutNextEntry(newEntry);
                        byte[] bytes = Encoding.UTF8.GetBytes(xml);

                        MemoryStream inStream = new MemoryStream(bytes);
                        inStream.WriteTo(zipStream);
                        inStream.Close();
                        zipStream.CloseEntry();
                        // attack pdf
                        newEntry = new ZipEntry(fileName + ".pdf");
                        newEntry.DateTime = DateTime.Now;
                        newEntry.IsUnicodeText = true;

                        zipStream.PutNextEntry(newEntry);

                        inStream = new MemoryStream(dataPdf);
                        inStream.WriteTo(zipStream);
                        inStream.Close();
                        zipStream.CloseEntry();
                        // attack file key
                        //newEntry = new ZipEntry("key.txt");
                        //newEntry.DateTime = DateTime.Now;
                        //newEntry.IsUnicodeText = true;

                        //zipStream.PutNextEntry(newEntry);
                        //byte[] bytekey = Encoding.UTF8.GetBytes(key);
                        //inStream = new MemoryStream(bytekey);
                        //inStream.WriteTo(zipStream);
                        //inStream.Close();
                        //zipStream.CloseEntry();

                        inStream = new MemoryStream();
                        using (StreamWriter sw = new StreamWriter(inStream))
                        {
                            sw.Write(tblDmmauhd.Rows[0]["dulieumau"].ToString());
                            sw.Flush();

                            newEntry = new ZipEntry(fileName + ".repx");
                            newEntry.DateTime = DateTime.Now;
                            newEntry.IsUnicodeText = true;
                            zipStream.PutNextEntry(newEntry);

                            inStream.WriteTo(zipStream);
                            inStream.Close();
                            zipStream.CloseEntry();

                            sw.Close();
                        }

                        zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
                        zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

                        outputMemStream.Position = 0;

                        result = outputMemStream.ToArray();

                        outputMemStream.Close();
                    }

                    else
                    {
                        throw new Exception("Không tìm thấy dữ liệu");
                    }
                }

                return result;

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public string DoubleStrToString(string strdec)
        {
            try
            {
                if (strdec.Contains("."))
                {
                    string result = strdec.TrimEnd('0').TrimEnd('.');
                    if (result.Length > 0 && result[result.Length - 1] == '.') return result.Substring(0, result.Length - 1);
                    return strdec.Contains(".") ? strdec.TrimEnd('0').TrimEnd('.') : strdec;
                }
                else
                {
                    return strdec;
                }
            }
            catch
            {
                return strdec;
            }
        }


        public async Task<JObject> SendEmail(string masothue, string email)
        {
            JObject res = new JObject();
            try
            {
                string tax_code = ConfigurationManager.AppSettings["schema_tax_Code"];
                string sql = $@"Select * from {CommonConstants.MASTER_SCHEMA}.inv_user where email = @email and username = @masothue and tax_code=@tax_code";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("email", email);
                parameters.Add("masothue", masothue);
                parameters.Add("tax_code", tax_code);

                DataTable user = await _masterDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                if (user.Rows.Count == 0)
                {
                    throw new Exception("Không tồn tại user hoặc email không đúng");
                }
                DataRow model = user.Rows[0];
                string username = model["username"].ToString();
                string mst = model["tax_code"].ToString();


                this._minvoiceDbContext.SetSiteHddt(mst);
                DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='CREATE_CUSTOMER_LOGIN' AND yesno='C' LIMIT 1");

                if (tblEmail.Rows.Count > 0)
                {
                    string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                    if (smtp_address.Length > 0)
                    {
                        int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                        bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                        this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                    }

                    string passwordnew = FunctionUtil.RandomPassWord();

                    sql = $"UPDATE {CommonConstants.MASTER_SCHEMA}.inv_user SET password=@password,date_edit=@date_edit \n"
                                + "WHERE inv_user_id=@inv_user_id";
                    parameters = new Dictionary<string, object>();
                    parameters.Add("inv_user_id", Guid.Parse(model["inv_user_id"].ToString()));
                    parameters.Add("password", FunctionUtil.CreateHashedPassword(passwordnew, null));
                    parameters.Add("date_edit", DateTime.Now);

                    await _masterDbContext.ExecuteNoneQueryAsync(sql, parameters);


                    string subject = tblEmail.Rows[0]["subject"].ToString();
                    string body = tblEmail.Rows[0]["body"].ToString()
                                    .Replace("#username#", username)
                                    .Replace("#password#", passwordnew)
                                    .Replace("#taxcode#", tax_code)
                                    .Replace("#fullname#", user.Rows[0]["fullname"].ToString())
                                    .Replace("#trangthai#", user.Rows[0]["active"].ToString() == "1" ? "Hoạt động" : "Tạm khóa")
                                    .Replace("#url_host#", _webHelper.GetUrlHost());

                    ThreadPool.QueueUserWorkItem(state => this._emailService.Send(email, subject, body));

                    res.Add("ok", "Tạo tài khoản tra cứu thành công ! Gửi email cho khách thành công !");
                }
                else
                {
                    res.Add("ok", "Tạo tài khoản tra cứu thành công !");
                }
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }
            return res;

        }
        public async Task<JObject> RegisterAcount(string masothue, string email, string name)
        {
            JObject res = new JObject();
            try
            {
                string sql = $@"Select * from {CommonConstants.MASTER_SCHEMA}.inv_user where username = @username";

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("username", masothue);

                DataTable user = await _masterDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                if (user.Rows.Count > 0)
                {
                    throw new Exception("Đã tồn tại tên đăng nhập trong hệ thống");
                }

                string tax_code = ConfigurationManager.AppSettings["schema_tax_Code"];
                this._minvoiceDbContext.SetSiteHddt(tax_code);
                sql = "SELECT x.* FROM #SCHEMA_NAME#.pl_customer x WHERE tax_code = @tax_code";
                parameters = new Dictionary<string, object>();
                parameters.Add("tax_code", masothue);
                DataTable data_customer = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, parameters);
                string code_customer = masothue;
                if (data_customer.Rows.Count > 0)
                {
                    code_customer = data_customer.Rows[0]["code"].ToString();
                }

                sql = $@"INSERT INTO {CommonConstants.MASTER_SCHEMA}.inv_user(inv_user_id, tax_code, username, code_customer, password, email, date_new, date_edit, schema_name, schema_url,fullname)
                                            VALUES(@inv_user_id, @tax_code, @username,@code_customer, @password, @email, @date_new, @date_edit, @schema_name, @schema_url, @fullname);";

                string passwordnew = FunctionUtil.RandomPassWord();
                parameters = new Dictionary<string, object>();
                parameters.Add("inv_user_id", Guid.NewGuid());
                parameters.Add("tax_code", tax_code);
                parameters.Add("username", masothue);
                parameters.Add("code_customer", code_customer);
                parameters.Add("password", FunctionUtil.CreateHashedPassword(passwordnew, null));
                parameters.Add("email", email);
                parameters.Add("date_new", DateTime.Now);
                parameters.Add("date_edit", DateTime.Now);
                parameters.Add("schema_name", tax_code);
                parameters.Add("fullname", name);
                parameters.Add("schema_url", ConfigurationManager.ConnectionStrings["InvoiceConnection"].ConnectionString);
                await _masterDbContext.ExecuteNoneQueryAsync(sql, parameters);


                DataTable tblEmail = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_tempemail WHERE email_type='CREATE_CUSTOMER_LOGIN' AND yesno='C' LIMIT 1");

                if (tblEmail.Rows.Count > 0)
                {
                    string smtp_address = tblEmail.Rows[0]["smtp_address"].ToString();

                    if (smtp_address.Length > 0)
                    {
                        int smtp_port = Convert.ToInt32(tblEmail.Rows[0]["smtp_port"].ToString());
                        bool enable_ssl = Convert.ToBoolean(tblEmail.Rows[0]["enable_ssl"]);

                        this._emailService.SetEmailServer(smtp_address, smtp_port, enable_ssl);
                    }



                    string subject = tblEmail.Rows[0]["subject"].ToString();
                    string body = tblEmail.Rows[0]["body"].ToString()
                                    .Replace("#username#", masothue)
                                    .Replace("#password#", passwordnew)
                                    .Replace("#taxcode#", tax_code)
                                    .Replace("#fullname#", name)
                                    .Replace("#trangthai#", "Hoạt động")
                                    .Replace("#url_host#", _webHelper.GetUrlHost());

                    ThreadPool.QueueUserWorkItem(state => this._emailService.Send(email, subject, body));

                    res.Add("ok", "Tạo tài khoản tra cứu thành công !");
                }
                else
                {
                    res.Add("ok", "Tạo tài khoản tra cứu thành công, email chưa gửi được vì chưa có template !");
                }
            }
            catch (Exception ex)
            {
                res.Add("error", ex.Message);
            }
            return res;

        }
    }
}