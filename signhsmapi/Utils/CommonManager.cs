﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text.RegularExpressions;
using System.Xml;

namespace SignHsmApi.Utils
{
    public class CommonManager
    {


        public static string SignXmlHsm32(string idSigner, string xml, X509Certificate2 cert, string tagSign, string idData)
        {

            XmlDocument document = new XmlDocument();
            document.PreserveWhitespace = false;

            try
            {
                DateTime now = DateTime.Now;

                if (now < cert.NotBefore || now > cert.NotAfter)
                {
                    throw new Exception("Chứng thư số đã hết hạn hoặc chưa đến thời gian sử dụng");
                }

                //xml = xml.Replace("'", "''");
                xml = xml.Replace("\r", "").Replace("\n", "");
                document.LoadXml(xml);

                AsymmetricAlgorithm asymmetricAlgorithm = cert.PrivateKey;

                SignedXml signedXml = new SignedXml(document);
                signedXml.Signature.Id = idSigner;
                signedXml.SigningKey = asymmetricAlgorithm;

                KeyInfo keyInfo = new KeyInfo();

                RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

                KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                keyInfoData.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyInfoData);
                signedXml.KeyInfo = keyInfo;

                Reference reference = new Reference();
                reference.Uri = idData;

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();

                reference.AddTransform(env);

                XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                reference.AddTransform(env1);

                signedXml.AddReference(reference);

                signedXml.ComputeSignature();

                XmlElement xmlDigitalSignature = signedXml.GetXml();


                XmlNodeList nodeList = document.GetElementsByTagName(tagSign);

                foreach (XmlNode node in nodeList)
                {

                    node.AppendChild(xmlDigitalSignature);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return document.OuterXml;

        }

        public static string SignXmlHsm78(string idSigner, string xml, X509Certificate2 cert, string tagSign, string idData)
        {

            if (string.IsNullOrEmpty(idSigner)) idSigner = "Seller";
            XmlDocument document = new XmlDocument();
            document.PreserveWhitespace = false;

            try
            {

                DateTime now = DateTime.Now;

                if (now < cert.NotBefore || now > cert.NotAfter)
                {
                    throw new Exception("Chứng thư số đã hết hạn hoặc chưa đến thời gian sử dụng");
                }

                //xml = xml.Replace("'", "''");
                xml = xml.Replace("\r", "").Replace("\n", "");
                document.LoadXml(xml);

                AsymmetricAlgorithm asymmetricAlgorithm = cert.PrivateKey;

                SignedXml signedXml = new SignedXml(document);
                signedXml.Signature.Id = idSigner;
                signedXml.SigningKey = asymmetricAlgorithm;

                KeyInfo keyInfo = new KeyInfo();

                /*RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));*/

                KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                keyInfoData.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyInfoData);
                signedXml.KeyInfo = keyInfo;

                Reference reference = new Reference();
                reference.Uri = idData;

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();

                reference.AddTransform(env);

                XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                reference.AddTransform(env1);

                signedXml.AddReference(reference);

                #region Lấy TimeSpan Hiện tại

                #endregion

                #region thẻ SigningTime
                XmlDocument xmlObject = new XmlDocument();
                xmlObject.PreserveWhitespace = true;
                XmlElement xmlProperties = xmlObject.CreateElement("SignatureProperties");
                XmlElement xmlProperty = xmlObject.CreateElement("SignatureProperty");
                xmlProperty.SetAttribute("Target", "#" + idSigner);
                XmlElement dataElement = xmlObject.CreateElement("SigningTime");
                DateTime dt = DateTime.Now;
                dataElement.AppendChild(xmlObject.CreateTextNode(dt.ToString("yyyy-MM-ddTHH:mm:ss")));
                xmlProperty.AppendChild(dataElement);
                xmlProperties.AppendChild(xmlProperty);
                xmlObject.AppendChild(xmlProperties);
                //document.DocumentElement.AppendChild(xmlProperties);
                //document.AppendChild(xmlProperties);


                DataObject dataObject = new DataObject();
                dataObject.Data = xmlObject.ChildNodes;
                dataObject.Id = string.Format("{0}Time", idSigner.Replace("#", ""));
                signedXml.AddObject(dataObject);
                #endregion

                #region Them reference

                #endregion

                Reference reference1 = new Reference();
                reference1.Uri = string.Format("#{0}Time", idSigner.Replace("#", ""));

                //XmlDsigEnvelopedSignatureTransform env2 = new XmlDsigEnvelopedSignatureTransform();
                //reference1.AddTransform(env2);
                //XmlDsigC14NTransform env3 = new XmlDsigC14NTransform();
                //reference1.AddTransform(env3);
                signedXml.AddReference(reference1);

                signedXml.ComputeSignature();

                XmlElement xmlDigitalSignature = signedXml.GetXml();


                XmlNodeList nodeList = document.GetElementsByTagName(tagSign);

                foreach (XmlNode node in nodeList)
                {
                    node.AppendChild(xmlDigitalSignature);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return document.OuterXml;

        }


        // Verify the signature of an XML file against an asymmetric
        // algorithm and return the result.
        public static Boolean VerifyXml(XmlDocument xmlDoc, RSA key)
        {
            // Check arguments.
            if (xmlDoc == null)
                throw new ArgumentException("xmlDoc");
            if (key == null)
                throw new ArgumentException("key");

            // Create a new SignedXml object and pass it
            // the XML document class.
            SignedXml signedXml = new SignedXml(xmlDoc);

            // Find the "Signature" node and create a new
            // XmlNodeList object.
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Signature");

            // Throw an exception if no signature was found.
            if (nodeList.Count <= 0)
            {
                throw new CryptographicException("Verification failed: No Signature was found in the document.");
            }

            signedXml.LoadXml((XmlElement)nodeList[0]);

            return signedXml.CheckSignature(key);
        }

        public static byte[] BytesDigitalSignature(X509Certificate2 myCertificate, byte[] bytePdf, string fieldName, string digestAlgorithms)
        {
            byte[] pdfBytes = null;

            using (MemoryStream msOutput = new MemoryStream())
            {
                if (msOutput.Position > 0)
                {
                    msOutput.Position = 0;
                }

                PdfReader Reader = new PdfReader(bytePdf);

                PdfStamper Stamper = PdfStamper.CreateSignature(Reader, msOutput, '\0', null, true);
                iTextSharp.text.pdf.PdfSignatureAppearance SAP = Stamper.SignatureAppearance;

                if (!string.IsNullOrWhiteSpace(fieldName))
                {
                    SAP.SetVisibleSignature(new iTextSharp.text.Rectangle(0, 0, 0, 0), Reader.NumberOfPages, fieldName);
                    SAP.SignDate = DateTime.Now;
                }

                Org.BouncyCastle.X509.X509Certificate BouncyCertificate = Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(myCertificate);
                var chain = new List<Org.BouncyCastle.X509.X509Certificate> { BouncyCertificate };

                IExternalSignature ES = new X509Certificate2Signature(myCertificate, digestAlgorithms);
                MakeSignature.SignDetached(SAP, ES, chain, null, null, null, 0, CryptoStandard.CMS);

                Stamper.Close();
                Reader.Close();

                pdfBytes = msOutput.ToArray();

                msOutput.Dispose();

            }
            return pdfBytes;
        }



        public static Boolean CheckIsTaxCode(string tax_code)
        {

            bool result = false;

            var patt = new Regex("^[0-9]{10}$");

            if (tax_code.Length > 10)
            {
                patt = new Regex("^[0-9]{10}[-][0-9]{3}$");
            }

            if (tax_code == "") return result;

            if (patt.IsMatch(tax_code) == true)
            {
                result = true;
                return result;
            }
            else
            {
                return result;
            }

            //for (int i = 0; i < tax_code.Length; i++)
            //{

            //    var number = Int32.Parse(tax_code[0].ToString()) * 31 + Int32.Parse(tax_code[1].ToString()) * 29 + Int32.Parse(tax_code[2].ToString()) * 23
            //  + Int32.Parse(tax_code[3].ToString()) * 19 + Int32.Parse(tax_code[4].ToString()) * 17 + Int32.Parse(tax_code[5].ToString()) * 13
            //  + Int32.Parse(tax_code[6].ToString()) * 7 + Int32.Parse(tax_code[7].ToString()) * 5 + Int32.Parse(tax_code[8].ToString()) * 3;

            //    return (10 - number % 11) == Int32.Parse(tax_code[9].ToString());
            //}



        }


        private static readonly string[] VietNamChar = new string[]
    {
        "aAeEoOuUiIdDyY",
        "áàạảãâấầậẩẫăắằặẳẵ",
        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
        "éèẹẻẽêếềệểễ",
        "ÉÈẸẺẼÊẾỀỆỂỄ",
        "óòọỏõôốồộổỗơớờợởỡ",
        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
        "úùụủũưứừựửữ",
        "ÚÙỤỦŨƯỨỪỰỬỮ",
        "íìịỉĩ",
        "ÍÌỊỈĨ",
        "đ",
        "Đ",
        "ýỳỵỷỹ",
        "ÝỲỴỶỸ"
};
        public static string convertStringVietNamese(string str)
        {
            //Thay thế và lọc dấu từng char      
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            return str;
        }

    }
}
