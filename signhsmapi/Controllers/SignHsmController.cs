﻿using System;
using System.Web.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SignHsmApi.Utils;
using System.Xml;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Threading;
using iTextSharp.text.pdf.security;

namespace SignHsmApi.Controllers
{
    public class SignHsmController : ApiController
    {

        [Route("api/SignHsm/GetInfoCert")]
        [HttpPost]
        public async Task<JObject> GetInfoCert([FromBody] JObject model)
        {
            //Json response
            JObject result = new JObject();
            X509Store store = null;

            try
            {
                // get value form json post 
                string SeriaNumber = model["SeriaNumber"].ToString();


                store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);

                bool hasCert = false;

                foreach (X509Certificate2 cert in store.Certificates)
                {
                    try
                    {
                        String sr = cert.GetSerialNumberString().ToString().ToUpper();

                        if (sr.CompareTo(SeriaNumber.ToUpper()) == 0)
                        {
                            hasCert = true;
                            result.Add("so_serial", cert.SerialNumber);
                            result.Add("issuer", cert.Issuer);
                            result.Add("subjectname", cert.Issuer + "; " + cert.Subject);
                            result.Add("ngaybatdau", string.Format("{0:yyyy-MM-dd HH:mm:ss}", cert.NotBefore));
                            result.Add("ngayketthuc", string.Format("{0:yyyy-MM-dd HH:mm:ss}", cert.NotAfter));

                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Loger.Error(ex.Message);
                        throw new Exception(ex.Message);
                    }
                }

                if (hasCert == false)
                {
                    Loger.Error("Khong ton tai serial");
                    throw new Exception("Khong ton tai serial");
                }

                result.Add("status", "OK");
                result.Add("message", "");
                return result;

            }
            catch (Exception ex)
            {
                Loger.Error(ex.Message);
                result.Add("data", "");
                result.Add("status", "Error");
                result.Add("message", ex.Message);
                return result;
            }
            finally
            {
                try
                {
                    store.Close();
                }
                catch (Exception ex) { }
            }
        }

        [Route("api/SignHsm/SignXML32")]
        [HttpPost]
        public async Task<JObject> SignXML32([FromBody] JObject model)
        {
            //Json response
            JObject result = new JObject();
            string xmlSign = "";
            X509Store store = null;
            try
            {
                // get value form json post 
                string SeriaNumber = model["SeriaNumber"].ToString();
                string idSigner = model["idSigner"].ToString();
                string xml = model["xml"].ToString();
                string tagSign = model["tagSign"].ToString();
                string dataType = model["dataType"].ToString();
                string idData = model["idData"].ToString();

                X509Certificate2 selectedCert = null;

                store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);

                RSACryptoServiceProvider m_privateKey = null;
                RSACryptoServiceProvider m_publicKey = null;

                bool hasCert = false;

                foreach (X509Certificate2 cert in store.Certificates)
                {
                    try
                    {
                        String sr = cert.GetSerialNumberString().ToString().ToUpper();

                        if (sr.CompareTo(SeriaNumber.ToUpper()) == 0)
                        {
                            hasCert = true;
                            //Loger.Error("Da compare SeriaNumber!");

                            selectedCert = cert;
                            m_privateKey = (RSACryptoServiceProvider)cert.PrivateKey;
                            m_publicKey = (RSACryptoServiceProvider)cert.PublicKey.Key;

                            if (m_privateKey.CspKeyContainerInfo.HardwareDevice)
                            {
                                xmlSign = CommonManager.SignXmlHsm32(idSigner, xml, selectedCert, tagSign, idData);

                                //// Create a new XML document.
                                //XmlDocument xmlDoc_v = new XmlDocument();
                                //// Load an XML file into the XmlDocument object.
                                //xmlDoc_v.PreserveWhitespace = true;
                                //xmlDoc_v.LoadXml(xmlSign);

                                //bool checkSign = CommonManager.VerifyXml(xmlDoc_v, m_publicKey);

                                //if (checkSign)
                                //{
                                //    //Loger.Error("Ky thanh cong !");
                                //}
                                //else
                                //{
                                //    throw new Exception("The XML signature is not valid.");
                                //}
                            }
                            else
                            {
                                xmlSign = "";
                                throw new Exception("Khong check duoc HardwareDevice !");
                            }
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Loger.Error(ex.Message);
                        throw new Exception(ex.Message);
                    }
                }

                if (hasCert == false)
                {
                    //Loger.Error("Khong ton tai serial");
                    throw new Exception("Khong ton tai serial");
                }

                result.Add("data", xmlSign);
                result.Add("status", "Ok");
                result.Add("message", "");
                return result;

            }
            catch (Exception ex)
            {
                //Loger.Error(ex.Message);
                result.Add("data", "");
                result.Add("status", "False");
                result.Add("message", ex.Message);
                return result;
            }
            finally
            {
                try
                {
                    store.Close();
                }
                catch (Exception ex) { }
            }
        }

        [Route("api/SignHsm/SignXML78")]
        [HttpPost]
        public async Task<JObject> SignXML78([FromBody] JObject model)
        {
            //Json response
            JObject result = new JObject();
            string xmlSign = "";
            X509Store store = null;
            try
            {
                // get value form json post 
                string SeriaNumber = model["SeriaNumber"].ToString();
                string idSigner = model["idSigner"].ToString();
                string xml = model["xml"].ToString();
                string tagSign = model["tagSign"].ToString();
                string dataType = model["dataType"].ToString();
                string idData = model["idData"].ToString();


                X509Certificate2 selectedCert = null;

                store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);

                RSACryptoServiceProvider m_privateKey = null;
                RSACryptoServiceProvider m_publicKey = null;

                bool hasCert = false;

                foreach (X509Certificate2 cert in store.Certificates)
                {
                    try
                    {
                        String sr = cert.GetSerialNumberString().ToString().ToUpper();

                        if (sr.CompareTo(SeriaNumber.ToUpper()) == 0)
                        {
                            hasCert = true;
                            //Loger.Error("Da compare SeriaNumber!");

                            selectedCert = cert;
                            m_privateKey = (RSACryptoServiceProvider)cert.PrivateKey;
                            m_publicKey = (RSACryptoServiceProvider)cert.PublicKey.Key;

                            if (m_privateKey.CspKeyContainerInfo.HardwareDevice)
                            {
                                xmlSign = CommonManager.SignXmlHsm78(idSigner, xml, selectedCert, tagSign, idData);

                                //// Create a new XML document.
                                //XmlDocument xmlDoc_v = new XmlDocument();
                                //// Load an XML file into the XmlDocument object.
                                //xmlDoc_v.PreserveWhitespace = true;
                                //xmlDoc_v.LoadXml(xmlSign);

                                //bool checkSign = CommonManager.VerifyXml(xmlDoc_v, m_publicKey);

                                //if (checkSign)
                                //{
                                //    //Loger.Error("Ky thanh cong !");
                                //}
                                //else
                                //{
                                //    throw new Exception("The XML signature is not valid.");
                                //}
                            }
                            else
                            {
                                xmlSign = "";
                                throw new Exception("Khong check duoc HardwareDevice !");
                            }
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Loger.Error(ex.Message);
                        throw new Exception(ex.Message);
                    }
                }

                if (hasCert == false)
                {
                    //Loger.Error("Khong ton tai serial");
                    throw new Exception("Khong ton tai serial");
                }

                result.Add("data", xmlSign);
                result.Add("status", "Ok");
                result.Add("message", "");
                return result;

            }
            catch (Exception ex)
            {
                //Loger.Error(ex.Message);
                result.Add("data", "");
                result.Add("status", "False");
                result.Add("message", ex.Message);
                return result;
            }
            finally
            {
                try
                {
                    store.Close();
                }
                catch (Exception ex) { }
            }
        }

        [Route("api/SignHsmPdf/SignPdf")]
        [HttpPost]
        public async Task<HttpResponseMessage> SignPdf()
        {
            HttpResponseMessage result = null;
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            X509Store store = null;

            byte[] bytes = null;
            byte[] bytePdfSign = null;
            string SeriaNumber = "";
            string fileName = "";
            string fieldName = "";
            string digestAlgorithms = DigestAlgorithms.SHA1;

            try
            {

                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }
                        else
                        {
                            if (stream.Headers.ContentDisposition.Name.Replace("\"", "").Equals("serialNumber"))
                            {
                                SeriaNumber = await stream.ReadAsStringAsync();
                            }
                            else if (stream.Headers.ContentDisposition.Name.Replace("\"", "").Equals("fieldName"))
                            {
                                fieldName = await stream.ReadAsStringAsync();
                            }
                            else if (stream.Headers.ContentDisposition.Name.Replace("\"", "").Equals("digestAlgorithms"))
                            {
                                digestAlgorithms = await stream.ReadAsStringAsync();
                            }
                        }
                    }
                }


                // File không được quá 500MB
                if (bytes.Length > 524288000)
                {
                    Loger.Error("File qua kich thuoc!");

                    string error = "File qua kich thuoc!";

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(error);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    result.Content.Headers.ContentLength = error.Length;
                    return result;

                }


                if (bytes.Length == 0)
                {
                    string error = "File ky khong duoc de trong!";
                    Loger.Error(error);
                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(error);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    result.Content.Headers.ContentLength = error.Length;
                    return result;

                }


                if (!fileName.Contains(".pdf"))
                {

                    string error = "File khong dung dinh dang!";
                    Loger.Error(error);
                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(error);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    result.Content.Headers.ContentLength = error.Length;
                    return result;

                }

                X509Certificate2 selectedCert = null;

                store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);

                RSACryptoServiceProvider m_privateKey = null;
                RSACryptoServiceProvider m_publicKey = null;

                bool hasCert = false;

                foreach (X509Certificate2 cert in store.Certificates)
                {
                    try
                    {
                        string sr = cert.GetSerialNumberString().ToString().ToUpper();

                        if (sr.CompareTo(SeriaNumber.ToUpper()) == 0)
                        {
                            hasCert = true;
                            Loger.Error("Da compare SeriaNumber!");

                            selectedCert = cert;
                            m_privateKey = (RSACryptoServiceProvider)cert.PrivateKey;
                            m_publicKey = (RSACryptoServiceProvider)cert.PublicKey.Key;

                            if (m_privateKey.CspKeyContainerInfo.HardwareDevice)
                            {
                                bytePdfSign = CommonManager.BytesDigitalSignature(selectedCert, bytes, fieldName, digestAlgorithms);
                            }
                            else
                            {
                                throw new Exception("Khong check duoc HardwareDevice !");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Loger.Error(ex.Message);
                        throw new Exception(ex.Message);
                    }
                }

                if (hasCert == false)
                {
                    Loger.Error("Khong ton tai serial");
                    throw new Exception("Khong ton tai serial");
                }

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(bytePdfSign);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                result.Content.Headers.ContentDisposition.FileName = fileName;
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentLength = bytePdfSign.Length;
                return result;

            }
            catch (System.Exception ex)
            {
                Loger.Error("Lỗi " + ex.Message);
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
                return result;
            }
            finally
            {
                try
                {
                    store.Close();
                }
                catch (Exception ex) { }
            }

        }

        [Route("api/Hsm/Part1")]
        [HttpPost]
        public async Task<HttpResponseMessage> Part1([FromBody] JObject model)
        {
            string lablel = model["tax_code"].ToString();
            string nameCompany = model["name"].ToString();
            string province = model["province"].ToString();

            HttpResponseMessage result = null;

            nameCompany = CommonManager.convertStringVietNamese(nameCompany);
            province = CommonManager.convertStringVietNamese(province);


            if (lablel.Length >= 70 || nameCompany.Length >= 70 || province.Length >= 70)
            {

                string err = "Khong duoc nhap gia tri qua 70 ky tu!";
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(err);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = err.Length;
                return result;
            }

            var regexItem = new Regex("^[a-zA-Z0-9- ]*$");

            if (!regexItem.IsMatch(nameCompany))
            {

                string err = "Ten khach hang khong dung dinh dang!";
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(err);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = err.Length;
                return result;
            }


            if (!regexItem.IsMatch(province))
            {

                string err = "Dia chi khach hang khong dung dinh dang!";
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(err);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = err.Length;
                return result;
            }

            Loger.Error("Da vao part 1");
            //JObject result = new JObject();

            string publicHandle = "";
            string privateHandle = "";

            byte[] byteCsr = null;

            try
            {

                string labelPublic = lablel + "-public";
                string labelPrivate = lablel + "-private";

                Process process = new Process();
                process.StartInfo.FileName = "C:\\Program Files\\SafeNet\\LunaClient\\Cmu.exe";
                process.StartInfo.Arguments = "gen -password=Mbf@123688 -modulusBits=2048 -publicExp=65537 -sign=T -verify=T -encrypt=T -decrypt=T  -labelPublic=\"" + labelPublic + "\" -labelPrivate=\"" + labelPrivate + "\"  -keygenmech=1"; // Note the /c command (*)
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();
                //* Read the output (or the error)
                string output = process.StandardOutput.ReadToEnd();

                Loger.Error("gen -password=Mbf@123688 -modulusBits=2048 -publicExp=65537 -sign=T -verify=T -encrypt=T -decrypt=T  -labelPublic=\"" + labelPublic + "\" -labelPrivate=\"" + labelPrivate + "\"  -keygenmech=1");
                Loger.Error("output " + output);

                output = output.Trim().Replace("Certificate Management Utility(64 - bit) v7.2.0 - 220.Copyright(c) 2018 SafeNet.All rights reserved.", "");

                Process process1 = new Process();
                process1.StartInfo.FileName = "C:\\Program Files\\SafeNet\\LunaClient\\Cmu.exe";
                process1.StartInfo.Arguments = "list -password=\"Mbf@123688\" -display=handle -class=public -label=\"" + labelPublic + "\""; // Note the /c command (*)
                process1.StartInfo.UseShellExecute = false;
                process1.StartInfo.RedirectStandardOutput = true;
                process1.StartInfo.RedirectStandardError = true;
                process1.Start();
                //* Read the output (or the error)
                string output1 = process1.StandardOutput.ReadToEnd();

                process1.WaitForExit();
                if (output1.Trim().Contains("handle"))
                {
                    publicHandle = output1.Replace("\r\n", "").Substring(output1.IndexOf("=") - 3).Trim();
                    Loger.Error("publicHandle " + publicHandle);
                }
                else
                {
                    string err = "Có lỗi ở bước 2 lấy publicHandle: " + output1.Replace("\r\n", "").Replace(@"""", "");
                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(err);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    result.Content.Headers.ContentLength = err.Length;
                    return result;
                }


                Process process2 = new Process();
                process2.StartInfo.FileName = "C:\\Program Files\\SafeNet\\LunaClient\\Cmu.exe";
                process2.StartInfo.Arguments = "list -password=\"Mbf@123688\" -display=handle -class=private -label=\"" + labelPrivate + "\""; // Note the /c command (*)
                process2.StartInfo.UseShellExecute = false;
                process2.StartInfo.RedirectStandardOutput = true;
                process2.StartInfo.RedirectStandardError = true;
                process2.Start();
                //* Read the output (or the error)
                string output2 = process2.StandardOutput.ReadToEnd();

                process2.WaitForExit();
                if (output2.Trim().Contains("handle"))
                {
                    privateHandle = output2.Replace("\r\n", "").Substring(output1.IndexOf("=") - 3).Trim();
                    Loger.Error("privateHandle " + privateHandle);
                }
                else
                {
                    string err = "Có lỗi ở bước 2 lấy privateHandle: " + output2.Replace("\r\n", "").Replace(@"""", "");
                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(err);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                    result.Content.Headers.ContentLength = err.Length;
                    return result;
                }

                if (output1.Contains("handle") && output2.Contains("handle"))
                {

                    string fileCsr = lablel + ".csr";

                    Process process3 = new Process();

                    process3.StartInfo.FileName = "cscript.exe";
                    string scriptName = "C:\\Program Files\\SafeNet\\LunaClient\\createCsr.vbs";

                    process3.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"", scriptName, publicHandle, privateHandle, province, fileCsr, nameCompany);

                    process3.StartInfo.UseShellExecute = false;
                    process3.StartInfo.RedirectStandardOutput = true;
                    process3.StartInfo.RedirectStandardError = true;
                    process3.Start();
                    //* Read the output (or the error)
                    string output3 = process3.StandardOutput.ReadToEnd();

                    //output3 = output3.Replace("Microsoft (R) Windows Script Host Version 5.812\r\n"
                    // + "Copyright(C) Microsoft Corporation.All rights reserved.", "");

                    Loger.Error("step3 " + output3.Trim());

                    process3.WaitForExit();

                    if (output3.Trim().Contains("successfull"))
                    {


                        //Thread.Sleep(10000);

                        Loger.Error("Vao Buoc 4");
                        Process process4 = new Process();

                        process4.StartInfo.FileName = "cscript.exe";
                        //process.StartInfo.Arguments = "cscript.exe hsm1.vbs";
                        string scriptName1 = "C:\\Program Files\\SafeNet\\LunaClient\\inputFileMST.vbs";

                        process4.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\"", scriptName1, lablel);
                        process4.StartInfo.UseShellExecute = false;
                        process4.StartInfo.RedirectStandardOutput = true;
                        process4.StartInfo.RedirectStandardError = true;

                        process4.Start();
                        //* Read the output (or the error)
                        string output4 = process4.StandardOutput.ReadToEnd();
                        //output4 = output4.Replace("Certificate Management Utility(64 - bit) v7.2.0 - 220.Copyright(c) 2018 SafeNet.All rights reserved.", "");
                        Loger.Error("step4 " + output4);
                        process4.WaitForExit();

                        string curFile = "C:\\Program Files\\SafeNet\\LunaClient\\" + fileCsr;


                        if (File.Exists(curFile))
                        {
                            //result.Add("success: ", "Đã qua bước 4");
                            byteCsr = File.ReadAllBytes(curFile);

                        }
                        else
                        {
                            string err = "Có lỗi ở bước 4!";
                            result = new HttpResponseMessage(HttpStatusCode.OK);
                            result.Content = new StringContent(err);
                            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                            result.Content.Headers.ContentLength = err.Length;
                            return result;

                            // result.Add("có lỗi ở bước 4 ", output4);
                        }

                    }
                    else
                    {

                        string err = "Có lỗi ở bước 3: " + output3.Replace("\r\n", "").Replace(@"""", "");
                        result = new HttpResponseMessage(HttpStatusCode.OK);
                        result.Content = new StringContent(err);
                        result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                        result.Content.Headers.ContentLength = err.Length;
                        return result;
                    }
                }


                process.WaitForExit();

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(byteCsr);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attach");
                result.Content.Headers.ContentDisposition.FileName = lablel;
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentLength = byteCsr.Length;
                return result;

                // return result;
                // }
            }
            catch (Exception ex)
            {
                Loger.Error("Lỗi " + ex.Message);
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(ex.Message);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                result.Content.Headers.ContentLength = ex.Message.Length;
                return result;
            }

        }



        [Route("api/Hsm/Part2")]
        [HttpPost]
        public async Task<JObject> Part2()
        {
            JObject json = new JObject();
            //HttpResponseMessage result = null;
            // Check if the request contains multipart / form - data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            byte[] bytes = null;
            string fileName = "";

            try
            {
                var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

                foreach (var stream in filesReadToProvider.Contents)
                {
                    if (stream.IsFormData())
                    {
                        var s = await stream.ReadAsFormDataAsync();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stream.Headers.ContentDisposition.FileName))
                        {
                            bytes = await stream.ReadAsByteArrayAsync();
                            fileName = stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                        }

                    }
                }

                Loger.Error("fileName: " + fileName);


                if (!fileName.EndsWith(".cer") && !fileName.EndsWith(".pem"))
                {

                    json.Add("error", "File Certificate khong dung dinh dang!");
                    return json;
                }

                string label = fileName.Replace(".cer", "").Replace(".pem", "");

                if (label.Length >= 80)
                {
                    json.Add("error", "Ten File khong duoc qua 80 ky tu!");
                    return json;
                }

                string slot = ConfigurationManager.AppSettings["Slot"];

                string filePath = "C:\\Program Files\\SafeNet\\LunaClient\\" + fileName;

                File.WriteAllBytes(filePath, bytes);

                Process process = new Process();

                process.StartInfo.FileName = "cscript.exe";

                string scriptName = "C:\\Program Files\\SafeNet\\LunaClient\\inputFileCert.vbs";

                process.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\"", scriptName, fileName);
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();

                string output = process.StandardOutput.ReadToEnd();

                Loger.Error(" " + output);

                process.WaitForExit();

                json.Add("success", "Import File Success");
                return json;
            }
            catch (System.Exception ex)
            {
                json.Add("error", ex.Message);
                return json;

            }

        }

    }
}
